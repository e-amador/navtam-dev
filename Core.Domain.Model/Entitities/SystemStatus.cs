﻿using NavCanada.Core.Domain.Model.Enums;
using System;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class SystemStatus
    {
        public int Id { get; set; }
        public SystemStatusEnum Status { get; set; }
        public DateTime LastUpdated { get; set; }
        public string LastError { get; set; }
    }
}

﻿using alatas.GeoJSON4EntityFramework;
using Core.Common.Geography;
using NavCanada.Core.Common.Common;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using System;
using System.Data.Entity.Spatial;
using System.Device.Location;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [Authorize]
    [RoutePrefix("api/doas")]
    public class DoaDashboardController : ApiBaseController
    {
        public DoaDashboardController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger) : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
        }

        [HttpGet]
        [Route("GeoJson")]
        public async Task<IHttpActionResult> GeoJson(int doaId)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                // NOF is too big to render and transfer every time
                var nof = (await IsLoggedUserInRole(CommonDefinitions.NofUserRole) || await IsLoggedUserInRole(CommonDefinitions.NofAdminRole));
                if (nof)
                    return Ok((new FeatureCollection()).Serialize(prettyPrint: false));

                var doaGeoArea = await GeoLocationCache.GetUserDoa(Uow, ApplicationContext.GetUserId());

                FeatureCollection features = null;

                if (doaGeoArea != null)
                {
                    try
                    {
                        features = new FeatureCollection(doaGeoArea.Normalize(), false);
                    }
                    catch (Exception)
                    {
                        //return InternalServerError(e);
                    }
                }

                return Ok(features?.Serialize(prettyPrint: false));

            }, "api/doas/GeoJson");
        }

        [HttpGet]
        [Route("InDoa")]
        public async Task<IHttpActionResult> InDoa(string location)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                var doaGeoArea = await GeoLocationCache.GetUserDoa(Uow, ApplicationContext.GetUserId());
                if (doaGeoArea == null)
                    return BadRequest();

                location = location.ToUpper();

                var dmsLocation = DMSLocation.FromKnownFormat(location);
                if (dmsLocation == null)
                    return BadRequest();

                var locationGeo = DbGeography.FromText(dmsLocation.GeoPointText);
                var inDoa = doaGeoArea.Intersects(locationGeo);

                var isInBilingualRegion = GeoDataService.IsInBilingualRegion(locationGeo, Uow);

                return Ok(new { Location = location, InDoa = inDoa, InBilingualRegion = isInBilingualRegion });

            }, "api/doas/InDoa");
        }
    }
}
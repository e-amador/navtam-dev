﻿using System.Data.Entity;
using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Domain.Model.Entitities;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using NavCanada.Core.Common.Common;
using NavCanada.Core.Data.NotamWiz.EF.Extensions;
using NavCanada.Core.Domain.Model.Dtos;
using System;

namespace NavCanada.Core.Data.EF.Repositories
{
    public class MessageExchangeQueueRepo : Repository<MessageExchangeQueue>, IMessageExchangeQueueRepo
    {
        public MessageExchangeQueueRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public async Task<MessageExchangeQueue> GetByIdAsync(Guid id)
        {
            return await DbSet.SingleOrDefaultAsync(e => e.Id == id);
        }

        public async Task<int> GetAllIncomingCountAsync(MessageExchangeStatus status)
        {
            if (status == MessageExchangeStatus.None)
                return await GetAllIncomingAndImportantCountAsync();

            return await DbSet.CountAsync(e => e.Inbound && e.Status == status);
        }

        public async Task<int> GetAllParkedCountAsync()
        {
            return await DbSet.CountAsync(e => e.Status == MessageExchangeStatus.Parked);
        }

        public async Task<int> GetAllUnreadCountAsync()
        {
            return await DbSet.CountAsync(e => !e.Read);
        }

        public async Task<int> GetAllOutgoingCountAsync(MessageExchangeStatus status)
        {
            if (status == MessageExchangeStatus.None)
                return await GetAllOutgoingAndImportantCountAsync();

            return await DbSet.CountAsync(e => !e.Inbound && e.Status == status);
        }

        public async Task<List<MessageExchangeQueueDto>> GetAllIncomingAsync(MessageExchangeStatus status, QueryOptions queryOptions)
        {
            if (status == MessageExchangeStatus.None)
                return await GetAllIncomingAndImportantAsync(queryOptions);


            var messages = await DbSet
                    .Where(e => e.Inbound && e.Status == status)
                    .ApplySort(queryOptions.Sort)
                    .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                    .Take(queryOptions.PageSize)
                    .ToListAsync();

            return GenerateMessageDto(messages);

        }

        public async Task<List<MessageExchangeQueueDto>> GetAllOutgoingAsync(MessageExchangeStatus status, QueryOptions queryOptions)
        {
            if (status == MessageExchangeStatus.None)
                return await GetAllOutgoingAndImportantAsync(queryOptions);


            var messages = await DbSet
                    .Where(e => !e.Inbound && e.Status == status)
                    .ApplySort(queryOptions.Sort)
                    .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                    .Take(queryOptions.PageSize)
                    .ToListAsync();

            return GenerateMessageDto(messages);

        }

        public async Task<List<MessageExchangeQueueDto>> GetAllParkedMessagesAsync(QueryOptions queryOptions)
        {
            var messages = await DbSet
                    .Where(e => e.Status == MessageExchangeStatus.Parked)
                    .ApplySort(queryOptions.Sort)
                    .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                    .Take(queryOptions.PageSize)
                    .ToListAsync();

            return GenerateMessageDto(messages);

        }

        private async Task<int> GetAllIncomingAndImportantCountAsync()
        {
            return await DbSet.CountAsync(e => e.Inbound && e.Status != MessageExchangeStatus.Parked && e.Status != MessageExchangeStatus.Deleted);
        }

        private async Task<int> GetAllOutgoingAndImportantCountAsync()
        {
            return await DbSet.CountAsync(e => !e.Inbound && (e.Status == MessageExchangeStatus.None || e.Status == MessageExchangeStatus.Important));
        }

        public async Task<List<MessageExchangeQueueDto>> GetAllIncomingAndImportantAsync(QueryOptions queryOptions)
        {
            var messages = await DbSet
                    .Where(e => e.Inbound && e.Status != MessageExchangeStatus.Parked && e.Status != MessageExchangeStatus.Deleted)
                    .ApplySort(queryOptions.Sort)
                    .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                    .Take(queryOptions.PageSize)
                    .ToListAsync();

            return GenerateMessageDto(messages);
        }

        public async Task<List<MessageExchangeQueueDto>> GetAllOutgoingAndImportantAsync(QueryOptions queryOptions)
        {
            var messages = await DbSet
                    .Where(e => !e.Inbound && (e.Status == MessageExchangeStatus.None || e.Status == MessageExchangeStatus.Important))
                    .ApplySort(queryOptions.Sort)
                    .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                    .Take(queryOptions.PageSize)
                    .ToListAsync();

            return GenerateMessageDto(messages);
        }


        private List<MessageExchangeQueueDto> GenerateMessageDto(List<MessageExchangeQueue> messages)
        {
            var dtos = new List<MessageExchangeQueueDto>();
            foreach (var message in messages)
            {
                dtos.Add(new MessageExchangeQueueDto
                {
                    Id = message.Id,
                    MessageType = message.MessageType,
                    ClientType = message.ClientType,
                    ClientAddress = message.ClientAddress,
                    ClientName = message.ClientName,
                    Recipients = message.Recipients,
                    Status = message.Status,
                    Locked = message.Locked,
                    Read = message.Read,
                    PriorityCode = message.PriorityCode, 
                    Delivered = message.Delivered,
                    Body = message.Body,
                    LastOwner = message.LastOwner,
                    Created = message.Created
                });
            }
            return dtos;
        }
    }
}

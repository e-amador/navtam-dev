﻿import { NgModule, APP_INITIALIZER, Injectable } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Select2Module } from 'ng2-select2';
import { DateTimePickerModule } from '../common/components/ng2-date-time-picker';

//import { StartupService } from './startup.service';
import { LocalizationModule, LocaleValidationModule, LocaleService, TranslationService } from 'angular-l10n';

import {
    JQUERY_TOKEN, //OR JQUERY_PROVIDER,
    TOASTR_TOKEN,
    Toastr,
    WindowRef,
    XsrfTokenService,
    Ng2ICheckModule,
    SimpleModalComponent,
    ServerPaginationComponent,
    ModalTriggerDirective,
    DateTimeFormatDirective,
    DatexPipe,
} from '../common/index';

import { UserComponent } from './usr.component';

import {
    //Shared
    DataService,
    NotifyService,
    MemoryStorageService,
    LocationService,
    ReportFilterComponent,
    ReportContainerComponent,
    ReportNotamComponent,
    ReportStatsComponent,
    ReportActiveComponent,
    //Proposals
    ProposalFilterComponent,
    UserListComponent,
    UserCreateComponent,
    UserEditComponent,
    UserViewComponent,
    UserViewHistoryComponent,
    //Nsds
    DynamicComponent,

    CatchAllDraftComponent,
    CatchAllCancelComponent,
    CatchAllService,

    ObstComponent,
    ObstCancelComponent,
    ObstacleService,

    MObstService,
    MObstComponent,
    MObstCancelComponent,

    ObstLgUsComponent,
    ObstLgUsCancelComponent,
    ObstacleLgUsService,

    MObstLgUsComponent,
    MObstLgUsCancelComponent,

    RwyClsdComponent,
    RwyClsdCancelComponent,
    RwyClsdService,

    TwyClsdComponent,
    TwyClsdCancelComponent,
    TwyClsdService,

    CarsClsdComponent,
    CarsClsdCancelComponent,
    CarsClsdService,

    AreaDefinitionNsdComponent,
    AreaDefinitionCancelNsdComponent,
    AreaDefinitionNsdService,

    RscComponent,
    RscService,
    RscContaminantsComponent,
    ClearedWidthComponent,
    ClearingOperationsComponent,
    SnowBanksComponent,
    RscTreatmentsComponent,
    RwyFrictionComponent,
    RemainingWidthRemarksComponent,
    NextObservationComponent,
    WindrowsComponent,
    TaxiwaysRemarksComponent,
    ApronRemarksComponent,

    NsdIcaoComponent,
    NsdIcaoFormatComponent,
    NsdFormComponent,
    NsdButtonBarComponent,
    NsdGeoRefToolComponent,
    NsdSchedulerDailyComponent,
    NsdSchedulerDateComponent,
    NsdSchedulerDaysOfWeekComponent,
    NsdSchedulerFreeTextComponent,
    //Resolvers
    CategoriesResolver,
    ProposalModelResolver,
    ProposalReadOnlyModelResolver,
    ProposalCreateModelResolver,
    ProposalCancelModelResolver,
    ProposalReplaceModelResolver,
    ProposalCloneModelResolver,
    ProposalHistoryModelResolver
} from './usr.index'

import { userRoutes } from './usr.routes';

declare let toastr: Toastr;
//declare let $: Object;

//export function startupServiceFactory(startupService: StartupService): Function {
//    return () => startupService.load();
//}

//export function createConfig(): SignalRConfiguration {
//    const c = new SignalRConfiguration();
//    c.hubName = 'SignalRHub';
//    //c.qs = { user: 'user.subscribed' };
//    c.url = window.location.origin;
//    c.logging = true;
//    return c;
//}

@Injectable()
export class LocalizationConfig {

    constructor(public locale: LocaleService, public translation: TranslationService) { }

    load(): Promise<any> {
        this.locale.addConfiguration()
            .addLanguage('en', 'ltr')
            .addLanguage('fr', 'ltr')
            //.setCookieExpiration(30)
            .defineLanguage(window['app'].cultureInfo)
            .defineCurrency("CAD")
            .defineDefaultLocale('en', 'CA')
        this.locale.init();

        this.translation.addConfiguration()
            .addProvider('./app/resources/locale-');

        let promise: Promise<any> = new Promise((resolve: any) => {
            this.translation.translationChanged.subscribe(() => {
                resolve(true);
            });
        });

        this.translation.init();

        return promise;
    }
}

// AoT compilation requires a reference to an exported function.
export function initLocalization(localizationConfig: LocalizationConfig): Function {
    return () => localizationConfig.load();
}

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forRoot(userRoutes),
        Select2Module,
        DateTimePickerModule,
        Ng2ICheckModule,
        //SignalRModule.forRoot(createConfig),
        LocalizationModule.forRoot() // New instance of TranslationService
    ],
    declarations: [
        UserComponent,
        //common
        DatexPipe,
        DateTimeFormatDirective,
        ModalTriggerDirective,
        SimpleModalComponent,
        ServerPaginationComponent,
        //Nsd Form,
        NsdFormComponent,
        NsdButtonBarComponent,
        NsdGeoRefToolComponent,
        NsdIcaoComponent,
        NsdIcaoFormatComponent,
        NsdSchedulerDailyComponent,
        NsdSchedulerDateComponent,
        NsdSchedulerDaysOfWeekComponent,
        NsdSchedulerFreeTextComponent,
        //UserComponent,
        UserListComponent,
        UserCreateComponent,
        UserEditComponent,
        UserViewComponent,
        UserViewHistoryComponent,
        ProposalFilterComponent,
        DynamicComponent,
        ReportFilterComponent,
        ReportContainerComponent,
        ReportNotamComponent,
        ReportStatsComponent,
        ReportActiveComponent,
        // CatchAll
        CatchAllDraftComponent,
        CatchAllCancelComponent,
        // Obstacle
        ObstComponent,
        ObstCancelComponent,
        // Obstacle LG US
        ObstLgUsComponent,
        ObstLgUsCancelComponent,
        //Multi Obstacle
        MObstComponent,
        MObstCancelComponent,
        //Multi Obstacle LG US
        MObstLgUsComponent,
        MObstLgUsCancelComponent,
        // Runway closed
        RwyClsdComponent,
        RwyClsdCancelComponent,
        // Taxyway closed
        TwyClsdComponent,
        TwyClsdCancelComponent,
        // CARS closed
        CarsClsdComponent,
        CarsClsdCancelComponent,
        // Airspace
        AreaDefinitionNsdComponent,
        AreaDefinitionCancelNsdComponent,
        CarsClsdCancelComponent,
        //Rsc
        RscComponent,
        RscContaminantsComponent,
        ClearedWidthComponent,
        ClearingOperationsComponent,
        SnowBanksComponent,
        RscTreatmentsComponent,
        RwyFrictionComponent,
        RemainingWidthRemarksComponent,
        NextObservationComponent,
        WindrowsComponent,
        TaxiwaysRemarksComponent,
        ApronRemarksComponent,
    ],
    providers: [
        DataService,
        NotifyService,
        MemoryStorageService,
        LocationService,
        CatchAllService,
        ObstacleService,
        ObstacleLgUsService,
        MObstService,
        RwyClsdService,
        TwyClsdService,
        CarsClsdService,
        AreaDefinitionNsdService,
        RscService,
        CategoriesResolver,
        ProposalModelResolver,
        ProposalReadOnlyModelResolver,
        ProposalCreateModelResolver,
        ProposalCancelModelResolver,
        ProposalReplaceModelResolver,
        ProposalCloneModelResolver,
        ProposalHistoryModelResolver,
        //StartupService,
        LocalizationConfig,
        {
            provide: APP_INITIALIZER,
            useFactory: initLocalization,
            deps: [LocalizationConfig],
            multi: true
        },
        //common
        //{
        //    // Provider for APP_INITIALIZER
        //    provide: APP_INITIALIZER,
        //    useFactory: startupServiceFactory,
        //    deps: [StartupService],
        //    multi: true
        //},
        //{ provide: JQUERY_TOKEN, useValue: $ }, 
        { provide: TOASTR_TOKEN, useValue: toastr },
        { provide: 'canDeactivateCreateEvent', useValue: checkDirtyState }, //route-guard
        XsrfTokenService,
        WindowRef
    ],
    bootstrap: [UserComponent]
})
export class UserModule { }

function checkDirtyState(component: UserCreateComponent): Promise<boolean> {
    let el: any = $('#modal-unsave-changes');
    let $modal = el.modal();
    return new Promise<boolean>((resolve, reject) => {
        if (!component.nsdForm.isSaveButtonDisabled) {
            let alreadyResolved = false;
            $("#btn-yes").click(function () {
                resolve(true);
                alreadyResolved = true;
            });
            $("#btn-no").click(function () {
                resolve(false);
                alreadyResolved = true;
            });
            el.on('hide.bs.modal', function (e) {
                if (!alreadyResolved) { resolve(false); }
            })
            $modal.modal("show");
        }
        else {
            resolve(true);
        }
    });
}
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RscService = void 0;
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Rx_1 = require("rxjs/Rx");
var xsrf_token_service_1 = require("../../../common/xsrf-token.service");
var windowRef_service_1 = require("../../../common/windowRef.service");
var RscService = /** @class */ (function () {
    function RscService(http, winRef, xsrfTokenService) {
        this.http = http;
        this.xsrfTokenService = xsrfTokenService;
        this.app = winRef.nativeWindow['app'];
    }
    RscService.prototype.getSdoRwys = function (sdoId) {
        if (this.app.antiForgeryTokenValue) {
            this.xsrfTokenService.addForgeryTokenToHeader(this.app.antiForgeryTokenValue);
        }
        return this.http.get(this.app.apiUrl + "rsc/GetRwys/?sdoId=" + sdoId)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    RscService.prototype.calculateRwyCC = function (data) {
        if (this.app.antiForgeryTokenValue) {
            this.xsrfTokenService.addForgeryTokenToHeader(this.app.antiForgeryTokenValue);
        }
        return this.http.post(this.app.apiUrl + "rsc/CalculateRwyCC/", data)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    RscService.prototype.calculateRwyCondCode = function (data) {
        if (this.app.antiForgeryTokenValue) {
            this.xsrfTokenService.addForgeryTokenToHeader(this.app.antiForgeryTokenValue);
        }
        return this.http.post(this.app.apiUrl + "rsc/CalculateRwyCC/", data)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    RscService.prototype.deepCopy = function (obj) {
        var copy;
        // Handle the 3 simple types, and null or undefined
        if (null == obj || "object" != typeof obj)
            return obj;
        // Handle Date
        if (obj instanceof Date) {
            copy = new Date();
            copy.setTime(obj.getTime());
            return copy;
        }
        // Handle Array
        if (obj instanceof Array) {
            copy = [];
            for (var i = 0, len = obj.length; i < len; i++) {
                copy[i] = this.deepCopy(obj[i]);
            }
            return copy;
        }
        // Handle Object
        if (obj instanceof Object) {
            copy = {};
            for (var attr in obj) {
                if (obj.hasOwnProperty(attr))
                    copy[attr] = this.deepCopy(obj[attr]);
            }
            return copy;
        }
        throw new Error("Unable to copy obj! Its type isn't supported.");
    };
    RscService.prototype.handleError = function (error) {
        return Rx_1.Observable.throw(error.statusText);
    };
    RscService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http, windowRef_service_1.WindowRef, xsrf_token_service_1.XsrfTokenService])
    ], RscService);
    return RscService;
}());
exports.RscService = RscService;
//# sourceMappingURL=rsc.service.js.map
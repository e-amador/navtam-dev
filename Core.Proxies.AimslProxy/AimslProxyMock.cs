﻿using System;
using System.Collections.Generic;
using AutoMapper;
using log4net;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AimslProxy.AimslServiceMock;
using NavCanada.Core.Proxies.AimslProxy.Common;
using NavCanada.Core.Proxies.AimslProxy.Mappers;

namespace NavCanada.Core.Proxies.AimslProxy
{
    public class AimslProxyMock : IAimslProxy
    {
        readonly ILog _logger = LogManager.GetLogger(typeof(AimslProxyMock));

        public AimslProxyMock()
        {
            AutoMapperConfig.RegisterMaps();    
        }

        public AismlResponse Submit(Domain.Model.Entitities.NotamProposal proposal, string accountableSource)
        {
            try
            {
                //TODO: Update AimslMock (Proxy.NotamProposal) to include the accountableSource parameter to match the real AimlsService.
                //AccountableSource will be ignored at this moment
                var cli = new AIMSLSoapClient();
                NotamProposalStatusMessage msg = cli.SubmitNotamProposal(Mapper.Map<Domain.Model.Entitities.NotamProposal, NotamProposal>(proposal));

                return Mapper.Map<NotamProposalStatusMessage, AismlResponse>(msg);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return new AismlResponse
                {
                    Code = ApiStatusCode.ConnectionError,
                    ErrorMessage = e.Message
                };
            }
        }


        public AismlResponse CheckStatus(long notamProposalId)
        {
            try
            {
                var cli = new AIMSLSoapClient();
                NotamProposalStatusMessage msg = cli.GetNotamProposalStatus(notamProposalId);

                return Mapper.Map<NotamProposalStatusMessage, AismlResponse>(msg);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return new AismlResponse
                {
                    Code = ApiStatusCode.ConnectionError,
                    ErrorMessage = e.Message
                };
            }
        }

        public List<AismlResponse> CheckStatus(long[] notamProposalIds)
        {
            throw new NotImplementedException();
        }

        public AismlResponse Widthraw(long notamProposalId)
        {
            try
            {
                var cli = new AIMSLSoapClient();
                NotamProposalStatusMessage msg = cli.WithdrawNotamProposal(notamProposalId);

                return Mapper.Map<NotamProposalStatusMessage, AismlResponse>(msg);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return new AismlResponse
                {
                    Code = ApiStatusCode.ConnectionError,
                    ErrorMessage = e.Message
                };
            }
        }
    }
}

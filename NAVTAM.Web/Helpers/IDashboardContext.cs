﻿using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.NsdEngine.Helpers;

namespace NAVTAM.Helpers
{
    public interface IDashboardContext
    {
        IUow Uow { get; }
        IUow GetTempUow();
        INsdManagerResolver GetNsdManagerResolver();
        INotamQueuePublisher GetQueuePublisher();
        IAeroRdsProxy GetAeroRdsProxy();
    }
}
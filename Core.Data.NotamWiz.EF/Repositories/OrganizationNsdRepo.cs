﻿using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Domain.Model.Entitities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    public class OrganizationNsdRepo : Repository<OrganizationNsdCategory>, IOrganizationNsdRepo
    {
        public OrganizationNsdRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public async Task<bool> AddNsdToOrganization(int nsdId, int orgId)
        {
            var ctx = AppDbContext;

            var nsd = ctx.NsdCategories.FirstOrDefaultAsync(nsdCat => nsdCat.Id == nsdId);
            if (nsd == null)
                throw new Exception("NSD not available or does not exist");

            var organization = ctx.Organizations.FirstOrDefaultAsync(org => org.Id == orgId);
            if (organization == null)
                throw new Exception("Organization not available or does not exist");

            var orgNsd = new OrganizationNsdCategory
            {
                Organization = await organization,
                NsdCategory = await nsd,
                OrganizationId = organization.Id,
                NsdCategoryId = nsd.Id
            };

            ctx.OrganizationNsds.Add(orgNsd);
            await ctx.SaveChangesAsync();

            return true;
        }

        public async Task<bool> RemoveNsdInOrganizationByNsdId(int nsdId, int orgId)
        {
            var ctx = AppDbContext;

            var org = await ctx.Organizations.FirstOrDefaultAsync(x => x.Id == orgId);
            if (org == null)
                throw new Exception("Organization not available or does not exist");

            var orgNsd = await DbSet.Where(x => x.OrganizationId == orgId && x.NsdCategoryId == nsdId).FirstOrDefaultAsync();
            if (orgNsd != null)
            {
                ctx.OrganizationNsds.Remove(orgNsd);
                await ctx.SaveChangesAsync();

                return true;
            }

            return false;
        }

        public Task<bool> OrganizationContainsNsd(int orgId, int catId)
        {
            return DbSet.AnyAsync(e => e.OrganizationId == orgId && e.NsdCategoryId == catId);
        }

        public async Task UpdateOrganizationNsds(int orgId, List<int> nsdIds)
        {
            var newIds = new HashSet<int>(nsdIds);

            var current = await DbSet.Where(e => e.OrganizationId == orgId).ToListAsync();
            var currentIds = new HashSet<int>(current.Select(e => e.NsdCategoryId));

            var toAdd = newIds.Where(id => !currentIds.Contains(id))
                              .Select(id => new OrganizationNsdCategory { OrganizationId = orgId, NsdCategoryId = id });

            foreach (var od in toAdd)
                Add(od);

            var toDelete = current.Where(e => !newIds.Contains(e.NsdCategoryId));

            foreach (var od in toDelete)
                Delete(od);
        }

        public async Task<bool> RemoveNsdFromAllOrganizationsByNsdId(int nsdId)
        {
            var ctx = AppDbContext;
            var orgsWithNsd = await ctx.OrganizationNsds.Where(y => y.NsdCategoryId == nsdId).ToListAsync();

            foreach (var orgNsd in orgsWithNsd)
                ctx.OrganizationNsds.Remove(orgNsd);

            return true;
        }
    }
}
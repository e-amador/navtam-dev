﻿using RSC.Model.Enums;
using System.Collections.Generic;

namespace RSC.Model
{
    public class RSCResponse
    {
        public RSCResponse()
        {
            Messages = new List<RSCResponseMessage>();
        }
        public int Version { get; set; }
        public ResponseStatus Status { get; set; }
        public string Mode { get; set; }
        public List<RSCResponseMessage> Messages { get; set; }
        public RSCText RSC { get; set; }
    }
}

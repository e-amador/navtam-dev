﻿app = app || {};

app.leafletmap = new function () {


    var map;

    var marker;
    var iconType;
    var zoomLevel = 6;

    var doaLayer;
    var nsdLayerGroup;

    var dynamicFeatureGroupMapLayer;
    var dynamicMarkerLayer;
    var dynamicLocalizationLayer;

    var locationStyling;
    var doaStyling;
    var markerStyling;
    var iconStyle;

    var mapConfigurationSettings;

    var invalidateSize;

    var getNestedObject = function getNestedObject(nestedObj, pathArr) {
        return pathArr.reduce(function (obj, key) {
            return obj && obj[key] !== 'undefined' ? obj[key] : undefined;
        }, nestedObj);
    };

    // 0: uninitialized
    // 1: initialized
    // 2: disconnected (internet connection error) // error

    var status = 0;
    var initMap = function (mapDivName, mapConfigOptions) {
        try {
            /*
            Commented because we need to show an error at the compiler time, to let the programmer know that the map
            have to be initialized at the time that the map div is visible and present on the HTML
            if (!document.getElementById('map')) {
                throw new Error();
            }
            */
            var mapEndpoints = mapConfigOptions || {
                darkMatter: window.app.mapServerHost + "/styles/dark-matter/{z}/{x}/{y}.png",
                klokantechBasic: window.app.mapServerHost + "/styles/klokantech-basic/{z}/{x}/{y}.png",
                osmBright: window.app.mapServerHost + "/styles/osm-bright/{z}/{x}/{y}.png",
                mapAttribution: "NOT FOR OPERATIONAL USE | PAS POUR UTILISATION OPÉRATIONNELLE",
                mapHealthApi: window.app.mapServerHost + "/health"
                //darkMatter: "http://maps.navone.ca/styles/dark-matter/{z}/{x}/{y}.png",
                //klokantechBasic: "http://maps.navone.ca/styles/klokantech-basic/{z}/{x}/{y}.png",
                //osmBright: "http://maps.navone.ca/styles/osm-bright/{z}/{x}/{y}.png",
                //mapAttribution: "NOT FOR OPERATIONAL USE | PAS POUR UTILISATION OPÉRATIONNELLE",
                //mapHealthApi: "http://maps.navone.ca/health"
            };

            var dark = L.tileLayer(mapEndpoints.darkMatter, { attribution: mapEndpoints.mapAttribution }),
                light = L.tileLayer(mapEndpoints.klokantechBasic, { attribution: mapEndpoints.mapAttribution }),
                bright = L.tileLayer(mapEndpoints.osmBright, { attribution: mapEndpoints.mapAttribution });

            var mapStyles = {
                "Dark": dark,
                "Basic": light,
                "Bright": bright
            };

            generateMapStyling();

            mapConfigurationSettings = {
                center: [45.385044, -76.486671],
                zoom: 3,
                layers: [dark, light, bright]
            };

            if (mapDivName === undefined || mapDivName === null || mapDivName === '') mapDivName = 'map';
            map = new L.map(mapDivName, mapConfigurationSettings);

            L.control.layers(mapStyles).addTo(map);

            dynamicFeatureGroupMapLayer = new L.featureGroup();
            dynamicMarkerLayer = new L.featureGroup();
            dynamicFeatureGroupMapLayer.addTo(map);
            dynamicMarkerLayer.addTo(map);

            dynamicFeatureGroupMapLayer.on("layeradd", function (layer) {
                var cord = getNestedObject(layer, ['layer', '_latlng']);

                if (typeof cord !== 'undefined') {
                    map.setView({ lat: cord.lat, lng: cord.lng }, 13);
                }
            });

            dynamicMarkerLayer.on("layeradd", function (layer) {
                var cord = getNestedObject(layer, ['layer', '_latlng']);

                if (typeof cord !== 'undefined') {
                    map.setView({ lat: cord.lat, lng: cord.lng }, 13);
                }
            });

            //Circle layer
            dynamicLocalizationLayer = new L.featureGroup();
            dynamicLocalizationLayer.addTo(map);
            dynamicLocalizationLayer.on("layeradd", function (layer) {
                var cord = getNestedObject(layer, ['layer', '_latlng']);

                if (typeof cord !== 'undefined') {
                    map.setView({ lat: cord.lat, lng: cord.lng }, 13);
                }
            });


            //map.setView(new L.latLng("52.9636976", "-104.4676491"));

            status = 1;

            // leave commented as there is an nsd that is throwing an error on this
            //if (callback) {
            //    callback();
            //}

        }
        catch (e) {
            status = 2;
        }
    };

    var isReady = function () {
        return status === 1;
    };

    var dispose = function () {
        map = null;
        status = 0;
    };

    var addMarker = function (geojson, featureName, onCompleted) {
        if (status === 1) {
            dynamicMarkerLayer.clearLayers();
            typeof marker !== 'undefined' ? marker.remove() : marker = new L.Marker();

            var cord = returnCoordinates(geojson);
            var latlng = new L.latLng(cord[1], cord[0]);

            marker.setLatLng(latlng);
            dynamicMarkerLayer.addLayer(marker);

            if (onCompleted) onCompleted();
        }
    };

    var addFeature = function (geoLocation, featureName, onCompleted) {
        if (status === 1) {
            var layer = getLayerOnMap('LOC');
            if (!layer) {
                if (!isgeoJsonParsed(geoLocation)) {
                    geoLocation = JSON.parse(geoLocation);
                }
                dynamicMarkerLayer.clearLayers();
                addGeoJsonToMap(geoLocation, featureName, locationStyling, 'LOC', "GeometryCollection");
            }

            if (onCompleted) onCompleted();
        }
    };

    var addDoa = function (geoLocation, featureName, onCompleted) {
        if (status === 1) {
            if (geoLocation) {
                removeDoaIfExists(featureName);
                var geojson = JSON.parse(geoLocation);

                if (geojson.features.length > 0) {
                    doaLayer = new L.geoJSON(geojson, { style: doaStyling });
                    doaLayer.addTo(map);

                    map.fitBounds(doaLayer.getBounds());
                }
            }

            if (onCompleted) onCompleted();
        }
    };

    var addLocationAndRadius = function (lat, long, rad) {
        if (status === 1) {

            dynamicLocalizationLayer.clearLayers();
            var marker = new L.Marker();
            marker.setLatLng([lat, long]);
            var circle = L.circle([lat, long], { radius: rad });
            dynamicLocalizationLayer.addLayer(marker);
            dynamicLocalizationLayer.addLayer(circle);

        }
    };

    var addPointToPointLine = function (lat_ini, long_ini, lat_end, long_end) {
        if (status === 1) {
            dynamicLocalizationLayer.clearLayers();
            var marker_ini = new L.Marker();
            marker_ini.setLatLng([lat_ini, long_ini]);
            var marker_end = new L.Marker();
            marker_end.setLatLng([lat_end, long_end]);

            var lineCoord = [[lat_ini, long_ini], [lat_end, long_end]];
            var line = L.polyline(lineCoord, { color: 'blue' });

            dynamicLocalizationLayer.addLayer(marker_ini);
            dynamicLocalizationLayer.addLayer(marker_end);
            dynamicLocalizationLayer.addLayer(line);
        }
    };

    var resetDoaMapView = function () {
        if (status === 1) {
            if (doaLayer)
                map.removeLayer(doaLayer);
            map.setView(new L.latLng("52.9636976", "-104.4676491"), 3);
        }
    };

    function nmToMeters(nm) {
        return nm / 0.000539957;
    }

    function feetToMeters(ft) {
        return ft / 3.28084;
    }

    function removeDoaIfExists(featureName) {
        if (doaLayer) {
            map.removeLayer(doaLayer);
            doaLayer = {};
        }
    }

    function returnCoordinates(data) {
        switch (returnGeoJsonType(data)) {
            case "FeatureCollection":
                return getNestedObject(data, ['features', 0, 'geometry', 'coordinates']);
            //break;

            case "Feature":
                return getNestedObject(data, ['geometry', 'coordinates']);
            //break;

            default:
                return {};
            //break;
        }
    }

    function returnGeoJsonType(data) {
        return getNestedObject(data, ['type']);
    }

    function isgeoJsonParsed(data) {
        var type = getNestedObject(data, ['type']);

        if (type === "FeatureCollection" || type === "Feature") {
            return true;
        }

        return false;
    }

    function addGeoJsonToMap(geoJson, featureName, options, icon, featureType) {
        var layer = L.geoJson(geoJson, { style: options });

        if (layerExists(layer))
            return;

        dynamicFeatureGroupMapLayer.addLayer(layer);
    }

    function layerExists(layer) {
        return dynamicFeatureGroupMapLayer.hasLayer(layer);
    }

    function getLayerOnMap(layerId) {
        map.eachLayer(function (layer) {
            if (layer.hasOwnProperty("feature") && layer.feature.properties.layerId === layerId) {
                return layer;
            }
        });

        return null;
    }

    function generateMapStyling() {
        doaStyling = {
            "color": "#1e0c96",
            "weight": 1,
            "opacity": 0.50
        };

        markerStyling = {
            "color": "#1e0c96",
            "weight": 1,
            "opacity": 1
        };

        locationStyling = {
            "color": "#960b0b",
            "weight": 1,
            "opacity": 1
        };
    }

    var disableMapPanning = function () {
        this.map.scrollWheelZoom.disable();
        this.map.dragging.disable();
        this.map.touchZoom.disable();
        this.map.doubleClickZoom.disable();
        this.map.boxZoom.disable();
        this.map.keyboard.disable();

        if (this.map.tap) {
            this.map.tap.disable();
        }
    };

    var setNewMarkerLocation = function (geojson, onCompleted) {
        if (status === 1) {
            this.addMarker(geojson, "");

            if (onCompleted) onCompleted();
        }
    };

    //ignoring excludingLayerIds until a better solution is found
    // we only use this for the doa which is already accomplished above by having it's own layer
    var clearFeaturesOnLayers = function (excludingLayerIds) {
        if (status === 1) {
            dynamicFeatureGroupMapLayer.clearLayers();
            dynamicMarkerLayer.clearLayers();
        }
    };

    var clearSelectedFeature = function (featureId) {
        if (status === 1) {
            featureId = featureId || [];
            map.eachLayer(function (layerIter) {
                var layer = getNestedObject(layerIter, ['feature', 'name']);
                if (layer === featureId) {
                    map.removeLayer(layer);
                    return;
                }
            });
        }
    };

    var textCordinatesToGeojson = function (textCoordinates) {
        function convert(str) {
            try {
                const degrees = parseInt(str.length === 7 ? str.substring(0, 2) : str.substring(0, 3));
                const minutes = parseInt(str.length === 7 ? str.substring(2, 4) : str.substring(3, 5));
                const seconds = parseInt(str.length === 7 ? str.substring(4, 6) : str.substring(5, 7));
                const sufx = str.length === 7 ? str.substring(6, 7) : str.substring(7, 8);

                var sing = sufx === 'W' || sufx === 'S' ? -1 : 1;
                return sing * (degrees + (minutes * 60.0 + seconds) / 3600.0);
            }
            catch (e) {
                return -1;
            }
        }

        const parts = (textCoordinates || "").split(' ');
        if (parts.length !== 2)
            return null;

        if (parts[0].length > 8 || parts[0].length < 7) {
            return null;
        }
        if (parts[1].length > 8 || parts[1].length < 7) {
            return null;
        }

        var lng = -1;
        var lat = convert(parts[0]);
        if (lat !== -1) {
            lng = convert(parts[1]);
        }

        if (lat === -1 || lng === -1) {
            return null;
        }

        return latitudeLongitudeToGeoJson(lat, lng);
    };

    var geojsonToTextCordinates = function (geojson) {
        function pad(str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }

        function convert(value, type) {
            try {
                const sign = value < 0;

                var abs = Math.abs(value);
                var deg = Math.trunc(abs);
                var min = Math.trunc((abs - deg) * 60.0);
                var sec = Math.round((abs - deg - min / 60.0) * 3600.0);
                if (sec > 59) {
                    sec = 0;
                    min = min + 1;
                    if (min > 59) {
                        min = 0;
                        deg = deg + 1;
                    }
                }

                if (type === 'lat') {
                    return pad(deg, 2) + pad(min, 2) + pad(sec, 2) + (sign ? 'S' : 'N');
                }

                return pad(deg, 3) + pad(min, 2) + pad(sec, 2) + (sign ? 'W' : 'E');
            }
            catch (e) {
                return "";
            }
        }

        const coordinates = geojson.features[0].geometry.coordinates;

        const lat = convert(coordinates[1], 'lat');
        const lng = convert(coordinates[0], 'lng');

        return lat + ' ' + lng;
    };

    var latitudeLongitudeToGeoJson = function (lat, lng) {
        return {
            "type": "FeatureCollection",
            "features": [
                {
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                        "type": "Point",
                        "bbox": [lat, lng, lat, lng],
                        "coordinates": [lng, lat]
                    }
                }
            ]
        };
    };

    var validateSize = function () {
        if (map) {
            map.invalidateSize();
        }
    };

    return {
        initMap: initMap,
        isReady: isReady,
        dispose: dispose,
        resizeMapForModal: validateSize,
        disableMapPan: disableMapPanning,

        addDoa: addDoa,
        addMarker: addMarker,
        addFeature: addFeature,
        resetDoaMapView: resetDoaMapView,

        clearFeaturesOnLayers: clearFeaturesOnLayers,
        setNewMarkerLocation: setNewMarkerLocation,

        textCordinatesToGeojson: textCordinatesToGeojson,
        geojsonToTextCordinates: geojsonToTextCordinates,
        latitudeLongitudeToGeoJson: latitudeLongitudeToGeoJson,
        addLocationAndRadius: addLocationAndRadius,
        addPointToPointLine: addPointToPointLine
    };
};

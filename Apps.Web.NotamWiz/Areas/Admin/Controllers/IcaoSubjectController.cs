﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using log4net;
using NavCanada.Applications.NotamWiz.Web.Code.Extentions;
using NavCanada.Applications.NotamWiz.Web.Controllers;
using NavCanada.Applications.NotamWiz.Web.Models;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;

namespace NavCanada.Applications.NotamWiz.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class IcaoSubjectController : BaseController
    {
        readonly ILog _log = LogManager.GetLogger(typeof(IcaoSubjectController));

        public IcaoSubjectController(IUow uow, IClusterQueues clusterQueues) 
            : base(uow, clusterQueues)
        {
        }

        // GET: Admin/ICAO_Subject
        public ActionResult Index()
        {
            _log.Object(new { Method = "Index", Parameters = new { } });
            return View();
        }

        public ActionResult Subjects_Read([DataSourceRequest]DataSourceRequest request)
        {
            _log.Object(new { Method = "Subjects_Read", Parameters = new {request } });

            var subjects = Uow.IcaoSubjectRepo.GetAll().Select(s=> new{
                s.AllowItemAChange,
                s.AllowScopeChange,
                s.AllowSeriesChange,
                s.Code,
                s.EntityCode,
                s.Id, 
                s.Name,
                s.NameFrench, 
                s.Scope 
            });
            DataSourceResult result = subjects.ToDataSourceResult(request);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Subjects_Create([DataSourceRequest]DataSourceRequest request, IcaoSubjectViewModel subject)
        {

            _log.Object(new { Method = "Subjects_Create", Parameters = new {request, subject } });

            // Will keep the inserted entitites here. Used to return the result later.
            var icaoSubjects = new List<IcaoSubject>();
            if (ModelState.IsValid)
            {
                var icaoSubject = Mapper.Map<IcaoSubjectViewModel, IcaoSubject>(subject);
                icaoSubjects.Add(icaoSubject);
                Uow.IcaoSubjectRepo.Add(icaoSubject);
                await Uow.SaveChangesAsync();
            }
            // Return the inserted entities. The grid needs the generated ProductID. Also return any validation errors.
            return Json(icaoSubjects.ToDataSourceResult(request, ModelState, Mapper.Map<IcaoSubject, IcaoSubjectViewModel>));
        }

        public async Task<ActionResult> Subjects_Update([DataSourceRequest]DataSourceRequest request, IcaoSubjectViewModel subject)
        {
            _log.Object(new { Method = "Subjects_Update", Parameters = new {request, subject } });

            // Will keep the updated entitites here. Used to return the result later.
            var icaoSubjects = new List<IcaoSubject>();
            if (ModelState.IsValid)
            {
                var updatetedSubject = Uow.IcaoSubjectRepo.GetById(subject.Id);

                updatetedSubject = Mapper.Map(subject, updatetedSubject);
                icaoSubjects.Add(updatetedSubject);
                Uow.IcaoSubjectRepo.Update(updatetedSubject);
                await Uow.SaveChangesAsync();
            }
            // Return the updated entities. Also return any validation errors.
            return Json(icaoSubjects.ToDataSourceResult(request, ModelState, Mapper.Map<IcaoSubject, IcaoSubjectViewModel>));
        }


        public async Task<ActionResult> Subject_Destroy([DataSourceRequest]DataSourceRequest request, IcaoSubjectViewModel subject)
        {
            _log.Object(new { Method = "Subject_Destroy", Parameters = new {request, subject } });

            // Will keep the destroyed entitites here. Used to return the result later.
            var entities = new List<IcaoSubject>();
            if (ModelState.IsValid)
            {
                var icaoSubConditions = await Uow.IcaoSubjectConditionRepo.GetBySubjectIdAsync(subject.Id);
                foreach (var icaoSubCondition in icaoSubConditions)
                    Uow.IcaoSubjectConditionRepo.Delete(icaoSubCondition);

                var entity = Mapper.Map<IcaoSubjectViewModel, IcaoSubject>(subject);
                entities.Add(entity);

                Uow.IcaoSubjectRepo.Delete(entity);
                await Uow.SaveChangesAsync();
            }
            // Return the destroyed entities. Also return any validation errors.
            return Json(entities.ToDataSourceResult(request, ModelState, Mapper.Map<IcaoSubject, IcaoSubjectViewModel>));
        }


        public ActionResult Conditions_Read(int parentId, [DataSourceRequest] DataSourceRequest request)
        {
            _log.Object(new { Method = "Conditions_Read", Parameters = new {parentId, request } });

            if (parentId == 0)
                return null;

            var condList = Mapper.Map<List<IcaoSubjectCondition>,List<IcaoSubjectConditionViewModel>>(Uow.IcaoSubjectConditionRepo.GetAll().Where(e => e.Subject.Id == parentId).ToList());
            return Json(condList, JsonRequestBehavior.AllowGet);

        }
        public async Task<ActionResult> Conditions_Create(int parentId, [DataSourceRequest] DataSourceRequest request, IcaoSubjectConditionViewModel cond)
        {

            _log.Object(new { Method = "Conditions_Create", Parameters = new {parentId, request, cond } });

            // Will keep the inserted entitites here. Used to return the result later.
            var entities = new List<IcaoSubjectCondition>();
            if (ModelState.IsValid)
            {
                var entity = Mapper.Map<IcaoSubjectConditionViewModel, IcaoSubjectCondition>(cond);

                var icaoSubject = await Uow.IcaoSubjectRepo.GetByIdAsync(parentId);
                if (icaoSubject != null)
                {
                    icaoSubject.Conditions.Add(entity);
                    entities.Add(entity);

                    await Uow.SaveChangesAsync();
                }
            }
            // Return the inserted entities. The grid needs the generated ProductID. Also return any validation errors.
            return Json(entities.ToDataSourceResult(request, ModelState, Mapper.Map<IcaoSubjectCondition, IcaoSubjectConditionViewModel>));
        }
        public async Task<JsonResult> Conditions_Update([DataSourceRequest] DataSourceRequest request, IcaoSubjectConditionViewModel cond)
        {
            _log.Object(new { Method = "Conditions_Update", Parameters = new {request, cond } });

            // Will keep the updated entitites here. Used to return the result later.
            var entities = new List<IcaoSubjectCondition>();
            if (ModelState.IsValid)
            {
                var updatedCond = Uow.IcaoSubjectConditionRepo.GetById(cond.Id);

                updatedCond = Mapper.Map(cond, updatedCond);
                entities.Add(updatedCond);

                Uow.IcaoSubjectConditionRepo.Update(updatedCond);
                await Uow.SaveChangesAsync();
            }
            // Return the updated entities. Also return any validation errors.
            return Json(entities.ToDataSourceResult(request, ModelState));
        }
        public async Task<ActionResult> Conditions_Destroy([DataSourceRequest] DataSourceRequest request, IcaoSubjectConditionViewModel cond)
        {
            _log.Object(new { Method = "Conditions_Destroy", Parameters = new {request, cond } });

            // Will keep the destroyed entitites here. Used to return the result later.
            var entities = new List<IcaoSubjectCondition>();
            if (ModelState.IsValid)
            {
                var deletedCond = Uow.IcaoSubjectConditionRepo.GetById(cond.Id);                
                entities.Add(deletedCond);
                Uow.IcaoSubjectConditionRepo.Delete(deletedCond);
                await Uow.SaveChangesAsync();
            }
            // Return the destroyed entities. Also return any validation errors.
            return Json(entities.ToDataSourceResult(request, ModelState, Mapper.Map<IcaoSubjectCondition, IcaoSubjectConditionViewModel>));
        }
        protected override void Dispose(bool disposing)
        {
            _log.Object(new { Method = "Dispose", Parameters = new {disposing } });
            base.Dispose(disposing);
        }
    }
}
"use strict";
/**
 * numberFixedLen.pipe
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NumberFixedLenPipe = void 0;
var core_1 = require("@angular/core");
var NumberFixedLenPipe = /** @class */ (function () {
    function NumberFixedLenPipe() {
    }
    NumberFixedLenPipe.prototype.transform = function (num, len) {
        num = Math.floor(num);
        len = Math.floor(len);
        if (isNaN(num) || isNaN(len)) {
            return num.toString();
        }
        var numString = num.toString();
        while (numString.length < len) {
            numString = '0' + numString;
        }
        return numString;
    };
    NumberFixedLenPipe = __decorate([
        core_1.Pipe({
            name: 'numberFixedLen'
        })
    ], NumberFixedLenPipe);
    return NumberFixedLenPipe;
}());
exports.NumberFixedLenPipe = NumberFixedLenPipe;
//# sourceMappingURL=numberFixedLen.pipe.js.map
﻿namespace NavCanada.Core.TaskEngine.Implementations
{
    public class TaskEngineDispatcherSettings
    {
        public int MaxPoisonMtoRetryCount { get; set; }
        public int SoonToExpireHrsDifference { get; set; }
        public bool EnabledPerfCounter { get; set; }
        public string InstanceName { get; set; }
    }
}
﻿using Microsoft.Practices.Unity;
using NavCanada.Core.Data.NotamWiz.Contracts;
using RSC.Business.SDO;
using RSC.Model;
using RSC.Model.Enums;
using RSC.WebService.Helpers;
using RSC.WebService.Schemas;
using System;
using System.Configuration;
using System.Diagnostics;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;

#pragma warning disable CS0618

namespace RSC.WebService
{
    /// <summary>
    /// Summary description for RSCService
    /// </summary>
    [WebService(Namespace = "http://www.navcanada.ca/nes-rsc/webservice")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class RSCService : BaseService<RSCService>
    {
        static RSCService()
        {
            _serviceEnabled = "true".Equals(ConfigurationManager.AppSettings["RSCServiceEnabled"] ?? "false");
            _dummyMode = ConfigurationManager.AppSettings["RSC_DUMMY_MODE"] == "1";
        }

        #region Injected Dependencies

        [Dependency]
        public IUow Uow { get; set; }

        #endregion

        [SoapHeader("Authentication", Required = true)]
        [WebMethod]
        public RSCResponseType reportRSC(RSCRequestType request)
        {
            try
            {
                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);  // HTTP 1.1.
                Context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
                Context.Response.AppendHeader("Pragma", "no-cache"); // HTTP 1.0.
                Context.Response.AppendHeader("Expires", "0"); // Proxies.

                // verify service is enabled
                if (!_serviceEnabled)
                {
                    Context.Response.StatusCode = 405;
                    return CreateError(20300, $"RSC Webservice processing issue: Service not available.");
                }

                // validate authentication
                if (!ValidateAuthentication())
                {
                    Context.Response.StatusCode = 401;
                    return CreateError(11201, "Username or password is wrong.");
                }

                // validate proposal vs schema
                var proposalXml = XMLSerializer.Serialize(request.RSCProposal);
                var validationMessage = XMLValidations.ValidateProposal(proposalXml);
                if (!string.IsNullOrEmpty(validationMessage))
                {
                    //Context.Response.StatusCode = 400;
                    return CreateError(20000, $"RSC Webservice schema validation issue: {validationMessage}");
                }

                // >> todo: validate user, user's role, aerodrome and RSC permissions

                // convert to RSC model
                var proposalModel = request.RSCProposal.ToModel();

                // create the RSC processing engine
                var rscEngine = new Business.RSCEngine(new SDOCacheRepository(Uow), _dummyMode);

                // process request
                var response = rscEngine.Process(proposalModel, request.RetrieveRSC);

                // >> todo: disseminate RSC here...

                return response.ToSchema();
            }
            catch (RSCException ex)
            {
                //Context.Response.StatusCode = 500;
                return CreateError(ex.ErrorCode, ex.Message);
            }
            catch (Exception ex)
            {
                //Context.Response.StatusCode = 500;
                return CreateError(20300, $"RSC Webservice processing issue: {ex.Message}");
            }
        }

        [SoapHeader("Authentication", Required = true)]
        [WebMethod]
        public AvailabilityResponseType checkAvailability()
        {
            Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);  // HTTP 1.1.
            Context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            Context.Response.AppendHeader("Pragma", "no-cache"); // HTTP 1.0.
            Context.Response.AppendHeader("Expires", "0"); // Proxies.

            // verify service is enabled
            if (!_serviceEnabled)
            {
                Context.Response.StatusCode = 405;
                return new AvailabilityResponseType()
                {
                    Status = AvailabilityResponseTypeStatus.Item2 // error
                };
            }

            if (ValidateAuthentication())
            {
                return new AvailabilityResponseType()
                {
                    Status = AvailabilityResponseTypeStatus.Item0, // OK
                    Message = new AvailabilityResponseTypeMessage[]
                    {
                        new AvailabilityResponseTypeMessage
                        {
                            MsgNumber = "10000",
                            Description = "System is up and running."
                        }
                    }
                };
            }
            else
            {
                Context.Response.StatusCode = 401;
                return new AvailabilityResponseType()
                {
                    Status = AvailabilityResponseTypeStatus.Item2 // error
                };
            }
        }

        /// <summary>
        /// Soap header will be injected automatically here.
        /// </summary>
        public AUTHHEADER Authentication;

        private bool ValidateAuthentication()
        {
            Debug.WriteLine($"User: {Authentication?.USERNAME}");
            Debug.WriteLine($"Password: {Authentication?.PASSWORD}");
            return "user".Equals(Authentication?.USERNAME) && "password".Equals(Authentication?.PASSWORD);
        }

        static RSCResponseType CreateError(int errorCode, string errorMessage)
        {
            var rscResponse = new RSCResponse() { Status = ResponseStatus.Error };
            rscResponse.Messages.Add(new RSCResponseMessage()
            {
                Status = ResponseStatus.Error,
                Number = errorCode,
                Description = errorMessage
            });
            return rscResponse.ToSchema();
        }

        /// <summary>
        /// Internal class to handle the soap authentication headers.
        /// </summary>
        /// <remarks>DO NOT rename this class or SOAP headers wont get serialized!</remarks>
        public class AUTHHEADER : SoapHeader
        {
            public string USERNAME;
            public string PASSWORD;
        }

        static bool _serviceEnabled;
        static bool _dummyMode;
    }
}
﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    using System.ComponentModel.DataAnnotations;

    public class IcaoSubjectConditionViewModel
    {
        public long Id { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public string DescriptionFrench { get; set; }

        public long SubjectId { get; set; }

        [Required]
        [StringLength(2)]
        public string Code { get; set; }

        [RegularExpression(@"^(IV|V|I)$", ErrorMessage = "Invalid Traffic")]
        public string Traffic
        {
            get;
            set;
        }

        [RegularExpression(@"^(NBO|NBM|NB|NO|NM|BOM|BO|BM|OM|N|B|O|M)$", ErrorMessage = "Invalid Traffic")]
        public string Purpose
        {
            get;
            set;
        }
        

        [Required]
        [Range(0,999)]
        public int Lower { get; set; }
        [Required]
        [Range(0, 999)]
        public int Upper { get; set; }
        [Required]
        [Range(0, 99)]
        public int Radius { get; set; }

        public bool CancelNotamOnly { get; set; }
        public bool Active { get; set; }
        public bool AllowFgEntry { get; set; }
        public bool AllowPurposeChange { get; set; }
    }
}
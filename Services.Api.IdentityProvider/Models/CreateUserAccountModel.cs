﻿namespace NavCanada.Services.Api.IdentityProvider.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public class CreateUserAccountModel
    {
        [Required(ErrorMessage = "Field can't be empty")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Field can't be empty")]
        public string Firstname { get; set; }

        [Required(ErrorMessage = "Field can't be empty")]
        public string Lastname { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        
        [Required(ErrorMessage = "Email is required!")]
        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail is not valid")]
        public string Email { get; set; }

        public string Role { get; set; }
        public List<SelectListItem> Roles { get; set; }

        public CreateUserAccountModel()
        {
            Roles = new List<SelectListItem>
            {
                new SelectListItem {Text = "Normal User", Value = "FreeUser"},
                new SelectListItem {Text = "Administrator", Value = "Admin"},
            };
        }

    }
}
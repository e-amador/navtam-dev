﻿using NDS.DataBus.WebClient.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace NDS.DataBus.WebClient.Subscriptions
{
    public class SubscriptionManager : IDisposable
    {

        static SubscriptionManager()
        {
            Instance = new SubscriptionManager(ConfigurationManager.AppSettings["DataBusConnectionString"]);
        }

        public static SubscriptionManager Instance { get; private set; }

        private SubscriptionManager(string connectionstring)
        {
            _dataBusConnectionstring = connectionstring;
            _subscriptions = new Dictionary<string, ClientSubscription>();
        }

        public IEnumerable<NotamViewModel> DrainTopic(string id)
        {
            lock (_subscriptions)
            {
                return _subscriptions.ContainsKey(id) ? _subscriptions[id].Drain() : null;
            }
        }

        public string CreateSubscription(string userName, string password, string topic, string subsName, string filters)
        {
            ClientSubscription subscription = null;

            lock (_subscriptions)
            {
                subscription = FindSubscription(topic, subsName);
                if (subscription == null)
                {
                    subscription = new ClientSubscription(topic, subsName, filters);
                    _subscriptions.Add(subscription.Id, subscription);
                    subscription.StartListening(BuildConnectionString(userName, password));
                }
            }

            return subscription.Id;
        }

        private ClientSubscription FindSubscription(string topic, string subsName)
        {
            return _subscriptions.Values.FirstOrDefault(s => topic.Equals(s.Topic, StringComparison.InvariantCulture) &&
                subsName.Equals(s.SubscriptionName, StringComparison.InvariantCulture));
        }

        public void StopListening(string id)
        {
            lock (_subscriptions)
            {
                if (_subscriptions.ContainsKey(id))
                {
                    var subscription = _subscriptions[id];
                    try
                    {
                        subscription.StopListening();
                    }
                    finally
                    {
                        _subscriptions.Remove(subscription.Id);
                    }
                }
            }
        }

        private string BuildConnectionString(string userName, string password)
        {
            return _dataBusConnectionstring.Replace("[%USER%]", userName).Replace("[%PASSWORD%]", password);
        }

        readonly string _dataBusConnectionstring;
        readonly Dictionary<string, ClientSubscription> _subscriptions;

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    foreach (var subs in _subscriptions.Values)
                    {
                        subs.StopListening();
                    }
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~SubscriptionManager() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
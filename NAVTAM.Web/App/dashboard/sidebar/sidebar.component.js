"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SidebarComponent = void 0;
var core_1 = require("@angular/core");
var data_service_1 = require("../shared/data.service");
var notify_service_1 = require("../shared/notify.service");
var SidebarComponent = /** @class */ (function () {
    function SidebarComponent(dataService, notifyService) {
        var _this = this;
        this.dataService = dataService;
        this.notifyService = notifyService;
        this.notifyService.subscribeCategorySubjectEvent()
            .subscribe(function (categoryId) { return _this.onCategoryChanged(categoryId); });
    }
    SidebarComponent.prototype.ngOnInit = function () {
    };
    SidebarComponent.prototype.onCategoryChanged = function (categoryId) {
        var _this = this;
        if (this.selectedCategory && this.selectedCategory.id === categoryId) {
            return;
        }
        this.dataService.getNsdCategories()
            .subscribe(function (categories) {
            if (categories.length > 0) {
                _this.nsdCategories = categories;
                categories[0].isSelected = true;
                _this.getNotamsByCategory(categoryId);
            }
        });
    };
    SidebarComponent.prototype.getNotamsByCategory = function (categoryId) {
        if (this.selectedCategory) {
            this.selectedCategory.isSelected = false;
        }
        var category = this.findCategory(categoryId);
        if (category) {
            this.selectedCategory = category;
            this.selectedCategory.isSelected = true;
            this.notifyService.emitCategorySubjectEvent(categoryId);
        }
    };
    SidebarComponent.prototype.findCategory = function (categoryId) {
        var categories = this.nsdCategories.filter(function (item) { return item.id === categoryId; });
        if (categories.length === 0) {
            for (var i = 0; i < this.nsdCategories.length; i++) {
                var children = this.nsdCategories[i].children;
                if (children) {
                    categories = children.filter(function (item) { return item.id === categoryId; });
                    if (categories.length > 0) {
                        break;
                    }
                }
            }
        }
        return categories.length === 1 ? categories[0] : null;
    };
    SidebarComponent = __decorate([
        core_1.Component({
            selector: 'sidebar',
            templateUrl: '/app/dashboard/sidebar/sidebar.component.html'
        }),
        __metadata("design:paramtypes", [data_service_1.DataService, notify_service_1.NotifyService])
    ], SidebarComponent);
    return SidebarComponent;
}());
exports.SidebarComponent = SidebarComponent;
//# sourceMappingURL=sidebar.component.js.map
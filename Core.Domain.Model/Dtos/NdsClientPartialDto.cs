﻿using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using System;

namespace NavCanada.Core.Domain.Model.Dtos
{
    public class NdsClientPartialDto
    {
        public int Id { get; set; }
        public string Client { get; set; }
        public string Address { get; set; }
        public NdsClientType ClientType { get; set; }
        public string Series { get; set; }
        public string ItemsA { get; set; }
        public string Operator { get; set; }
        public DateTime Updated { get; set; }
        public bool French { get; set; }
        public bool AllowQueries { get; set; }
        public DateTime? LastSynced { get; set; }
        public bool Active { get; set; }
    }
}

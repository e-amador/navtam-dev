﻿using System;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Core.Proxies.AimslProxy.Common
{
    public class NotamProposalStatus
    {
        public string ProposalId { get; set; }

        public NotamProposalStatusCode Status { get; set; }

        public DateTime StatusDateTime { get; set; }

        public string RejectionReason { get; set; }

        public long InoNotamProposalId { get; set; }

        public Notam SubmittedNotam { get; set; }
    }
}

﻿using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.Areas.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    using AutoMapper;

    using Kendo.Mvc.Extensions;
    using Kendo.Mvc.UI;

    using Microsoft.AspNet.Identity;

    using Code.Helpers;
    using Web.Controllers;
    using Models;
    using Core.Common.Common;
    using Core.Common.Contracts;
    using Core.Data.NotamWiz.Contracts;
    using Core.Domain.Model.Entitities;
    using Core.Domain.Model.Enums;
    using Core.SqlServiceBroker;

    using Westwind.Globalization;

    [Authorize(Roles = "CCS")]
    public class NewRegistrationController : BaseController
    {
        private readonly ILog _logger = LogManager.GetLogger(typeof(NewRegistrationController));

        public NewRegistrationController(IUow uow, IClusterQueues clusterQueues) 
            : base(uow, clusterQueues)
        {
        }

        // GET: Admin/NewRegistration
        public async Task<ActionResult> Index()
        {
            var orgs = await Uow.OrganizationRepo.GetAllAsync();

            ViewData["Orgs"] = orgs.Select( e=> new OrganizationListViewModel
            {
                Name = e.Name,
                Id = e.Id
            })
            .ToList();

            await SetViewBagRolesForCssAndAdminAsync(User.Identity.GetUserId());

            return View();
        }

        //---------------- READ USERS ( POPULATE GRID )
        /// <summary>
        /// Populates table and reads it into the Grid.
        /// </summary>
        /// 
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> ReadNewRegistrationUsers([DataSourceRequest] DataSourceRequest request, 
            [Bind(Prefix = "models")] IEnumerable<UserRegistrationManagementViewModel> userRegistrationManagement)
        {
            var userProfiles = await Uow.UserProfileRepo.GetUnapprovedAsync();

            var userRegMngtViewModelLst = Mapper.Map<List<UserProfile>, List<UserRegistrationManagementViewModel>>(userProfiles);

            for (var iter = 0; iter < userProfiles.Count; iter++)
            {
                var userProfile = userProfiles[iter];
                var userRegMngtViewModel = userRegMngtViewModelLst[iter];

                if (userProfile.RegistrationId.HasValue)
                {
                    var registration = await Uow.RegistrationRepo.GetByIdAsync(userProfile.RegistrationId.Value);
                    userRegMngtViewModel.userVM = Mapper.Map<UserProfile, UserManagementViewModel>(userProfile);

                    if( registration != null )
                        userRegMngtViewModel.orgVM = Mapper.Map<Registration, OrganizationViewModel>(registration);
                }
            }

            return (Json(userRegMngtViewModelLst.ToDataSourceResult(request)));
        }


        public async Task<ActionResult> ShowDetails([DataSourceRequest]DataSourceRequest request, string userId)
        {
            var userProfile = await UserManager.FindByIdAsync(userId);

            return Json(Mapper.Map<UserProfile, UserManagementViewModel>(userProfile));
        }

        [HttpPost]
        public ActionResult StartRegProcess()
        {
            return PartialView("StartRegProcess");
        }

        [HttpPost]
        public async Task<ContentResult> CompleteRegistration(UserRegistrationManagementViewModel userRegistrationManagement)
        {
            // ModelState for password will always be false, can user cannot update user password 
            ModelState.Remove("userRegistrationManagement.userVM.Password");
            ModelState.Remove("userRegistrationManagement.userVM.ConfirmPassword");

            if (userRegistrationManagement == null || userRegistrationManagement.userVM == null || userRegistrationManagement.SelectedOrganizationName == null || !ModelState.IsValid)
                return ProcessResult.GetErrorResult("NRC-0002", DbRes.T("Unknown Error on edit User Organization", "NewRegistrationRes"));

            Uow.BeginTransaction();
            try
            {
                // Create new organization if needed  
                if (! await Uow.OrganizationRepo.ExistsByNameAsync(userRegistrationManagement.SelectedOrganizationName) )
                {
                    var myOrg = Mapper.Map<OrganizationViewModel, Organization>(userRegistrationManagement.orgVM);

                    if (userRegistrationManagement.DOAId != 0)
                    {
                        myOrg.AssignedDoa = await Uow.DoaRepo.GetByIdAsync(userRegistrationManagement.DOAId);
                    }
                    else if (!string.IsNullOrEmpty(userRegistrationManagement.AirdromeMid))
                    {
                        // Get AD id 
                        var airport =  await Uow.SdoSubjectRepo.GetByEntityNameAndMid("Ahp", userRegistrationManagement.AirdromeMid);

                        // link to shard AD DOA                        
                        myOrg.AssignedDoa = new Doa
                        {
                            Name = "DOA for " + airport.Name,
                            Description = "Linked to Shared AD DOA",
                            DoaGeoArea = airport.SubjectGeoRefPoint.Buffer(9260 /*5 Nautical Miles ~= 9260 meter */),
                            SharedDoaDefinitionId = 1,
                            SharedDoaParameters = "{ Ahp_codeId: \"" + airport.Designator + "\"}"
                        };
                    }

                    Uow.OrganizationRepo.Add(myOrg);
                    await Uow.SaveChangesAsync();

                    try
                    {
                        var userProfile = await UserManager.FindByIdAsync(userRegistrationManagement.userVM.UserId);
                        var updatedUser = Mapper.Map(userRegistrationManagement.userVM, userProfile);
                        // update Registration status
                        updatedUser.Registration = userProfile.Registration;
                        updatedUser.Registration.Status = (int)RegistrationStatus.Completed;
                        updatedUser.Registration.StatusTime = DateTime.Now;

                        // assign user to an organization
                        var org = await Uow.OrganizationRepo.GetByNameAsync(userRegistrationManagement.SelectedOrganizationName);
                        var registartionId = userProfile.RegistrationId.Value;
                        updatedUser.OrganizationId = org.Id;
                        updatedUser.RegistrationId = null;

                        // remove all current user roles then add OrgAdminRole
                        var roleIds = userProfile.Roles.Select(c => c.RoleId).ToList();
                        if (roleIds.Any())
                        {
                            var appRoles = await Uow.ApplicationRoleRepo.GetByRoleIdsAsync(roleIds);

                            foreach (var appRole in appRoles)
                            {
                                await UserManager.RemoveFromRoleAsync(updatedUser.Id, appRole.Name);

                            }
                        }

                        // Add mandatory roles 
                        UserManager.AddToRole(updatedUser.Id, CommonDefinitions.OrgAdminRole);
                        UserManager.AddToRole(updatedUser.Id, CommonDefinitions.UserRole);

                        // add all other selected roles
                        if (userRegistrationManagement.userVM.UserRoles != null)
                        {
                            var otherRolesToAdd = await Uow.ApplicationRoleRepo.GetDistintUserRoleAndOrgAdminRoleAsync();
                            foreach (string roleId in userRegistrationManagement.userVM.UserRoles)
                            {
                                var roletoadd = otherRolesToAdd.FirstOrDefault(role => role.Id == roleId);
                                if (roletoadd != null)
                                    await UserManager.AddToRoleAsync(updatedUser.Id, roletoadd.Name);
                            }
                        }

                        // approve user 
                        updatedUser.IsApproved = true;

                        // save changes 
                        UserManager.SetLockoutEnabled(updatedUser.Id, false);

                        await UserManager.UpdateAsync(updatedUser);

                        //Publish Mto to target Queeue
                        var mto = CreateMtoForUserRegistration(MessageTransferObjectId.RegistrationNotificationEmail, updatedUser.Id, registartionId, RegistrationStatus.Completed, null);
                        PublishMtoToTargetQueue(mto, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings);

                        Uow.Commit();
                    }
                    catch (Exception ex)
                    {
                        _logger.Error("CompleteRegistration: Somenthing went wrong and transaction was rollback. The Email to user was never sent", ex);
                        Uow.Rollback();
                        return ProcessResult.GetErrorResult("NRC-0004", DbRes.T("Failure to add user to Role or Organization", "NewRegistrationRes"));
                    }
                }
            }
            catch (Exception)
            {
                Uow.Rollback();
                return ProcessResult.GetErrorResult("NRC-0004", DbRes.T("Failure to create Organization", "NewRegistrationRes"));
            }

            return ProcessResult.GetSuccessResult(DbRes.T("Registration Completed Successfully", "NewRegistrationRes"));
        }

        [HttpPost]
        public async Task<ActionResult> AssignOrganization([DataSourceRequest]DataSourceRequest request, string userId, string orgName, OrganizationViewModel organizationVm)
        {
            if (userId == null || organizationVm == null || !ModelState.IsValid)
                return ProcessResult.GetErrorResult("NRC-0002", DbRes.T("UnknownErrorOnEditUserOrganization", "NewRegistrationRes"));

            try
            {
                UserProfile userProfile = await UserManager.FindByIdAsync(userId);
                if (!String.IsNullOrEmpty(orgName))
                {
                    var newOrg = await Uow.OrganizationRepo.GetByNameAsync(orgName);
                    userProfile.Organization = newOrg;
                    userProfile.OrganizationId = newOrg.Id;

                    await UserManager.UpdateAsync(userProfile);
                }
                else
                {
                    var myOrg = Mapper.Map<OrganizationViewModel, Organization>(organizationVm);


                    if (! await Uow.OrganizationRepo.ExistsByNameAsync(myOrg.Name))
                    {
                        Uow.OrganizationRepo.Add(myOrg);
                        await Uow.SaveChangesAsync();

                        userProfile.Organization = myOrg;
                        userProfile.OrganizationId = myOrg.Id;
                        await UserManager.UpdateAsync(userProfile);
                    }
                    else
                    {
                        var org = Mapper.Map<OrganizationViewModel, Organization>(organizationVm);

                        if (await Uow.OrganizationRepo.ExistsByIsAndNotReadOnlyAsync(org.Id))
                        {
                            Uow.OrganizationRepo.Update(org);
                            await Uow.SaveChangesAsync();
                        }
                    }
                }
                return ProcessResult.GetSuccessResult(DbRes.T("SuccessToAssignUserOrganizations", "NewRegistrationRes"));
            }
            catch (Exception)
            {
                return ProcessResult.GetErrorResult("NRC-0001", DbRes.T("FailureToAssignUserOrganizations", "NewRegistrationRes"));
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public async Task<JsonResult> GetRoles()
        {
            var appRoles = await Uow.ApplicationRoleRepo.GetWithUserRoleOrgAdminRoleAndCatchAllAsync();

            var response = appRoles.Select( e=> new 
            {
                value = e.Id,
                text = e.Name
            })
            .ToList();

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        //  --------- Reject USER
        /// <summary>
        /// Called when CCS rejects user.
        /// </summary>
        /// <param name="packageId"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ContentResult> RejectUser(string packageId)
        {
            try
            {
                if (packageId == null || !ModelState.IsValid)
                    return ProcessResult.GetErrorResult("NRC-0008", DbRes.T("UnknownError", "NewRegistrationRes"));

                var userProfile = await UserManager.FindByIdAsync(packageId);

                Uow.BeginTransaction();
                if (userProfile != null && userProfile.RegistrationId.HasValue)
                {
                    var userRegistrationsInfo = await Uow.RegistrationRepo.GetByIdAsync(userProfile.RegistrationId.Value);

                    userRegistrationsInfo.Status = (int)RegistrationStatus.Rejected;
                    userRegistrationsInfo.StatusTime = DateTime.Now;

                    await UserManager.DeleteAsync(userProfile);
                                        
                    //Publish Mto to target Queeue
                    var mto = CreateMtoForUserRegistration(MessageTransferObjectId.RegistrationNotificationEmail, null, userRegistrationsInfo.Id, RegistrationStatus.Rejected, null);
                    PublishMtoToTargetQueue(mto, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings);
                    
                    Uow.Commit();

                    return ProcessResult.GetSuccessResult(DbRes.T("IsRejectedAndDeleted", "NewRegistrationRes"));
                }
                return ProcessResult.GetErrorResult("NRC-0009", DbRes.T("FailureToRejectUser", "NewRegistrationRes"));
            }
            catch (Exception )
            {
                Uow.Rollback();
                return ProcessResult.GetErrorResult("NRC-0010", DbRes.T("FailureToRejectUser", "NewRegistrationRes"));
            }
        }

        //private void SendNotificationEmail(RegistrationStatus registrationStatus, UserProfile userProfile)
        //{

        //    // send notificationEmail
        //    NotificationServices.NotificationServices _NotificationService = new NotificationServices.NotificationServices();
        //    bool emailsent = false;
        //    string emailerrormessage = null;
        //    if (registrationStatus == RegistrationStatus.Rejected)
        //    {
        //        emailsent = _NotificationService.SendUserApplicationStatusNotification(out emailerrormessage, NotificationServices.Models.Enums.NotificationType.Email, userProfile.FirstName,
        //                                                                               userProfile.UserName, userProfile.Email, NotificationServices.Models.Enums.RegistrationStatus.Rejected);
        //    }
        //    else
        //    {
        //        emailsent = _NotificationService.SendUserApplicationStatusNotification(out emailerrormessage, NotificationServices.Models.Enums.NotificationType.Email, userProfile.FirstName,
        //                                                               userProfile.UserName, userProfile.Email, NotificationServices.Models.Enums.RegistrationStatus.Completed);
        //    }

        //}
    }
}
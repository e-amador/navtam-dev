﻿using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.TaskEngine.Mto;

namespace NAVTAM.Helpers
{
    public interface INotamQueuePublisher
    {
        void Publish(IUow uow, MessageTransferObject mto);
    }
}
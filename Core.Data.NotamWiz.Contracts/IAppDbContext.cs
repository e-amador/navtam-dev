﻿namespace NavCanada.Core.Data.NotamWiz.Contracts
{
    using System.Data.Entity;
    using Domain.Model.Entitities;

    public interface IAppDbContext
    {
        //IDbSet<BilingualRegion> BilingualRegions { get; set; }
        IDbSet<AerodromeDisseminationCategory> AerodromeDisseminationCategories { get; set; }
        IDbSet<ConfigurationValue> ConfigurationValues { get; set; }
        IDbSet<DigitalSla> DigitalSlas { get; set; }
        IDbSet<Doa> Doas { get; set; }
        IDbSet<GeoRegion> GeoRegions { get; set; }
        IDbSet<SeriesAllocation> SeriesAllocations { get; set; }
        IDbSet<Group> Groups { get; set; }
        IDbSet<IcaoSubject> IcaoSubjects { get; set; }
        IDbSet<IcaoSubjectCondition> IcaoSubjectConditions { get; set; }
        IDbSet<NotificationTemplate> NotificationTemplates { get; set; }
        IDbSet<NotificationTracking> NotificationTrackings { get; set; }
        IDbSet<Organization> Organizations { get; set; }
        IDbSet<Permission> Permissions { get; set; }
        IDbSet<Registration> Registrations { get; set; }
        IDbSet<RolePermission> RolePermissions { get ; set; }

        IDbSet<SdoCache> SdoCaches { get; set; }

        IDbSet<UserPreference> UserPreferences { get; set; }
        IDbSet<PoisonMtoRecord> PoisonMtoRecords { get; set; }
        IDbSet<PoisonMto> PoisonMtos { get; set; }
        IDbSet<ScheduleTask> ScheduleTasks { get; set; }

        IDbSet<NsdCategory> NsdCategories { get; set; }
        IDbSet<Proposal> Proposals { get; set; }
        IDbSet<ProposalHistory> ProposalHistories { get; set; }
        IDbSet<ProposalAttachment> ProposalAttachments { get; set; }
        IDbSet<Notam> Notams { get; set; }
        IDbSet<NotamTopic> NotamTopics { get; set; }
        IDbSet<ScheduleJob> ScheduleJobs { get; set; }
        IDbSet<MessageExchangeQueue> MessageExchangeQueues { get; set; }
        IDbSet<MessageExchangeTemplate> MessageExchangeTemplates { get; set; }
        IDbSet<NofTemplateMessageRecord> NofTemplateMessageRecords { get; set; }
        IDbSet<ItemD> ItemDs { get; set; }
        IDbSet<UserRegistration> UserRegistrations { get; set; }
        IDbSet<OriginatorInfo> OriginatorInfos { get; set; }
    }
}

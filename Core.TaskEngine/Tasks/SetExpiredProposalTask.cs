﻿using System;
using System.Linq;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.TaskEngine.Contracts;
using NavCanada.Core.TaskEngine.Mto;

namespace NavCanada.Core.TaskEngine.Tasks
{
    public class SetExpiredProposalTask : IMessageTask
    {
        public void Execute(IMessageTaskContext context)
        {
            var logger = context.ExternalServices.Logger;
            var loggerType = GetType();

            try
            {
                // if this were to fail we don't want to put the system down
                context.Mto.Add(MessageDefs.OverridesCriticalFailureKey, true, DataType.Boolean);

                logger.LogDebug(loggerType, "Begin executing Set Expired Proposal Task.");

                var uow = context.ClusterStorage.Uow;

                var expiredProposals = uow.ProposalRepo.GetExpiredProposals();
                if (expiredProposals.Any())
                {
                    foreach (var proposal in expiredProposals)
                    {
                        proposal.Status = NotamProposalStatusCode.Expired;
                        uow.ProposalRepo.Update(proposal);
                    }
                }

                logger.LogDebug(loggerType, $"End executing Set Expired Proposal Task, total: {expiredProposals.Count}.");
            }
            catch (Exception ex)
            {
                logger.LogError(ex, loggerType, "Failed to execute Set Expired Proposal Task!");
                throw;
            }
        }
    }
}
﻿import { Component, OnInit, OnChanges, OnDestroy, Inject, Input } from '@angular/core';
import { TranslationService } from 'angular-l10n';
import { FormGroup, FormBuilder, FormArray, AbstractControl, Validators } from '@angular/forms';
import { RscService } from '../rsc.service';
import { IWindrowsModel } from '../rsc.model';

import { TOASTR_TOKEN, Toastr } from '../../../../common/toastr.service';

@Component({
    selector: 'rsc-windrows',
    templateUrl: './app/dashboard/nsds/rsc/components/rsc-windrows.component.html'
})
export class WindrowsComponent implements OnInit, OnChanges, OnDestroy {
    @Input('form') mainForm: FormGroup = null;
    @Input('isReadOnly') isReadOnly: boolean = false;
    @Input('model') model: IWindrowsModel = null;

    frmGroup: FormGroup;
    isFormLoadingData: boolean;

    constructor(
        private fb: FormBuilder,
        public translation: TranslationService,
        private rscService: RscService,
        @Inject(TOASTR_TOKEN) private toastr: Toastr) {
    }

    ngOnInit() {
        this.isFormLoadingData = false;

        this.configureReactiveForm();
    }

    ngOnChanges() {
    }

    ngOnDestroy() {
    }

    $ctrl(name: string): AbstractControl {
        return this.frmGroup.get(name);
    }

    private configureReactiveForm() {
        const frmArrControls = <FormArray>this.mainForm.controls['frmWindrowsArr'];

        this.frmGroup = this.fb.group({
            windrowsRemarks: [{ value: this.model.windrowsRemarks, disabled: this.isReadOnly }, []],
        });

        frmArrControls.push(this.frmGroup);
    }

    //Export functions
    public isFormValid(): boolean {

        return true;
    }

    public exportComponentInternalData(): IWindrowsModel {
        let data: IWindrowsModel = {
            parent: this.model.parent,
            windrowsRemarks: this.model.windrowsRemarks,
        };
        return data;
    }

    public clearValues(): void {
        alert('Clear component values: Windrows');
    }
}
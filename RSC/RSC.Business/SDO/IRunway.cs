﻿namespace RSC.Business.SDO
{
    public interface IRunway
    {
        string Id { get; }
        string Designator { get; }
        /// <summary>
        /// Length in feet.
        /// </summary>
        int Length { get; }
        /// <summary>
        /// Width in feet.
        /// </summary>
        int Width { get; }
    }
}

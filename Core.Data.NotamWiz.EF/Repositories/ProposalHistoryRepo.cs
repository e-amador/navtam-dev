﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Data.EF.Extensions;
using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF.Extensions;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    public class ProposalHistoryRepo : Repository<ProposalHistory>, IProposalHistoryRepo
    {
        public ProposalHistoryRepo(DbContext dbContext)
            : base(dbContext)
        {
        }

        public async Task<ProposalHistory> GetByIdAsync(int id)
        {
            return await DbSet.SingleOrDefaultAsync(e => e.Id == id);
        }

        public async Task<ProposalHistory> GetLatestByProposalIdAsync(int proposalId)
        {
            return await DbSet
                .Where(e => e.ProposalId == proposalId)
                .OrderByDescending(e => e.Received)
                .FirstOrDefaultAsync();
        }

        public async Task<ProposalHistory> GetByProposalIdAndNotamIdAsync(string notamId, int proposalId)
        {
            return await DbSet
                .Where(e => e.ProposalId == proposalId && e.NotamId == notamId)
                .OrderByDescending(e => e.Received)
                .FirstOrDefaultAsync();
        }


        public async Task<List<ProposalDto>> GetByProposalIdPartialAsync(int proposalId)
        {
            /*
                    e.Status == NotamProposalStatusCode.Submitted || 
                    e.Status == NotamProposalStatusCode.Withdrawn ||
                    e.Status == NotamProposalStatusCode.Disseminated ||
                    e.Status == NotamProposalStatusCode.DisseminatedModified ||
                    e.Status == NotamProposalStatusCode.Rejected ||
                    e.Status == NotamProposalStatusCode.Replaced ||
                    e.Status == NotamProposalStatusCode.Cancelled ||
                    e.Status == NotamProposalStatusCode.ModifiedCancelled ||
                    e.Status == NotamProposalStatusCode.Terminated)
            */
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e => e.ProposalId == proposalId)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalHistory>> GetByProposalIdAsync(int proposalId)
        {
            return await DbSet
                .Where(e => e.ProposalId == proposalId)
                .OrderBy(e => e.Id)
                .ToListAsync();
        }

        public async Task<ProposalHistory> GetModifyByUserAsync(int proposalId)
        {
            return await DbSet
                .OrderByDescending(e => e.Id)
                .Skip(1) //skip the latest proposal inserted before comitting the transaction
                .FirstOrDefaultAsync(e =>
                    e.ProposalId == proposalId &&
                    (
                        e.Status == NotamProposalStatusCode.Submitted ||
                        e.Status == NotamProposalStatusCode.Disseminated ||
                        e.Status == NotamProposalStatusCode.DisseminatedModified
                     )
                 );
        }

        public async Task<ProposalHistory> GetLastSubmittedProposalAsync(int proposalId)
        {
            return await DbSet
                .OrderByDescending(e => e.Id)
                .FirstOrDefaultAsync(e =>
                    e.ProposalId == proposalId &&
                    (
                        e.Status == NotamProposalStatusCode.Submitted ||
                        e.Status == NotamProposalStatusCode.ModifiedAndSubmitted
                     )
                 );
        }

        public async Task<ProposalHistory> GetLastParkedProposalAsync(int proposalId)
        {
            return await DbSet
                .OrderBy(e => e.Id)
                .FirstOrDefaultAsync(e =>
                    e.ProposalId == proposalId &&
                    (
                        e.Status == NotamProposalStatusCode.Parked ||
                        e.Status == NotamProposalStatusCode.ParkedDraft
                     )
                 );
        }

        public async Task<ProposalHistory> GetLastDisseminatedProposalAsync(int proposalId)
        {
            return await DbSet
                .OrderBy(e => e.Id)
                .FirstOrDefaultAsync(e =>
                    e.ProposalId == proposalId &&
                    (
                        e.Status == NotamProposalStatusCode.Disseminated ||
                        e.Status == NotamProposalStatusCode.DisseminatedModified
                     )
                 );
        }

        public async Task<int> GetCountBySeries(string series)
        {
            return await DbSet.CountAsync(p => p.Series == series);
        }

        public async Task<List<ProposalHistory>> GetByProposalIdDescAsync(int proposalId)
        {
            return await DbSet
                .Where(e => e.ProposalId == proposalId)
                .OrderByDescending(e => e.Id)
                .ToListAsync();
        }

        public async Task<ProposalHistory> GetLatestByStatusProposalIdAsync(int proposalId, NotamProposalStatusCode status)
        {
            return await DbSet
                .Where(e => e.ProposalId == proposalId && e.Status == status)
                .OrderByDescending(e => e.Id)
                .FirstOrDefaultAsync();
        }

        public async Task<List<ProposalDto>> FilterAsync(FilterCriteria filterCriteria, int orgId, OrganizationType orgType, DbGeography region, IQueryable<ProposalHistory> filterQuery)
        {
            List<ProposalHistory> proposals = null;

            var ctx = DbContext as AppDbContext;
            ctx.Database.CommandTimeout = 120;

            if (orgType == OrganizationType.External)
            {
                proposals = await filterQuery
                    .Include(e => e.IcaoSubject)
                    .Include(e => e.IcaoSubjectCondition)
                    .Include(e => e.Category)
                    .Where(e =>
                        (e.OrganizationId == orgId) &&
                        !e.Grouped &&
                        !e.Delete &&
                        e.Location != null && region.Intersects(e.Location))
                    .ApplySort(filterCriteria.QueryOptions.Sort)
                    .Skip(filterCriteria.QueryOptions.PageSize * (filterCriteria.QueryOptions.Page - 1))
                    .Take(filterCriteria.QueryOptions.PageSize)
                    .ToListAsync();
            }
            else
            {
                proposals = await filterQuery
                    .Include(e => e.IcaoSubject)
                    .Include(e => e.IcaoSubjectCondition)
                    .Include(e => e.Category)
                    .Where(e =>
                        //(e.OrganizationType != OrganizationType.Nof) &&
                        !e.Grouped &&
                        !e.Delete &&
                        e.Location != null &&
                        region.Intersects(e.Location))
                    .ApplySort(filterCriteria.QueryOptions.Sort)
                    .Skip(filterCriteria.QueryOptions.PageSize * (filterCriteria.QueryOptions.Page - 1))
                    .Take(filterCriteria.QueryOptions.PageSize)
                    .ToListAsync();
            }

            return GenerateProposalDto(proposals);
        }

        public async Task<int> FilterCountAsync(FilterCriteria filterCriteria, int orgId, OrganizationType orgType, DbGeography region, IQueryable<ProposalHistory> filterQuery)
        {
            var ctx = DbContext as AppDbContext;
            ctx.Database.CommandTimeout = 120;

            if (orgType == OrganizationType.External)
            {
                return await filterQuery.CountAsync(e =>
                                (e.OrganizationId == orgId) &&
                                !e.Grouped &&
                                !e.Delete && e.Location != null && region.Intersects(e.Location));
            }

            return await filterQuery.CountAsync(e =>
                        //(e.OrganizationType != OrganizationType.Nof) &&
                        !e.Grouped &&
                        !e.Delete &&
                        e.Location != null && region.Intersects(e.Location));
        }

        public IQueryable<ProposalHistory> PrepareQueryFilter(FilterCriteria filterCriteria)
        {
            var expressions = new List<Expression<Func<ProposalHistory, bool>>>();
            IQueryable<ProposalHistory> query = DbSet;
            bool endValidityFilter = false;

            foreach (var cond in filterCriteria.Conditions)
            {
                var field = cond.Field.ToUpper();
                var value = cond.Value.ToString().ToUpper();
                var condition = cond.Value.ToString().ToUpper();
                var oper = cond.Operator;
                switch (field)
                {
                    case "PROPOSALTYPE":
                        cond.Value = (int)Enum.Parse(typeof(NotamType), value);
                        break;
                    case "LOWERLIMIT":
                    case "UPPERLIMIT":
                    case "RADIUS":
                        int number;
                        if (int.TryParse(value, out number))
                        {
                            cond.Value = number;
                        }
                        else
                        {
                            cond.Value = 0;
                        }
                        break;
                    case "RECEIVED":
                        DateTime published;
                        if (DateTime.TryParseExact(value, "yyMMddHHmm", CultureInfo.InvariantCulture, DateTimeStyles.None, out published))
                        {
                            cond.Value = value;
                        }
                        else
                        {
                            cond.Value = DateTime.Now;
                        }
                        break;
                    case "STARTACTIVITY":
                        if (condition == "IMMEDIATE" || condition == "")
                        {
                            cond.Value = null;
                        }
                        else
                        {
                            DateTime startActivity;
                            if (DateTime.TryParseExact(value, "yyMMddHHmm", CultureInfo.InvariantCulture, DateTimeStyles.None, out startActivity))
                            {
                                cond.Value = value;
                            }
                            else
                            {
                                cond.Value = DateTime.Now;
                            }
                        }
                        break;
                    case "ENDVALIDITY":
                        endValidityFilter = true; // cond.Operator == "GTE" || cond.Operator == "GT";
                        if (condition == "PERM" || condition == "")
                        {
                            cond.Value = null;
                        }
                        else
                        {
                            DateTime endValidity;
                            if (DateTime.TryParseExact(value, "yyMMddHHmm", CultureInfo.InvariantCulture, DateTimeStyles.None, out endValidity))
                            {
                                cond.Value = value;
                            }
                            else
                            {
                                cond.Value = DateTime.Now;
                            }
                        }
                        break;
                }

                var expression = BuildWhereExpression(cond.Field, cond.Value, cond.Operator);
                if (endValidityFilter)
                {
                    if (endValidityFilter) //&& (oper == "EQ" || oper == "GT" || oper == "GTE")
                    {
                        var otherExpression = BuildWhereExpression("Permanent", true, "EQ");
                        expression = expression.Or(otherExpression);
                    }
                }

                query = query.Where(expression);
            }

            return query;
        }

        private List<ProposalDto> GenerateProposalDto(List<ProposalHistory> proposals)
        {
            var dtos = new List<ProposalDto>();
            var culture = new CultureInfo("en-US");
            foreach (var proposal in proposals)
            {
                dtos.Add(new ProposalDto
                {
                    Id = proposal.Id,
                    NotamId = proposal.NotamId,
                    CategoryId = proposal.CategoryId,
                    CategoryName = proposal.Category.Name,
                    ItemA = proposal.ItemA,
                    IcaoSubjectDesc = proposal.IcaoSubject.Name,
                    IcaoConditionDesc = proposal.IcaoSubjectCondition.Description,
                    StartActivity = (proposal.StartActivity.HasValue) ? proposal.StartActivity.Value.ToString("yyMMddHHmm", culture) : "IMMEDIATE",
                    EndValidity = (proposal.EndValidity.HasValue) ? proposal.EndValidity.Value.ToString("yyMMddHHmm", culture) : ((proposal.ProposalType == NotamType.C) ? "" : "PERM"),
                    Estimated = proposal.Estimated,
                    Received = proposal.Received,
                    ModifiedByUsr = proposal.ModifiedByUsr,
                    Originator = proposal.Originator,
                    ProposalType = proposal.ProposalType,
                    Status = proposal.Status,
                    ItemE = proposal.ItemE,
                    Operator = proposal.Operator,
                });
            }
            return dtos;
        }
    }
}
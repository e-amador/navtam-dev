﻿import { Component, ViewChild, OnInit, OnDestroy, Inject, Injector } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'

import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';
import { TranslationService } from 'angular-l10n';

import { DataService } from '../shared/data.service'
import { ProposalStatus } from '../shared/data.model';

import { NsdFormComponent } from '../nsds/nsd-form.component'

//import { SignalR, SignalRConnection, BroadcastEventListener, ConnectionStatus } from 'ng2-signalr';

@Component({ 
    templateUrl: '/app/dashboard/nof/nof-pick.component.html'
})
export class NofPickComponent implements OnInit, OnDestroy  {
    
    model : any = null;
    nsdForm : NsdFormComponent;
    title: string;
    faIcon: string;
    proposalOwner: string;
    isOwnershipActionTaken: boolean = false;
    isOwnershipActionLost: boolean = false;

    @ViewChild(NsdFormComponent)
    set form(v: NsdFormComponent){
        this.nsdForm = v;
    }

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private dataService : DataService, 
        private activatedRoute: ActivatedRoute, 
        //private _signalR: SignalR
        private router: Router,
        public translation: TranslationService) {

        switch (activatedRoute.snapshot.data[0]['type']) {
            case 'draft':
            case 'clone':
                this.title = this.translation.translate('Common.Edit'); //"Edit";
                this.faIcon = "icon-plus";
                break;
            case 'replace':
                this.title = this.translation.translate('Common.Replace'); //"Replace";
                this.faIcon = "fa fa-code-fork";
                break;
            case 'cancel':
                this.title = this.translation.translate('Common.Cancel'); //"Cancel";
                this.faIcon = "fa fa-hand-paper-o";
                break;
            default:
                this.title = this.translation.translate('Common.Edit'); //"Edit";
                this.faIcon = "icon-plus";
                break;
        }  

    }

    ngOnInit() {
        window['loadLeafletMapScript'].call(this, null);
        this.model = this.activatedRoute.snapshot.data['model'];
        if (this.model.status === ProposalStatus.Taken) {
            this.proposalOwner = this.model.modifiedByUsr;

            const self = this;
            const el = $('#modal-takeownership');

            (<any>el).modal({});

            el.on('hide.bs.modal', function (e) {
                window.setTimeout(function () {
                    if (!self.isOwnershipActionTaken) {
                        self.router.navigate(['/dashboard']);
                    }
                }, 500); 
            })
        }
    }

    ngOnDestroy() {
        if (window["app"].leafletmap) {
            window["app"].leafletmap.dispose();
        }
    }

    get langCulture() {
        return (window["app"].cultureInfo || "en").toUpperCase();
    }

    takeProposalOwnership() {
        this.isOwnershipActionTaken = true;
        this.dataService.getProposalModelEnforced(this.model.proposalId)
            .subscribe((data: any) => {
                this.model = data;
            }, (error: any) => {
                let msg = this.translation.translate('NofPick.TakeOwnershipError') + error.message;
                this.toastr.error(msg, "", { 'positionClass': 'toast-bottom-right' });
                this.backToDashboard();
           });
    }

    private showOwnershipActionLostNotification(data: any) {
        const userId = window['app']['usrId'];
        if (data.userId !== userId && this.model.proposalId === data.id) {
            const self = this;
            const el = $('#modal-ownershiptaken');

            (<any>el).modal({});

            el.on('hide.bs.modal', function (e) {
                window.setTimeout(function () {
                    if (!self.isOwnershipActionLost) {
                        self.router.navigate(['/dashboard']);
                    }
                }, 500);
            })
        }
    }

    backToDashboard() {
        this.proposalOwner = null;
    }
}
﻿import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { INsdCategory } from './../shared/data.model';
import { DataService } from './../shared/data.service'

@Injectable()
//export class CategoriesResolver implements Resolve<INsdCategory[]> {
export class CategoriesResolver implements Resolve<any> {

    constructor(private dataService: DataService) {
    }
    resolve() {
        return this.dataService.getNsdCategories().map(categories => categories);
    }
}
namespace NavCanada.Applications.NotamWiz.Web.NSDs.NavaIds.V0R1
{
    public class ViewModel : NotamProposalViewModelBase
    {
        public NavaOption NavaType { get; set; }
        public NavaOption NavaOption { get; set; }
    }
}
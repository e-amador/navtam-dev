﻿import { Component, OnInit, OnChanges, OnDestroy, Inject, Input } from '@angular/core';
import { TranslationService } from 'angular-l10n';
import { ISnowbanksModel, ISnowbanksRunway, ISnowbank, IBankHeight, CardinalSideEnum } from '../rsc.model';
import { FormGroup, FormBuilder, FormArray, AbstractControl, Validators } from '@angular/forms';
import { RscService } from '../rsc.service';

import { TOASTR_TOKEN, Toastr } from '../../../../common/toastr.service';

@Component({
    selector: 'rsc-snowbanks',
    templateUrl: './app/dashboard/nsds/rsc/components/rsc-snowbanks.component.html'
})
export class SnowBanksComponent implements OnInit, OnChanges, OnDestroy {
    @Input('form') mainForm: FormGroup = null;
    @Input('model') model: ISnowbanksModel = null;
    @Input('isReadOnly') isReadOnly: boolean = false;

    frmGroup: FormGroup;
    isFormLoadingData: boolean;

    cardinalOptions = {
        width: "100%",
        placeholder: {
            id: '-1',
            text: this.translation.translate('ClearedWidthComponent.Select')
        }
    }
    cardinalValues: any;
    cardinalPointsByDirections: any;

    constructor(
        private fb: FormBuilder,
        public translation: TranslationService,
        private rscService: RscService,
        @Inject(TOASTR_TOKEN) private toastr: Toastr) {
    }

    ngOnInit() {
        this.isFormLoadingData = false;
        this.cardinalValues = this.model.parent.cardinalSideEnum;
        this.cardinalPointsByDirections = this.model.parent.cardinalPointsByDirections;

        this.configureReactiveForm();

    }

    ngOnChanges() {
    }

    ngOnDestroy() {
    }

    $ctrl(name: string): AbstractControl {
        return this.frmGroup.get(name);
    }

    private configureReactiveForm() {
        const frmArrControls = <FormArray>this.mainForm.controls['frmSnowBanksArr'];

        this.frmGroup = this.fb.group({
            grpFirstSnowbank: this.fb.group({
                bankHeightFt: [{ value: this.model.firstSnowbank.bankHeight.bankHeightFt, disabled: this.isReadOnly }, []],
                bankHeightInch: [{ value: this.model.firstSnowbank.bankHeight.bankHeightInch, disabled: this.isReadOnly }, []],
                bankEdgeFt: [{ value: this.model.firstSnowbank.bankEdgeFt, disabled: this.isReadOnly }, []],
                bankSide: [{ value: this.model.firstSnowbank.bankSide, disabled: this.isReadOnly }, []],
            }),
            grpSecondSnowbank: this.fb.group({
                bankHeightFt: [{ value: this.model.secondSnowbank.bankHeight.bankHeightFt, disabled: this.isReadOnly }, []],
                bankHeightInch: [{ value: this.model.secondSnowbank.bankHeight.bankHeightInch, disabled: this.isReadOnly }, []],
                bankEdgeFt: [{ value: this.model.secondSnowbank.bankEdgeFt, disabled: this.isReadOnly }, []],
                bankSide: [{ value: this.model.secondSnowbank.bankSide, disabled: this.isReadOnly }, []],
            }),
        });

        frmArrControls.push(this.frmGroup);

    }

    onBankHeightFtChange(groupName: string): void {
        if (groupName === 'first') {
            //Do something
        } else {
            //Do something
        }
    }

    onBankHeightInchChange(groupName: string): void {
        if (groupName === 'first') {
            //Do something
        } else {
            //Do something
        }
    }

    onBankEdgeFtChange(groupName: string): void {
        if (groupName === 'first') {
            //Do something
        } else {
            //Do something
        }
    }

    onBankSideChanged(groupName: string, e: any): void {
        if (e.value) {
            let cardinalId = e.value;
            if (groupName === 'first') {
                const control = this.frmGroup.get('grpFirstSnowbank.bankSide')
                if (this.isFormLoadingData) {
                    cardinalId = control.value;
                } else {
                    if (cardinalId !== control.value) {
                        control.setValue(cardinalId);
                        control.markAsDirty();
                        this.model.firstSnowbank.bankSide = cardinalId;
                    }
                }
            } else {
                const control = this.frmGroup.get('grpFirstSnowbank.bankSide');
                if (this.isFormLoadingData) {
                    cardinalId = control.value;
                } else {
                    if (cardinalId !== control.value) {
                        control.setValue(cardinalId);
                        control.markAsDirty();
                        this.model.secondSnowbank.bankSide = cardinalId;
                    }
                }
            }
        }
    }

    //General functions
    private isValidNumber(value: string | number): boolean {
        return ((value != null) &&
            (value !== '') &&
            !isNaN(Number(value.toString())));
    }

    private isANumber(str: string): boolean {
        return !/\D/.test(str);
    }

    public validateKeyPressNumber(evt: any) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!this.isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
        }
    }
    // End General functions

    //Export functions
    public isFormValid(): boolean {

        return true;
    }

    public exportComponentInternalData(): ISnowbanksModel {
        let first: ISnowbank = {
            bankHeight: {
                bankHeightFt: this.model.firstSnowbank.bankHeight.bankHeightFt,
                bankHeightInch: this.model.firstSnowbank.bankHeight.bankHeightInch
            },
            bankEdgeFt: this.model.firstSnowbank.bankEdgeFt,
            bankSide: this.model.firstSnowbank.bankSide,
        };
        let second: ISnowbank = {
            bankHeight: {
                bankHeightFt: this.model.secondSnowbank.bankHeight.bankHeightFt,
                bankHeightInch: this.model.secondSnowbank.bankHeight.bankHeightInch
            },
            bankEdgeFt: this.model.secondSnowbank.bankEdgeFt,
            bankSide: this.model.secondSnowbank.bankSide,
        };
        let data: ISnowbanksModel = {
            parent: this.model.parent,
            runway: this.model.runway,
            firstSnowbank: first,
            secondSnowbank: second,
        };
        return data;

    }

    public clearValues(): void {
        alert('Clear Component Values: Snowbanks');
    }


}


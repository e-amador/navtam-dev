﻿namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    using System.Data.Entity;

    using Contracts.Repositories;
    using Domain.Model.Entitities;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using System.Linq;
    using System;

    public class ProposalAttachmentRepo : Repository<ProposalAttachment>, IProposalAttachmentRepo
    {
        public ProposalAttachmentRepo(DbContext dbContext)
            : base(dbContext)
        {
        }

        public async Task<ProposalAttachment> GetByIdAsync(Guid id)
        {
            return await DbSet.SingleOrDefaultAsync(e => e.Id == id);
        }

        public async Task<List<ProposalAttachment>> GetByProposalIdAsync(int proposalId)
        {
            return await DbSet
                .Where(e => e.ProposalId == proposalId)
                .ToListAsync();
        }
    }
}

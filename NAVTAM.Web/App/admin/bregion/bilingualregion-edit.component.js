"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BilingualRegionEditComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var windowRef_service_1 = require("../../common/windowRef.service");
var angular_l10n_1 = require("angular-l10n");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var toastr_service_1 = require("./../../common/toastr.service");
var data_service_1 = require("../shared/data.service");
var data_model_1 = require("../shared/data.model");
var BilingualRegionEditComponent = /** @class */ (function () {
    function BilingualRegionEditComponent(toastr, fb, dataService, memStorageService, router, winRef, activatedRoute, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.activatedRoute = activatedRoute;
        this.locale = locale;
        this.translation = translation;
        this.model = null;
        this.isValidating = false;
        this.isValidDoaLocation = true;
        this.isValidModifyLocation = true;
        this.isSubmitting = false;
        this.doaChangesList = [];
        this.selectedDoaLocation = null;
        this.doaSources = [];
        this.selectedSourceId = 1;
        this.firSources = [];
        this.selectedFirId = '-1';
        this.activeLocationPoint = null;
        this.radiusLimit = 999;
        this.doaSource = this.translation.translate('DoasAdminSession.DoaSourceGeography');
        this.mapHealthy = false;
        if (this.winRef.appConfig.ccs || this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);
    }
    BilingualRegionEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activeLocationPoint = { source: '', location: '', inDoa: false, radius: 0 };
        this.selectedDoaLocation = { source: '', location: '', inDoa: false, radius: 5 };
        this.action = this.activatedRoute.snapshot.data[0].action;
        this.model = this.activatedRoute.snapshot.data['model'];
        this.bRegion = this.model;
        //Sources
        this.doaSourceOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('DoasAdminSession.SelectSource')
            },
            width: "100%"
        };
        this.loadDoaSources();
        this.selectedSourceId = +this.doaSources.find(function (x) { return x.id === String(_this.bRegion.region.source); }).id;
        //FIRS
        this.firSourceOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('DoasAdminSession.SelectFir')
            },
            width: "100%"
        };
        this.loadFirSources();
        this.configureReactiveForm();
    };
    BilingualRegionEditComponent.prototype.ngAfterViewInit = function () {
        this.mapHealthCheck();
    };
    BilingualRegionEditComponent.prototype.ngOnDestroy = function () {
        this.disposeMap();
    };
    Object.defineProperty(BilingualRegionEditComponent.prototype, "langCulture", {
        get: function () {
            return (window["app"].cultureInfo || "en").toUpperCase();
        },
        enumerable: false,
        configurable: true
    });
    BilingualRegionEditComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.isSubmitting)
            return;
        this.isSubmitting = true;
        var doaUpdate = this.prepareData();
        if (doaUpdate) {
            this.dataService.saveDoa(doaUpdate)
                .subscribe(function (data) {
                var msg = _this.translation.translate('DoasAdminSession.SuccessDoaCreated');
                _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                _this.router.navigate(["/administration"]);
            }, function (error) {
                _this.isSubmitting = false;
                var msg = _this.translation.translate('DoasAdminSession.FailureDoaCreated');
                _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
        }
        else {
            this.isSubmitting = false;
            var msg = this.translation.translate('DoasAdminSession.ErrorDoaData');
            this.toastr.error(msg, "", { 'positionClass': 'toast-bottom-right' });
        }
    };
    BilingualRegionEditComponent.prototype.onCancel = function () {
        var _this = this;
        this.dataService.getDoaBilingualRegion()
            .subscribe(function (model) {
            _this.isValidDoaLocation = true;
            _this.model = model;
            _this.bRegion = _this.model;
            _this.selectedSourceId = +_this.doaSources.find(function (x) { return x.id === String(_this.bRegion.region.source); }).id;
            _this.selectedFirId = '-1';
            _this.doaChangesList = [];
            _this.activeLocationPoint = { source: '', location: '', inDoa: false, radius: 0 };
            _this.selectedDoaLocation = { source: '', location: '', inDoa: false, radius: 5 };
            _this.configureReactiveForm();
            _this.bilingualRegionRenderMap();
        }, function (error) {
            _this.isValidDoaLocation = false;
            _this.bRegion.region.location = '';
            _this.isValidating = false;
        });
    };
    BilingualRegionEditComponent.prototype.configureReactiveForm = function () {
        var _this = this;
        this.bRegionForm = this.fb.group({
            doaName: [{ value: this.model.name, disabled: true }, []],
            doaDescription: [{ value: this.model.description, disabled: true }, []],
            doaSource: [{ value: this.doaSource, disabled: true }, []],
            doaGeo: [{ value: this.model.region.geography, disabled: true }, []],
            doaLocation: [{ value: this.model.region.location, disabled: false }, []],
            doaRadius: [{ value: this.model.region.radius, disabled: false }, []],
            doaPoints: [{ value: this.model.region.points, disabled: false }, []],
            modifyLocation: [{ value: this.selectedDoaLocation.location, disabled: false }, []],
            modifyRadius: [{ value: this.selectedDoaLocation.radius, disabled: false }, []],
        });
        this.$ctrl("doaRadius").setValidators(radiusValidatorFn(this.radiusLimit));
        this.$ctrl("modifyRadius").setValidators(radiusValidatorFn(this.radiusLimit));
        //doaLocation is the Point to define a Region
        this.$ctrl("doaLocation").valueChanges.debounceTime(1000).subscribe(function () { return _this.validateDoaLocation(); });
        //doaPoints is the set of point to define a Region
        this.$ctrl("doaPoints").valueChanges.debounceTime(1000).subscribe(function () { return _this.validateDoaPointsLocation(); });
        //modifyLocation is the Point and Actions to modify an already defined Region
        this.$ctrl("modifyLocation").valueChanges.debounceTime(1000).subscribe(function () { return _this.validateModifyLocation(); });
        this.$ctrl("doaRadius").valueChanges.debounceTime(1000).subscribe(function () { return _this.validateDoaLocation(); });
    };
    BilingualRegionEditComponent.prototype.validateForm = function () {
        if (this.isValidating)
            return false;
        if (!this.bRegionForm.pristine) {
            return this.validateData();
        }
        return false;
    };
    BilingualRegionEditComponent.prototype.prepareData = function () {
        if (this.bRegion.region.source === data_model_1.DoaRegionSource.PointAndRadius) {
            var radCtrl = this.$ctrl("doaRadius");
            this.bRegion.region.radius = +radCtrl.value;
            this.bRegion.region.location = this.bRegion.region.location.toUpperCase();
        }
        var doaUpdate = {
            id: this.bRegion.id,
            name: this.bRegion.name,
            description: this.bRegion.description,
            region: this.bRegion.region,
            doaType: data_model_1.RegionType.Bilingual,
            changes: this.doaChangesList
        };
        return doaUpdate;
    };
    BilingualRegionEditComponent.prototype.validateData = function () {
        var data = this.prepareData();
        if (data) {
            if (data.region.source === data_model_1.DoaRegionSource.None)
                return false;
            if (data.region.source === data_model_1.DoaRegionSource.FIR && data.region.fir.length === 0)
                return false;
            if (data.region.source === data_model_1.DoaRegionSource.Geography && data.region.geography.length === 0)
                return false;
            if (data.region.source === data_model_1.DoaRegionSource.PointAndRadius) {
                if (data.region.location.length === 0)
                    return false;
                if (data.region.radius < 1 || data.region.radius > this.radiusLimit)
                    return false;
            }
            if (data.region.source === data_model_1.DoaRegionSource.Points && data.region.points.length === 0)
                return false;
            return true;
        }
        return false;
    };
    BilingualRegionEditComponent.prototype.validateDoaLocation = function () {
        var _this = this;
        if (this.isValidating)
            return;
        var dLocation = this.$ctrl('doaLocation');
        if (dLocation.value) {
            if (dLocation.value.length > 3) {
                this.isValidating = true;
                this.dataService.validateDoaLocation(dLocation.value)
                    .subscribe(function (resp) {
                    _this.isValidDoaLocation = true;
                    _this.bRegion.region.location = resp.toUpperCase();
                    _this.isValidating = false;
                    _this.validateForm();
                    _this.bilingualRegionRenderMap();
                }, function (error) {
                    _this.isValidDoaLocation = false;
                    _this.bRegion.region.location = '';
                    _this.isValidating = false;
                });
            }
            else
                this.isValidDoaLocation = false;
        }
        else
            this.isValidDoaLocation = false;
    };
    BilingualRegionEditComponent.prototype.validateDoaPointsLocation = function () {
        var _this = this;
        if (this.isValidating)
            return;
        var dLocation = this.$ctrl('doaPoints');
        if (dLocation.value) {
            if (dLocation.value.length > 3) {
                this.isValidating = true;
                this.dataService.validateDoaPoints(dLocation.value)
                    .subscribe(function (resp) {
                    _this.isValidDoaLocation = true;
                    _this.bRegion.region.points = resp.toUpperCase();
                    _this.validateForm();
                    _this.bilingualRegionRenderMap();
                    _this.isValidating = false;
                }, function (error) {
                    _this.isValidDoaLocation = false;
                    _this.bRegion.region.points = '';
                    _this.isValidating = false;
                });
            }
            else
                this.isValidDoaLocation = false;
        }
        else
            this.isValidDoaLocation = false;
    };
    BilingualRegionEditComponent.prototype.validateModifyLocation = function () {
        var _this = this;
        if (this.isValidating)
            return;
        var dLocation = this.$ctrl('modifyLocation');
        if (dLocation.value) {
            if (dLocation.value.length > 3) {
                this.isValidating = true;
                this.isValidModifyLocation = false;
                var regLoc = {
                    region: this.bRegion.region,
                    location: dLocation.value
                };
                this.dataService.validateLocInRegion(regLoc)
                    .subscribe(function (resp) {
                    _this.isValidModifyLocation = true;
                    _this.activeLocationPoint = resp;
                    _this.isValidating = false;
                    _this.validateForm();
                }, function (error) {
                    _this.activeLocationPoint.location = '';
                    _this.isValidating = false;
                });
            }
        }
    };
    BilingualRegionEditComponent.prototype.prepareModifyLocationData = function () {
        if (this.isValidModifyLocation) {
            var radiusCtrl = this.$ctrl('modifyRadius');
            var loc = { source: '', location: '', inDoa: false, radius: 5 };
            loc.source = this.activeLocationPoint.source.toUpperCase();
            loc.location = this.activeLocationPoint.location.toUpperCase();
            loc.radius = +radiusCtrl.value;
            loc.inDoa = this.activeLocationPoint.inDoa;
            var point = this.doaChangesList.find(function (x) { return x.source === loc.source && x.location === loc.location; });
            if (point) {
                point.radius = loc.radius;
                this.selectedDoaLocation = point;
            }
            else {
                var index = this.doaChangesList.push(loc);
                this.selectedDoaLocation = this.doaChangesList[index - 1];
            }
            this.bilingualRegionRenderMap();
        }
    };
    BilingualRegionEditComponent.prototype.includeLocationEnabled = function () {
        if (this.isValidModifyLocation && !this.isValidating) {
            var radiusCtrl = this.$ctrl('modifyRadius');
            if (this.activeLocationPoint && +radiusCtrl.value > 0 && +radiusCtrl.value <= this.radiusLimit && this.activeLocationPoint.source !== '' && this.activeLocationPoint.location !== '') {
                return !this.activeLocationPoint.inDoa;
            }
        }
        return false;
    };
    BilingualRegionEditComponent.prototype.excludeLocationEnabled = function () {
        if (this.isValidModifyLocation && !this.isValidating) {
            var radiusCtrl = this.$ctrl('modifyRadius');
            if (this.activeLocationPoint && +radiusCtrl.value > 0 && +radiusCtrl.value <= this.radiusLimit && this.activeLocationPoint.source !== '' && this.activeLocationPoint.location !== '')
                return this.activeLocationPoint.inDoa;
        }
        return false;
    };
    BilingualRegionEditComponent.prototype.disableSubmit = function () {
        if (this.bRegionForm.pristine)
            return true;
        if (this.isValidating)
            return true;
        if (this.isSubmitting)
            return true;
        return !this.validateForm();
    };
    BilingualRegionEditComponent.prototype.pickLocation = function (loc) {
        if (this.doaChangesList) {
            var point = this.doaChangesList.find(function (x) { return (x.source === loc.source && x.location === loc.location && x.inDoa === loc.inDoa); });
            if (point) {
                this.selectedDoaLocation = point;
                var ctrlLocation = this.$ctrl('doaLocation');
                var ctrlRadius = this.$ctrl('doaRadius');
                ctrlLocation.setValue(this.selectedDoaLocation.source);
                ctrlRadius.setValue(this.selectedDoaLocation.radius);
            }
        }
    };
    BilingualRegionEditComponent.prototype.$ctrl = function (name) {
        return this.bRegionForm.get(name);
    };
    BilingualRegionEditComponent.prototype.rowColor = function (loc) {
        if (!this.selectedDoaLocation)
            return '';
        if (this.selectedDoaLocation.source === loc.source &&
            this.selectedDoaLocation.location === loc.location &&
            this.selectedDoaLocation.inDoa === loc.inDoa)
            return 'bg-submitted';
        return '';
    };
    BilingualRegionEditComponent.prototype.deletePoint = function (loc) {
        if (this.doaChangesList) {
            var index = this.doaChangesList.findIndex(function (x) { return (x.source === loc.source && x.location === loc.location && x.inDoa === loc.inDoa); });
            if (index > -1) {
                this.doaChangesList.splice(index, 1);
                if (this.doaChangesList.length > 0) {
                    this.selectedDoaLocation = this.doaChangesList[0];
                    var ctrlLocation = this.$ctrl('modifyLocation');
                    var ctrlRadius = this.$ctrl('modifyRadius');
                    ctrlLocation.setValue(this.selectedDoaLocation.source);
                    ctrlRadius.setValue(this.selectedDoaLocation.radius);
                }
                else {
                    var ctrlLocation = this.$ctrl('modifyLocation');
                    var ctrlRadius = this.$ctrl('modifyRadius');
                    ctrlLocation.setValue('');
                    ctrlRadius.setValue(5);
                    this.activeLocationPoint = { source: '', location: '', inDoa: false, radius: 0 };
                    this.selectedDoaLocation = { source: '', location: '', inDoa: false, radius: 5 };
                }
                this.bilingualRegionRenderMap();
            }
        }
    };
    BilingualRegionEditComponent.prototype.getAction = function (inDoa) {
        if (inDoa)
            return this.translation.translate('DoasAdminSession.Exclude');
        else
            return this.translation.translate('DoasAdminSession.Include');
    };
    BilingualRegionEditComponent.prototype.validateRadius = function (ctrlName) {
        if (this.bRegionForm.pristine)
            return true;
        var fc = this.bRegionForm.get(ctrlName);
        if (fc.errors)
            return false;
        if (fc.value) {
            if (fc.value > this.radiusLimit)
                return false;
            if (fc.value < 1)
                return false;
        }
        else
            return false;
        return true;
    };
    BilingualRegionEditComponent.prototype.validModifyLocation = function () {
        return this.isValidModifyLocation || this.bRegionForm.pristine;
    };
    BilingualRegionEditComponent.prototype.decRadius = function (ctrlName) {
        var radCtrl = this.$ctrl(ctrlName);
        if (+radCtrl.value > 0) {
            radCtrl.setValue(+radCtrl.value - 1);
            radCtrl.markAsDirty();
        }
    };
    BilingualRegionEditComponent.prototype.incRadius = function (ctrlName) {
        var radCtrl = this.$ctrl(ctrlName);
        if (+radCtrl.value < this.radiusLimit) {
            radCtrl.setValue(+radCtrl.value + 1);
            radCtrl.markAsDirty();
        }
    };
    BilingualRegionEditComponent.prototype.validateKeyPressNumber = function (evt) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
        }
    };
    BilingualRegionEditComponent.prototype.saveDoaFile = function () {
        var _this = this;
        var winObj = this.winRef.nativeWindow;
        if (winObj["FormData"] !== undefined) {
            var fileUpload = $("#attachments").get(0);
            var files = fileUpload.files;
            // Create FormData object  
            var fileData = new FormData();
            fileData.append(files[0].name, files[0]);
            this.dataService.saveDoaFile(fileData)
                .finally(function () {
                $("#attachments").val('');
                if (!/safari/i.test(navigator.userAgent)) {
                    $("#attachments").type = '';
                    $("#attachments").type = 'file';
                }
            })
                .subscribe(function (data) {
                _this.bRegion.region.geography = data;
                _this.bRegion.region.source = data_model_1.DoaRegionSource.Geography;
                _this.$ctrl('doaGeo').setValue(_this.bRegion.region.geography);
                var message = _this.translation.translate('DoasAdminSession.FileSuccess');
                _this.toastr.success(message, "", { 'positionClass': 'toast-bottom-right' });
                _this.bRegionForm.markAsDirty();
                _this.resetChangeList();
                _this.bilingualRegionRenderMap();
            }, function (error) {
                _this.bRegion.region.geography = "";
                _this.bRegion.region.source = data_model_1.DoaRegionSource.None;
                _this.$ctrl('doaGeo').setValue(_this.bRegion.region.geography);
                _this.toastr.error(_this.translation.translate('DoasAdminSession.MessageSaveAttachmentError') + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
        }
        else {
            this.bRegion.region.geography = "";
            this.bRegion.region.source = data_model_1.DoaRegionSource.None;
            this.toastr.error(this.translation.translate('DoasAdminSession.MessageSaveAttachmentBrowser1'), this.translation.translate('DoasAdminSession.MessageSaveAttachmentBrowser2'), { 'positionClass': 'toast-bottom-right' });
        }
    };
    BilingualRegionEditComponent.prototype.loadFirSources = function () {
        var _this = this;
        this.dataService.getAllFirs()
            .subscribe(function (firs) {
            _this.firSources = firs.map(function (r) {
                return {
                    id: r.id,
                    text: r.designator + " - " + r.name
                };
            });
            _this.firSources.unshift({ id: "-1", text: "" });
            _this.selectedFirId = '-1';
        });
    };
    BilingualRegionEditComponent.prototype.loadDoaSources = function () {
        this.doaSources = [
            //{ id: '0', text: this.translation.translate('DoasAdminSession.DoaSourceNone') },
            { id: '1', text: this.translation.translate('DoasAdminSession.DoaSourceGeography') },
            { id: '2', text: this.translation.translate('DoasAdminSession.DoaSourceFir') },
            { id: '3', text: this.translation.translate('DoasAdminSession.DoaSourcePoints') },
            { id: '4', text: this.translation.translate('DoasAdminSession.DoaSourcePointRadius') }
        ];
        this.selectedSourceId = 1;
    };
    BilingualRegionEditComponent.prototype.onFirChanged = function (data) {
        if (data.value) {
            if (this.bRegion.region.fir !== data.value) {
                this.bRegion.region.fir = data.value;
                this.resetChangeList();
                this.bRegionForm.markAsDirty();
                this.bilingualRegionRenderMap();
            }
        }
        else {
            this.bRegion.region.fir = '';
            this.resetChangeList();
        }
    };
    BilingualRegionEditComponent.prototype.onSourceChanged = function (data) {
        if (data.value) {
            var diss = this.doaSources.find(function (x) { return x.id === String(data.value); });
            if (diss) {
                this.bRegion.region.source = +diss.id;
                this.selectedSourceId = +diss.id;
                this.bRegion.region.fir = '';
                this.bRegion.region.geography = '';
                this.bRegion.region.geoFeatures = '';
                this.bRegion.region.location = '';
                this.bRegion.region.points = '';
                if (this.bRegion.region.source === data_model_1.DoaRegionSource.PointAndRadius && this.bRegion.region.radius === 0) {
                    this.bRegion.region.radius = 5;
                    var radCtrl = this.$ctrl("doaRadius");
                    radCtrl.setValue(this.bRegion.region.radius);
                    this.bRegion.region.location = '';
                }
                //Delete the list of changes
                this.resetChangeList();
            }
            else {
                this.bRegion.region.source = 1;
                this.selectedSourceId = 1;
            }
        }
        else {
            this.bRegion.region.source = 1;
            this.selectedSourceId = 1;
        }
    };
    BilingualRegionEditComponent.prototype.resetChangeList = function () {
        var ctrlLocation = this.$ctrl('modifyLocation');
        var ctrlRadius = this.$ctrl('modifyRadius');
        ctrlLocation.setValue('');
        ctrlRadius.setValue(5);
        this.activeLocationPoint = { source: '', location: '', inDoa: false, radius: 0 };
        this.selectedDoaLocation = { source: '', location: '', inDoa: false, radius: 5 };
        this.doaChangesList = [];
    };
    BilingualRegionEditComponent.prototype.backToDoasList = function () {
        this.disposeMap();
        this.router.navigate(['/administration']);
    };
    //Map functions
    BilingualRegionEditComponent.prototype.bilingualRegionRenderMap = function () {
        var reg = {
            region: this.bRegion.region,
            changes: this.doaChangesList
        };
        this.renderMap(reg);
    };
    BilingualRegionEditComponent.prototype.initMap = function () {
        var winObj = this.winRef.nativeWindow;
        this.leafletmap = winObj['app'].leafletmap;
        if (this.leafletmap) {
            this.leafletmap.initMap();
        }
    };
    BilingualRegionEditComponent.prototype.mapHealthCheck = function () {
        var _this = this;
        this.dataService.getMapHealth().subscribe(function (response) {
            if (response) {
                _this.initMap();
                _this.mapHealthy = true;
            }
        }, function (error) {
            _this.mapHealthy = false;
            return false;
        }, function () { return _this.renderRegionInMap(_this.model.region.geoFeatures, "Doa"); });
    };
    BilingualRegionEditComponent.prototype.mapAvailable = function () {
        if (this.leafletmap) {
            return this.leafletmap.isReady();
        }
        return false;
    };
    BilingualRegionEditComponent.prototype.renderMap = function (reg) {
        var _this = this;
        this.dataService.getGeoFeatures(reg)
            .subscribe(function (resp) {
            var geoFeatures = resp;
            _this.renderRegionInMap(geoFeatures, "DoaModified");
        }, function (error) {
            _this.toastr.error(_this.translation.translate('DoasAdminSession.RenderMapErrorMsg'), _this.translation.translate('DoasAdminSession.RenderMapErrorTitle'), { 'positionClass': 'toast-bottom-right' });
        });
    };
    BilingualRegionEditComponent.prototype.renderRegionInMap = function (geoFeatures, name) {
        if (geoFeatures !== null) {
            this.leafletmap.addDoa(geoFeatures, name, function () {
                console.log("OK");
            });
        }
        else {
            this.toastr.error(this.translation.translate('DoasAdminSession.RenderMapErrorMsg'), this.translation.translate('DoasAdminSession.RenderMapErrorTitle'), { 'positionClass': 'toast-bottom-right' });
        }
    };
    BilingualRegionEditComponent.prototype.disposeMap = function () {
        if (this.leafletmap) {
            this.leafletmap.dispose();
        }
    };
    BilingualRegionEditComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/bregion/bilingualregion-edit.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], BilingualRegionEditComponent);
    return BilingualRegionEditComponent;
}());
exports.BilingualRegionEditComponent = BilingualRegionEditComponent;
function radiusValidatorFn(maxRadius) {
    return function (c) {
        if (typeof (c.value) === "string") {
            if (!c.value) {
                return { 'required': true };
            }
            if (c.value === "") {
                return { 'required': true };
            }
        }
        if (isANumber(c.value)) {
            if (+c.value < 1 || +c.value > maxRadius) {
                return { 'invalid': true };
            }
            var v = +c.value;
            if (v > maxRadius) {
                return { 'invalid': true };
            }
            return null;
        }
        return { 'invalid': true };
    };
}
function isANumber(str) {
    return !/\D/.test(str);
}
//# sourceMappingURL=bilingualregion-edit.component.js.map
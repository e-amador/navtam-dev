﻿import { Injectable } from '@angular/core';
import { Resolve,  ActivatedRouteSnapshot } from '@angular/router';

import { INsdCategory, ICloneRequest } from './../shared/data.model';
import { DataService } from './../shared/data.service'

@Injectable()
export class ProposalCloneModelResolver implements Resolve<any> {

    constructor( private dataService : DataService ) {
    }

    resolve( route : ActivatedRouteSnapshot ) {
        var cloneReq: ICloneRequest = {
            id: +route.params['id'],
            history: route.params['history'],
        };
        return this.dataService.cloneProposal(cloneReq).map(model => model);
    }
}
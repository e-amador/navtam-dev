"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WindowRef = void 0;
var core_1 = require("@angular/core");
function _window() {
    // return the global native browser window object
    return window;
}
var WindowRef = /** @class */ (function () {
    function WindowRef() {
    }
    Object.defineProperty(WindowRef.prototype, "nativeWindow", {
        get: function () {
            return _window();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(WindowRef.prototype, "appConfig", {
        get: function () {
            return window['app'];
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(WindowRef.prototype, "authData", {
        get: function () {
            return window['app'].authData;
        },
        enumerable: false,
        configurable: true
    });
    WindowRef.prototype.saveAuthData = function (authData) {
        window['app'].authData = authData;
    };
    WindowRef.prototype.zeroPad = function (num, places) {
        var zero = places - num.toString().length + 1;
        return Array(+(zero > 0 && zero)).join("0") + num;
    };
    WindowRef = __decorate([
        core_1.Injectable()
    ], WindowRef);
    return WindowRef;
}());
exports.WindowRef = WindowRef;
//# sourceMappingURL=windowRef.service.js.map
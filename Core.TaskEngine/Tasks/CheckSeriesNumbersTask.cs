﻿using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.TaskEngine.Contracts;
using NavCanada.Core.TaskEngine.Helpers;
using NavCanada.Core.TaskEngine.Mto;
using NDS.Relay;
using System;
using System.Configuration;
using System.Diagnostics;
using System.Linq;

namespace NavCanada.Core.TaskEngine.Tasks.AeroRDS
{
    public class CheckSeriesNumbersTask : IMessageTask
    {
        static CheckSeriesNumbersTask()
        {
            _notifyNumberIncrement = int.Parse(ConfigurationManager.AppSettings["SeriesNumberNotifyIncrement"] ?? "100");
        }

        public void Execute(IMessageTaskContext context)
        {
            var logger = context.ExternalServices.Logger;
            var loggerType = GetType();
            try
            {
                // if this were to fail we don't want to put the system down
                context.Mto.Add(MessageDefs.OverridesCriticalFailureKey, true, DataType.Boolean);

                logger.LogDebug(loggerType, "Check Series Numbers task STARTED excecuting.");

                var sw = Stopwatch.StartNew();

                CheckSeriesNumbers(context, GetMaxSeriesNumber());

                sw.Stop();

                logger.LogDebug(loggerType, $"Check Series Numbers task COMPLETED in {sw.Elapsed}");
            }
            catch (Exception ex)
            {
                logger.LogError(ex, loggerType, "Check Series Numbers task FAILED");
                throw;
            }
        }

        static int GetMaxSeriesNumber()
        {
            return int.Parse(ConfigurationManager.AppSettings["WarnWhenSeriesNumberReaches"] ?? "9000");
        }

        static void CheckSeriesNumbers(IMessageTaskContext context, int maxNumber)
        {
            var uow = context.ClusterStorage.Uow;
            // get all the available series
            var availableSeries = uow.SeriesChecklistRepo.GetAll().Select(e => e.Series).ToList();
            var year = DateTime.UtcNow.Year;
            foreach (var series in availableSeries)
            {
                if (!string.IsNullOrEmpty(series))
                {
                    var seriesNumber = uow.SeriesNumberRepo.GetSeriesNumber(series[0], year);
                    if (seriesNumber != null && seriesNumber.Number > maxNumber)
                    {
                        // series stores the next number
                        var seriesNotams = seriesNumber.Number - 1;

                        // for now only notify once (this is a very rare event)
                        var notifySeries = uow.NotifySeriesRepo.GetById(seriesNumber.Id);
                        if (notifySeries == null)
                        {
                            ReportSeries(context, series, seriesNotams);
                            uow.NotifySeriesRepo.Add(new NotifySeries
                            {
                                Id = seriesNumber.Id,
                                Count = seriesNotams,
                                LastNotified = DateTime.UtcNow
                            });
                            uow.SaveChanges();
                        }
                        else
                        {
                            // notify again again every 100 notams
                            var iterMaxNumber = Math.Max(maxNumber, notifySeries.Count) + _notifyNumberIncrement;
                            if (seriesNotams >= iterMaxNumber)
                            {
                                ReportSeries(context, series, seriesNotams);

                                notifySeries.Count = iterMaxNumber;
                                notifySeries.LastNotified = DateTime.UtcNow;
                                uow.NotifySeriesRepo.Update(notifySeries);

                                uow.SaveChanges();
                            }
                        }
                    }
                }
            }
        }

        static void ReportSeries(IMessageTaskContext context, string series, int number)
        {
            var subject = $"NES Series '{series}' has reached {number} NOTAMS!";
            var messageBody = $"WARNING: {subject}";
            var emailContent = $"<html><body><p>{messageBody}</p></body></html>";

            EmailHelper.QueueEmailMessage(context.ClusterStorage.Queues.TaskEngineQueue, EmailHelper.AdministratorEmail, subject, emailContent);

            var message = new MessageExchangeQueue
            {
                Id = Guid.NewGuid(),
                PriorityCode = "GG",
                ClientAddress = AftnConsts.NOFAddress,
                Recipients = AftnConsts.NOFAddress,
                Status = MessageExchangeStatus.Important,
                ClientType = Domain.Model.Enums.NdsClientType.Aftn,
                Body = messageBody.ToUpper(),
                Created = DateTime.UtcNow,
                LastUpdated = DateTime.UtcNow,
                ClientName = "nof",
                MessageType = MessageExchangeType.Notification,
                Locked = false,
                Inbound = true,
                Read = false
            };

            var uow = context.ClusterStorage.Uow;
            uow.MessageExchangeQueueRepo.Add(message);
            uow.SaveChanges();
        }

        static readonly int _notifyNumberIncrement = 100;
    }
}
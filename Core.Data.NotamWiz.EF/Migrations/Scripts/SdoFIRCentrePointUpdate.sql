﻿
BEGIN TRY
    BEGIN TRANSACTION 

		-- Montreal FIR
		UPDATE [dbo].[SdoCache] 
		SET RefPoint = geography::STPointFromText('POINT(-69.68333333333334 54.6666667)', 4326) 
		WHERE Type = 0 AND Designator = 'CZUL'

		-- Vancouver FIR
		UPDATE [dbo].[SdoCache] 
		SET RefPoint = geography::STPointFromText('POINT(-125.1 52.0833333)', 4326) 
		WHERE Type = 0 AND Designator = 'CZVR'

		-- Edmonton FIR
		UPDATE [dbo].[SdoCache] 
		SET RefPoint = geography::STPointFromText('POINT(-108.18333333333334 69.5)', 4326) 
		WHERE Type = 0 AND Designator = 'CZEG'

		-- Winnipeg FIR
		UPDATE [dbo].[SdoCache] 
		SET RefPoint = geography::STPointFromText('POINT(-93.96666666666667 55.3)', 4326) 
		WHERE Type = 0 AND Designator = 'CZWG'

		-- Toronto FIR
		UPDATE [dbo].[SdoCache] 
		SET RefPoint = geography::STPointFromText('POINT(-80.91666666666667 47.4833333)', 4326) 
		WHERE Type = 0 AND Designator = 'CZYZ'

		-- Moncton FIR
		UPDATE [dbo].[SdoCache] 
		SET RefPoint = geography::STPointFromText('POINT(-62.46666666666667 45.2833333)', 4326) 
		WHERE Type = 0 AND Designator = 'CZQM'

		-- Gander FIR
		UPDATE [dbo].[SdoCache] 
		SET RefPoint = geography::STPointFromText('POINT(-43.45 54.9)', 4326) 
		WHERE Type = 0 AND Designator = 'CZQX'

    COMMIT
END TRY
BEGIN CATCH
    IF @@TRANCOUNT > 0
        ROLLBACK
END CATCH

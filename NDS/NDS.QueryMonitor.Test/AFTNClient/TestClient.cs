﻿using System;
using System.Collections.Generic;
using System.Text;
using NDS.DataBus.Contracts;
using NDS.Common.CommonDefinitions;

namespace NDS.QueryMonitor.Test.AFTNClient
{
    public class TestClient
    {
        private IDataBus _dataBus;
        private IPostQueue _postMessage;

        public void Run(IDataBus dataBus)
        {
            try
            {
                _dataBus = dataBus; 
                
                //pseudo NOTAM request
                var notam = tempGetNotamRequest();

                CreateNOTAMReplyQueue();
                CreateNOTAMRequestQueue();
                SendNOTAMRequestQueueRequest(notam);
                ListenToReplyQueue();
             
            }
            catch (Exception)
            {
                throw;
            }
        }
        private void ListenToReplyQueue()
        {
            _dataBus.ReadFromQueue("CYZZNYXU__ReplyQueue", ProcessQueueMessages);
        }
        private void ProcessQueueMessages(INdsMessage message)
        {
            var body = message.Body;
            var props = message.Properties;
            Console.WriteLine("Body:"+ body + "Props:" + props);
        }
        private void CreateNOTAMReplyQueue()
        {
           _dataBus.CreateQueue("CYZZNYXU__ReplyQueue");
        }
        private void CreateNOTAMRequestQueue()
        {
           _postMessage = _dataBus.CreateQueue("CYZZNYXU__RequestQueue");
        }
        private void SendNOTAMRequestQueueRequest(Dictionary<string, StringBuilder> notam)
        {
            List<Property> props = new List<Property> { Property.Create("replyto", "CYZZNYXU__ReplyQueue") };
            var msg = _dataBus.CreateMessage("TEST", props);

            _postMessage.PostMessage(msg);
        }
        /// <summary>
        /// example AFTN Request Message
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, StringBuilder> tempGetNotamRequest()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("GG CYHQYNYX");
            sb.AppendLine("170306 CYZZNYXU");
            sb.AppendLine("RQN CYHQ D0123/15");
            return GetErrorCode(sb);
        }
        /// <summary>
        /// AFTN message urgency codes
        /// </summary>
        /// <param name="sb"></param>
        /// <returns></returns>
        private Dictionary<string, StringBuilder> GetErrorCode(StringBuilder sb)
        {
            var notam = new Dictionary<string, StringBuilder>();
            switch (sb[0]+sb[1].ToString())
            {
                case "GG":
                    notam.Add(NDSCommonDefintions.AISM, sb);
                    return notam;
                case "SS":
                    notam.Add(NDSCommonDefintions.Distress, sb);
                    return notam;
                case "DD":
                    notam.Add(NDSCommonDefintions.Urgency, sb);
                    return notam;
                case "KK":
                    notam.Add(NDSCommonDefintions.Administrative, sb);
                    return notam;
                case "FF":
                    notam.Add(NDSCommonDefintions.FlightSafetyMessages, sb);
                    return notam;
                default:
                    notam.Add(NDSCommonDefintions.AISM, sb);
                    return notam;
            }
        }
    }
}

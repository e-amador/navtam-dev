﻿using NavCanada.Core.Domain.Model.Entitities;
using System.Data.Entity.ModelConfiguration;

namespace NavCanada.Core.Data.EF.Configurations
{
    public class ProposalAcknowledgementConfig : EntityTypeConfiguration<ProposalAcknowledgement>
    {
        public ProposalAcknowledgementConfig()
        {
            HasKey(e => new { e.ProposalId, e.OrganizationId } );
        }
    }
}

﻿namespace NavCanada.Applications.NotamWiz.Web.Controllers
{
    using System;
    using System.Web.Http;

    public class UTCTimeController : ApiController
    {
        public IHttpActionResult GetUTCTime()
        {
            DateTime date = DateTime.UtcNow;

            var result = new
            {
                year = date.Year,
                month = date.Month,
                day = date.Day,
                hour = date.Hour,
                minute = date.Minute,
                second = date.Second,
                millsecond = date.Millisecond
            };

            return Ok(result);
        }
    }
}

﻿import { Component, OnInit, OnChanges, OnDestroy, Inject, Input } from '@angular/core';
import { TranslationService } from 'angular-l10n';
import { FormGroup, FormBuilder, FormArray, AbstractControl, Validators } from '@angular/forms'
import { IRemainingWidthModel } from '../rsc.model'
import { RscService } from '../rsc.service'

import {
} from '../../../shared/data.model';

import * as moment from 'moment';
import { TOASTR_TOKEN, Toastr } from '../../../../common/toastr.service';

@Component({
    selector: 'rsc-remaining-width-remarks',
    templateUrl: './app/dashboard/nsds/rsc/components/rsc-remainingwidthremarks.component.html'
})
export class RemainingWidthRemarksComponent {
    @Input('form') mainForm: FormGroup = null;
    @Input('model') model: IRemainingWidthModel = null;
    @Input('isReadOnly') isReadOnly: boolean = false;

    frmGroup: FormGroup;
    isFormLoadingData: boolean;

    constructor(
        private fb: FormBuilder,
        public translation: TranslationService,
        private rscService: RscService,
        @Inject(TOASTR_TOKEN) private toastr: Toastr) {
    }

    ngOnInit() {
        this.isFormLoadingData = false;

        this.configureReactiveForm();

    }

    ngOnChanges() {
    }

    ngOnDestroy() {
    }

    $ctrl(name: string): AbstractControl {
        return this.frmGroup.get(name);
    }

    private configureReactiveForm() {
        const frmArrControls = <FormArray>this.mainForm.controls['frmRemainingWidthRemarksArr'];

        this.frmGroup = this.fb.group({
            remainingWidthRemarks: [{ value: this.model.remainingWidthRemarks, disabled: this.isReadOnly }, [Validators.required]],
        });

        frmArrControls.push(this.frmGroup);

    }

    //Export functions
    public isFormValid(): boolean {

        return true;
    }

    public exportComponentInternalData(): IRemainingWidthModel {
        let data: IRemainingWidthModel = {
            parent: this.model.parent,
            remainingWidthRemarks: this.model.remainingWidthRemarks,
        };
        return data;
    }

    public clearValues(): void {
        alert('Clear Component values: RemainingWidthRemarks');
    }


}
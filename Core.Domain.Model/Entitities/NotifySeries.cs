﻿using System;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class NotifySeries
    {
        public int Id { get; set; }
        public int Count { get; set; }
        public DateTime LastNotified { get; set; }
    }
}

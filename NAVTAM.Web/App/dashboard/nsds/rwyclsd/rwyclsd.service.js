"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RwyClsdService = void 0;
var core_1 = require("@angular/core");
var Rx_1 = require("rxjs/Rx");
var http_1 = require("@angular/http");
var xsrf_token_service_1 = require("../../../common/xsrf-token.service");
var windowRef_service_1 = require("../../../common/windowRef.service");
var RwyClsdService = /** @class */ (function () {
    function RwyClsdService(http, winRef, xsrfTokenService) {
        this.http = http;
        this.xsrfTokenService = xsrfTokenService;
        this.app = winRef.nativeWindow['app'];
        if (this.app.antiForgeryTokenValue) {
            this.xsrfTokenService.addForgeryTokenToHeader(this.app.antiForgeryTokenValue);
        }
    }
    RwyClsdService.prototype.getClosureReasons = function () {
        return this.http
            .get(this.app.apiUrl + "closures/get")
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    RwyClsdService.prototype.checkIfBilingualRegion = function (subjectId) {
        return this.http
            .get(this.app.apiUrl + "closures/isinbilingualregion/?subjectId=" + subjectId)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    RwyClsdService.prototype.getDoaLocation = function (doaId) {
        return this.http
            .get(this.app.apiUrl + "doas/GeoJson/?doaId=" + doaId)
            .map(function (response) {
            return response.json();
        })
            .catch(this.handleError);
    };
    RwyClsdService.prototype.getEffectedRunway = function (subject) {
        return this.http
            .get(this.app.apiUrl + "sdosubjects/GetAllRwyByAhpIdAsync/?ahpId=" + subject)
            .map(function (response) {
            return response.json();
        })
            .catch(this.handleError);
    };
    RwyClsdService.prototype.getGeoJsonAhp = function (subject) {
        return this.http
            .get(this.app.apiUrl + "closures/getAhpGeo/?ahpId=" + subject)
            .map(function (response) {
            return response.json();
        })
            .catch(this.handleError);
    };
    RwyClsdService.prototype.convertGeo = function (subject) {
        return this.http
            .get(this.app.apiUrl + "closures/getGeo/?subjectId=" + subject)
            .map(function (response) {
            return response.json();
        })
            .catch(this.handleError);
    };
    RwyClsdService.prototype.handleError = function (error) {
        return Rx_1.Observable.throw(error.statusText);
    };
    RwyClsdService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http, windowRef_service_1.WindowRef, xsrf_token_service_1.XsrfTokenService])
    ], RwyClsdService);
    return RwyClsdService;
}());
exports.RwyClsdService = RwyClsdService;
//# sourceMappingURL=rwyclsd.service.js.map
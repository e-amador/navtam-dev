﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NavCanada.Core.Common.Security;

namespace NAVTAM.Web.Test.Ciphering
{
    [TestClass]
    public class CipherUtilsTest
    {
        [TestMethod]
        public void ConnectionString_decrypt_test()
        {
            var cipheredValue = "ECS:GRTKXFQaGHcUh5zgalHOAaxxTc7MCDK+WRkwNCnGxh0fT2hp2AJm9FwwnNfjlncApenoHUoFw9qqAnPTRE4ZmDYWKPaZ2l/zP8H/d7tZTB8=";
            var expected = "Data Source=localhost;Initial Catalog=NES_PR1;User Id=sa;Password=password;";

            // test with ciphered connection string value
            Assert.AreEqual(CipherUtils.DecryptConnectionString(cipheredValue), expected);

            // test with plain connection string value
            Assert.AreEqual(CipherUtils.DecryptConnectionString(expected), expected);
        }

        [TestMethod]
        public void ConnectionString_encrypt_test()
        {
            var expected = "ECS:GRTKXFQaGHcUh5zgalHOAaxxTc7MCDK+WRkwNCnGxh0fT2hp2AJm9FwwnNfjlncApenoHUoFw9qqAnPTRE4ZmDYWKPaZ2l/zP8H/d7tZTB8=";
            var text = "Data Source=localhost;Initial Catalog=NES_PR1;User Id=sa;Password=password;";

            // test with ciphered connection string value
            Assert.AreEqual(CipherUtils.EncryptConnectionString(text), expected);
        }
    }
}

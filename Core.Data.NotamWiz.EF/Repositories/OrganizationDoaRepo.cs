﻿using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Domain.Model.Entitities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Linq;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Common.Common;
using NavCanada.Core.Data.NotamWiz.EF.Extensions;
using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    public class OrganizationDoaRepo : Repository<OrganizationDoa>, IOrganizationDoaRepo
    {
        public OrganizationDoaRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public async Task<List<Organization>> GetOrganizationWithDoaId(int doaId)
        {
            var ctx = AppDbContext;

            var list = await DbSet
                .Join(ctx.Organizations,
                    s => s.OrganizationId,
                    d => d.Id,
                    (s, d) => new
                    {
                        org = d,
                        odoa = s
                    })
                .Where(r => r.odoa.DoaId == doaId && !r.org.Deleted)
                .Select(r => r.org)
                .ToListAsync();

            return list;
        }

        public Task<int> CountOrganizationsAsync(int doaId)
        {
            return DbSet.CountAsync(e => e.DoaId == doaId);
        }

        public async Task<List<DoaAdminPartialDto>> GetDoaPartials(QueryOptions queryOptions, string separator)
        {
            var ctx = AppDbContext;
            var filter = queryOptions.FilterValue;

            var queryOrgs = await DbSet
                .Join(ctx.Organizations, e => e.OrganizationId, e => e.Id, (od, o) => new { DoaId = od.DoaId, OrgName = o.Name, OrgDeleted = o.Deleted })
                .Where(e => !e.OrgDeleted )
                .ToListAsync();

            var doaMap = new Dictionary<int, List<string>>();
            foreach (var doaOrg in queryOrgs)
            {
                List<string> orgs = null;
                if (!doaMap.TryGetValue(doaOrg.DoaId, out orgs))
                {
                    orgs = new List<string>();
                    doaMap[doaOrg.DoaId] = orgs;
                }
                orgs.Add(doaOrg.OrgName);
            }

            var dtos = await ctx.Doas
                .Where(e => e.DoaType == RegionType.Doa && 
                        (e.Name.Contains(filter) || e.Description.Contains(filter))
                      )
                .Select(d => new DoaAdminPartialDto() { Id = d.Id, Name = d.Name, Description = d.Description })
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            foreach (var dto in dtos)
            {
                List<string> orgs;
                if (doaMap.TryGetValue(dto.Id, out orgs))
                {
                    dto.Organizations = string.Join(separator, orgs);
                    dto.OrgCount = orgs.Count;
                }
            }
            return dtos;
        }

        public Task<List<Doa>> GetOrganizationDoasAsync(int orgId)
        {
            var ctx = AppDbContext;
            return DbSet.Where(od => od.OrganizationId == orgId)
                        .Join(ctx.Doas, od => od.DoaId, d => d.Id, (od, d) => d)
                        .ToListAsync();
        }

        public Task<List<Organization>> GetOrganizationsAssignedToDoaAsync(int doaId)
        {
            var ctx = AppDbContext;
            return DbSet.Where(od => od.DoaId == doaId)
                        .Join(ctx.Organizations, od => od.OrganizationId, o => o.Id, (od, o) => o)
                        .ToListAsync();
        }

        public async Task UpdateOrganizationDoas(int orgId, IEnumerable<int> doaIds)
        {
            var newIds = new HashSet<int>(doaIds);

            var current = await DbSet.Where(e => e.OrganizationId == orgId).ToListAsync();
            var currentIds = new HashSet<int>(current.Select(e => e.DoaId));

            var toAdd = newIds.Where(id => !currentIds.Contains(id))
                              .Select(id => new OrganizationDoa { DoaId = id, OrganizationId = orgId });

            foreach (var od in toAdd)
                Add(od);

            var toDelete = current.Where(od => !newIds.Contains(od.DoaId));

            foreach (var od in toDelete)
                Delete(od);
        }
    }
}

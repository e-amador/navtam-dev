﻿namespace NavCanada.Core.Proxies.SignalrHubProxy
{
    using System.Threading.Tasks;

    public interface ISignalrHubProxy
    {
        Task Notify(string method, params object[] args);

        bool Start();
        bool Restart();
        void Stop();
    }
}

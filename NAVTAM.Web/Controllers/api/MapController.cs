﻿using System.Net;
using System.Web.Http;
using System.Threading.Tasks;
using NavCanada.Core.Domain.Model.Dtos;
using System.IO;

namespace NAVTAM.Controllers.api
{
    [RoutePrefix("api/Map")]
    [Authorize]
    public class MapController : ApiController
    {
        [Route("getconfiguration")]
        [HttpGet]
        public  IHttpActionResult GetConfigurationSettings()
        {
            return Ok(new MapConfigDto());
        }

        [Route("getmaphealth")]
        [HttpGet]
        public async Task<IHttpActionResult> MapHealthAsync()
        {
            var mapconfig = new MapConfigDto();
                        
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(mapconfig.MapHealthApi);
            request.Timeout = 500;
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            var result = "";
            using (HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                result = await reader.ReadToEndAsync();
            }

            //TODO log here if map is down
            return Ok(result);
        }
    }
}
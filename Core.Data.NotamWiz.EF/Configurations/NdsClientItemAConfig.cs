﻿using NavCanada.Core.Domain.Model.Entitities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace NavCanada.Core.Data.EF.Configurations
{
    public class NdsClientItemAConfig : EntityTypeConfiguration<NdsClientItemA>
    {
        public NdsClientItemAConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            //HasRequired(e => e.RelaySubscription)
            //    .WithMany()
            //    .HasForeignKey(e => e.RelaySubscriptionId)
            //.WillCascadeOnDelete(false);

            Property(e => e.ItemA).IsRequired().HasMaxLength(5);
        }
    }
}

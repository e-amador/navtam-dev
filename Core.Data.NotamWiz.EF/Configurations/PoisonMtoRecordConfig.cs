﻿namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    using System.Data.Entity.ModelConfiguration;

    using Domain.Model.Entitities;

    public class PoisonMtoRecordConfig : EntityTypeConfiguration<PoisonMtoRecord>
    {
        public PoisonMtoRecordConfig()
        {
            HasKey(e => e.Id);
        }
    }
}

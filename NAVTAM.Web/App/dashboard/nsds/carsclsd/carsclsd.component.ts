﻿import { Component, OnInit, OnDestroy, Inject, Injector } from '@angular/core'
import { FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms'

import { DataService } from '../../shared/data.service'
import { LocationService } from '../../shared/location.service'
import { CarsClsdService } from './carsclsd.service'
import { ISdoSubject } from '../../shared/data.model'
import { ICarsClsdStatus } from './carsclsd.model'
import { WindowRef } from './../../../common/windowRef.service'

import { TOASTR_TOKEN, Toastr } from './../../../common/toastr.service';

import { LocaleService, TranslationService } from 'angular-l10n';

@Component({
    templateUrl: '/app/dashboard/nsds/carsclsd/carsclsd.component.html'
})
export class CarsClsdComponent implements OnInit, OnDestroy {
    action: string = "";
    token: any = null;
    nsdForm: FormGroup;
    nsdFrmGroup: FormGroup;
    isReadOnly: boolean = false;
    leafletmap: any;
    isBilingualRegion: boolean = false;
    isFormLoadingData: boolean;

    btnMondayClass: string = 'btn-white';

    subjectSelected: ISdoSubject = null;
    subjects: ISdoSubject[];

    carsStatus: ICarsClsdStatus[] = [];
    carsStatusId: number = -1;

    disabledDayCheckbox: boolean = true;

    addReason: boolean = false;
    selectedLanguage: string = '';

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private injector: Injector,
        private fb: FormBuilder,
        private locationService: LocationService,
        private carsClsdService: CarsClsdService,
        private winRef: WindowRef,
        private dataService: DataService,
        public locale: LocaleService,
        public translation: TranslationService ) {

        this.token = this.injector.get('token');
        this.isReadOnly = this.injector.get('isReadOnly');
        this.nsdForm = this.injector.get('form');
        this.action = this.injector.get('type');
    }

    ngOnInit() {
        this.isFormLoadingData = !(!this.token.subjectId);
        const app = this.carsClsdService.app;
        this.selectedLanguage = this.winRef.appConfig.cultureInfo;

        this.leafletmap = app.leafletmap;


        let frmArr = <FormArray>this.nsdForm.controls['frmArr'];
        if (frmArr.length === 0) {
            if (this.isFormLoadingData === false) {
                this.configureReactiveForm();
                this.loadMapLocations(app.doaId, false);
            } else {
                this.configureReactiveFormLoading();
            }
        } else {
            /*
            this is required because of the grouping funtionallity. When the NsdFormGroup 
            was configured and any of items in the group has changed (new set of tokens values)
            the NsdForm Group needs to be updated with the new tokens values */
            this.updateReactiveForm(frmArr);
        }

        this.carsClsdService.getCarsClsdStatus(this.selectedLanguage)
            .subscribe((status: ICarsClsdStatus[]) => {
                this.carsStatus = status;
                if (this.isFormLoadingData) {
                    this.carsStatusId = this.token.carsStatusId;
                    this.setFirstSelectItemInCombo(this.carsStatus, this.token.carsStatusId);

                    this.dataService.getSdoSubject(this.token.subjectId)
                        .subscribe((sdoSubjectResult: ISdoSubject) => {
                            this.subjectSelected = sdoSubjectResult;
                            this.subjects = [];
                            this.subjects.push(sdoSubjectResult);

                            this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);

                            this.loadMapLocations(app.doaId, true);
                            this.updateMap(sdoSubjectResult, true);
                            this.isFormLoadingData = false;

                            let val = this.$ctrl('refreshIcao').value;
                            this.$ctrl('refreshIcao').setValue((+val) + 1);
                            this.$ctrl('refreshIcao').setValidators(refreshIcaoValidator);
                            this.$ctrl('refreshIcao').updateValueAndValidity()
                        });
                }
                else {
                    this.setFirstSelectItemInCombo(this.carsStatus, -1);
                    this.$ctrl('refreshIcao').setValidators(refreshIcaoValidator);
                    this.$ctrl('refreshIcao').updateValueAndValidity()
                }
            });
    }

    ngOnDestroy() {
        //if (this.leafletmap) {
        //    this.leafletmap.dispose();
        //    this.leafletmap = null;
        //}
    }

    public get IsHrsOperationSelected(): boolean {
        return (this.$ctrl('carsStatusId').value === 1);
    }

    private disableDayCheck() {
        if (this.isFormLoadingData) return false;
        if (this.$ctrl('subjectId').value === '') return true;
        if (this.$ctrl('carsStatusId').value !== 1) return true;
        let fc = this.$ctrl('everyday');
        if (fc.value === true) return true;
        return false;
    }

    private get IsAllDaysChecked(): boolean {
        if (this.isFormLoadingData) return false;
        if (this.$ctrl('subjectId').value === '') return false;
        if (this.$ctrl('carsStatusId').value !== 1) return false;
        if (this.$ctrl('daysOfWeek.monday').value === false ||
            this.$ctrl('daysOfWeek.tuesday').value === false ||
            this.$ctrl('daysOfWeek.wednesday').value === false ||
            this.$ctrl('daysOfWeek.thursday').value === false ||
            this.$ctrl('daysOfWeek.friday').value === false ||
            this.$ctrl('daysOfWeek.saturday').value === false ||
            this.$ctrl('daysOfWeek.sunday').value === false) {
            return false;
        }
        return true;
    }
    public selectAllDays() {
        let everydayCtrl = this.$ctrl('everyday');
        this.$ctrl('daysOfWeek').setValue({
            monday: everydayCtrl.value, tuesday: everydayCtrl.value, wednesday: everydayCtrl.value, thursday: everydayCtrl.value, friday: everydayCtrl.value, saturday: everydayCtrl.value, sunday: everydayCtrl.value
        });
        //if (everydayCtrl.value === true) {
        //    this.$ctrl('daysOfWeek').setValue({
        //        monday: true, tuesday: true, wednesday: true, thursday: true, friday: true, saturday: true, sunday: true
        //    });
        //} 
        this.validateAndTrigger();
    }

    public checkboxChanged(controlName: string, checked: boolean) {
        if (this.isFormLoadingData) return;
        const control = this.nsdFrmGroup.get(controlName);
        control.setValue(checked);
        this.$ctrl('everyday').patchValue(this.IsAllDaysChecked);
        this.validateAndTrigger();
    }

    public changeDaylightSavingTime() {
        if (this.isFormLoadingData) return;
        this.validateAndTrigger();
    }

    public validateKeyPressHours(evt: any, ctrlName: string) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var num = +key;
            var pos: number = (<any>evt).currentTarget.selectionStart;
            if (pos === 0) { //val.length
                //First number
                if (key !== '0' && key !== '1' && key !== '2') {
                    evt.returnValue = false;
                    if (evt.preventDefault) evt.preventDefault();
                    return;
                }
            } else if (pos === 1) { //val.length === 1
                //Second number
                const fc = this.$ctrl(ctrlName);
                const val: string = fc.value;
                if (val[0] === '2' && num > 3) {
                    evt.returnValue = false;
                    if (evt.preventDefault) evt.preventDefault();
                    return;
                }
            } else if (pos === 2) { //val.length === 2
                //Third number
                if (num > 5) {
                    evt.returnValue = false;
                    if (evt.preventDefault) evt.preventDefault();
                    return;
                }
            } 
        }
    }

    public carsClosedHoursChange() {
        if (this.isFormLoadingData) return;
        this.validateAndTrigger();
    }

    public carsStatusChanged(e: any) {
        if (this.isFormLoadingData) return;
        if (e.value !== -1) {
            const statusId = +e.value;
            if (statusId === this.carsStatusId) {
                //for some weird reasons select2 templateSelection is called multiples 
                //times after selection
                return;
            }
            if (statusId !== -1) {
                this.carsStatusId = statusId;
                this.$ctrl('carsStatusId').setValue(statusId);
                this.enableDisableCtrls();
                if (!this.isFormLoadingData) {
                    this.validateAndTrigger();
                }
            } else {
                this.$ctrl('reason').patchValue('');
                this.$ctrl('reasonFr').patchValue('');
                this.$ctrl('reason').disable();
                this.$ctrl('reasonFr').disable();
            }
        }
    }

    private enableDisableCtrls() {
        if (this.isFormLoadingData) return;
        this.disabledDayCheckbox = this.disableDayCheck();
        if (this.disabledDayCheckbox) {
            this.$ctrl('startHour').patchValue('');
            this.$ctrl('startHour').disable();
            this.$ctrl('endHour').patchValue('');
            this.$ctrl('endHour').disable();
            this.$ctrl('daylightSavingTime').patchValue(false);
            this.$ctrl('daylightSavingTime').disable();
            this.$ctrl('everyday').patchValue(false);
            this.$ctrl('everyday').disable();
            this.$ctrl('daysOfWeek').patchValue({
                monday: false, tuesday: false, wednesday: false, thursday: false, friday: false, saturday: false, sunday: false
            });
            
        } else {
            this.$ctrl('startHour').enable();
            this.$ctrl('endHour').enable();
            this.$ctrl('daylightSavingTime').enable();
            this.$ctrl('everyday').enable();
            this.$ctrl('everyday').patchValue(true);
            this.$ctrl('daysOfWeek').patchValue({
                monday: true, tuesday: true, wednesday: true, thursday: true, friday: true, saturday: true, sunday: true
            });
        }
        if (this.$ctrl('subjectId').value === '') {
            this.$ctrl('reason').patchValue('');
            this.$ctrl('reasonFr').patchValue('');
            this.$ctrl('reason').disable();
            this.$ctrl('reasonFr').disable();
        } else {
            if (this.$ctrl('carsStatusId').value === -1) {
                this.$ctrl('reason').patchValue('');
                this.$ctrl('reasonFr').patchValue('');
                this.$ctrl('reason').disable();
                this.$ctrl('reasonFr').disable();
            } else {
                this.$ctrl('reason').enable();
                this.$ctrl('reasonFr').enable();
            }
        }
    }

    public addAReason(e: any) {
        const fc = this.$ctrl('addReason');
        if (!fc.value) {
            this.$ctrl('reason').patchValue('');
            this.$ctrl('reasonFr').patchValue('');
            if (this.addReason !== fc.value) {
                this.addReason = fc.value;
                if (this.isFormLoadingData) return;
                this.validateAndTrigger();
            }
        } else {
            this.addReason = fc.value;
            this.setFrmError({ reasonNeeded: { valid: false } });
        }

    }

    public changeReason() {
        if (this.isFormLoadingData) return;
        this.validateAndTrigger();
    }

    private updateSubjectSelected(sdoSubject: ISdoSubject) {
        const subjectId = this.$ctrl('subjectId').value;
        if (subjectId === sdoSubject.id) {
            //for some weird reasons select2 templateSelection is called multiples 
            //times after selection
            return;
        }
        if (sdoSubject.id !== subjectId) {
            const subjectCtrl = this.$ctrl('subjectId');
            subjectCtrl.setValue(sdoSubject.id);
            subjectCtrl.markAsDirty();
        }
        this.subjectSelected = sdoSubject;
        if (!sdoSubject.subjectGeo) {
            this.dataService.getSdoSubject(sdoSubject.id)
                .subscribe((sdoSubjectResult: ISdoSubject) => {
                    this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);
                    this.updateMap(sdoSubjectResult, true);
                    this.setInitialStatus();
                    this.validateAndTrigger();
                });
        } else {
            this.setBilingualRegion(sdoSubject.isBilingualRegion);
            //Here the code to upload
        }

    }

    private configureReactiveForm() {
        const frmArrControls = <FormArray>this.nsdForm.controls['frmArr'];
        this.disabledDayCheckbox = true;
        this.nsdFrmGroup = this.fb.group({
            subjectId: ['', [Validators.required]],
            subjectLocation: '',
            carsStatusId: [{ value: -1, disabled: true }],
            addReason: this.addReason,
            everyday: [{ value: false, disabled: true }],
            daysOfWeek: this.fb.group({
                monday: [{ value: false, disabled: this.disabledDayCheckbox }],
                tuesday: [{ value: false, disabled: this.disabledDayCheckbox }],
                wednesday: [{ value: false, disabled: this.disabledDayCheckbox }],
                thursday: [{ value: false, disabled: this.disabledDayCheckbox }],
                friday: [{ value: false, disabled: this.disabledDayCheckbox }],
                saturday: [{ value: false, disabled: this.disabledDayCheckbox }],
                sunday: [{ value: false, disabled: this.disabledDayCheckbox }],
            }),
            reason: [{ value: '', disabled: true }],
            reasonFr: [{ value: '', disabled: true }],
            startHour: [{ value: '', disabled: true }],
            endHour: [{ value: '', disabled: true }],
            daylightSavingTime: [{ value: false, disabled: true }],
            refreshIcao: '0',
        });
        frmArrControls.push(this.nsdFrmGroup);
    }

    private configureReactiveFormLoading() {
        const frmArrControls = <FormArray>this.nsdForm.controls['frmArr'];
        let disableCtrl: boolean = (this.isReadOnly) ? true : (this.token.carsStatusId === 1) ? false : true;
        this.disabledDayCheckbox = (this.isReadOnly) ? true : (this.token.carsStatusId !== 1) ? true : false;
        if (this.token.reason && this.token.reason.length > 0) this.addReason = true;
        if (this.token.reasonFr && this.token.reasonFr.length > 0) this.addReason = true;

        this.nsdFrmGroup = this.fb.group({
            subjectId: [this.token.subjectId, [Validators.required]],
            subjectLocation: this.token.subjectLocation,
            carsStatusId: [{ value: this.token.carsStatusId, disabled: this.isReadOnly }],
            addReason: [{ value: this.addReason, disabled: this.isReadOnly }],
            everyday: [{ value: this.token.everyday, disabled: disableCtrl }],
            daysOfWeek: this.fb.group({
                monday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.monday : true, disabled: this.disabledDayCheckbox }],
                tuesday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.tuesday : true, disabled: this.disabledDayCheckbox }],
                wednesday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.wednesday : true, disabled: this.disabledDayCheckbox }],
                thursday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.thursday : true, disabled: this.disabledDayCheckbox }],
                friday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.friday : true, disabled: this.disabledDayCheckbox }],
                saturday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.saturday : true, disabled: this.disabledDayCheckbox }],
                sunday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.sunday : true, disabled: this.disabledDayCheckbox }],
            }),
            reason: [{ value: this.token.reason || '', disabled: this.isReadOnly }],
            reasonFr: [{ value: this.token.reasonFr || '', disabled: this.isReadOnly }],
            startHour: [{ value: this.token.startHour || '', disabled: disableCtrl }],
            endHour: [{ value: this.token.endHour || '', disabled: disableCtrl }],
            daylightSavingTime: [{ value: this.token.daylightSavingTime, disabled: disableCtrl }],
            refreshIcao: '0',
        });
        frmArrControls.push(this.nsdFrmGroup);
    }

    private updateReactiveForm(formArr: FormArray) {
        this.nsdFrmGroup = <FormGroup>formArr.controls[0];
        this.disabledDayCheckbox = (this.isReadOnly) ? true : (this.token.carsStatusId !== 1) ? true : false;
        if (this.token.reason && this.token.reason.length > 0) this.addReason = true;
        if (this.token.reasonFr && this.token.reasonFr.length > 0) this.addReason = true;

        //It is better to update only those controls that tokens values have changed.
        this.updateFormControlValue("subjectId", this.token.subjectId);
        this.updateFormControlValue("subjectLocation", this.token.subjectLocation);
        this.updateFormControlValue("carsStatusId", this.token.carsStatusId);
        this.updateFormControlValue("addReason", this.addReason);
        this.updateFormControlValue("everyday", this.token.everyday);
        this.updateFormControlValue("daysOfWeek.monday", (!this.token.everyday) ? this.token.daysOfWeek.monday : true);
        this.updateFormControlValue("daysOfWeek.tuesday", (!this.token.everyday) ? this.token.daysOfWeek.tuesday : true);
        this.updateFormControlValue("daysOfWeek.wednesday", (!this.token.everyday) ? this.token.daysOfWeek.wednesday : true);
        this.updateFormControlValue("daysOfWeek.thursday", (!this.token.everyday) ? this.token.daysOfWeek.thursday : true);
        this.updateFormControlValue("daysOfWeek.friday", (!this.token.everyday) ? this.token.daysOfWeek.friday : true);
        this.updateFormControlValue("daysOfWeek.saturday", (!this.token.everyday) ? this.token.daysOfWeek.saturday : true);
        this.updateFormControlValue("daysOfWeek.sunday", (!this.token.everyday) ? this.token.daysOfWeek.sunday : true);
        this.updateFormControlValue("reason", this.token.reason || '');
        this.updateFormControlValue("reasonFr", this.token.reasonFr || '');
        this.updateFormControlValue("startHour", this.token.startHour || '');
        this.updateFormControlValue("endHour", this.token.endHour || '');
        this.updateFormControlValue("daylightSavingTime", this.token.daylightSavingTime);
        //this.updateFormControlValue("refreshIcao", '0');
    }

    private updateFormControlValue(controlName: string, value) {
        let control = this.nsdFrmGroup.get(controlName);
        if (control && control.value !== value) {
            control.setValue(value);
        }
    }

    //Validation functions
    public validateSubjectId() {
        const fc = this.$ctrl('subjectId');
        return fc.value || this.nsdFrmGroup.pristine;
    }

    public validateCarsStatusId() {
        const fc = this.$ctrl('carsStatusId');
        return (fc.value > -1) || this.nsdFrmGroup.pristine;
    }

    public validateHoursEmptyCtrls(ctrlName: string) {
        if (this.$ctrl('carsStatusId').value === 1) {
            const fc = this.$ctrl(ctrlName);
            if (!fc.value || fc.value === null || fc.value === '' || fc.value.length != 4) return false;
        }
        return true;
    }

    public validateReasonCtrls(ctrlName: string) {
        if (this.$ctrl(ctrlName).value.length < 1) return false;
        return true;
    }

    private setFrmError(err: any) {
        if (err !== null) {
            this.nsdForm.controls['frmArr'].setErrors(err);
            this.$ctrl('refreshIcao').setValue(0);
            //this.nsdForm.controls['frmArr'].markAsDirty();
        } else {
            this.nsdForm.controls['frmArr'].setErrors(null);
            let val = this.$ctrl('refreshIcao').value;
            this.$ctrl('refreshIcao').setValue((+val) + 1);
            this.nsdForm.controls['frmArr'].markAsDirty();
        }
    }

    public validateAndTrigger() {
        if (this.$ctrl('subjectId').value === '') {
            this.setFrmError({ subjectIdNeeded: { valid: false } });
            return;
        }
        if (this.$ctrl('carsStatusId').value === -1) {
            this.setFrmError({ statusIdNeeded: { valid: false } });
            return;
        }
        if (this.$ctrl('carsStatusId').value === 1) {
            //Validate the Hours and Days of the Week
            let fc = this.$ctrl('startHour');
            if (!fc.value || fc.value === null || fc.value === '' || fc.value.length !== 4) {
                this.setFrmError({ startHourNeeded: { valid: false } });
                return;
            }
            fc = this.$ctrl('endHour');
            if (!fc.value || fc.value === null || fc.value === '' || fc.value.length !== 4) {
                this.setFrmError({ endHourNeeded: { valid: false } });
                return;
            }
            fc = this.$ctrl('everyday');
            if (fc.value === false) {
                //Validate each day
                if (this.$ctrl('daysOfWeek.monday').value === false &&
                    this.$ctrl('daysOfWeek.tuesday').value === false &&
                    this.$ctrl('daysOfWeek.wednesday').value === false &&
                    this.$ctrl('daysOfWeek.thursday').value === false &&
                    this.$ctrl('daysOfWeek.friday').value === false &&
                    this.$ctrl('daysOfWeek.saturday').value === false &&
                    this.$ctrl('daysOfWeek.sunday').value === false) {
                    this.setFrmError({ dayOfWeekNeeded: { valid: false } });
                    return;
                }
            }
        }
        if (this.addReason) {
            if (!this.$ctrl('reason').value || this.$ctrl('reason').value.length < 1) {
                this.setFrmError({ reasonNeeded: { valid: false } });
                return;
            }
            if (this.isBilingualRegion) {
                if (!this.$ctrl('reasonFr').value || this.$ctrl('reasonFr').value.length < 1) {
                    this.setFrmError({ reasonNeeded: { valid: false } });
                    return;
                }
            }
        }
        this.setFrmError(null);
    }

    //Miscelaneas functions
    $ctrl(name: string): AbstractControl {
        return this.nsdFrmGroup.get(name);
    }

    private setInitialStatus() {
        this.carsStatusId = -1;
        this.$ctrl('carsStatusId').patchValue(this.carsStatusId);
        this.$ctrl('carsStatusId').enable();


        this.$ctrl('startHour').patchValue('');
        this.$ctrl('startHour').disable();
        this.$ctrl('endHour').patchValue('');
        this.$ctrl('endHour').disable();
        this.$ctrl('daylightSavingTime').patchValue(false);
        this.$ctrl('daylightSavingTime').disable();
        this.$ctrl('everyday').patchValue(false);
        this.$ctrl('everyday').disable();

        this.$ctrl('daysOfWeek').patchValue({
            monday: false, tuesday: false, wednesday: false, thursday: false, friday: false, saturday: false, sunday: false
        });

        this.$ctrl('reason').patchValue('');
        this.$ctrl('reasonFr').patchValue('');
        this.$ctrl('reason').disable();
        this.$ctrl('reasonFr').disable();

        this.disabledDayCheckbox = true;

    }

    public carsStatusOptions = {
        width: "100%",
        placeholder: {
            id: '-1',
            text: this.translation.translate('Nsd.CarsStatusPlaceholder')
        }
    }

    public sdoSubjectWithCarsOptions(): any {
        const self = this;
        const app = this.carsClsdService.app;
        return {
            language: {
                inputTooShort: function (args) {
                    return self.translation.translate('Schedule.InputTooShort');
                },
            },
            minimumInputLength: 2,
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.SdoSubjectPlaceholder')
            },
            ajax: {
                type: 'GET',
                url: app.apiUrl + "carsclsd/filterwithcars",
                dataType: 'json',
                delay: 500,
                headers: {
                    "RequestVerificationToken": app.antiForgeryTokenValue
                },
                data: function (parms, page) {
                    return {
                        Query: parms.term,
                        Size: 200,
                        doaId: app.doaId //set by the app controller
                    }
                },
                processResults: function (data) {
                    return {
                        results: data
                    }
                },
                cache: true
            },
            templateResult: function (data: any) {
                if (data.loading) {
                    return data.text;
                }
                return self.dataService.getSdoExtendedName(data);
            },
            templateSelection: function (selection: ISdoSubject) {
                if (selection && selection.id === "-1") {
                    return selection.text;
                }

                self.updateSubjectSelected(selection);
                return self.dataService.getSdoExtendedName(selection);
            },
        }
    }

    private updateMap(sdoSubject: ISdoSubject, acceptSubjectLocation: boolean) {
        this.leafletmap.clearFeaturesOnLayers(['DOA']);
        if (sdoSubject.subjectGeoRefPoint) {
            const geojson = JSON.parse(sdoSubject.subjectGeoRefPoint);
            if (geojson.features.length === 1) {
                const subLocation = this.leafletmap.geojsonToTextCordinates(geojson);
                const coordinates = geojson.features[0].geometry.coordinates;
                const markerGeojson = acceptSubjectLocation ? geojson : this.getGeoJsonFromLocation(this.token.subjectLocation);
                if (acceptSubjectLocation) {
                    this.nsdFrmGroup.patchValue({
                        subjectLocation: subLocation,
                        latitude: coordinates[1],
                        longitude: coordinates[0],
                    }, { onlySelf: true, emitEvent: false });
                }
                this.leafletmap.addMarker(geojson, sdoSubject.name, () => {
                    this.leafletmap.addFeature(sdoSubject.subjectGeo, sdoSubject.name, () => {
                        if (markerGeojson) {
                            this.leafletmap.setNewMarkerLocation(markerGeojson);
                        }
                    });
                });
            }
        }

    }

    private setFirstSelectItemInCombo(items: any, itemId: any, insertUndefinedAtZero: boolean = true) {
        if (this.isFormLoadingData) {
            const index = items.findIndex(x => x.id === itemId);
            if (index > 0) { //move selected item to position 0
                items.splice(0, 0, items.splice(index, 1)[0]);
            }
        } else { //insert undefined item at position 0
            if (insertUndefinedAtZero) {
                items.unshift({
                    id: -1,
                    text: '',
                    textFr: '',
                });
            }
        }
    }

    private loadMapLocations(doaId: number, loadingFromProposal: boolean) {
        const self = this;
        this.dataService.getDoaLocation(doaId)
            .subscribe((geoJsonStr: any) => {
                const interval = window.setInterval(function () {
                    if (self.leafletmap && self.leafletmap.isReady()) {
                        window.clearInterval(interval);
                        self.leafletmap.addDoa(geoJsonStr, 'DOA');
                        if (loadingFromProposal) {
                            self.updateMap(self.subjectSelected, false);
                        }
                    };
                }, 100);
            }, (error: any) => {
                this.toastr.error(this.translation.translate('Nsd.DOAArea') + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    private setBilingualRegion(value: boolean) {
        this.isBilingualRegion = value;
        if (this.isBilingualRegion) {
            if (!this.isReadOnly) {
                //Do something
            }
        } else {
            if (!this.isReadOnly) {
                //Do something
            }
        }
    }

    private getGeoJsonFromLocation(value: string) {
        const location = this.locationService.parse(value);
        return location ? this.leafletmap.latitudeLongitudeToGeoJson(location.latitude, location.longitude) : null;
    }

}

function isANumber(str: string): boolean {
    return !/\D/.test(str);
}

function refreshIcaoValidator(c: AbstractControl): { [key: string]: boolean } | null {
    const val = +c.value;
    if (val === 0) {
        return { 'required': true }
    }
    return null;
}

﻿import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Localization, LocaleService, TranslationService } from 'angular-l10n';

declare var $: any;

@Component({
    selector: 'my-app',
    //template: `<router-outlet></router-outlet>`
    templateUrl: '/app/admin/admin.component.html'
})
export class AdminComponent extends Localization implements OnInit, AfterViewInit {

    constructor(public locale: LocaleService, public translation: TranslationService) {
        super(locale, translation);
        this.locale.setDefaultLocale(window['app'].cultureInfo, "CA");
        this.locale.defaultLocaleChanged.subscribe((item: string) => { this.onLanguageCodeChangedDataRecieved(item); });
    }

    ngOnInit() {
    }   

    public ChangeCulture(language: string, country: string, currency: string) {
        this.locale.setDefaultLocale(language, country);
        this.locale.setCurrentCurrency(currency);
    }

    public ChangeCurrency(currency: string) {
        this.locale.setCurrentCurrency(currency);
    }

    private onLanguageCodeChangedDataRecieved(item: string) {
        console.log('onLanguageCodeChangedDataRecieved App');
        console.log(item);
    }

    ngAfterViewInit() {
        setTimeout(function() {
            $('.loader-overlay').addClass('loaded');
            $('body > section').animate({
                opacity: 1,
            }, 200);
        }, 400);
    } 
}
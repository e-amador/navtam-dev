﻿namespace NavCanada.Core.TaskEngine.Test.Tasks.SubmitNotamProposal
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;

    using Domain.Model.Entitities;
    using MessageTask;
    using TaskEngine.Tasks.CommonTasks;
    using Test;

    [TestClass]
    public class SubmitNotamProposalSignalrTaskTest : BaseTaskEngineTest
    {
        [TestInitialize]
        public override void Initialize()
        {
            base.Initialize();
        }

        [TestMethod]
        public void SubmitNotamProposal_ShouldInvokeSignalrHub_WhenNotamSuccessfullySubmitted()
        {
            var task = new WorkFlowSignalrTask();

            //set MessageContext with the NotamProposal submitted by the previous task: SubmiNotamProposalTask
            var context = new MessageTaskContext
            {
                NotamProposal = new NotamProposal(),
                ExternalServices = ExternalServices
            };

            //Configure SignalR Hub Proxy 
            var isCalled = false;
            SignalrHubProxyServiceMock.Setup(x => x.Notify(It.IsAny<string>(), It.IsAny<object[]>())).Callback(() =>
            {
                isCalled = true;
            });

            task.Execute(context);

            //check that notify method was called;
            Assert.IsTrue(isCalled);
        }

    }
}

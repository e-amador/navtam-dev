﻿using NavCanada.Core.Domain.Model.Enums;
using System;

namespace NAVTAM.ViewModels
{
    public class NofStatsViewModel
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public AggregatePeriod AggregatePeriod { get; set; }
    }
}
﻿using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AeroRdsProxy.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NAVTAM.Helpers
{
    public class SdoCacheBroker
    {
        public SdoCacheBroker(IUow uow)
        {
            Uow = uow;
        }

        public IUow Uow { get; }

        public static Task<Aerodrome> GetAerodromeCache(IUow uow, SdoCache subject, bool runways, bool taxiways)
        {
            var sdoBroker = new SdoCacheBroker(uow);
            return sdoBroker.GetAerodrome(subject, runways, taxiways);
        }

        public async Task<Aerodrome> GetAerodrome(SdoCache sdoCache, bool runways, bool taxiways)
        {
            var aerodrome = Deserialize<Aerodrome>(sdoCache.Attributes);
            if (aerodrome == null)
                return null;

            if (runways)
                aerodrome.Runways = await GetChildren(sdoCache.Id, SdoEntityType.Runway, GetRunway); 

            if (taxiways)
                aerodrome.Taxiways = await GetChildren(sdoCache.Id, SdoEntityType.Taxiway, GetTaxiway);

            return aerodrome;
        }

        private async Task<Runway> GetRunway(SdoCache sdoCache)
        {
            var runway = Deserialize<Runway>(sdoCache.Attributes);

            if (runway != null)
                runway.RunwayDirections = await GetChildren(sdoCache.Id, SdoEntityType.RunwayDirection, GetRunwayDirection);

            return runway;
        }

        private Task<Taxiway> GetTaxiway(SdoCache sdoCache) => Task.FromResult(Deserialize<Taxiway>(sdoCache.Attributes));

        private async Task<RunwayDirection> GetRunwayDirection(SdoCache sdoCache)
        {
            var rwd = Deserialize<RunwayDirection>(sdoCache.Attributes);

            if (rwd != null)
                rwd.RunwayDirectionDeclaredDistances = await GetChildren(sdoCache.Id, SdoEntityType.RunwayDirectionDeclaredDistance, GetRunwayDirectionDeclaredDistance);

            return rwd;
        }

        private Task<RunwayDirectionDeclaredDistance> GetRunwayDirectionDeclaredDistance(SdoCache sdoCache) =>
            Task.FromResult(Deserialize<RunwayDirectionDeclaredDistance>(sdoCache.Attributes));

        private async Task<List<T>> GetChildren<T>(string parentId, SdoEntityType type, Func<SdoCache, Task<T>> LoadChild)
        {
            var sdoChildren = await Uow.SdoCacheRepo.GetChildrenByTypeAsync(parentId, type);

            var children = new List<T>();
            foreach (var sdoChild in sdoChildren)
            {
                var child = await LoadChild(sdoChild);
                children.Add(child);
            }

            return children;
        }

        private async Task<string> GetSdoObjectAttributes(string id) => (await Uow.SdoCacheRepo.GetByIdAsync(id))?.Attributes;

        private async Task<T> GetCachedObject<T>(string id) where T: class
        {
            var attributes = await GetSdoObjectAttributes(id);
            return attributes != null ? Deserialize<T>(attributes) : null;
        }

        static T Deserialize<T>(string json) => JsonConvert.DeserializeObject<T>(json, _serializerSettings);

        static JsonSerializerSettings _serializerSettings = new JsonSerializerSettings { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() };
    }
}
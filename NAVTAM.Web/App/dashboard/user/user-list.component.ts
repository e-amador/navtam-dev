﻿import { Component, OnInit, OnDestroy, Inject, HostListener, ViewChild } from '@angular/core'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';
import { ActivatedRoute, Router } from '@angular/router'
import { DataService } from '../shared/data.service'
import { MemoryStorageService } from '../shared/mem-storage.service'
import { WindowRef } from '../../common/windowRef.service'
import * as moment from 'moment';
import { LocaleService, TranslationService } from 'angular-l10n';
import { ProposalFilterComponent } from './proposal-filter.component';

import {
    IProposal,
    IQueryOptions,
    INsdCategory,
    IPaging,
    ITableHeaders,
    ProposalStatus,
    ProposalType,
    NsdCategory
} from '../shared/data.model'

declare var $: any;

const syncFrecuency: number = 30; //seconds

@Component({
    templateUrl: '/app/dashboard/user/user-list.component.html',
    styleUrls: ['app/dashboard/user/user-list.component.css']
})
export class UserListComponent implements OnInit, OnDestroy {
    @ViewChild(ProposalFilterComponent) filter: ProposalFilterComponent;

    proposals: IProposal[];
    proposalHistory: IProposal[];
    queryOptions: IQueryOptions;
    selectedProposal: IProposal;
    lastSyncTime?: moment.Moment = null;
    paging: IPaging = {
        currentPage: 1,
        pageSize: 0,
        totalCount: 0,
        totalPages: 0,
    };
    filterPaging: IPaging = {
        currentPage: 1,
        pageSize: 0,
        totalCount: 0,
        totalPages: 0,
    };
    filterTotalPages: number = 0;
    totalPages: number = 0;

    txtModify: string;
    txtWidthdrawn: string;

    currentCategoryId: number;
    currentSubCategoryId: number;
    categoriesData: Array<any>;
    categoryStartValue: string;
    subCategoryStartValue: string;

    parentCategories: Array<any> = [];
    childCategories: Array<any> = [];

    itemsPerPageData: Array<any>;
    txtWidthdrawModifyButtom: string;
    iconWidthdrawModifyButtom: string;
    loadingData: boolean = false;
    tableHeaders: ITableHeaders;

    frsPrevKeyCodeEvent: any;
    sndPrevKeyCodeEvent: any;

    isFilterApplied: boolean = false;
    filterPannelOpen: boolean = false;
    checkPeriodicalHandler: any = null;
    intervalHandler = null;

    expiringCount: number = 0;
    runExpiringBellAnimation: boolean = false;
    expiringApplied: boolean = false;
    isOrgAdmin: boolean = false;
    isLeavingForm: boolean = false;

    alarms = [
        { id: 0, name: 'Bell Default', ext: 'wav' },
        { id: 1, name: 'Chime Default', ext: 'wav' },
        { id: 2, name: 'Bell Sound', ext: 'mp3' },
        { id: 3, name: 'Police Warning', ext: 'mp3' },
        { id: 4, name: 'Burglary Alarm', ext: 'mp3' },
        { id: 5, name: 'Security Alarm', ext: 'mp3' },
    ];

    currentEstimatedQueueSound: any;
    audioElem: any = null;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private route: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService,
        private winRef: WindowRef) {
        this.tableHeaders = {
            sorting:
                [
                    { columnName: 'NotamId', direction: 0 },
                    { columnName: 'CategoryId', direction: 0 },
                    { columnName: 'ItemA', direction: 0 },
                    { columnName: 'SiteID', direction: 0 },
                    { columnName: 'ItemE', direction: 0 },
                    { columnName: 'StartActivity', direction: 0 },
                    { columnName: 'EndValidity', direction: 0 },
                    { columnName: 'Estimated', direction: 0 },
                    { columnName: 'Received', direction: -1 },
                    { columnName: 'Operator', direction: 0 },
                    { columnName: 'Originator', direction: 0 },
                    { columnName: 'ModifiedByUsr', direction: 0 },
                    { columnName: 'Status', direction: 0 },
                ],
            itemsPerPage:
                [
                    { id: '10', text: '10' },
                    { id: '25', text: '25' },
                    { id: '50', text: '50' },
                    { id: '100', text: '100' },
                    { id: '1000', text: '1000' },
                ]
        };

        this.setWidthrawModifyButtonApparenance(null);
        this.paging.pageSize = +this.tableHeaders.itemsPerPage[2].id;
        this.filterPaging.pageSize = this.paging.pageSize;
        this.queryOptions = {
            page: 1,
            pageSize: this.paging.pageSize,
            sort: "_" + this.tableHeaders.sorting[8].columnName //Default Received Ascending
        }

        this.txtModify = this.translation.translate('ActionButtons.BtnModify');
        this.txtWidthdrawn = this.translation.translate('ActionButtons.BtnWidthdraw');
        this.txtWidthdrawModifyButtom = this.txtModify;

        this.isOrgAdmin = this.winRef.appConfig.isOrgAdmin;
    }

    ngOnInit() {
        //If we want to change the default, we need to work here
        this.currentEstimatedQueueSound = this.alarms[0];

        this.categoriesData = this.route.snapshot.data['categories'];
        this.addAllToComboFilter(this.parentCategories);
        for (let i = 0; i < this.categoriesData.length; i++) {
            const category = this.categoriesData[i];
            this.addItemToComboFilter(this.parentCategories, category);
        }

        var faKey = this.memStorageService.get(this.memStorageService.FILTER_APPLIED_KEY);
        if (faKey) this.isFilterApplied = faKey;
        if (this.isFilterApplied) {
            this.filter.loadFilterState();
        }

        //Set latest combo box status
        let catId = this.memStorageService.get(this.memStorageService.CATEGORY_ID_KEY);
        let subCatId = this.memStorageService.get(this.memStorageService.SUB_CATEGORY_ID_KEY);

        this.categoryStartValue = catId ? catId : this.parentCategories[0].id;
        this.currentCategoryId = +this.categoryStartValue;

        if (subCatId) {
            this.childCategories = this.addChildCategoryToComboFilter(this.childCategories, this.currentCategoryId);
            this.currentSubCategoryId = subCatId;
            this.subCategoryStartValue = subCatId;
        }

        //Set latest query options
        const queryOptions = this.memStorageService.get(this.memStorageService.PROPOSAL_QUERY_OPTIONS_KEY);
        if (queryOptions) {
            this.queryOptions = queryOptions;
        }

        if (this.isFilterApplied) {
            this.filter.runFilter(false);
        } else {
            this.getProposalsByCategory(this.queryOptions.page);
        }
        this.checkProposalStatus();
        this.getExpiringQueueStats();
        this.checkExpireCounter();
    }

    ngOnDestroy() {
        if (this.checkPeriodicalHandler) {
            clearInterval(this.checkPeriodicalHandler);
        }
        if (this.intervalHandler) {
            window.clearInterval(this.intervalHandler);
        }

    }

    ngAfterViewInit() {
        this.filter.parent = this;
    }

    get langCulture() {
        return (window["app"].cultureInfo || "en").toUpperCase();
    }

    changeRoute(routeValue) {
        this.isLeavingForm = true;
        this.router.navigate(routeValue).catch(() => {
            this.isLeavingForm = false;
            this.toastr.error("This action is not allowed at this time.", "Action failed!", { 'positionClass': 'toast-bottom-right' });
        });
    }

    getItemEString(proposal: IProposal, history: boolean): string {
        const maxsize = history ? 90 : 110;
        var result = proposal.itemE.substring(0, maxsize);
        if (proposal.itemE.length > maxsize) result += '...';
        return result;
    }

    getOriginatorString(proposal: IProposal, history: boolean): string {
        const maxsize = history ? 10 : 10;
        var result = proposal.originator.substring(0, maxsize);
        if (proposal.originator.length > maxsize) result += '...';
        return result;
    }

    getOperatorString(proposal: IProposal): string {
        if (proposal.modifiedByUsr === null || proposal.modifiedByUsr.length === 0) return proposal.operator;
        else return proposal.modifiedByUsr;
    }

    getApproverString(proposal: IProposal): string {
        if (proposal.status === ProposalStatus.Disseminated ||
            proposal.status === ProposalStatus.DisseminatedModified ||
            proposal.status === ProposalStatus.Rejected) return proposal.modifiedByUsr;
        else return '';
    }

    private checkExpireCounter() {
        const self = this;
        this.intervalHandler = window.setInterval(function () {
            self.getExpiringQueueStats.call(self);
        }, syncFrecuency * 1000);
    }

    getExpiringQueueStats() {
        try {
            this.dataService.getExpiringTotal()
                .subscribe((count: any) => {
                    const self = this;
                    let shouldPlaySound = false;

                    if (count != this.expiringCount) {
                        if (count > this.expiringCount) {
                            shouldPlaySound = true;
                            self.audioElem = document.getElementById('estimated-queue-sound');
                            self.audioElem.load();
                            self.audioElem.play();
                        }

                        this.expiringCount = count;

                        self.runExpiringBellAnimation = true;
                        setTimeout(function () {
                            self.runExpiringBellAnimation = false;
                            if (shouldPlaySound) {
                                self.audioElem.pause();
                                self.audioElem.currentTime = 0;
                                self.audioElem = null;
                            }
                        }, 10000);

                        if (this.expiringApplied)
                            this.getProposalsByCategory(this.queryOptions.page);
                    }
                    if ((this.expiringCount === 0 && this.expiringApplied) ) {
                        this.executeFilter();
                    }
                });
        }
        catch (e) {
        }
    }

    executeFilter(): void {
        this.expiringApplied = !this.expiringApplied;
        if (this.expiringApplied) {
            this.getProposalsByCategory(this.queryOptions.page);
        } else {
            this.changeCategory(this.parentCategories[0].id);
        }
    }

    runCloneAction(): void {
        if (this.selectedProposal) {
            this.isLeavingForm = true;
            this.router.navigate(["/dashboard/clone", this.selectedProposal.id, this.isFilterApplied]);
        }
    }

    runClearFilter($event: any): void {
        this.filter.clearFilter($event, false);
        this.changeCategory(this.parentCategories[0].id);
    }

    @HostListener('window:keyup', ['$event'])
    onKeyup($event: any) {
        switch ($event.code) {
            case "ArrowUp"://NAVIGATE TABLE ROW UP
                if (this.selectedProposal.index > 0) {
                    this.selectProposal(this.proposals[this.selectedProposal.index - 1]);
                }
                break;
            case "ArrowDown": //NAVIGATE TABLE ROW DOWN
                if (this.selectedProposal.index < this.proposals.length - 1) {
                    this.selectProposal(this.proposals[this.selectedProposal.index + 1]);
                }
                break;
            case "KeyV"://ALT-V: REVIEW
                if ($event.altKey && this.selectedProposal) {
                    this.reviewProposal();
                }
                break;
            case "Enter": //ENTER: MODIFY/WIDTHRAW
                if (!this.isModifyButtonDisabled() && this.selectedProposal) {
                    this.router.navigate(["/dashboard/edit", this.selectedProposal.id]);
                }
                break;
            case "KeyM": //ALT-M: MODIFY/WIDTHRAW
                if ($event.altKey) {
                    if (!this.isModifyButtonDisabled() && this.selectedProposal) {
                        this.router.navigate(["/dashboard/edit", this.selectedProposal.id]);
                    }
                }
                break;
            case "KeyH"://ALT-H: HISTORY
                if ($event.altKey && this.selectedProposal && !this.isHistoryButtonDisabled()) {
                    this.showHistory(this.selectedProposal.id);
                }
                break;
            case "KeyZ"://ALT-Z: DISCARD
                debugger;
                if ($event.altKey && this.selectedProposal) {
                    if (!this.isDiscardButtonDisabled(this.selectedProposal.status)) {
                        this.confirmDiscardProposal();
                    }
                }
                break;
            case "KeyC":
                if ($event.altKey && this.selectedProposal) {//ALT-C CLONE PROPOSAL
                    this.router.navigate(["/dashboard/clone", this.selectedProposal.id, this.isFilterApplied]);
                } else { //CTRL-C START NOTAM CREATION
                    if ($event.ctrlKey) {
                        this.frsPrevKeyCodeEvent = $event;
                    } else {
                        if (this.frsPrevKeyCodeEvent) { //CTRL-C C: CATCHALL
                            this.currentCategoryId = NsdCategory.CatchAll;
                            this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                        }
                    }
                }
                break;
            case "KeyO": //CTRL-C O: OBSTACLE
                if (this.frsPrevKeyCodeEvent) {
                    this.sndPrevKeyCodeEvent = $event;
                }
                break;
            case "KeyR": //CTRL-C R: RUNWAY
                if (this.frsPrevKeyCodeEvent) {
                    this.sndPrevKeyCodeEvent = $event;
                }
                break;
            case "KeyT": // CTRL-C T: Taxiway
                if (this.frsPrevKeyCodeEvent) {
                    this.sndPrevKeyCodeEvent = $event;
                }
                break;
            case "KeyA": // CTRL-C T:
                if (this.frsPrevKeyCodeEvent) {
                    this.sndPrevKeyCodeEvent = $event;
                }
                break;
            case "Digit1":
                //CTRL-C O 1: OBST
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyO") {
                    this.currentCategoryId = NsdCategory.Obst;
                    this.currentSubCategoryId = 4;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                // CTRL-C R 1: RWY CLOSED
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyR") {
                    this.currentCategoryId = NsdCategory.RwyClsd;
                    this.currentSubCategoryId = 7;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                //CTRL-C T 1: TWY CLOSED
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyT") {
                    this.currentCategoryId = NsdCategory.TwyClsd;
                    this.currentSubCategoryId = 10;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                //CTRL-C A 1: CARS CLOSED
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyA") {
                    this.currentCategoryId = NsdCategory.CarsClsd;
                    this.currentSubCategoryId = 13;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                break;
            case "Digit2":
                //CTRL-C O 2: OBST LGT U/S
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyO") {
                    this.currentCategoryId = NsdCategory.ObstLGT;
                    this.currentSubCategoryId = 5;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                break;
            case "Digit3":
                //CTRL-C O 3: MULTI OBST
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyO") {
                    this.currentCategoryId = NsdCategory.MultiObst;
                    this.currentSubCategoryId = 14;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                break;
            case "Digit4":
                //CTRL-C O 3: MULTI OBST LGT U/S
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyO") {
                    this.currentCategoryId = NsdCategory.MultiObstLGT;
                    this.currentSubCategoryId = 15;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                break;
        }
    }

    onFilterChanged(event: any) {
        if (event.error) {
            this.toastr.error("Filter error: " + event.error.message, "", { 'positionClass': 'toast-bottom-right' });
        } else {
            this.isFilterApplied = event.isFilterApplied;
            this.filterPannelOpen = false;
            if (this.isFilterApplied) {
                this.processProposals(event.data);
            } else {
                this.getProposalsByCategory(this.queryOptions.page);
            }
        }
        this.loadingData = false;
    }

    onCategoryChanged(e: any): void {
        this.changeCategory(e.value);
    }

    onChildCategoryChanged(e: any): void {
        this.changeChildCategory(e.value);
    }

    reviewProposal() {
        this.isLeavingForm = true;
        if (this.isFilterApplied) {
            this.router.navigate(["/dashboard/viewhistory", this.selectedProposal.id]);
        } else {
            this.router.navigate(["/dashboard/view", this.selectedProposal.id]);
        }
    }

    sort(index) {
        const dir = this.tableHeaders.sorting[index].direction;

        for (let i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }

        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            if (this.isFilterApplied) {
                this.filter.changeFilterSort('_' + this.tableHeaders.sorting[index].columnName);
            } else {
                this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
            }
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            if (this.isFilterApplied) {
                this.filter.changeFilterSort(this.tableHeaders.sorting[index].columnName);
            } else {
                this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
            }
        }

        if (this.isFilterApplied) {
            this.loadingData = true;
            this.filter.runFilter(false, 1);
        } else {
            this.getProposalsByCategory(this.queryOptions.page);
        }
    }

    sortingClass(index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';

        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';

        return 'sorting';
    }

    confirmDiscardProposal() {
        $('#modal-delete').modal({});
    }

    discardProposal() {
        if (this.selectedProposal) {
            this.isLeavingForm = true;
            this.dataService.discardProposal(this.selectedProposal.id)
                .subscribe(r => {
                    let msg = this.translation.translate('Dashboard.MessageDiscardSuccess')
                    this.getProposalsByCategory(this.queryOptions.page);
                    this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                }, error => {
                    let msg = this.translation.translate('Dashboard.MessageDiscardSuccess')
                    this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
                    this.isLeavingForm = false;
                });
        }
    }

    resolveCategoryId() {
        if (this.childCategories.length > 0) {
            return this.currentSubCategoryId !== NsdCategory.All ? this.currentSubCategoryId : this.currentCategoryId;
        }

        return this.currentCategoryId;
    }

    isCreateButtonDisable() {
        if (this.isLeavingForm || this.isFilterApplied) {
            return true;
        }
        return false;
    }

    isModifyButtonDisabled() {
        if (this.isLeavingForm) {
            return true;
        }
        if (!this.selectedProposal) {
            return true;
        }

        if (this.isFilterApplied) {
            return true;
        }

        if (this.selectedProposal.seriesChecklist) {
            return true;
        }

        return this.selectedProposal.status === ProposalStatus.Disseminated ||
            this.selectedProposal.status === ProposalStatus.Terminated ||
            this.selectedProposal.status === ProposalStatus.DisseminatedModified ||
            this.selectedProposal.status === ProposalStatus.Picked ||
            this.selectedProposal.status === ProposalStatus.Parked ||
            this.selectedProposal.status === ProposalStatus.ParkedPicked ||
            this.selectedProposal.status === ProposalStatus.ParkedDraft;
    }

    isReviewButtonDisabled() {
        if (this.isLeavingForm) {
            return true;
        }
        if (!this.selectedProposal) {
            return true;
        }

        if (this.selectedProposal.seriesChecklist) {
            return true;
        }

        return false;
    }

    isReplaceButtonDisabled() {
        if (this.isLeavingForm) {
            return true;
        }
        if (!this.selectedProposal) {
            return true;
        }
        if (this.isFilterApplied) {
            return true;
        }
        if (this.selectedProposal.seriesChecklist) {
            return true;
        }
        if (this.selectedProposal.grouped && !this.selectedProposal.groupId) {
            return true;
        }
        if (this.selectedProposal.isObsolete) return true;

        if (this.selectedProposal.status === ProposalStatus.Terminated) return true;

        if (this.selectedProposal.status !== ProposalStatus.Disseminated && this.selectedProposal.status !== ProposalStatus.DisseminatedModified) {
            return true;
        }
        return false;
    }

    isCancelButtonDisabled() {
        if (this.isLeavingForm) {
            return true;
        }
        if (!this.selectedProposal) {
            return true;
        }
        if (this.isFilterApplied) {
            return true;
        }
        if (this.selectedProposal.seriesChecklist) {
            return true;
        }
        if (this.selectedProposal.grouped && !this.selectedProposal.groupId) {
            return true;
        }
        if (this.selectedProposal.isObsolete) return true;

        if (this.selectedProposal.proposalType === ProposalType.Cancel ||
            this.selectedProposal.proposalType === ProposalStatus.Expired ||
            this.selectedProposal.status === ProposalStatus.Terminated) {
            return true;
        }

        if (this.selectedProposal.status !== ProposalStatus.Disseminated && this.selectedProposal.status !== ProposalStatus.DisseminatedModified) {
            return true;
        }

        return false;
    }

    isHistoryButtonDisabled() {
        if (!this.selectedProposal) {
            return true;
        }

        if (this.selectedProposal.seriesChecklist) {
            return true;
        }

        return this.isFilterApplied || !this.selectedProposal;
    }

    isCloneButtonDisabled() {
        if (this.isLeavingForm) {
            return true;
        }
        if (this.isFilterApplied) {
            return false;
        }

        if (!this.selectedProposal) {
            return true;
        }

        if (this.selectedProposal.seriesChecklist) {
            return true;
        }

        if (this.selectedProposal.proposalType === ProposalType.Cancel) {
            return true;
        }

        return false;
    }

    private checkProposalStatus() {
        let self = this;
        this.checkPeriodicalHandler = window.setInterval(function () {
            if (self.proposals !== undefined && !self.isFilterApplied) {
                if (self.proposals.length > 0) {
                    //let proposalsIds = self.proposals.filter(p => p.status !== ProposalStatus.Draft && !p.grouped)
                    //    .map(function (p: IProposal) {
                    //        return p.id;
                    //    });
                    let proposalsIds = self.proposals.filter(p => !p.grouped)
                        .map(function (p: IProposal) {
                            return p.id;
                        });
                    self.dataService.getProposalStatus(proposalsIds, syncFrecuency, self.lastSyncTime)
                        .subscribe((response: any) => {
                            var proposalStatus = response.proposalStatus;
                            self.lastSyncTime = response.lastSyncTime;
                            for (var p of proposalStatus) {
                                self.updateProposalStatus(p);
                            }
                            self.setWidthrawModifyButtonApparenance(self.selectedProposal);
                        });
                }
            }
        }, syncFrecuency * 1000);
    }

    private updateProposalStatus(data: any) {
        if (data) {

            const index = this.proposals.findIndex(x => x.id === data.id);
            if (index >= 0) {
                if (data.status === ProposalStatus.Deleted || data.status === ProposalStatus.Discarded ||
                    data.status === ProposalStatus.Expired || data.status === ProposalStatus.Terminated) {
                    this.deleteRow(index);
                }
                else {
                    const $rowElem = $("#p_" + data.id);
                    let iniBgColorClass = this.rowColor(this.proposals[index]);

                    this.proposals[index].notamId = data.notamId;
                    if (data.startActivity) {
                        this.proposals[index].startActivity = data.startActivity;
                    }
                    this.proposals[index].endValidity = data.endValidity; //clean endValidity when cancellation...
                    this.proposals[index].status = data.status;
                    this.proposals[index].soonToExpire = data.soonToExpire;
                    this.proposals[index].modifiedByUsr = data.modifiedByUsr;
                    this.proposals[index].originator = data.originator;
                    this.proposals[index].operator = data.operator;
                    this.proposals[index].itemE = data.itemE;
                    this.proposals[index].itemA = data.itemA;
                    this.proposals[index].siteId = data.siteId;
                    this.proposals[index].estimated = data.estimated;

                    let endBgColorClass = this.rowColor(this.proposals[index]);

                    if (iniBgColorClass !== endBgColorClass) {
                        $rowElem.removeClass(iniBgColorClass, { duration: 500 });
                    }

                    ////when cancellation
                    //if (data.endValidity === "") {
                    //    this.proposals[index].endValidity = data.endValidity; //clean endValidity when cancellation...
                    //}
                    //if (data.startActivity) {
                    //    this.proposals[index].startActivity = data.startActivity;
                    //}
                    if (iniBgColorClass !== endBgColorClass) {
                        $rowElem.addClass(endBgColorClass, { duration: 500 });
                    }
                }
            }
        }
    }

    private deleteRow(index: number) {
        this.proposals.splice(index, 1);
        const selectedIndex = this.proposals.findIndex(x => x.isSelected);
        if (this.proposals.length > 0) {
            this.selectedProposal = this.proposals[0];
            if (selectedIndex >= 0 && this.selectedProposal.id !== selectedIndex) {
                this.proposals[selectedIndex].isSelected = false;
            }
            this.selectedProposal.isSelected = true;
        }
    }

    cssIcon(proposal) {
        switch (proposal.status) {
            case ProposalStatus.Draft: return 'fa-save';
            case ProposalStatus.Submitted: return 'fa-upload';
            case ProposalStatus.Withdrawn: return 'fa-download';
            case ProposalStatus.Picked: return 'fa-hourglass-half';
            case ProposalStatus.Expired: return 'fa-clock-o';
            case ProposalStatus.Replaced: return 'fa-code-fork';
            case ProposalStatus.Parked: return 'fa-lock';
            case ProposalStatus.ParkedDraft: return 'fa-lock';
            case ProposalStatus.ParkedPicked: return 'fa-lock';
            case ProposalStatus.Disseminated: return 'fa-wifi';
            case ProposalStatus.DisseminatedModified: return 'fa-wifi';
            case ProposalStatus.Cancelled: return 'fa-hand-paper-o';
            case ProposalStatus.Discarded: return 'fa-trash';
            case ProposalStatus.Rejected: return 'fa-ban';
            case ProposalStatus.Terminated: return 'fa-hand-paper-o';
            default: return 'fa-save';
        }
    }

    rowColor(proposal) {
        if (proposal.grouped) {
            return "bg-rejected";
        }

        if (proposal.soonToExpire) {
            return 'bg-soontoexpire';
        }

        switch (proposal.status) {
            case ProposalStatus.Draft: return 'bg-saved';
            case ProposalStatus.Submitted: return 'bg-submitted';
            case ProposalStatus.Withdrawn: return 'bg-widthdrawn';
            case ProposalStatus.Picked: return 'bg-picked';
            case ProposalStatus.Parked: return 'bg-picked';
            case ProposalStatus.ParkedDraft: return 'bg-picked';
            case ProposalStatus.ParkedPicked: return 'bg-picked';
            case ProposalStatus.Replaced: return 'bg-replace';
            case ProposalStatus.Rejected: return 'bg-rejected fg-white';
            case ProposalStatus.Expired: return 'bg-expired';
            case ProposalStatus.Cancelled: return 'bg-cancel';
            case ProposalStatus.Terminated: return 'bg-cancel';
            case ProposalStatus.DisseminatedModified: return 'c-orange';
            default: return '';
        }
    }

    isDiscardButtonDisabled(proposalStatus) {
        if (!this.selectedProposal) {
            return true;
        }

        return this.isFilterApplied ||
            proposalStatus === ProposalStatus.Picked ||
            proposalStatus === ProposalStatus.Submitted ||
            proposalStatus === ProposalStatus.Withdrawn ||
            proposalStatus === ProposalStatus.Disseminated ||
            proposalStatus === ProposalStatus.ParkedDraft ||
            proposalStatus === ProposalStatus.ParkedPicked ||
            proposalStatus === ProposalStatus.Parked ||
            proposalStatus === ProposalStatus.Terminated ||
            proposalStatus === ProposalStatus.DisseminatedModified;
    }

    itemsPerPageChanged(e: any): void {
        if (this.isFilterApplied) {
            this.loadingData = true;
            this.filter.runFilter(false, 1, e.value);
        }
        else {
            this.queryOptions.pageSize = e.value;
            this.getProposalsByCategory(NsdCategory.All);
        }
    }

    selectProposal(proposal) {
        if (this.selectedProposal) {
            this.selectedProposal.isSelected = false;
        }
        this.selectedProposal = proposal
        this.selectedProposal.isSelected = true;
        this.setWidthrawModifyButtonApparenance(proposal);
        this.memStorageService.save(this.memStorageService.PROPOSAL_ID_KEY, this.selectedProposal.id);
    }

    showGroup(rowIndex: number) {
        let pos = rowIndex + 1;
        if (this.proposals[rowIndex].groupOpen) {
            if (this.proposals[rowIndex].group.length > 0) {
                this.proposals.splice(pos, this.proposals[rowIndex].group.length);
            }
            this.proposals[rowIndex].groupOpen = false;
        } else {
            this.proposals[rowIndex].groupOpen = true;
        }

        this.proposals[rowIndex].group = [];
        if (this.proposals[rowIndex].groupOpen) {
            this.dataService.getGroupedProposals(this.proposals[rowIndex].groupId)
                .subscribe((proposals: IProposal[]) => {
                    if (proposals.length > 0) {
                        for (let i = 0; i < proposals.length; i++) {
                            proposals[i].isSelected = false;
                            proposals[i].grouped = true;
                            proposals[i].groupId = null;
                            this.proposals[rowIndex].group.push(proposals[i]);
                        }

                        let pos = rowIndex + 1;
                        if (this.proposals[rowIndex].group) {
                            let isLastRow = this.proposals.length === pos;
                            for (let i = 0; i < this.proposals[rowIndex].group.length; i++) {
                                let pg = this.proposals[rowIndex].group[i];
                                if (isLastRow) {
                                    this.proposals.push(pg);
                                } else {
                                    this.proposals.splice(pos + i, 0, pg);
                                }
                            }
                        }
                    }
                });
        }
    }

    modifyOrWithdrawProposal(proposal: IProposal) {
        this.selectProposal(proposal);

        if (proposal.grouped) {
            return;
        }

        if (!this.isModifyButtonDisabled()) {
            this.isLeavingForm = true;
            this.memStorageService.save(this.memStorageService.MODIFY_ACTION_KEY, this.txtWidthdrawModifyButtom === this.txtModify ? 0 : 1);
            if (this.selectedProposal.categoryId === 18) {
                this.changeRoute(["/dashboard/edit/rsc", this.selectedProposal.id]); 
            } else {
                this.changeRoute(["/dashboard/edit", this.selectedProposal.id]); //this.router.navigate(["/dashboard/edit", this.selectedProposal.id]).catch;
            }
        }
    }

    onPageChange(page: number) {
        this.getProposalsByCategory(page);
    }

    onFilterPageChange(page: number) {
        this.loadingData = true;
        this.filter.runFilter(false, page);
    }

    showHistory(proposalId: number) {
        $('#proposal-history').modal({});
        this.dataService.getProposalHistory(proposalId)
            .subscribe((proposals: IProposal[]) => {
                if (proposals.length > 0) {
                    this.proposalHistory = proposals;
                    this.proposalHistory[0].isSelected = true;
                } else {
                    this.proposalHistory = [];
                }
            });
    }

    isNSDAllowed(catName: string): boolean {
        var id = this.categoriesData.findIndex(x => x.name === catName);
        if (id === -1) return false;
        return true;
    }

    private changeCategory(catId: string): void {
        this.currentCategoryId = +catId;
        this.childCategories = this.addChildCategoryToComboFilter(this.childCategories, this.currentCategoryId);
        const queryOptions = this.memStorageService.get(this.memStorageService.PROPOSAL_QUERY_OPTIONS_KEY);

        this.memStorageService.save(this.memStorageService.CATEGORY_ID_KEY, this.currentCategoryId);
        this.memStorageService.save(this.memStorageService.SUB_CATEGORY_ID_KEY, NsdCategory.All); //set all by default
        if (!this.expiringApplied)
            this.getProposalsByCategory(NsdCategory.All);
    }

    private changeChildCategory(subCatId: string): void {
        this.currentSubCategoryId = +subCatId;
        this.memStorageService.save(this.memStorageService.SUB_CATEGORY_ID_KEY, this.currentSubCategoryId);
        this.getProposalsByCategory(NsdCategory.All);
    }

    private getProposalsByCategory(page) {
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            if (this.isFilterApplied) {
                this.filter.runFilter(false, page, this.queryOptions.pageSize);
            } else if (this.expiringApplied) {
                this.dataService.getExpiringProposals(this.queryOptions)
                    .subscribe((proposals: any) => {
                        this.processProposals(proposals);
                    });
            } else {
                this.memStorageService.save(this.memStorageService.PROPOSAL_QUERY_OPTIONS_KEY, this.queryOptions);

                if (this.childCategories.length > 0) {
                    if (this.currentSubCategoryId !== NsdCategory.All) {
                        this.dataService.getProposalsByCategory(this.currentSubCategoryId, this.queryOptions)
                            .subscribe((proposals: any) => {
                                this.processProposals(proposals);
                            });
                    } else {
                        this.dataService.getProposalsByParentCategory(this.currentCategoryId, this.queryOptions)
                            .subscribe((proposals: any) => {
                                this.processProposals(proposals);
                            });
                    }
                } else {
                    this.dataService.getProposalsByCategory(this.currentCategoryId, this.queryOptions)
                        .subscribe((proposals: any) => {
                            this.processProposals(proposals);
                        });
                }
            }
        }
        catch (e) {
            this.loadingData = false;
        }
    }

    private processProposals(proposals: any) {

        if (this.isFilterApplied) {
            this.filterPaging = proposals.paging;
            this.filterTotalPages = proposals.paging.totalPages;
        }

        this.paging = proposals.paging;
        this.totalPages = proposals.paging.totalPages;

        for (var iter = 0; iter < proposals.data.length; iter++) proposals.data[iter].index = iter;

        const lastProposalSelected = this.memStorageService.get(this.memStorageService.PROPOSAL_ID_KEY);
        if (proposals.data.length > 0) {
            this.proposals = proposals.data;
            let index = 0;
            if (lastProposalSelected) {
                index = this.proposals.findIndex(x => x.id === lastProposalSelected);
                if (index < 0) index = 0; // when not found first should be selected;
            }
            this.proposals[index].isSelected = true;
            this.selectedProposal = this.proposals[index];
            this.setWidthrawModifyButtonApparenance(this.selectedProposal);
            this.loadingData = false;
        } else {
            this.proposals = [];
            this.selectedProposal = null;
            this.loadingData = false;
        }
    }

    private addItemToComboFilter(comboData: Array<any>, category: INsdCategory) {
        comboData.push({
            groupCategoryId: category.groupCategoryId,
            id: category.id,
            isSelected: category.isSelected,
            name: category.name,
            text: category.name
        });
    }

    private setWidthrawModifyButtonApparenance(proposal: any) {
        proposal = proposal || { status: ProposalStatus.Draft };

        const isButtonTextModified = proposal.status === ProposalStatus.Draft ||
            proposal.status === ProposalStatus.Withdrawn ||
            proposal.status === ProposalStatus.Replaced ||
            proposal.status === ProposalStatus.Rejected ||
            proposal.status === ProposalStatus.Cancelled

        this.txtWidthdrawModifyButtom = isButtonTextModified ? this.txtModify : this.txtWidthdrawn;
        this.iconWidthdrawModifyButtom = isButtonTextModified ? "fa-pencil-square-o" : "fa-download";
    }

    private addChildCategoryToComboFilter(comboData: Array<any>, currentCategoryId: number) {
        comboData = [];
        const categoryIndex = this.categoriesData.findIndex(x => x.id === currentCategoryId);
        if (categoryIndex >= 0) {
            if (this.categoriesData[categoryIndex].children) {
                this.addAllToComboFilter(comboData);
                for (let i = 0; i < this.categoriesData[categoryIndex].children.length; i++) {
                    const subCategory = this.categoriesData[categoryIndex].children[i];
                    this.addItemToComboFilter(comboData, subCategory);
                }
            }
        }
        this.currentSubCategoryId = NsdCategory.All;

        return comboData;
    }

    private addAllToComboFilter(comboData: Array<any>) {
        comboData.push({
            groupCategoryId: 0,
            id: 1,
            isSelected: true,
            name: 'Root',
            text: "ALL"
        });
    }
}
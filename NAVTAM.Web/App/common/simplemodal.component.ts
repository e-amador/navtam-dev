﻿import { Component, Input } from '@angular/core'

@Component({
    selector: 'simple-modal',
    template: `
        <div id="{{modalId}}" class="modal fade" tabindex="-1">
            <div class="modal-dialog modal-xl" style="margin-top: 13% !important;">
                <div class="modal-content">
                    <div class="modal-header bg-green">
                        <button type="button" class="close" style="color:#fff" data-dismiss="modal">
                            <span>&times;</span>
                        </button>
                        <h4 class="modal-title">{{title}}</h4>
                    </div>
                    <div class="modal-body">
                        <ng-content></ng-content>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-dark" data-dismiss="modal">{{btnClose}}</button>
                    </div>
                </div>
            </div>
        </div>`,
    styles: [`
        .modal-body { }
    `]    
})
export class SimpleModalComponent {
    @Input() title: string;
    @Input() modalId: string;
    @Input() btnClose: string;
}
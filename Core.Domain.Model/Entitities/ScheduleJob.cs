﻿using System;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class ScheduleJob
    {
        public string Id { get; set; }
        public DateTime LastTrigger { get; set; }
    }
}

﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;

    using NavCanada.Core.Domain.Model.Enums;

    public class NotamPackageViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Condition
        {
            get
            {
                if (Proposals.Count() > 0)
                    return Thread.CurrentThread.CurrentUICulture.Name == "fr" ? LastProposal.ItemEFrench : LastProposal.ItemE;

                return "";
            }
        }
        public long NSDId { get; set; }
        public bool NoUserInputCancel { get; set; }
        public string NSDUrl { get; set; }    


        public string SubjectCategory { get; set; }
        public string SubjectType { get; set; }
        public string SDOSubjectId { get; set; }
        public string TempSubjectId { get; set; }
        public SdoSubjectViewModel SDOSubject { get; set; }
        public SdoSubjectViewModel TopParentSDOSubject { get; set; }

        public List<NotamProposalViewModel> Proposals { get;  set; }

        public NotamPackageViewModel()
        {
            Proposals = new List<NotamProposalViewModel>();
        }

        public string FinalNotamType
        {
            get
            {
                if (Proposals.Count() > 0)
                    return LastProposal.Type;

                return NotamType.N.ToString();
            }
        }

        public string FinalStatus
        {
            get
            {
                if (Proposals.Count() > 0)
                    return LastProposal.Type == NotamType.C.ToString() && LastProposal.StatusDescription =="Submitted" ? "Canceled" : LastProposal.StatusDescription;

                return NotamProposalStatusCode.Draft.ToString();
            }
        }

        public int FinalInternalStatus
        {
            get
            {
                if (Proposals.Count() > 0)
                    return LastProposal.InternalStatus;

                return (int)NotamProposalStatusCode.Draft;
            }
        }

        public NotamProposalViewModel LastProposal
        {
            get
            {
                return Proposals.OrderBy(p => p.StatusTime).Last();
            }
        }

        public NotamProposalViewModel LastSubmittedProposal 
        {
            get
            {
                return Proposals.Where(p => p.SubmittedNotam != null).OrderBy(p => p.StatusTime).LastOrDefault();
            }
        }


        public DateTime FinalStatusDate
        {
            get
            {
                if (Proposals.Count() > 0)
                    return LastProposal.StatusTime;

                return DateTime.Now;
            }
        }
        public DateTime? FinalStartValidity
        {
            get
            {
                if (Proposals.Count() > 0)
                    return LastProposal.StartValidity;

                return null;
            }
        }
        public DateTime? FinalEndValidity
        {
            get
            {
                if (Proposals.Count() > 0)
                    return LastProposal.EndValidity;

                return null;
            }
        }

        public string NotamNumber
        {
            get
            {
                var lastSubmittedProposal = LastSubmittedProposal;
                if (lastSubmittedProposal != null)
                    return lastSubmittedProposal.SubmittedNotam.Number.ToString();

                return "";

            }
        }

        public string DisplayedNotamNumber
        {
            get
            {
                return NotamNumber == "" ? "" : LastSubmittedProposal.Series + NotamNumber.PadLeft(4, '0') + "/" + LastSubmittedProposal.SubmittedNotam.Year;
            }
        }

    }

}

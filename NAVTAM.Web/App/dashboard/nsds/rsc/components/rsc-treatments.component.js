"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RscTreatmentsComponent = void 0;
var core_1 = require("@angular/core");
var angular_l10n_1 = require("angular-l10n");
var forms_1 = require("@angular/forms");
var rsc_service_1 = require("../rsc.service");
var toastr_service_1 = require("../../../../common/toastr.service");
var RscTreatmentsComponent = /** @class */ (function () {
    function RscTreatmentsComponent(fb, translation, rscService, toastr) {
        this.fb = fb;
        this.translation = translation;
        this.rscService = rscService;
        this.toastr = toastr;
        this.mainForm = null;
        this.model = null;
        this.isReadOnly = false;
    }
    RscTreatmentsComponent.prototype.ngOnInit = function () {
        this.isFormLoadingData = false;
        this.configureReactiveForm();
        if (!this.model.sand) {
            this.$ctrl('sandTimeApplied').disable();
        }
        if (!this.model.chemicallyTreated) {
            this.$ctrl('chemicalTimeApplied').disable();
        }
    };
    RscTreatmentsComponent.prototype.ngOnChanges = function () {
    };
    RscTreatmentsComponent.prototype.ngOnDestroy = function () {
    };
    RscTreatmentsComponent.prototype.$ctrl = function (name) {
        return this.frmGroup.get(name);
    };
    RscTreatmentsComponent.prototype.configureReactiveForm = function () {
        var frmArrControls = this.mainForm.controls['frmTreatmentsArr'];
        this.frmGroup = this.fb.group({
            sandTreated: [{ value: this.model.sand, disabled: this.isReadOnly }, []],
            sandTimeApplied: [{ value: this.model.sandTimeApplied, disabled: false }, []],
            chemicallyTreated: [{ value: this.model.chemicallyTreated, disabled: this.isReadOnly }, []],
            chemicalTimeApplied: [{ value: this.model.chemicalTimeApplied, disabled: false }, []],
            remarks: [{ value: this.model.remarks, disabled: this.isReadOnly }, []],
        });
        frmArrControls.push(this.frmGroup);
    };
    RscTreatmentsComponent.prototype.sandTreatedChangingClick = function () {
        this.model.sand = !this.model.sand;
        var ctrl = this.$ctrl('sandTimeApplied');
        if (!this.isSandTimeAppliedEnabled()) {
            this.model.sandTimeApplied = '';
            ctrl.patchValue(this.model.sandTimeApplied);
            ctrl.disable();
        }
        else
            ctrl.enable();
    };
    RscTreatmentsComponent.prototype.setSandTimeApplied = function () {
    };
    RscTreatmentsComponent.prototype.chemicallyTreatedChangingClick = function () {
        this.model.chemicallyTreated = !this.model.chemicallyTreated;
        var ctrl = this.$ctrl('chemicalTimeApplied');
        if (!this.isChemicalTimeAppliedEnabled()) {
            this.model.chemicalTimeApplied = '';
            ctrl.patchValue(this.model.chemicalTimeApplied);
            ctrl.disable();
        }
        else
            ctrl.enable();
    };
    RscTreatmentsComponent.prototype.setChemicalTimeApplied = function () {
    };
    RscTreatmentsComponent.prototype.isSandTimeAppliedEnabled = function () {
        return !this.isReadOnly && this.model.sand;
    };
    RscTreatmentsComponent.prototype.isChemicalTimeAppliedEnabled = function () {
        return !this.isReadOnly && this.model.chemicallyTreated;
    };
    RscTreatmentsComponent.prototype.validateKeyPressHours = function (evt, ctrlName) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var num = +key;
            var pos = evt.currentTarget.selectionStart;
            if (pos === 0) { //val.length
                //First number
                if (key !== '0' && key !== '1' && key !== '2') {
                    evt.returnValue = false;
                    if (evt.preventDefault)
                        evt.preventDefault();
                    return;
                }
            }
            else if (pos === 1) { //val.length === 1
                //Second number
                var fc = this.$ctrl(ctrlName);
                var val = fc.value;
                if (val[0] === '2' && num > 3) {
                    evt.returnValue = false;
                    if (evt.preventDefault)
                        evt.preventDefault();
                    return;
                }
            }
            else if (pos === 2) { //val.length === 2
                //Third number
                if (num > 5) {
                    evt.returnValue = false;
                    if (evt.preventDefault)
                        evt.preventDefault();
                    return;
                }
            }
        }
    };
    //Export functions
    RscTreatmentsComponent.prototype.isFormValid = function () {
        return true;
    };
    RscTreatmentsComponent.prototype.exportComponentInternalData = function () {
        var data = {
            parent: this.model.parent,
            sand: this.model.sand,
            sandTimeApplied: this.model.sandTimeApplied,
            chemicallyTreated: this.model.chemicallyTreated,
            chemicalTimeApplied: this.model.chemicalTimeApplied,
            remarks: this.model.remarks,
        };
        return data;
    };
    RscTreatmentsComponent.prototype.clearValues = function () {
        alert('Clear Component Values: Treatments');
    };
    __decorate([
        core_1.Input('form'),
        __metadata("design:type", forms_1.FormGroup)
    ], RscTreatmentsComponent.prototype, "mainForm", void 0);
    __decorate([
        core_1.Input('model'),
        __metadata("design:type", Object)
    ], RscTreatmentsComponent.prototype, "model", void 0);
    __decorate([
        core_1.Input('isReadOnly'),
        __metadata("design:type", Boolean)
    ], RscTreatmentsComponent.prototype, "isReadOnly", void 0);
    RscTreatmentsComponent = __decorate([
        core_1.Component({
            selector: 'rsc-treatments',
            templateUrl: './app/dashboard/nsds/rsc/components/rsc-treatments.component.html'
        }),
        __param(3, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [forms_1.FormBuilder,
            angular_l10n_1.TranslationService,
            rsc_service_1.RscService, Object])
    ], RscTreatmentsComponent);
    return RscTreatmentsComponent;
}());
exports.RscTreatmentsComponent = RscTreatmentsComponent;
function isANumber(str) {
    return !/\D/.test(str);
}
//# sourceMappingURL=rsc-treatments.component.js.map
﻿namespace NavCanada.Utils.Apps.InoMock.WebServiceMessages
{
    using System;

    using NavCanada.Utils.Apps.InoMock.Models;

    public class NotamProposalStatusMessage
    {
        public long NotamProposalId { get; set; }
        public NotamProposalStatusType Status { get; set; }
        public DateTime StatusTime { get; set; }
        public Notam SubmittedNotam { get; set; }
        public string RejectionReason { get; set;  }
    }
}
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrgEditComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var windowRef_service_1 = require("../../common/windowRef.service");
var angular_l10n_1 = require("angular-l10n");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var toastr_service_1 = require("./../../common/toastr.service");
var phone_validator_1 = require("../shared/phone.validator");
var data_service_1 = require("../shared/data.service");
var OrgEditComponent = /** @class */ (function () {
    function OrgEditComponent(toastr, fb, dataService, memStorageService, router, winRef, activatedRoute, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.activatedRoute = activatedRoute;
        this.locale = locale;
        this.translation = translation;
        this.model = null;
        this.isOrgNameUnique = true;
        this.isValidatingOrg = false;
        this.isSubmitting = false;
        if (this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);
    }
    OrgEditComponent.prototype.ngOnInit = function () {
        this.doaOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('OrgSession.SelectDoa')
            },
            multiple: true,
            width: "100%"
        };
        this.nsdOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('OrgSession.SelectNsds')
            },
            multiple: true,
            width: "100%"
        };
        this.orgTypeOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('OrgSession.SelectDoa')
            },
            multiple: false,
            width: "100%"
        };
        this.orgTypes = [
            {
                id: '0',
                text: this.translation.translate('OrgSession.OrgTypeExternal')
            },
            {
                id: '1',
                text: this.translation.translate('OrgSession.OrgTypeFic')
            },
            {
                id: '2',
                text: this.translation.translate('OrgSession.OrgTypeNof')
            },
            {
                id: '3',
                text: this.translation.translate('OrgSession.OrgTypeAdministration')
            }
        ];
        this.action = this.activatedRoute.snapshot.data[0].action;
        this.model = this.activatedRoute.snapshot.data['model'];
        this.orgType = this.model.type;
        this.model.telephone = phone_validator_1.PhoneValidator.formatTenDigits(this.model.telephone);
        this.isReadOnly = this.action === "view";
        this.configureReactiveForm();
        this.loadDoas();
        this.loadNsds();
    };
    OrgEditComponent.prototype.onDoaChanged = function (data) {
        if (data.value && data.value.length > 0) {
            this.$ctrl("doas").setValue(data.value.join(','));
        }
        else {
            this.$ctrl("doas").setValue("");
        }
        if (this.orgForm.pristine && data.value && !this.matchIds(data.value, this.model.doaIds)) {
            this.orgForm.markAsDirty();
        }
    };
    OrgEditComponent.prototype.onNsdChanged = function (data) {
        if (data.value && data.value.length > 0) {
            this.orgForm.get('nsds').setValue(data.value.join(','));
        }
        else {
            this.orgForm.get('nsds').setValue("");
        }
        if (this.orgForm.pristine && data.value && !this.matchIds(data.value, this.model.nsdIds)) {
            this.orgForm.markAsDirty();
        }
    };
    OrgEditComponent.prototype.onOrgTypeChanged = function (data) {
        if (data.value && data.value.length > 0) {
            this.$ctrl("orgType").setValue(data.value);
        }
        else {
            this.$ctrl("orgType").setValue("");
        }
        if (this.orgForm.pristine && data.value && data.value !== this.model.type) {
            this.orgForm.markAsDirty();
        }
    };
    OrgEditComponent.prototype.getAction = function () {
        return this.action;
    };
    OrgEditComponent.prototype.backToOrgList = function () {
        this.router.navigate(['/administration/orgs']);
    };
    OrgEditComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.isSubmitting)
            return;
        this.isSubmitting = true;
        var org = this.prepareUpdateOrg();
        this.dataService.updateOrg(org)
            .subscribe(function (data) {
            var msg = _this.translation.translate('OrgSession.SuccessOrgUpdated');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            _this.router.navigate(["/administration/orgs"]);
        }, function (error) {
            _this.isSubmitting = false;
            var msg = _this.translation.translate('OrgSession.FailureOrgUpdated');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    OrgEditComponent.prototype.validateName = function () {
        var usernameCtrl = this.orgForm.get("name");
        return usernameCtrl.valid || this.orgForm.pristine;
    };
    OrgEditComponent.prototype.validateEmailAddress = function () {
        var emailCtrl = this.orgForm.get("emailAddress");
        return emailCtrl.valid || this.orgForm.pristine;
    };
    OrgEditComponent.prototype.validateAddress = function () {
        var emailCtrl = this.orgForm.get("address");
        return emailCtrl.valid || this.orgForm.pristine;
    };
    OrgEditComponent.prototype.validateTelephone = function () {
        var emailCtrl = this.orgForm.get("telephone");
        return emailCtrl.valid || this.orgForm.pristine;
    };
    OrgEditComponent.prototype.validateTypeOfOperation = function () {
        var typeOfOperationCtrl = this.orgForm.get("typeOfOperation");
        return typeOfOperationCtrl.valid || this.orgForm.pristine;
    };
    OrgEditComponent.prototype.validateDoas = function () {
        var fc = this.orgForm.get('doas');
        return fc.value !== -1 || this.orgForm.pristine;
    };
    OrgEditComponent.prototype.validateNsds = function () {
        var fc = this.orgForm.get('nsds');
        return fc.value || this.orgForm.pristine;
    };
    OrgEditComponent.prototype.disableSave = function () {
        return this.orgForm.pristine || !this.orgForm.valid || !this.isOrgNameUnique || this.isSubmitting;
    };
    OrgEditComponent.prototype.loadDoas = function () {
        var _this = this;
        this.dataService.getDoas()
            .subscribe(function (doa) {
            _this.doas = doa.map(function (r) {
                return {
                    id: r.id,
                    text: r.name
                };
            });
            _this.selectedDoaIds = _this.model.doaIds || -1;
        });
    };
    OrgEditComponent.prototype.loadNsds = function () {
        var _this = this;
        this.dataService.getNsdLeaves()
            .subscribe(function (nsds) {
            _this.nsds = nsds.map(function (r) {
                return {
                    id: r.id,
                    text: r.name
                };
            });
            _this.selectedNsdIds = _this.model.nsdIds || -1;
        });
    };
    OrgEditComponent.prototype.validateOrgNameUnique = function () {
        var _this = this;
        if (this.isValidatingOrg)
            return;
        if (this.validateName()) {
            var orgName = this.$ctrl('name');
            if (orgName.value) {
                if (orgName.value.length >= 3) {
                    this.isValidatingOrg = true;
                    this.dataService.isOrgNameUnique(this.model.id, orgName.value)
                        .finally(function () {
                        _this.isValidatingOrg = false;
                    })
                        .subscribe(function (resp) {
                        _this.isOrgNameUnique = resp;
                    });
                }
            }
        }
    };
    OrgEditComponent.prototype.configureReactiveForm = function () {
        var _this = this;
        this.orgForm = this.fb.group({
            name: [{ value: this.model.name, disabled: this.isReadOnly }, [forms_1.Validators.required, forms_1.Validators.minLength(3), forms_1.Validators.maxLength(100), forms_1.Validators.pattern('[a-zA-Z].*')]],
            address: [{ value: this.model.address, disabled: this.isReadOnly }, [forms_1.Validators.required]],
            emailAddress: [{ value: this.model.emailAddress, disabled: this.isReadOnly }, [forms_1.Validators.required, forms_1.Validators.pattern("[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}")]],
            headOffice: [{ value: this.model.headOffice, disabled: this.isReadOnly }, []],
            typeOfOperation: [{ value: this.model.typeOfOperation, disabled: this.isReadOnly }, [forms_1.Validators.required]],
            emailDistribution: [{ value: this.model.emailDistribution, disabled: this.isReadOnly }, []],
            doas: [{ value: this.model.assignedDoaIds || -1, disabled: false }, [doasValidator]],
            nsds: [{ value: this.model.assignedNsdIds || -1, disabled: false }, [nsdsValidator]],
            telephone: [{ value: this.model.telephone, disabled: this.isReadOnly }, [phoneNumberValidator]],
            orgType: { value: this.model.type, disabled: this.isReadOnly },
        });
        this.$ctrl("name").valueChanges.debounceTime(1000).subscribe(function () { return _this.validateOrgNameUnique(); });
    };
    OrgEditComponent.prototype.$ctrl = function (name) {
        return this.orgForm.get(name);
    };
    OrgEditComponent.prototype.phoneNumberFocus = function () {
        var phoneCtrl = this.$ctrl("telephone");
        var value = phone_validator_1.PhoneValidator.stripFormat(phoneCtrl.value);
        phoneCtrl.setValue(value);
    };
    OrgEditComponent.prototype.phoneNumberBlur = function () {
        var phoneCtrl = this.$ctrl("telephone");
        var value = phone_validator_1.PhoneValidator.formatTenDigits(phoneCtrl.value);
        phoneCtrl.setValue(value);
    };
    OrgEditComponent.prototype.prepareUpdateOrg = function () {
        var org = {
            id: this.model.id,
            name: this.orgForm.get('name').value,
            address: this.orgForm.get('address').value,
            emailAddress: this.orgForm.get('emailAddress').value,
            emailDistribution: this.orgForm.get('emailDistribution').value,
            headOffice: this.orgForm.get('headOffice').value,
            telephone: this.orgForm.get('telephone').value,
            typeOfOperation: this.orgForm.get('typeOfOperation').value,
            assignedDoaIds: this.parseIds(this.orgForm.get('doas').value),
            assignedNsdIds: this.parseIds(this.orgForm.get('nsds').value),
            type: this.parseOrgType(this.orgForm.get('orgType').value),
        };
        return org;
    };
    OrgEditComponent.prototype.parseOrgType = function (value) {
        return +(value || 0);
    };
    OrgEditComponent.prototype.parseIds = function (value) {
        return (value || "").split(",").map(function (v) { return +v; });
    };
    OrgEditComponent.prototype.matchIds = function (newValues, oldValues) {
        var newJoined = newValues ? newValues.join(' ') : "";
        var oldJoined = oldValues ? oldValues.join(' ') : "";
        return newJoined === oldJoined;
    };
    OrgEditComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/org/org-edit.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], OrgEditComponent);
    return OrgEditComponent;
}());
exports.OrgEditComponent = OrgEditComponent;
function doasValidator(c) {
    if (!c.value) {
        return { 'required': true };
    }
    return null;
}
function nsdsValidator(c) {
    if (!c.value) {
        return { 'required': true };
    }
    return null;
}
function phoneNumberValidator(c) {
    var value = c.value;
    if (!value)
        return { 'required': true };
    if (!phone_validator_1.PhoneValidator.isValidPhoneNumber(c.value, 10)) {
        return { 'pattern': true };
    }
    return null;
}
//# sourceMappingURL=org-edit.component.js.map
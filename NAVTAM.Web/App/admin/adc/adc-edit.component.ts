﻿import { Component, Inject, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { WindowRef } from '../../common/windowRef.service';
import { FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms'
import { LocaleService, TranslationService } from 'angular-l10n';
import { MemoryStorageService } from '../shared/mem-storage.service'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';
import { DataService } from '../shared/data.service'
import { LocationService } from '../../dashboard/shared/location.service'
import { IAhpNoDC, IAerodromeDisseminationCategory, ISelectOptions } from '../shared/data.model';

@Component({
    templateUrl: '/app/admin/adc/adc-edit.component.html'
})
export class ADCEditComponent implements OnInit {

    model: any = null;
    action: string;
    sdoModel: any = null;

    addAdcForm: FormGroup;

    disseminationCategories: ISelectOptions[] = [];
    selectedDisseminationCategory?: string = '-1';
    disseminationCategoriesOptions: Select2Options;

    isReadOnly: boolean = false;
    isSubmitting: boolean = false;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private winRef: WindowRef,
        public locale: LocaleService,
        public translation: TranslationService,
        public geoLocation: LocationService) {

        if (this.winRef.appConfig.ccs)
            this.router.navigate(['/administration']);
    }

    ngOnInit() {

        this.disseminationCategoriesOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('ADCSession.SelectDissCat')
            },
            width: "100%"
        }

        this.action = this.activatedRoute.snapshot.data[0].action;
        this.model = this.activatedRoute.snapshot.data['model'];
        this.isReadOnly = this.action === "view";

        this.loadDissCategory();
        this.configureReactiveForm();

        this.dataService.getAhpModel(this.model.ahpMid)
            .subscribe((sdo: IAhpNoDC) => {
                this.sdoModel = sdo;
                if (this.model.ahpCodeId !== this.sdoModel.designator) this.model.ahpCodeId = this.sdoModel.designator;
                if (this.model.ahpName !== this.sdoModel.name) this.model.ahpName = this.sdoModel.name;
                if (this.model.latitude !== this.sdoModel.latitude) this.model.latitude = this.sdoModel.latitude;
                if (this.model.longitude !== this.sdoModel.longitude) this.model.longitude = this.sdoModel.longitude;
            });
    }

    backToADCList() {
        this.router.navigate(['/administration/adc-tab']);
    }

    disableSubmit(): boolean {
        if (this.addAdcForm.pristine) return true;
        if (!this.addAdcForm.valid) return true;
        if (this.isSubmitting) return true;
        return false;
    }

    onSubmit() {
        if (this.isSubmitting) return;
        this.isSubmitting = true;
        var adcToSave = this.prepareADC();
        this.dataService.updateADC(adcToSave)
            .subscribe(data => {
                let msg = this.translation.translate('ADCSession.SuccessADCCreated');
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                this.router.navigate(["/administration/adc-tab"]);
            }, error => {
                this.isSubmitting = false;
                let msg = this.translation.translate('ADCSession.FailureADCCreated');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });

    }

    private getLatitude(lat: number): string {
        return this.geoLocation.convertLatToDMS(lat);
    }

    private getLongitude(long: number): string {
        return this.geoLocation.convertLongToDMS(long);
    }

    private configureReactiveForm() {
        this.addAdcForm = this.fb.group({
            designator: [{ value: this.model.ahpCodeId, disabled: true }, []],
            name: [{ value: this.model.ahpName, disabled: true }, []],
            disseminationCategory: [{ value: String(this.model.disseminationCategory), disabled: this.isReadOnly }, [validateDisseminationCategory]],
            disseminationCategoryName: [{ value: this.model.disseminationCategoryName, disabled: this.isReadOnly }, [Validators.required]],
        });
    }

    private prepareADC(): IAerodromeDisseminationCategory {
        const adc: IAerodromeDisseminationCategory = {
            id: this.model.id,
            ahpCodeId: this.model.ahpCodeId,
            ahpMid: this.model.ahpMid,
            ahpName: this.model.ahpName,
            latitude: this.model.latitude,
            longitude: this.model.longitude,
            disseminationCategory: this.addAdcForm.get('disseminationCategory').value,
            disseminationCategoryName: this.addAdcForm.get('disseminationCategoryName').value.toUpperCase(),
            servedCity: "",
        }
        return adc;
    }

    public validateDissemination() {
        const fc = this.addAdcForm.get('disseminationCategory');
        if (validateDisseminationCategory(fc) !== null) return false;
        return true;
    }

    public onDisseminationChanged(data: { value: string }) {
        if (data.value) {
            var diss = this.disseminationCategories.find(x => x.id === String(data.value));
            if (diss) {
                this.addAdcForm.get('disseminationCategory').setValue(diss.id);
                this.addAdcForm.get('disseminationCategoryName').setValue(diss.text);
            } else {
                this.addAdcForm.get('disseminationCategory').setValue('-1');
                this.addAdcForm.get('disseminationCategoryName').setValue('');
            }
        } else {
            this.addAdcForm.get('disseminationCategory').setValue('-1');
            this.addAdcForm.get('disseminationCategoryName').setValue('');
        }
        if (data.value !== '-1' && this.selectedDisseminationCategory !== this.addAdcForm.get('disseminationCategory').value) {
            this.addAdcForm.markAsDirty();
        }
    }

    private loadDissCategory(): void {
        this.disseminationCategories = [
            { id: '-1', text: '' },
            { id: '0', text: 'National' },
            { id: '1', text: 'US' },
            { id: '2', text: 'International' },
        ];
        this.selectedDisseminationCategory = String(this.model.disseminationCategory) || '-1';

    }
}

function validateDisseminationCategory(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value === '-1') {
        return { 'required': true }
    }
    return null;
}


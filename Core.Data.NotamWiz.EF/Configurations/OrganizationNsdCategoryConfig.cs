﻿using NavCanada.Core.Domain.Model.Entitities;
using System.Data.Entity.ModelConfiguration;

namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    public class OrganizationNsdCategoryConfig : EntityTypeConfiguration<OrganizationNsdCategory>
    {
        public OrganizationNsdCategoryConfig()
        {
            HasKey(e => new { e.OrganizationId, e.NsdCategoryId });
        }
    }
}

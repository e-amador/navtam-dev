﻿{
    var Errors = new Array;
    Errors["Invalid Days Range"] = {english: "Invalid Day Range English", french:""};

    var CurrentError;

    function getError(code){
        if(Errors[code] !== undefined ) {
            try
            {
                if(nsdUtility !== undefined  && nsdUtility.UILanguage === "F")
                    return Errors[code].french;
            }
            catch(error){}
            
            return Errors[code].english;
        }
        return code;
    }

    var maxGapForDaysInAMonth = 48;  // hours
    var startDateTime, endDateTime;

    var Entries = [];
    


    Array.prototype.deepJoin = function (seperator) {
        seperator = typeof seperator !== 'undefined' ? seperator : '';

        var result = "";
        this.forEach(function (entry) {
            if (Array.isArray(entry)) {
                result += entry.deepJoin(seperator);
            } else {
                if (entry === null || entry === undefined) {
                    result += seperator;
                } else {
                    result += entry.toString() + seperator;
                }
            }
        });

        return result;
    }


    String.prototype.deepJoin = function () {
        return this;
    }

    function getValidDateTime(dt, newDate){



        dt = dt.deepJoin("");


        if(typeof dt !== "string" && dt.lenght !== 10) return null;



        var parts = dt.match(/.{1,2}/g).map(function(item) {
            return parseInt(item, 10);
        });



        var date = new Date(2000+parts[0], parts[1]-1, parts[2], parts[3], parts[4], 0, 0);



        if ( Object.prototype.toString.call(date) === "[object Date]" ) {

            //detect month overflow using days example 1502290000 will overflow to generate  1 MAR 2015 00:00
            if(date.getMonth() !== parts[1]-1) {                 
                return null;
            }


            // it is a date, check time
            if ( isNaN( date.getTime() ) ) return null;

            // datetime is are good

            return date;
        }

        return null;
     
    }

    var days = ["MON","TUE","WED","THU","FRI","SAT","SUN"];
    function isValidDayRang(a){
        var f = days.indexOf(a[0]);
        var t = days.indexOf(a[2]);

        // check for follwoing days
        if(
             f===-1 || t===-1 || f === t-1 || f===t+1 || f===t || f=== 0 && t === days.length-1 || t=== 0 && f === days.length-1
           ){
            return false;
        }

        return true;
    }

    function isValidTimeRange(a){

        // time part should be minimum 15 min
        var timeParts = a.deepJoin("").replace("-","").match(/.{1,2}/g).map(function(item) {
            return parseInt(item, 10);
        });
        var date1 = new Date(1970,9,31,timeParts[0], timeParts[1],0,0);
        var date2 = (timeParts[2] >= timeParts[0])? new Date(1970,9,31,timeParts[2], timeParts[3],0,0) : new Date(1970,10,01,timeParts[2], timeParts[3],0,0);
        var minuteDif =  Math.round((date2 - date1) / 60000); // minutes;
        if(minuteDif < 15 && minuteDif > 1425 /*fullday*/ ) return false; 

        //if(timeParts[2] < timeParts[0] && minuteDif > 720 ){  // crossing midnight
        //  return false;
        //}

        return true;
    }

    function isValidDays(a){
        var parts = a.deepJoin("").split(" ");

        // should have no dublicates and should not cover whole week
        if(hasDuplicates(parts) || parts.length === 7) return false;

        // order of days should be chronological MON-->SUN
        var orderIsIncorrect = false;
        parts.forEach(
            function(o,i,a){ 
                if(i>0 &&  days.indexOf(a[i-1]) > days.indexOf(a[i])){
                    orderIsIncorrect = true;                                
                    return false;
                }
            });
        if(orderIsIncorrect) return false;

        if(parts.length >=3 && hasThreeInSequence(parts)) return false;


        return true;
    }

    function hasThreeInSequence(values) {

        var bigArray = new Array;
        var i;
        for(i = 0; i < values.length; i++) {
            bigArray[days.indexOf(values[i])] = 1;
        }

        for(i = 0; i < values.length; i++) {
            index = days.indexOf(values[i]);
            if(index === 0 || index === 6) {
                continue;
            }
        
            if(bigArray[index-1] === 1 && bigArray[index+1] === 1) {
                return true;
            }
        }

        return false;
    }

    function hasDuplicates(array) {
        var valuesSoFar = {};
        for (var i = 0; i < array.length; ++i) {
            var value = array[i];
            if (Object.prototype.hasOwnProperty.call(valuesSoFar, value)) {
                return true;
            }
            valuesSoFar[value] = true;
        }
        return false;
    }

}







start = a:(validityPeriodStart " TIL " validityPeriodEnd "\n"  schedule ) { debugger; return  a.splice(4).deepJoin("");}


validityPeriodStart "Start of Validaty Period"= a:(dateTime) { startDateTime =getValidDateTime(a);  if(startDateTime === null) error("Invalid Start of Validaty Period"); } 

validityPeriodEnd "End of Validaty Period" = a:(dateTime) { endDateTime =getValidDateTime(a);  if(endDateTime === null || endDateTime <= startDateTime) error("Invalid End of Validaty Period"); } 
 

schedule = ( (daily/WeekDays) exceptions?) / (DaysInAMonth ("," S DaysInAMonth)*)


daily = (periodWithNoCompleteEvent (" " periodWithNoCompleteEvent)* " DLY")  / (periodWithCompleteEvent(" " periodWithCompleteEvent)*) 
DaysInAMonth = (  (daysInAMonth ("," S daysInAMonth)*) ) / (daysInAMonthWithTime ("," S daysInAMonthWithTime)+ ) 
// TODO - we should prevent three or more continus days 




//////////////////////////////// Month start
daysInAMonth = monthShortName ( !timeOfDay  (S ((dayPart"-"dayPart)/dayPart)) )*  timeOfDay
daysInAMonthWithTime = (monthShortName S dayPart S time "-" dayPart S time)

//////////////////////////////// Month End



///////////////////////////////// Specific Days START

WeekDays =specificDays ("," S specificDays)*  
specificDays = dayRange / dayRangeWithTimeOrEvent 

dayRange = o:((weekDaysRange/days) timeOfDay)
days = o:(day (S day)* ) { Entries.push({type:'Days', data:o.deepJoin("")}); return o;}
dayRangeWithTimeOrEvent = (dayTime / dayEvent) "-" (dayTime /dayEvent)
dayTime = o:(day S time) { Entries.push({type:'Day&time', data:o.deepJoin("")}); return o;}
dayEvent = day S startEvent




// TODO prvent using full week
weekDaysRange = o:(day  "-" day) { Entries.push({type:'Day2Day', data:o.deepJoin("")}); return o;}


day "Day Name (ex. MON, TUE...etc)" = o:("MON"/"TUE"/"WED"/"THU"/"FRI"/"SAT"/"SUN") 
///////////////////////////////// Specific Days END




////////////////////////////////// DAILY START
// DAILY --- TODO full event/eventTime/timeEvent should be allowed once,  
dailyNoDAY = (periodWithNoCompleteEvent (" " periodWithNoCompleteEvent)* )  / (periodWithCompleteEvent(" " periodWithCompleteEvent)*)
timeOfDay =(S (H24/dailyNoDAY))

H24 = o:("H24") { Entries.push({type:'H24', data:o}); return o;}
periodWithNoCompleteEvent = o:(dateRange/timeRange/timeEventRange/eventTimeRange) 
periodWithCompleteEvent  = o:(dateRange/timeRange/eventRange) 

dateRange = o:(dateTime "TIL" dateTime) { Entries.push({type:'DataTime2Datetime', data:o.deepJoin("")}); return o;}

timeRange = o:(time "-" time ) { Entries.push({type:'Time2Time', data:o.deepJoin("")}); return o;}

eventRange = o:(startEvent "-" endEvent) { Entries.push({type:'Event2Event', data:o.deepJoin("")}); return o;}

eventTimeRange = o:(startEvent "-" time) { Entries.push({type:'Event2Time', data:o.join}); return o;}

timeEventRange  = o:(time "-" endEvent) { Entries.push({type:'Time2Event', data:o.deepJoin("")}); return o;}


startEvent  = (ss plus?) / (sr minus?)
endEvent = startEvent

ss  = "SS"
sr = "SR"
plus = S "PLUS" S minutePart S "MIN"
minus = S "MINUS" S minutePart S "MIN"





// Exceptions
exceptions= S "EXC" S dayOfMonth (" " dayOfMonth)* 




// COMMON
date "Date (yyMMdd)"= yearPart monthPart dayPart 
time "Time (HHMM)"= hourPart minutePart
dateTime =  date time
yearPart = d d 
monthPart = a:(!"00" "0" d/ "1" [0-2]) { return a; }
dayPart = a:(!"00" [0-2]d/[3][0-1]) { return a; }
hourPart= [0-1]d/"2"[0-3]
minutePart "Minuites (00-59)" = [0-5] d
secondPart = minutePart
monthShortName "Month Name (ex. JAN,FEB...etc)" = "JAN"/"FEB"/"MAR"/"MAY"/"JUN"/"JUL"/"AUG"/"SEP"/"OCT"/"NOV"/"DEC"
dayOfMonth = monthShortName S dayPart 


// TOOLs
S  =" "
d = [0-9]




////////////////////////////////// DAILY END
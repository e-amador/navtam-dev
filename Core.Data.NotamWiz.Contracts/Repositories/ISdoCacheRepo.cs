﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{


    public interface ISdoCacheRepo : IRepository<SdoCache>
    {
        Task<SdoCache> GetByIdAsync(string id);

        List<SdoCache> GetAllByType(SdoEntityType type);

        List<SdoCache> GetAllByTypeIncludingDeleted(SdoEntityType type);

        Task<List<SdoCache>> GetAllByTypeAsync(SdoEntityType type);

        Task<List<SdoCache>> GetChildrenByTypeAsync(string parentId, SdoEntityType childType);

        Task<SdoCache> GetByDesignatorAsync(string designator);

        List<SdoCache> GetFirs();
        Task<List<SdoCache>> GetFirsAsync();
        SdoCache GetFirContaining(DbGeography geoLoc);

        bool FirExists(string designator);
        DbGeography GetFirGeography(string designator);

        Task<SdoCache> GetFirByDesignatorAsync(string designator);

        Task<List<KeyValuePair<string, string>>> GetAerodromesAndFirsAsync();

        SdoCache GetAerodromeByDesignator(string designator);
        List<SdoCache> GetAerodromeRunways(string ahpId);

        Task<List<SdoSubjectDto>> FilterAerodromesInArea(string match, int pageSize, DbGeography geoLoc);
        Task<List<SdoSubjectDto>> FilterAerodromesOutsideArea(string match, int pageSize, DbGeography geoLoc);

        Task<List<SdoSubjectDto>> FilterCatchAllSubjectsAsync(string match, int pageSize, DbGeography geoLoc);

        Task<List<SdoSubjectDto>> FilterAerodromesWithRunwaysAsync(string match, int pageSize, DbGeography geoLoc);

        Task<List<SdoSubjectDto>> GetAerodromeRunwaysAsync(string ahpId);

        Task<List<SdoSubjectDto>> FilterAerodromesWithTaxiwaysAsync(string match, int pageSize, DbGeography geoLoc);

        Task<List<SdoSubjectDto>> GetAerodromeTaxiwaysAsync(string ahpId);

        List<SdoSubjectDto> GetAerodromeTaxiways(string ahpId);

        Task<List<SdoSubjectDto>> FilterAerodromesWithCarsAsync(string match, DbGeography geoLoc);

        Task<int> GetTotalAerodromesWithouDisseminationCategoryAsync();

        Task<int> GetTotalAerodromesWithouDisseminationCategoryAsync(QueryOptions queryOptions);

        Task<List<NearbyAerodromeDto>> GetNearbyAerodromesAsync(DbGeography dbGeo, double distance);
        Task<List<NearbyAerodromeDto>> GetNearbyInternationalAerodromesAsync(DbGeography dbGeo, double distance);

        Task<List<AerodromeWithoutDisseminationCategoryDto>> FilterAerodromesWithouDisseminationCategoryAsync(QueryOptions queryOptions);

        bool IsWaterAerodrome(string ahpMid);

        Task UpdateKeywords();

        string GetServedCity(SdoCache sdo);

        DateTime? GetLatestEffectiveDate();
    }
}
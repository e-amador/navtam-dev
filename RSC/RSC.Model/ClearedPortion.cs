﻿namespace RSC.Model
{
    /// <summary>
    /// Cleared portion of runway - Choice of either average of runway surface or 3 thirds of runway.
    /// </summary>
    public class ClearedPortion
    {
        /// <summary>
        /// Average of the Runway Surface.
        /// </summary>
        public ClearedSurface Average { get; set; }
        /// <summary>
        /// List of Third of Runway (expected 3 thirds)
        /// </summary>
        public ClearedSurfaceThird[] RunwayThirds { get; set; }

        /// <summary>
        /// Is Average or Thirds
        /// </summary>
        public bool IsAverage => Average != null;
    }
}
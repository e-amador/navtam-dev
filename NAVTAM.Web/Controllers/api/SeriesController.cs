﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [RoutePrefix("api/series")]
    [Authorize]
    public class SeriesController : ApiBaseController
    {
        public SeriesController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger) 
            : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
        }

        [HttpGet]
        [Route("GetSerie")]
        public async Task<IHttpActionResult> GetSerie(string subjectId, string qCode)
        {
            return await LogAction(async () =>
            {
                var unknownSeries = "";

                var subject = await Uow.SdoCacheRepo.GetByIdAsync(subjectId);
                if (subject == null)
                    return Ok(unknownSeries);

                var region = await Uow.GeoRegionRepo.GetByGeoLocationAsync(subject.RefPoint);
                if (region == null)
                    return Ok(unknownSeries);

                var dc = await GetSubjectDisseminationCategory(subject);
                var seriesAllocation = await Uow.SeriesAllocationRepo.GetByRegionIdAndCodeAndCategoryIdAsync(region.Id, qCode, dc);
                var series = seriesAllocation.Count != 0 ? seriesAllocation.First().Series : unknownSeries;

                return Ok(series);

            }, "api/series/getseries");
        }

        [HttpPost, Route("page", Name = "RegisteredSeriesPage")]
        [Authorize(Roles = "NOF_ADMINISTRATOR, Administrator")]
        public async Task<IHttpActionResult> GetSeriesInPage([FromBody]QueryOptions queryOptions)
        {
            return await LogAction(async () =>
            {
                if (queryOptions.PageSize > MaxRecordsToReturn)
                    queryOptions.PageSize = MaxRecordsToReturn;

                var totalCount = await Uow.SeriesAllocationRepo.GetAllCountAsync(queryOptions);
                var series = await Uow.SeriesAllocationRepo.GetAllAsync(queryOptions);
                if (series.Count == 0 && queryOptions.Page > 1)
                {
                    queryOptions.Page = queryOptions.Page - 1;
                    series = await Uow.SeriesAllocationRepo.GetAllAsync(queryOptions);
                }

                return Ok(ResponsewithPagingEnvelope(queryOptions, "RegisteredSeriesPage", series, totalCount));

            }, "api/series/page");
        }

        [HttpDelete, Route("delete/{id}")]
        [Authorize(Roles = "NOF_ADMINISTRATOR, Administrator")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            return await LogAction(async () =>
            {
                var serie = await Uow.SeriesAllocationRepo.GetByIdAsync(id);
                if (serie == null)
                    return NotFound();

                Uow.SeriesAllocationRepo.Delete(serie);
                await Uow.SaveChangesAsync();

                Logger.LogInfo(GetType(), $"DELETE: {SeriesAllocationSummary(serie)} deleted by '{ApplicationContext.GetUserName()}'.");

                return Ok();

            }, $"api/series/delete/{id}");
        }

        [HttpGet, Route("getserie")]
        public async Task<IHttpActionResult> Get(int id)
        {
            return await LogAction(async () =>
            {
                var serie = await Uow.SeriesAllocationRepo.GetByIdWithRegionAsync(id);

                var serieDto = new SerieAllocationDto
                {
                    Id = serie.Id,
                    RegionId = serie.RegionId,
                    RegionName = serie.Region.Name,
                    DisseminationCategory = serie.DisseminationCategory,
                    DisseminationCategoryName = serie.DisseminationCategory.ToString(),
                    QCode = serie.QCode,
                    Series = serie.Series,
                    Subject = serie.Subject,
                };

                return Ok(serieDto);

            }, $"api/series/getserie/{id}");
        }

        [HttpPost, Route("create")]
        [Authorize(Roles = "NOF_ADMINISTRATOR, Administrator")]
        public async Task<IHttpActionResult> Create([FromBody]SerieAllocationDto serieVm)
        {
            return await LogAction(async () =>
            {
                if (!ModelState.IsValid)
                    return BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid);

                var serie = new SeriesAllocation
                {
                    Subject = serieVm.Subject,
                    RegionId = serieVm.RegionId,
                    DisseminationCategory = serieVm.DisseminationCategory,
                    QCode = serieVm.QCode,
                    Series = serieVm.Series
                };

                Uow.SeriesAllocationRepo.Add(serie);
                await Uow.SaveChangesAsync();

                Logger.LogInfo(GetType(), $"NEW: {SeriesAllocationSummary(serie)} created  by '{ApplicationContext.GetUserName()}'.");

                return Ok(serieVm);

            }, $"api/series/create");
        }

        [HttpPut, Route("update")]
        [Authorize(Roles = "NOF_ADMINISTRATOR, Administrator")]
        public async Task<IHttpActionResult> Update([FromBody]SerieAllocationDto serieVm)
        {
            return await LogAction(async () =>
            {
                if (!ModelState.IsValid)
                    return BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid);

                var serie = await Uow.SeriesAllocationRepo.GetByIdAsync(serieVm.Id);
                if (serie == null)
                    return NotFound();

                var beforeSummary = SeriesAllocationSummary(serie);

                serie.Subject = serieVm.Subject;
                serie.DisseminationCategory = serieVm.DisseminationCategory;
                serie.QCode = serieVm.QCode;
                serie.RegionId = serieVm.RegionId;
                serie.Series = serieVm.Series;

                Uow.SeriesAllocationRepo.Update(serie);
                await Uow.SaveChangesAsync();

                var afterSummary = SeriesAllocationSummary(serie);

                Logger.LogInfo(GetType(), $"UPDATE: {beforeSummary} updated to {afterSummary}  by '{ApplicationContext.GetUserName()}'.");

                return Ok(serieVm);

            }, $"api/series/update");
        }

        [HttpGet, Route("candelete")]
        [Authorize(Roles = "Administrator, CCS, NOF_ADMINISTRATOR")]
        public async Task<IHttpActionResult> CanBeDeleted(int serieId)
        {
            return await LogAction(async () =>
            {
                
                var series = await Uow.SeriesAllocationRepo.GetByIdAsync(serieId);
                if (series == null) return BadRequest();
                var result = await Uow.ProposalHistoryRepo.GetCountBySeries(series.Series);
                
                return Ok((result == 0) ? true : false);
            }, $"api/series/candelete/{serieId}");
        }

        private async Task<DisseminationCategory> GetSubjectDisseminationCategory(SdoCache subject)
        {
            var defaultCategory = DisseminationCategory.International;
            if (subject == null)
                return defaultCategory;

            if (subject.Type == SdoEntityType.Fir)
                return DisseminationCategory.National;

            if (subject.Type != SdoEntityType.Aerodrome)
                subject = subject.Parent;

            if (subject == null)
                return defaultCategory;

            var adc = await Uow.AerodromeDisseminationCategoryRepo.GetByAhpMidAsync(subject.Id);
            return adc != null ? adc.DisseminationCategory : defaultCategory;
        }

        static string SeriesAllocationSummary(SeriesAllocation series)
        {
            return $"SeriesAllocation(Series: '{series.Series}', QCode: '{series.QCode}', Region: '{series.RegionId}', Dissemination Category: '{series.DisseminationCategory}', Subject: '{series.Subject}')";
        }
    }
}
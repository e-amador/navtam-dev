﻿import { NgModule, APP_INITIALIZER, Injectable } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Select2Module } from 'ng2-select2';
import { DateTimePickerModule } from '../common/components/ng2-date-time-picker';

//import { StartupService } from './startup.service';

import { LocalizationModule, LocaleService, TranslationService } from 'angular-l10n';

import {
    JQUERY_TOKEN, //OR JQUERY_PROVIDER,
    TOASTR_TOKEN,
    Toastr,
    WindowRef,
    XsrfTokenService,
    Ng2ICheckModule,
    SimpleModalComponent,
    ServerPaginationComponent,
    ModalTriggerDirective,
    DateTimeFormatDirective,
    DatexPipe,
    SystemStatusComponent} from '../common/index';

import { AdminComponent } from './admin.component';

import {
    //Shared
    DataService,
    MemoryStorageService,
    SidebarComponent,
    //User
    UserListComponent,
    UserEditComponent,
    UserNewComponent,
    UserResetPasswordComponent,
    //Org
    OrgListComponent,
    OrgNewComponent,
    OrgEditComponent,
    //IcaoSubjects
    IcaoSubjectsListComponent,
    IcaoSubjectEditComponent,
    IcaoSubjectNewComponent,
    IcaoConditionEditComponent,
    IcaoConditionNewComponent,
    //SeriesAllocation
    SeriesListComponent,
    SeriesNewComponent,
    SeriesEditComponent,
    //AerodromeDissemination Category
    ADCTabListComponent,
    ADCAddSdoComponent,
    ADCEditComponent,
    //DOAS
    DoasListComponent,
    DoaNewComponent,
    DoaEditComponent,
    DoaEditModifyComponent,
    //BilingualRegion
    BilingualRegionEditComponent,
    //NorthernRegion
    NorthernRegionEditComponent,
    //NSDs
    NsdListComponent,
    //GeoRegion
    GeoRegionListComponent,
    GeoRegionShowComponent,
    GeoRegionNewComponent,
    //Config
    ConfigListComponent,
    ConfigNewComponent,
    //Checklist
    ChecklistComponent,
    SeriesChecklistNewComponent,
    //AeroRDS
    AerordsComponent,
    //Subscription
    SubscComponent,
    SubscEditComponent,
    SubscCreateComponent,
    //Resolvers
    RolesResolver,
    UserModelResolver,
    OrgModelResolver,
    SeriesModelResolver,
    IcaoSubjectModelResolver,
    IcaoConditionModelResolver,
    AhpModelResolver,
    AdcModelResolver,
    DoaModelResolver,
    DoaCopyModelResolver,
    GeoRegionModelResolver,
    BilingualRegionModelResolver,
    NorthernRegionModelResolver,
    ConfigModelResolver,
    ChecklistModelResolver,
    SeriesChecklistResolver,
    SubscriptionResolver,
    //Location Service
    LocationService,
} from './index'

import { adminRoutes } from './admin.routes';

declare let toastr: Toastr;

//export function startupServiceFactory(startupService: StartupService): Function {
//    return () => startupService.load();
//}

@Injectable()
export class LocalizationConfig {
    resourceCount: number = 0;
    constructor(public locale: LocaleService, public translation: TranslationService) { }

    load(): Promise<any> {
        this.locale.addConfiguration()
            .addLanguage('en', 'ltr')
            .addLanguage('fr', 'ltr')
            //.setCookieExpiration(30)
            .defineLanguage(window['app'].cultureInfo)
            .defineCurrency("CAD")
            .defineDefaultLocale('en', 'CA')
        this.locale.init();

        this.translation.addConfiguration()
            .addProvider('./app/resources/admin.locale-');

        let promise: Promise<any> = new Promise((resolve: any) => {
            this.translation.translationChanged.subscribe(() => {
                resolve(true);
            });
        });

        this.translation.init();

        return promise;
    }
}

// AoT compilation requires a reference to an exported function.
export function initLocalization(localizationConfig: LocalizationConfig): Function {
    return () => localizationConfig.load();
}

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forRoot(adminRoutes),
        Select2Module,
        DateTimePickerModule,
        Ng2ICheckModule,
        LocalizationModule.forRoot() // New instance of TranslationService
    ],
    declarations: [
        AdminComponent,
        //common
        DatexPipe,
        DateTimeFormatDirective,
        ModalTriggerDirective,
        SimpleModalComponent,
        ServerPaginationComponent,
        SidebarComponent,
        SystemStatusComponent,
        //User,
        UserListComponent,
        UserEditComponent,
        UserNewComponent,
        UserResetPasswordComponent,
        //Org
        OrgListComponent,
        OrgNewComponent,
        OrgEditComponent,
        //IcaoSubjects
        IcaoSubjectsListComponent,
        IcaoSubjectEditComponent,
        IcaoSubjectNewComponent,
        IcaoConditionEditComponent,
        IcaoConditionNewComponent,
        //SeriesAllocation
        SeriesListComponent,
        SeriesNewComponent,
        SeriesEditComponent,
        //AerodromeDisseminationCategory
        ADCTabListComponent,
        ADCAddSdoComponent,
        ADCEditComponent,
        //DOAS
        DoasListComponent,
        DoaNewComponent,
        DoaEditComponent,
        DoaEditModifyComponent,
        //BilingualRegion
        BilingualRegionEditComponent,
        //NorthernRegion
        NorthernRegionEditComponent,
        //NSDs
        NsdListComponent,
        //GeoRegion
        GeoRegionListComponent,
        GeoRegionShowComponent,
        GeoRegionNewComponent,
        //Config
        ConfigListComponent,
        ConfigNewComponent,
        //Checklist
        ChecklistComponent,
        SeriesChecklistNewComponent,
        //AeroRDS
        AerordsComponent,
        //Subscription
        SubscComponent,
        SubscEditComponent,
        SubscCreateComponent,
    ],
    providers: [
        DataService,
        MemoryStorageService,
        RolesResolver,
        UserModelResolver,
        OrgModelResolver,
        SeriesModelResolver,
        AhpModelResolver,
        AdcModelResolver,
        DoaModelResolver,
        DoaCopyModelResolver,
        IcaoSubjectModelResolver,
        IcaoConditionModelResolver,
        GeoRegionModelResolver,
        BilingualRegionModelResolver,
        NorthernRegionModelResolver,
        ConfigModelResolver,
        ChecklistModelResolver,
        SeriesChecklistResolver,
        //StartupService,
        LocationService,
        SubscriptionResolver,
        LocalizationConfig,
        {
            provide: APP_INITIALIZER,
            useFactory: initLocalization,
            deps: [LocalizationConfig],
            multi: true
        },
        //common
        //{
        //    // Provider for APP_INITIALIZER
        //    provide: APP_INITIALIZER,
        //    useFactory: startupServiceFactory,
        //    deps: [StartupService],
        //    multi: true
        //},
        { provide: TOASTR_TOKEN, useValue: toastr },
        XsrfTokenService,
        WindowRef
    ],
    bootstrap: [AdminComponent]
})
export class AdminModule { }
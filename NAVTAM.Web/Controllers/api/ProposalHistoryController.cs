﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Common.Extensions;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using NAVTAM.NsdEngine.Helpers;
using NAVTAM.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [RoutePrefix("api/proposalhistory")]
    [Authorize]
    public class ProposalHistoryController : ApiBaseController
    {
        public ProposalHistoryController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, INsdManagerResolver managerResolver, ILogger logger) 
            : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
            _managerResolver = managerResolver;
        }

        [Route("GetModel")]
        [HttpGet]
        public async Task<IHttpActionResult> GetModel(int id)
        {
            return await LogAction(async () =>
            {
                var proposalHistory = await Uow.ProposalHistoryRepo.GetByIdAsync(id);

                if (proposalHistory == null)
                    return NotFound();

                var proposal = new Proposal();
                proposalHistory.CopyPropertiesTo(proposal);
                proposal.Id = proposalHistory.ProposalId;

                //ApplyCurrentCulture();
                var nsdManager = _managerResolver.GetNsdManager($"{proposal.CategoryId}-{proposal.Version}");

                var model = nsdManager.CreateViewModelFromProposal(proposal);
                model.Attachments = await GetAllProposalAttachments(proposal);
                model.Token.Annotations = SortAnnotationsByAscendingOrder(model.Token.Annotations);
                model.ParentNotamId = await RetrieveParentNotamId(proposal);

                model.IcaoText = ICAOFormatUtils.Generate(model, true);

                return Ok(model);

            }, "api/proposalhistory/getmodel");
        }

        [Route("{proposalId}")]
        public async Task<IHttpActionResult> GetAll(int proposalId)
        {
            return await LogAction(async () => Ok(await Uow.ProposalHistoryRepo.GetByProposalIdPartialAsync(proposalId)), $"api/proposalhistory/{proposalId}");
        }

        [Route("filter", Name = "FilterProposalHistories")]
        [HttpPost]
        public async Task<IHttpActionResult> FilterProposals([FromBody]FilterCriteria filterCriteria)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                var orgId = int.Parse(ApplicationContext.GetCookie("org_id", "0"));
                var org = await Uow.OrganizationRepo.GetByIdAsync(orgId);
                if (org == null)
                    return Unauthorized();

                var userDoa = await GeoLocationCache.GetUserDoa(Uow, ApplicationContext.GetUserId());
                var filterQuery = Uow.ProposalHistoryRepo.PrepareQueryFilter(filterCriteria);
                var totalCount = await Uow.ProposalHistoryRepo.FilterCountAsync(filterCriteria, org.Id, org.Type, userDoa, filterQuery);
                var proposals = await Uow.ProposalHistoryRepo.FilterAsync(filterCriteria, org.Id, org.Type, userDoa, filterQuery);

                return Ok(ResponsewithPagingEnvelope(filterCriteria.QueryOptions, "FilterProposalHistories", proposals, totalCount));

            }, "api/proposalhistory/filter");
        }

        [Route("queryfilter")]
        [HttpPost]
        public async Task<IHttpActionResult> SaveUserQueryFilter(UserQueryFilterViewModel model)
        {
            return await LogAction(async () =>
            {
                if (!ModelState.IsValid)
                    return BadRequest("Invalid Request");

                if (!ValidUserRequest())
                    return Unauthorized();

                if (model.Username != ApplicationContext.GetUserName())
                    return Unauthorized();

                var userFilter = await Uow.UserQueryFilterRepo.GetProposalFiltersBySlotAndUsernameAsync(model.SlotNumber, model.Username);
                if (userFilter != null)
                {
                    userFilter.JsonBundle = Newtonsoft.Json.JsonConvert.SerializeObject(model.FilterConditions);
                    Uow.UserQueryFilterRepo.Update(userFilter);
                }
                else
                {
                    Uow.UserQueryFilterRepo.Add(new UserQueryFilter
                    {
                        FilterName = model.FilterName,
                        SlotNumber = model.SlotNumber,
                        Username = model.Username,
                        UserFilterType = UserFilterType.ProposalHistory,
                        JsonBundle = Newtonsoft.Json.JsonConvert.SerializeObject(model.FilterConditions)
                    });
                }
                await Uow.SaveChangesAsync();

                return Ok();

            }, "api/proposalhistory/queryfilter");
        }

        [Route("queryfilter/{slotNumber}")]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteUserQueryFilter(int slotNumber)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                var userFilter = await Uow.UserQueryFilterRepo.GetProposalFiltersBySlotAndUsernameAsync(slotNumber, System.Web.HttpContext.Current.User.Identity.Name);
                if (userFilter == null)
                    return NotFound();

                Uow.UserQueryFilterRepo.Delete(userFilter);
                await Uow.SaveChangesAsync();

                return Ok();

            }, $"api/proposalhistory/queryfilter/{slotNumber}");
        }


        [Route("queryfilter/active/{username}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetActiveUserQueryFilters(string username)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                if (username != ApplicationContext.GetUserName())
                    return Unauthorized();

                var userFilters = await Uow.UserQueryFilterRepo.GetAllProposalFiltersByUsernameAsync(username);

                var response = new List<int>();
                foreach (var userFilter in userFilters)
                    response.Add(userFilter.SlotNumber);

                return Ok(response);

            }, $"api/proposalhistory/active/{username}");
        }

        [Route("queryfilter/{slotNumber}/{username}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetUserQueryFilter(int slotNumber, string username)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                if (username != ApplicationContext.GetUserName())
                    return Unauthorized();

                var userFilter = await Uow.UserQueryFilterRepo.GetProposalFiltersBySlotAndUsernameAsync(slotNumber, username);
                if (userFilter == null)
                    return NotFound();

                var response = JsonConvert.DeserializeObject<FilterCondition[]>(userFilter.JsonBundle, new JsonSerializerSettings
                {
                    ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver()
                });

                return Ok(response);

            }, $"api/proposalhistory/queryfilter/{slotNumber}/{username}");
        }

        private INsdManagerResolver _managerResolver;
    }
}

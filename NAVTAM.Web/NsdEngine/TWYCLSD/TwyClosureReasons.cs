﻿using System.Collections.Generic;

namespace NAVTAM.NsdEngine.TWYCLSD
{
    public static class TwyClosureReasons
    {
        public static List<TwyClosureReason> GetClosureReasons()
        {
            var twyClsdReasons = new List<TwyClosureReason>() {
                //The first is empty because this field is optional: the user can select empty to clean the selection
                new TwyClosureReason(0,"NO SPECIFIED","NON SPECIFIE"),
                new TwyClosureReason(1, "PAINTING", "PEINTURE"),
                new TwyClosureReason(2, "LINE PAINTING", "PEINTURE DE LIGNES"),
                new TwyClosureReason(3, "CONSTRUCTION", "CONSTRUCTION" ),
                new TwyClosureReason(4, "MAINTENANCE", "ENTRETIEN" ),
                new TwyClosureReason(5, "CRACK FILLING", "COLMATAGE" ),
                new TwyClosureReason(6, "RESURFACING", "SURACAGE DE REVETEMENT" ),
                new TwyClosureReason(7, "GRADING AND PACKING", "NIVELAGE ET REMPLISSAGE" ),
                new TwyClosureReason(8, "DISABLED ACFT", "ACFT EN PANNE" ),
                new TwyClosureReason(9, "OTHER", "AUTRE" )
            };

            return twyClsdReasons;
        }

        public static List<TwyClosureReason> GetItemEClosureReasons()
        {
            var twyItemEClsdReasons = new List<TwyClosureReason>() {
                //The first is empty because this field is optional: the user can select empty to clean the selection
                new TwyClosureReason(0,"NO SPECIFIED","NON SPECIFIE"),
                new TwyClosureReason(1, "PAINTING", "PEINTURE"),
                new TwyClosureReason(2, "LINE PAINTING", "PEINTURE DE LIGNES"),
                new TwyClosureReason(3, "CONST", "CONST" ),
                new TwyClosureReason(4, "MAINT", "MAINT" ),
                new TwyClosureReason(5, "CRACK FILLING", "COLMATAGE" ),
                new TwyClosureReason(6, "RESURFACING", "SURACAGE DE REVETEMENT" ),
                new TwyClosureReason(7, "GRADING AND PACKING", "NIVELAGE ET REMPLISSAGE" ),
                new TwyClosureReason(8, "DISABLED ACFT", "ACFT EN PANNE" ),
                new TwyClosureReason(9, "OTHER", "AUTRE" )
            };

            return twyItemEClsdReasons;
        }
    }

    public class TwyClosureReason
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string TextFr { get; set; }

        public TwyClosureReason(int id, string text, string textFr)
        {
            Id = id;
            Text = text;
            TextFr = textFr;
        }
    }
}
﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    public class CategoryViewModel
    {
        public long CategoryID { get; set; }
        public string CategoryName { get; set; }
    }
}
﻿using NSD.SolaceBus.Configuration;
using SolaceSystems.Solclient.Messaging;
using System;
using INdsMessage = NDS.DataBus.Contracts.IMessage;
using INdsPostQueue = NDS.DataBus.Contracts.IPostQueue;

namespace NSD.SolaceBus
{
    public class SolaceTopicPoster : INdsPostQueue
    {
        public SolaceTopicPoster(SessionConfiguration config, string topicPath)
        {
            _context = ContextFactory.Instance.CreateContext(new ContextProperties(), null);
            var sessionProperties = SessionUtils.NewSessionPropertiesFromConfig(config);

            _session = _context.CreateSession(sessionProperties, MessageHandler, SessionHandler);
            if (_session.Connect() != ReturnCode.SOLCLIENT_OK)
                throw new Exception("Cannot start VMR session!");

            _topic = ContextFactory.Instance.CreateTopic(topicPath);
        }

        public void PostMessage(INdsMessage message)
        {
            var solaceMessage = message as SolaceMessageWrapper;
            if (solaceMessage == null)
                throw new ArgumentException("Unexpected message type!");

            solaceMessage.InnerMessage.Destination = _topic;

            if (_session.Send(solaceMessage.InnerMessage) != ReturnCode.SOLCLIENT_OK)
                throw new Exception("Could not send the message!");
        }

        private void SessionHandler(object sender, SessionEventArgs e)
        {
        }

        private void MessageHandler(object sender, MessageEventArgs e)
        {
        }

        private IContext _context;
        private ISession _session;
        private ITopic _topic;

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.
                _session?.Dispose();
                _session = null;

                _topic?.Dispose();
                _topic = null;

                _context?.Dispose();
                _context = null;

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~SolaceTopicPoster() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}

﻿namespace RSC.Model.Enums
{
    public enum MeasurementDeviceEnum : int
    {
        TapleyMeter = 130, // TAPLEY METRE
        JamesDecelerometer = 131, // DECELEROMETRE JAMES
        Bowmonk = 132, // BOWMONK
        DecelerometerTES = 133 // DECELEROMETRE TES 
    }
}
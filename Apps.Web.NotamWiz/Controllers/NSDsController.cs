﻿using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    using Kendo.Mvc.Extensions;
    using Kendo.Mvc.UI;

    using Microsoft.AspNet.Identity;

    using Code.Extentions;
    using Models;
    using Core.Common.Contracts;
    using Core.Data.NotamWiz.Contracts;
    using Core.Domain.Model.Entitities;
    using AutoMapper;

    [Authorize]
    public class NsdsController : BaseController
    {
        public NsdsController(IUow uow, IClusterQueues clusterQueues) 
            : base(uow, clusterQueues)
        {
        }
        private static readonly ILog Log = LogManager.GetLogger(typeof(NsdsController));


        [HttpPost]
        public ActionResult GetAvailableNsds(NotamSubjectViewModel subject)
        {
            Dictionary<string, Object> viewParameters = new Dictionary<string, Object>();
            viewParameters.Add("subject", subject);
            return PartialView("AvailableNsdList", viewParameters);
        }

        /* Populates NOTAM Scenario Window */
        public async Task<ActionResult> GetNsdsForSubject([DataSourceRequest]DataSourceRequest request, NotamSubjectViewModel subject)
        {
            NotamSubjectViewModel Subject;
            Subject = Mapper.Map<NotamSubjectViewModel>(Uow.SdoSubjectRepo.GetById(subject.SubjectId), opt =>
            {
                opt.Items["db"] = Uow;
            });

            var currentUser = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            Organization userOrg = Uow.OrganizationRepo.GetById(currentUser.OrganizationId);

            IList<string> userRoleLists = ((ClaimsIdentity)User.Identity).Claims
                .Where(c => c.Type == ClaimTypes.Role)
                .Select(c => c.Value).ToList();

            string[] userRoles = new string[userRoleLists.Count];
            userRoleLists.CopyTo(userRoles, 0);
            var allActive = await Uow.NsdRepo.GetAllActiveAsync(userOrg.Id, subject.SubjectType);

            var nsds = new List<Nsd>();
           
            foreach (var n in allActive)
            {
                if (string.IsNullOrEmpty(n.Versions.Last().AssociatedRoles) || n.Versions.Last().AssociatedRoles.ContainsAny(userRoles))
                {
                    // TODO (Omar): use filter to check SDO using odata instead of SDO database.

                    /*if (n.Versions.Last().SubjectsFilter != null && n.Versions.Last().SubjectsFilter.Length > 0)
                    {
                        string sql = n.Versions.Last().SubjectsFilter.Replace("@subjectMid", subject.SubjectMid);

                        int passed = SDOdb.Database.SqlQuery<int>(sql).FirstOrDefault();
                        if (passed == 1)
                        {
                            nsds.Add(n);
                        }
                    }
                    else*/
                    if (n.Name.Equals("CARS CLSD"))
                    {
                        if (Subject.SdoAttributes != null && Subject.SdoAttributes.Contains("CARS : True"))
                        {
                            nsds.Add(n);
                        }
                    }
                    else
                    {
                        nsds.Add(n);
                    }

                  
                }
            }

            var results = nsds.Select(d => new NsdInfoViewModel()
            {
                Id = d.Id,
                Name = d.Name,
                Category = d.Category,
                Description = d.Description,
                FileLocation = d.FilesLocation,
                Version = d.Versions.Last().Version,
                HasView = d.Versions.Last().HasView,
                ForSubjectType = d.ForSubjectType
            }).ToList();

            return Json(results.ToDataSourceResult(request));

        }
    }
}
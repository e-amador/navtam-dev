﻿using RSC.Model.Enums;

namespace RSC.Model
{
    /// <summary>
    /// Complex structure describing the surface condition of the cleared portion.  This structure allows reporting in thirds or as an average across the entire surface.
    /// </summary>
    public class RunwayClearedPortion : ClearedPortion
    {
        /// <summary>
        /// Treatment applied to this runway.
        /// </summary>
        public TreatmentEnum[] Treatments { get; set; }
        /// <summary>
        /// An additional text comment may be provided. This field should only be used
        /// for information that cannot be accommodated using existing defined fields.
        /// </summary>
        public FreeformText ContaminantComment { get; set; }
        /// <summary>
        /// Complex structure describing additional Runway Surface Conditions.
        /// </summary>
        public OtherConditions OtherConditions { get; set; }
        /// <summary>
        /// Complex Structure describing the Runway Friction.
        /// </summary>
        public RwyFriction AverageRwyFriction { get; set; }
    }
}
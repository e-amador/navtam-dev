﻿namespace NAVTAM.ViewModels
{
    public class TimePeriod
    {
        public int StartHours;
        public int StartMinutes;
        public int EndHours;
        public int EndMinutes;

        public void SanitizeData()
        {
            if (StartHours < 0) StartHours = 0;
            if (EndHours < 0) EndHours = 0;
            if (StartMinutes < 0) StartMinutes = 0;
            if (EndMinutes < 0) EndMinutes = 0;

            if( StartHours > 23 ) StartHours = 23;
            if (EndHours > 23) EndHours = 23;

            if (StartMinutes > 59) StartHours = 59;
            if (EndMinutes > 59) EndMinutes = 59;
        }
    }
}
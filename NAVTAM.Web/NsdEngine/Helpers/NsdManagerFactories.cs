﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NAVTAM.NsdEngine.Helpers
{
    public class NsdManagerFactories
    {
        private readonly IDictionary<string, Func<INsdManager>> _nsdManagerFactories;

        public NsdManagerFactories()
        {
            _nsdManagerFactories = GetFactories();
        }

        public IDictionary<string, Func<INsdManager>> GetFactories()
        {
            return new Dictionary<string, Func<INsdManager>>
            {
                { "2-V10", () => new CATCHALL.V10.CatchAllNsdManager(2, "CatchAll", "V10") },
                { "4-V10", () => new OBST.V10.ObstNsdManager(4, "Obst", "V10") },
                { "5-V10", () => new OBSTLGUS.V10.ObstLgUsNsdManager(5, "ObstLgUs", "V10") },
                { "7-V10", () => new RWYCLSD.V10.RwyClsdNsdManager(7, "RwyClsd", "V10") },
                { "10-V10", () => new TWYCLSD.V10.TwyClsdNsdManager(10, "TwyClsd", "V10") },
                { "11-V10", () => new TWYCLSD.V10.TwyClsdNsdManager(11, "TwyClsd", "V10") },
                { "13-V10", () => new CARSCLSD.V10.CarsClsdNsdManager(13, "CarsClsd", "V10") },
                { "14-V10", () => new MOBST.V10.MObstNsdManager(14, "MObst", "V10") },
                { "15-V10", () => new MOBSTLGUS.V10.MObstLgUsNsdManager(15, "MObstLgUs", "V10") },
               	{ "18-V10", () => new RSC.V10.RSCNsdManager(18, "RSC", "V10")  }, 
               	{ "19-V10", () => new AIRSPACE.V10.AreaDefinitionNsdManager(19, "AreaDefinition", "V10")} 
            };
        }

        public Func<INsdManager> GetAgentFactory(string key)
        {
            Func<INsdManager> factory = null;

            if ( key.Contains("-V"))
            {
                _nsdManagerFactories.TryGetValue(key, out factory);
            }
            else
            {
                var latestVersionKey = _nsdManagerFactories.Where(e => e.Key.StartsWith(key)).LastOrDefault(); 
                if(latestVersionKey.Key != null )
                    _nsdManagerFactories.TryGetValue(latestVersionKey.Key, out factory);
            }

            return factory;
        }
    }
}

﻿namespace NavCanada.Core.Domain.Model.Dtos
{
    public class MultiPolygonDto:GeometryDto
    {
        public double[][][][] Coordinates { get; set; }
    }
}

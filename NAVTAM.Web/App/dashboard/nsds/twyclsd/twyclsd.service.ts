﻿import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { XsrfTokenService } from '../../../common/xsrf-token.service'
import { WindowRef } from '../../../common/windowRef.service'

@Injectable()
export class TwyClsdService {
    constructor(private http: Http, winRef: WindowRef, private xsrfTokenService: XsrfTokenService) {
        this.app = winRef.nativeWindow['app'];
    }

    public app: any;

    getSdoTwys(sdoId: string): Observable<any> {
        return this.http.get(`${this.app.apiUrl}taxiways/GetTwys/?sdoId=${sdoId}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getTwyClsdReasons(): Observable<any> {
        return this.http.get(`${this.app.apiUrl}taxiways/GetTwyClsdReasons`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    private handleError(error: Response) {
        return Observable.throw(error.statusText);
    }

}
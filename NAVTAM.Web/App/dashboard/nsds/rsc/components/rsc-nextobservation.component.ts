﻿import { Component, OnInit, OnChanges, OnDestroy, Inject, Input } from '@angular/core';
import { TranslationService } from 'angular-l10n';
import { FormGroup, FormBuilder, FormArray, AbstractControl, Validators } from '@angular/forms'
import { INextObservationModel } from '../rsc.model'
import { RscService } from '../rsc.service'

import {
} from '../../../shared/data.model';

import * as moment from 'moment';
import { TOASTR_TOKEN, Toastr } from '../../../../common/toastr.service';

@Component({
    selector: 'rsc-nextobservation',
    templateUrl: './app/dashboard/nsds/rsc/components/rsc-nextobservation.component.html'
})
export class NextObservationComponent implements OnInit, OnChanges, OnDestroy {
    @Input('form') mainForm: FormGroup = null;
    @Input('isReadOnly') isReadOnly: boolean = false;
    @Input('model') model: INextObservationModel = null;

    frmGroup: FormGroup;
    isFormLoadingData: boolean;

    nextScheduledObservationMoment: any;
    nextScheduledObservationText: string = "";
    dateFormat: string = "YYMMDDHHmm";

    constructor(
        private fb: FormBuilder,
        public translation: TranslationService,
        private rscService: RscService,
        @Inject(TOASTR_TOKEN) private toastr: Toastr) {
    }

    ngOnInit() {
        this.isFormLoadingData = false;
        this.nextScheduledObservationMoment = moment.utc().add(8, 'hours');

        this.configureReactiveForm();

        //set Measure Date
        this.nextScheduledObservationText = this.nextScheduledObservationMoment.format("YYYY/MM/DD, H:mm") + " UTC";
        const fc = this.frmGroup.get('nextScheduledObservation');
        const formatedDate = this.nextScheduledObservationMoment.format(this.dateFormat);
        if (fc.value !== formatedDate) {
            fc.patchValue(formatedDate);
        }
        this.model.nextScheduledObservation = this.nextScheduledObservationMoment;

    }

    ngOnChanges() {
    }

    ngOnDestroy() {
    }

    $ctrl(name: string): AbstractControl {
        return this.frmGroup.get(name);
    }

    private configureReactiveForm() {
        const frmArrControls = <FormArray>this.mainForm.controls['frmNextScheduledObservationArr'];

        this.frmGroup = this.fb.group({
            generalRemarks: [{ value: this.model.generalRemarks, disabled: this.isReadOnly }, [Validators.required]],
            nextScheduledObservation: [{ value: this.nextScheduledObservationMoment, disabled: this.isReadOnly }, [Validators.required]],
        });

        frmArrControls.push(this.frmGroup);
    }

    //Measure Date
    isCalendarActive() {
        return true;
    }

    setNextScheduledObservation(date: any, leaveEmpty?: boolean) {
        const fc = this.frmGroup.get('nextScheduledObservation');
        if (date) {
            let momentDate = moment.utc(date);
            if (momentDate.isValid()) {
                this.nextScheduledObservationText = momentDate.format("YYYY/MM/DD, H:mm") + " UTC";
                const formatedDate = momentDate.format(this.dateFormat);
                if (fc.value !== formatedDate) {
                    fc.setValue(formatedDate);
                    fc.markAsDirty();
                    this.nextScheduledObservationMoment = momentDate;
                    this.model.nextScheduledObservation = this.nextScheduledObservationMoment;
                }

            }
        } else {
            if (leaveEmpty) {
                fc.setValue(null);
                fc.markAsDirty();
                this.model.nextScheduledObservation = null;
            } else {
                this.setNextScheduledObservation(moment.utc());
            }
        }
    }

    validateNextScheduledObservation(errorName: string) {
        return true;
    }

    onNextScheduledObservationInputChange(strDate: string) {
        if (strDate && strDate.length === 10) {
            let momentDate = moment.utc(strDate, this.dateFormat);
            if (momentDate.isValid()) {
                this.nextScheduledObservationText = momentDate.format("YYYY/MM/DD, H:mm") + " UTC";
                const fc = this.frmGroup.get('nextScheduledObservation');
                const formatedDate = momentDate.format(this.dateFormat);
                if (fc.value !== formatedDate) {
                    fc.setValue(formatedDate);
                    fc.markAsDirty();
                    this.nextScheduledObservationMoment = momentDate;
                    this.model.nextScheduledObservation = this.nextScheduledObservationMoment;
                }
            } else {
                this.nextScheduledObservationText = "";
                this.model.nextScheduledObservation = null;
            }
        } else {
            this.nextScheduledObservationText = "";
            this.model.nextScheduledObservation = null;
        }
    }


    //General functions
    private isValidNumber(value: string | number): boolean {
        return ((value != null) &&
            (value !== '') &&
            !isNaN(Number(value.toString())));
    }

    private isANumber(str: string): boolean {
        return !/\D/.test(str);
    }

    //Export functions
    public isFormValid(): boolean {

        return true;
    }

    public exportComponentInternalData(): INextObservationModel {
        let data: INextObservationModel = {
            parent: this.model.parent,
            nextScheduledObservation: this.nextScheduledObservationMoment,
            generalRemarks: this.model.generalRemarks,
        };
        return data;
    }

    public clearValues(): void {
        alert('Clear Component Values: NextObservation');
    }


}
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StartupService = void 0;
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
require("rxjs/add/operator/toPromise");
require("rxjs/add/operator/catch");
var StartupService = /** @class */ (function () {
    function StartupService(http) {
        this.http = http;
    }
    // This is the method you want to call at bootstrap 
    // Important: It should return a Promise
    StartupService.prototype.load = function () {
        var _this = this;
        // This is an authorize endpoint in the MVC controllers that does not requires bearer token auth
        var apiUrl = window['app'].rootUrl + "account/accesstoken?userid=" + window['app'].usrId;
        return this.http
            .get(apiUrl)
            .map(function (res) { return res.json(); })
            .toPromise()
            .then(function (authData) { return _this.setAuthData(authData); })
            .catch(function (err) { return Promise.resolve(); });
    };
    StartupService.prototype.setAuthData = function (authData) {
        this.authData = authData;
        window['app'].authData = authData;
        return this.authData;
    };
    StartupService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], StartupService);
    return StartupService;
}());
exports.StartupService = StartupService;
//# sourceMappingURL=startup.service.js.map
﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum RegionType
    {
		Doa = 0,
		Bilingual = 1,
		Northern = 2
    }
}

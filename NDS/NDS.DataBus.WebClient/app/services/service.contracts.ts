﻿export interface INotam {
    id: string;
    body: string;
    operator: string;
    icaoCode: string;
}
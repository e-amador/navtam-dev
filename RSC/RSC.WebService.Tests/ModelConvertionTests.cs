﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RSC.WebService.Helpers;
using RSC.WebService.Schemas;
using System;
using System.Globalization;
using System.IO;
using System.Linq;

namespace RSC.WebService.Tests
{
    [TestClass]
    public class ModelConvertionTests
    {
        [TestMethod]
        [DeploymentItem("Data\\RSCProposal-0003.xml")]
        public void Convert_xml_proposal_to_model()
        {
            var xmlProposal = LoadProposal("RSCProposal-0003.xml");
            var proposal = xmlProposal.ToModel();

            Assert.IsNotNull(proposal, "Must be a valid object.");

            // version
            Assert.AreEqual(proposal.Version, "0.1", "Version must be valid!");

            // aerodrome
            Assert.AreEqual(proposal.Aerodrome, "CYOW", "Aerodrome must match!");

            // origin
            Assert.AreEqual(proposal.Origin, "test", "Origin must match!");

            // created
            var created = DateTime.ParseExact("2019-12-17T09:30:47", "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture);
            Assert.AreEqual(proposal.Created, created, "Created must match!");

            // runways
            Assert.IsNotNull(proposal.Runways, "Must have runway list!");
            Assert.AreEqual(proposal.Runways.Length, 1, "Must have one runway!");
        }

        [TestMethod]
        [DeploymentItem("Data\\cancel-proposal.xml")]
        public void Convert_cancel_proposal()
        {
            var xmlProposal = LoadProposal("cancel-proposal.xml");
            var proposal = xmlProposal.ToModel();

            Assert.IsNotNull(proposal, "Must be a valid object.");

            Assert.IsTrue(proposal.CancelCurrentRSC, "Expected no winter maintenance!");
        }

        [TestMethod]
        [DeploymentItem("Data\\one-runway-no-winter-maint.xml")]
        public void Convert_runway_no_WinterMaintenance()
        {
            var xmlProposal = LoadProposal("one-runway-no-winter-maint.xml");
            var proposal = xmlProposal.ToModel();

            Assert.IsNotNull(proposal, "Must be a valid object.");

            var runway = proposal.Runways.FirstOrDefault();
            Assert.IsNotNull(runway, "Expected one runway!");

            Assert.AreEqual(runway.RunwayId, "RWY-07/32", "Must match ID!");

            Assert.IsTrue(runway.NoWinterMaintenance, "Expected no winter maintenance!");
        }

        [TestMethod]
        [DeploymentItem("Data\\one-runway-with-winter-maint.xml")]
        public void Convert_runway_with_WinterMaintenance()
        {
            var xmlProposal = LoadProposal("one-runway-with-winter-maint.xml");
            var proposal = xmlProposal.ToModel();

            Assert.IsNotNull(proposal, "Must be a valid object.");

            var runway = proposal.Runways.FirstOrDefault();
            Assert.IsNotNull(runway, "Expected one runway!");

            Assert.IsFalse(runway.NoWinterMaintenance, "Expected winter maintenance!");

            var obsTime = DateTime.ParseExact("2019-12-17T14:33", "yyyy-MM-ddTHH:mm", CultureInfo.InvariantCulture);
            Assert.AreEqual(runway.ObsTimeUTC, obsTime);

            Assert.AreEqual(runway.CentredClearedWidth, 140);

            Assert.IsNotNull(runway.ClearedPortion);
        }

        [TestMethod]
        [DeploymentItem("Data\\many-runways.xml")]
        public void Convert_many_runways()
        {
            var xmlProposal = LoadProposal("many-runways.xml");
            var proposal = xmlProposal.ToModel();

            Assert.IsNotNull(proposal, "Must be a valid object.");

            var runways = proposal.Runways.Count();
            Assert.IsTrue(runways > 1, "Expected more than one runway!");
        }

        [TestMethod]
        [DeploymentItem("Data\\conditions-changing-rapidly.xml")]
        public void Convert_with_ConditionsChangingRapidly()
        {
            var xmlProposal = LoadProposal("conditions-changing-rapidly.xml");
            var proposal = xmlProposal.ToModel();

            Assert.IsNotNull(proposal, "Must be a valid object.");
            Assert.IsNotNull(proposal.ConditionsChangingRapidly, "Must contain a telefone.");
        }

        enum TestEnum
        {
            A,
            B,
            C
        }

        [TestMethod]
        public void TempTest()
        {
            var types = new TestEnum[] { TestEnum.A, TestEnum.B, TestEnum.C };
            var objs = new object[] { 1, "2", true };

            int a = 0;
            string s = "";
            bool b = false;
            SchemaFieldAssigner<TestEnum>
                .For(types, objs)
                .On(TestEnum.A, v => a = (int)v)
                .On(TestEnum.B, v => s = (string)v)
                .On(TestEnum.C, v => b = (bool)v)
                .Apply();

            Assert.AreEqual(a, 1);
            Assert.AreEqual(s, "2");
            Assert.AreEqual(b, true);
        }


        static RSCProposalType LoadProposal(string filePath)
        {
            var xml = File.ReadAllText(filePath);
            return XMLSerializer.Deserialize<RSCProposalType>(xml);
        }
    }
}

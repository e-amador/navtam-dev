﻿using System;

namespace NavCanada.Core.Proxies.SignalrHubProxy
{
    using System.Net;
    using System.Threading.Tasks;

    using log4net;

    using Microsoft.AspNet.SignalR.Client;

    public class SignalrHubProxy : ISignalrHubProxy
    {
        public SignalrHubProxy(IAuthenticate authenticate, string notamWizEndpointUrl)
        {
            _authenticate = authenticate;
            _hubConnection = new HubConnection(notamWizEndpointUrl);
            _hubProxy = _hubConnection.CreateHubProxy("NotificationHub");
        }

        public Task Notify(string method, params object[] args)
        {
            Logger.Info(method + ": " + string.Join(",", args));
            if (_hubConnection.State != (ConnectionState.Connected))
            {
                if (Restart())
                    return _hubProxy.Invoke(method, args);
            }

            return _hubProxy.Invoke(method, args);
        }

        public bool Start()
        {
            Cookie returnedCookie = null;
            var authResult = false;

            Task.Run(() => _authenticate.AuthenticateUser(out authResult, out returnedCookie)).Wait();

            _hubConnection.CookieContainer = new CookieContainer();
            _hubConnection.CookieContainer.Add(returnedCookie);

            if (authResult)
            {
                try
                {
                    _hubConnection.Start().Wait();

                }
                catch
                {
                    Logger.Error("Failed to connect to SignalR NotificationHub");
                    return false;
                }
            }
            else
            {
                Logger.Error("Failed to authenticate user");
                return false;
            }
            return true;
        }

        public bool Restart()
        {
            Stop();
            return Start();
        }

        public void Stop()
        {
            _hubConnection.Stop();
        }

        static readonly ILog Logger = LogManager.GetLogger(typeof(SignalrHubProxy));
        private readonly HubConnection _hubConnection;
        private readonly IHubProxy _hubProxy;
        private readonly IAuthenticate _authenticate;
    }
}

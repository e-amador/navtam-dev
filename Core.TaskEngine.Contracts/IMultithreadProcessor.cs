﻿namespace NavCanada.Core.TaskEngine.Contracts
{
    public interface IMultithreadProcessor
    {
        void StartProcessing(int threadCount, int millisecondsNapTime);
        void StopProcessing(int millisecondsTimeOut);
    }
}

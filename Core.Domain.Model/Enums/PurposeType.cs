﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum PurposeType
    {
        NONE = 0,
        NB,
        BO,
        M,
        K,
        NBO,
        B
    }
}

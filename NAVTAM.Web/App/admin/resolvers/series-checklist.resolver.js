"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SeriesChecklistResolver = void 0;
var core_1 = require("@angular/core");
var data_service_1 = require("./../shared/data.service");
var SeriesChecklistResolver = /** @class */ (function () {
    function SeriesChecklistResolver(dataService) {
        this.dataService = dataService;
    }
    SeriesChecklistResolver.prototype.resolve = function (route) {
        var series = route.params['series'];
        return this.dataService.getSeriesChecklist(series).map(function (model) { return model; });
    };
    SeriesChecklistResolver = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [data_service_1.DataService])
    ], SeriesChecklistResolver);
    return SeriesChecklistResolver;
}());
exports.SeriesChecklistResolver = SeriesChecklistResolver;
//# sourceMappingURL=series-checklist.resolver.js.map
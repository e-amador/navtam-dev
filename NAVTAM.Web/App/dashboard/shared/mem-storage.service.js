"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MemoryStorageService = void 0;
var core_1 = require("@angular/core");
var MemoryStorageService = /** @class */ (function () {
    function MemoryStorageService() {
        this.NOTAM_QUERY_OPTIONS_KEY = "NOTAM_QUERY_OPTIONS_KEY";
        this.REPORT_NOTAM_QUERY_OPTIONS_KEY = "REPORT_NOTAM_QUERY_OPTIONS_KEY";
        this.REPORT_NOTAM_ACTIVE_QUERY_OPTIONS_KEY = "REPORT_NOTAM_ACTIVE_QUERY_OPTIONS_KEY";
        this.PROPOSAL_QUERY_OPTIONS_KEY = "PROPOSAL_QUERY_OPTIONS_KEY";
        this.NSD_CLIENTS_QUERY_OPTIONS_KEY = "NSD_CLIENTS_QUERY_OPTIONS_KEY";
        this.MESSAGES_QUEUE_QUERY_OPTIONS_KEY = "MESSAGES_QUEUE_QUERY_OPTIONS_KEY";
        this.FILTER_APPLIED_KEY = "FILTER_APPLIED_KEY";
        this.REPORT_FILTER_APPLIED_KEY = "REPORT_FILTER_APPLIED_KEY";
        this.FILTER_QUERY_OPTIONS_KEY = "FILTER_QUERY_OPTIONS_KEY";
        this.REPORT_FILTER_QUERY_OPTIONS_KEY = "REPORT_FILTER_QUERY_OPTIONS_KEY";
        this.MSG_FILTER_APPLIED_KEY = "MSG_FILTER_APPLIED_KEY";
        this.PROPOSAL_FILTER_KEY = "PROPOSAL_FILTER_KEY";
        this.NOTAM_FILTER_KEY = "NOTAM_FILTER_KEY";
        this.REPORT_NOTAM_FILTER_KEY = "REPORT_NOTAM_FILTER_KEY";
        this.PROPOSAL_ID_KEY = "PROPOSAL_ID_KEY";
        this.NOTAM_ID_KEY = "NOTAM_ID_KEY";
        this.NSD_CLIENT_ID_KEY = "NSD_CLIENT_ID_KEY";
        this.MSG_TAB_SELECTED_KEY = "MSG_TAB_SELECTED_KEY";
        this.MESSAGE_QUEUE_ID_KEY = "MESSAGE_QUEUE_ID_KEY";
        this.CATEGORY_ID_KEY = "CATEGORY_ID_KEY";
        this.SUB_CATEGORY_ID_KEY = "SUB_CATEGORY_ID_KEY";
        this.ACTIVE_QUEUE_KEY = "ACTIVE_QUEUE_KEY";
        this.MODIFY_ACTION_KEY = "MODIFY_ACTION_KEY"; //0-Update 1-Widtrawn
        this.PENDING_QUEUE_SOUND_KEY = "PENDING_QUEUE_SOUND_KEY";
        this.MESSAGE_QUEUE_SOUND_KEY = "MESSAGE_QUEUE_SOUND_KEY";
        this.PENDING_QUEUE_SOUND_COUNT_KEY = "PENDING_QUEUE_SOUND_COUNT_KEY";
        this.MESSAGE_QUEUE_SOUND_COUNT_KEY = "MESSAGE_QUEUE_SOUND_COUNT_KEY";
        this._memoryStorage = [];
    }
    MemoryStorageService.prototype.save = function (key, value) {
        var index = this._memoryStorage.findIndex(function (x) { return x.key === key; });
        if (index >= 0) {
            this._memoryStorage[index].value = value;
        }
        else {
            this._memoryStorage.push({
                key: key,
                value: value
            });
        }
    };
    MemoryStorageService.prototype.get = function (key) {
        var index = this._memoryStorage.findIndex(function (x) { return x.key === key; });
        if (index >= 0) {
            return this._memoryStorage[index].value;
        }
        return null;
    };
    MemoryStorageService.prototype.remove = function (key) {
        var index = this._memoryStorage.findIndex(function (x) { return x.key === key; });
        if (index >= 0) {
            this._memoryStorage.splice(index, 1);
        }
    };
    MemoryStorageService = __decorate([
        core_1.Injectable()
    ], MemoryStorageService);
    return MemoryStorageService;
}());
exports.MemoryStorageService = MemoryStorageService;
//# sourceMappingURL=mem-storage.service.js.map
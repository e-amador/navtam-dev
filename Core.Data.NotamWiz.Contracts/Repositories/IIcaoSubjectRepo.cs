﻿namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    using System.Threading.Tasks;

    using Domain.Model.Entitities;
    using System.Collections.Generic;
    using Domain.Model.Dtos;
    using Common.Common;

    public interface IIcaoSubjectRepo : IRepository<IcaoSubject>
    {
        Task<IcaoSubject> GetByIdAsync(int id);
        IcaoSubject GetByCode(string code);
        Task<IcaoSubject> GetByCodeAsync(string code);
        IcaoSubject GetByIdWithConditions(int id);
        Task<IcaoSubjectDto> GetModelAsync(int id);
        Task<List<IcaoSubjectDto>> GetAhpSubjectsAsync(string lang);
        Task<List<IcaoSubjectDto>> GetFirSubjectsAsync(string lang);
        Task<List<IcaoSubjectDto>> GetSubjectsAsync(string lang);
        Task<List<IcaoSubjectDto>> GetAllAsync(QueryOptions queryOptions);
        Task<List<string>> GetAllDistinctCodesAsync();
        Task<int> GetAllCountAsync();
        Task<int> GetAllCountAsync(QueryOptions queryOptions);
    }
}


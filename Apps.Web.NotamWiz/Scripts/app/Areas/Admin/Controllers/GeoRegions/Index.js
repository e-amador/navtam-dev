﻿
define(["jquery", "Resources", 'kendoExt'], function ($, res, bing, util) {

    var GeoRegionsManager;
    var labels = {};

    return {
        init: function (lbs) {
            labels = lbs;
            RES = res.getResources("GeoRegions");
            GeoRegionsManager = this;
            GeoRegionsManager.initUI();
        },

        initUI: function () {

            $("#geoRegionsGrid").kendoGrid({
                resizable: true,
                selectable: true,
                Filterable: true,
                Groupable: true,
                scrollable: true,
                pageSize: 10,
                batch: false,
                pageable: true,
                toolbar: [
                      {
                          template: '<a id="CreateRegion" class="k-button" >' + labels.createLabel + '</a>'
                      }
                ],
                pageable: {
                    refresh: true,
                    buttonCount: 5
                },
                columns: [
                    { field: "Name", title: labels.nameLabel },
                    {
                        command: [
                          {
                              name: "editGeo", text: labels.editLabel,
                              click: function (e) {
                                  var tr = $(e.target).closest("tr"); // get the current table row (tr)                                   
                                  var geo = this.dataItem(tr);
                                  $.ajax({
                                      url: "GeoRegions/Edit",
                                      data: { Id: geo.Id },
                                      success: function (data) {

                                          $("#controlWindow").html("");
                                          $("#controlWindow").html(data);

                                      },
                                      cache: false
                                  });
                              }

                          },
                          {
                              name: "deleteGeo", text: labels.deleteLabel,
                              click: function (e) {
                                  var tr = $(e.target).closest("tr"); // get the current table row (tr)                                   
                                  var geo = this.dataItem(tr);
                                  $.when(kendo.ui.ExtOkCancelDialog.show({
                                      title: labels.confirmLabel,
                                      message: labels.confirmDelete,
                                      width: '400px',
                                      height: '150px',
                                      icon: "k-Ext-warning",
                                      //buttons: [{ name: 'ssss' }, { name: 'dddd' }]
                                  })).done(function (response) {
                                      if (response.button === 'OK') {
                                          $.ajax({
                                              url: "GeoRegions/Delete",
                                              data: { Id: geo.Id },
                                              type: "POST",
                                              success: function (data) {
                                                  debugger;
                                                  if (data.Success) {
                                                      $('#geoRegionsGrid').data('kendoGrid').dataSource.read();
                                                      $('#geoRegionsGrid').data('kendoGrid').refresh();
                                                  } else {
                                                      kendo.ui.ExtAlertDialog.show({
                                                      title: labels.errorLabel,
                                                      message: data.Error,
                                                      icon: "k-Ext-information"
                                                  });
                                                  }
                                              },
                                              error: function () {
                                                  kendo.ui.ExtAlertDialog.show({
                                                      title: labels.errorLabel,
                                                      message: labels.errorDeletingReg,
                                                      icon: "k-Ext-information"
                                                  });
                                              },
                                              cache: false
                                          });
                                      }
                                  });



                              }
                          }
                        ], width: 170
                    }
                ],

                dataSource: {
                    schema: {
                        model: { id: "Id" },
                        data: function (data) { return data.Data; },

                    },
                    transport: {
                        read: { url: "GeoRegions/GetGeoRegions", dataType: "json", type: "POST" },
                        update: { url: "GeoRegions/Update", dataType: "json", type: "POST" },
                        destroy: { url: "GeoRegions/Delete", dataType: "json", type: "POST" }

                    },


                },
                dataBound: function onDataBound(e) {

                }

            });

            $("#CreateRegion").on('click', function () {


                $.ajax({

                    url: "GeoRegions/Create",
                    success: function (data) {

                        $("#controlWindow").html("");
                        $("#controlWindow").html(data);

                    },
                    cache: false
                });
            });
        }
    }
});




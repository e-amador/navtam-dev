"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MObstAreaType = exports.ProposalStatus = exports.MessageStatus = exports.ClientType = exports.NsdCategory = exports.ProposalType = exports.FilterOperator = exports.NofQueues = exports.RegionSource = exports.IPoint = void 0;
var IPoint = /** @class */ (function () {
    function IPoint() {
    }
    return IPoint;
}());
exports.IPoint = IPoint;
var RegionSource = /** @class */ (function () {
    function RegionSource() {
    }
    RegionSource.None = 0;
    RegionSource.Geography = 1;
    RegionSource.FIR = 2;
    RegionSource.Points = 3;
    RegionSource.PointAndRadius = 4;
    return RegionSource;
}());
exports.RegionSource = RegionSource;
;
var NofQueues = /** @class */ (function () {
    function NofQueues() {
    }
    NofQueues.Pending = "PENDING_QUEUE";
    NofQueues.Park = "PARK_QUEUE";
    NofQueues.Review = "REVIEW_QUEUE";
    NofQueues.Message = "MESSAGE_QUEUE";
    return NofQueues;
}());
exports.NofQueues = NofQueues;
var FilterOperator = /** @class */ (function () {
    function FilterOperator() {
    }
    FilterOperator.EQ = "EQ";
    FilterOperator.NEQ = "NEQ";
    FilterOperator.LT = "LT";
    FilterOperator.GT = "GT";
    FilterOperator.LTE = "LTE";
    FilterOperator.GTE = "GTE";
    FilterOperator.SW = "SW";
    FilterOperator.EW = "EW";
    FilterOperator.CT = "CT";
    return FilterOperator;
}());
exports.FilterOperator = FilterOperator;
var ProposalType = /** @class */ (function () {
    function ProposalType() {
    }
    ProposalType.New = 0;
    ProposalType.Replace = 1;
    ProposalType.Cancel = 2;
    return ProposalType;
}());
exports.ProposalType = ProposalType;
var NsdCategory = /** @class */ (function () {
    function NsdCategory() {
    }
    NsdCategory.All = 1;
    NsdCategory.CatchAll = 2;
    NsdCategory.Obstacles = 3;
    NsdCategory.Obst = 4;
    NsdCategory.ObstLGT = 5;
    NsdCategory.Runways = 6;
    NsdCategory.RwyClsd = 7;
    NsdCategory.TwyClsd = 11;
    NsdCategory.CarsClsd = 13;
    NsdCategory.MultiObst = 14;
    NsdCategory.MultiObstLGT = 15;
    return NsdCategory;
}());
exports.NsdCategory = NsdCategory;
var ClientType = /** @class */ (function () {
    function ClientType() {
    }
    ClientType.AFTN = 0;
    ClientType.SWIM = 1;
    ClientType.REST = 2;
    return ClientType;
}());
exports.ClientType = ClientType;
var MessageStatus = /** @class */ (function () {
    function MessageStatus() {
    }
    MessageStatus.None = 0;
    MessageStatus.Important = 1;
    MessageStatus.Draft = 2;
    MessageStatus.Parked = 3;
    MessageStatus.Deleted = 4;
    MessageStatus.Reply = 5;
    MessageStatus.AlreadyTaken = 200;
    return MessageStatus;
}());
exports.MessageStatus = MessageStatus;
var ProposalStatus = /** @class */ (function () {
    function ProposalStatus() {
    }
    ProposalStatus.Undefined = 0;
    // User Proposal status
    ProposalStatus.Draft = 1;
    ProposalStatus.Submitted = 2;
    ProposalStatus.Withdrawn = 3;
    // NOF Proposal status
    ProposalStatus.Picked = 10;
    ProposalStatus.Parked = 11;
    ProposalStatus.Disseminated = 12;
    ProposalStatus.Rejected = 13;
    ProposalStatus.SoontoExpire = 14;
    ProposalStatus.Expired = 15;
    ProposalStatus.ParkedDraft = 16;
    ProposalStatus.Terminated = 17;
    ProposalStatus.DisseminatedModified = 18;
    ProposalStatus.ParkedPicked = 19;
    //Everybody
    ProposalStatus.Cancelled = 20;
    ProposalStatus.Replaced = 21;
    ProposalStatus.Deleted = 22;
    ProposalStatus.Discarded = 23;
    ProposalStatus.Taken = 255;
    return ProposalStatus;
}());
exports.ProposalStatus = ProposalStatus;
var MObstAreaType = /** @class */ (function () {
    function MObstAreaType() {
    }
    MObstAreaType.Circle = 0;
    MObstAreaType.Line = 1;
    return MObstAreaType;
}());
exports.MObstAreaType = MObstAreaType;
//export class RadiusUnit {
//    static NM: number = 0;
//    static FT: number = 1;
//}
//# sourceMappingURL=data.model.js.map
﻿using NDS.SolaceVMR.Configuration;
using SolaceSystems.Solclient.Messaging;
using System;
using INdsMessage = NDS.Common.Contracts.INdsMessage;
using INdsPostQueue = NDS.Common.Contracts.IPostQueue;

namespace NDS.SolaceVMR
{
    public class SolaceQueuePoster : INdsPostQueue
    {
        public SolaceQueuePoster(SessionConfiguration config, string queueName)
        {
            _context = ContextFactory.Instance.CreateContext(new ContextProperties(), null);
            var sessionProperties = SessionUtils.NewSessionPropertiesFromConfig(config);

            _session = _context.CreateSession(sessionProperties, MessageHandler, SessionHandler);
            if (_session.Connect() != ReturnCode.SOLCLIENT_OK)
                throw new Exception("Cannot start VMR session!");

            _queue = ContextFactory.Instance.CreateQueue(queueName);
        }

        public void PostMessage(INdsMessage message)
        {
            var solaceMessage = message as SolaceMessageWrapper;
            if (solaceMessage == null)
                throw new ArgumentException("Unexpected message type!");

            var innerMessage = solaceMessage.InnerMessage;
            innerMessage.DeliveryMode = MessageDeliveryMode.Persistent;
            innerMessage.Destination = _queue;

            if (_session.Send(innerMessage) != ReturnCode.SOLCLIENT_OK)
                throw new Exception("Could not send the message!");
        }

        private void SessionHandler(object sender, SessionEventArgs e)
        {
        }

        private void MessageHandler(object sender, MessageEventArgs e)
        {
        }

        private void DiscconectSession()
        {
            try
            {
                _session?.Disconnect();
            }
            catch (Exception)
            {
            }
        }

        private IContext _context;
        private ISession _session;
        private IQueue _queue;

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // no managed objects to dispose
                }

                DiscconectSession();

                _session?.Dispose();
                _session = null;

                _queue?.Dispose();
                _queue = null;

                _context?.Dispose();
                _context = null;

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~SolaceQueuePoster()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}

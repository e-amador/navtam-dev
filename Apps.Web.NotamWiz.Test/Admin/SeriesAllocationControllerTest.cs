﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using NavCanada.Applications.NotamWiz.Web.Code.Configurations;
using NavCanada.Applications.NotamWiz.Web.Models;
using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Applications.NotamWiz.Web.Areas.Admin.Controllers;
using NavCanada.Applications.NotamWiz.Web.Code.Helpers;
using Newtonsoft.Json;

namespace Apps.Web.NotamWiz.Test.Admin
{
    [TestClass]
    public class SeriesAllocationControllerTest
    {
        #region Initialize

        private HttpContextBase _httpContext;
        private List<SeriesAllocation> _series;

        private SeriesAllocationController _controller;

        private Mock<IUow> _uowMock { get; set; }
        private Mock<IClusterQueues> _clusterQueuesMock { get; set; }
        private IUow iow { get; set; }

        private Mock<ControllerContext> _controllerContextMock;
        private Mock<ISeriesAllocationRepo> _seriesRepoMock;
       


        [TestInitialize]
        public void InitializeTest()
        {
            #region Test Model Obj
            AutoMapperConfig.RegisterMaps();

            _series = new List<SeriesAllocation>
            {
                // valid modelstates
                new SeriesAllocation{ DisseminationCategory = DisseminationCategory.National, Id = 1, QCode = "12", RegionId = 1, Series = "33" },
                new SeriesAllocation{ DisseminationCategory = DisseminationCategory.National, Id = 2, QCode = "12", RegionId = 1, Series = "34" },
                new SeriesAllocation{ DisseminationCategory = DisseminationCategory.National, Id = 3, QCode = "12", RegionId = 1, Series = "35" },
                // invalid modelstates below
                new SeriesAllocation{ DisseminationCategory = DisseminationCategory.National, Id=4, RegionId=2, Series="44"},
                new SeriesAllocation{ Id =5, RegionId=3, QCode="1337" },
                new SeriesAllocation{ DisseminationCategory = DisseminationCategory.National, Id=6, RegionId=2 },
            };

            #endregion

            #region DAL Initialize
            _uowMock = new Mock<IUow>();
            _clusterQueuesMock = new Mock<IClusterQueues>();
            _seriesRepoMock = new Mock<ISeriesAllocationRepo>();
            var dbresMock = new Mock<IDbResWrapper>();


            _uowMock.Setup(e => e.SeriesAllocationRepo).Returns(_seriesRepoMock.Object);


            #endregion

            #region Controller setup
            _controllerContextMock = new Mock<ControllerContext>();
            _httpContext = MvcMockHelpers.MockHttpContext("~/Admin/");
            _controllerContextMock.SetupGet(p => p.HttpContext).Returns(_httpContext);
            dbresMock.Setup(d => d.T(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
               .Returns<string, string, string>((resId, resourceSet, lang) => resId);


            _controller = new SeriesAllocationController(_uowMock.Object, _clusterQueuesMock.Object)
            {
                ControllerContext = _controllerContextMock.Object,
                DbRes = dbresMock.Object
            };
            #endregion
        }

        #endregion

        #region Tests

        [TestMethod]
        public void SeriesAllocationController_Create_ShouldCreateIfModelValid()
        {
            //valid model
            var seriesvm = AutoMapper.Mapper.Map<SeriesAllocation, SeriesAllocationViewModel>(_series.SingleOrDefault(x => x.Id == 2));
            var result = _controller.SeriesAllocation_Create(new DataSourceRequest(), seriesvm).Result as JsonResult;
            
            Assert.IsNotNull(result);
            _uowMock.Verify(x => x.SaveChangesAsync(), Times.Once);
        }
        [TestMethod]
        public void SeriesAllocationController_Create_ShouldNotAddIncorrectModel()
        {
            //incorrect model
            var seriesvm = AutoMapper.Mapper.Map<SeriesAllocation, SeriesAllocationViewModel>(_series.SingleOrDefault(x => x.Id == 4));
            var result = _controller.SeriesAllocation_Create(new DataSourceRequest(), seriesvm);
            
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsTrue(result.IsCompleted);
            _uowMock.Verify(x => x.SaveChangesAsync(), Times.Never);
        }

        [TestMethod]
        public void SeriesAllocationController_Destroy_ShouldReturnNullIfEntityDoesntExist()
        {
            //add spoof series
            _series.Add(new SeriesAllocation
            {
                Id = 99999,
                QCode = "1",
                RegionId = 1,
                DisseminationCategory = DisseminationCategory.International,
                Series = "2"
            });

            var seriesvm = AutoMapper.Mapper.Map<SeriesAllocation, SeriesAllocationViewModel>(_series.SingleOrDefault(x => x.Id == 99999));
            var result = _controller.SeriesAllocation_Destroy(new DataSourceRequest(), seriesvm.Id);
            Assert.IsNull(result.Result);
            
            _uowMock.Verify(x => x.SaveChangesAsync(), Times.Never);
        }
        #endregion
    }
}

﻿using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class UserQueryFilter
    {
        public int Id { get; set; }
        public string FilterName{ get; set; }
        public string Username { get; set; }
        public UserFilterType UserFilterType { get; set; }
        public int SlotNumber { get; set; }
        public string JsonBundle { get; set; }
    }
}

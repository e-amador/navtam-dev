﻿using Microsoft.Owin;

[assembly: OwinStartup(typeof(NavCanada.Services.Api.Diagnostics.Startup))]
namespace NavCanada.Services.Api.Diagnostics
{
    using Owin;

    public partial class Startup
    {
        // add this static variable
        public void Configuration(IAppBuilder app)
        {
        }
    }
}

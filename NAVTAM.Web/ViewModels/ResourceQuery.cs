﻿namespace NAVTAM.ViewModels
{
    public class ResourceQuery
    {
        public string Query { get; set; }
        public int Size { get; set; }
        public string UserId { get; set; }
        public string OrgId { get; set; }
        public string DoaId { get; set; }
    }
}
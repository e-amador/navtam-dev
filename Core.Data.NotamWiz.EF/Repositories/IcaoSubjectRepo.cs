﻿namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;

    using Contracts.Repositories;
    using Domain.Model.Entitities;
    using System.Collections.Generic;
    using Domain.Model.Dtos;
    using Common.Common;
    using Extensions;

    public class IcaoSubjectRepo : Repository<IcaoSubject>, IIcaoSubjectRepo
    {
        public IcaoSubjectRepo(DbContext dbContext)
            : base(dbContext)
        {
        }

        public virtual Task<IcaoSubject> GetByIdAsync(int id)
        {
            return DbSet.FirstOrDefaultAsync(e=> e.Id == id);
        }

        public IcaoSubject GetByCode(string code)
        {
            return DbSet.FirstOrDefault(e => e.Code == code);
        }

        public Task<IcaoSubject> GetByCodeAsync(string code)
        {
            return DbSet.FirstOrDefaultAsync(e => e.Code == code);
        }

        public virtual IcaoSubject GetByIdWithConditions(int id)
        {
            return DbSet.Include("Conditions").FirstOrDefault(i=>i.Id == id);
        }

        public async Task<int> GetAllCountAsync()
        {
            return await DbSet.CountAsync();
        }

        public async Task<IcaoSubjectDto> GetModelAsync(int id)
        {
            var subject = await DbSet.SingleOrDefaultAsync(e => e.Id == id);
            if(subject != null )
            {
                return new IcaoSubjectDto
                {
                    Id = subject.Id,
                    Name = subject.Name,
                    NameFrench = subject.NameFrench,
                    EntityCode = subject.EntityCode,
                    Code = subject.Code,
                    Scope = subject.Scope,
                    RequiresItemA = subject.AllowItemAChange,
                    RequiresScope = subject.AllowScopeChange,
                    RequiresSeries = subject.AllowSeriesChange
                };
            }

            return null;
        }

        public async Task<List<string>> GetAllDistinctCodesAsync()
        {
            return await DbSet.Select(e=> e.Code).Distinct().ToListAsync();
        }

        public async Task<int> GetAllCountAsync(QueryOptions queryOptions)
        {
            var filter = queryOptions.FilterValue;
            return await DbSet
                .Where( s => s.Name.Contains(filter) || s.NameFrench.Contains(filter) || s.EntityCode.Contains(filter) || s.Code.Contains(filter))
                .CountAsync();
        }

        public async Task<List<IcaoSubjectDto>> GetAllAsync(QueryOptions queryOptions)
        {
            var ctx = DbContext as AppDbContext;
            var filter = queryOptions.FilterValue;

            var icaoSubjects = await DbSet
                .Where(s => s.Name.Contains(filter) || s.NameFrench.Contains(filter) || s.EntityCode.Contains(filter) || s.Code.Contains(filter))
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            var dtos = new List<IcaoSubjectDto>();

            foreach (var subject in icaoSubjects)
            {
                dtos.Add(new IcaoSubjectDto
                {
                    Id = subject.Id,
                    Name = subject.Name,
                    NameFrench = subject.NameFrench,
                    EntityCode = subject.EntityCode,
                    Code = subject.Code,
                    Scope = subject.Scope,
                    RequiresItemA = subject.AllowItemAChange,
                    RequiresScope = subject.AllowScopeChange,
                    RequiresSeries = subject.AllowSeriesChange 
                });
            }
            return dtos;
        }

        public async Task<List<IcaoSubjectDto>> GetAhpSubjectsAsync(string lang)
        {
            return await DbSet
                .Where(e => 
                    e.EntityCode != "FIR" &&
                    (e.Scope == "A" || e.Scope == "AE")
                )
                .Select(e => new IcaoSubjectDto
                {
                    Id = e.Id,
                    Name = lang == "en" ? e.Name : e.NameFrench,
                    EntityCode = e.EntityCode,
                    Code = e.Code,
                    Scope = e.Scope,
                    RequiresItemA = e.AllowItemAChange,
                    RequiresScope = e.AllowScopeChange,
                    RequiresSeries = e.AllowSeriesChange
                })
                .ToListAsync();
        }

        public async Task<List<IcaoSubjectDto>> GetFirSubjectsAsync(string lang)
        {
            return await DbSet
                .Where(e =>
                    e.EntityCode == "FIR" &&
                    (e.Scope == "E" || e.Scope == "W")
                )
                .Select(e => new IcaoSubjectDto
                {
                    Id = e.Id,
                    Name = lang == "en" ? e.Name : e.NameFrench,
                    EntityCode = e.EntityCode,
                    Code = e.Code,
                    Scope = e.Scope,
                    RequiresItemA = e.AllowItemAChange,
                    RequiresScope = e.AllowScopeChange,
                    RequiresSeries = e.AllowSeriesChange
                })
                .ToListAsync();
        }

        public async Task<List<IcaoSubjectDto>> GetSubjectsAsync(string lang)
        {
            return await DbSet
                .Select(e=> new IcaoSubjectDto
                {
                    Id = e.Id,
                    Name = lang == "en" ? e.Name : e.NameFrench,
                    EntityCode = e.EntityCode,
                    Code = e.Code,
                    Scope = e.Scope,
                    RequiresItemA = e.AllowItemAChange,
                    RequiresScope = e.AllowScopeChange,
                    RequiresSeries = e.AllowSeriesChange
                })
                .ToListAsync();
        }
    }
}

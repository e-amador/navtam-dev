﻿namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    using System.Threading.Tasks;
    using Domain.Model.Entitities;
    using System.Collections.Generic;
    using Domain.Model.Dtos;
    using Common.Common;
    using System;

    public interface IMessageExchangeQueueRepo : IRepository<MessageExchangeQueue>
    {
        Task<MessageExchangeQueue> GetByIdAsync(Guid id);
        Task<int> GetAllIncomingCountAsync(MessageExchangeStatus status);
        Task<int> GetAllOutgoingCountAsync(MessageExchangeStatus status);
        Task<int> GetAllParkedCountAsync();
        Task<int> GetAllUnreadCountAsync();
        Task<List<MessageExchangeQueueDto>> GetAllIncomingAsync(MessageExchangeStatus status, QueryOptions queryOptions);
        Task<List<MessageExchangeQueueDto>> GetAllOutgoingAsync(MessageExchangeStatus status, QueryOptions queryOptions);
        Task<List<MessageExchangeQueueDto>> GetAllParkedMessagesAsync(QueryOptions queryOptions);
    }
}




﻿using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    public interface IUserDoaRepo : IRepository<UserDoa>
    {
        Task<List<UserDoa>> GetDoasByUserAsync(string userId);
        Task AdjustUserDoas(int orgId, IEnumerable<int> doaIds);
        Task<DbGeography> JoinUserDoas(string userId);
    }
}

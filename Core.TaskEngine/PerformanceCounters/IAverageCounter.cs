﻿namespace NavCanada.Core.TaskEngine.PerformanceCounters
{
    public interface IAverageCounter
    {
        long Start();
        void Stop(long ticks);
        void Reset();
    }
}

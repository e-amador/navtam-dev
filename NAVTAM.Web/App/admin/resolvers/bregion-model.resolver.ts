﻿import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { DataService } from './../shared/data.service'

@Injectable()
export class BilingualRegionModelResolver implements Resolve<any> {

    constructor(private dataService: DataService) {
    }

    resolve(route: ActivatedRouteSnapshot) {
        return this.dataService.getDoaBilingualRegion().map(model => model);
    }
}


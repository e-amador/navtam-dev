﻿define(["jquery", "Resources", "kendoExt"], function ($, res, kendoExt) {
    var kendoValidator = null;
    return {
        init: function (labels) {
            var self = this;

            var kendoValidatorRegister = $("#registerForm").kendoValidator({
                messages: {
                    matches: function (input) {
                        return labels.passwordsMustMatch;
                    },
                    email: labels.invalidEmail,
                    passwordRule: labels.invalidPassword,
                    validmask: function (input) {
                        if (input.is("[name=Telephone]")) {
                            return labels.invalidPhoneNumber;
                        }
                        if (input.is("[name=Fax]")) {
                            return labels.invalidFaxNumber;
                        }
                        if (input.is("[name=OrgTelephone]")) {
                            return labels.invalidPhoneNumber;
                        }
                    },
                    required: function (input) {
                        if (input.is("[name=UserName]")) {
                            return labels.userNameRequired;
                        }
                        if (input.is("[name=FirstName]")) {
                            return labels.firstNameRequired;
                        }
                        if (input.is("[name=LastName]")) {
                            return labels.lastNameRequired;
                        }
                        if (input.is("[name=Position]")) {
                            return labels.positionRequired;
                        }
                        if (input.is("[name=EmailAddress]")) {
                            return labels.emailRequired;
                        }
                        if (input.is("[name=Telephone]")) {
                            return labels.telephoneRequired;
                        }
                        if (input.is("[name=Password]")) {
                            return labels.passwordRequired;
                        }
                        if (input.is("[name=ConfirmPassword]")) {
                            return labels.mustConfirmPassword;
                        }
                        // organization
                        if (input.is("[name=OrgName]")) {
                            return labels.orgNameRequired;
                        }
                        if (input.is("[name=OrgAddress]")) {
                            return labels.orgAddressRequired;
                        }
                        if (input.is("[name=OrgEmailAddress]")) {
                            return labels.orgEmailRequired;
                        }
                        if (input.is("[name=OrgTelephone]")) {
                            return labels.orgTelephoneRequired;
                        }
                        if (input.is("[name=TypeOfOperation]")) {
                            return labels.orgTypeRequired;
                        }
                        if (input.is("[id=chkcondition]")) {
                            return labels.mustCheckTerms;
                        }
                        return "";
                    }
                },
                rules: {
                    passwordRule: function (input) {
                        if (input.is("[name=Password]")) {
                            var validated = true;

                            var value = input.val();  // contains at least 8 chars
                            if (value.length < 8)
                                validated = false;

                            if (!/\d/.test(value))  // contains at least one digit
                                validated = false;

                            if (!/[a-z]/.test(value)) // contains at least one lower case
                                validated = false;

                            if (!/[A-Z]/.test(value)) // contains at least one upper case
                                validated = false;

                            if (!/[\]\[?\/<>~#`!@$%^&*()+={}|_:";',{ ]/.test(value)) // contains at least one special character
                                validated = false;

                            return validated;
                        }

                        return true;
                    },
                    matches: function (input) {
                        if (input.is("[name=ConfirmPassword]")) {
                            var confirmPassword = input.val();
                            var match = $("#Password").val();
                            return confirmPassword === match;
                        }
                        return true;
                    },
                    terms: function (input) {
                        if (input.is("[id=chkcondition]")) {
                            return $("#chkcondition").is(":checked");
                        }
                        return true;
                    }
                },
                validmask: function (input) {
                    if (input.is("[name=Telephone]")) {
                        if (input.val() !== '') {
                            var data = input.data("kendoMaskedTextBox");
                            return data.value().indexOf(data.options.promptChar) === -1;
                        }
                    }
                    if (input.is("[name=OrgTelephone]")) {
                        if (input.val() !== '') {
                            var data = input.data("kendoMaskedTextBox");
                            return data.value().indexOf(data.options.promptChar) === -1;
                        }
                    }
                    if (input.is("[name=Fax]")) {
                        if (input.val() !== '') {
                            var data = input.data("kendoMaskedTextBox");
                            return data.value().indexOf(data.options.promptChar) === -1;
                        }
                    }
                    return true;
                }
            }).data("kendoValidator");

            self.wireControllers();
            $("#RegisterButton").click(function (event) {
                if (kendoValidatorRegister.validate() === false) {
                    event.preventDefault();
                }
            });
        },
        wireControllers: function () {
            $("#Telephone").kendoMaskedTextBox({
                mask: "(000) 000-0000"
            });
            $("#OrgTelephone").kendoMaskedTextBox({
                mask: "(000) 000-0000"
            });
            $("#Fax").kendoMaskedTextBox({
                mask: "(000) 000-0000"
            });
            $("#CancelButton").kendoButton({
                click: function (e) {
                    window.location.href = '/';
                }
            });
        }
    }
});

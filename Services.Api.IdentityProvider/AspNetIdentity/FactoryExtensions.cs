﻿namespace NavCanada.Services.Api.IdentityProvider.AspNetIdentity
{
    using System.Collections.Generic;
    using System.Linq;

    using IdentityServer3.Core.Configuration;
    using IdentityServer3.Core.Models;
    using IdentityServer3.EntityFramework;

    public static class FactoryExtensions
    {
        public static void ConfigureClients(this IdentityServerServiceFactory factory, IEnumerable<Client> clients)
        {
            var clientRepo = factory.Registrations.FirstOrDefault(f => f.DependencyType == typeof(IClientConfigurationDbContext));
            if (clientRepo != null)
                using (ClientConfigurationDbContext db = clientRepo.Factory.Invoke(null) as ClientConfigurationDbContext)
                {
                    if (db != null && !db.Clients.Any())
                    {
                        foreach (var c in clients)
                        {
                            var e = c.ToEntity();
                            db.Clients.Add(e);
                        }
                        db.SaveChanges();
                    }
                }
        }

        public static void ConfigureScopes(this IdentityServerServiceFactory factory, IEnumerable<Scope> scopes)
        {
            var scopeRepo = factory.Registrations.FirstOrDefault(f => f.DependencyType == typeof(IScopeConfigurationDbContext));
            if (scopeRepo != null)
                using (ScopeConfigurationDbContext db = scopeRepo.Factory.Invoke(null) as ScopeConfigurationDbContext)
                {
                    if (db != null && !db.Scopes.Any())
                    {
                        foreach (var s in scopes)
                        {
                            var e = s.ToEntity();
                            db.Scopes.Add(e);
                        }
                        db.SaveChanges();
                    }
                }
        }
    }
}
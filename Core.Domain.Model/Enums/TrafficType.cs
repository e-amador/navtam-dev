﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum TrafficType
    {
        I = 0,
        V,
        IV,
        K,
    }
}

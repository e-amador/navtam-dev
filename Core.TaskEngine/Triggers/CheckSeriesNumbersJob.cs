﻿using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Common.Common;
using System;

namespace NavCanada.Core.TaskEngine.Triggers
{
    internal class CheckSeriesNumbersJob : ScheduleJobTriggerBase
    {
        protected override string GetScheduleTaskId() => ScheduleIdentity.CheckSeriesNumbersScheduler;
        protected override MessageTransferObjectId GetSheduleTaskMessageId() => MessageTransferObjectId.CheckSeriesNumbersMsg;
#if DEBUG
        protected override TimeSpan MinExecuteSpan => TimeSpan.FromMinutes(2.5);
#else
        protected override TimeSpan MinExecuteSpan => TimeSpan.FromMinutes(30);
#endif
    }
}
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UnitOfMeasurement = exports.TreatmentEnum = exports.OtherConditionsEdgeEnum = exports.OtherConditionsAreaEnum = exports.OffsetSideEnum = exports.OCLocationEnum = exports.MeasurementDeviceEnum = exports.ContaminantWithNoDepthEnum = exports.ContaminantWithDepthEnum = exports.CardinalSideEnum = exports.ResponseStatus = void 0;
var ResponseStatus;
(function (ResponseStatus) {
    ResponseStatus[ResponseStatus["Ok"] = 0] = "Ok";
    ResponseStatus[ResponseStatus["Warning"] = 1] = "Warning";
    ResponseStatus[ResponseStatus["Error"] = 2] = "Error";
})(ResponseStatus = exports.ResponseStatus || (exports.ResponseStatus = {}));
/**Defines which side of the runway is affected by a condition.  */
var CardinalSideEnum;
(function (CardinalSideEnum) {
    CardinalSideEnum[CardinalSideEnum["E"] = 250] = "E";
    CardinalSideEnum[CardinalSideEnum["SE"] = 251] = "SE";
    CardinalSideEnum[CardinalSideEnum["S"] = 252] = "S";
    CardinalSideEnum[CardinalSideEnum["SW"] = 253] = "SW";
    CardinalSideEnum[CardinalSideEnum["W"] = 254] = "W";
    CardinalSideEnum[CardinalSideEnum["NW"] = 255] = "NW";
    CardinalSideEnum[CardinalSideEnum["N"] = 256] = "N";
    CardinalSideEnum[CardinalSideEnum["NE"] = 257] = "NE";
})(CardinalSideEnum = exports.CardinalSideEnum || (exports.CardinalSideEnum = {}));
var ContaminantWithDepthEnum;
(function (ContaminantWithDepthEnum) {
    ContaminantWithDepthEnum[ContaminantWithDepthEnum["StandingWater"] = 60] = "StandingWater";
    ContaminantWithDepthEnum[ContaminantWithDepthEnum["DrySnow"] = 61] = "DrySnow";
    ContaminantWithDepthEnum[ContaminantWithDepthEnum["WetSnow"] = 62] = "WetSnow";
    ContaminantWithDepthEnum[ContaminantWithDepthEnum["Slush"] = 63] = "Slush";
    ContaminantWithDepthEnum[ContaminantWithDepthEnum["Unused64"] = 64] = "Unused64";
    ContaminantWithDepthEnum[ContaminantWithDepthEnum["DrySnowOnTopOfIce"] = 65] = "DrySnowOnTopOfIce";
    ContaminantWithDepthEnum[ContaminantWithDepthEnum["WetSnowOnTopOfIce"] = 66] = "WetSnowOnTopOfIce";
    ContaminantWithDepthEnum[ContaminantWithDepthEnum["SlushOnTopOfIce"] = 67] = "SlushOnTopOfIce";
    ContaminantWithDepthEnum[ContaminantWithDepthEnum["DrySnowOnTopOfCompactedSnow"] = 68] = "DrySnowOnTopOfCompactedSnow";
    ContaminantWithDepthEnum[ContaminantWithDepthEnum["Unused69"] = 69] = "Unused69";
    ContaminantWithDepthEnum[ContaminantWithDepthEnum["Unused70"] = 70] = "Unused70";
    ContaminantWithDepthEnum[ContaminantWithDepthEnum["WaterOnTopOfCompactedSnow"] = 71] = "WaterOnTopOfCompactedSnow";
    ContaminantWithDepthEnum[ContaminantWithDepthEnum["WetSnowOnTopOfCompactedSnow"] = 72] = "WetSnowOnTopOfCompactedSnow";
})(ContaminantWithDepthEnum = exports.ContaminantWithDepthEnum || (exports.ContaminantWithDepthEnum = {}));
var ContaminantWithNoDepthEnum;
(function (ContaminantWithNoDepthEnum) {
    ContaminantWithNoDepthEnum[ContaminantWithNoDepthEnum["Dry"] = 20] = "Dry";
    ContaminantWithNoDepthEnum[ContaminantWithNoDepthEnum["Unused21"] = 21] = "Unused21";
    ContaminantWithNoDepthEnum[ContaminantWithNoDepthEnum["Wet"] = 22] = "Wet";
    ContaminantWithNoDepthEnum[ContaminantWithNoDepthEnum["Frost"] = 23] = "Frost";
    ContaminantWithNoDepthEnum[ContaminantWithNoDepthEnum["Ice"] = 24] = "Ice";
    ContaminantWithNoDepthEnum[ContaminantWithNoDepthEnum["CompactedSnow"] = 25] = "CompactedSnow";
    ContaminantWithNoDepthEnum[ContaminantWithNoDepthEnum["Unused26"] = 26] = "Unused26";
    ContaminantWithNoDepthEnum[ContaminantWithNoDepthEnum["WetIce"] = 27] = "WetIce";
    ContaminantWithNoDepthEnum[ContaminantWithNoDepthEnum["Unused28"] = 28] = "Unused28";
    ContaminantWithNoDepthEnum[ContaminantWithNoDepthEnum["CompactedSnowGravelMix"] = 29] = "CompactedSnowGravelMix";
    ContaminantWithNoDepthEnum[ContaminantWithNoDepthEnum["Unused30"] = 30] = "Unused30";
    ContaminantWithNoDepthEnum[ContaminantWithNoDepthEnum["Unused31"] = 31] = "Unused31";
    ContaminantWithNoDepthEnum[ContaminantWithNoDepthEnum["SlipperyWhenWet"] = 32] = "SlipperyWhenWet";
})(ContaminantWithNoDepthEnum = exports.ContaminantWithNoDepthEnum || (exports.ContaminantWithNoDepthEnum = {}));
var MeasurementDeviceEnum;
(function (MeasurementDeviceEnum) {
    MeasurementDeviceEnum[MeasurementDeviceEnum["TapleyMeter"] = 130] = "TapleyMeter";
    MeasurementDeviceEnum[MeasurementDeviceEnum["JamesDecelerometer"] = 131] = "JamesDecelerometer";
    MeasurementDeviceEnum[MeasurementDeviceEnum["Bowmonk"] = 132] = "Bowmonk";
    MeasurementDeviceEnum[MeasurementDeviceEnum["DecelerometerTES"] = 133] = "DecelerometerTES";
})(MeasurementDeviceEnum = exports.MeasurementDeviceEnum || (exports.MeasurementDeviceEnum = {}));
/**The location of the condition */
var OCLocationEnum;
(function (OCLocationEnum) {
    OCLocationEnum[OCLocationEnum["AlongInsideRedl"] = 127] = "AlongInsideRedl";
    OCLocationEnum[OCLocationEnum["AlongInsideRwyEdge"] = 128] = "AlongInsideRwyEdge";
    OCLocationEnum[OCLocationEnum["AlongClearedWidth"] = 129] = "AlongClearedWidth";
    OCLocationEnum[OCLocationEnum["FMCL"] = 134] = "FMCL";
})(OCLocationEnum = exports.OCLocationEnum || (exports.OCLocationEnum = {}));
/**
* The side of the centreline for the current edge of the cleared area. Either E-W
* or N-S the latter enumerations depending upon exact runway orientation: Low end # from 05 to 13
* use N/S; anything else use E/W.
*/
var OffsetSideEnum;
(function (OffsetSideEnum) {
    OffsetSideEnum[OffsetSideEnum["East"] = 11] = "East";
    OffsetSideEnum[OffsetSideEnum["West"] = 12] = "West";
    OffsetSideEnum[OffsetSideEnum["North"] = 13] = "North";
    OffsetSideEnum[OffsetSideEnum["South"] = 14] = "South";
})(OffsetSideEnum = exports.OffsetSideEnum || (exports.OffsetSideEnum = {}));
var OtherConditionsAreaEnum;
(function (OtherConditionsAreaEnum) {
    OtherConditionsAreaEnum[OtherConditionsAreaEnum["IcePatches"] = 122] = "IcePatches";
    OtherConditionsAreaEnum[OtherConditionsAreaEnum["CompactedSnowPatches"] = 123] = "CompactedSnowPatches";
    OtherConditionsAreaEnum[OtherConditionsAreaEnum["StandingWaterPatches"] = 124] = "StandingWaterPatches";
})(OtherConditionsAreaEnum = exports.OtherConditionsAreaEnum || (exports.OtherConditionsAreaEnum = {}));
var OtherConditionsEdgeEnum;
(function (OtherConditionsEdgeEnum) {
    OtherConditionsEdgeEnum[OtherConditionsEdgeEnum["SnowDrifts"] = 120] = "SnowDrifts";
    OtherConditionsEdgeEnum[OtherConditionsEdgeEnum["Windrows"] = 121] = "Windrows";
    OtherConditionsEdgeEnum[OtherConditionsEdgeEnum["SnowBanks"] = 125] = "SnowBanks";
})(OtherConditionsEdgeEnum = exports.OtherConditionsEdgeEnum || (exports.OtherConditionsEdgeEnum = {}));
var TreatmentEnum;
(function (TreatmentEnum) {
    TreatmentEnum[TreatmentEnum["LoseSand"] = 100] = "LoseSand";
    TreatmentEnum[TreatmentEnum["Unused101"] = 101] = "Unused101";
    TreatmentEnum[TreatmentEnum["Graded"] = 104] = "Graded";
    TreatmentEnum[TreatmentEnum["Packed"] = 106] = "Packed";
    TreatmentEnum[TreatmentEnum["Unused107"] = 107] = "Unused107";
    TreatmentEnum[TreatmentEnum["ChemicallyTreated"] = 108] = "ChemicallyTreated";
    TreatmentEnum[TreatmentEnum["Scarified"] = 109] = "Scarified";
})(TreatmentEnum = exports.TreatmentEnum || (exports.TreatmentEnum = {}));
/**Distance Unit of Measure: either ft or ins. */
var UnitOfMeasurement;
(function (UnitOfMeasurement) {
    UnitOfMeasurement[UnitOfMeasurement["Inches"] = 9] = "Inches";
    UnitOfMeasurement[UnitOfMeasurement["Feet"] = 10] = "Feet";
})(UnitOfMeasurement = exports.UnitOfMeasurement || (exports.UnitOfMeasurement = {}));
//# sourceMappingURL=rsc.data.model.js.map
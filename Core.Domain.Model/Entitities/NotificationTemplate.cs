﻿using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class NotificationTemplate
    {
        public int Id { get; set; }

        public NotificationType NotificationType { get; set; }

        public TemplateType TemplateType { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string SubjectEng { get; set; }

        public string SubjectFr { get; set; }

        public string BodyEng { get; set; }

        public string BodyFr { get; set; }

        public int NotificationFrequency { get; set; }
    }
}
﻿using NDS.DataBus.WebClient.Subscriptions;
using System;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace NDS.DataBus.WebClient.Controllers
{
    public class TopicController : ApiController
    {
        public IHttpActionResult Create(string userName, string  password, string topic, string subscription, string filters)
        {
            var subsName = string.IsNullOrEmpty(subscription) ? $"Subs-{GetUserId("UNKNOWN_CLIENT")}" : subscription;
            var id = SubscriptionManager.CreateSubscription(userName, password, topic, subsName, filters);
            return Ok(id);
        }

        [HttpGet]
        public IHttpActionResult Drain(string id)
        {
            try
            {
                var result = SubscriptionManager.DrainTopic(id);
                if (result != null)
                    return Ok(result);
                else
                    return NotFound();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        [HttpPost]
        public IHttpActionResult StopListening(string id)
        {
            try
            {
                SubscriptionManager.StopListening(id);
                return Ok(1);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        public string GetUserId(string defValue)
        {
            if (Request.Properties.ContainsKey("MS_HttpContext"))
            {
                var ctx = Request.Properties["MS_HttpContext"] as HttpContextBase;
                if (ctx != null)
                {
                    return string.Join(".", (ctx.Request.UserHostAddress ?? "").Where(c => char.IsLetterOrDigit(c)));
                }
            }
            return defValue;
        }

        private SubscriptionManager SubscriptionManager => SubscriptionManager.Instance;
    }
}

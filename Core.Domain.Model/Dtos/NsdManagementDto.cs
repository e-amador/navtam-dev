﻿namespace NavCanada.Core.Domain.Model.Dtos
{
    public class NsdManagementDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
        public int OrgId { get; set; }
        public bool Operational { get; set; }
    }
}
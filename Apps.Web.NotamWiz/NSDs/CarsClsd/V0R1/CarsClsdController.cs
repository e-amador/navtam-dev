﻿using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.NSDs.CarsClsd.V0R1
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Code.Extentions;
    using Code.GeoTools;
    using Code.Helpers;
    using Models;
    using Core.Common.Contracts;
    using Core.Data.NotamWiz.Contracts;

    public class CarsClsdController : NsdBaseController
    {
        private const int BeamLineThreshold = 50;
        private const int RoundingDistance = 10;

        private readonly ILog _log = LogManager.GetLogger(typeof(CarsClsdController));

        public CarsClsdController(IUow uow, IClusterQueues clusterQueues)
            : base(uow, clusterQueues)
        {
        }
        protected string GetSchScriptLocation()
        {
            _log.Object(new
            {
                Method = "GetScriptLocation",
                Parameters = new
                {
                }
            });
            var parts = GetType().Namespace.Split('.');
            var count = parts.Count();
            return "/Nsds/" + parts[count - 2] + "/" + parts[count - 1] + "/CarsSchedule.js";
        }

       
        public override ActionResult Index(string id)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            dictionary.Add("Schdlr_valOverlaping_mesage", DbRes.T("Overlapping events are not allowed", "CarsSchedule"));

            dictionary.Add("Schdlr_fieldDate_title", DbRes.T("EDateId", "CarsSchedule"));
            dictionary.Add("Schdlr_fieldEndDate_title", DbRes.T("EndDateId", "CarsSchedule"));
            dictionary.Add("Schdlr_fieldTime_title", DbRes.T("TimeId", "CarsSchedule"));
            dictionary.Add("Schdlr_fieldEndTime_title", DbRes.T("EndTimeId", "CarsSchedule"));
            dictionary.Add("Schdlr_fieldContEvent_title", DbRes.T("ContinuesEvent", "CarsSchedule"));
            dictionary.Add("Schdlr_fieldrbSelection_title", DbRes.T("RadioButtonSelectionWCT", "CarsSchedule"));
            dictionary.Add("Schdlr_fieldAction_title", DbRes.T("Action", "CarsSchedule"));
            dictionary.Add("Schdlr_fieldDateTime_title", DbRes.T("Start and End Date/Time", "CarsSchedule"));
            dictionary.Add("Schdlr_fieldPeriod_title", DbRes.T("Period", "CarsSchedule"));
            dictionary.Add("Schdlr_fieldPeriodDT_title", DbRes.T("DT Period", "CarsSchedule"));
            dictionary.Add("Schdlr_SchRequired", DbRes.T("Schedule is Required", "CarsSchedule"));
            ViewBag.ScheduleData = Json(dictionary);

            return View(new ViewModel());
        }

        [HttpPost]
        public ActionResult GetTimeScheduleApi()
        {

            return PartialView("CarsSchedule");
        }


        protected override async Task<Tuple<ProcessResult, NotamProposalViewModel>> GenerateAsync(string proposalType, NotamProposalViewModel notamProposal, NotamProposalViewModelBase notamProposalBase)
        {
            _log.Object(new
            {
                Method = "GenerateAsync",
                Parameters = new
                {
                    proposalType,
                    notamProposal,
                    notamProposalBase
                }
            });
            var proposal = notamProposalBase as ViewModel;
            if (proposal != null)
            {
                notamProposal.IsCatchAll = false;
                notamProposal.ContainFreeText = false;
                if (proposalType == "C")
                {
                    //var itemELatLong = GeoDataService.IsValidDecimalLocation(proposal.Location) ? new GeoCoordinate().FromLocation(proposal.Location).ToDMSLocation() : proposal.Location;
                    notamProposal.Type = proposalType;
                    notamProposal.ReferredNumber = notamProposal.SubmittedNotam.Number;
                    notamProposal.ReferredSeries = notamProposal.SubmittedNotam.Series;
                    notamProposal.ReferredYear = notamProposal.SubmittedNotam.Year;
                    notamProposal.Code45 = "CN";
                    notamProposal.ItemE = string.Format("{0}", "Open");
                    notamProposal.ItemEFrench = proposal.Bilingual ? string.Format("{0}", "Open-FR") : string.Empty;
                    return new Tuple<ProcessResult, NotamProposalViewModel>(new ProcessResult(), notamProposal);
                }
                else
                {
                    notamProposal.Purpose = "B";
                    notamProposal.Traffic = "IV";
                    notamProposal.Code23 = "SF";
                    notamProposal.Code45 = GetCode45(proposal.CondStatusValue);
                    notamProposal.Scope = "A";
                    notamProposal.Lower = 0;
                    notamProposal.Upper = 999;
                    notamProposal.Radius = 5;
                    notamProposal.Fir = Subject.Fir;
                    notamProposal.ItemA = GetItemA(Subject.Ad);
                    notamProposal.Coordinates = GeoDataService.DMSToRoundedDM(string.Format("{0} {1}", GeoDataService.DecimalToDMS(Subject.SubjectGeoRefLat, true), GeoDataService.DecimalToDMS(Subject.SubjectGeoRefLong, false)));
                    notamProposal.ItemE = GetItemE(false, proposal.SchText, proposal.CondStatusValue);
                    notamProposal.ItemEFrench = GetItemE(true, proposal.SchText, proposal.CondStatusValue);
              
                }              
            }
            return new Tuple<ProcessResult, NotamProposalViewModel>(new ProcessResult(), notamProposal);
        }

        protected override async Task<NotamProposalViewModel> ProcessNewProposalAsync(NotamProposalViewModel notamProposal)
        {
            return notamProposal;
        }


         private string GetCode45(int condStatusValue)
        {
             return condStatusValue == 0 ? "LC" : (condStatusValue == 1 ? "AH" : "");
        }
        private string GetItemE(bool bilingual, string schText, int condStatusValue)
        {
            return condStatusValue == 0 ? ((bilingual ? "STATION RADIO D'AD COMMUNAUTAIRE (CARS) CLSD" : "COMMUNITY AD AVIATION STATION (CARS) CLSD")) : (condStatusValue == 1 ? ((bilingual ? "HR D'OPS DE LA STATION RADIO D'AD COMMUNAUTAIRE (CARS):" : "COMMUNITY AD AVIATION STATION (CARS) HR OF OPS:") + "\n" + schText) : "");
        }

        protected override async Task<ProcessResult> ValidateAsync(NotamProposalViewModelBase notamProposalBase)
        {
            this._log.Object(new
            {
                Method = "ValidateAsync",
                Parameters = new
                {
                    notamProposalBase
                }
            });
            if (AeroRdsUtility.GetAhpByMid(Subject.SubjectMid) == null || !Subject.SdoAttributes.Contains("CARS : True"))
            {
                return new ProcessResult
                {
                    Success = false,
                    ErrorCode = "AOAC-003",
                    Errors = new List<string>
                    {
                        "Invalid Aerodrome."
                    }
                };
            }
            var validator = new CarsValidator(this, DbRes);
            var validationResult = await validator.ValidateAsync(notamProposalBase as ViewModel);
            if (!validationResult.IsValid)
            {
                var result = new ProcessResult
                {
                    Success = false,
                    ErrorCode = "AOAC-001"
                };
                foreach (var failure in validationResult.Errors)
                {
                    result.Errors.Add(failure.ErrorMessage);
                }
                return result;
            }
            return new ProcessResult();
        }

     }
}
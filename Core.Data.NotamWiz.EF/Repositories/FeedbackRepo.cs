﻿namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    using System.Data.Entity;

    using Contracts.Repositories;
    using Domain.Model.Entitities;

    public class FeedbackRepo : Repository<Feedback>, IFeedbackRepo
    {
        public FeedbackRepo(DbContext dbContext)
            : base(dbContext)
        {
        }
    }
}

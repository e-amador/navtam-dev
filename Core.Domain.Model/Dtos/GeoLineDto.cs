﻿namespace NavCanada.Core.Domain.Model.Dtos
{
    using System.Device.Location;

    public class GeoLineDto
    {
        public GeoCoordinate X { get; private set; }
        public GeoCoordinate Y { get; private set; }

        public GeoLineDto(GeoCoordinate x, GeoCoordinate y)
        {
            X = x;
            Y = y;
        }

    }
}

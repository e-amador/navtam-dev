﻿namespace NavCanada.Core.TaskEngine.MessageTask
{
    using Common.Contracts;
    using Domain.Model.Entitities;
    using Contracts;
    using Implementations;
    using Mto;
    using System;

    /// <summary>
    /// Message Task Context
    /// </summary>
    public class MessageTaskContext : IMessageTaskContext
    {
        /// <summary>
        /// All context queues
        /// </summary>
        public IClusterQueues Queues
        {
            get;
            set;
        }

        /// <summary>
        /// Message Transfer Object
        /// </summary>
        public MessageTransferObject Mto
        {
            get;
            set;
        }

        public IClusterStorage ClusterStorage
        {
            get;
            set;
        }

        public IExternalServices ExternalServices
        {
            get;
            set;
        }

        public Proposal Proposal
        {
            get;
            set;
        }

        public MtoProcessorSettings MtoProcessorSettings
        {
            get;
            set;
        }

        public bool DiscardSignalrNotification
        {
            get;
            set;
        }

        public object Data
        {
            get;
            set;
        }
    }
}
﻿namespace NavCanada.Core.Scheduler.Contracts
{
    public interface IStorageFactory
    {
        bool NewStorageConnection(out IClusterStorage clusterStorage);
    }
}

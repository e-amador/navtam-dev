﻿using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NAVTAM.ViewModels;
using System;
using System.Threading.Tasks;

namespace NAVTAM.NsdEngine.RWYCLSD.V10
{
    public class RwyClsdNsdManager : NsdManager
    {
        public RwyClsdNsdManager(int categoryId, string categoryName, string version)
            : base(categoryId, categoryName, version) { }

        protected override TokenViewModel CreateTokenViewModel()
        {
            return new RwyClsdViewModel();
        }

        protected override TokenViewModel DeserializeToken(string tokens)
        {
            return Deserialize<RwyClsdViewModel>(tokens);
        }

        protected override ValidationResult ValidateModel(ProposalViewModel model, ProposalContext context)
        {
            var token = model.Token as RwyClsdViewModel;
            if (token == null) return ValidationResult.Fail(Resources.InvalidTokenType);

            if (context.SdoCacheSubject().Equals(null)) return ValidationResult.Fail(Resources.InvalidSubjectId);

            if (string.IsNullOrWhiteSpace(token.EffectedRunwayName)) return ValidationResult.Fail(Resources.InvalidRunway);


            var forbiddenWords = context.GetContext<string>("ForbiddenItemEWords");
            if (!string.IsNullOrWhiteSpace(token.ClosureReasonId) && token.ClosureReasonId.Equals("Re9"))
            {
                if (ContainsForbiddenWord(token.ClosureReasonOtherFreeText, forbiddenWords))
                    return ValidationResult.Fail(Resources.CARS_ReasonForbiddenWords); //It use the same message text as CARS
            }

            if (!string.IsNullOrWhiteSpace(token.ClosureReasonId) && token.ClosureReasonId.Equals("Re9"))
            {
                if (ContainsForbiddenWord(token.ClosureReasonOtherFreeText, forbiddenWords))
                    return ValidationResult.Fail(Resources.CARS_ReasonForbiddenWords); //It use the same message text as CARS

                if (ContainsForbiddenWord(token.ClosureReasonOtherFreeTextFr, forbiddenWords))
                    return ValidationResult.Fail(Resources.CARS_ReasonForbiddenWords); //It use the same message text as CARS
            }
            
            if (token.TxyRestrictions)
            {
                if (ContainsForbiddenWord(token.FreeTextTaxiwayRestriction, forbiddenWords))
                    return ValidationResult.Fail(Resources.CARS_ReasonForbiddenWords); //It use the same message text as CARS

                if (ContainsForbiddenWord(token.FreeTextTaxiwayRestrictionFr, forbiddenWords))
                    return ValidationResult.Fail(Resources.CARS_ReasonForbiddenWords); //It use the same message text as CARS
            }         

            return ValidationResult.Succeed();
        }


        protected override bool ContainFreeText => false;
        protected bool CheckForFreeText(ProposalContext context, TokenViewModel token)
        {
            var freeTextclosure = (token as RwyClsdViewModel).ClosureReasonOtherFreeText;
            var freeTextRestrictions = (token as RwyClsdViewModel).FreeTextTaxiwayRestriction;

            if (!string.IsNullOrWhiteSpace(freeTextclosure) || !string.IsNullOrWhiteSpace(freeTextRestrictions)) return true;

            return false;
        }

        protected override string GetNotamLocation(TokenViewModel token)
        {
            return (token as RwyClsdViewModel).SubjectLocation;
        }

        protected override async Task UpdateNsdContext(TokenViewModel token, ProposalContext context)
        {
            var subjectViewModel = await GetSubjectViewModel(token.SubjectId, context.Uow);
            var subjectId = (token as RwyClsdViewModel).EffectedAerodrome;
            var sdoCacheSubject = await context.Uow.SdoCacheRepo.GetByIdAsync(subjectId);
            context.AddContext("SdoCacheSubject", sdoCacheSubject);
        }

        private void UpdateNSDContextForCancellation(ProposalContext context, TokenViewModel token)
        {
        }

        protected override NearSdoSubject GetNearestSdoSubject(TokenViewModel token, ProposalContext context)
        {
            return (context.SdoCacheSubject() != null) ? new NearSdoSubject(context.SdoCacheSubject(), 0.0) : null;
            //return new NearSdoSubject(context.SdoCacheSubject(), 0.0);
        }

        protected override string GetFir(TokenViewModel token, ProposalContext context)
        {
            return context.NearestSubject.Subject?.Fir?.ToUpper();
        }

        protected override string GenerateItemA(TokenViewModel token, ProposalContext context)
        {
            return CovertAerodromeDesignatorToItemA(context.SdoCacheSubject().Designator);
        }

        protected override BilingualText GenerateItemE(TokenViewModel baseToken, ProposalContext context)
        {
            var rwyCont = context.SdoCacheSubject();
            var token = baseToken as RwyClsdViewModel;
            var itemEPrefix = GetItemEAerodromeNamePrefix(context, rwyCont.Name).Append(GetAmendPubMessagePrefix(token));

            var itemE = !context.IsCancelation ?
                GenerateItemE(context, token) :
                GenerateCancelItemE(context, token);

            if (itemEPrefix.English != null)
            {
                itemE.English = itemEPrefix.English + itemE.English;
                if (context.IsBilingual) itemE.French = itemEPrefix.French + itemE.French;
            }

            if (!context.IsBilingual)
                itemE.French = string.Empty;

            return itemE;
        }

        protected override int GetRadius(TokenViewModel token, ProposalContext context)
        {
            return 005;
        }

        protected override string GetScope(TokenViewModel token, ProposalContext context)
        {
            return ScopeType.A.ToString();
        }

        protected override async Task<string> GetSeries(TokenViewModel token, ProposalContext context)
        {
            return await GetNotamSeriesForSubject(context.Uow, context.SdoCacheSubject(), "MR", false);
        }

        protected override string GetTraffic(TokenViewModel token, ProposalContext context)
        {
            return TrafficType.IV.ToString();
        }

        protected override string GetCode23(TokenViewModel token, ProposalContext context)
        {
            return "MR";
        }

        protected override string GetCode45(TokenViewModel token, ProposalContext context)
        {
            return context.IsCancelation ? "AK" : "LC";
        }

        protected override string GetPurpose(TokenViewModel token, ProposalContext context)
        {
            return PurposeType.NBO.ToString();
        }

        protected override int GetLowerLimit(TokenViewModel token, ProposalContext context)
        {
            return 0;
        }

        protected override int GetUpperLimit(TokenViewModel token, ProposalContext context)
        {
            return 999;
        }

        protected override string GetDisplayName()
        {
            return Resources.RwyClsdDisplayName;
        }

        #region ItemE
        private BilingualText GenerateItemE(ProposalContext context, RwyClsdViewModel rwyClsdViewModel)
        {
            if (context.IsBilingual)
                return RwyClsdItemEGenerator.BuildItemE(rwyClsdViewModel, new RwyClsdReasons());

            return RwyClsdItemEGenerator.BuildItemE(rwyClsdViewModel, new RwyClsdReasons());
        }

        private BilingualText GenerateCancelItemE(ProposalContext context, RwyClsdViewModel rwyClsdViewModel)
        {
            Tuple<string, string> cancelItemE = new Tuple<string, string>(CancelItemEng(rwyClsdViewModel), CancelItemEFrench(rwyClsdViewModel));

            return new BilingualText { English = cancelItemE.Item1, French = cancelItemE.Item2 };
        }

        private string CancelItemEFrench(RwyClsdViewModel model)
        {
            return $"{model.EffectedRunwayName.Replace('-', ' ')} OPN";
        }
        
        private string CancelItemEng(RwyClsdViewModel model)
        {
            return $"{model.EffectedRunwayName.Replace('-', ' ')} OPN";
        }

        protected override string GetNsdHelpLink()
        {
            return Resources.RwyClsdHelpLink;
        }

        #endregion
    }

    static class RwyClsdNsdManagerExt
    {
        internal static SdoCache SdoCacheSubject(this ProposalContext context) => context.GetContext<SdoCache>("SdoCacheSubject");
    }
}
﻿namespace RSC.Business.SDO
{
    public interface ISDORepository
    {
        IAerodrome GetAerodrome(string designator);
        string[] GetForbiddenAftnWords();
    }
}

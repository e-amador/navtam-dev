﻿using NavCanada.Core.Domain.Model.Entitities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace NavCanada.Core.Data.EF.Configurations
{
    public class AerodromeRSCConfig : EntityTypeConfiguration<AerodromeRSC>
    {
        public AerodromeRSCConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(e => e.Designator)
                .HasMaxLength(4)
                .IsRequired()
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(new IndexAttribute("IX_DESIGNATOR", 1) { IsUnique = true }));

            Property(e => e.RowVersion)
                .IsRowVersion()
                .IsConcurrencyToken();
        }
    }
}

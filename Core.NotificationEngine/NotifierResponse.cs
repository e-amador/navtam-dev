﻿using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Core.NotificationEngine
{
    public class NotifierResponse
    {
        public ApiStatusCode Code { get; set; }

        public string ErrorMessage { get; set; }
    }
}

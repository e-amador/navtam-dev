﻿import { Injectable } from '@angular/core';
import { Resolve,  ActivatedRouteSnapshot } from '@angular/router';

import { INsdCategory } from './../shared/data.model';
import { DataService } from './../shared/data.service'

@Injectable()
export class ProposalCancelModelResolver implements Resolve<any> {

    constructor( private dataService : DataService ) {
    }

    resolve( route : ActivatedRouteSnapshot ) {
        const id = +route.params['id'];
        return this.dataService.cancelProposal(id).map(model => model);
    }
}
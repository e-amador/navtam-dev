﻿namespace NavCanada.Core.TaskEngine.Tasks.DiagnosticTask
{
    using System;

    using SqlServiceBroker.Contracts;
    using Contracts;
    using Mto;

    public class ChatDiagnosticTask : IMessageTask
    {
        public void Execute(IMessageTaskContext context)
        {
            // retrieve conversation handle from mto
            var senderConversationIdFromMto = Guid.Parse(context.Mto[MessageDefs.SenderConversationIdKey].Value.ToString());

            // create response mto
            var responseMto = new MessageTransferObject();
            responseMto.Add(MessageDefs.UniqueIdentifierKey, Guid.NewGuid().ToString(), DataType.String);
            responseMto.Add(MessageDefs.DiagnosticChatPipeKey, "Pong...", DataType.String);
            var conversationHandler = context.ClusterStorage.Queues.InitiatorQueue as IConversationHandler;
            if (conversationHandler != null)
            {
                conversationHandler.SendOnConversation(senderConversationIdFromMto);
            }
            context.ClusterStorage.Queues.InitiatorQueue.Publish(responseMto);

            context.ExternalServices.Logger.LogDebug(GetType(), "ChatDiagnosticTask: Task finish excecuting...");
        }
    }
}
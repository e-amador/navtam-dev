﻿using NavCanada.Applications.NotamWiz.Web.Code.Helpers;
using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.NSDs.ObstLgtUS.V0R1
{
    using System.Device.Location;
    using System.Threading.Tasks;

    using FluentValidation;

    using NavCanada.Applications.NotamWiz.Web.Code.Extentions;
    using NavCanada.Applications.NotamWiz.Web.Code.GeoTools;


    public class ObstLgtUSValidator : AbstractValidator<ViewModel>
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ObstLgtUSValidator));

        public ObstLgtUSValidator(NsdBaseController controller, IDbResWrapper DbRes)
        {
            this.log.Object(new
            {
                Method = "ObstLgtUSValidator",
                Parameters = new
                {
                    controller
                }
            });
            Controller = controller;
            RuleFor(v => v.Height).GreaterThan(0).LessThanOrEqualTo(3000).WithMessage(DbRes.T("Invalid Height", "NSD-ObstLgtUS"));
            RuleFor(v => v.Elevation).GreaterThan(0).LessThanOrEqualTo(99999).WithMessage(DbRes.T("Invalid Elevation", "NSD-ObstLgtUS"));
            RuleFor(v => v.Location).Must(GeoDataService.IsValidLocation).WithMessage(DbRes.T("Invalid Location", "NSD-ObstLgtUS")).MustAsync(BeWithinDoa).WithMessage(DbRes.T("Obstacle Location is not Within your Domain of Authority.", "NSD-ObstLgtUS"));
        }

        private NsdBaseController Controller
        {
            get;
            set;
        }

        private async Task<bool> BeWithinDoa(string location)
        {
            var loc = new GeoCoordinate().FromLocation(location);
            this.log.Object(new
            {
                Method = "BeWithinDoa",
                Parameters = new
                {
                    location = loc
                }
            });
            var doa = await Controller.GetUserDoaAsync();
            return GeoDataService.IsLocationWithinDOA(loc, doa);
        }
    }
}
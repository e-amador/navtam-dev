"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IcaoConditionEditComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var windowRef_service_1 = require("../../common/windowRef.service");
var angular_l10n_1 = require("angular-l10n");
var toastr_service_1 = require("./../../common/toastr.service");
var data_service_1 = require("../shared/data.service");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var IcaoConditionEditComponent = /** @class */ (function () {
    function IcaoConditionEditComponent(toastr, fb, dataService, router, activatedRoute, winRef, memStorageService, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.winRef = winRef;
        this.memStorageService = memStorageService;
        this.locale = locale;
        this.translation = translation;
        this.model = null;
        this.radiusLimit = 999;
        this.lowerLimit = 998;
        this.upperLimit = 999;
        this.descriptionTextLimit = 150;
        this.isSubmitting = false;
        if (this.winRef.appConfig.ccs)
            this.router.navigate(['/administration']);
    }
    IcaoConditionEditComponent.prototype.ngOnInit = function () {
        this.action = this.activatedRoute.snapshot.data[0].action;
        this.model = this.activatedRoute.snapshot.data['model'];
        this.isReadOnly = this.action === "view";
        this.configureReactiveForm();
    };
    IcaoConditionEditComponent.prototype.onSelectedCodeChanged = function (data) {
        if (data.value !== "-1") {
            this.icaoConditionForm.get('code').setValue(data.value);
            this.icaoConditionForm.markAsDirty();
        }
        else {
            this.icaoConditionForm.get('code').setValue("-1");
        }
    };
    IcaoConditionEditComponent.prototype.backToIcaoSubjectList = function () {
        this.router.navigate(['/administration/icaosubjects']);
    };
    IcaoConditionEditComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.isSubmitting)
            return;
        this.isSubmitting = true;
        var icaoCondition = this.prepareUpdateIcaoCondition();
        this.dataService.updateIcaoCondition(icaoCondition)
            .subscribe(function (data) {
            var msg = _this.translation.translate('IcaoSubjectSession.SuccessConditionUpdated');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            _this.backToIcaoSubjectList();
        }, function (error) {
            _this.isSubmitting = false;
            var msg = _this.translation.translate('IcaoSubjectSession.FailureConditionUpdated');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    IcaoConditionEditComponent.prototype.validateDescription = function () {
        var ctrl = this.icaoConditionForm.get("description");
        return ctrl.valid || this.icaoConditionForm.pristine;
    };
    IcaoConditionEditComponent.prototype.validateDescriptionLength = function (ctrlName) {
        var descCtrl = this.icaoConditionForm.get(ctrlName);
        var DescriptionLength = descCtrl.value;
        return DescriptionLength.length > this.descriptionTextLimit;
    };
    IcaoConditionEditComponent.prototype.validateDescriptionFrench = function () {
        var ctrl = this.icaoConditionForm.get("descriptionFrench");
        return ctrl.valid || this.icaoConditionForm.pristine;
    };
    IcaoConditionEditComponent.prototype.validateCode45 = function () {
        if (this.icaoConditionForm.pristine)
            return true;
        var fc = this.icaoConditionForm.get("code");
        if (fc.errors)
            return false;
        if (!fc.value)
            return false;
        if (!isALetter(fc.value))
            return false;
        if (fc.value.length !== 2)
            return false;
        return true;
    };
    IcaoConditionEditComponent.prototype.validateRadius = function () {
        if (this.icaoConditionForm.pristine)
            return true;
        var fc = this.icaoConditionForm.get('radius');
        if (fc.errors)
            return false;
        if (fc.value) {
            if (fc.value > this.radiusLimit)
                return false;
            if (fc.value < 1)
                return false;
        }
        else
            return false;
        return true;
    };
    IcaoConditionEditComponent.prototype.validateLowerLimit = function () {
        if (this.icaoConditionForm.pristine)
            return true;
        var ll = this.icaoConditionForm.get('lowerLimit');
        var ul = this.icaoConditionForm.get('upperLimit');
        if (ll.value === undefined)
            return false;
        if (!isANumber(ll.value))
            return false;
        if (+ll.value > this.lowerLimit)
            return false;
        if (+ul.value && isANumber(ul.value)) {
            if (+ll.value < +ul.value)
                return true;
            else
                return false;
        }
        return true;
    };
    IcaoConditionEditComponent.prototype.validateUpperLimit = function () {
        if (this.icaoConditionForm.pristine)
            return true;
        var ll = this.icaoConditionForm.get('lowerLimit');
        var ul = this.icaoConditionForm.get('upperLimit');
        if (!ul.value)
            return false;
        if (!isANumber(ul.value))
            return false;
        if (+ul.value > this.upperLimit)
            return false;
        if (+ll.value && isANumber(ll.value)) {
            if (+ll.value >= +ul.value)
                return false;
        }
        return true;
    };
    IcaoConditionEditComponent.prototype.disableSubmit = function () {
        if (this.icaoConditionForm.pristine || !this.icaoConditionForm.valid)
            return true;
        if (!this.validateCode45() || !this.validateLowerLimit() || !this.validateUpperLimit())
            return true;
        if (this.isSubmitting)
            return true;
        return false;
    };
    IcaoConditionEditComponent.prototype.checkboxChanged = function (controlName, checked) {
        var control = this.icaoConditionForm.get(controlName);
        control.setValue(checked);
        control.markAsDirty();
    };
    IcaoConditionEditComponent.prototype.decValue = function (fieldName) {
        var value = 0;
        switch (fieldName) {
            case 'lowerLimit':
                value = +this.icaoConditionForm.get('lowerLimit').value;
                if (value > 0) {
                    this.icaoConditionForm.get('lowerLimit').setValue(value - 1);
                    this.icaoConditionForm.get('lowerLimit').updateValueAndValidity();
                    this.icaoConditionForm.get('upperLimit').updateValueAndValidity();
                }
                break;
            case 'upperLimit':
                value = +this.icaoConditionForm.get('upperLimit').value;
                if (value > 1) {
                    this.icaoConditionForm.get('upperLimit').setValue(value - 1);
                    this.icaoConditionForm.get('lowerLimit').updateValueAndValidity();
                    this.icaoConditionForm.get('upperLimit').updateValueAndValidity();
                }
                break;
            case 'radius':
                value = +this.icaoConditionForm.get('radius').value;
                if (value > 1) {
                    this.icaoConditionForm.get('radius').setValue(value - 1);
                    this.icaoConditionForm.get('radius').markAsDirty();
                }
                break;
        }
    };
    IcaoConditionEditComponent.prototype.incValue = function (fieldName) {
        var value = 0;
        switch (fieldName) {
            case 'lowerLimit':
                value = +this.icaoConditionForm.get('lowerLimit').value;
                if (value < this.lowerLimit) {
                    this.icaoConditionForm.get('lowerLimit').setValue(value + 1);
                    this.icaoConditionForm.get('lowerLimit').updateValueAndValidity();
                }
                break;
            case 'upperLimit':
                value = +this.icaoConditionForm.get('upperLimit').value;
                if (value < this.upperLimit) {
                    this.icaoConditionForm.get('upperLimit').setValue(value + 1);
                    this.icaoConditionForm.get('upperLimit').updateValueAndValidity();
                }
                break;
            case 'radius':
                value = +this.icaoConditionForm.get('radius').value;
                if (value < this.radiusLimit) {
                    this.icaoConditionForm.get('radius').setValue(value + 1);
                    this.icaoConditionForm.get('radius').markAsDirty();
                }
                break;
        }
    };
    IcaoConditionEditComponent.prototype.validateKeyPressNumber = function (evt) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
        }
    };
    IcaoConditionEditComponent.prototype.validateKeyPressLetter = function (evt) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (key !== '\u0000' && !isALetter(key)) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
        }
    };
    IcaoConditionEditComponent.prototype.configureReactiveForm = function () {
        this.icaoConditionForm = this.fb.group({
            description: [{ value: this.model.description, disabled: this.isReadOnly }, [forms_1.Validators.required]],
            descriptionFrench: [{ value: this.model.descriptionFrench, disabled: this.isReadOnly }, [forms_1.Validators.required]],
            code: [{ value: this.model.code, disabled: this.isReadOnly }, [forms_1.Validators.required]],
            radius: [{ value: this.model.radius, disabled: this.isReadOnly }, []],
            lowerLimit: [{ value: this.model.lower, disabled: this.isReadOnly }, []],
            upperLimit: [{ value: this.model.upper, disabled: this.isReadOnly }, []],
            grpPurposes: this.fb.group({
                purposeB: this.model.b,
                purposeM: this.model.m,
                purposeN: this.model.n,
                purposeO: this.model.o,
            }),
            grpTraffic: this.fb.group({
                trafficI: this.model.i,
                trafficV: this.model.v,
            }),
            requiresItemFG: [{ value: this.model.requiresItemFG, disabled: this.isReadOnly }],
            requiresPurpose: [{ value: this.model.requiresPurpose, disabled: this.isReadOnly }],
            cancellationOnly: [{ value: this.model.cancellationOnly, disabled: this.isReadOnly }],
            active: [{ value: this.model.active, disabled: this.isReadOnly }]
        });
        this.icaoConditionForm.get("radius").setValidators(radiusValidatorFn(this.radiusLimit));
        this.icaoConditionForm.get("grpPurposes").setValidators(purposeAllowValidator);
        this.icaoConditionForm.get("grpTraffic").setValidators(trafficValidator);
    };
    IcaoConditionEditComponent.prototype.prepareUpdateIcaoCondition = function () {
        var code45 = this.icaoConditionForm.get('code').value;
        code45 = code45.toString().toUpperCase();
        var icaoCondition = {
            id: this.model.id,
            subjectId: this.model.subjectId,
            description: this.icaoConditionForm.get('description').value,
            descriptionFrench: this.icaoConditionForm.get('descriptionFrench').value,
            code: code45,
            radius: this.icaoConditionForm.get('radius').value,
            lower: this.icaoConditionForm.get('lowerLimit').value,
            upper: this.icaoConditionForm.get('upperLimit').value,
            b: this.icaoConditionForm.get('grpPurposes.purposeB').value,
            m: this.icaoConditionForm.get('grpPurposes.purposeM').value,
            o: this.icaoConditionForm.get('grpPurposes.purposeO').value,
            n: this.icaoConditionForm.get('grpPurposes.purposeN').value,
            i: this.icaoConditionForm.get('grpTraffic.trafficI').value,
            v: this.icaoConditionForm.get('grpTraffic.trafficV').value,
            requiresItemFG: this.icaoConditionForm.get('requiresItemFG').value,
            requiresPurpose: this.icaoConditionForm.get('requiresPurpose').value,
            cancellationOnly: this.icaoConditionForm.get('cancellationOnly').value,
            active: this.icaoConditionForm.get('active').value
        };
        return icaoCondition;
    };
    IcaoConditionEditComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/icaosubjects/icaoconditions-edit.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            router_1.Router,
            router_1.ActivatedRoute,
            windowRef_service_1.WindowRef,
            mem_storage_service_1.MemoryStorageService,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], IcaoConditionEditComponent);
    return IcaoConditionEditComponent;
}());
exports.IcaoConditionEditComponent = IcaoConditionEditComponent;
function radiusValidatorFn(maxRadius) {
    return function (c) {
        if (typeof (c.value) === "string") {
            if (!c.value) {
                return { 'required': true };
            }
            if (c.value === "") {
                return { 'required': true };
            }
        }
        if (isANumber(c.value)) {
            if (+c.value < 1 || +c.value > maxRadius) {
                return { 'invalid': true };
            }
            var v = +c.value;
            if (v > maxRadius) {
                return { 'invalid': true };
            }
            return null;
        }
        return { 'invalid': true };
    };
}
function trafficValidator(c) {
    var i = c.get("trafficI");
    var v = c.get("trafficV");
    if (i.value || v.value) {
        return null;
    }
    return { 'trafficRequired': true };
}
function purposeAllowValidator(c) {
    var nCtrl = c.get("purposeN");
    var bCtrl = c.get("purposeB");
    var oCtrl = c.get("purposeO");
    var mCtrl = c.get("purposeM");
    var n = nCtrl.value;
    var b = bCtrl.value;
    var o = oCtrl.value;
    var m = mCtrl.value;
    if (n || b || o || m) {
        if (n && b && !o && !m)
            return null; //NB
        if (b && o && !m && !n)
            return null; //BO
        if (m && !o && !b && !n)
            return null; //M
        if (b && !o && !m && !n)
            return null; //B
        if (n && b && o && !m)
            return null; //NBO
        return { 'purposeInvalid': true };
    }
    else {
        return { 'purposeRequired': true };
    }
}
function isANumber(str) {
    return !/\D/.test(str);
}
function isALetter(str) {
    return !/[^a-z]/i.test(str);
}
//# sourceMappingURL=icaoconditions-edit.component.js.map
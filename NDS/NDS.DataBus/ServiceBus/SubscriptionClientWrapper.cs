﻿using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using NDS.DataBus.Contracts;

namespace NDS.DataBus.ServiceBus
{
    public class SubscriptionClientWrapper : ISubscription
    {
        public SubscriptionClientWrapper(NamespaceManager manager, SubscriptionClient client)
        {
            _namespaceManager = manager;
            _subscriptionClient = client;
        }

        public void Unsubscribe()
        {
            _subscriptionClient.Close();
            _namespaceManager.DeleteSubscription(_subscriptionClient.TopicPath, _subscriptionClient.Name);
        }

        readonly NamespaceManager _namespaceManager;
        readonly SubscriptionClient _subscriptionClient;
    }
}

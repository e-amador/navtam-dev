﻿import {Injectable} from '@angular/core';

import { IMemoryStorage } from './data.model';

@Injectable()
export class MemoryStorageService {

    ITEM_QUERY_OPTIONS_KEY = "ITEM_QUERY_OPTIONS_KEY";
    ITEM_ID_KEY = "ITEM_ID_KEY";
    NSD_CLIENTS_QUERY_OPTIONS_KEY = "NSD_CLIENTS_QUERY_OPTIONS_KEY";
    NSD_CLIENT_ID_KEY = "NSD_CLIENT_ID_KEY";

    save(key: string, value: any) {
        const index = this._memoryStorage.findIndex(x => x.key === key);
        if( index >= 0 ) {
            this._memoryStorage[index].value = value;
        } else {
            this._memoryStorage.push({
                key: key, 
                value: value});
        }
    }

    get(key) : any {
        const index = this._memoryStorage.findIndex(x => x.key === key);
        if( index >= 0) {
            return this._memoryStorage[index].value;
        }
        return null;
    }

    remove(key): any {
        const index = this._memoryStorage.findIndex(x => x.key === key);
        if (index >= 0) {
            this._memoryStorage.splice(index,1);
        }
    }

    private _memoryStorage : Array<IMemoryStorage> = [];
}
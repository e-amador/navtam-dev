﻿namespace NavCanada.Applications.NotamWiz.Web.oData
{
    using System.Linq;
    using System.Web.Http;
    using System.Web.OData;

    using NavCanada.Core.Data.NotamWiz.Contracts;
    using NavCanada.Core.Domain.Model.Entitities;

    [Authorize(Roles = "User")]
    public class OrganizationsController : BaseODataController
    {


        public OrganizationsController(IUow uow)
            : base(uow)        
        {
            
        }

        // GET: odata/Organizations
        [EnableQuery]
        public IQueryable<Organization> GetOrganizations()
        {
            return Uow.OrganizationRepo.GetAll();
        }

        // GET: odata/Organizations(5)
        [EnableQuery]
        public SingleResult<Organization> GetOrganization([FromODataUri] long key)
        {
            return SingleResult.Create(Uow.OrganizationRepo.GetAll().Where(organization => organization.Id == key));
        }


        [EnableQuery]
        public SingleResult<Doa> GetAssignedDOA([FromODataUri] long key)
        {
            return SingleResult.Create(Uow.OrganizationRepo.GetAll().Where(m => m.Id == key).Select(m => m.AssignedDoa));
        }

        // GET: odata/Organizations(5)/Groups
        //[EnableQuery]
        //public IQueryable<Group> GetGroups([FromODataUri] long key)
        //{
        //    return db.Organizations.Where(m => m.OrganizationId == key).SelectMany(m => m.Groups);
        //}

        // GET: odata/Organizations(5)/NOTAMProposals
        [EnableQuery]
        public IQueryable<NotamProposal> GetNOTAMProposals([FromODataUri] long key)
        {
            return Uow.OrganizationRepo.GetAll().Where(m => m.Id == key).SelectMany(m => m.NotamProposals);
        }

        // GET: odata/Organizations(5)/NSDs
        [EnableQuery]
        public IQueryable<Nsd> GetNSDs([FromODataUri] long key)
        {
            return Uow.OrganizationRepo.GetAll().Where(m => m.Id == key).SelectMany(m => m.Nsds);
        }

        //// GET: odata/Organizations(5)/OrganizationDOAs
        //[EnableQuery]
        //public IQueryable<Doa> GetOrganizationDOAs([FromODataUri] long key)
        //{
        //    return Uow.OrganizationRepo.GetAll().Where(m => m.Id == key).SelectMany(m => m.OrganizationDoas);
        //}

        // GET: odata/Organizations(5)/Users
        [EnableQuery]
        public IQueryable<UserProfile> GetUsers([FromODataUri] long key)
        {
            return Uow.OrganizationRepo.GetAll().Where(m => m.Id == key).SelectMany(m => m.Users);
        }


        private bool OrganizationExists(long key)
        {
            return Uow.OrganizationRepo.GetAll().Any(e => e.Id == key);
        }
    }
}

﻿namespace NavCanada.Core.TaskEngine.Mto
{
    using System;
    using System.Text;

    public class ByteArrayHelper
    {
        /// <summary>
        /// Given somthing like 0x12, return "01 02"
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="separator"></param>
        /// <param name="upperCase"></param>
        /// <returns></returns>
        public static string ToByteString(byte[] bytes, string separator = " ", bool upperCase = true)
        {
            if (bytes == null)
                return null;

            return ToByteString(bytes, 0, bytes.Length, separator, upperCase);
        }

        public static string ToByteString(byte[] bytes, int index, int count, string separator = " ", bool upperCase = true)
        {
            if (bytes == null)
                return null;

            if (bytes.Length == 0)
                return String.Empty;

            var sb = new StringBuilder(((index + count) * (2 + separator.Length)) - separator.Length);

            for (var i = index; i < (index + count); i++)
            {
                if (i > index)
                    sb.Append(separator);

                sb.Append(bytes[i].ToString(upperCase ? "X02" : "x02"));
            }

            return sb.ToString();
        }
    }
}

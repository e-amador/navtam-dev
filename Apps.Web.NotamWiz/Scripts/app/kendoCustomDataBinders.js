﻿define(["jquery", "kendo/kendo.binder.min"], function ($) {
    kendo.data.binders.customvisibility = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            kendo.data.Binder.fn.init.call(this, element, bindings, options);
            this.visibleCase = $(element).data('visiblecase');
        },

        refresh: function () {
            var data = this.bindings['customvisibility'].get();
            if (data) {
                if ($.inArray(data, this.visibleCase) >= 0) {
                    $(this.element).css('visibility', 'visible');
                } else {
                    $(this.element).css('visibility', 'collapse');
                }
            }
        }
    });

    kendo.data.binders.customEnabled = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            kendo.data.Binder.fn.init.call(this, element, bindings, options);
            this.enablecase = $(element).data('enablecase').split(",");
        },

        refresh: function () {
            var data = this.bindings['customEnabled'].get();
            if (data) {
                if ($.inArray(data, this.enablecase) === -1) {
                    $(this.element).find('input, textarea, select').attr('readonly', true).addClass('k-state-disabled');
                    $(this.element).attr('readonly', true).addClass('k-state-disabled');

                } else {
                    $(this.element).find('input, textarea, select').attr('readonly', false).removeClass('k-state-disabled');
                    $(this.element).attr('readonly', false).removeClass('k-state-disabled');


                }
            }
        }
    });


    kendo.data.binders.widget.customEnabled = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            kendo.data.Binder.fn.init.call(this, element, bindings, options);
            this.enablecase = $(element.wrapper).find("[data-enablecase]").data("enablecase").split(",");
        },

        refresh: function () {
            var data = this.bindings['customEnabled'].get();
            if (data) {
                if ($.inArray(data, this.enablecase) === -1) {
                    $(this.element).find('input, textarea, select').attr('readonly', true).addClass('k-state-disabled');
                    $(this.element).attr('readonly', true).addClass('k-state-disabled');
                    var nb = $(this.element.wrapper).find('input[data-role="numerictextbox"]').data("kendoNumericTextBox");
                    if (nb !== undefined) {
                        nb.enable(false);
                    }

                } else {
                    $(this.element).find('input, textarea, select').attr('readonly', false).removeClass('k-state-disabled');
                    $(this.element).attr('readonly', false).removeClass('k-state-disabled');
                    var nb = $(this.element.wrapper).find('input[data-role="numerictextbox"]').data("kendoNumericTextBox");
                    if (nb !== undefined) {
                        nb.enable(true);
                    }
                }
            }
        }
    });
});
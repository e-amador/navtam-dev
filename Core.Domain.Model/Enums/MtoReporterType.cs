﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum MtoReporterType
    {
        TaskEngine = 0,
        WebWiz = 1
    }
}

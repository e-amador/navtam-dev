"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AerordsComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var windowRef_service_1 = require("../../common/windowRef.service");
var angular_l10n_1 = require("angular-l10n");
var toastr_service_1 = require("./../../common/toastr.service");
var data_service_1 = require("../shared/data.service");
var AerordsComponent = /** @class */ (function () {
    function AerordsComponent(toastr, dataService, router, winRef, activatedRoute, locale, translation) {
        this.toastr = toastr;
        this.dataService = dataService;
        this.router = router;
        this.winRef = winRef;
        this.activatedRoute = activatedRoute;
        this.locale = locale;
        this.translation = translation;
        this.status = {
            airacActiveDate: "?",
            airacEffectiveDate: "?",
            lastActiveDate: "?",
            lastActiveUpdated: "?",
            lastEffectiveDate: "?",
            lastEffectiveUpdated: "?"
        };
        if (this.winRef.appConfig.ccs || this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);
    }
    AerordsComponent.prototype.ngOnInit = function () {
        this.refreshCacheStatus();
    };
    Object.defineProperty(AerordsComponent.prototype, "langCulture", {
        get: function () {
            return (window["app"].cultureInfo || "en").toUpperCase();
        },
        enumerable: false,
        configurable: true
    });
    AerordsComponent.prototype.refreshCacheStatus = function (event) {
        var _this = this;
        if (event === void 0) { event = null; }
        if (event)
            event.preventDefault();
        this.dataService.getAeroRDSCacheStatus()
            .subscribe(function (data) {
            _this.status = data;
        }, function (error) {
            var msg = _this.translation.translate('AerordsSession.FailureGlobal');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    Object.defineProperty(AerordsComponent.prototype, "activeDateClass", {
        get: function () {
            return this.status && this.status.airacActiveDate !== this.status.lastActiveDate ? "bg-sdocache-outdated" : "bg-sdocache-latest";
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(AerordsComponent.prototype, "effectiveDateClass", {
        get: function () {
            return this.status && this.status.airacEffectiveDate === this.status.lastEffectiveDate ? "bg-sdocache-latest" : "";
        },
        enumerable: false,
        configurable: true
    });
    AerordsComponent.prototype.globalSync = function () {
        $('#modal-global').modal({});
    };
    AerordsComponent.prototype.effectiveCacheSync = function () {
        $('#modal-effective-sync').modal({});
    };
    AerordsComponent.prototype.effectiveCacheLoad = function () {
        $('#modal-effective-load').modal({});
    };
    AerordsComponent.prototype.runGlobalSync = function () {
        var _this = this;
        this.dataService.pushAeroRDSGlobalSync()
            .subscribe(function (data) {
            var msg = _this.translation.translate('AerordsSession.SuccessGlobal');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
        }, function (error) {
            var msg = _this.translation.translate('AerordsSession.FailureGlobal');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    AerordsComponent.prototype.runEffectiveCacheSync = function () {
        var _this = this;
        this.dataService.pushAeroRDSEffectiveCacheSync()
            .subscribe(function (data) {
            var msg = _this.translation.translate('AerordsSession.SuccessPartial');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
        }, function (error) {
            var msg = _this.translation.translate('AerordsSession.FailurePartial');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    AerordsComponent.prototype.runEffectiveCacheLoad = function () {
        var _this = this;
        this.dataService.pushAeroRDSEffectiveCacheLoad()
            .subscribe(function (data) {
            var msg = _this.translation.translate('AerordsSession.SuccessPartial');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
        }, function (error) {
            var msg = _this.translation.translate('AerordsSession.FailurePartial');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    AerordsComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/aerords/aerords.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, data_service_1.DataService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], AerordsComponent);
    return AerordsComponent;
}());
exports.AerordsComponent = AerordsComponent;
//# sourceMappingURL=aerords.component.js.map
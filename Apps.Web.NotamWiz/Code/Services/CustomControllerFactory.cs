﻿namespace NavCanada.Applications.NotamWiz.Web.Code.Services
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Reflection;
    using System.Text.RegularExpressions;
    using System.Web.Http;
    using System.Web.Mvc;

    public class CustomControllerFactory : IControllerFactory 
    {
        private const string VersionKey = "version";
        private const string ControllerKey = "controller";
        private readonly Lazy<Dictionary<string, Type>> _controllers;
        private readonly HashSet<string> _duplicates;
        IControllerFactory baseFactory;

        private Dictionary<string, Type> InitializeControllerDictionary()
        {
            var dictionary = new Dictionary<string, Type>(StringComparer.OrdinalIgnoreCase);
            var controllerTypes = Assembly.GetExecutingAssembly().GetTypes().Where(type => typeof(Controller).IsAssignableFrom(type) &&  Regex.Match(type.FullName, @"NavCanada\.SE\.NotamWiz\.UI\.Wiz\.Web\.NSDs\.[A-Za-z]+\.V[0-9]+R[0-9]+").Success);

            foreach (Type t in controllerTypes)
            {
                var segments = t.Namespace.Split(Type.Delimiter);

                // For the dictionary key, strip "Controller" from the end of the type name.
                // This matches the behavior of DefaultHttpControllerSelector.
                var controllerName = t.Name.Remove(t.Name.Length - 10);

                var key = String.Format(CultureInfo.InvariantCulture, "{0}.{1}", segments[segments.Length - 1], controllerName);

                // Check for duplicate keys.
                if (dictionary.Keys.Contains(key))
                {
                    this._duplicates.Add(key);
                }
                else
                {
                    dictionary[key] = t;
                }
            }

            // Remove any duplicates from the dictionary, because these create ambiguous matches. 
            // For example, "Foo.V1.ProductsController" and "Bar.V1.ProductsController" both map to "v1.products".
            foreach (string s in this._duplicates)
            {
                dictionary.Remove(s);
            }
            return dictionary;
        }

        // Get a value from the route data, if present.
        private static T GetRouteVariable<T>(System.Web.Routing.RouteData routeData, string name)
        {
            object result = null;
            if (routeData.Values.TryGetValue(name, out result))
            {
                return (T)result;
            }
            return default(T);
        }

        public Type SelectController(System.Web.Routing.RequestContext request)
        {

            if (request.RouteData == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            // Get the namespace and controller variables from the route data.
            string version = GetRouteVariable<string>(request.RouteData, VersionKey);
            if (version == null)
            {
                return null;
            }

            string controllerName = GetRouteVariable<string>(request.RouteData, ControllerKey);
            if (controllerName == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            // Find a matching controller.
            string key = String.Format(CultureInfo.InvariantCulture, "{0}.{1}", version, controllerName);

            Type controllerType;
            if (this._controllers.Value.TryGetValue(key, out controllerType))
            {
                return controllerType;
            }
            else if (this._duplicates.Contains(key))
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError));
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }


        public CustomControllerFactory(IControllerFactory baseF){
            this.baseFactory = baseF;
            this._duplicates = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            this._controllers = new Lazy<Dictionary<string, Type>>(InitializeControllerDictionary);
        }

        public IController CreateController(System.Web.Routing.RequestContext requestContext, string controllerName)
        {

            Type controllerType = SelectController(requestContext);

            if(controllerType == null)
            {
                return this.baseFactory.CreateController(requestContext, controllerName);
            }


            return Activator.CreateInstance(controllerType) as IController;
        }

        public System.Web.SessionState.SessionStateBehavior GetControllerSessionBehavior(System.Web.Routing.RequestContext requestContext, string controllerName)
        {
            return this.baseFactory.GetControllerSessionBehavior(requestContext, controllerName);
        }

        public void ReleaseController(IController controller)
        {
            this.baseFactory.ReleaseController(controller);
        }
    }
}
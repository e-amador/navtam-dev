﻿using System.Collections.Generic;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class NsdCategory
    {
        public int Id { get; set; }
        public int? ParentCategoryId { get; set; }
        public NsdCategory ParentCategory { get; set; }
        public string Name { get; set; }
        public virtual ICollection<OrganizationNsdCategory> Organizations { get; set; }
        public bool Operational { get; set; }
    }
}
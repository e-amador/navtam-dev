﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    using NavCanada.Core.Domain.Model.Dtos;

    public class NsdEditorViewModel
    {
        public bool Error { get; set; }
        public string Message { get; set; }
        public bool ReadOnly { get; set; }
        public long PackageId { get; set; }
        public string PropsalType { get; set; }
        public long NOTAMPackageId { get; set; }
        public NsdInfoViewModel nsd { get; set; }
        public NotamSubjectViewModel subject {get;set;}
        public bool Bilingual { get; set; }
        public NotamProposalViewModel notam { get; set; }
        public MultiPolygonDto DOAGeoJson { get; set; }
        public dynamic Setting { get; set; }
        public bool IsFicRole{get;set;}
        
    }
}
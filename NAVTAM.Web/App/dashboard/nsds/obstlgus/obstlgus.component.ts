﻿import { Component, OnInit, OnDestroy, Inject, Injector } from '@angular/core'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms'

import { DataService } from '../../shared/data.service'
import { LocationService } from '../../shared/location.service'
import { ObstacleLgUsService, IObstacleLgUsType, IInDoaValidationResultLgUs } from './obstlgus.service'

import { LocaleService, TranslationService } from 'angular-l10n';

@Component({
    templateUrl: '/app/dashboard/nsds/obstlgus/obstlgus.component.html'
})
export class ObstLgUsComponent implements OnInit, OnDestroy {
    action: string = "";
    token: any = null;
    nsdForm: FormGroup;
    nsdFrmGroup: FormGroup;
    isReadOnly: boolean = false;
    obstacleTypes: ISelect2ObstacleType[] = [];
    leafletmap: any;

    isFormLoadingData: boolean;
    //Validations limit
    upperLimit: number = 999;
    upperLimitFT: number = 99999;
    radiusLimit: number = 999;
    maxElevation: number = 99999;
    maxHeight: number = 9999;

    constructor(private injector: Injector,
        private fb: FormBuilder,
        private locationService: LocationService,
        private obstacleService: ObstacleLgUsService,
        private dataService: DataService,
        public translation: TranslationService) {

        this.token = this.injector.get('token');
        this.isReadOnly = this.injector.get('isReadOnly');
        this.nsdForm = this.injector.get('form');
        this.action = this.injector.get('type');
    }

    ngOnInit() {
        this.isFormLoadingData = !(!this.token.subjectId);
        const app = this.obstacleService.app;
        this.leafletmap = app.leafletmap;
        //this.configureReactiveForm();
        let frmArr = <FormArray>this.nsdForm.controls['frmArr'];
        if (frmArr.length === 0) {
            this.configureReactiveForm();
        } else {
            /*
            this is required because of the grouping funtionallity. When the NsdFormGroup 
            was configured and any of items in the group has changed (new set of tokens values)
            the NsdForm Group needs to be updated with the new tokens values */
            this.updateReactiveForm(frmArr);
        }

        this.loadObstacleTypes();
        this.showDoaMapRegion(app.doaId);
    }

    ngOnDestroy() {
        //if (this.leafletmap) {
        //    this.leafletmap.dispose();
        //    this.leafletmap = null;
        //}
    }

    $ctrl(name: string): AbstractControl {
        return this.nsdFrmGroup.get(name);
    }

    objectTypeSelectOptions() {
        return {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.ObjectTypePlaceholder')
            }
        }
    }

    validateLocation() {
        const fc = this.nsdFrmGroup.get('location');
        return !fc.errors || this.nsdForm.pristine;
    }
      
    validateRequiredLocation(): boolean {
        const fc = this.$ctrl('location');
        if (this.nsdForm.pristine)
            return false;

        return fc.errors && fc.errors.required;
    } 

    isLocationFormatInvalid(): boolean {
        const fc = this.$ctrl('location');
        if (this.nsdForm.pristine)
            return false;

        return fc.errors && fc.errors.format;
    }

    isInvalidLocation(): boolean {
        const fc = this.$ctrl('location');
        if (this.nsdForm.pristine)
            return false;

        return fc.errors && fc.errors.invalidLocation;
    }

    isValidatingLocation(): boolean {
        const fc = this.$ctrl('location');
        if (this.nsdForm.pristine)
            return false;

        return fc.errors && fc.errors.validatingLocation;
    }

    invalidObstacleType(): boolean {
        const ctrl = this.$ctrl('obstacleType');
        return !this.isReadOnly && this.$ctrl('obstacleType').invalid;
    }

    validateObstacleTypeId() {
        const fc = this.nsdFrmGroup.get('obstacleType');
        return fc.value !== -1 || this.nsdForm.pristine;
    }

    validateHeight() {
        if (this.nsdForm.pristine) return true;
        const fc = this.$ctrl('height');
        if (fc.errors) return false;
        if (fc.value !== null) {
            if (+fc.value > this.maxHeight) return false;
            if (+fc.value < 0) return false;
        } else return false;
        return true;
    }

    validateElevation() {
        if (this.nsdForm.pristine) return true;
        const fc = this.$ctrl('elevation');
        if (fc.errors) return false;
        if (fc.value !== null) {
            if (+fc.value > this.maxElevation) return false;
            if (+fc.value < 0) return false;
        } else return false;
        return true;
    }

    decHeightValue() {
        const radCtrl = this.$ctrl('height');
        if (+radCtrl.value > 0) {
            radCtrl.setValue(+radCtrl.value - 1);
            radCtrl.markAsDirty();
        }
    }

    incHeightValue() {
        const radCtrl = this.$ctrl('height');
        if (+radCtrl.value < this.maxHeight) {
            radCtrl.setValue(+radCtrl.value + 1);
            radCtrl.markAsDirty();
        }
    }

    decElevationValue() {
        const radCtrl = this.$ctrl('elevation');
        if (+radCtrl.value > 0) {
            radCtrl.setValue(+radCtrl.value - 1);
            radCtrl.markAsDirty();
        }
    }

    incElevationValue() {
        const radCtrl = this.$ctrl('elevation');
        if (+radCtrl.value < this.maxElevation) {
            radCtrl.setValue(+radCtrl.value + 1);
            radCtrl.markAsDirty();
        }
    }

    public validateKeyPressNumber(evt: any) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
        }
    }

    setCheckBoxValue(name: string) {
        this.$ctrl(name).value === true
    }

    setObjectType($event) {
        const obstTypeCtrl = this.$ctrl('obstacleType');
        const newValue = $event.value;
        if (newValue !== "-1" ) {
            if (newValue !== obstTypeCtrl.value) {
                obstTypeCtrl.setValue(newValue);
                obstTypeCtrl.markAsDirty();
            }
            this.refreshMap();
        } else {
            //??
        }
    }

    private loadObstacleTypes() {
        this.obstacleService
            .getObstacleTypes()
            .subscribe((obstacleTypes: IObstacleLgUsType[]) => {
                let obstacles = obstacleTypes.map((item: IObstacleLgUsType) => {
                    return {
                        id: item.id,
                        text: item.name
                    }
                });
                // make the 'token.obstacleType' value the first on the list for "optionless convenience" or angular will throw an exception!
                this.makeFirstElement(obstacles, (obst) => obst.id === this.token.obstacleType, true);
                this.obstacleTypes = obstacles;
            });
    }

    private makeFirstElement(arr: any[], makeFirst: Function, insertUndefinedAtZero: boolean = true) {
        let len = arr.length;
        for (let i = 0; i < len; i++) {
            if (makeFirst(arr[i])) {
                this.swapArrayPositions(arr, 0, i);
                return;
            }
        }
        if (insertUndefinedAtZero) {
            arr.unshift({
                id: -1,
                text: '',
            });
        }
        //this.swapArrayPositions(arr, 0, len);
    }

    private swapArrayPositions(arr: any[], p1: number, p2: number) {
        const temp = arr[p1];
        arr[p1] = arr[p2];
        arr[p2] = temp;
    }

    private radioButtonChanged(name: string) {
        this.$ctrl(name).markAsDirty();
    }

    private hasLowerCase(str) {
        return (/[a-z]/.test(str));
    }

    private locationChanged(value) {
        let tmp: string = this.$ctrl('location').value;
        if (tmp !== null && this.hasLowerCase(tmp)) this.$ctrl('location').patchValue(tmp.toUpperCase());
        if (value !== null && this.hasLowerCase(value)) value = value.toUpperCase();

        if (this.leafletmap === null) this.leafletmap = this.obstacleService.app.leafletmap;

        const pair = this.locationService.parse(value);
        const geojson = pair ? this.leafletmap.latitudeLongitudeToGeoJson(pair.latitude, pair.longitude) : null;
        if (geojson) {
            this.obstacleService
                .validateLocation(value)
                .subscribe((result: IInDoaValidationResultLgUs) => {
                    var locCtrl = this.$ctrl('location');
                    var currentLocation = locCtrl.value;
                    if (currentLocation === value) {
                        if (result.inDoa) {
                            this.leafletmap.clearFeaturesOnLayers();
                            this.leafletmap.addMarker(geojson, this.getSelectedObstacleName());
                            this.leafletmap.setNewMarkerLocation(geojson);
                            locCtrl.setErrors(null);
                            this.triggerFormChangeEvent(value);
                        } else {
                            locCtrl.setErrors({ invalidLocation: true });
                        }
                    }
                }, (error: any) => {
                    var locCtrl = this.$ctrl('location');
                    locCtrl.setErrors({ invalidLocation: true });
                });
        }
    }

    private triggerFormChangeEvent(value) {
        this.$ctrl('refreshDummy').setValue(value);
    }

    private refreshMap() {
        window.setTimeout(() => this.locationChanged(this.$ctrl('location').value), 1000);
    }

    private getSelectedObstacleName() {
        const obstId = this.$ctrl('obstacleType').value;

        if (!obstId || !this.obstacleTypes)
            return "OBST";

        const obstacle = this.obstacleTypes.find(obst => obst.id === obstId);
        return obstacle ? obstacle.text : "OBST";
    }

    private configureReactiveForm() {
        const frmArrControls = <FormArray>this.nsdForm.controls['frmArr'];

        this.nsdFrmGroup = this.fb.group({
            obstacleType: [{ value: this.token.obstacleType || -1, disabled: this.isReadOnly }, [obstacleTypeValidator]],
            location: [{ value: this.token.location, disabled: this.isReadOnly }, [Validators.required, validateLocation(this.locationService)]],
            height: [{ value: this.token.height, disabled: this.isReadOnly }, []],
            elevation: [{ value: this.token.elevation, disabled: this.isReadOnly }, []],
            refreshDummy: ""
        });

        this.$ctrl("elevation").setValidators(elevationValidatorFn(this.maxElevation));
        this.$ctrl("height").setValidators(elevationValidatorFn(this.maxHeight));
        this.$ctrl("location").valueChanges.debounceTime(1000).subscribe((value) => this.locationChanged(value));

        frmArrControls.push(this.nsdFrmGroup);
    }

    private updateReactiveForm(formArr: FormArray) {
        this.nsdFrmGroup = <FormGroup>formArr.controls[0];
        //It is better to update only those controls that tokens values have changed.
        this.updateFormControlValue("obstacleType", this.token.obstacleType);
        this.updateFormControlValue("location", this.token.location);
        this.updateFormControlValue("height", this.token.height);
        this.updateFormControlValue("elevation", this.token.elevation);
    }

    private updateFormControlValue(controlName: string, value) {
        let control = this.nsdFrmGroup.get(controlName);
        if (control && control.value !== value) {
            control.setValue(value);
        }
    }

    private showDoaMapRegion(doaId: number) {
        this.dataService
            .getDoaLocation(doaId)
            .subscribe((geoJsonStr: any) => this.renderMap(geoJsonStr));
    }

    private renderMap(geoJsonStr: string) {
        const self = this;
        const interval = window.setInterval(function () {
            if (self.leafletmap && self.leafletmap.isReady()) {
                window.clearInterval(interval);
                self.leafletmap.addDoa(geoJsonStr, 'DOA');
            }
        }, 100);
    }
}

interface ISelect2ObstacleType {
    id: string,
    text: string
}

function obstacleTypeValidator(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value === -1) {
        return { 'required': true }
    }
    return null;
}

function validateLocation(locationService: LocationService): Function {
    return (ctrl: AbstractControl): { [key: string]: boolean } | null => {
        const location = ctrl.value;
        if (!location)
            return { required: true };

        if (locationService.parse(location) === null)
            return { format: true };

        return null;
    }
}

function elevationValidatorFn(maxElevation: number): ValidatorFn {
    return (c: AbstractControl): { [key: string]: any } => {
        if (typeof (c.value) === "string") {
            if (!c.value) {
                return { 'required': true }
            }
            if (c.value === "") {
                return { 'required': true }
            }
        }
        if (isANumber(c.value)) {
            if (+c.value < 0 || +c.value > maxElevation) {
                return { 'invalid': true }
            }
            var v: number = +c.value;
            if (v > maxElevation) {
                return { 'invalid': true }
            }
            return null;
        }
        return { 'invalid': true }
    };
}

function heightValidatorFn(maxHeight: number): ValidatorFn {
    return (c: AbstractControl): { [key: string]: any } => {
        if (typeof (c.value) === "string") {
            if (!c.value) {
                return { 'required': true }
            }
            if (c.value === "") {
                return { 'required': true }
            }
        }
        if (isANumber(c.value)) {
            if (+c.value < 0 || +c.value > maxHeight) {
                return { 'invalid': true }
            }
            var v: number = +c.value;
            if (v > maxHeight) {
                return { 'invalid': true }
            }
            return null;
        }
        return { 'invalid': true }
    };
}

function isANumber(str: string): boolean {
    return !/\D/.test(str);
}

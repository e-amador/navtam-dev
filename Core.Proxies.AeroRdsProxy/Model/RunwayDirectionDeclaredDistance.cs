﻿using System;
using System.Collections.Generic;

namespace NavCanada.Core.Proxies.AeroRdsProxy.Model
{
    public class RunwayDirectionDeclaredDistance
    {
        public string RunwayDirectionDeclaredDistanceId { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string Status { get; set; }
        public int SEQ { get; set; }
        public string FK_RunwayDirection { get; set; }
        public string FK_Taxiway { get; set; }
        public string TimePeriod { get; set; }
        public string Type { get; set; }
        public double DistanceValue { get; set; }
        public string UOMDistance { get; set; }
        public string Remark { get; set; }
    }

    public class RunwayDirectionDeclaredDistances
    {
        public IList<RunwayDirectionDeclaredDistance> Value { get; set; }
    }
}

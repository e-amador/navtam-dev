."$PSScriptRoot\utils.ps1"

$buildEnviroment = $args[0]; #BUILD_CONFIG (TEST, DEBUG, PROD, DEMO) 
$projectName = $args[1] + "_$buildEnviroment"; #PROJECT_NAME

$servers = $args[2]; #REMOTE_SERVER_IPS
$username = $args[3]; #USERNAME
$password = $args[4]; #PASSWORD
$remoteWebAppDir = $args[5]; #REMOTE_ROOT_DIR FOR THE WEB APP
$serviceHostList = $args[6]; #TASKENGINE_SERVICE_IP SERVERS (SEPARATED BY COLOMS)
$buildNumber = $args[7]; #JENKING BUILD_NUMBER
$createDeploymentPackage = $args[8]; #FLAG FOR CREATING DEPLOYMENT PACKAGE
$webSiteName = $args[9]; #WEB_SITE_NAME (Name of the WebSite on IIS)

$solutionDir = "C:\workspace\" + $projectName

function InstallWebApplication($credentials, $localDeploymentDir) {
	try{
			Write-Host " ";
			Write-Host "-----------------------------------------------------";
			Write-Host "-----------------------------------------------------";
			Write-Host "Start WebApplication installation to $servers";
			Write-Host "-----------------------------------------------------";

			$packageDirectory = "$solutionDir\WEB_DEPLOYMENT"
			$remoteServers = $servers.split(",");
			foreach($webAppSrver in $remoteServers){ 
				#Start remote server session
				$s = New-PSSession -ErrorAction Stop -ComputerName $webAppSrver -Credential $credentials
				
				#Stop Internet Information Server Before Start Deployment on 
				Write-Host "Stop IIS on $webAppSrver"
				StopIIS $s

				#Start Deployment to remote server
				DeployWebApp $webAppSrver $webSiteName $packageDirectory $username $password

				#Remove remote server session
				Remove-PSSession $s
			}

			#Run Migrations
			RunMigrations $localDeploymentDir

			#Start remote server session
			#$session = New-PSSession -ErrorAction Stop -ComputerName $remoteServers[0] -Credential $credentials

			#execute deploy command and deploy to remote server
			#CopyFilesToRemoteServer $session "$localDeploymentDir\*" $remoteWebAppDir 1

			#Remove remote server session
			#Remove-PSSession $session

			foreach($webAppSrver in $remoteServers){ 
				#Start remote server session
				$s = New-PSSession -ErrorAction Stop -ComputerName $webAppSrver -Credential $credentials
				
				#Start Internet Information Server After Deployment
				Write-Host "Start IIS on $webAppSrver"
				StartIIS $s

				#Remove remote server session
				Remove-PSSession $s
			}

			Write-Host "WebApplication fully installed on $server";
	}
	catch{
		Write-Host "There was an error installing WebApplication to $server";
		Write-Host $_.Exception.Message;
		throw 1
	}
}

function InstallTaskEnginService($credentials) {
	try {
		#Repeat the installation on each remote server
		$remoteServers = $serviceHostList.split(",");

		foreach($remoteServer in $remoteServers){ 
			Write-Host " ";
			Write-Host "-----------------------------------------------------";
			Write-Host "-----------------------------------------------------";
			Write-Host "Start installation of TaskEngine service on $remoteServer";
			Write-Host "-----------------------------------------------------";

			#Start remote server session
			$session = New-PSSession -ErrorAction Stop -ComputerName $remoteServer -Credential $credentials

			$remoteDir = "C:\NAVTAM_BIN"
			$serviceName = "SERVICES.WIN.TASKENGINE"
			$serviceDirPath = "$solutionDir\$serviceName\BIN\x64\$buildEnviroment"

			#Stop TaskEngine
			StopWindowsService $session $serviceName

			#Copy TaskEngine binaries to remote server
			CopyFilesToRemoteServer $session "$serviceDirPath\*" $remoteDir 1
            #DeployWinService $remoteWebServer $remoteDir $taskEngineDirectory $username $password

			#Install TaskEngine
			$serviceExePath = "$remoteDir\Services.Win.TaskEngine.exe"
			InstallWindowsService64 $session $serviceExePath

			#Start TaskEngine
			StartWindowsService $session $serviceName

			#Remove remote server session
			Remove-PSSession $session

			Write-Host "TaskEngine service fully installed on $remoteServer";
		}
	}
	catch {
		Write-Host "There was an error deploying TaskEngine to $remoteServer";
		Write-Host $_.Exception.Message;
		throw 1
	}
}

function CreateDeploymentPackage() {
	try {
		$winSvcDir = "$solutionDir\Services.Win.TaskEngine\bin\x64\$buildEnviroment"
		$packageName = $projectName + "_" + $buildNumber
		$packageTempDir = "$solutionDir\$packageName"

		#Create Temporary Folder for the Package
		New-Item -Path $packageTempDir -ItemType directory
		
		#Zip WebApp Deployment to Temp Forder
		ZipFolderToFile "$solutionDir\WEB_DEPLOYMENT" "$packageTempDir\WebApp.zip"

		#Zip TaskEngine Deployment Service to Temp Forder
		ZipFolderToFile $winSvcDir "$packageTempDir\TaskEngine.zip"

		Copy-Item -Path "$solutionDir\Jenkins.Scripts\utils.ps1" -Destination $packageTempDir
		Copy-Item -Path "$solutionDir\Jenkins.Scripts\certification.ps1" -Destination $packageTempDir
		Copy-Item -Path "$solutionDir\Jenkins.Scripts\installation-notes.txt" -Destination $packageTempDir

		#Zip Package Directory one more time into a Zip formated file named "PROJECTNAME_BUILD_BUILD#"
		ZipFolderToFile "$packageTempDir" "$solutionDir\$packageName.zip"

		#Copy Zipped Package To the predefined package location "C:\inetpub\wwwroot\Build\Navtam_BUILD" 
		Copy-Item -Path "$solutionDir\$packageName.zip" -Destination "C:\inetpub\wwwroot\Build\$projectName" 
	}
	catch {
		Write-Host "There was an error creating the deployment package";
		Write-Host $_.Exception.Message;
		throw 1
	}
}


try {
	$ErrorActionPreference = "Stop"

	Write-Host "$buildEnviroment, $projectName, $server, $username, $password, $remoteDeploymentDir, $serviceHostList, $buildNumber, $solutionDir"

	#create remote server credentials
	$credentials = CreateCredentials $username $password
	$localDeploymentDir = "$solutionDir\WEB_DEPLOYMENT"

	#Download all packages locally before deploy remotelly
	InstallNpmPackages $localDeploymentDir

	#Migration will run in the local local deployment folder (Copy EF Migration Tool)
	Copy-Item -Path "$solutionDir\Packages\EntityFramework.6.1.3\tools\migrate.exe" -Destination "$localDeploymentDir\bin"

	if( $buildEnviroment -eq "DEV" ) {
		#Install WebApplication on remote server
		InstallWebApplication $credentials $localDeploymentDir

		#Install TaskEngine to all remote servers
		InstallTaskEnginService $credentials
	}
	
	if( $buildEnviroment -eq "DEMO" ) {
		#Install WebApplication on remote server
		InstallWebApplication $credentials $localDeploymentDir
	}	

	#Create Deployment Package and Zip it to Shared Location if required
	If ($createDeploymentPackage -eq $true) { 
		CreateDeploymentPackage 
	}
	exit 0
}
catch {
	exit 1
}



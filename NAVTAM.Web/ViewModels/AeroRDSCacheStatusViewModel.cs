﻿namespace NAVTAM.ViewModels
{
    public class AeroRDSCacheStatusViewModel
    {
        public int Id { get; set; }
        public string AiracActiveDate { get; set; }
        public string AiracEffectiveDate { get; set; }
        public string LastActiveDate { get; set; }
        public string LastEffectiveDate { get; set; }
        public string LastActiveUpdated { get; set; }
        public string LastEffectiveUpdated { get; set; }
    }
}
﻿using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.SqlServiceBroker;
using NavCanada.Core.TaskEngine.Contracts;
using NavCanada.Core.TaskEngine.Helpers;
using NavCanada.Core.TaskEngine.Mto;
using System;
using System.Threading;

namespace NavCanada.Core.TaskEngine.Tasks
{
    public class QueueNextDisseminationJobTask : IMessageTask
    {
        public void Execute(IMessageTaskContext context)
        {
            var loggerType = GetType();
            var logger = context.ExternalServices.Logger;
            try
            {
                logger.LogDebug(loggerType, "QueueNextDisseminationJobTask task started...");

                var keyObj = context.Mto[MessageDefs.EntityIdentifierKey];
                if (keyObj == null)
                {
                    logger.LogWarn(loggerType, "Mto message missing entity identifier...");
                    return;
                }

                var jobIdRaw = keyObj.Value.ToString();

                int jobId;
                if (!int.TryParse(jobIdRaw, out jobId))
                {
                    logger.LogWarn(loggerType, $"Failed to convert Job Id '{jobIdRaw}'", new { jobId = jobIdRaw });
                    return;
                }

                var uow = context.ClusterStorage.Uow;
                var job = uow.DisseminationJobRepo.GetById(jobId);

                if (job == null)
                {
                    logger.LogWarn(loggerType, $"Task completed but Job {jobId} was NOT FOUND in DB.", new { jobId });
                    return;
                }

                QueueNextDisseminationJob(context.ClusterStorage.Uow, jobId);

                // sleep the configured delay
                Thread.Sleep(job.Delay);

                logger.LogDebug(loggerType, $"QueueNextDisseminationJobTask finished dissemination of notam# {job.LastNotam}.");
            }
            catch (Exception ex)
            {
                logger.LogError(ex, loggerType, "ProcessDisseminateJobTask has FAILED!");
                throw;
            }
        }

        static void QueueNextDisseminationJob(IUow uow, int jobId)
        {
            var mto = MtoHelper.CreateMtoMessage(MessageTransferObjectId.ProcessDisseminateJobMsg, m => m.Add(MessageDefs.EntityIdentifierKey, jobId, DataType.Int32));
            SubmitToServiceBrokerQueue(uow, mto);
        }

        static void SubmitToServiceBrokerQueue(IUow uow, MessageTransferObject mto)
        {
            var taskEngineQueue = new SqlServiceBrokerQueue(uow.DbContext, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings);
            taskEngineQueue.Publish(mto);
        }
    }
}

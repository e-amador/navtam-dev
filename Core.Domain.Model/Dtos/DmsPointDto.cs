﻿namespace NavCanada.Core.Domain.Model.Dtos
{
    using System;

    using Enums;

    public class DmsPointDto
    {
        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public DmsPointDto()
        { }

        public DmsPointDto(string lat, string lon)
        {

            Latitude = GetDecimal(lat, true);
            Longitude = GetDecimal(lon, false);

        }

        double GetDecimal(string val, bool isLAt)
        {
            var dms = new DmsDto();
            string decimals = "";
            string direction = val.Substring(val.Length - 1);
            PointType type = (direction == "N" || direction == "S") ? PointType.Lat : PointType.Lon;

            //value without direction
            val = val.Substring(0, val.Length - 1);
            var dmsStr = val;

            if (dmsStr.Contains("."))
            {
                decimals = "." + dmsStr.Split('.')[1];
                dmsStr = dmsStr.Split('.')[0];
            }

            try
            {
                if (type == PointType.Lon)
                {
                    var odd = dmsStr.Length == 7;
                    var degLen = odd ? 3 : 2;
                    var minPos = odd ? 3 : 2;
                    var secPos = odd ? 5 : 4;

                    dms.Degrees = int.Parse(dmsStr.Substring(0, degLen));

                    // minutes
                    if (dmsStr.Length >= degLen + 2)
                        dms.Minutes = int.Parse(dmsStr.Substring(minPos, 2));
                    else
                        dms.Minutes = 0;

                    //Seconds
                    if (dmsStr.Length >= degLen + 4)
                        dms.Seconds = double.Parse(dmsStr.Substring(secPos) + decimals);
                    else
                        dms.Seconds = 0;
                }
                else
                {
                    dms.Degrees = int.Parse(dmsStr.Substring(0, 2));

                    //Minutes
                    if (dmsStr.Length > 2)
                        dms.Minutes = int.Parse(dmsStr.Substring(2, Math.Min(2, dmsStr.Length - 2)));
                    else
                        dms.Minutes = 2;

                    //Seconds
                    if (dmsStr.Length > 4)
                        dms.Seconds = double.Parse(dmsStr.Substring(4) + decimals);
                    else
                        dms.Seconds = 0;

                }
            }
            catch (Exception)
            {
                throw;
                //  log.Error("Exception occurred while converting geography " + dmsStr + direction + "\n" + e);
            }

            dms.Degrees *= (direction == "S" || direction == "W" ? -1 : 1);

            if (isLAt)
                LatitudeDMS = dms;
            else
                LongitudeDMS = dms;

            return (Math.Abs(dms.Degrees) + dms.Minutes / 60d + dms.Seconds / 3600d) * (direction == "S" || direction == "W" ? -1 : 1);
        }

        private DmsDto LatitudeDMS { get; set; }

        private DmsDto LongitudeDMS { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3} {4} {5} {6} {7}",
                Math.Abs(LatitudeDMS.Degrees),
                LatitudeDMS.Minutes,
                LatitudeDMS.Seconds,
                LatitudeDMS.Degrees < 0 ? "S" : "N",
                Math.Abs(LongitudeDMS.Degrees),
                LongitudeDMS.Minutes,
                LongitudeDMS.Seconds,
                LongitudeDMS.Degrees < 0 ? "W" : "E");
        }
    }
}

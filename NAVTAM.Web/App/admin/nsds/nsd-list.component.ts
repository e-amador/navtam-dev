﻿import { Component, OnInit, Inject } from '@angular/core'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';
import { ActivatedRoute, Router } from '@angular/router'
import { Select2OptionData } from 'ng2-select2';
import { DataService } from '../shared/data.service'
import { MemoryStorageService } from '../shared/mem-storage.service'
import { WindowRef } from '../../common/windowRef.service'
import { LocaleService, TranslationService } from 'angular-l10n';
import {
    INsdManagementDto,
    IQueryOptions,
    IPaging,
    INsdNodeTree,
    ITableHeaders,
    IOrganization,
    INsdCategories
} from '../shared/data.model'
import { forEach } from '@angular/router/src/utils/collection';
import { ISelectOptions, INsdCategory, NsdCategory } from '../../dashboard/shared/data.model';
import { PACKAGE_ROOT_URL } from '@angular/core/src/application_tokens';

declare var $: any;

@Component({
    templateUrl: '/app/admin/nsds/nsd-list.component.html'
})

export class NsdListComponent implements OnInit {
    nsds: INsdManagementDto[];
    orgs: IOrganization[];

    queryOptions: IQueryOptions;
    selectedNsd: INsdManagementDto = null;
    previousSelectedNsd: INsdManagementDto = null;
    selectedOrganization: IOrganization = null;

    loadingData: boolean = false;
    tableHeaders: ITableHeaders;
    canbeDeleted: boolean = false;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private route: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService) {

        if (this.winRef.appConfig.ccs || this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);

        this.tableHeaders = {
            sorting:
                [
                    { columnName: 'Name', direction: 0 },
                    { columnName: 'Category', direction: 0 },
                    { columnName: 'FreeText', direction: 0 },
                    { columnName: 'Operational', direction: 0 }
                ],
            itemsPerPage:
                [
                    { id: '10', text: '10' },
                    { id: '20', text: '20' },
                    { id: '50', text: '50' },
                    { id: '100', text: '100' },
                    { id: '200', text: '200' },
                ]
        };
    };

    ngOnInit() {
        this.getNsds();
        this.loadingData = false;
    }

    get langCulture() {
        return (window["app"].cultureInfo || "en").toUpperCase();
    }

    activateNsd(nsd: INsdManagementDto) {
        if (nsd) {
            this.enableNsd(nsd);
        }
    }

    adjustNsdStatusModal() {
        if (this.selectedNsd) {
            $('#modal-adjust-operational-status').modal({});
        }
    }

    adjustNsdStatus() {
        if (this.selectedNsd) {
            this.selectedNsd.operational ? this.disableNsd(this.selectedNsd) : this.activateNsd(this.selectedNsd);
        }

        this.onComplete(this.selectedNsd, this.selectedOrganization);
    }

    isLoadingData() {
        return this.loadingData;
    }

    adjustOrganizationNsd() {
        if (this.selectedNsd && this.selectedOrganization) {
            $('#modal-remove-organizations-status').modal({});
        }
    }

    disableNsd(nsd: INsdManagementDto) {
        if (nsd) {
            this.deactivateNsd(nsd);
        }
    }

    isOperational(): boolean {
        if (this.selectedNsd) {
            return this.selectedNsd.operational;
        }

        return false;
    }

    isOperationalListStatus(nsd): boolean {
        return nsd.operational;
    }

    removeallOrganizations() {
        $('#modal-revoke-organization-status').modal({});
        $('#modal-remove-allorganizations').modal({});
    }

    revokeOrganizationAccess() {
        if (this.selectedNsd && this.selectedOrganization) {
            this.selectedNsd.orgId = this.selectedOrganization.id;
            this.dataService.removeNsdFromOrganization(this.selectedNsd).subscribe((x => { this.getOrganizations(+this.selectedNsd.id) }));
            this.selectedOrganization = null;
        }
    }

    isNsdSelected(): boolean {
        if (this.selectedNsd) return true;

        return false;
    }

    isOrganizationSelected(): boolean {
        if (this.selectedOrganization) return true;

        return false;
    }

    onNsdSelect() {
        this.getOrganizations(+this.selectedNsd.id);
    }

    onComplete(nsd, org) {
        if (nsd) {
            this.selectedNsd = nsd;
        }
        if (org) {
            this.selectedOrganization = org;
        }
    }

    selectNsd(nsd) {
        if (this.selectedNsd) {
            this.selectedNsd.isSelected = false;
        }

        this.selectedOrganization = null;
        this.selectedNsd = nsd;
        this.selectedNsd.isSelected = true;
        this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, this.selectedNsd.id);

        this.onNsdSelect();
    }

    selectOrg(org) {
        if (this.selectedOrganization) {
            this.selectedOrganization.isSelected = false;
            this.selectedOrganization = null;
        }

        if (org.id > -1) {
            this.selectedOrganization = org;
            this.selectedOrganization.isSelected = true;
            this.selectedNsd.orgId = this.selectedOrganization.id;
            this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, this.selectedOrganization.id);
        }
    }

    isOrgSelected(): boolean {
        if (this.selectedOrganization) return true;

        return false;
    }

    private enableNsd(selectedNsd: INsdManagementDto) {
        if (this.isNsdSelected()) {
            this.memStorageService.save("nsd", this.selectedNsd);

            this.dataService.activateNsd(selectedNsd).subscribe(() => this.getNsds());
            this.getOrganizations(+this.selectedNsd.id);

            this.selectedNsd = this.memStorageService.get("nsd");
            this.memStorageService.remove("nsd");
        }

        return;
    }

    private deactivateNsd(selectedNsd: INsdManagementDto) {
        if (this.isNsdSelected()) {
            this.memStorageService.save("nsd", this.selectedNsd);

            this.dataService.disableNsd(selectedNsd).subscribe(() => this.getNsds());
            this.getOrganizations(+this.selectedNsd.id);

            this.selectedNsd = this.memStorageService.get("nsd");
            this.memStorageService.remove("nsd");

        }

        return;
    }

    private getNsds() {
        try {
            this.loadingData = true;
            this.dataService.getNsds().subscribe((nsds: any) => { this.processNsd(nsds) });
            this.loadingData = false;
        } catch (e) {} //TODO: Log client side actions?
    }

    private getOrganizations(id: number): any {
        this.dataService.getOrganizationsWithNsdId(id).subscribe((orgs: any) => { this.processOrgs(orgs) });
    }

    private organizationIsSelected(): boolean {
        if (this.selectedOrganization) {
            return this.selectedOrganization.isSelected;
        }

        return false;
    }

    private removeFromAllOrganizations(selectedNsd: INsdManagementDto) {
        if (this.isNsdSelected()) {
            this.dataService.removeFromAllOrganizations(selectedNsd).subscribe(() => this.getNsds());
            this.getOrganizations(+this.selectedNsd.id);
        }

        this.onComplete(this.selectedNsd, this.selectedOrganization);

        return;
    }

    private processOrgs(orgs: IOrganization[]) {
        let orgArray = [];
        for (let org of orgs) {
            if (!org.isReadOnlyOrg) {
                let organization = { id: org.id, text: org.name };
                orgArray.push(organization);
            }
        }

        if (orgArray.length == 0) {
            let noOrgsExist = {
                id: -1,
                text: "No Organizations"
            }

            orgArray.push(noOrgsExist);
        }

        this.orgs = orgArray;
    }

    private removeNsdFromOrganization() {
        if (this.selectedNsd && this.selectedOrganization.isSelected) {
            this.dataService.removeNsdFromOrganization(this.selectedNsd).subscribe(() => this.getOrganizations(+this.selectedNsd.id));

            this.onComplete(this.selectedNsd, this.selectedOrganization);
        }
    }

    private processNsd(nsdArray: INsdManagementDto[]) {
        this.nsds = [];
        this.nsds = nsdArray;

        let index = 0;
        if (this.previousSelectedNsd) {
            index = this.nsds.findIndex(x => x.id === this.selectedNsd.id);
            if (index < 0)
                index = 0;
        }

        this.nsds[index].isSelected = true;
        this.selectedNsd = this.nsds[index];
        this.onNsdSelect();
    }
}
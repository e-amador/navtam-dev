﻿using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.NSDs.TwyClsd.V0R1
{
    using System.Collections.Generic;
    using System.Linq;

    using FluentValidation;

    using NavCanada.Applications.NotamWiz.Web.Code.Extentions;
    using NavCanada.Applications.NotamWiz.Web.NSDs;

    public class TwyClsdValidator : AbstractValidator<ViewModel>
    {
        private readonly ILog log = LogManager.GetLogger(typeof(TwyClsdValidator));

        public TwyClsdValidator(NsdBaseController controller)
        {
            this.log.Object(new
            {
                Method = "TwyClsdValidator",
                Parameters = new
                {
                    controller
                }
            });
            Controller = controller;
            RuleFor(viewModel => viewModel.ClosedTaxiways).Must(closedTaxiways => closedTaxiways.Count > 0);
            RuleFor(viewModel => viewModel.ClosedTaxiways).Must(Points);
            RuleFor(viewModel => viewModel.IsOtherClosureReason).Must(ClosureReasonText);
        }

        private NsdBaseController Controller
        {
            get;
            set;
        }

        private static bool ClosureReasonText(ViewModel viewModel, bool isOtherClosureReason)
        {
            if (isOtherClosureReason)
            {
                if (viewModel.Bilingual)
                {
                    return !(string.IsNullOrEmpty(viewModel.ClosureReasonText) && string.IsNullOrEmpty(viewModel.ClosureReasonTextFrench));
                }
                return !string.IsNullOrEmpty(viewModel.ClosureReasonText);
            }
            return true;
        }

        private static bool Points(ViewModel viewModel, List<ClosedTaxiway> closedTaxiways)
        {
            foreach (var closedTaxiway in closedTaxiways.Where(closedTaxiway => closedTaxiway.IsPartialClosure))
            {
                if (viewModel.Bilingual)
                {
                    return !(string.IsNullOrEmpty(closedTaxiway.Point1) && string.IsNullOrEmpty(closedTaxiway.Point1French) && string.IsNullOrEmpty(closedTaxiway.Point2) && string.IsNullOrEmpty(closedTaxiway.Point2French));
                }
                return !(string.IsNullOrEmpty(closedTaxiway.Point1) && string.IsNullOrEmpty(closedTaxiway.Point1French));
            }
            return true;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NAVTAM.ViewModels
{
    public class ConfigurationValueViewModel
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Value { get; set; }

        public string Category { get; set; }

        public string Action { get; set; }
    }
}
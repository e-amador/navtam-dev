﻿
define(["jquery", "Resources", "kendo/kendo.grid.min"], function ($, res) {

    var labels = {};

    return {
        init: function (lbs) {
            labels = lbs;
            this.initUI();
        },

        initUI: function () {
            $("#subjectGrid").kendoGrid({
                pageable: true,
                scrollable: true,
                filterable: true,
                sortable: true,
                columns: [
                    { field: "EntityCode", title: labels.subjectLabel, width: ~120 },
                    { field: "Name", title: labels.nameLabel },
                    { field: "NameFrench", title: labels.nameFrenchLabel },
                    { field: "Code", title: labels.codeLabel, width: 90 },
                    { field: "Scope", title: labels.scopeLabel, width: 90, values: ["A", "E", "W", "AE"] },
                    {
                        field: "AllowItemAChange", title: labels.itemAUpdate, width: 140,
                        template: "<input type='checkbox' #: AllowItemAChange ? 'checked=true' : '' # disabled='disabled' ></input>",
                    },
                    {
                        field: "AllowScopeChange", title: labels.scopeUpdate, width: 140,
                        template: "<input type='checkbox' #: AllowScopeChange ? 'checked=true' : '' # disabled='disabled' ></input>",

                    },
                    {
                        field: "AllowSeriesChange", title: labels.seriesUpdate, width: 140,
                        template: "<input type='checkbox' #: AllowSeriesChange ? 'checked=true' : '' # disabled='disabled' ></input>",

                    },
                    {
                        command: [
                          { name: "edit", text: { edit: labels.editLabel, cancel: labels.cancelLabel, update: labels.updateLabel } },
                          { name: "destroy", text: labels.deleteLabel }
                        ]
                    }

                ],
                toolbar: [
                    { name: "create", text: labels.addLabel }
                ],
                editable: {
                    mode: "inline"
                },
                detailTemplate: kendo.template($("#subjectConditiontemplate").html()),
                detailInit: this.detailInit,
                dataSource: {
                    schema: {
                        model: {
                            id: "Id",
                            fields: {
                                Id: { editable: false },
                                AllowItemAChange: { type: "boolean" },
                                AllowScopeChange: { type: "boolean" },
                                AllowSeriesChange: { type: "boolean" }

                            }
                        },
                        data: function (data) { return data.Data; },
                    },
                    transport: {
                        create: { url: "IcaoSubject/Subjects_Create", dataType: "json", type: "POST" },
                        read: { url: "IcaoSubject/Subjects_Read", dataType: "json", type: "POST" },
                        update: { url: "IcaoSubject/Subjects_Update", dataType: "json", type: "POST" },
                        destroy: { url: "IcaoSubject/Subject_Destroy", dataType: "json", type: "POST" }
                    }

                },
                edit: function (e) {
                    if (e.model.isNew()) {
                        debugger;
                        $(".k-grid-edit-row").on("click", ".k-hierarchy-cell", function (e) {
                            e.stopPropagation();
                        });
                    }
                }
            });
        },

        detailInit: function (e) {
            var detailRow = e.detailRow;
            detailRow.find(".conditionsGrid").kendoGrid({
                animation: {
                    open: { effects: "fadeIn" }
                },
                filterable: true,
                sortable: true,
                columns: [
                    { field: "Description", title: labels.descriptionLabel },
                    { field: "DescriptionFrench", title: labels.descriptionLabelFrench },
                    { field: "Lower", title: labels.lowerLabel, width: 90 },
                    { field: "Upper", title: labels.upperLabel, width: 90 },
                    { field: "Radius", title: labels.radiusLabel, width: 95 },
                    { field: "Code", title: labels.codeLabel, width: 90 },
                    { field: "Traffic", title: labels.traffic, width: 110, values: ["I", "V", "IV"] },
                    { field: "Purpose", title: labels.purpose, width: 110, values: ["NBO", "NBM", "NB", "NO", "NM", "BOM", "BO", "BM", "OM", "N", "B", "O", "M"] },
                    { field: "AllowFgEntry", title: labels.fandGEntry, width: 150, template: "<input type='checkbox' #: AllowFgEntry ? 'checked=true' : '' # disabled='disabled' ></input>" },
                    { field: "AllowPurposeChange", title: labels.changePurpose, width: 150, template: "<input type='checkbox' #: AllowPurposeChange ? 'checked=true' : '' # disabled='disabled' ></input>" },
                    { field: "CancelNotamOnly", title: labels.cancelOnly, width: 130, template: "<input type='checkbox' #: CancelNotamOnly ? 'checked=true' : '' # disabled='disabled' ></input>", attributes: { "class": "specialColumn" } },
                    { field: "Active", title: labels.activeLabel, width: 100, template: "<input type='checkbox' #: Active ? 'checked=checked' : '' # disabled='disabled' ></input>", attributes: { "class": "specialColumn" } },
                    {
                        command: [
                            { name: "edit", text: { edit: labels.editLabel, cancel: labels.cancelLabel, update: labels.updateLabel } },
                            { name: "destroy", text: labels.deleteLabel }
                        ]
                    }
                ],
                toolbar: [
                    { name: "create", text: labels.addLabel }
                ],
                editable: {
                    mode: "inline"
                },
                dataSource: {
                    transport: {
                        create: {
                            url: "IcaoSubject/Conditions_Create", dataType: "json", type: "POST", data: { "parentId": e.data.Id, id: 0 },
                            complete: function () { $(".conditionsGrid").data("kendoGrid").dataSource.read(); }
                        },
                        read: {
                            url: "IcaoSubject/Conditions_Read", dataType: "json", type: "POST", data: { "parentId": e.data.Id }
                        },
                        update: {
                            url: "IcaoSubject/Conditions_Update", dataType: "json", type: "POST",
                            complete: function () { $(".conditionsGrid").data("kendoGrid").dataSource.read(); }
                        },
                        destroy: { url: "IcaoSubject/Conditions_Destroy", dataType: "json", type: "POST" }
                    },
                    schema: {
                        model: {
                            id: "Id",
                            fields: {
                                Id: { editable: false },
                                AllowFgEntry: { type: "boolean" },
                                AllowPurposeChange: { type: "boolean" },
                                CancelNotamOnly: { type: "boolean" },
                                Active: { type: "boolean" },
                                Traffic: { type: "string" },
                                Purpose: { type: "string" },
                            }
                        }
                    }
                },
            });
        }

    };
});

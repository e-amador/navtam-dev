﻿import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { XsrfTokenService } from '../../../common/xsrf-token.service'
import { WindowRef } from '../../../common/windowRef.service'
import { IClearedSurfaceThird, IRunwaySubject, IRwyCCInput } from './rsc.model';

@Injectable()
export class RscService {
    constructor(private http: Http, winRef: WindowRef, private xsrfTokenService: XsrfTokenService) {
        this.app = winRef.nativeWindow['app'];
    }

    public app: any;

    getSdoRwys(sdoId: string): Observable<IRunwaySubject[]> {
        if (this.app.antiForgeryTokenValue) {
            this.xsrfTokenService.addForgeryTokenToHeader(this.app.antiForgeryTokenValue);
        }
        return this.http.get(`${this.app.apiUrl}rsc/GetRwys/?sdoId=${sdoId}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    calculateRwyCC(data: IClearedSurfaceThird): Observable<IClearedSurfaceThird> {
        if (this.app.antiForgeryTokenValue) {
            this.xsrfTokenService.addForgeryTokenToHeader(this.app.antiForgeryTokenValue);
        }
        return this.http.post(`${this.app.apiUrl}rsc/CalculateRwyCC/`,data)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);

    }

    calculateRwyCondCode(data: IRwyCCInput): Observable<string> {
        if (this.app.antiForgeryTokenValue) {
            this.xsrfTokenService.addForgeryTokenToHeader(this.app.antiForgeryTokenValue);
        }
        return this.http.post(`${this.app.apiUrl}rsc/CalculateRwyCC/`, data)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);

    }

    public deepCopy(obj) {
        var copy;

        // Handle the 3 simple types, and null or undefined
        if (null == obj || "object" != typeof obj) return obj;

        // Handle Date
        if (obj instanceof Date) {
            copy = new Date();
            copy.setTime(obj.getTime());
            return copy;
        }

        // Handle Array
        if (obj instanceof Array) {
            copy = [];
            for (var i = 0, len = obj.length; i < len; i++) {
                copy[i] = this.deepCopy(obj[i]);
            }
            return copy;
        }

        // Handle Object
        if (obj instanceof Object) {
            copy = {};
            for (var attr in obj) {
                if (obj.hasOwnProperty(attr)) copy[attr] = this.deepCopy(obj[attr]);
            }
            return copy;
        }

        throw new Error("Unable to copy obj! Its type isn't supported.");
    }

    private handleError(error: Response) {
        return Observable.throw(error.statusText);
    }

}
﻿namespace NavCanada.Core.Domain.Model.Dtos
{
    public class UserRoleDto
    {
        public string RoleId { get; set; }
        public string RoleName { get; set; }
    }
}

﻿import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ICheckComponent} from './ng2-icheck.component';
import {IRadioComponent} from './ng2-iradio.component';

export {ICheckComponent} from './ng2-icheck.component';
export {IRadioComponent} from './ng2-iradio.component';

@NgModule({
  imports: [CommonModule],
  declarations: [
      ICheckComponent,
      IRadioComponent
  ],
  providers: [

  ],
  exports: [
      ICheckComponent,
      IRadioComponent
  ]
})
export class Ng2ICheckModule {}
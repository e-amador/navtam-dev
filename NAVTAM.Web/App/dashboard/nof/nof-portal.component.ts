﻿import { Component, OnInit, OnDestroy } from '@angular/core'
import { ActivatedRoute } from '@angular/router'

import { DataService } from '../shared/data.service'
import { NofQueues } from '../shared/data.model'
import { MemoryStorageService } from '../shared/mem-storage.service'

@Component({ 
    templateUrl: '/app/dashboard/nof/nof-portal.component.html'
})
export class NofPortalComponent implements OnInit, OnDestroy  {

    currentTab: string;
    pendingCount: number = 0;
    parkCount: number = 0;
    messageCount: number = 0; 
    reviewListCount: number = 0;
    runPendingBellAnimation: boolean = false;
    runParkBellAnimation: boolean = false;
    runUnreadBellAnimation: boolean = false;
    tabsDisabled: boolean = false;
    intervalHandler = null;

    alarms = [
        { id: 0, name: 'Bell Default', ext: 'wav'},
        { id: 1, name: 'Chime Default', ext: 'wav'},
        { id: 2, name: 'Bell Sound', ext: 'mp3'},
        { id: 3, name: 'Police Warning', ext: 'mp3'},
        { id: 4, name: 'Burglary Alarm', ext: 'mp3'},
        { id: 5, name: 'Security Alarm', ext: 'mp3'},
    ];

    currentPendingQueueSound : any;
    currentMessageQueueSound: any;

    constructor(private route: ActivatedRoute,
        private dataService: DataService,
        private memStorageService: MemoryStorageService) {
    }

    ngOnInit() {
        let pendingQueueSoundId = this.memStorageService.get(this.memStorageService.PENDING_QUEUE_SOUND_KEY) || 0;
        let messageQueueSoundId = this.memStorageService.get(this.memStorageService.MESSAGE_QUEUE_SOUND_KEY) || 1;

        this.currentPendingQueueSound = this.alarms[pendingQueueSoundId];
        this.currentMessageQueueSound = this.alarms[messageQueueSoundId];

        let pendingCount = this.memStorageService.get(this.memStorageService.PENDING_QUEUE_SOUND_COUNT_KEY) || 0;
        if (pendingCount === 0) {
            this.memStorageService.save(this.memStorageService.PENDING_QUEUE_SOUND_COUNT_KEY, pendingCount);
        }
        let messageCount = this.memStorageService.get(this.memStorageService.MESSAGE_QUEUE_SOUND_COUNT_KEY) || 0;
        if (messageCount === 0) {
            this.memStorageService.save(this.memStorageService.MESSAGE_QUEUE_SOUND_COUNT_KEY, messageCount);
        }

        this.currentTab = this.memStorageService.get(this.memStorageService.ACTIVE_QUEUE_KEY) || "pending";
        this.checkQueueCounter();
    }

    ngOnDestroy() {
        if (this.intervalHandler) {
            window.clearInterval(this.intervalHandler);
        }
    }

    get langCulture() {
        return (window["app"].cultureInfo || "en").toUpperCase();
    }

    onTabClick(queueName: string) {
        this.currentTab = queueName;
        this.memStorageService.save(this.memStorageService.ACTIVE_QUEUE_KEY, this.currentTab);
    }

    onNotify(message : string): void {
        switch (message) {
            case "disable-tabs":
                this.tabsDisabled = true;
                break;
            case "enable-tabs":
                this.tabsDisabled = false;
                break;
        }
    }

    changePendingQueueAlarm(event, soundId) {
        event.preventDefault();
        this.currentPendingQueueSound = this.alarms[soundId];
        this.memStorageService.save(this.memStorageService.PENDING_QUEUE_SOUND_KEY, soundId);
    }

    changeMessagegQueueAlarm(event, soundId) {
        event.preventDefault();
        this.currentMessageQueueSound = this.alarms[soundId];
        this.memStorageService.save(this.memStorageService.MESSAGE_QUEUE_SOUND_KEY, soundId);
    }

    getPendingQueueStats() {
        try {
            this.dataService.getNofTotalPending()
                .subscribe((count: any) => {
                    if (count != this.pendingCount) {
                        this.pendingCount = count;
                        this.updatePendingTemplateCountToPage(count);
                    }
                    this.memStorageService.save(this.memStorageService.PENDING_QUEUE_SOUND_COUNT_KEY, count);
                });
        }
        catch (e) {
        }
    }

    private getMessageCount() {
        this.dataService.getTotalUnreadMessages()
            .subscribe((count: any) => {
                if (this.messageCount != count) {
                    this.messageCount = count;
                    this.updateUnreadCountToPage(count);
                }
                this.memStorageService.save(this.memStorageService.MESSAGE_QUEUE_SOUND_COUNT_KEY, count);
            });
    }

    getParkingQueueStats() {
        try {
            this.dataService.getNofTotalParked()
                .subscribe((count: any) => {
                    if (count != this.parkCount) {
                        this.parkCount = count;
                        this.updateParkTemplateCountToPage();
                    }
                });
        }
        catch (e) {
        }
    }

    private checkQueueCounter() {
        const self = this;
        this.intervalHandler = window.setInterval(function () {
            self.getMessageCount.call(self);
            self.getPendingQueueStats.call(self);
            self.getParkingQueueStats.call(self);
        }, 5000);
    }

    private updatePendingTemplateCountToPage(newCount): void {
        const self = this;
        let audioElem: any;

        let prevCount = this.memStorageService.get(this.memStorageService.PENDING_QUEUE_SOUND_COUNT_KEY);

        let shouldPlaySound = (prevCount === 0 && newCount > 0);
        if (shouldPlaySound) {
            audioElem = document.getElementById('pending-queue-sound');
            audioElem.load();
            audioElem.play();
        }

        self.runPendingBellAnimation = true;

        setTimeout(function () {
            self.runPendingBellAnimation = false;
            if (shouldPlaySound) {
                audioElem.pause();
                audioElem.currentTime = 0;
            }
        }, 10000);
    }

    private updateUnreadCountToPage(newCount): void {
        const self = this;
        let audioElem: any;

        let prevCount = this.memStorageService.get(this.memStorageService.MESSAGE_QUEUE_SOUND_COUNT_KEY);

        let shouldPlaySound = (prevCount === 0 && newCount > 0);
        if (shouldPlaySound) {
            audioElem = document.getElementById('message-queue-sound');
            audioElem.load();
            audioElem.play();
        }

        self.runUnreadBellAnimation = true;

        setTimeout(function () {
            self.runUnreadBellAnimation = false;
            if (shouldPlaySound) {
                audioElem.pause();
                audioElem.currentTime = 0;
            }
        }, 10000);
    }

    private updateParkTemplateCountToPage(): void {
        const self = this;
        self.runParkBellAnimation = true;
        setTimeout(function () {
            self.runParkBellAnimation = false;
        }, 10000);
    }
}

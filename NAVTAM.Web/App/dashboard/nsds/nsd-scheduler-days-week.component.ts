﻿import { Component, OnInit, OnDestroy, AfterViewInit, HostListener, Input, ChangeDetectorRef } from '@angular/core'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators } from '@angular/forms'
import { NsdFormComponent } from '../nsds/nsd-form.component';
import { WindowRef } from '../../common/windowRef.service';

import { ProposalStatus, ProposalType } from '../shared/data.model';

import * as moment from 'moment';
import { merge } from 'rxjs/observable/merge';

declare var $: any;

@Component({
    selector: 'scheduler-days-week',
    templateUrl: '/app/dashboard/nsds/nsd-scheduler-days-week.component.html'
})
export class NsdSchedulerDaysOfWeekComponent implements OnInit, AfterViewInit, OnDestroy {

    public parent: NsdFormComponent = null;
    @Input('form') form: FormGroup;
    @Input('model') model: any;

    lastEventId: number = -1;
    currentEvent: any = null;
    displayStartDate: any = null;

    slootDuration = 15;
    scrollTime = '06:00:00';
    initialized: boolean = false;

    data: any = null;
    calendar: any;
    isFormValid: boolean = false; 

    SR = "06:00";
    SS = "18:00";


    eventDetails = {
        starttime: '',
        endtime: '',
        s_sunrise: false,
        s_sunset: false,
        e_sunrise: false,
        e_sunset: false,
        s_setTime: false,
        e_setTime: false
    }

    defaultColor = '#3a87ad';
    selectedColor = '#a1c5f761';

    startEventUpdate: any = null;
    start: any = null;
    end: any = null;
    starttime: any = null;
    endtime: any = null;
    sheduleCleared: boolean = false;
    weekEventsDic = {};
    arrayOfKeys;
    repeatToggle: boolean = false;   

    public dailyRow: any = {
        starttime: "",
        endtime: ""
    }

    constructor(private windowRef: WindowRef, private cdRef: ChangeDetectorRef) {
        this.arrayOfKeys = Object.keys(this.weekEventsDic);
    }

    ngOnInit() {
    }

    ngOnDestroy() {
    }

    ngAfterViewInit() {
        let self = this;
        this.calendar = $('#calendar-weekly');
        $('.w-datepicker').each(function () {
            $(this).bootstrapDatepicker({
                autoclose: false,
                todayHighlight: true, 
                orientation: "top", 
                startView: 0, // 0: month view , 1: year view, 2: multiple year view
                language: "en",
                forceParse: false,
                daysOfWeekDisabled: "", // Disable 1 or various day. For monday and thursday: 1,3
                calendarWeeks: false, // Display week number 
                toggleActive: true, // Close other when open
                multidate: false, // Allow to select various days
            }).on('changeDate', function (e) {
                self.dateChanged(e);
                $(this).bootstrapDatepicker('hide');
            });
        });

        $('#w-starttime').mask('00:00', { onComplete: function (cep) {self.starttime = cep; }});
        $('#w-endtime').mask('00:00', {onComplete: function (cep) {self.endtime = cep;}});
        $('#w-modal-starttime').mask('00:00', {onComplete: function (cep) {self.eventDetails.starttime = cep;}});
        $('#w-modal-endtime').mask('00:00', {onComplete: function (cep) {self.eventDetails.endtime = cep;} });
    }

    initialize(data, start, end, slotDuration?, events?) {
        this.data = data;
        let currentDate = moment();

        this.initCalendar(this.calendar, currentDate);

        if (this.data) {
            if (this.data.length > 0) {
                start = moment(start);
                end = moment(end);

                this.start = new Date(start.format('YYYY'), start.format('MM') - 1, start.format('DD'));
                this.end = new Date(end.format('YYYY'), end.format('MM') - 1, end.format('DD'));
            }

            for (let i = 0; i < this.data.length; i++) {

                let obj = JSON.parse(this.data[i].event);

                let event = this.setEvent(obj);

                if (i === 0) {
                    this.displayStartDate = start;
                    this.scrollTime = `${this.starttime}:00`;
                    currentDate = event.start;
                }

                this.starttime = start.format("HH:mm");
                this.endtime = end.format("HH:mm");

                this.calendar.fullCalendar('renderEvent', event, true); 
            }
            this.calendar.fullCalendar('gotoDate', currentDate);
        } else if( events ){
            for (let i = 0; i < events.length; i++) {
                let evt = this.setEvent(events[i]);
                if (i === 0) {
                    currentDate = evt.start;
                }
                this.calendar.fullCalendar('renderEvent', evt, true); 
            }
            this.calendar.fullCalendar('gotoDate', currentDate);
        } else {
            //trick to enforce refreshing the calendar when modifications were made with calendar hidden 
            this.calendar.fullCalendar('prev');
            this.calendar.fullCalendar('next');
        }
    }

    toggleShedule(event, clearItemD: boolean = false) {
        let isPannelOpenNow = this.isPanelOpen();

        if (event) {
            event.preventDefault();
            this.repeatToggle = true;
        } else {
            this.repeatToggle = false;
        }
        if (!this.initialized) {
            this.slideToggle();
        }

        this.showSchedule(this.parent.type === 'clone' ? false : clearItemD);

        let isPannelOpenThen = this.isPanelOpen();

        if (isPannelOpenNow === isPannelOpenThen) {
            if (!isPannelOpenNow || !this.repeatToggle) {
                this.slideToggle();
            }
        }
    }

    deleteEvents(event, clearItemD: boolean = false) {
        if (event && clearItemD) {
            if (this.data && this.data.length > 0) {
                this.parent.resetSchedulersStatus();
            }
            event.preventDefault();
        }

        this.weekEventsDic = {};
        this.parent.hasScheduler = false;

        this.data = new Object();

        this.start = null;
        this.end = null;
        this.starttime = null;
        this.endtime = null;

        this.clearSchedule(clearItemD);

        if (clearItemD) {
            let enforceToggle = this.parent.type === 'clone' || this.parent.model.proposalId;
            this.parent.schedulerDaily.toggleScheduleAfterDeleteAction(enforceToggle, true);
            this.parent.schedulerDate.toggleScheduleAfterDeleteAction(enforceToggle, true);
        }
        this.parent.enableActivePeridDates();
        this.data = null;
    }

    removeEvents() {
        if (this.hasEvents()) {
            this.calendar.fullCalendar('removeEvents');
            this.weekEventsDic = {};
            this.data = null;

            this.start = null;
            this.end = null;
            this.starttime = null;
            this.endtime = null;

            $('#w-start').bootstrapDatepicker('update', '');
            $('#w-end').bootstrapDatepicker('update', '');


            this.toggleScheduleAfterDeleteAction(true, true);
        }
    } 

    clearSchedule(clearItemD: boolean) {
        if (this.data) {

            this.toggleScheduleAfterDeleteAction(true, this.initialized)

            $('#w-start').bootstrapDatepicker('update', '');
            $('#w-end').bootstrapDatepicker('update', '');

            this.calendar.fullCalendar('destroy');

            let start = null;
            let end = null;
            if (this.start) {
                let starttime = moment(this.starttime, 'HH:mm');
                let endtime = moment(this.endtime, 'HH:mm');
                start = moment(this.start).set({
                    hour: starttime.get('hour'),
                    minute: starttime.get('minute'),
                })
                end = moment(this.end).set({
                    hour: endtime.get('hour'),
                    minute: endtime.get('minute'),
                })
            }

            this.initialize((this.parent.type === 'clone' || this.parent.model.proposalId) ? this.data : null, start, end);

            if (start && start.isValid()) {
                this.calendar.fullCalendar('gotoDate', start);
            } else {
                this.calendar.fullCalendar('gotoDate', moment());
            }
        } else {
            if (clearItemD) {
                if (this.parent.type === 'clone') {
                    this.toggleScheduleAfterDeleteAction(this.parent.model.proposalId ? true : false, this.initialized);
                } else {
                    this.toggleScheduleAfterDeleteAction(this.sheduleCleared, this.hasEvents());
                    this.sheduleCleared = false;
                }
            } 

            this.initialize(null, this.start, this.end);
        }
    }

    toggleScheduleAfterDeleteAction(enforce: boolean, initialized : boolean) {
        if (enforce) {
            this.slideToggle();
            this.initialized = initialized;
        }
    }

    resetClearScheduleFlag() {
        this.sheduleCleared = true;
        this.repeatToggle = false;
    }

    slideToggle() {
        $('#schedule-weekly-content').slideToggle();
    }

    isPanelOpen() {
        return $('#schedule-weekly-content').css('display') !== 'none';
    }


    closePanelIfOpen() {
        if (this.isPanelOpen()) {
            this.slideToggle();
        }
    }

    showSchedule(clearItemD: boolean) {
        this.clearSchedule(clearItemD);

        if (!this.displayStartDate) {
            this.displayStartDate = moment();
        }

        if (this.start) {
            this.start = moment.isMoment(this.start) ? this.start : moment(this.start);
            this.end = moment.isMoment(this.end) ? this.end : moment(this.end);

            $('#w-start').bootstrapDatepicker('update', this.start.toDate());
        }

        if (this.end) {
            $('#w-end').bootstrapDatepicker('update', this.end.toDate());
        }
    }

    hasEvents() {
        return !$.isEmptyObject(this.weekEventsDic);
    }


    isClearButtonDisabled() {
        if (this.parent && this.parent.isReadOnly) {
            return true;
        }

        if (this.hasEvents()) {
            return false;
        }

        if (this.calendar) {
            var events = this.calendar.fullCalendar('clientEvents');
            return events.length <= 0;
        }

        return true;
    }

    checkboxChanged(control: string, e: any) {
        switch (control) {
            case 's_sunrise':
                this.eventDetails.s_sunrise = !this.eventDetails.s_sunrise;
                this.eventDetails.s_sunset = false;
                if (this.eventDetails.s_sunrise) {
                    this.eventDetails.e_sunrise = false;
                    this.eventDetails.starttime = this.SR;
                    this.starttime = this.SR;
                    if (!this.eventDetails.e_sunrise && !this.eventDetails.e_sunset) {
                        if (this.eventDetails.endtime) {
                            let hours = this.eventDetails.endtime.substring(0, 2);
                            if (parseInt(hours) <= 6) {
                                this.eventDetails.endtime = "07:00";
                            }
                        } else {
                            this.eventDetails.endtime = "07:00";
                        }
                    }
                }
                break;
            case 's_sunset':
                this.eventDetails.s_sunset = !this.eventDetails.s_sunset;
                this.eventDetails.s_sunrise = false;
                if (this.eventDetails.s_sunset) {
                    this.eventDetails.e_sunrise = false;
                    this.eventDetails.e_sunset = false;
                    this.eventDetails.starttime = this.SS;
                    this.starttime = this.SS;
                    if (!this.eventDetails.e_sunrise && !this.eventDetails.e_sunset) {
                        if (this.eventDetails.endtime) {
                            let hours = this.eventDetails.endtime.substring(0, 2);
                            if (parseInt(hours) <= 18) {
                                this.eventDetails.endtime = "22:00";
                            }
                        } else {
                            this.eventDetails.endtime = "22:00";
                        }
                    }
                }
                break;
            case 'e_sunrise':
                this.eventDetails.e_sunrise = !this.eventDetails.e_sunrise;
                this.eventDetails.e_sunset = false;
                if (this.eventDetails.e_sunrise) {
                    this.eventDetails.s_sunrise = false;
                    this.eventDetails.s_sunset = false;
                    this.eventDetails.endtime = this.SR;
                    this.endtime = this.SR;
                    if (!this.eventDetails.s_sunrise && !this.eventDetails.s_sunset) {
                        if (this.eventDetails.starttime) {
                            let hours = this.eventDetails.starttime.substring(0, 2);
                            if (parseInt(hours) >= 6) {
                                this.eventDetails.starttime = "04:00";
                            }
                        } else {
                            this.eventDetails.starttime = "04:00";
                        }
                    }
                }
                break;
            case 'e_sunset':
                this.eventDetails.e_sunset = !this.eventDetails.e_sunset;
                this.eventDetails.e_sunrise = false;
                if (this.eventDetails.e_sunset) {
                    this.eventDetails.s_sunset = false;
                    this.eventDetails.endtime = this.SS;
                    this.endtime = this.SS;
                    if (!this.eventDetails.s_sunrise && !this.eventDetails.s_sunset) {
                        if (this.eventDetails.starttime) {
                            let hours = this.eventDetails.starttime.substring(0, 2);
                            if (parseInt(hours) >= 18) {
                                this.eventDetails.starttime = "13:00";
                            }
                        } else {
                            this.eventDetails.starttime = "13:00";
                        }
                    }
                }
                break;
        }
    }

    onSetTimeChanged(control: string, e: any) {
        let events = this.getCalendarEvents();
        for (let i = 0; i < events.length; i++) {
            if (events[i]._id !== this.currentEvent['_id']) {
                if (control === "s_setTime") {
                    events[i].s_setTime = false;
                } else { //set_endTime
                    events[i].e_setTime = false;
                }
            }
        }
         
        switch (control) {
            case 's_setTime':
                this.eventDetails.s_setTime = !this.eventDetails.s_setTime;
                this.currentEvent['s_setTime'] = this.eventDetails.s_setTime;
                this.starttime = this.eventDetails.s_setTime ? this.eventDetails.starttime : '';
                break;
            case 'e_setTime':
                this.eventDetails.e_setTime = !this.eventDetails.e_setTime;
                this.currentEvent['e_setTime'] = this.eventDetails.e_setTime;
                this.endtime = this.eventDetails.e_setTime ? this.eventDetails.endtime : '';
                break;
        }
    }

    isValid() {
        if (this.parent && this.parent.isReadOnly) {
            return false;
        }

        let hasStart = $('#w-start').bootstrapDatepicker('getDate') != null;
        let hasEnd = $('#w-end').bootstrapDatepicker('getDate') != null;
        let hasEvents = false;
        if (this.calendar) {
            let events = this.getCalendarEvents();
            hasEvents = events.length > 0;
        }

        return hasStart && this.starttime && hasEnd && this.endtime && hasEvents;
    }

    zoomIn() {
        if (this.slootDuration > 0) {
            this.slootDuration -= 1;

            let events = null;
            if (!this.data) {
                events = this.calendar.fullCalendar('clientEvents');
            }
            this.calendar.fullCalendar('destroy');

            let starttime = moment(this.starttime, 'HH:mm');
            let endtime = moment(this.endtime, 'HH:mm');

            let start = moment(this.start).set({
                hour: starttime.get('hour'),
                minute: starttime.get('minute'),
            })
            let end = moment(this.end).set({
                hour: endtime.get('hour'),
                minute: endtime.get('minute'),
            })
            this.initialize(this.data, start, end, this.slootDuration, events);
        }
    }

    zoomOut() {
        if (this.slootDuration < 15) {
            this.slootDuration += 1;
            let events = null;
            if (!this.data) {
                events = this.calendar.fullCalendar('clientEvents');
            }
            this.calendar.fullCalendar('destroy');

            let starttime = moment(this.starttime, 'HH:mm');
            let endtime = moment(this.endtime, 'HH:mm');

            let start = moment(this.start).set({
                hour: starttime.get('hour'),
                minute: starttime.get('minute'),
            })
            let end = moment(this.end).set({
                hour: endtime.get('hour'),
                minute: endtime.get('minute'),
            })
            this.initialize(this.data, start, end, this.slootDuration, events);
        }
    }

    areConsecutiveDays(d1, d2) {
        switch (d1) {
            case "MON":
                return d2 === "TUE";
            case "TUE":
                return d2 === "WED";
            case "WED":
                return d2 === "THU";
            case "THU":
                return d2 === "FRI";
            case "FRI":
                return d2 === "SAT";
            case "SAT":
                return d2 === "SUN";
            case "SUN":
                return d2 === "MON";
            default:
                return false;
        }
    }

    sameTimeRange(ev1, ev2) {
        return ev1.extendedHours === ev2.extendedHours && ev1.startTime === ev2.startTime && ev1.endTime === ev2.endTime;
    }

    areMergeableEvents(ev1, ev2) {
        return this.areConsecutiveDays(ev1.lastDay, ev2.orgDayOfWeek) && this.sameTimeRange(ev1, ev2); //ev1.extendedHours === ev2.extendedHours && ev1.startTime === ev2.startTime && ev1.endTime === ev2.endTime;
    }

    mergeEventDetails(details) {
        let merged = [];
        let last = null;
        for (let k = 0; k < details.length; k++) {
            let current = details[k];
            current.orgDayOfWeek = current.dayOfWeek;
            if (last && this.areMergeableEvents(last, current)) {
                last.dayOfWeek = `${last.orgDayOfWeek}-${current.dayOfWeek}`;
                last.lastDay = current.dayOfWeek;
            } else {
                current.lastDay = current.dayOfWeek;
                last = current;
                merged.push(current);
            }
        }

        // fix for merging SUN-MON
        let len = merged.length;
        if (len >= 2 && this.areMergeableEvents(merged[len - 1], merged[0])) {
            let last = merged[len - 1];
            let current = merged[0];
            last.dayOfWeek = `${last.orgDayOfWeek}-${current.lastDay}`;
            merged[0] = last;
            merged.pop();
        }

        let normalized = [];
        for (let i = 0; i < merged.length; i++) {
            let current = merged[i];
            if (current._status === undefined) {
                current._status = 0;
                normalized.push(current);
                for (let j = i + 1; j < merged.length; j++) {
                    let next = merged[j];
                    if (next._status === undefined && this.sameTimeRange(current, next)) {
                        current.dayOfWeek = `${current.dayOfWeek} ${next.dayOfWeek}`;
                        next._status = 1;
                    }
                }
            }
        }

        return normalized; // merged;
    }

    formatTimeRange(value) {
        var start = (value.start || "").replace(":", "");
        var end = (value.end || "").replace(":", "");
        return `${start}-${end}`;
    }

    pushTimeRange(ranges: any[], key: string, range: string) {
        const last = ranges.length && ranges[ranges.length - 1];
        if (last && last.range === range) {
            last.key += ` ${key}`;
        } else {
            ranges.push({ key: key, range: range });
        }
    }

    parseEvents(toggle : boolean = false) {
        let events = this.calendar.fullCalendar('clientEvents');
        events = events.sort(this.sortEvents);

        if (this.data && this.data.length > 0) {
            let proposalId = this.data[0].proposalId;
            this.resetDataEvents(proposalId, events);
        }

        this.weekEventsDic = {};
        let details = this.mergeEventDetails(this.getDailyEvent(events));

        for (let k = 0; k < details.length; k++) {
            this.updateDataEvent(details[k]);

            if (details[k].extendedHours && details[k].startTime === "00:00") {
                continue;
            }

            let key = details[k].dayOfWeek;
            let value = {
                start: details[k].startTime,
                end: details[k].endTime,
            }

            if (this.weekEventsDic[key]) {
                this.weekEventsDic[key].push(value);
            } else {
                this.weekEventsDic[key] = [];
                this.weekEventsDic[key].push(value);
            }
        }

        let dayRanges = []
        let keys = Object.keys(this.weekEventsDic);
        keys.forEach(key => {
            let time = this.weekEventsDic[key];
            let timeRanges = time.map(this.formatTimeRange).join(' ');
            this.pushTimeRange(dayRanges, key, timeRanges);
        });
        let itemD = dayRanges.map(r => `${r.key} ${r.range}`).join(', ');
       
        this.arrayOfKeys = Object.keys(this.weekEventsDic);

        this.parent.hasScheduler = true;

        if (this.start && this.end) {
            let startDate = moment(this.start);
            let endDate = moment(this.end);

            let sDay = startDate.format('DD');
            let sMonth = startDate.format('MM');
            let sYear = startDate.format('YYYY');
            let sTime = (this.starttime === "SR" ? this.SR : (this.starttime === "SS" ? this.SS : this.starttime));

            let eDay = endDate.format('DD');
            let eMonth = endDate.format('MM');
            let eYear = endDate.format('YYYY');
            let eTime = (this.endtime === "SR" ? this.SR : (this.endtime === "SS" ? this.SS : this.endtime));

            let start = moment.utc(`${sDay}-${sMonth}-${sYear} ${sTime}`, 'DD-MM-YYYY HH:mm');
            let end = moment.utc(`${eDay}-${eMonth}-${eYear} ${eTime}`, 'DD-MM-YYYY HH:mm');

            this.parent.disableActivePeridDates(start, end, itemD);
        }

        if (toggle) {
            this.slideToggle();
            //this.toggleShedule(null, true);
        }
    }

    getCalendarEvents() {
        return this.calendar.fullCalendar('clientEvents');
    }

    slideScheduleClose() {
        this.slideToggle();
        this.initialized = true;
    }

    dateChanged(ev) {
        switch (ev.currentTarget.id) {
            case "w-start":
                this.start = moment(new Date(ev.currentTarget.value));
                this.calendar.fullCalendar('removeEvents');
                this.calendar.fullCalendar('gotoDate', this.start);

                if (this.end && this.end.isValid()) {
                    let el = $('#w-start');
                    if (this.start.isAfter(this.end)) {
                        this.start = null;
                        var x = el.bootstrapDatepicker('getDate');
                        el.bootstrapDatepicker('update', '');
                        el.css({
                            "border-color": "red",
                            "border-width": "1px",
                            "border-style": "solid"
                        });
                    } else {
                        el.css({
                            "border-color": "white",
                            "border-width": "0px",
                        });
                    }
                }
                break;
            case "w-end":
                this.end = moment(new Date(ev.currentTarget.value));
                if (this.start && this.start.isValid()) {
                    let el = $('#w-end');
                    if (this.start.isAfter(this.end)) {
                        this.end = null;
                        el.bootstrapDatepicker('update', '');
                        el.css({
                            "border-color": "red",
                            "border-width": "1px",
                            "border-style": "solid"
                        });
                    } else {
                        el.css({
                            "border-color": "white",
                            "border-width": "0px",
                        });
                    }
                }
                break;
        }
        $(this).datepicker('hide');
    }

    private sortEvents(event1, event2) {
        var start1 = new Date(event1.start).getTime();
        var start2 = new Date(event2.start).getTime();
        return start1 > start2 ? 1 : -1;
    };

    private getDailyEvent(dailyEvents) {
        let events = []
        for (let i = 0; i < dailyEvents.length; i++) {
            let startEvent = dailyEvents[i].start;
            let endEvent = dailyEvents[i].end

            let dayOfWeek = startEvent.format('ddd').toUpperCase();

            let startHour = startEvent.format('HH');
            let startMinute = startEvent.format('mm');

            let endHour = endEvent.format('HH');
            let endMinute = endEvent.format('mm');

            let start = null;
            let end = null;
            if (dailyEvents[i].s_sunrise) {
                start = "SR";
            }
            else if (dailyEvents[i].s_sunset) {
                start = "SS";
            }
            if (dailyEvents[i].e_sunrise) {
                end = "SR";
            }
            else if (dailyEvents[i].e_sunset) {
                end = "SS";
            }

            start = start ? start : moment(`${startHour}:${startMinute}`, 'HH:mm').format('HH:mm');
            end = end ? end : moment(`${endHour}:${endMinute}`, 'HH:mm').format('HH:mm');

            events.push({
                id: dailyEvents[i]._id || dailyEvents[i].id,
                dayOfWeek: dayOfWeek,
                startTime: start,
                endTime: end,
                start: dailyEvents[i].start,
                end: dailyEvents[i].end,
                s_sunrise: dailyEvents[i].s_sunrise,
                e_sunrise: dailyEvents[i].e_sunrise,
                s_sunset: dailyEvents[i].s_sunset,
                e_sunset: dailyEvents[i].e_sunset,
                extendedHours: dailyEvents[i].extendedHours,
                extEndTime: dailyEvents[i].exEndtime,
                exStarttime: dailyEvents[i].exStarttime,
                exEndtime: dailyEvents[i].exEndtime,
            });


        }

        return events.length > 0 ? this.removeDuplicates(events) : events;
    }

    private removeDuplicates(myArr) {
        var props = Object.keys(myArr[0])
        return myArr.filter((item, index, self) =>
            index === self.findIndex((t) => (
                props.every(prop => {
                    return t[prop] === item[prop]
                })
            ))
        )
    }

    private initCalendar(calendar: any, currentDate: any) {
        const self = this;

        let duration = `00:${this.windowRef.zeroPad(this.slootDuration, 2)}:00`

        calendar.fullCalendar({
            defaultDate: moment(currentDate),
            defaultView: 'agendaWeek',
            contentHeight: 820,
            firstDay: 1,
            slotDuration: duration,
            allDaySlot: false,
            columnFormat: {
                week: 'ddd',
            },
            selectable: true,
            editable: true,
            displayEventTime: false,
            droppable: true, // this allows things to be dropped onto the calendar !!!
            eventLimit: true, // allow "more" link when too many events, 
            fixedWeekCount: false,
            eventOverlap: false,
            header: false,
            axisFormat: 'HH:mm',
            timeFormat: 'HH:mm{ - HH:mm}',
            //maxtime: "23:59:00",
            minTime: 0,
            eventDragStart: function (event, jsEvent, ui, view) {
                self.startEventUpdate = {
                    start: event.start,
                    end: event.end
                };
            },
            eventDataTransform: function (event) {           
                return event;
            },         
            eventDragStop: function (event, jsEvent, ui, view) {
            },
            eventDrop: function (event, delta, revertFunc, jsEvent, ui, view) {

                let isSameDay = event.start.isSame(event.end, 'day');
                if (!isSameDay) { // crossed midnight
                    revertFunc();
                }

                if (event.s_setTime) {
                    self.starttime = moment(event.start).format('HH:mm');
                }
                if (event.e_setTime) {
                    self.endtime = moment(event.end).format('HH:mm');
                }

                self.startEventUpdate = null;
            },
            eventResizeStart: function (event, jsEvent, ui, view) {
                self.startEventUpdate = {
                    start: event.start,
                    end: event.end
                };
            },
            select: function (start, end, event, view) {
                let eventData = {
                    start: start,
                    end: end,
                    s_sunrise: false,
                    s_sunset: false,
                    e_sunrise: false,
                    e_sunset: false,
                    extendedHours: false,
                    exStarttime: '',
                    exEndtime: '',
                    allDay: false,
                };

                if (self.IsValidEventStartEndDate(eventData)) {
                    if (!self.parent.isReadOnly && !self.isOverlapping({ start: start, end: end })) {
                        self.calendar.fullCalendar('renderEvent', eventData, true);

                        self.parent.hasScheduler = true;
                    }
                } 

                self.calendar.fullCalendar('unselect');
                
            },

            eventResizeStop: function (event, jsEvent, ui, view) {
                let x = 1;
            },
            eventResize: function (event, delta, revertFunc, jsEvent, ui, view) {
                if (!self.IsValidEventStartEndDate(event)) {
                    revertFunc();
                } else {
                    let prevStartTime = self.startEventUpdate.start.format('hh:mm');
                    let events = calendar.fullCalendar('clientEvents');
                    for (let i = 0; i < events.length; i++) {
                        let startTime = events[i].start.format('hh:mm');
                        let endTime = events[i].end.format('hh:mm');
                        if (event._id !== events[i]._id) {
                            //if ((events[i].parentId === event.parentId || events[i].groupId === event.groupId)/* && prevStartTime === startTime*/) {
                            //    events[i].end = events[i].end.add(delta._milliseconds, 'milliseconds');
                            //    calendar.fullCalendar('updateEvent', events[i]);
                            //}
                        } else if (event._id === events[i]._id) {
                            calendar.fullCalendar('updateEvent', events[i]);
                        }
                    }

                    self.startEventUpdate = null;
                }
            },
            eventRender: function (event, element, view) {             
                if (self.parent && self.parent.isReadOnly) {
                    event.editable = false;
                }

                let time = event.sunrise ? "SUNRISE - " : '{ ' + event.start.format('HH:mm') + ' - ';

                if (event.sunset) {
                    time = time + 'SUNSET'
                } else {
                    time = time + event.end.format('HH:mm') + ' }';
                }

                element.find('.fc-content').prepend('<span><i class="fa fa-clock-o m-r-5"></i>' + time + '</span>');
                element.css('background-color', event.eventColor);
                element.css('border-color', event.eventColor);                            
            },
            eventClick: function (event, jsEvent, view) {
                if (!self.model.readOnlyMode) {
                    let el = $('#calendar-weekly-modal');

                    self.eventDetails.starttime = event.start.format('HH:mm');
                    self.eventDetails.endtime = event.end.format('HH:mm');
                    self.currentEvent = event;

                    $('.time').mask('00:00');
                    el.modal();

                    self.eventDetails.s_sunrise = event.s_sunrise;
                    self.eventDetails.s_sunset = event.s_sunset;
                    self.eventDetails.e_sunrise = event.e_sunrise;
                    self.eventDetails.e_sunset = event.e_sunset;
                    self.eventDetails.s_setTime = event.s_setTime;
                    self.eventDetails.e_setTime = event.e_setTime;

                    el.on('shown.bs.modal', function (e) {
                        self.cdRef.detectChanges();
                    });

                    el.on('hidden.bs.modal', function () {
                        $(this).data('bs.modal', null);
                    });

                    el.on('hide.bs.modal', function (e) {
                        let pressedButton = $(document.activeElement).attr('id');
                        if (pressedButton === "weekly_btn-modal-yes") {
                            let _start = moment(self.eventDetails.starttime, "HH:mm").format("HH:mm");
                            let _end = moment(self.eventDetails.endtime, "HH:mm").format("HH:mm");
                            if (_start.length === 5) {
                                let eventStartTime = event.start.format('HH:mm');
                                let eventEndTime = event.end.format('HH:mm');

                                let events = calendar.fullCalendar('clientEvents');
                                let count = 0;
                                let lastParentId = events[0].parentId;
                                for (let i = 0; i < events.length; i++) {
                                    let startTime = events[i].start.format('HH:mm');
                                    let endTime = events[i].end.format('HH:mm');
                                    if ((events[i].parentId === event.parentId || events[i].groupId === event.groupId) && eventStartTime === startTime && eventEndTime == endTime) {
                                        if (events[i].parentId !== lastParentId) {
                                            lastParentId = events[i].parentId;
                                            count = 0;
                                        }
                                        count += 1;
                                        let start = events[i].start.clone();
                                        let end = events[i].end.clone();

                                        events[i].start = events[i].start.set({
                                            hour: parseInt(_start.substring(0, 2)),
                                            minute: parseInt(_start.substring(3))
                                        });
                                        events[i].end = events[i].end.set({
                                            hour: parseInt(_end.substring(0, 2)),
                                            minute: parseInt(_end.substring(3))
                                        });

                                        if (events[i].s_setTime) {
                                            self.starttime = self.eventDetails.starttime;
                                        }
                                        if (events[i].e_setTime) {
                                            self.endtime = self.eventDetails.endtime;
                                        }

                                        if (self.isOverlapping(events[i])) {
                                            events[i].start = start;
                                            events[i].end = end;
                                        } else {
                                            events[i].s_sunrise = self.eventDetails.s_sunrise;
                                            events[i].s_sunset = self.eventDetails.s_sunset;
                                            events[i].e_sunrise = self.eventDetails.e_sunrise;
                                            events[i].e_sunset = self.eventDetails.e_sunset;


                                            let eventColor = events[i].eventColor;
                                            if (self.eventDetails.s_sunrise || self.eventDetails.s_sunset || self.eventDetails.e_sunrise || self.eventDetails.e_sunset) {
                                                events[i].previousColor = events[i].eventColor;
                                                events[i].eventColor = '#2b3c4e';
                                            } else {
                                                if (event.eventColor === '#2b3c4e') {
                                                    events[i].eventColor = events[i].previousColor;
                                                    events[i].previousColor = null;
                                                }
                                            }

                                            calendar.fullCalendar('updateEvent', events[i]);
                                        }
                                    }
                                }
                            }
                        } else {
                            if (pressedButton === "weekly_btn-modal-delete") {
                                if (event.s_setTime) {
                                    self.starttime = '';
                                }
                                if (event.e_setTime) {
                                    self.endtime = '';
                                }

                                calendar.fullCalendar('removeEvents', event._id);
                                calendar.fullCalendar('renderEvents');
                            }
                        }
                        self.eventDetails.s_sunrise = false;
                        self.eventDetails.s_sunset = false;

                        $('#calendar-weekly-modal').off('hide.bs.modal');
                    });
                }               
            },
        });

    } 

    private updateDataEvent(event) {
        if (!this.data) {
            return
        };

        for (let i = 0; i < this.data.length; i++) {
            let obj = JSON.parse(this.data[i].event);

            let id = event._id || event.id;
            if (obj.id !== id) {
                continue
            };

            obj.start = event.start;
            obj.end = event.end;
            obj.s_sunset = event.s_sunset;
            obj.s_sunrise = event.s_sunrise;
            obj.e_sunset = event.e_sunset;
            obj.e_sunrise = event.e_sunrise;
            obj.extendedHours = event.extendedHours;
            obj.exStarttime = event.exStarttime;
            obj.exEndtime = event.exEndtime;

            this.data[i].event = JSON.stringify(obj);
        }
    }

    private resetDataEvents(proposalId, cEvents) {
        this.data = [];
        for (let i = 0; i < cEvents.length; i++) {
            let cEvent = cEvents[i];

            let event = {
                id: cEvent._id || cEvent.id,
                start: cEvent.start,
                end: cEvent.end,
                linked: cEvent.linked,
                groupId: cEvent.groupId,
                eventColor: cEvent.eventColor,
                previousColor: cEvent.previousColor,
                parentId: cEvent.parentId,
                s_sunset: cEvent.s_sunset,
                s_sunrise: cEvent.s_sunrise,
                e_sunset: cEvent.e_sunset,
                e_sunrise: cEvent.e_sunrise,
                s_setTime: cEvent.s_setTime,
                e_setTime: cEvent.e_setTime
            };

            this.data.push({
                calendarType: 2,
                proposalId: proposalId,
                calendarId: -1,
                eventId: event.id,
                itemD: this.model.itemD,
                event: JSON.stringify(event)
            });
        }
    }


    private isOverlapping(event) {
        var array = this.calendar.fullCalendar('clientEvents');
        for (let i in array) {
            if (array[i]._id != event._id) {
                if (!(array[i].start >= event.end || array[i].end <= event.start)) {
                    return true;
                }
            }
        }
        return false;
    }

    private IsValidEventStartEndDate(event) {
        return event.end.date() === event.start.date() 
    }

    private setEvent(obj) {
        let event = {
            id: obj.id,
            eventColor: obj.eventColor,
            previousColor: obj.previousColor,
            groupId: obj.groupId,
            linked: obj.linked,
            start: moment.utc(obj.start),
            end: moment.utc(obj.end),
            s_sunset: obj.s_sunset,
            s_sunrise: obj.s_sunrise,
            e_sunset: obj.e_sunset,
            e_sunrise: obj.e_sunrise,
            s_setTime: obj.s_setTime,
            e_setTime: obj.e_setTime,
            allDay: false,
            stick: true
        }

        return event;
    }

    // validators
    private isStartHasValue() {
        return $('#w-start').bootstrapDatepicker('getDate') != null;
    }

    isStartValidDate(): boolean {
        if (this.isStartHasValue()) {
            return this.isValidDate();
        }

        return true;
    }

    private isValidDate() : boolean {
        let start = moment($('#w-start').bootstrapDatepicker('getDate')).format("YYYY/MM/DD");
        let now = moment().format("YYYY/MM/DD");
        
        return moment(start).isSameOrAfter(now);
    }

    isValidEndTime() : boolean {
        if (this.isEndHaveValue()) {
            let end = this.endtime;
            let start= this.starttime;
            let now = moment().format('hh:mm');

            return (end > now && end > start);
        }

        return true;
    }

    isEndHaveValue(): boolean {
        return $('#w-end').bootstrapDatepicker('getDate') != null;
    }

    isEndValidDate(): boolean {
        let start = moment($('#w-start').bootstrapDatepicker('getDate'));
        let end = moment($('#w-end').bootstrapDatepicker('getDate'));

        if (start.isValid() && end.isValid()) {
            return start.isSameOrBefore(end);            
        }

        return true;
    }
}
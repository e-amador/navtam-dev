using System;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using NavCanada.Core.Data.NotamWiz.EF.Helpers;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.SqlServiceBroker;
using System.Data.Entity;
using NEDSYS.NsdEngine.Helpers;

namespace NEDSYS.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();


            //Register as a singleton
            container.RegisterInstance(new RepositoryFactories(), new ContainerControlledLifetimeManager());
            container.RegisterInstance(new NsdManagerFactories(), new ContainerControlledLifetimeManager());
            container.RegisterType<INsdManagerResolver, NsdManagerResolver>(new ContainerControlledLifetimeManager());

            //Register as always new instance
            container.RegisterInstance(new RepositoryProvider(container.Resolve<RepositoryFactories>()));
            container.RegisterType<IRepositoryProvider, RepositoryProvider>(new TransientLifetimeManager());

            //container.RegisterInstance(new AppDbContext("DefaultConnection"));
            container.RegisterType<DbContext, AppDbContext>(new InjectionConstructor("DefaultConnection"));
            var uow = new Uow(container.Resolve<AppDbContext>(), container.Resolve<RepositoryProvider>());

            //Register as always a new instance
            container.RegisterInstance(uow);
            container.RegisterType<IUow, Uow>(new TransientLifetimeManager());

            IQueue initiatorToTaskEngineSsbSettings = new SqlServiceBrokerQueue(container.Resolve<AppDbContext>(), SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings);
            IQueue taskEngineToInitiatorSsbSettings = new SqlServiceBrokerQueue(container.Resolve<AppDbContext>(), SqlServiceBrokerQueueSettings.TaskEngineToInitiatorSsbSettings);

            container.RegisterType<IQueue, SqlServiceBrokerQueue>(new TransientLifetimeManager());

            IClusterQueues queues = new ClusterQueues(
                    initiatorToTaskEngineSsbSettings,
                    taskEngineToInitiatorSsbSettings);

            container.RegisterInstance(queues);
            container.RegisterType<IClusterQueues, ClusterQueues>(new TransientLifetimeManager());
        }
    }
}


﻿define(["jquery", "Resources", "utilities", "kendo-web", "kendoExt"], function ($, res, Util)
{

    var thisIndex, expandedRow;
    var grid;
    var scheduler;
    var currentEvent;
    var MydataSource;
    var check;
    var validator;
    var startDatetime = null;
    var endDatetime = null;
    var proposalVM = null;
    var timeScheduleData = null;
    var docisready = false;
    var labels = {};

    return {

        init: function (TimeScheduleData, _BeginDatetime, _ExpiryDatetime, model)
        {
            thisIndex = this;
            proposalVM = model;
            grid: $("#grid");
            scheduler: $("#scheduler").data("kendoScheduler");
            check: "success";
            validator: null;
            timeScheduleData = TimeScheduleData;
            startDatetime = _BeginDatetime;
            endDatetime = _ExpiryDatetime;


        },
        PostInitUI: function ()
        {

            $("#weekdaystd").hide();
            $("#timeslectortd").hide();                                 
            $('#FromTimeObjectDT').data('kendoTimePicker').enable(false);
            $('#ToTimeObjectDT').data('kendoTimePicker').enable(false);

            thisIndex._ResetSubSelection();
        },

        initUI: function (lbls)
        {
            labels = lbls;
            validator = $("#errorinput").kendoValidator({
                rules: {
                    Overlapping: function (e)
                    {
                        if (check === "error_Overlapping")
                        {
                            $("#errorinput").addClass('Invalid')
                            return false;
                        }

                        $("#errorinput").removeClass('Invalid')
                        return true;
                    },
                    SevenDayGap: function (e)
                    {
                        if (check === "error_SevenDayGap")
                        {
                            $("#errorinput").addClass('Invalid')
                            return false;
                        }

                        $("#errorinput").removeClass('Invalid')
                        return true;
                    },
                    PastDate: function (e)
                    {
                        if (check === "error_PastDate")
                        {
                            $("#errorinput").addClass('Invalid')
                            return false;
                        }

                        $("#errorinput").removeClass('Invalid')
                        return true;
                    }
                },
                messages: {
                    Overlapping: ((labels.Schdlr_valOverlaping_mesage)),
                    SevenDayGap: ((labels.Schdlr_valDaysGap_mesage)),
                    PastDate: ((labels.Schdlr_valPastDate_mesage))
                }


            }).data("kendoValidator");

            $('[class^="checkbox"]').keypress(function (event)
            {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == 13)
                {
                    thisIndex._clickCheckBox(this);
                }
                event.stopPropagation();
            });
            $('[class^="RadioButtonSelection"]').keypress(function (event)
            {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == 13)
                {
                    thisIndex._CheckRadionButton(this);
                }
                event.stopPropagation();
            });

    
            $("#FromTimeObject").kendoTimePicker({
                format: "HH",
                interval: 60
            });


            $("#ToTimeObject").kendoTimePicker({
                format: "HH",
                interval: 60
            });
                  

            $("#FromTimeObjectDT").kendoTimePicker({
                format: "HH",
                interval: 60
            });


            $("#ToTimeObjectDT").kendoTimePicker({
                format: "HH",
                interval: 60
            });


           // hide and unhide controls
            $('.RadioButtonSelectionWCT').click(function ()
            {
                // clear all controls
                thisIndex._ResetSubSelection();
                if ($(this).attr("value") == "weekdays")
                {
                    $("#weekdaystd").show();
                    $("#timeslectortd").show();
                    $("#schedulertd").hide();
                    $("#schedulerInfotd").hide();
                }

                if ($(this).attr("value") == "timeonly")
                {
                    $("#timeslectortd").show();
                    $("#schedulertd").hide();
                    $("#schedulerInfotd").hide();
                    $("#weekdaystd").hide();
                }
            });
            $("#addandclear").click(function ()
            {

                thisIndex._AddEventstoGrid();
                thisIndex._ResetSubSelection();

            });

            $("#add").click(function ()
            {

                thisIndex._AddEventstoGrid();

            });

            $("#GenerateTextbtn").click(function ()
            {

                document.getElementById("Generatedtext").value = thisIndex._GenerateText();

            });

            $("#Okbtn").click(function ()
            {
                proposalVM.nsdTokens.set("TimeScheduleData", MydataSource.data());
                proposalVM.nsdTokens.set("SchText", thisIndex._GenerateText());            
                $("#CarsScheduleAPI-Popup").data("kendoWindow").close();

            });

            $("#ClearGridandScheduler").click(function ()
            {
                thisIndex._ClearGrid();
                thisIndex._ResetSubSelection();
                thisIndex._ResetSelection();
                document.getElementById("Generatedtext").value = "";

            });

            $('#DT').change(function() {
                if ($(this).is(":checked"))
                {
                    $('#FromTimeObjectDT').data('kendoTimePicker').enable(true);
                    $('#ToTimeObjectDT').data('kendoTimePicker').enable(true);  
                }
                else
                {
                    $('#FromTimeObjectDT').data('kendoTimePicker').enable(false);
                    $('#ToTimeObjectDT').data('kendoTimePicker').enable(false);
                }
            });


            var products;
            MydataSource = new kendo.data.DataSource({
                data: products,
                schema: {
                    model: {
                        fields: {
                            DateId: { editable: false },
                            EndDateId: { editable: false },
                            TimeId: { editable: false },
                            EndTimeId: { editable: false },
                            ContinuesEvent: { editable: false },
                            RadioButtonSelectionWCT: { editable: false },
                            IncludeorExclude: { editable: false },
                            StartDateEndDate: { editable: false },
                            StartTimeEndTime: { editable: false },
                            StartTimeDTEndTimeDT: { editable: false }
                        }
                    }
                }
                ,
                change: function (e)
                {
                    thisIndex._reformateview("grid");
                }
            });

            $("#grid").kendoGrid({
                refresh: true,
                scrollable: true,
                dataSource: MydataSource,
                height: 250,
                width: 600,
                editable: {
                    confirmation: false // the confirmation message for destroy command
                },
                columns: [
                    {
                        field: "DateId",
                        title: labels.Schdlr_fieldDate_title,
                        width: 1,
                        hidden: true,
                        editable: false
                    }
                    ,
                    {
                        field: "EndDateId",
                        title: labels.Schdlr_fieldEndDate_title,
                        width: 1,
                        hidden: true,
                        editable: false
                    }
                    ,
                    {
                        field: "TimeId",
                        title: labels.Schdlr_fieldTime_title,
                        width: 1,
                        hidden: true,
                        editable: false
                    }
                    ,
                    {
                        field: "EndTimeId",
                        title: labels.Schdlr_fieldEndTime_title,
                        width: 1,
                        hidden: true,
                        editable: false
                    }
                    ,
                       {
                           field: "ContinuesEvent",
                           title: labels.Schdlr_fieldContEvent_title,
                           width: 1,
                           hidden: true,
                           editable: false
                       }
                    ,
                    {
                        field: "RadioButtonSelectionWCT",
                        title: labels.Schdlr_fieldrbSelection_title,
                        width: 1,
                        hidden: true,
                        editable: false
                    }
                    ,
                    {
                        field: "IncludeorExclude",
                        title: labels.Schdlr_fieldAction_title,
                        width: 100,
                        editable: false
                    },
                    {
                        field: "StartDateEndDate",
                        title: labels.Schdlr_fieldDateTime_title,
                        editable: false
                    },
                    {
                        field: "StartTimeEndTime",
                        title: labels.Schdlr_fieldPeriod_title,
                        editable: false
                    },
                    {
                        field: "StartTimeDTEndTimeDT",
                        title: labels.Schdlr_fieldPeriodDT_title,
                        editable: false
                    }
                , { command: [{ className: "btn-destroy", name: "destroy", text: "Remove" }], width: 120 }
                ]
            });
            // grid
            if (timeScheduleData.replace(/""/g, '') !== "")
            {
                var dates = JSON.parse(timeScheduleData);
                for (var idx = 0; idx < dates.length; idx++)
                {
                    MydataSource.add({
                        RadioButtonSelectionWCT: dates[idx].RadioButtonSelectionWCT,
                        DateId: dates[idx].DateId,
                        EndDateId: dates[idx].EndDateId,
                        TimeId: dates[idx].TimeId,
                        EndTimeId: dates[idx].EndTimeId,
                        ContinuesEvent: dates[idx].ContinuesEvent,
                        IncludeorExclude: dates[idx].IncludeorExclude,
                        StartDateEndDate: dates[idx].StartDateEndDate,
                        StartTimeEndTime: dates[idx].StartTimeEndTime,
                        StartTimeDTEndTimeDT: dates[idx].StartTimeDTEndTimeDT
                    });
                }

            }

            $(document.body).keydown(function (e)
            {
                if (e.altKey && e.shiftKey && e.keyCode == 69)
                {
                    $("#FromTimeObject").data("kendoDropDownList").focus();
                }
            });
        },

        _Validate: function ()
        {
            return validator.validate();

        },

        _clickCheckBox: function clickCheckBox(box)
        {
            var $box = $(box);
            $box.prop('checked', !$box.prop('checked'));

        },

        // end check box entr key event action

        // radio button enter key event action

        _CheckRadionButton: function CheckRadionButton(box)
        {
            var $box = $(box);
            $box.click();
        },

        _GenerateText: function GenerateText()
        {
            var GeneratedText_Include;
            var kendoGrid = $("#grid").data('kendoGrid');
            var datasourcedata = kendoGrid.dataSource.data();
            if (datasourcedata.length > 0)
            {
                for (var i = 0; i < datasourcedata.length; i++)
                {
                    if (datasourcedata[i].IncludeorExclude === "Include")
                    {
                        if (datasourcedata[i].StartDateEndDate === "DLY")
                        {
                            if (datasourcedata[i].StartTimeEndTime === "H24")
                            {
                                GeneratedText_Include = datasourcedata[i].StartDateEndDate
                            }
                            else
                            {
                                GeneratedText_Include = datasourcedata[i].StartTimeEndTime.replace(/:/g, '') +
                                    (datasourcedata[i].StartTimeDTEndTimeDT != "" ? (" (DT " + datasourcedata[i].StartTimeDTEndTimeDT.replace(/:/g, '') + ")") : "")
                                    + " " + datasourcedata[i].StartDateEndDate;
                            }
                        }

                        else if (typeof GeneratedText_Include === 'undefined' || GeneratedText_Include === "")
                        {
                            GeneratedText_Include = datasourcedata[i].StartDateEndDate + " " + datasourcedata[i].StartTimeEndTime.replace(/:/g, '') +
                                    (datasourcedata[i].StartTimeDTEndTimeDT != "" ? (" (DT " + datasourcedata[i].StartTimeDTEndTimeDT.replace(/:/g, '') +")") : "");
                        }
                        else
                        {
                            GeneratedText_Include = GeneratedText_Include + ", " + datasourcedata[i].StartDateEndDate + " " + datasourcedata[i].StartTimeEndTime.replace(/:/g, '') +
                                    (datasourcedata[i].StartTimeDTEndTimeDT != "" ? (" (DT " + datasourcedata[i].StartTimeDTEndTimeDT.replace(/:/g, '') + ")") : "");
                        }

                    }   
                }
            }
            else
            {
                return null;
            }
            return (GeneratedText_Include).replace(/\s\s+/g, ' ');
        },
        _ClearGrid: function ClearGrid()
        {
            $("#grid").data('kendoGrid').dataSource.data([]);
        },

        _ResetSelection: function ResetSelection()
        {
            document.getElementById("weekdays").checked = false;
            document.getElementById("timeonly").checked = false;
            document.getElementById("weekdays").disabled = false;
            document.getElementById("timeonly").disabled = false;
            $("#weekdaystd").hide();
            $("#schedulertd").hide();
            $("#schedulerInfotd").hide();
            $("#timeslectortd").hide();

            $('#FromTimeObjectDT').data('kendoTimePicker').enable(false);
            $('#ToTimeObjectDT').data('kendoTimePicker').enable(false);

            document.getElementById("DT").checked = false;     
    
            thisIndex._Validate();
                                                                          
        },
        _ResetSubSelection: function ResetSubSelection()
        {
            var FromTimeObject = $("#FromTimeObject").data("kendoTimePicker");
            FromTimeObject.value("00:00 AM");

            var ToTimeObject = $("#ToTimeObject").data("kendoTimePicker");
            ToTimeObject.value("00:00 AM");

            var FromTimeObjectDT = $("#FromTimeObjectDT").data("kendoTimePicker");
            FromTimeObjectDT.value("00:00 AM");

            var ToTimeObjectDT = $("#ToTimeObjectDT").data("kendoTimePicker");
            ToTimeObjectDT.value("00:00 AM");

            $(".checkboxDays").removeAttr('checked');

            $("#DT").removeAttr('checked');
        },

        _SortGrid: function SortGrid()
        {
            var kendoGrid = $("#grid").data('kendoGrid');
            if (kendoGrid.dataSource.data().length > 0)
            {
                var dsSort = [];
                dsSort.push({ field: "IncludeorExclude", dir: "desc" });
                dsSort.push({ field: "DateId", dir: "asc" });
                kendoGrid.dataSource.sort(dsSort);
            }

        },
        _getDayofYear: function getDayofYear(Date)
        {
            var now = new Date();
            var start = new Date(now.getFullYear(), 0, 0);
            var diff = now - start;
            var oneDay = 1000 * 60 * 60 * 24;
            var day = Math.floor(diff / oneDay);
        },

        _getcurrentDateGap: function getcurrentDateGap(DateId1, DateId2)
        {
            var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
            var selectedyear1 = DateId1.substring(0, 4);
            var selectedMonth1 = (parseInt(DateId1.substring(4, 6)) - 1).toString();
            var selectedDay1 = DateId1.substring(6, 8);
            var firstDate = new Date(selectedyear1, selectedMonth1, selectedDay1);

            if (typeof DateId2.getMonth === 'function')
            {
                return Math.round((firstDate.getTime() - DateId2.getTime()) / (oneDay));
            }
            else
            {
                var selectedyear2 = DateId2.substring(0, 4);
                var selectedMonth2 = (parseInt(DateId2.substring(4, 6)) - 1).toString();
                var selectedDay2 = DateId2.substring(6, 8);

                var SecondDate = new Date(selectedyear2, selectedMonth2, selectedDay2);

                return Math.round((firstDate.getTime() - SecondDate.getTime()) / (oneDay));
            }
        },
        //Check if event date ranges follows within start and end of notam 
        _checkDates: function checkDates(DateId, EndDateId)
        {
            var notamstartdate;
            var notamenddate;
            if (!startDatetime)
            {

                notamstartdate = new Date();
                notamstartdate.setHours(0);
            }
            else
            {

                notamstartdate = startDatetime;
                startDatetime.setHours(0);
            }

            var currentbiggestgap = thisIndex._getcurrentDateGap(DateId, notamstartdate);
            if (currentbiggestgap < 0)
            {
                return false;
            }

            if (!endDatetime)
            {
            }
            else
            {                    
                notamenddate = endDatetime;
                notamenddate.setHours(0);
                currentbiggestgap = thisIndex._getcurrentDateGap(EndDateId, notamenddate);
                if (currentbiggestgap > 0)
                {
                    return false;
                }
            }



            return true;
        },

        _checkforduplicates: function checkforduplicates(ActionSelection, DateId, EndDateId, StartDateEndDate, StartTime, EndTime)
        {
            if (thisIndex._checkDates(DateId, EndDateId))
            {
                var kendoGrid = $("#grid").data('kendoGrid');
                var datasourcedata = kendoGrid.dataSource.data();
                if (datasourcedata.length > 0)
                {
                    for (var i = 0; i < datasourcedata.length; i++)
                    {
                        var DateIdArray = datasourcedata[i].DateId.split(',');
                        var EndDateIdArray = datasourcedata[i].EndDateId.split(',');
                        for (var j = 0 ; j < DateIdArray.length; j++)
                        {
                            var TimeIdArray = datasourcedata[i].TimeId.split(',');
                            var EndTimeIdArray = datasourcedata[i].EndTimeId.split(',');
                            for (var g = 0 ; g < TimeIdArray.length; g++)
                            {
                                if ((parseInt(TimeIdArray[g]) <= parseInt(StartTime) && parseInt(EndTimeIdArray[g]) >= parseInt(StartTime)) // start date between an existing start end date
                               || (parseInt(TimeIdArray[g]) <= parseInt(EndTime) && parseInt(EndTimeIdArray[g]) >= parseInt(EndTime)) // end date between an existing start end date
                                || (parseInt(TimeIdArray[g]) >= parseInt(StartTime) && parseInt(EndTimeIdArray[g]) <= parseInt(EndTime)))
                                {
                                    return "error_Overlapping";
                                }
                            }
                        }
                    }
                }
                if (datasourcedata.length > 0)
                {
                    var biggestgap = -1;

                    for (var i = 0; i < datasourcedata.length; i++)
                    {

                        if (datasourcedata[i].IncludeorExclude === ActionSelection)
                        {
                            var EndDateIdArray = datasourcedata[i].EndDateId.split(',');
                            for (var j = 0 ; j < EndDateIdArray.length; j++)
                            {

                                var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
                                var StartDate = new Date(DateId.substring(0, 4), (parseInt(DateId.substring(4, 6)) - 1).toString(), DateId.substring(6, 8));
                                var EndDate = new Date(EndDateId.substring(0, 4), (parseInt(EndDateId.substring(4, 6)) - 1).toString(), EndDateId.substring(6, 8));
                                var EndDateFromDataSource = new Date(EndDateIdArray[j].substring(0, 4), (parseInt(EndDateIdArray[j].substring(4, 6)) - 1).toString(), EndDateIdArray[j].substring(6, 8));

                                var currentbiggestgapfromStartDate = Math.round(Math.abs((StartDate.getTime() - EndDateFromDataSource.getTime()) / (oneDay)));
                                var currentbiggestgapfromEndDate = Math.round(Math.abs((EndDate.getTime() - EndDateFromDataSource.getTime()) / (oneDay)));

                                var currentbiggestgap;
                                if (currentbiggestgapfromStartDate <= currentbiggestgapfromEndDate)
                                {
                                    currentbiggestgap = currentbiggestgapfromStartDate;
                                }
                                else
                                {
                                    currentbiggestgap = currentbiggestgapfromEndDate;
                                }

                                if (currentbiggestgap <= 7 || biggestgap === -1)
                                {
                                    biggestgap = currentbiggestgap;
                                }
                                else
                                {
                                    if (biggestgap > 7)
                                    {
                                        biggestgap = currentbiggestgap;
                                    }
                                }
                            }
                        }
                    }
                    if (biggestgap > 7)
                    {
                        return "error_SevenDayGap";
                    }
                }

                return "success";
            }
            return "error_PastDate";
        },
        _split: function split(arr)
        {
            var res = [];
            var subres = [];
            for (var i = 0; i < arr.length; i++)
            {
                var length = subres.length;
                if (length === 0 || subres[length - 1] === parseInt(arr[i]) - 1)
                {
                    subres.push(parseInt(arr[i]));
                } else
                {
                    res.push(subres);
                    subres = [parseInt(arr[i])];
                }
            }
            res.push(subres);
            return res;
        },

        _checkforContinuity: function checkforContinuity(RadioButtonSelectionWCT, DateId, EndDateId, IncludeorExclude, StartDateEndDate, StartTimeEndTime, StartTimeDTEndTimeDT, StartTime, EndTime, continues)
        {
            var kendoGrid = $("#grid").data('kendoGrid');
            var datasourcedata = kendoGrid.dataSource.data();
            if (datasourcedata.length > 0)
            {
                for (var i = 0; i < datasourcedata.length; i++)
                {
                    if (RadioButtonSelectionWCT === "TimeOnly")
                    {

                        datasourcedata[i].StartTimeEndTime = datasourcedata[i].StartTimeEndTime + " " + StartTimeEndTime;
                        datasourcedata[i].StartTimeEndTime = datasourcedata[i].StartTimeEndTime.replace(/ MINUS /g, "M");
                        datasourcedata[i].StartTimeEndTime = datasourcedata[i].StartTimeEndTime.replace(/ PLUS /g, "P");


                        var StartTimeEndTimeArray = datasourcedata[i].StartTimeEndTime.split(' ');
                        StartTimeEndTimeArray.sort();
                        var FinalDateTimeArray = StartTimeEndTimeArray.join(" ");
                        FinalDateTimeArray = FinalDateTimeArray.replace(/M/g, " MINUS ");
                        FinalDateTimeArray = FinalDateTimeArray.replace(/P/g, " PLUS ");
                        datasourcedata[i].StartTimeEndTime = FinalDateTimeArray;

                        datasourcedata[i].TimeId = datasourcedata[i].TimeId + "," + StartTime;
                        var StartTimeArray = datasourcedata[i].TimeId.split(',');
                        StartTimeArray.sort();
                        var FinalStartTimeArray = StartTimeArray.join(",");
                        datasourcedata[i].TimeId = FinalStartTimeArray;


                        datasourcedata[i].EndTimeId = datasourcedata[i].EndTimeId + "," + EndTime;
                        var EndTimeArray = datasourcedata[i].EndTimeId.split(',');
                        EndTimeArray.sort();
                        var FinalEndTimeArray = EndTimeArray.join(",");
                        datasourcedata[i].EndTimeId = FinalEndTimeArray;



                        datasourcedata[i].StartTimeDTEndTimeDT = datasourcedata[i].StartTimeDTEndTimeDT + " " + StartTimeDTEndTimeDT;
                        datasourcedata[i].StartTimeDTEndTimeDT = datasourcedata[i].StartTimeDTEndTimeDT.replace(/ MINUS /g, "M");
                        datasourcedata[i].StartTimeDTEndTimeDT = datasourcedata[i].StartTimeDTEndTimeDT.replace(/ PLUS /g, "P");


                        var StartTimeDTEndTimeDTArray = datasourcedata[i].StartTimeDTEndTimeDT.split(' ');
                        StartTimeDTEndTimeDTArray.sort();
                        var FinalDateTimeDTArray = StartTimeDTEndTimeDTArray.join(" ");
                        FinalDateTimeDTArray = FinalDateTimeDTArray.replace(/M/g, " MINUS ");
                        FinalDateTimeDTArray = FinalDateTimeDTArray.replace(/P/g, " PLUS ");
                        datasourcedata[i].StartTimeDTEndTimeDTArray = FinalDateTimeDTArray;


                        return "1";

                    }

                    else if (RadioButtonSelectionWCT === "WeekDays")
                    {

                        if (datasourcedata[i].StartTimeEndTime === StartTimeEndTime)
                        {
                            datasourcedata[i].DateId = datasourcedata[i].DateId + "," + DateId;
                            datasourcedata[i].EndDateId = datasourcedata[i].EndDateId + "," + EndDateId;

                            datasourcedata[i].TimeId = datasourcedata[i].TimeId + "," + StartTime;
                            var startimearray = datasourcedata[i].TimeId.split(',');
                            startimearray.sort();
                            var jointstarttimearray = startimearray.join(',');
                            datasourcedata[i].TimeId = jointstarttimearray;

                            datasourcedata[i].EndTimeId = datasourcedata[i].EndTimeId + "," + EndTime;
                            var endtimearray = datasourcedata[i].EndTimeId.split(',');
                            endtimearray.sort();
                            var jointendtimearray = endtimearray.join(',');
                            datasourcedata[i].EndTimeId = jointendtimearray;

                            var daysarray = datasourcedata[i].DateId.split(',');
                            daysarray.sort();
                            var days = daysarray.join(",");
                            datasourcedata[i].DateId = days;
                            datasourcedata[i].EndDateId = days;

                            var _StartDateEndDate;

                            for (var w = 0; w < daysarray.length ; w++)
                            {
                                var weekdayElement = document.getElementById(parseInt(daysarray[w]));
                                if (_StartDateEndDate !== undefined)
                                {

                                    _StartDateEndDate = _StartDateEndDate + " ";
                                    _StartDateEndDate = _StartDateEndDate + (weekdayElement.name);
                                }
                                else
                                {
                                    _StartDateEndDate = (weekdayElement.name);
                                }
                            }
                            datasourcedata[i].StartDateEndDate = _StartDateEndDate;
                            datasourcedata[i].StartTimeEndTime = StartTimeEndTime;
                            return "1";
                        }
                        else if (datasourcedata[i].DateId === DateId)
                        {
                            datasourcedata[i].TimeId = datasourcedata[i].TimeId + "," + StartTime;
                            var StartTimeArray = datasourcedata[i].TimeId.split(',');
                            StartTimeArray.sort();
                            var FinalStartTimeArray = StartTimeArray.join(",");
                            datasourcedata[i].TimeId = FinalStartTimeArray;


                            datasourcedata[i].EndTimeId = datasourcedata[i].EndTimeId + "," + EndTime;
                            var EndTimeArray = datasourcedata[i].EndTimeId.split(',');
                            EndTimeArray.sort();
                            var FinalEndTimeArray = EndTimeArray.join(",");
                            datasourcedata[i].EndTimeId = FinalEndTimeArray;

                            datasourcedata[i].StartTimeEndTime = datasourcedata[i].StartTimeEndTime + " " + StartTimeEndTime;
                            datasourcedata[i].StartTimeEndTime = datasourcedata[i].StartTimeEndTime.replace(/ MINUS /g, "M");
                            datasourcedata[i].StartTimeEndTime = datasourcedata[i].StartTimeEndTime.replace(/ PLUS /g, "P");
                            var StartTimeEndTimeArray = datasourcedata[i].StartTimeEndTime.split(' ');
                            StartTimeEndTimeArray.sort();
                            var FinalDateTimeArray = StartTimeEndTimeArray.join(" ");
                            FinalDateTimeArray = FinalDateTimeArray.replace(/M/g, " MINUS ");
                            FinalDateTimeArray = FinalDateTimeArray.replace(/P/g, " PLUS ");
                            datasourcedata[i].StartTimeEndTime = FinalDateTimeArray;
                            return "1";
                        }
                    }

                }
            }
            return "0";

        },

        _unique: function unique(list)
        {
            var result = [];
            $.each(list, function (i, e)
            {
                if ($.inArray(e, result) == -1) result.push(e);
            });
            return result;
        },

        _checkforDateContinuity: function checkforDateContinuity()
        {
            var kendoGrid = $("#grid").data('kendoGrid');
            var datasourcedata = kendoGrid.dataSource.data();
            if (datasourcedata.length > 0)
            {

                for (var i = datasourcedata.length - 1; i >= 0 ; i--)
                {
                    if (!datasourcedata[i].ContinuesEvent)
                    {
                        for (var j = 0; j < datasourcedata.length; j++)
                        {
                            if (datasourcedata[i].uid !== datasourcedata[j].uid)
                            {
                                if (datasourcedata[i].DateId === datasourcedata[j].DateId && datasourcedata[i].IncludeorExclude === datasourcedata[j].IncludeorExclude)
                                {
                                    datasourcedata[j].StartTimeEndTime = datasourcedata[j].StartTimeEndTime + " " + datasourcedata[i].StartTimeEndTime; 
                                    var StartTimeEndTimeArray = datasourcedata[j].StartTimeEndTime.split(" ");
                                    StartTimeEndTimeArray.sort();
                                    var UniqueStartTimeEndTimeArray = thisIndex._unique(StartTimeEndTimeArray);
                                    var FinalDateTimeArray = UniqueStartTimeEndTimeArray.join(" ");
                                    datasourcedata[i].StartTimeEndTime = FinalDateTimeArray;

                                    datasourcedata[j].TimeId = datasourcedata[j].TimeId + "," + datasourcedata[i].TimeId;
                                    var StartTimeArray = datasourcedata[j].TimeId.split(',');
                                    StartTimeArray.sort();
                                    var FinalStartTimeArray = StartTimeArray.join(",");
                                    datasourcedata[j].TimeId = FinalStartTimeArray;


                                    datasourcedata[j].EndTimeId = datasourcedata[j].EndTimeId + "," + datasourcedata[i].EndTimeId;
                                    var EndTimeArray = datasourcedata[j].EndTimeId.split(',');
                                    EndTimeArray.sort();
                                    var FinalEndTimeArray = EndTimeArray.join(",");
                                    datasourcedata[j].EndTimeId = FinalEndTimeArray;


                                    datasourcedata[j].StartTimeDTEndTimeDT = datasourcedata[j].StartTimeDTEndTimeDT + " " + datasourcedata[i].StartTimeDTEndTimeDT;
                                    var StartTimeDTEndTimeDTArray = datasourcedata[j].StartTimeDTEndTimeDT.split(" ");
                                    StartTimeDTEndTimeDTArray.sort();
                                    var UniqueStartTimeDTEndTimeDTArray = thisIndex._unique(StartTimeDTEndTimeDTArray);
                                    var FinalDateTimeDTArray = UniqueStartTimeDTEndTimeDTArray.join(" ");
                                    datasourcedata[i].StartTimeDTEndTimeDT = FinalDateTimeDTArray;

          

                                    var itemtoremove = datasourcedata[i];
                                    datasourcedata.remove(itemtoremove);
                                    j = datasourcedata.length;
                                    i--;
                                }
                                else if (datasourcedata[i].StartTimeEndTime === datasourcedata[j].StartTimeEndTime && datasourcedata[i].IncludeorExclude === datasourcedata[j].IncludeorExclude
                                    && String(datasourcedata[j].DateId).substring(4, 6) === String(datasourcedata[i].DateId).substring(4, 6)
                                    && datasourcedata[i].DateId !== datasourcedata[j].DateId)
                                {
                                    datasourcedata[j].DateId = datasourcedata[j].DateId + "," + datasourcedata[i].DateId;
                                    datasourcedata[j].EndDateId = datasourcedata[j].EndDateId + "," + datasourcedata[i].EndDateId;

                                    var daysarray = datasourcedata[j].DateId.split(',');
                                    daysarray.sort();
                                    var days = daysarray.join(",");
                                    datasourcedata[j].DateId = days;
                                    datasourcedata[j].EndDateId = days;
                                    var _StartDateEndDate;
                                    if (datasourcedata[j].RadioButtonSelectionWCT === "WeekDays")
                                    {

                                        for (var w = 0; w < daysarray.length ; w++)
                                        {
                                            var weekdayElement = document.getElementById(parseInt(daysarray[w]));
                                            if (_StartDateEndDate !== undefined)
                                            {
                                                _StartDateEndDate = _StartDateEndDate + " ";
                                                _StartDateEndDate = _StartDateEndDate + (weekdayElement.name);
                                            }
                                            else
                                            {
                                                _StartDateEndDate = (weekdayElement.name);
                                            }
                                        }
                                        datasourcedata[j].StartDateEndDate = _StartDateEndDate;
                                    }
                                    var itemtoremove = datasourcedata[i];
                                    datasourcedata.remove(itemtoremove);
                                    j = datasourcedata.length;
                                    i--;
                                }
                            }
                        }

                    }
                }

            }

        },
        _reformateview: function reformateview(p1)
        {
            var kendoGrid = $("#grid").data('kendoGrid');
            var datasourcedata = kendoGrid.dataSource.data();
            var action;
            var RadioButtonSelectionWCT;
            for (var i = 0; i < datasourcedata.length; i++)
            {

                if (datasourcedata[i].IncludeorExclude === "Include")
                {
                    action = datasourcedata[i].IncludeorExclude;
                    RadioButtonSelectionWCT = datasourcedata[i].RadioButtonSelectionWCT;
                }
            }

            if (action === "Include" && RadioButtonSelectionWCT === "WeekDays")
            {

                    document.getElementById("weekdays").disabled = false;
                    document.getElementById("timeonly").disabled = true;
            }
            else if (action === "Include" && RadioButtonSelectionWCT === "TimeOnly")
            {
                    document.getElementById("timeonly").disabled = false;
                    document.getElementById("weekdays").disabled = true;
            }
            else
            {
                if (p1 === "grid")
                {
                    document.getElementById('ClearGridandScheduler').click();
                }
                else
                {
                    document.getElementById("calendar").disabled = false;
                    document.getElementById("weekdays").disabled = false;
                    document.getElementById("timeonly").disabled = false;
                    document.getElementById("Exclude").disabled = true;
                }

            }
        },


        _AddEventstoGrid: function AddEventstoGrid()
        {
            // days of the week code
            thisIndex._Validate();
            var _StartTime = $("#FromTimeObject").val();
            var _EndTime = $("#ToTimeObject").val();
            var _StartTimeEndTime;
            var checkboxes = document.getElementsByClassName("checkboxDays");

            var _StartDateEndDate;
            var _starttimeId;
            var _endtimeId;

            var _startTimeTimeOnlyId;
            var _endTimeTimeOnlyId;


            _starttimeId = parseInt($("#FromTimeObject").val()) * 60;
            _endtimeId = parseInt($("#ToTimeObject").val()) * 60;

            _StartTimeEndTime = _StartTime + "-" + _EndTime;

            if (_StartTimeEndTime === "00-00")
            {
                _StartTimeEndTime = "H24";
                _endtimeId = (23 * 60) + 59;
            }


            _startTimeTimeOnlyId = _starttimeId
            _endTimeTimeOnlyId = _endtimeId;

            _starttimeId = _starttimeId.toString();
            _endtimeId = _endtimeId.toString();


            // DT Time
            var _StartTimeDT = $("#FromTimeObjectDT").val();
            var _EndTimeDT = $("#ToTimeObjectDT").val();
            var _StartTimeDTEndTimeDT;
          
            var _starttimeDTId;
            var _endtimeDTId;

            var _startTimeDTTimeOnlyId;
            var _endTimeDTTimeOnlyId;


            _starttimeDTId = parseInt($("#FromTimeObjectDT").val()) * 60;
            _endtimeDTId = parseInt($("#ToTimeObjectDT").val()) * 60;

            _StartTimeDTEndTimeDT = _StartTimeDT + "-" + _EndTimeDT;

            if (_StartTimeDTEndTimeDT === "00-00")
            {
                _StartTimeDTEndTimeDT = "H24";
                _endtimeDTId = (23 * 60) + 59;
            }


            _startTimeDTTimeOnlyId = _starttimeId
            _endTimeTimeOnlyId = _endtimeDTId;

            _starttimeDTId = _starttimeDTId.toString();
            _endtimeDTId = _endtimeDTId.toString();      


            if (document.getElementById('timeonly').checked && _StartTimeEndTime !== "")
            {
                check = thisIndex._checkforduplicates("Include", "0", "1", "DLY", _starttimeId, _endtimeId);
                if (check === "success")
                {
                    if (thisIndex._checkforContinuity("TimeOnly", "0", "1", "Include", "DLY", _StartTimeEndTime,_StartTimeDTEndTimeDT, _starttimeId, _endtimeId) === "0")
                    {
                        MydataSource.add({
                            RadioButtonSelectionWCT: "TimeOnly",
                            DateId: "0",
                            EndDateId: "1",
                            TimeId: _starttimeId,
                            EndTimeId: _endtimeId,
                            ContinuesEvent: false,
                            IncludeorExclude: "Include",
                            StartDateEndDate: "DLY",
                            StartTimeEndTime: _StartTimeEndTime,
                            StartTimeDTEndTimeDT: ((document.getElementById('DT').checked === true) ? ((_StartTimeDTEndTimeDT === "H24") ? "" :  _StartTimeDTEndTimeDT ) : "")

                                
                        });
                    }
                }
                else
                {
                    thisIndex._Validate();
                    check = "success";
                    //alert("Overlapping Events or more than 7 day gap are not allowed");
                }
            }

            if (document.getElementById('weekdays').checked)
            {
                for (var i = 0, l = checkboxes.length; i < l; ++i)
                {
                    if (parseInt(_endtimeId) <= parseInt(_starttimeId))
                    {
                        _endtimeId = (parseInt(_endtimeId) + ((parseInt(checkboxes[i].id) + 1) * 24 * 60) + (24 * 60)).toString();
                    }
                    else
                    {
                        _endtimeId = (parseInt(_endtimeId) + (parseInt(checkboxes[i].id) * 24 * 60)).toString();
                    }

                    _starttimeId = (parseInt(_starttimeId) + (parseInt(checkboxes[i].id) * 24 * 60)).toString();

                    // Get value of each selected checkbox and build the query string
                    if (checkboxes[i].checked)
                    {
                        check = thisIndex._checkforduplicates("Include", checkboxes[i].id.toString(), checkboxes[i].id.toString(), checkboxes[i].name, _starttimeId, _endtimeId);
                        if (check === "success")
                        {
                            if (thisIndex._checkforContinuity("WeekDays", checkboxes[i].id.toString(), checkboxes[i].id.toString(), "Include", _StartDateEndDate, _StartTimeEndTime,_StartTimeDTEndTimeDT, _starttimeId, _endtimeId) === "0")
                            {
                                MydataSource.add({
                                    RadioButtonSelectionWCT: "WeekDays",
                                    DateId: checkboxes[i].id.toString(),
                                    EndDateId: checkboxes[i].id.toString(),
                                    TimeId: _starttimeId,
                                    EndTimeId: _endtimeId,
                                    ContinuesEvent: false,
                                    IncludeorExclude: "Include",
                                    StartDateEndDate: checkboxes[i].name,
                                    StartTimeEndTime: _StartTimeEndTime,
                                    StartTimeDTEndTimeDT: ((document.getElementById('DT').checked === true) ? ((_StartTimeDTEndTimeDT === "H24") ? "" : _StartTimeDTEndTimeDT) : "")
                                });
                            }
                        }
                        else
                        {
                            thisIndex._Validate();
                            check = "success";
                        }

                        thisIndex._checkforDateContinuity();
                    }
                }
            }
            thisIndex._SortGrid();
        }

    };
});
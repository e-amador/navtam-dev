﻿namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    using System.Data.Entity;

    using Contracts.Repositories;
    using Domain.Model.Entitities;

    public class GroupRepo : Repository<Group>, IGroupRepo
    {
        public GroupRepo(DbContext dbContext)
            : base(dbContext)
        {
        }
    }
}

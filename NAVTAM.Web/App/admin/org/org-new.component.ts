﻿import { Component, Inject, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms'
import { WindowRef } from '../../common/windowRef.service'
import { Select2OptionData } from 'ng2-select2';
import { LocaleService, TranslationService } from 'angular-l10n';
import { MemoryStorageService } from '../shared/mem-storage.service'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';
import { PhoneValidator } from '../shared/phone.validator'
import { DataService } from '../shared/data.service'
import { IOrganization, IDoaPartial, INsdPartial } from '../shared/data.model';

@Component({ 
    templateUrl: '/app/admin/org/org-new.component.html'
})
export class OrgNewComponent implements OnInit  {
    
    model: any = null;
    action: string;

    orgForm: FormGroup; 

    isReadOnly: boolean;

    public isOrgNameUnique: boolean = true;
    public isValidatingOrg: boolean = false;

    public isAdminNameUnique: boolean = true;
    public isValidatingAdmin: boolean = false;
    public isSubmitting: boolean = false;

    public orgTypeOptions: Select2Options;
    public orgTypes: Array<Select2OptionData>;
    public orgType?: number;

    public doaOptions: Select2Options;
    public selectedDoaId?: number;
    public doas: Array<Select2OptionData>;

    public nsdOptions: Select2Options;
    public selectedNsdIds?: number[];
    public nsds: Array<Select2OptionData>;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private winRef: WindowRef,
        public locale: LocaleService,
        public translation: TranslationService) {

        if (this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);
    }

    ngOnInit() {
        this.orgTypeOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('OrgSession.SelectDoa')
            },
            multiple: false,
            width: "100%"
        };

        this.doaOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('OrgSession.SelectDoa')
            },
            multiple: true,
            width: "100%"
        };

        this.nsdOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('OrgSession.SelectNsds')
            },
            multiple: true,
            width: "100%"
        };

        this.orgTypes = [
            {
                id: '0',
                text: this.translation.translate('OrgSession.OrgTypeExternal')
            },
            {
                id: '1',
                text: this.translation.translate('OrgSession.OrgTypeFic')
            },
            {
                id: '2',
                text: this.translation.translate('OrgSession.OrgTypeNof')
            },
            {
                id: '3',
                text: this.translation.translate('OrgSession.OrgTypeAdministration')
            }
        ];

        this.model = this.prepareEmptyOrg();

        this.configureReactiveForm();

        this.loadDoas();
        this.loadNsds();
    }

    onOrgTypeChanged(data: { value: string }) {
        if (data.value && data.value.length > 0) {
            this.orgForm.get('orgType').setValue(data.value);
        } else {
            this.orgForm.get('orgType').setValue("");
        }
    }

    onDoaChanged(data: { value: string[] }) {
        if (data.value && data.value.length > 0) {
            this.orgForm.get('doas').setValue(data.value.join(','));
        } else {
            this.orgForm.get('doas').setValue("");
        }
    }

    onNsdChanged(data: { value: string[] }) {
        if (data.value && data.value.length > 0) {
            this.orgForm.get('nsds').setValue(data.value.join(','));
        } else {
            this.orgForm.get('nsds').setValue("");
        }
    }

    backToOrgList() {
        this.router.navigate(['/administration/orgs']);
    }

    onSubmit() {
        if (this.isSubmitting) return;
        this.isSubmitting = true;
        const org = this.prepareSaveOrg();
        this.dataService.saveOrg(org)
            .subscribe(data => {
                let msg = this.translation.translate('OrgSession.SuccessOrgSaved');
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                this.router.navigate(["/administration/orgs"]);
            }, error => {
                this.isSubmitting = false;
                let msg = this.translation.translate('OrgSession.FailureOrgSaved');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    validateName() {
        const usernameCtrl = this.orgForm.get("name");
        return usernameCtrl.valid || this.orgForm.pristine;
    }

    validateEmailAddress() {
        const emailCtrl = this.orgForm.get("emailAddress");
        return emailCtrl.valid || this.orgForm.pristine;
    }

    validateAddress() {
        const emailCtrl = this.orgForm.get("address");
        return emailCtrl.valid || this.orgForm.pristine;
    }

    validateTelephone() {
        const emailCtrl = this.orgForm.get("telephone");
        return emailCtrl.valid || this.orgForm.pristine;
    }

    validateTypeOfOperation() {
        const typeOfOperationCtrl = this.orgForm.get("typeOfOperation");
        return typeOfOperationCtrl.valid || this.orgForm.pristine;
    }

    validateDoas() {
        const fc = this.orgForm.get('doas');
        return fc.value || this.orgForm.pristine;
    }

    validateNsds() {
        const fc = this.orgForm.get('nsds');
        return fc.value || this.orgForm.pristine;
    }

    validateUsername() {
        const usernameCtrl = this.orgForm.get("username");
        return usernameCtrl.valid || this.orgForm.pristine;
    }

    validateFirstName() {
        const firstNameCtrl = this.orgForm.get("firstName");
        return firstNameCtrl.valid || this.orgForm.pristine;
    }

    validateLastName() {
        const lastNameCtrl = this.orgForm.get("lastName");
        return lastNameCtrl.valid || this.orgForm.pristine;
    }

    disableSubmit(): boolean {
        if (this.orgForm.pristine || !this.orgForm.valid)
            return true;
        if (this.isValidatingOrg)
            return true;
        if (this.isSubmitting) return true;
        return !this.isOrgNameUnique || !this.isAdminNameUnique;
    }

    private loadDoas(): void {
        this.dataService.getDoas()
            .subscribe((doa: IDoaPartial[]) => {
                this.doas = doa.map(
                    function (r: any) {
                        return <Select2OptionData>{
                            id: r.id,
                            text: r.name
                        };
                    });
                this.selectedDoaId = this.model.assignedDoaId || -1;
            });
    }

    private loadNsds(): void {
        this.dataService.getNsdLeaves()
            .subscribe((nsds: INsdPartial[]) => {
                this.nsds = nsds.map(
                    function (r: any) {
                        return <Select2OptionData>{
                            id: r.id,
                            text: r.name
                        };
                    });
                this.selectedDoaId = -1;
            });
    }

    private validateOrgNameUnique() {
        if (this.isValidatingOrg) return;
        if (this.validateName()) {
            const orgName = this.$ctrl('name');
            if (orgName.value) {
                if (orgName.value.length >= 3) {
                    this.isValidatingOrg = true;
                    this.dataService.isOrgNameUnique(0, orgName.value)
                        .finally(() => {
                            this.isValidatingOrg = false;
                        })
                        .subscribe((resp: boolean) => {
                            this.isOrgNameUnique = resp;
                        });
                }
            }
        }
    }

    private validateOrgAdminNameUnique() {
        if (this.isValidatingAdmin) return;
        if (this.validateUsername()) {
            const adminName = this.$ctrl('username');
            if (adminName.value) {
                this.isValidatingAdmin = true;
                this.dataService.isOrgAdminNameUnique(adminName.value)
                    .finally(() => {
                        this.isValidatingAdmin = false;
                    })
                    .subscribe((resp: boolean) => {
                        this.isAdminNameUnique = resp;
                    });
            }
        }
    }

    private $ctrl(name: string): AbstractControl {
        return this.orgForm.get(name);
    }

    private configureReactiveForm() {
        this.orgForm = this.fb.group({
            // organization
            name: [{ value: this.model.name, disabled: false }, [Validators.required, Validators.minLength(3), Validators.maxLength(100), Validators.pattern('[a-zA-Z].*')]],
            address: [{ value: this.model.address, disabled: false }, [Validators.required]],
            emailAddress: [{ value: this.model.emailAddress, disabled: false }, [Validators.required, Validators.pattern("[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}")]],
            headOffice: [{ value: this.model.headOffice, disabled: false }, []],
            typeOfOperation: [{ value: this.model.typeOfOperation, disabled: false }, [Validators.required]],
            emailDistribution: [{ value: this.model.emailDistribution, disabled: false }, []],
            doas: [{ value: this.model.assignedDoaIds || -1, disabled: false }, [doasValidator]],
            nsds: [{ value: this.model.assignedNsdIds || -1, disabled: false }, [nsdsValidator]],
            telephone: [{ value: this.model.telephone, disabled: false }, [phoneNumberValidator]],
            orgType: { value: this.model.type, disabled: false },

            // administrator
            username: [{ value: this.model.username, disabled: false }, [Validators.required, Validators.minLength(3), Validators.maxLength(16), Validators.pattern('[a-zA-Z].*')]],
            firstName: [{ value: this.model.firstName, disabled: false }, [Validators.required]],
            lastName: [{ value: this.model.lastName, disabled: false }, [Validators.required]],
        });

        this.$ctrl("name").valueChanges.debounceTime(1000).subscribe(() => this.validateOrgNameUnique());
        this.$ctrl("username").valueChanges.debounceTime(1000).subscribe(() => this.validateOrgAdminNameUnique());
    }

    phoneNumberFocus() {
        const phoneCtrl = this.$ctrl("telephone");
        const value = PhoneValidator.stripFormat(phoneCtrl.value);
        phoneCtrl.setValue(value);
    }

    phoneNumberBlur() {
        const phoneCtrl = this.$ctrl("telephone");
        const value = PhoneValidator.formatTenDigits(phoneCtrl.value);
        phoneCtrl.setValue(value);
    }

    private prepareSaveOrg(): IOrganization {
        const org: IOrganization = {
            name: this.orgForm.get('name').value,
            address: this.orgForm.get('address').value,
            emailAddress: this.orgForm.get('emailAddress').value,
            emailDistribution: this.orgForm.get('emailDistribution').value,
            headOffice: this.orgForm.get('headOffice').value,
            telephone: this.orgForm.get('telephone').value,
            typeOfOperation: this.orgForm.get('typeOfOperation').value,
            type: this.parseOrgType(this.orgForm.get('orgType').value),
            assignedDoaIds: this.parseIds(this.orgForm.get('doas').value),
            assignedNsdIds: this.parseIds(this.orgForm.get('nsds').value),
            adminUserName: this.orgForm.get('username').value,
            adminFirstName: this.orgForm.get('firstName').value,
            adminLastName: this.orgForm.get('lastName').value,
        }
        return org;
    }

    private parseOrgType(value: string): number {
        return +(value || 0);
    }

    private parseIds(value: string): number[] {
        return (value || "").split(",").map(v => +v);
    }

    private prepareEmptyOrg(): IOrganization {
        const org: IOrganization = {
            id: null,
            name: "",
            address: "",
            emailAddress: "",
            emailDistribution: "",
            telephone: "",
            typeOfOperation: "",
            isReadOnlyOrg: false,
            type: 0,
            headOffice: ""
        }

        return org;
    }
}

function doasValidator(c: AbstractControl): { [key: string]: boolean } | null {
    if (!c.value) {
        return { 'required': true }
    }
    return null;
}

function nsdsValidator(c: AbstractControl): { [key: string]: boolean } | null {
    if (!c.value) {
        return { 'required': true }
    }
    return null;
}

function phoneNumberValidator(c: AbstractControl): { [key: string]: boolean } | null {
    const value = c.value;
    if (!value)
        return { 'required': true }

    if (!PhoneValidator.isValidPhoneNumber(c.value, 10)) {
        return { 'pattern': true }
    }

    return null;
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NAVTAM.Helpers;

namespace NAVTAM.Web.Test.AFTN
{
    [TestClass]
    public class AFTNFormatTables
    {
        [TestMethod]
        public void FormatTable_FormatSquareBracketTables_test()
        {
            var source = "Before text\r\n[`Header 1` `Header 2` `Header 3` `Header Four`\n`1` `2` `3` `4`]\r\nAfter text";
            var expected = "Before text\r\nHeader 1 Header 2 Header 3 Header Four\r\n   1        2        3          4     \r\nAfter text";
            var formatted = TableFormatter.FormatSquareBracketTables(source);
            Assert.AreEqual(formatted, expected);
        }

        [TestMethod]
        public void FormatTable_Validate_test()
        {
            Assert.IsFalse(TableFormatter.ContainsValidTables("asasassa asasaas sasasa asasasa sq sasasas asasasasa"));
            Assert.IsTrue(TableFormatter.ContainsValidTables("asasassa[asasaas]sasasa[asasasa sq sasasas]asasasasa"));
            Assert.IsFalse(TableFormatter.ContainsValidTables("asasassa[asasaassasasa[asasasa sq sasasas]asasasasa"));
        }
    }
}

﻿using NavCanada.Core.Domain.Model.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NAVTAM.ViewModels
{
    public class OrganizationBaseViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        [EmailAddress()]
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }
        [Required]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$")]
        public string Telephone { get; set; }
        [Required]
        public string TypeOfOperation { get; set; }
        public string HeadOffice { get; set; }
        public string EmailDistribution { get; set; }
        public List<int> AssignedDoaIds { get; set; }
        public List<int> AssignedNsdIds { get; set; }
        public OrganizationType Type { get; set; }
    }

    public class OrganizationCreateViewModel : OrganizationBaseViewModel
    {
        [Required]
        public string AdminUserName { get; set; }
        [Required]
        public string AdminFirstName { get; set; }
        [Required]
        public string AdminLastName { get; set; }
        //[Required]
        //[DataType(DataType.Password)]
        //public string AdminPassword { get; set; }
        //[Required]
        //[DataType(DataType.Password)]
        //public string AdminConfirmPassword { get; set; }
    }

    public class OrganizationUpdateViewModel : OrganizationBaseViewModel
    {
        [Required]
        public int Id { get; set; }
    }
}
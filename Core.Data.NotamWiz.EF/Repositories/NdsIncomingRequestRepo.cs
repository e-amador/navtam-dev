﻿using NavCanada.Core.Data.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace NavCanada.Core.Data.EF.Repositories
{
    class NdsIncomingRequestRepo : Repository<NdsIncomingRequest>, INdsIncomingRequestRepo
    {
        public NdsIncomingRequestRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public int GetCount() => DbSet.Count();        

        public List<NdsIncomingRequest> GetLatestMessages(int max) => DbSet.OrderByDescending(e => e.RecievedDate).Take(max).ToList();
    }
}

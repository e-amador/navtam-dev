﻿using AutoMapper;
using NavCanada.SE.NOTAMWiz.Models;
using NavCanada.SE.NOTAMWiz.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NavCanada.SE.NotamWiz.UI.Wiz.Web.Models;

namespace NavCanada.SE.NOTAMWiz.App_Start.AutoMapperResolvers
{
    public class TempSubject2VMConvertor : ITypeConverter<TempSubject, DashboardItemViewModel>
    {

        public DashboardItemViewModel Convert(ResolutionContext context)
        {
            TempSubject d = context.SourceValue as TempSubject;

            DashboardItemViewModel dvm = new DashboardItemViewModel()
            {
                Name = d.Name,
                HasChildren = false,
                Type = enDashboardItemTypes.TemporarySubject,
                SubjectType = d.Type,
                SubjectId = d.Id.ToString()

            };


            return dvm;
        }

    }
}

﻿namespace RSC.Model
{
    /// <summary>
    /// Contaminant height/depth as Trace or inches or feet
    /// </summary>
    public class ContamDepthFtIn
    {
        /// <summary>
        /// Pattern for just a TRACE of the contaminant.
        /// </summary>
        public bool ContamTrace { get; set; }
        /// <summary>
        ///  Restrictions for Depth of Contamination in inches (with value 1.5 for Contaminants in Cleared Portion)
        /// </summary>
        public decimal ContamInches { get; set; }
        /// <summary>
        /// Restrictions for Depth of Contamination in feet
        /// </summary>
        public int ContamFeet { get; set; }
    }
}
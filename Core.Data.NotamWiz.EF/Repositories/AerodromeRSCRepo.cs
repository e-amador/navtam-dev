﻿using System.Data.Entity;
using System.Linq;
using NavCanada.Core.Data.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Domain.Model.Entitities;

namespace NavCanada.Core.Data.EF.Repositories
{
    class AerodromeRSCRepo : Repository<AerodromeRSC>, IAerodromeRSCRepo
    {
        public AerodromeRSCRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public AerodromeRSC GetByDesignator(string designator) => DbSet.FirstOrDefault(e => e.Designator == designator);
    }
}

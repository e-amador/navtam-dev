﻿namespace NavCanada.Core.NotificationEngine
{
    public interface INotifier
    {
        NotifierResponse NotifyByEmail(string recipients, string subject, string message);
    }
}

﻿using System;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class AeroRDSCache
    {
        public string Id { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public string Cache { get; set; }
    }
}

﻿import { Component, OnInit, OnDestroy, AfterViewInit, HostListener, Input, ChangeDetectorRef } from '@angular/core'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators } from '@angular/forms'
import { NsdFormComponent } from '../nsds/nsd-form.component';

import { DataService } from '../shared/data.service';
import { ProposalStatus, ProposalType } from '../shared/data.model';

import * as moment from 'moment';

declare var $: any;

@Component({
    selector: 'scheduler-freetext',
    templateUrl: '/app/dashboard/nsds/nsd-scheduler-free-text.component.html'
})
export class NsdSchedulerFreeTextComponent implements OnInit, AfterViewInit, OnDestroy {

    public parent: NsdFormComponent = null;
    @Input('form') form: FormGroup;
    @Input('model') model: any;

    isDisabled: boolean = false;
    itemD: string = "";   

    start: any = null;
    end: any = null;
    starttime: any = null;
    endtime: any = null;
    validationResult: boolean = true;
    overridden: boolean = false;
    parserError: string = "";

    constructor(private cdRef: ChangeDetectorRef, private dataService: DataService) {
    }


    ngOnInit() {
    }

    ngOnDestroy() {
    }


    ngAfterViewInit() {
        let self = this;
        $('.ft-datepicker').each(function () {
            $(this).bootstrapDatepicker({
                autoclose: $(this).data('autoclose') ? $(this).data('autoclose') : false,
                todayHighlight: $(this).data('today-highlight') ? $(this).data('today-highlight') : true,
                orientation: $(this).data('orientation') ? $(this).data('orientation') : "top",
                startView: $(this).data('view') ? $(this).data('view') : 0, // 0: month view , 1: year view, 2: multiple year view
                language: $(this).data('lang') ? $(this).data('lang') : "en",
                forceParse: $(this).data('parse') ? $(this).data('parse') : false,
                daysOfWeekDisabled: $(this).data('day-disabled') ? $(this).data('day-disabled') : "", // Disable 1 or various day. For monday and thursday: 1,3
                calendarWeeks: $(this).data('calendar-week') ? $(this).data('calendar-week') : false, // Display week number 
                toggleActive: $(this).data('toggle-active') ? $(this).data('toggle-active') : true, // Close other when open
                multidate: $(this).data('multidate') ? $(this).data('multidate') : false, // Allow to select various days

            }).on('changeDate', function (e) {
                self.dateChanged(e);
                $(this).bootstrapDatepicker('hide');
            });
        });

        $('#ft-starttime').mask('00:00');
        $('#ft-endtime').mask('00:00');
    }

    initialize(data, start, end) {
        if (data.length === 1) {
            let obj = JSON.parse(data[0].event);

            this.itemD = obj.itemD;

            start = moment(start);
            end = moment(end);

            this.start = new Date(start.format('YYYY'), start.format('MM') - 1, start.format('DD'));
            this.end = new Date(end.format('YYYY'), end.format('MM') - 1, end.format('DD'));

            this.starttime = start.format("HH:mm");
            this.endtime = end.format("HH:mm");

            $('#ft-start').bootstrapDatepicker('update', this.start);
            $('#ft-end').bootstrapDatepicker('update', this.end);

            this.isDisabled = false;
        }
    }

    setDate(start, end, starttime, endtime) {
        if (start) {
            start = typeof (start) === 'string' ? moment(start, "MM/DD/YYYY") : moment(start);
        }
        if (end) {
            end = typeof (end) === 'string' ? moment(end, "MM/DD/YYYY") : moment(end);
        }

        this.start = new Date(start.format('YYYY'), start.format('MM') - 1, start.format('DD'));
        this.end = new Date(end.format('YYYY'), end.format('MM') - 1, end.format('DD'));
        this.starttime = starttime;
        this.endtime = endtime;

        let startElem = $('#ft-start');
        startElem.bootstrapDatepicker('update', this.start);
        startElem.prop('disabled', true);

        let endElem = $('#ft-end');
        endElem.bootstrapDatepicker('update', this.end);
        endElem.prop('disabled', true);

        this.isDisabled = true;
    }

    dateChanged(ev) {
        switch (ev.currentTarget.id) {
            case "ft-start":
                this.start = moment.utc(ev.currentTarget.value, "MM/DD/YYYY");
                if (this.end && this.end.isValid()) {
                    let el = $('#ft-start');
                    if (this.start.isAfter(this.end)) {
                        this.start = null;
                        var x = el.bootstrapDatepicker('getDate');
                        el.bootstrapDatepicker('update', '');
                        el.css({
                            "border-color": "red",
                            "border-width": "1px",
                            "border-style": "solid"
                        });
                    } else {
                        el.css({
                            "border-color": "white",
                            "border-width": "0px",
                        });
                    }
                }
                break;
            case "ft-end":
                this.end = moment.utc(ev.currentTarget.value, "MM/DD/YYYY");
                if (this.start && this.start.isValid()) {
                    let el = $('#w-end');
                    if (this.start.isAfter(this.end)) {
                        this.end = null;
                        el.bootstrapDatepicker('update', '');
                        el.css({
                            "border-color": "red",
                            "border-width": "1px",
                            "border-style": "solid"
                        });
                    } else {
                        el.css({
                            "border-color": "white",
                            "border-width": "0px",
                        });
                    }
                }
                break;
        }
        $(this).datepicker('hide');
    }

    getFreeText() {
        return this.itemD;
    }

    setFreeText(itemD: string) {
        this.itemD = itemD.toUpperCase();
    }

    validate() {
        this.dataService.validateItemD(this.itemD)
            .subscribe(response => {
                this.parserError = "";
                switch (response.case) {
                    case "Success":
                        this.validationResult = true;
                        this.parent.updateFormControlValue("itemD", this.itemD);
                        this.parent.nsdForm.markAsDirty();
                        break;
                    case "Failure":                        
                        this.validationResult = false;
                        this.parserError = response.errorMessage;
                        break;
                }
            }, error => {
                //todo
            });
    }

    apply() {
        let start = this.setMomentDateTime(this.start, this.starttime.split(':'), 'DD/MM/YYYY HH:mm');
        let end = this.setMomentDateTime(this.end, this.endtime.split(':'), 'DD/MM/YYYY HH:mm');

        this.parent.disableActivePeridDates(start, end, this.itemD);
        this.parent.isScheduleOverridden = true;
    }

    isValid() {
        return this.checkValidity();
    }

    override() {
        this.overridden = true;
        if (this.parent.hasScheduler) {
            let self = this;
            let itemD = this.itemD;
            this.parent.clearSchedule(null, function (actionResult) {
                if (actionResult) {
                    self.itemD = itemD;
                    self.isDisabled = false;
                    self.parent.hasScheduler = false;
                    self.parent.isScheduleOverridden = true;
                    self.parent.updateFormControlValue("itemD", self.itemD);
                    self.parent.nsdForm.markAsDirty();
                    $('#ft-start').prop('disabled', false);
                    $('#ft-end').prop('disabled', false);
                }
            });
        } else {
            this.isDisabled = false;
            this.parent.hasScheduler = false;
            this.parent.updateFormControlValue("itemD", this.itemD);
            this.parent.nsdForm.markAsDirty();
            this.parent.isScheduleOverridden = true;
            $('#ft-start').prop('disabled', false);
            $('#ft-end').prop('disabled', false);
        }
    }

    isClearButtonDisabled() {
        if (this.parent && this.parent.isReadOnly) {
            return true;
        }

        return !this.checkValidity();
    }

    clear() {
        this.clearFormControls();
        this.parent.enableActivePeridDates();
        this.parent.resetSchedulersStatus();

        this.parent.schedulerDaily.removeEvents();
        this.parent.schedulerDayWeeks.removeEvents();
        this.parent.schedulerDate.removeEvents();
    }

    clearFormControls() {
        this.start = null;
        this.end = null;
        this.starttime = "";
        this.itemD = "";
        this.endtime = "";

        let startElem = $('#ft-start');
        startElem.bootstrapDatepicker('update', null);
        startElem.prop('disabled', false);

        let endElem = $('#ft-end');
        endElem.bootstrapDatepicker('update', null);
        endElem.prop('disabled', false);

        this.isDisabled = false;
    }

    private setMomentDateTime(date, time, format) {
        let day = date.format('DD');
        let month = date.format('MM');
        let year = date.format('YYYY');

        let hour = time[0];
        let minute = time[1];

        return moment.utc(`${day}-${month}-${year} ${hour}:${minute}`, 'DD-MM-YYYY HH:mm').format(format);
    }

    private checkValidity() {
        if (this.parent && this.parent.hasScheduler && this.parent.schedulerTab !== 4) {
            return false;
        }

        let start = moment($('#ft-start').bootstrapDatepicker('getDate'));
        let end = moment($('#ft-end').bootstrapDatepicker('getDate'));

        this.start = start.isValid() ? start : null;
        this.end = end.isValid() ? end : null;

        if (this.start && this.starttime && this.end && this.endtime) {
            return this.itemD.length > 1;
        }

        return false;
    }
}

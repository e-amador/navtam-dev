#region Using directives

using AppenderSkeleton = log4net.Appender.AppenderSkeleton;
using FixFlags = log4net.Core.FixFlags;
using LoggingEvent = log4net.Core.LoggingEvent;
using LogManager = log4net.LogManager;

#endregion


namespace NavCanada.Applications.NotamWiz.Web.log4net.SignalR
{
    using System;

    using Microsoft.AspNet.SignalR.Client;

    public class SignalrAppender : AppenderSkeleton
    {
        public Action<LogEntry> MessageLogged;
        private FixFlags _fixFlags = FixFlags.All;

        private string _proxyUrl = "";
        private IHubProxy proxyConnection;

        public SignalrAppender()
        {
            System.Diagnostics.Debug.WriteLine("Instantiating");
            LocalInstance = this;
        }

        public static SignalrAppender LocalInstance { get; private set; }

        public string ProxyUrl
        {
            get { return this._proxyUrl; }
            set
            {
                if (value != "")
                {
                    HubConnection connection = new HubConnection(value);
                    this.proxyConnection = connection.CreateHubProxy("signalrAppenderHub");
                    connection.Start().Wait();
                }
                else
                {
                    this.proxyConnection = null;
                }
                this._proxyUrl = value;
            }
        }

        public virtual FixFlags Fix
        {
            get { return this._fixFlags; }
            set { this._fixFlags = value; }
        }

        protected override void Append(LoggingEvent loggingEvent)
        {
            // LoggingEvent may be used beyond the lifetime of the Append()
            // so we must fix any volatile data in the event
            loggingEvent.Fix = Fix;

            var formattedEvent = RenderLoggingEvent(loggingEvent);

            var logEntry = new LogEntry(formattedEvent, new JsonLoggingEventData(loggingEvent));

            if (this.proxyConnection != null)
            {
                ProxyOnMessageLogged(logEntry);
            }
            else if (this.MessageLogged != null)
            {
                this.MessageLogged(logEntry);
            }
        }

        private void ProxyOnMessageLogged(LogEntry entry)
        {
            try
            {
                this.proxyConnection.Invoke("OnMessageLogged", entry);
            }
            catch (Exception e)
            {
                LogManager.GetLogger("").Warn("OnMessageLogged Failed:", e);
            }
        }
    }


    public class LogEntry
    {
        public LogEntry(string formttedEvent, JsonLoggingEventData loggingEvent)
        {
            FormattedEvent = formttedEvent;
            LoggingEvent = loggingEvent;
        }

        public string FormattedEvent { get; set; }
        public JsonLoggingEventData LoggingEvent { get; set; }
    }
}
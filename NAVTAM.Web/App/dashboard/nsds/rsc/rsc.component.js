"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RscComponent = void 0;
var forms_1 = require("@angular/forms");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var windowRef_service_1 = require("../../../common/windowRef.service");
var mem_storage_service_1 = require("../../shared/mem-storage.service");
var toastr_service_1 = require("./../../../common/toastr.service");
var location_service_1 = require("../../shared/location.service");
var rsc_service_1 = require("./rsc.service");
var nsd_buttonbar_component_1 = require("../../nsds/nsd-buttonbar.component");
var nsd_georeftool_component_1 = require("../../nsds/nsd-georeftool.component");
var data_model_1 = require("../../shared/data.model");
var data_service_1 = require("../../shared/data.service");
var angular_l10n_1 = require("angular-l10n");
var phone_validator_1 = require("../../../admin/shared/phone.validator");
var rsc_model_1 = require("./rsc.model");
var rsc_clearedwidth_component_1 = require("./components/rsc-clearedwidth.component");
var rsc_contaminants_component_1 = require("./components/rsc-contaminants.component");
var rsc_rwyfriction_component_1 = require("./components/rsc-rwyfriction.component");
var rsc_remainingwidthremarks_component_1 = require("./components/rsc-remainingwidthremarks.component");
var rsc_clearingoperations_component_1 = require("./components/rsc-clearingoperations.component");
var rsc_snowbanks_component_1 = require("./components/rsc-snowbanks.component");
var rsc_treatments_component_1 = require("./components/rsc-treatments.component");
var rsc_windrows_component_1 = require("./components/rsc-windrows.component");
var rsc_taxiwaysremarks_component_1 = require("./components/rsc-taxiwaysremarks.component");
var rsc_apronremarks_component_1 = require("./components/rsc-apronremarks.component");
var rsc_nextobservation_component_1 = require("./components/rsc-nextobservation.component");
var moment = require("moment");
var RscComponent = /** @class */ (function () {
    function RscComponent(toastr, fb, dataService, activatedRoute, windowRef, memStorageService, router, locationService, rscService, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.activatedRoute = activatedRoute;
        this.windowRef = windowRef;
        this.memStorageService = memStorageService;
        this.router = router;
        this.locationService = locationService;
        this.rscService = rscService;
        this.translation = translation;
        this.isNOF = false;
        this.model = null;
        this.token = null;
        this.isReadOnly = false;
        this.tabActive = 1;
        this.culture = 'en';
        this.isBilingualRegion = false;
        this.isFormLoadingData = false;
        //End enum definitions
        this.subjectSelected = null;
        this.subjectId = '';
        this.subjects = [];
        //Data definitions
        this.runwaysRSCData = [];
        this.runwayFrictionData = null;
        this.nextObservationData = null;
        this.remainingWidthData = null;
        this.windrowsRemarksData = null;
        this.runwaySelected = null;
        this.runwayId = '-1';
        this.runways = [];
        //End Data definitions
        this.runwayComboList = [];
        this.maintenance = false;
        this.grfValue = true;
        this.waitingForGeneratingIcao = false;
        this.isSaveButtonDisabled = true;
        this.isSubmitButtonDisabled = true;
        this.isWaitingForResponse = false;
        this.isRunwaySelectorDisabled = true;
        this.mapHealthy = false;
        this.componentData = null;
        this.observationDateText = "";
        this.dateFormat = "YYMMDDHHmm";
        this.runwaysOptions = {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('RSC.SelectRunway')
            }
        };
    }
    RscComponent.prototype.ngOnInit = function () {
        window['loadLeafletMapScript'].call(this, null);
        this.model = this.activatedRoute.snapshot.data['model'];
        this.observationDateMoment = moment.utc();
        this.model.canReject = false;
        this.token = this.model.token;
        this.isFormLoadingData = !(!this.token.subjectId);
        this.isNOF = this.activatedRoute.snapshot.data[0]['isNOF'];
        this.leafletmap = this.rscService.app.leafletmap;
        this.culture = this.rscService.app.cultureInfo;
        //Definition of ENUMS
        this.contaminants = [
            { id: '00', text: this.translation.translate('ContaminantsComponent.SelectContaminant'), contaminatId: '00', needDepth: false, temperature: 0 },
            { id: '20', text: this.translation.translate('ContaminantWithNoDepth.Dry'), contaminatId: '20', needDepth: false, temperature: 0 },
            { id: '22', text: this.translation.translate('ContaminantWithNoDepth.Wet'), contaminatId: '22', needDepth: false, temperature: 0 },
            { id: '23', text: this.translation.translate('ContaminantWithNoDepth.Frost'), contaminatId: '23', needDepth: false, temperature: 0 },
            { id: '24', text: this.translation.translate('ContaminantWithNoDepth.Ice'), contaminatId: '24', needDepth: false, temperature: 0 },
            { id: '25', text: this.translation.translate('ContaminantWithNoDepth.CompactedSnow20'), contaminatId: '25', needDepth: false, temperature: -20 },
            { id: '26', text: this.translation.translate('ContaminantWithNoDepth.CompactedSnow10'), contaminatId: '25', needDepth: false, temperature: -10 },
            { id: '27', text: this.translation.translate('ContaminantWithNoDepth.WetIce'), contaminatId: '27', needDepth: false, temperature: 0 },
            { id: '29', text: this.translation.translate('ContaminantWithNoDepth.CompactedSnowGravelMix'), contaminatId: '29', needDepth: false, temperature: 0 },
            { id: '32', text: this.translation.translate('ContaminantWithNoDepth.SlipperyWhenWet'), contaminatId: '32', needDepth: false, temperature: 0 },
            { id: '60', text: this.translation.translate('ContaminantWithDepth.StandingWater'), contaminatId: '60', needDepth: true, temperature: 0 },
            { id: '61', text: this.translation.translate('ContaminantWithDepth.DrySnow'), contaminatId: '61', needDepth: true, temperature: 0 },
            { id: '62', text: this.translation.translate('ContaminantWithDepth.WetSnow'), contaminatId: '62', needDepth: true, temperature: 0 },
            { id: '63', text: this.translation.translate('ContaminantWithDepth.Slush'), contaminatId: '63', needDepth: true, temperature: 0 },
            { id: '65', text: this.translation.translate('ContaminantWithDepth.DrySnowOnTopOfIce'), contaminatId: '65', needDepth: true, temperature: 0 },
            { id: '66', text: this.translation.translate('ContaminantWithDepth.WetSnowOnTopOfIce'), contaminatId: '66', needDepth: true, temperature: 0 },
            { id: '67', text: this.translation.translate('ContaminantWithDepth.SlushOnTopOfIce'), contaminatId: '67', needDepth: true, temperature: 0 },
            { id: '68', text: this.translation.translate('ContaminantWithDepth.DrySnowOnTopOfCompactedSnow'), contaminatId: '68', needDepth: true, temperature: 0 },
            { id: '71', text: this.translation.translate('ContaminantWithDepth.WaterOnTopOfCompactedSnow'), contaminatId: '71', needDepth: true, temperature: 0 },
            { id: '72', text: this.translation.translate('ContaminantWithDepth.WetSnowOnTopOfCompactedSnow'), contaminatId: '72', needDepth: true, temperature: 0 },
        ];
        this.depths = [
            { id: '00.00', text: '0' },
            { id: '00.13', text: '1/8' },
            { id: '00.25', text: '1/4' },
            { id: '00.50', text: '1/2' },
            { id: '00.75', text: '3/4' },
            { id: '01.00', text: '1' },
            { id: '01.50', text: '1.5' },
            { id: '02.00', text: '2' },
            { id: '03.00', text: '3' },
            { id: '04.00', text: '4' },
            { id: '05.00', text: '5' },
            { id: '06.00', text: '6' },
            { id: '07.00', text: '7' },
            { id: '08.00', text: '8' },
            { id: '09.00', text: '9' },
            { id: '10.00', text: '10' },
            { id: '11.00', text: '11' },
            { id: '12.00', text: '12' },
            { id: '13.00', text: '13' },
            { id: '14.00', text: '14' },
            { id: '15.00', text: '15' },
            { id: '16.00', text: '16' },
            { id: '17.00', text: '17' },
            { id: '18.00', text: '18' },
            { id: '19.00', text: '19' },
            { id: '20.00', text: '20' },
            { id: '21.00', text: '21' },
            { id: '22.00', text: '22' },
            { id: '23.00', text: '23' },
            { id: '24.00', text: '24' },
            { id: '25.00', text: '25' },
            { id: '26.00', text: '26' },
            { id: '27.00', text: '27' },
            { id: '28.00', text: '28' },
            { id: '29.00', text: '29' },
            { id: '30.00', text: '30' },
            { id: '31.00', text: '31' },
            { id: '32.00', text: '32' },
            { id: '33.00', text: '33' },
            { id: '34.00', text: '34' },
            { id: '35.00', text: '35' },
            { id: '36.00', text: '36' },
            { id: '37.00', text: '37' },
            { id: '38.00', text: '38' },
            { id: '39.00', text: '39' },
            { id: '40.00', text: '40' },
            { id: '41.00', text: '41' },
            { id: '42.00', text: '42' },
            { id: '43.00', text: '43' },
            { id: '44.00', text: '44' },
            { id: '45.00', text: '45' },
            { id: '46.00', text: '46' },
            { id: '47.00', text: '47' },
            { id: '48.00', text: '48' },
            { id: '49.00', text: '49' },
            { id: '50.00', text: '50' },
            { id: '51.00', text: '51' },
            { id: '52.00', text: '52' },
            { id: '53.00', text: '53' },
            { id: '54.00', text: '54' },
            { id: '55.00', text: '55' },
            { id: '56.00', text: '56' },
            { id: '57.00', text: '57' },
            { id: '58.00', text: '58' },
            { id: '59.00', text: '59' },
            { id: '60.00', text: '60' }
        ];
        this.coefficients = [
            { id: '00', text: '.00' },
            { id: '01', text: '.01' },
            { id: '02', text: '.02' },
            { id: '03', text: '.03' },
            { id: '04', text: '.04' },
            { id: '05', text: '.05' },
            { id: '06', text: '.06' },
            { id: '07', text: '.07' },
            { id: '08', text: '.08' },
            { id: '09', text: '.09' },
            { id: '10', text: '.10' },
            { id: '11', text: '.11' },
            { id: '12', text: '.12' },
            { id: '13', text: '.13' },
            { id: '14', text: '.14' },
            { id: '15', text: '.15' },
            { id: '16', text: '.16' },
            { id: '17', text: '.17' },
            { id: '18', text: '.18' },
            { id: '19', text: '.19' },
            { id: '20', text: '.20' },
            { id: '21', text: '.21' },
            { id: '22', text: '.22' },
            { id: '23', text: '.23' },
            { id: '24', text: '.24' },
            { id: '25', text: '.25' },
            { id: '26', text: '.26' },
            { id: '27', text: '.27' },
            { id: '28', text: '.28' },
            { id: '29', text: '.29' },
            { id: '30', text: '.30' },
            { id: '31', text: '.31' },
            { id: '32', text: '.32' },
            { id: '33', text: '.33' },
            { id: '34', text: '.34' },
            { id: '35', text: '.35' },
            { id: '36', text: '.36' },
            { id: '37', text: '.37' },
            { id: '38', text: '.38' },
            { id: '39', text: '.39' },
            { id: '40', text: '.40' },
            { id: '41', text: '.41' },
            { id: '42', text: '.42' },
            { id: '43', text: '.43' },
            { id: '44', text: '.44' },
            { id: '45', text: '.45' },
            { id: '46', text: '.46' },
            { id: '47', text: '.47' },
            { id: '48', text: '.48' },
            { id: '49', text: '.49' },
            { id: '50', text: '.50' },
            { id: '51', text: '.51' },
            { id: '52', text: '.42' },
            { id: '53', text: '.53' },
            { id: '54', text: '.54' },
            { id: '55', text: '.55' },
            { id: '56', text: '.56' },
            { id: '57', text: '.57' },
            { id: '58', text: '.58' },
            { id: '59', text: '.59' },
            { id: '60', text: '.60' },
            { id: '61', text: '.61' },
            { id: '62', text: '.62' },
            { id: '63', text: '.63' },
            { id: '64', text: '.64' },
            { id: '65', text: '.65' },
            { id: '66', text: '.66' },
            { id: '67', text: '.67' },
            { id: '68', text: '.68' },
            { id: '69', text: '.69' },
            { id: '70', text: '.70' },
            { id: '71', text: '.71' },
            { id: '72', text: '.72' },
            { id: '73', text: '.73' },
            { id: '74', text: '.74' },
            { id: '75', text: '.75' },
            { id: '76', text: '.76' },
            { id: '77', text: '.77' },
            { id: '78', text: '.78' },
            { id: '79', text: '.79' },
            { id: '80', text: '.80' },
            { id: '81', text: '.81' },
            { id: '82', text: '.82' },
            { id: '83', text: '.83' },
            { id: '84', text: '.84' },
            { id: '85', text: '.85' },
            { id: '86', text: '.86' },
            { id: '87', text: '.87' },
            { id: '88', text: '.88' },
            { id: '89', text: '.89' },
            { id: '90', text: '.90' },
            { id: '91', text: '.91' },
            { id: '92', text: '.92' },
            { id: '93', text: '.93' },
            { id: '94', text: '.94' },
            { id: '95', text: '.95' },
            { id: '96', text: '.96' },
            { id: '97', text: '.97' },
            { id: '98', text: '.98' },
            { id: '99', text: '.99' }
        ];
        this.coverages = [
            { id: '010', text: '10' },
            { id: '020', text: '20' },
            { id: '025', text: '25' },
            { id: '030', text: '30' },
            { id: '040', text: '40' },
            { id: '050', text: '50' },
            { id: '060', text: '60' },
            { id: '070', text: '70' },
            { id: '075', text: '75' },
            { id: '080', text: '80' },
            { id: '090', text: '90' },
            { id: '100', text: '100' }
        ];
        this.conditionCodes = [
            { id: '0', text: '0' },
            { id: '1', text: '1' },
            { id: '2', text: '2' },
            { id: '3', text: '3' },
            { id: '4', text: '4' },
            { id: '5', text: '5' },
            { id: '6', text: '6' }
        ];
        this.conditionCodeAction = [
            { id: '0', text: this.translation.translate('ContaminantsComponent.ConfirmRWYCC') },
            { id: '1', text: this.translation.translate('ContaminantsComponent.DowngradeRWYCC') },
            { id: '2', text: this.translation.translate('ContaminantsComponent.UpgradeRWYCC') }
        ];
        this.unitOfMeasurement = [
            { id: '9', text: this.translation.translate('UnitOfMeasurement.Inches'), additional: null },
            { id: '10', text: this.translation.translate('UnitOfMeasurement.Feet'), additional: null }
        ];
        this.cardinalSideEnum = [
            { id: '250', text: 'E', additional: null },
            { id: '251', text: 'SE', additional: null },
            { id: '252', text: 'S', additional: null },
            { id: '253', text: (this.culture === 'en') ? 'SW' : 'SO', additional: null },
            { id: '254', text: (this.culture === 'en') ? 'W' : 'O', additional: null },
            { id: '255', text: (this.culture === 'en') ? 'NW' : 'NO', additional: null },
            { id: '256', text: 'N', additional: null },
            { id: '257', text: 'NE', additional: null },
        ];
        this.measurementDevice = [
            { id: '130', text: this.translation.translate('MeasurementDevice.TapleyMeter'), additional: null },
            { id: '131', text: this.translation.translate('MeasurementDevice.JamesDecelerometer'), additional: null },
            { id: '132', text: this.translation.translate('MeasurementDevice.Bowmonk'), additional: null },
            { id: '133', text: this.translation.translate('MeasurementDevice.DecelerometerTES'), additional: null },
        ];
        this.ocLocation = [
            { id: '127', text: this.translation.translate('OCLocationEnum.AlongInsideRedl'), additional: null },
            { id: '128', text: this.translation.translate('OCLocationEnum.AlongInsideRwyEdge'), additional: null },
            { id: '129', text: this.translation.translate('OCLocationEnum.AlongClearedWidth'), additional: null },
            { id: '134', text: this.translation.translate('OCLocationEnum.FMCL'), additional: null },
        ];
        this.offsetSide = [
            { id: '11', text: this.translation.translate('OffsetSide.East'), additional: null },
            { id: '12', text: this.translation.translate('OffsetSide.West'), additional: null },
            { id: '13', text: this.translation.translate('OffsetSide.North'), additional: null },
            { id: '14', text: this.translation.translate('OffsetSide.South'), additional: null },
        ];
        this.otherConditionsArea = [
            { id: '122', text: this.translation.translate('OtherConditionsArea.IcePatches'), additional: null },
            { id: '123', text: this.translation.translate('OtherConditionsArea.CompactedSnowPatches'), additional: null },
            { id: '124', text: this.translation.translate('OtherConditionsArea.StandingWaterPatches'), additional: null },
        ];
        this.otherConditionsEdge = [
            { id: '120', text: this.translation.translate('OtherConditionsEdge.SnowDrifts'), additional: null },
            { id: '121', text: this.translation.translate('OtherConditionsEdge.Windrows'), additional: null },
            { id: '122', text: this.translation.translate('OtherConditionsEdge.SnowBanks'), additional: null },
        ];
        this.treatment = [
            { id: '100', text: this.translation.translate('Treatment.LoseSand'), additional: null },
            { id: '104', text: this.translation.translate('Treatment.Graded'), additional: null },
            { id: '106', text: this.translation.translate('Treatment.Packed'), additional: null },
            { id: '108', text: this.translation.translate('Treatment.ChemicallyTreated'), additional: null },
            { id: '109', text: this.translation.translate('Treatment.Scarified'), additional: null },
        ];
        this.cardinalPointsByDirections = [
            //Runway Designator,  Cardinal direction right of centreline,  Cardinal direction left of centreline
            {
                designator: "01,02",
                right: this.translation.translate('CardinalValues.East'),
                left: this.translation.translate('CardinalValues.West')
            },
            {
                designator: "03,04,05,06",
                right: this.translation.translate('CardinalValues.SouthEast'),
                left: this.translation.translate('CardinalValues.NorthWest')
            },
            {
                designator: "07,08,09,10,11",
                right: this.translation.translate('CardinalValues.South'),
                left: this.translation.translate('CardinalValues.North')
            },
            {
                designator: "12,13,14,15",
                right: this.translation.translate('CardinalValues.SouthWest'),
                left: this.translation.translate('CardinalValues.NorthEast')
            },
            {
                designator: "16,17,18,19,20",
                right: this.translation.translate('CardinalValues.West'),
                left: this.translation.translate('CardinalValues.East')
            },
            {
                designator: "21,22,23,24",
                right: this.translation.translate('CardinalValues.NorthWest'),
                left: this.translation.translate('CardinalValues.SouthEast')
            },
            {
                designator: "25,26,27,28,29",
                right: this.translation.translate('CardinalValues.North'),
                left: this.translation.translate('CardinalValues.South')
            },
            {
                designator: "30,31,32,33",
                right: this.translation.translate('CardinalValues.NorthEast'),
                left: this.translation.translate('CardinalValues.SouthWest')
            },
            {
                designator: "34,35,36",
                right: this.translation.translate('CardinalValues.East'),
                left: this.translation.translate('CardinalValues.West')
            },
        ];
        //End definition od ENUMS
        this.configureReactiveForm();
        this.observationDateText = this.observationDateMoment.format("YYYY/MM/DD, H:mm") + " UTC";
        var fc = this.$ctrl('observationDate');
        var formatedDate = this.observationDateMoment.format(this.dateFormat);
        if (fc.value !== formatedDate) {
            fc.patchValue(formatedDate);
        }
        var backLabel = this.translation.translate('ActionButtons.BackToReviewList');
        if (this.isReadOnly) {
            this.txtBackToReviewList = backLabel;
        }
        else {
            var fromWidthdrawnAction = this.memStorageService.get(this.memStorageService.MODIFY_ACTION_KEY);
            this.txtBackToReviewList = fromWidthdrawnAction ? this.translation.translate('ActionButtons.CancelAnd') + backLabel : backLabel;
        }
        this.isSaveButtonDisabled = this.isSaveButtonDisabled || this.isReadOnly;
        if (this.isFormLoadingData) {
            this.loadSubject(this.token.subjectId);
        }
        this.initMap();
        this.mapHealthCheck();
    };
    RscComponent.prototype.ngOnDestroy = function () {
        if (window["app"].leafletmap) {
            window["app"].leafletmap.dispose();
        }
    };
    RscComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.buttonBars.forEach(function (child) {
            child.parent = _this;
        });
        if (!this.isReadOnly) {
            this.nsdForm.valueChanges
                .debounceTime(2000)
                .subscribe(function () {
                if (_this.validateForm())
                    _this.generateIcao();
            });
        }
    };
    RscComponent.prototype.isCalendarActive = function () {
        return true;
    };
    RscComponent.prototype.setObservationDate = function (date, leaveEmpty) {
        var fc = this.$ctrl('observationDate');
        if (date) {
            var momentDate = moment.utc(date);
            if (momentDate.isValid()) {
                this.observationDateText = momentDate.format("YYYY/MM/DD, H:mm") + " UTC";
                var formatedDate = momentDate.format(this.dateFormat);
                if (fc.value !== formatedDate) {
                    fc.setValue(formatedDate);
                    fc.markAsDirty();
                    this.observationDateMoment = momentDate;
                }
            }
        }
        else {
            if (leaveEmpty) {
                fc.setValue(null);
                fc.markAsDirty();
            }
            else {
                this.setObservationDate(moment.utc());
            }
        }
    };
    RscComponent.prototype.validateObservationDate = function (errorName) {
        return true;
    };
    RscComponent.prototype.onObservationDateInputChange = function (strDate) {
        if (strDate && strDate.length === 10) {
            var momentDate = moment.utc(strDate, this.dateFormat);
            if (momentDate.isValid()) {
                this.observationDateText = momentDate.format("YYYY/MM/DD, H:mm") + " UTC";
                var fc = this.$ctrl('observationDate');
                var formatedDate = momentDate.format(this.dateFormat);
                if (fc.value !== formatedDate) {
                    fc.setValue(formatedDate);
                    fc.markAsDirty();
                    this.observationDateMoment = momentDate;
                }
            }
            else {
                this.observationDateText = "";
            }
        }
        else {
            this.observationDateText = "";
        }
    };
    RscComponent.prototype.createClearedWidthModel = function () {
        var _this = this;
        var firstOffset = {
            offsetFT: 0,
            offsetSide: rsc_model_1.CardinalSideEnum.E,
        };
        var secondOffset = {
            offsetFT: 0,
            offsetSide: rsc_model_1.CardinalSideEnum.E,
        };
        var data = {
            parent: this,
            runway: this.runways.find(function (r) { return r.runwayId === _this.runwayId; }),
            clearedFull: false,
            centredClearedWidth: 0,
            clearedPortionOffsets: [firstOffset, secondOffset],
        };
        return data;
    };
    RscComponent.prototype.createContaminantModel = function (id) {
        var data = {
            parent: this,
            id: id,
            data: {
                firstContaminant: {
                    contaminantId: '-1',
                    id: '-1',
                    depth: '-1',
                    coverage: '-1'
                },
                secondContaminant: {
                    contaminantId: '-1',
                    id: '-1',
                    depth: '-1',
                    coverage: '-1'
                },
                rwyCC: '-1',
                rwyCCCalculated: '',
                rwyCCActionSelected: '-1',
                coefficient: '-1',
                temperature: -1,
            }
        };
        return data;
    };
    RscComponent.prototype.createRwyFrictionModel = function () {
        var data = {
            parent: this,
            measureDate: null,
            coefficient: '-1',
            temperature: '-1',
        };
        return data;
    };
    RscComponent.prototype.createClearingOperationsModel = function () {
        var text = {
            english: '',
            french: '',
        };
        var data = {
            parent: this,
            clearingUnderway: false,
            startAt: '',
            completedBy: '',
            remarks: text,
        };
        return data;
    };
    RscComponent.prototype.createSnowBanksModel = function () {
        var _this = this;
        var first = {
            bankHeight: {
                bankHeightFt: 0,
                bankHeightInch: 0
            },
            bankEdgeFt: 0,
            bankSide: rsc_model_1.CardinalSideEnum.E,
        };
        var second = {
            bankHeight: {
                bankHeightFt: 0,
                bankHeightInch: 0
            },
            bankEdgeFt: 0,
            bankSide: rsc_model_1.CardinalSideEnum.E,
        };
        var data = {
            parent: this,
            runway: this.runways.find(function (r) { return r.runwayId === _this.runwayId; }),
            firstSnowbank: first,
            secondSnowbank: second,
        };
        return data;
    };
    RscComponent.prototype.createTreatmentsModel = function () {
        var data = {
            parent: this,
            sand: false,
            sandTimeApplied: '',
            chemicallyTreated: false,
            chemicalTimeApplied: '',
            remarks: '',
        };
        return data;
    };
    RscComponent.prototype.createRemainingWidthRemarksModel = function () {
        var data = {
            parent: this,
            remainingWidthRemarks: '',
        };
        return data;
    };
    RscComponent.prototype.createNextObservationModel = function () {
        var data = {
            parent: this,
            nextScheduledObservation: moment.utc().add(8, 'hours'),
            generalRemarks: '',
        };
        return data;
    };
    RscComponent.prototype.createWindrowsModel = function () {
        var data = {
            parent: this,
            windrowsRemarks: '',
        };
        return data;
    };
    RscComponent.prototype.createTaxiwaysRemarksModel = function () {
        var data = {
            parent: this,
            taxiwaysRemarks: '',
        };
        return data;
    };
    RscComponent.prototype.createApronRemarksModel = function () {
        var data = {
            parent: this,
            apronRemarks: '',
        };
        return data;
    };
    RscComponent.prototype.configureReactiveForm = function () {
        var _this = this;
        this.nsdForm = this.fb.group({
            subjectId: [this.token.subjectId || '', [forms_1.Validators.required]],
            subjectLocation: this.token.subjectLocation || '',
            conditionsChanging: [{ value: this.token.conditionsChanging, disabled: false }],
            telephone: [{ value: this.token.telephone, disabled: this.isReadOnly }],
            extension: [{ value: this.token.extension, disabled: this.isReadOnly }],
            maintenance: [{ value: this.maintenance, disabled: false }],
            observationDate: [{ value: this.observationDateMoment, disabled: this.isReadOnly }],
            grf: [{ value: this.grfValue, disabled: false }],
            runwayId: [{ value: this.runwayId, disabled: this.isRunwaySelectorDisabled }],
            refreshIcao: [{ value: 0 }, [refreshIcaoValidator]],
            frmClearedWithArr: this.fb.array([]),
            frmContaminantArr: this.fb.array([]),
            frmAverageCRFIArr: this.fb.array([]),
            frmRemainingWidthRemarksArr: this.fb.array([]),
            frmCleaningOperationsArr: this.fb.array([]),
            frmSnowBanksArr: this.fb.array([]),
            frmTreatmentsArr: this.fb.array([]),
            frmWindrowsArr: this.fb.array([]),
            frmTaxiwaysRemarksArr: this.fb.array([]),
            frmApronRemarksArr: this.fb.array([]),
            frmNextScheduledObservationArr: this.fb.array([]),
        });
        this.nsdForm.get('subjectId').valueChanges.subscribe(function (value) { return _this.subjectIdChanged(value); });
    };
    RscComponent.prototype.subjectIdChanged = function (value) {
        this.token.subjectId = value;
    };
    RscComponent.prototype.loadSubject = function (subjectId) {
        var _this = this;
        this.dataService.getSdoSubject(subjectId)
            .subscribe(function (sdoSubjectResult) {
            _this.subjectSelected = sdoSubjectResult;
            _this.subjects = [];
            _this.subjects.push(sdoSubjectResult);
            _this.loadSdoSubjectRwy(_this.subjectSelected.id);
        });
    };
    RscComponent.prototype.sdoSubjectWithTwysOptions = function () {
        var self = this;
        var app = this.rscService.app;
        return {
            minimumInputLength: 2,
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.SdoSubjectPlaceholder')
            },
            language: {
                inputTooShort: function (args) {
                    return self.translation.translate('Schedule.InputTooShort');
                },
            },
            ajax: {
                type: 'GET',
                url: app.apiUrl + "rsc/filterwithrwy",
                dataType: 'json',
                delay: 500,
                headers: {
                    "RequestVerificationToken": app.antiForgeryTokenValue
                },
                data: function (parms, page) {
                    return {
                        Query: parms.term,
                        Size: 200,
                        doaId: app.doaId //set by the app controller
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            templateResult: function (data) {
                if (data.loading) {
                    return data.text;
                }
                return self.dataService.getSdoExtendedName(data);
            },
            templateSelection: function (selection) {
                if (selection && selection.id === "-1") {
                    return selection.text;
                }
                self.updateSubjectSelected(selection);
                return self.dataService.getSdoExtendedName(selection);
            },
        };
    };
    RscComponent.prototype.updateSubjectSelected = function (sdoSubject) {
        var _this = this;
        var subjectId = this.$ctrl('subjectId').value;
        if (subjectId === sdoSubject.id) {
            return;
        }
        if (sdoSubject.id !== subjectId) {
            var subjectCtrl = this.$ctrl('subjectId');
            subjectCtrl.setValue(sdoSubject.id);
            subjectCtrl.markAsDirty();
        }
        this.subjectSelected = sdoSubject;
        if (!sdoSubject.subjectGeo) {
            this.dataService.getSdoSubject(sdoSubject.id)
                .subscribe(function (sdoSubjectResult) {
                _this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);
                _this.cleanAllSelectedData();
                _this.loadSdoSubjectRwy(sdoSubject.id);
                _this.updateMap(sdoSubjectResult, true);
            });
        }
        else {
            this.setBilingualRegion(sdoSubject.isBilingualRegion);
            this.loadSdoSubjectRwy(sdoSubject.id);
        }
    };
    RscComponent.prototype.cleanAllSelectedData = function () {
        this.runways = [];
        this.runwaySelected = null;
        this.runwayComboList = [];
        this.runwayId = '-1';
        this.runwaysRSCData = [];
        this.cleanRunwayData();
        this.$ctrl('refreshIcao').setValue(0);
    };
    RscComponent.prototype.cleanRunwayData = function () {
        this.maintenance = false;
        this.grfValue = true;
        this.$ctrl('maintenance').setValue(this.maintenance);
        this.$ctrl('grf').setValue(this.grfValue);
        this.$ctrl('runwayId').setValue(this.runwayId);
        //Clean the Components of the Runway
    };
    RscComponent.prototype.loadSdoSubjectRwy = function (sdoId) {
        var _this = this;
        if (sdoId) {
            var self_1 = this;
            this.rscService.getSdoRwys(sdoId)
                .subscribe(function (sdoData) {
                //this.cleanSelectedData();
                _this.runways = sdoData;
                _this.prepareRunwayComboList();
                _this.isRunwaySelectorDisabled = (_this.$ctrl('subjectId').value === '');
                if (_this.isFormLoadingData) {
                    _this.runwaysRSCData = _this.model.token.rscRunways;
                    _this.runwaySelected = _this.runways.find(function (x) { return x.runwayId === _this.runwaysRSCData[0].runwayId; });
                    _this.runwayId = _this.runwaySelected.runwayId;
                    _this.pickRunway(_this.runwayId);
                    _this.isFormLoadingData = false;
                }
            });
        }
    };
    RscComponent.prototype.prepareRunwayComboList = function () {
        this.runwayComboList = [];
        this.runwayComboList = this.runways.map(function (item) {
            return {
                id: item.runwayId,
                text: item.designator.toUpperCase(),
            };
        });
        if (!this.isFormLoadingData) {
            this.runwayComboList.unshift({
                id: "-1",
                text: "",
            });
        }
    };
    RscComponent.prototype.wipeComponentData = function () {
        this.model.clearedWidth = null;
        this.model.contaminants = [];
        this.model.rwyFriction = null;
        this.model.remainingWidthRemarks = null;
        this.model.clearingOperations = null;
        //Snowbanks
        //Treatments
        this.model.windrowsRemarks = null;
        this.model.taxiwaysRemarks = null;
        this.model.apronRemarks = null;
        this.model.nextObservation = null;
    };
    RscComponent.prototype.maintenanceChange = function () {
        if (this.$ctrl('subjectId').value === '')
            return;
        this.maintenance = !this.maintenance;
        this.$ctrl('maintenance').patchValue(this.maintenance);
        this.wipeComponentData();
        if (!this.maintenance) {
            this.grfValue = false;
            this.$ctrl('grf').setValue(this.grfValue);
            this.setObservationDate(null);
        }
        else {
            this.grfValue = true;
            this.$ctrl('grf').setValue(this.grfValue);
            this.observationDateMoment = moment.utc().format("YYYY/MM/DD, H:mm");
            this.setObservationDate(this.observationDateMoment);
            //Components
            this.model.clearedWidth = this.createClearedWidthModel();
            this.model.contaminants.push(this.createContaminantModel('Touchdown'));
            this.model.contaminants.push(this.createContaminantModel('Midpoint'));
            this.model.contaminants.push(this.createContaminantModel('Rollout'));
            this.model.rwyFriction = this.createRwyFrictionModel();
            this.model.remainingWidthRemarks = this.createRemainingWidthRemarksModel();
            this.model.clearingOperations = this.createClearingOperationsModel();
            this.model.snowbanks = this.createSnowBanksModel();
            this.model.treatments = this.createTreatmentsModel();
            this.model.windrowsRemarks = this.createWindrowsModel();
            this.model.taxiwaysRemarks = this.createTaxiwaysRemarksModel();
            this.model.apronRemarks = this.createApronRemarksModel();
            this.model.nextObservation = this.createNextObservationModel();
        }
    };
    RscComponent.prototype.grfChange = function () {
        if (this.$ctrl('subjectId').value === '')
            return;
        this.grfValue = !this.grfValue;
        this.$ctrl('grf').patchValue(this.grfValue);
        this.model.contaminants = [];
        this.nsdForm.get('frmContaminantArr').controls.splice(0, this.grfValue ? 1 : 3);
        if (this.grfValue) {
            this.model.contaminants.push(this.createContaminantModel('Touchdown'));
            this.model.contaminants.push(this.createContaminantModel('Midpoint'));
            this.model.contaminants.push(this.createContaminantModel('Rollout'));
        }
        else {
            this.model.contaminants.push(this.createContaminantModel('Average'));
        }
    };
    RscComponent.prototype.populateContaminantValues = function (actual) {
        if (actual === 'Touchdown' || actual === 'Midpoint') {
            var next_1 = '';
            if (actual === 'Touchdown') {
                next_1 = 'Midpoint';
            }
            else {
                next_1 = 'Rollout';
            }
            var contaminantsCtrl = this.contaminantComponentsList.find(function (c) { return c.model.id === actual; });
            if (contaminantsCtrl) {
                var nItem = contaminantsCtrl.exportComponentInternalData();
                //next component
                nItem.id = next_1;
                contaminantsCtrl = this.contaminantComponentsList.find(function (c) { return c.model.id === next_1; });
                if (contaminantsCtrl)
                    contaminantsCtrl.refreshValues(nItem);
            }
        }
    };
    RscComponent.prototype.clearContaminantValues = function (id) {
        if (id.length > 0) {
            var contaminantsCtrl = this.contaminantComponentsList.find(function (c) { return c.model.id === id; });
            if (contaminantsCtrl) {
                contaminantsCtrl.processClean();
            }
        }
    };
    RscComponent.prototype.clearClearedWidthValues = function () {
        if (this.clearedWidthComponent)
            this.clearedWidthComponent.clearValues();
    };
    RscComponent.prototype.clearAverageRunwayCRFIValues = function () {
        if (this.rwyFriction)
            this.rwyFriction.cleanValues();
    };
    RscComponent.prototype.clearRemainingWidthRemarksValues = function () {
        if (this.remainingWidth)
            this.remainingWidth.clearValues();
    };
    RscComponent.prototype.clearClearingOperationsValues = function () {
        if (this.clearingOperationsComponent)
            this.clearingOperationsComponent.clearValues();
    };
    RscComponent.prototype.clearSnowbanksValues = function () {
        if (this.snowBanksComponent)
            this.snowBanksComponent.clearValues();
    };
    RscComponent.prototype.clearTreatmentsValues = function () {
        if (this.treatmentsComponent)
            this.treatmentsComponent.clearValues();
    };
    RscComponent.prototype.clearWindrowsRemarksValues = function () {
        if (this.windrowsComponent)
            this.windrowsComponent.clearValues();
    };
    RscComponent.prototype.clearTaxiwaysRemarksValues = function () {
        if (this.taxiwaysRemarksComponent)
            this.taxiwaysRemarksComponent.clearValues();
    };
    RscComponent.prototype.clearApronRemarksValues = function () {
        if (this.apronRemarksComponent)
            this.apronRemarksComponent.clearValues();
    };
    RscComponent.prototype.clearNextScheduleObservationValues = function () {
        if (this.nextObservation)
            this.nextObservation.clearValues();
    };
    RscComponent.prototype.selectedRunwayChanged = function ($event) {
        if (this.isReadOnly || !$event.value)
            return;
        var twySel = this.$ctrl('runwayId');
        if (twySel) {
            var val = $event.value;
            if (this.runwayId === val)
                return;
            this.runwayId = val;
            twySel.patchValue(this.runwayId);
            if (val === '-1')
                return;
            var runway = this.runwaysRSCData.find(function (x) { return x.runwayId === val; });
            if (runway) {
                //Update the fields
                this.updateRunwayFields(runway);
            }
            else {
                if (this.runwaySelected)
                    this.runwaySelected.selected = false;
                //Clean the fields
                this.cleanRunwayData();
                this.maintenance = false;
                this.maintenanceChange();
            }
            this.runwaySelected = this.runways.find(function (x) { return x.runwayId === val; });
        }
    };
    RscComponent.prototype.getContaminantComponent = function (name) {
        var ctrl = null;
        if (name.trim().toUpperCase() === 'TOUCHDOWN') {
            //Touchdown
            ctrl = this.contaminantComponentsList.find(function (c) { return c.model.id === 'Touchdown'; });
        }
        else if (name.trim().toUpperCase() === 'MIDPOINT') {
            //Midpoint
            ctrl = this.contaminantComponentsList.find(function (c) { return c.model.id === 'Midpoint'; });
        }
        else if (name.trim().toUpperCase() === 'ROLLOUT') {
            //Rollout
            ctrl = this.contaminantComponentsList.find(function (c) { return c.model.id === 'Rollout'; });
        }
        else if (name.trim().toUpperCase() === 'AVERAGE') {
            //Average
            ctrl = this.contaminantComponentsList.find(function (c) { return c.model.id === 'Average'; });
        }
        return ctrl;
    };
    RscComponent.prototype.setClearedPortionData = function () {
        var data;
        if (!this.grfValue) {
            //Average
            var dataSfc = this.getContaminantComponent('average').exportClearedSurfaceData();
            data = {
                isAverage: true,
                average: dataSfc,
                runwayThirds: null,
            };
        }
        else {
            //Thirds
            var thirds = [];
            //Touchdown
            var dataSfc = this.getContaminantComponent('touchdown').exportClearedSurfaceThirdData();
            thirds.push(dataSfc);
            //Midpoint
            dataSfc = this.getContaminantComponent('midpoint').exportClearedSurfaceThirdData();
            thirds.push(dataSfc);
            //Rollout
            dataSfc = this.getContaminantComponent('rollout').exportClearedSurfaceThirdData();
            thirds.push(dataSfc);
            data = {
                isAverage: false,
                average: null,
                runwayThirds: thirds,
            };
        }
        return data;
    };
    RscComponent.prototype.setRunwayClearedPortion = function () {
        var cpd = this.setClearedPortionData();
        var data = {
            average: cpd.average,
            isAverage: cpd.isAverage,
            runwayThirds: cpd.runwayThirds,
            averageRwyFriction: null,
            contaminantComment: null,
            otherConditions: null,
        };
        return data;
    };
    RscComponent.prototype.setRSCRunwayData = function () {
        var data = {
            runwayId: this.runwayId,
            obsTimeUTC: this.observationDateMoment,
            //Data from the ClearedWithComponent
            clearedFull: true,
            centredClearedWidth: 0,
            clearedPortionOffsets: null,
            //Data from the Contaminants
            clearedPortion: this.setRunwayClearedPortion(),
            //Data from the Remaining Width Remarks TextBox
            remainingWidth: null,
            noWinterMaintenance: !this.maintenance,
            //Data from SnowBanksComponent
            snowbanks: null,
            //Data from ClearingOperationsComponent
            clearingOperations: null,
            //Data from TreatmentsComponent
            treaments: null,
        };
        return data;
    };
    RscComponent.prototype.setRSCPayload = function () {
        var app = this.rscService.app;
        var time = moment().add(8, 'hours');
        var nextObsComponent = this.nextObservation.exportComponentInternalData();
        var token = {
            immediate: true,
            permanent: false,
            originator: app.usrName,
            endValidity: moment.utc(time, 'YYMMDDHHmm'),
            subjectId: this.subjectId,
            subjectLocation: this.$ctrl('subjectLocation').value,
            isBilingual: this.isBilingualRegion,
            aerodrome: this.subjectSelected.designator,
            aprons: null,
            conditionsChanging: this.token.conditionsChanging,
            telephone: (this.token.conditionsChanging) ? this.token.telephone : '',
            extension: (this.token.conditionsChanging) ? this.token.extension : '',
            generalRemarks: nextObsComponent.generalRemarks,
            nextPlannedObs: nextObsComponent.nextScheduledObservation,
            taxiways: null,
            rscRunways: this.runwaysRSCData
        };
        return token;
    };
    RscComponent.prototype.includeRunway = function () {
        var _this = this;
        if (!this.runwaySelected)
            return;
        var sel = this.runwaysRSCData.findIndex(function (x) { return x.runwayId === _this.runwaySelected.runwayId; });
        if (sel < 0) {
            this.runwaySelected.selected = true;
            var rwyData = this.setRSCRunwayData();
            this.runwaysRSCData.push(rwyData);
            if (this.validateForm())
                this.generateIcao();
        }
    };
    RscComponent.prototype.modifyRunway = function () {
        var _this = this;
        if (!this.runwaySelected)
            return;
        var sel = this.runwaysRSCData.findIndex(function (x) { return x.runwayId === _this.runwaySelected.runwayId; });
        if (sel > -1) {
            this.runwaySelected.selected = true;
            var rwyData = this.setRSCRunwayData();
            this.runwaysRSCData.splice(sel, 1, rwyData);
            if (this.validateForm())
                this.generateIcao();
        }
    };
    RscComponent.prototype.excludeRunway = function () {
        var _this = this;
        if (!this.runwaySelected)
            return;
        var sel = this.runwaysRSCData.findIndex(function (x) { return x.runwayId === _this.runwaySelected.runwayId; });
        if (sel > -1) {
            this.runwaySelected.selected = false;
            this.runwaysRSCData.splice(sel, 1);
            if (this.validateForm())
                this.generateIcao();
        }
    };
    RscComponent.prototype.updateRunwayFields = function (runway) {
        this.grfValue = true;
        this.maintenance = !runway.noWinterMaintenance;
        if (this.maintenance) {
            this.grfValue = !runway.clearedPortion.isAverage;
            //Do something
            if (this.grfValue) {
            }
            else {
            }
        }
        this.$ctrl('maintenance').patchValue(this.maintenance);
        this.$ctrl('grf').patchValue(this.grfValue);
        if (this.isBilingualRegion) {
        }
        else {
        }
    };
    RscComponent.prototype.pickRunway = function (val) {
        this.runwayId = val;
        this.$ctrl('runwayId').patchValue(this.runwayId);
        var runwayData = this.runwaysRSCData.find(function (x) { return x.runwayId === val; });
        if (runwayData) {
            //Update the fields
            this.updateRunwayFields(runwayData);
        }
        else {
            if (this.runwaySelected)
                this.runwaySelected.selected = false;
            this.cleanRunwayData();
        }
        this.runwaySelected = this.runways.find(function (x) { return x.runwayId === val; });
    };
    RscComponent.prototype.getRunwayInfoToShow = function (rwy) {
        if (rwy) {
            if (rwy.noMaintenance) {
                return 'NO WINTER MAINTENANCE';
            }
            if (rwy.average) {
                return rwy.full.data;
            }
            else {
                return '1/3 ' + rwy.direction.firstData.data + ' 2/3 ' + rwy.direction.middleData.data + ' 3/3 ' + rwy.direction.endData.data;
            }
        }
        return "";
    };
    RscComponent.prototype.isExcludeButtonDisabled = function () {
        var _this = this;
        if (this.isReadOnly)
            return true;
        if (!this.runwaySelected)
            return true;
        var sel = this.runwaysRSCData.find(function (x) { return x.runwayId === _this.runwaySelected.runwayId; });
        if (sel)
            return false;
        else
            return true;
    };
    RscComponent.prototype.isModifyButtonDisabled = function () {
        var _this = this;
        if (this.isReadOnly)
            return true;
        if (!this.runwaySelected)
            return true;
        var sel = this.runwaysRSCData.find(function (x) { return x.runwayId === _this.runwaySelected.runwayId; });
        if (sel) {
            //Do something
            return false;
        }
        else
            return true;
    };
    RscComponent.prototype.isIncludeButtonDisabled = function () {
        var _this = this;
        if (this.isReadOnly)
            return true;
        if (!this.runwaySelected)
            return true;
        var sel = this.runwaysRSCData.find(function (x) { return x.runwayId === _this.runwaySelected.runwayId; });
        //Ifthe runway is NOT included yet on the list, we need to validate the data
        if (!sel)
            return !this.validateData();
        return true;
    };
    RscComponent.prototype.validateData = function () {
        //Fake data
        if (this.$ctrl('maintenance').value === true) {
            var valid = false;
            //Validate Contaminants component
            valid = this.$ctrl('frmContaminantArr').valid;
            if (!valid)
                return false;
            if (this.$ctrl('frmContaminantArr').pristine)
                return false;
            if (this.$ctrl('grf').value === true) {
                //Validate the data
                valid = this.$ctrl('frmContaminantArr').valid;
                if (!valid)
                    return false;
                // End validate data
            }
            else {
                // Validate the components
                valid = this.$ctrl('frmContaminantArr').valid;
                if (!valid)
                    return false;
                // Keep validating other components
                //End validate components
            }
        }
        return true;
    };
    RscComponent.prototype.validateForm = function () {
        this.subjectId = this.$ctrl('subjectId').value;
        this.$ctrl('refreshIcao').setValue(0);
        if (!this.subjectId || this.subjectId.length === 0 || this.subjectId === '-1')
            return false;
        if (this.$ctrl('conditionsChanging').value === true) {
            if (this.$ctrl('telephone').valid) {
                this.$ctrl('refreshIcao').setValue(1);
                return true;
            }
            return false;
        }
        if (!this.runways || !this.runwaysRSCData)
            return false;
        if (this.runwaysRSCData.length < 1)
            return false;
        //To DO:
        //For each runwaysSelectedData, validate the Data
        //For now, it is always not valid
        //this.$ctrl('refreshIcao').setValue(1);
        //return true;
        return false;
    };
    RscComponent.prototype.rowColor = function (id) {
        if (!this.runwaySelected) {
            return '';
        }
        if (this.runwaySelected.runwayId === id)
            return 'bg-submitted';
        return '';
    };
    RscComponent.prototype.buttonColor = function () {
        if (!this.isModifyButtonDisabled()) {
            //do something
        }
        return 'btn-info';
    };
    RscComponent.prototype.setBilingualRegion = function (value) {
        this.token.isBilingual = value;
        this.isBilingualRegion = value;
        if (this.isBilingualRegion) {
            if (!this.isReadOnly) {
                //
            }
        }
        else {
            if (!this.isReadOnly) {
                //
            }
        }
    };
    RscComponent.prototype.resetIcaoModel = function () {
        this.model.code23 = "";
        this.model.code45 = "";
        this.model.radius = 0;
        this.model.lowerLimit = 0;
        this.model.upperLimit = 0;
        this.model.scope = "";
        this.model.series = "";
        this.model.startActivity = null;
        this.model.endValidity = null;
        this.model.fir = "";
        this.model.itemA = "";
        this.model.itemD = "";
        this.model.itemE = "";
        this.model.itemEFrench = "";
        this.model.itemF = 0;
        this.model.itemF = 0;
    };
    RscComponent.prototype.updateModel = function (data) {
        data.organizationId = data.organizationId ? data.organizationId : this.model.organizationId;
        data.proposalId = data.proposalId ? data.proposalId : this.model.proposalId;
        var attachments = this.model.attachments;
        this.model = data;
        this.model.attachments = this.model.attachments || attachments;
        this.token = data.token;
    };
    RscComponent.prototype.generateIcao = function () {
        var _this = this;
        if (this.model.proposalType === data_model_1.ProposalType.Cancel || this.nsdForm.dirty && !this.isReadOnly) {
            if (this.waitingForGeneratingIcao)
                return;
            this.isSaveButtonDisabled = (this.token.conditionsChanging) ? true : !this.nsdForm.valid;
            this.isSubmitButtonDisabled = !this.nsdForm.valid;
            if (this.nsdForm.valid) {
                this.isSubmitButtonDisabled = this.model.status === data_model_1.ProposalStatus.Submitted || this.model.status === data_model_1.ProposalStatus.Terminated;
                this.resetIcaoModel();
                var payload = this.setPayload(true);
                this.waitingForGeneratingIcao = true;
                this.dataService.generateIcao(payload)
                    .subscribe(function (data) {
                    _this.model.token.isBilingual = data.token.isBilingual; // get the isBilingual flag from the ICAO generation
                    data.token = _this.model.token; //don't override tokens with server response
                    data.groupId = _this.model.groupId;
                    data.rejectionReason = _this.model.rejectionReason;
                    _this.updateModel(data);
                    _this.waitingForGeneratingIcao = false;
                }, function (error) {
                    _this.waitingForGeneratingIcao = false;
                    _this.isSaveButtonDisabled = true;
                    _this.isSubmitButtonDisabled = true;
                    var errorMessage = error.message;
                    if (error.exceptionMessage) {
                        errorMessage += errorMessage + " " + error.exceptionMessage;
                    }
                    _this.toastr.error(_this.translation.translate('Dashboard.MessageIcaoError') + errorMessage, "", { 'positionClass': 'toast-bottom-right' });
                });
            }
            else {
                this.waitingForGeneratingIcao = false;
                this.model.itemE = "";
                this.model.itemEFrench = "";
            }
        }
        else {
            this.waitingForGeneratingIcao = false;
        }
    };
    RscComponent.prototype.setPayload = function (toGenerateIcao) {
        this.model.token = this.setRSCPayload();
        var payload = {
            categoryId: this.model.categoryId,
            version: this.model.version,
            name: this.model.name,
            token: this.model.token,
            referredSeries: this.model.referredSeries,
            referredNumber: this.model.referredNumber,
            referredYear: this.model.referredYear,
            proposalType: this.model.proposalType,
            organizationId: this.model.organizationId,
            proposalId: this.model.proposalId,
            status: this.model.status,
            statusUpdated: this.model.statusUpdated,
            parentNotamId: this.model.parentNotamId,
            notamId: this.model.notamId,
            groupId: this.model.groupId,
            number: this.model.number,
            year: this.model.year,
            rowVersion: this.model.rowVersion
        };
        return payload;
    };
    RscComponent.prototype.isEditing = function () {
        return (this.$ctrl('refreshIcao').value === 0);
    };
    RscComponent.prototype.enableEditing = function () {
        this.$ctrl('refreshIcao').setValue(0);
    };
    RscComponent.prototype.stopEditing = function () {
        this.$ctrl('refreshIcao').setValue(1);
    };
    RscComponent.prototype.conditionsChangingClick = function () {
        if (this.$ctrl('subjectId').value === '')
            return;
        this.token.conditionsChanging = !this.token.conditionsChanging;
        this.$ctrl('conditionsChanging').patchValue(this.token.conditionsChanging);
        if (this.token.conditionsChanging) {
            this.$ctrl('telephone').setValidators(phoneNumberValidator);
        }
        else {
            this.$ctrl('telephone').clearValidators();
        }
    };
    RscComponent.prototype.phoneNumberFocus = function () {
        var phoneCtrl = this.$ctrl("telephone");
        var value = phone_validator_1.PhoneValidator.stripFormat(phoneCtrl.value);
        phoneCtrl.setValue(value);
    };
    RscComponent.prototype.phoneNumberBlur = function () {
        var phoneCtrl = this.$ctrl("telephone");
        var value = phone_validator_1.PhoneValidator.formatTenDigits(phoneCtrl.value);
        phoneCtrl.setValue(value);
    };
    RscComponent.prototype.validateTelephone = function () {
        if (this.token.conditionsChanging) {
            var phoneCtrl = this.$ctrl("telephone");
            return phoneCtrl.valid || this.nsdForm.pristine;
        }
        return true;
    };
    // For the NsdButtonBarComponent form
    RscComponent.prototype.isSaveDraftButtonDisabled = function () {
        this.isSaveButtonDisabled = (this.$ctrl('conditionsChanging').value === true) ? true : !this.validateForm();
        return this.isSaveButtonDisabled;
    };
    RscComponent.prototype.isSubmitProposalButtonDisabled = function () {
        this.isSubmitButtonDisabled = !this.validateForm();
        return this.isSubmitButtonDisabled || this.isWaitingForResponse;
    };
    RscComponent.prototype.isRejectProposalDisable = function () {
        return true;
    };
    RscComponent.prototype.isParkProposalDisabled = function () {
        this.isSaveButtonDisabled = (this.$ctrl('conditionsChanging').value === true) ? true : !this.validateForm();
        return this.isSaveButtonDisabled;
    };
    RscComponent.prototype.saveDraft = function () {
        var _this = this;
        var payload = this.setPayload(false);
        function success(data) {
            this.isSaveButtonDisabled = true;
            //this.txtBackToReviewList = this.backToReviewListLabel;
            this.updateModel(data);
            this.toastr.success(this.translation.translate('Dashboard.MessageSaveSuccess'), "", { 'positionClass': 'toast-bottom-right' });
        }
        function failure(error) {
            this.toastr.error(this.translation.translate('Dashboard.MessageSaveFailure') + error.message, "", { 'positionClass': 'toast-bottom-right' });
        }
        this.dataService.saveDraftProposal(payload)
            .subscribe(function (data) {
            success.call(_this, data);
        }, function (error) {
            failure.call(_this, error);
        });
    };
    RscComponent.prototype.submitProposal = function (onError) {
        this.disseminateProposal(onError);
    };
    RscComponent.prototype.discardProposal = function (onError) {
        var _this = this;
        this.dataService.discardProposal(this.model.proposalId)
            .subscribe(function (response) {
            var msg = _this.translation.translate('Dashboard.MessageDiscardSuccess');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            _this.isSaveButtonDisabled = true;
            _this.disposeMap();
            _this.router.navigate(['/dashboard']);
        }, function (error) {
            if (onError)
                onError(error);
            var msg = _this.translation.translate('Dashboard.MessageDiscardSuccess');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    RscComponent.prototype.parkProposal = function (onError) {
        var _this = this;
        this.isWaitingForResponse = true;
        var payload = this.setPayload(false);
        return this.dataService.parkProposal(payload)
            .subscribe(function (data) {
            _this.isSaveButtonDisabled = true;
            _this.nsdForm.markAsPristine();
            _this.router.navigate(['/dashboard']);
        }, function (error) {
            _this.isWaitingForResponse = false;
            if (onError)
                onError.call(error);
            _this.toastr.error(_this.translation.translate('Dashboard.MessageParkFailure') + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    RscComponent.prototype.disseminateProposal = function (onError) {
        var _this = this;
        this.isWaitingForResponse = true;
        var payload = this.setPayload(false);
        this.dataService.disseminateProposal(payload)
            .subscribe(function (data) {
            _this.isSaveButtonDisabled = true;
            _this.nsdForm.markAsPristine();
            _this.router.navigate(['/dashboard']);
        }, function (error) {
            _this.isWaitingForResponse = false;
            if (onError)
                onError.call(error);
            _this.toastr.error(_this.translation.translate('Dashboard.MessageDisseminateFailure') + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    RscComponent.prototype.backToNofProposals = function () {
        this.disposeMap();
        this.router.navigate(['/dashboard']);
    };
    RscComponent.prototype.backToProposals = function () {
        this.disposeMap();
        this.router.navigate(['/dashboard']);
    };
    // End for NsdButtonBarComponent form
    // this is nessecary because of how we load the map in a modal window
    RscComponent.prototype.initMap = function () {
        var winObj = this.windowRef.nativeWindow;
        var leafletmap = winObj['app'].leafletmap;
        if (leafletmap) {
            leafletmap.initMap();
        }
    };
    RscComponent.prototype.mapHealthCheck = function () {
        var _this = this;
        this.dataService.getMapHealth().subscribe(function (response) {
            if (response) {
                _this.mapHealthy = true;
            }
        }, function (error) {
            _this.mapHealthy = false;
            return false;
        });
    };
    RscComponent.prototype.mapAvailable = function () {
        return this.mapHealthy;
    };
    RscComponent.prototype.updateMap = function (sdoSubject, acceptSubjectLocation) {
        var _this = this;
        this.leafletmap.clearFeaturesOnLayers(['DOA']);
        if (sdoSubject.subjectGeoRefPoint) {
            var geojson = JSON.parse(sdoSubject.subjectGeoRefPoint);
            if (geojson.features.length === 1) {
                var subLocation = this.leafletmap.geojsonToTextCordinates(geojson);
                var coordinates = geojson.features[0].geometry.coordinates;
                var markerGeojson_1 = acceptSubjectLocation ? geojson : this.getGeoJsonFromLocation(this.token.subjectLocation);
                if (acceptSubjectLocation) {
                    this.nsdForm.patchValue({
                        subjectLocation: subLocation,
                        latitude: coordinates[1],
                        longitude: coordinates[0],
                    }, { onlySelf: true, emitEvent: false });
                }
                this.leafletmap.addMarker(geojson, sdoSubject.name, function () {
                    _this.leafletmap.addFeature(sdoSubject.subjectGeo, sdoSubject.name, function () {
                        if (markerGeojson_1) {
                            _this.leafletmap.setNewMarkerLocation(markerGeojson_1);
                        }
                    });
                });
            }
        }
    };
    RscComponent.prototype.getGeoJsonFromLocation = function (value) {
        var location = this.locationService.parse(value);
        return location ? this.leafletmap.latitudeLongitudeToGeoJson(location.latitude, location.longitude) : null;
    };
    RscComponent.prototype.activateTab = function (tab) {
        switch (tab) {
            case 'map':
                this.tabActive = 0;
                break;
            case 'icao-text':
                this.tabActive = 1;
                break;
            case 'icao-format':
                if (this.nsdForm.valid) {
                    this.tabActive = 2;
                }
                break;
            case 'geoMap':
                this.tabActive = 3;
                break;
            case 'last-icao-format':
                if (this.nsdForm.valid) {
                    this.tabActive = 4;
                }
                break;
            default:
                this.tabActive = 1;
        }
    };
    RscComponent.prototype.validateMap = function () {
        var winObj = this.windowRef.nativeWindow;
        var leafletmap = winObj['app'].leafletmap;
        if (leafletmap) {
            leafletmap.resizeMapForModal();
        }
    };
    RscComponent.prototype.validateGeoMap = function () {
        var winObj = this.windowRef.nativeWindow;
        var leafletgeomap = winObj['app'].leafletgeomap;
        if (leafletgeomap) {
            leafletgeomap.resizeGeoMapForModal();
        }
    };
    RscComponent.prototype.disposeMap = function () {
        var winObj = this.windowRef.nativeWindow;
        var leafletmap = winObj['app'].leafletmap;
        if (leafletmap) {
            leafletmap.dispose();
        }
    };
    RscComponent.prototype.$ctrl = function (name) {
        return this.nsdForm.get(name);
    };
    Object.defineProperty(RscComponent.prototype, "langCulture", {
        get: function () {
            return (window["app"].cultureInfo || "en").toUpperCase();
        },
        enumerable: false,
        configurable: true
    });
    __decorate([
        core_1.Input('isNOF'),
        __metadata("design:type", Boolean)
    ], RscComponent.prototype, "isNOF", void 0);
    __decorate([
        core_1.ViewChildren(nsd_buttonbar_component_1.NsdButtonBarComponent),
        __metadata("design:type", core_1.QueryList)
    ], RscComponent.prototype, "buttonBars", void 0);
    __decorate([
        core_1.ViewChild(nsd_georeftool_component_1.NsdGeoRefToolComponent),
        __metadata("design:type", nsd_georeftool_component_1.NsdGeoRefToolComponent)
    ], RscComponent.prototype, "geoRefTool", void 0);
    __decorate([
        core_1.ViewChild(rsc_clearedwidth_component_1.ClearedWidthComponent),
        __metadata("design:type", rsc_clearedwidth_component_1.ClearedWidthComponent)
    ], RscComponent.prototype, "clearedWidthComponent", void 0);
    __decorate([
        core_1.ViewChildren(rsc_contaminants_component_1.RscContaminantsComponent),
        __metadata("design:type", core_1.QueryList)
    ], RscComponent.prototype, "contaminantComponentsList", void 0);
    __decorate([
        core_1.ViewChild(rsc_rwyfriction_component_1.RwyFrictionComponent),
        __metadata("design:type", rsc_rwyfriction_component_1.RwyFrictionComponent)
    ], RscComponent.prototype, "rwyFriction", void 0);
    __decorate([
        core_1.ViewChild(rsc_remainingwidthremarks_component_1.RemainingWidthRemarksComponent),
        __metadata("design:type", rsc_remainingwidthremarks_component_1.RemainingWidthRemarksComponent)
    ], RscComponent.prototype, "remainingWidth", void 0);
    __decorate([
        core_1.ViewChild(rsc_clearingoperations_component_1.ClearingOperationsComponent),
        __metadata("design:type", rsc_clearingoperations_component_1.ClearingOperationsComponent)
    ], RscComponent.prototype, "clearingOperationsComponent", void 0);
    __decorate([
        core_1.ViewChild(rsc_snowbanks_component_1.SnowBanksComponent),
        __metadata("design:type", rsc_snowbanks_component_1.SnowBanksComponent)
    ], RscComponent.prototype, "snowBanksComponent", void 0);
    __decorate([
        core_1.ViewChild(rsc_treatments_component_1.RscTreatmentsComponent),
        __metadata("design:type", rsc_treatments_component_1.RscTreatmentsComponent)
    ], RscComponent.prototype, "treatmentsComponent", void 0);
    __decorate([
        core_1.ViewChild(rsc_windrows_component_1.WindrowsComponent),
        __metadata("design:type", rsc_windrows_component_1.WindrowsComponent)
    ], RscComponent.prototype, "windrowsComponent", void 0);
    __decorate([
        core_1.ViewChild(rsc_taxiwaysremarks_component_1.TaxiwaysRemarksComponent),
        __metadata("design:type", rsc_taxiwaysremarks_component_1.TaxiwaysRemarksComponent)
    ], RscComponent.prototype, "taxiwaysRemarksComponent", void 0);
    __decorate([
        core_1.ViewChild(rsc_apronremarks_component_1.ApronRemarksComponent),
        __metadata("design:type", rsc_apronremarks_component_1.ApronRemarksComponent)
    ], RscComponent.prototype, "apronRemarksComponent", void 0);
    __decorate([
        core_1.ViewChild(rsc_nextobservation_component_1.NextObservationComponent),
        __metadata("design:type", rsc_nextobservation_component_1.NextObservationComponent)
    ], RscComponent.prototype, "nextObservation", void 0);
    RscComponent = __decorate([
        core_1.Component({
            templateUrl: './app/dashboard/nsds/rsc/rsc.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            router_1.ActivatedRoute,
            windowRef_service_1.WindowRef,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            location_service_1.LocationService,
            rsc_service_1.RscService,
            angular_l10n_1.TranslationService])
    ], RscComponent);
    return RscComponent;
}());
exports.RscComponent = RscComponent;
function refreshIcaoValidator(c) {
    if (c.value === 0) {
        return { 'required': true };
    }
    return null;
}
function phoneNumberValidator(c) {
    var value = c.value;
    if (!value)
        return { 'required': true };
    if (!phone_validator_1.PhoneValidator.isValidPhoneNumber(c.value, 10)) {
        return { 'pattern': true };
    }
    return null;
}
//# sourceMappingURL=rsc.component.js.map
﻿namespace NavCanada.Core.Domain.Model.Entitities
{
    public class ProposalAcknowledgement
    {
        public int ProposalId { get; set; }
        public int OrganizationId { get; set; }
        public bool Acknowledge { get; set; }
    }
}

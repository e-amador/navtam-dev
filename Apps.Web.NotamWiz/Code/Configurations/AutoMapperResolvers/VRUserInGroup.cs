﻿using AutoMapper;
using NavCanada.SE.NOTAMWiz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NavCanada.SE.NOTAMWiz.App_Start.AutoMapperResolvers
{
    public class VRUserInGroup : IValueResolver
    {

        public ResolutionResult Resolve(ResolutionResult source)
        {
        //    int currentGroup = (int)source.Context.Options.Items["groupId"];
        //    bool userInGroup = (source.Value as UserProfile).Groups.Any(grp => grp.GroupId == currentGroup);

           // return source.New(userInGroup);
            return source.New(new Object());
        }
    }
}
﻿namespace NavCanada.Services.Api.Diagnostics.Controllers
{
    using System;
    using System.Threading.Tasks;
    using System.Web.Http;

    using Data.Contracts;
    using Models;

    [RoutePrefix("api/keepalive")]
    public class KeepAliveController : ApiController
    {
        private readonly IDiagnosticUow _uow;

        public KeepAliveController(IDiagnosticUow uow)
        {
            _uow = uow;
        }


        [HttpPost]
        public async Task<IHttpActionResult> Post(KeepAliveRequest request)
        {
            try
            {
                var keepAlive = new KeepAlive
                {
                    Id = Guid.NewGuid(),
                    Component = request.Component,
                    Tick = request.Tick,
                    Received = DateTime.UtcNow
                };
                _uow.KeepAliveRepo.Add(keepAlive);

                await _uow.SaveChangesAsync();

                return Ok(keepAlive);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
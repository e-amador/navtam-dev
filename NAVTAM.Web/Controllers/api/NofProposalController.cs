﻿using Business.Common;
using Core.Common.Geography;
using Microsoft.Ajax.Utilities;
using NavCanada.Core.Common.Common;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Common.Extensions;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.TaskEngine.Mto;
using NAVTAM.Helpers;
using NAVTAM.NsdEngine;
using NAVTAM.SignalrHub;
using NAVTAM.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    //TODO: Implement logging for grouped proposals
    [RoutePrefix("api/nof")]
    [Authorize]
    public class NofProposalController : ProposalController
    {
        public NofProposalController(IDashboardContext context, IEventHubContext eventHubContext, IGeoLocationCache geoLocationCache, ILogger logger) 
            : base(context, eventHubContext, geoLocationCache, logger)
        {
        }

        [Route("pending/{catId}", Name = "GetNofQueuePending")]
        [HttpPost]
        public async Task<IHttpActionResult> GetNofQueueSubmitted(int catId, [FromBody]QueryOptions queryOptions)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                if (queryOptions.PageSize > MaxRecordsToReturn)
                    queryOptions.PageSize = MaxRecordsToReturn;

                int totalCount = (catId == 1)
                    ? await Uow.ProposalRepo.GetNofPendingQueueCountAsync()
                    : await Uow.ProposalRepo.GetNofPendingQueueCountByCategoryIdAsync(catId);

                var proposals = (catId == 1)
                    ? await Uow.ProposalRepo.GetAllNofPendingQueueAsync(queryOptions)
                    : await Uow.ProposalRepo.GetNofPendingQueueByCategoryIdAsync(catId, queryOptions);

                return Ok(ResponsewithPagingEnvelope(queryOptions, "GetNofQueuePending", proposals, totalCount));

            }, $"api/nof/pending/{catId}");
        }

        [Route("park/{catId}", Name = "GetNofQueueParked")]
        [HttpPost]
        public async Task<IHttpActionResult> GetNofParkedSubmitted(int catId, [FromBody]QueryOptions queryOptions)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                if (queryOptions.PageSize > MaxRecordsToReturn)
                    queryOptions.PageSize = MaxRecordsToReturn;

                int totalCount = (catId == 1)
                    ? await Uow.ProposalRepo.GetNofParkedQueueCountAsync()
                    : await Uow.ProposalRepo.GetNofParkedQueueCountByCategoryIdAsync(catId);

                var proposals = (catId == 1)
                    ? await Uow.ProposalRepo.GetAllNofParkedQueueByAsync(queryOptions)
                    : await Uow.ProposalRepo.GetNofParkedQueueByCategoryIdAsync(catId, queryOptions);

                return Ok(ResponsewithPagingEnvelope(queryOptions, "GetNofQueueParked", proposals, totalCount));

            }, $"api/nof/park/{catId}");
        }

        [Route("review/{catId}", Name = "GetNofQueueReview")]
        [HttpPost]
        public async Task<IHttpActionResult> GetNofQueueReviewed(int catId, [FromBody]QueryOptions queryOptions)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                if (queryOptions.PageSize > MaxRecordsToReturn)
                    queryOptions.PageSize = MaxRecordsToReturn;

                var orgId = int.Parse(ApplicationContext.GetCookie("org_id", "0"));

                int totalCount = (catId == 1)
                    ? await Uow.ProposalRepo.GetNofReviewQueueCountAsync(orgId)
                    : await Uow.ProposalRepo.GetNofReviewQueueCountByCategoryIdAsync(orgId, catId);

                var proposals = (catId == 1)
                    ? await Uow.ProposalRepo.GetAllNofReviewQueueByAsync(orgId, queryOptions)
                    : await Uow.ProposalRepo.GetNofReviewQueueByCategoryIdAsync(orgId, catId, queryOptions);

                return Ok(ResponsewithPagingEnvelope(queryOptions, "GetNofQueueReview", proposals, totalCount));

            }, $"api/nof/review/{catId}");
        }

        [Route("pending/children/{catId}", Name = "GetChildrenNofQueuePending")]
        [HttpPost]
        public async Task<IHttpActionResult> GetChildrenNofQueueSubmitted(int catId, [FromBody]QueryOptions queryOptions)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                if (queryOptions.PageSize > MaxRecordsToReturn)
                    queryOptions.PageSize = MaxRecordsToReturn;

                var childCategories = await Uow.NsdCategoryRepo.GetChildCategoriesAsync(catId);
                int[] childCategoryIds = null;
                if (childCategories.Any())
                    childCategoryIds = childCategories.Select(x => x.Id).ToArray();

                int totalCount = await Uow.ProposalRepo.GetNofPendingByChildrenQueueCountByCategoryIdAsync(childCategoryIds);
                var proposals = await Uow.ProposalRepo.GetNofPendingQueueByChildrenCategoryIdsAsync(childCategoryIds, queryOptions);

                return Ok(ResponsewithPagingEnvelope(queryOptions, "GetChildrenNofQueuePending", proposals, totalCount));

            }, $"api/nof/pending/children/{catId}");
        }

        [Route("park/children/{catId}", Name = "GetChildrenNofQueueParked")]
        [HttpPost]
        public async Task<IHttpActionResult> GetChildrenNofQueueParked(int catId, [FromBody]QueryOptions queryOptions)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                if (queryOptions.PageSize > MaxRecordsToReturn)
                    queryOptions.PageSize = MaxRecordsToReturn;

                var childCategories = await Uow.NsdCategoryRepo.GetChildCategoriesAsync(catId);
                int[] childCategoryIds = null;
                if (childCategories.Any())
                    childCategoryIds = childCategories.Select(x => x.Id).ToArray();

                int totalCount = await Uow.ProposalRepo.GetNofParkByChildrenQueueCountByCategoryIdAsync(childCategoryIds);
                var proposals = await Uow.ProposalRepo.GetNofParkedByChildrenCategoryIdsAsync(childCategoryIds, queryOptions);


                return Ok(ResponsewithPagingEnvelope(queryOptions, "GetChildrenNofQueueParked", proposals, totalCount));

            }, $"api/nof/park/children/{catId}");
        }

        [Route("review/children/{catId}", Name = "GetChildrenNofQueueReviewed")]
        [HttpPost]
        public async Task<IHttpActionResult> GetChildrenNofQueueReviewed(int catId, [FromBody]QueryOptions queryOptions)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                if (queryOptions.PageSize > MaxRecordsToReturn)
                    queryOptions.PageSize = MaxRecordsToReturn;

                var orgId = int.Parse(ApplicationContext.GetCookie("org_id", "0"));

                var childCategories = await Uow.NsdCategoryRepo.GetChildCategoriesAsync(catId);
                int[] childCategoryIds = null;
                if (childCategories.Any())
                    childCategoryIds = childCategories.Select(x => x.Id).ToArray();

                int totalCount = await Uow.ProposalRepo.GetNofReviewByChildrenQueueCountByCategoryIdAsync(orgId, childCategoryIds);
                var proposals = await Uow.ProposalRepo.GetNofReviewByChildrenCategoryIdsAsync(orgId, childCategoryIds, queryOptions);

                return Ok(ResponsewithPagingEnvelope(queryOptions, "GetChildrenNofQueueReviewed", proposals, totalCount));

            }, $"api/nof/review/children/{catId}");
        }

        [Route("pending/count")]
        [HttpGet]
        public async Task<IHttpActionResult> GetNofTotalPending()
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                int totalCount = await Uow.ProposalRepo.GetNofPendingQueueCountAsync();

                return Ok(totalCount);

            }, "api/nof/pending/count");
        }

        [Route("park/count")]
        [HttpGet]
        public async Task<IHttpActionResult> GetNofTotalParked()
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                return Ok(await Uow.ProposalRepo.GetNofParkedQueueCountAsync());

            }, "api/nof/park/count");
        }

        [Route("review/count")]
        [HttpGet]
        public async Task<IHttpActionResult> GetNofTotalReviewed()
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                var orgId = int.Parse(ApplicationContext.GetCookie("org_id", "0"));
                int totalCount = await Uow.ProposalRepo.GetNofReviewQueueCountAsync(orgId);

                return Ok(totalCount);

            }, "api/nof/review/count");
        }

        [Route("proposals/grouped/park/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetNofParkProposalByGroupId(Guid id)
        {
            return await LogAction(async () => 
                Ok(await Uow.ProposalRepo.GetNofParkedProposalsGroupedAsync(id)), $"api/nof/proposals/grouped/park/{id}");
        }

        [Route("proposals/grouped/pending/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetNofPendingProposalByGroupId(Guid id)
        {
            return await LogAction(async () => 
                Ok(await Uow.ProposalRepo.GetNofPendingProposalsGroupedAsync(id)), $"api/nof/proposals/grouped/pending/{id}");
        }

        [Route("proposals/grouped/review/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetNofReviewProposalByGroupId(Guid id)
        {
            return await LogAction(async () =>
            {
                var orgId = int.Parse(ApplicationContext.GetCookie("org_id", "0"));
                return Ok(await Uow.ProposalRepo.GetNofReviewProposalsGroupedAsync(orgId, id));
            }, $"api/nof/proposals/grouped/review/{id}");
        }

        [Route("discard/{id}")]
        [HttpDelete]
        public async Task<IHttpActionResult> Discard(int id)
        {
            return await LogTransaction(async () =>
            {
                var proposal = await Uow.ProposalRepo.GetByIdAsync(id);
                if (proposal != null)
                {
                    return proposal.GroupId.HasValue ? await DiscardNofGroupProposal(proposal) : await DiscardNofProposal(proposal);
                }
                else
                {
                    return ApiStepResult.Fail(NotFound());
                }
            }, $"api/nof/discard/{id}");
        }

        [Route("proposals/restorestatus/{id}/{updateStatus}")]
        [HttpPost]
        public async Task<IHttpActionResult> RestoreStatus(int id, bool updateStatus)
        {
            //TODO: It seems this function gets invoked regardless the client action (cancel or accept). 
            return await LogTransaction(async () =>
            {
                var proposal = await Uow.ProposalRepo.GetByIdAsync(id);
                if (proposal == null)
                    return ApiStepResult.Fail(NotFound());

                //Were are using ItemX to flag an status when NOF Replace or Cancel a Notam directly from the NOTAM tab. 
                //In case the NOF operator decide to abort Replace/Cancel action, the flag 'ReplaceNotam=1' must be removed.
                var tag = "ReplaceNotam=1";
                if (proposal.ItemX != null && proposal.ItemX.Contains(tag))
                {
                    proposal.ItemX.Replace(tag, "");
                    await Uow.SaveChangesAsync();
                }

                if (!updateStatus)
                    return ApiStepResult.Succeed(Ok());

                if (proposal.Status != NotamProposalStatusCode.Picked && proposal.Status != NotamProposalStatusCode.ParkedPicked)
                    return ApiStepResult.Succeed(Ok());

                if (proposal.UserId != ApplicationContext.GetUserId())
                    return ApiStepResult.Succeed(Ok(Resources.OtherUserWorkingProposal));

                return proposal.GroupId.HasValue
                    ? await RestoreGroupLastValidStatus(proposal)
                    : await RestoreLastValidStatus(proposal);

            }, $"api/nof/proposals/restorestatus/{id}/{updateStatus}");
        }

        [Route("ParkProposal")]
        [HttpPost]
        [Authorize(Roles = "NOF_ROLE, NOF_ADMINISTRATOR")]
        public async Task<IHttpActionResult> ParkProposal(ProposalViewModel model)
        {
            return await LogTransaction(async () =>
            {
                if (!ModelState.IsValid)
                    return ApiStepResult.Fail(BadRequest(Resources.BadRequest));

                var validationResult = await ValidateModelVsProposal(model);
                if (validationResult.Failed)
                    return validationResult;

                model.NotamId = BuildNotamId(model);

                if (model.Status == NotamProposalStatusCode.Undefined)
                    model.Status = NotamProposalStatusCode.ParkedDraft;
                else if (model.Status != NotamProposalStatusCode.Picked && model.Status != NotamProposalStatusCode.ParkedPicked)
                    return ApiStepResult.Fail(BadRequest(Resources.ErrorProposalStatus));
                else
                    model.Status = NotamProposalStatusCode.Parked;

                return await SaveNotamProposal(model, model.Status);

            }, "api/nof/ParkProposal");
        }

        [Route("ParkGroupedProposals")]
        [HttpPost]
        [Authorize(Roles = "NOF_ROLE, NOF_ADMINISTRATOR")]
        public async Task<IHttpActionResult> ParkGroupedProposals(List<ProposalViewModel> proposals)
        {
            return await LogTransaction(async () =>
            {
                if (proposals == null || proposals.Count == 0 || !ModelState.IsValid)
                    return ApiStepResult.Fail(BadRequest(Resources.BadRequest));

                var badRequest = false;
                foreach (var proposal in proposals)
                {
                    proposal.NotamId = BuildNotamId(proposal);

                    if (proposal.Status == NotamProposalStatusCode.Undefined)
                        proposal.Status = NotamProposalStatusCode.ParkedDraft;
                    else if (proposal.Status != NotamProposalStatusCode.Picked || proposal.Status != NotamProposalStatusCode.ParkedPicked)
                        badRequest = true; // >> ??
                    else
                        proposal.Status = NotamProposalStatusCode.Parked;
                }

                if (badRequest)
                    return ApiStepResult.Fail(BadRequest(Resources.ErrorProposalStatus));

                return await SaveNotamGroupedProposal(proposals, proposals[0].Status);

            }, "api/nof/ParkGroupedProposals");
        }

        [Route("RejectProposal")]
        [HttpPost]
        [Authorize(Roles = "NOF_ROLE, NOF_ADMINISTRATOR")]
        public Task<IHttpActionResult> RejectProposal(ProposalViewModel model)
        {
            return LogTransaction(() => ExecuteRejectProposal(model), "api/nof/RejectProposal");
        }

        private async Task<ApiStepResult> ExecuteRejectProposal(ProposalViewModel model)
        {
            if (!ModelState.IsValid) return ApiStepResult.Fail(BadRequest(Resources.BadRequest));

            if (model.Token.RejectionReason.IsNullOrWhiteSpace())
                return ApiStepResult.Fail(BadRequest(Resources.BadRequest));

            model.NotamId = BuildNotamId(model);

            var proposal = await Uow.ProposalRepo.GetByIdAsync(model.ProposalId.Value);
            if (proposal == null) return ApiStepResult.Fail(NotFound());

            var success = (proposal.Status == NotamProposalStatusCode.Picked ||
                proposal.Status == NotamProposalStatusCode.Parked ||
                proposal.Status == NotamProposalStatusCode.ParkedDraft ||
                proposal.Status == NotamProposalStatusCode.ParkedPicked ||
                proposal.Status == NotamProposalStatusCode.Submitted);
            if (!success) return ApiStepResult.Fail(BadRequest(Resources.ErrorProposalStatus));
            success = (proposal.UserId == ApplicationContext.GetUserId());
            if (!success) return ApiStepResult.Fail(BadRequest(Resources.OtherUserWorkingProposal));

            //Uow.BeginTransaction();
            try
            {
                proposal.Status = NotamProposalStatusCode.Rejected;
                proposal.Received = DateTime.UtcNow.RemoveSeconds();
                proposal.UserId = ApplicationContext.GetUserId();
                proposal.RejectionReason = model.Token.RejectionReason;

                Uow.ProposalRepo.Update(proposal);
                Uow.ProposalHistoryRepo.Add(HistoryFromProposal(proposal));

                await Uow.SaveChangesAsync();

                if (proposal.GroupId.HasValue)
                {
                    var groupedProposals = await Uow.ProposalRepo.GetGroupedProposalsByGroupIdAsync(proposal.GroupId.Value);
                    foreach (var p in groupedProposals)
                    {
                        if (proposal.Id != p.Id)
                        {
                            p.Status = NotamProposalStatusCode.Rejected;
                            p.Received = DateTime.UtcNow.RemoveSeconds();
                            p.UserId = ApplicationContext.GetUserId();
                            p.RejectionReason = model.Token.RejectionReason;

                            Uow.ProposalRepo.Update(p);
                            Uow.ProposalHistoryRepo.Add(HistoryFromProposal(p));

                            await Uow.SaveChangesAsync();
                        }
                    }
                }

                //Uow.Commit();

                //SetIcaoText(model);

                return ApiStepResult.Succeed(Ok(model));
            }
            catch (Exception)
            {
                //Uow.Rollback();
                throw;
            }
        }

        [Route("DiseminateProposal")]
        [HttpPost]
        [Authorize(Roles = "NOF_ROLE, NOF_ADMINISTRATOR")]
        public Task<IHttpActionResult> DisseminateProposal(ProposalViewModel model)
        {
            return LogTransaction(() => ExecuteDisseminateProposal(model), "api/nof/DisseminateProposal");
        }

        private async Task<ApiStepResult> ExecuteDisseminateProposal(ProposalViewModel model)
        {
            if (!ModelState.IsValid)
                return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid));

            // validate the system is healthy
            var validationResult = ValidateSystemStatus();
            if (!validationResult.Succeeded)
                return ApiStepResult.Fail(BadRequest(validationResult.ErrorMessage));

            model.NotamId = BuildNotamId(model);

            // parse and validate the organization
            var orgId = int.Parse(ApplicationContext.GetCookie("org_id", "0"));
            if (orgId == 0)
                return ApiStepResult.Fail(BadRequest(Resources.InvalidOrganizationId));

            // parse and validate the DOA
            var doaId = int.Parse(ApplicationContext.GetCookie("doa_id", "0"));
            if (doaId == 0)
                return ApiStepResult.Fail(BadRequest(Resources.InvalidDOAId));

            // retrieve the manager
            var nsdManager = _managerResolver.GetNsdManager($"{model.CategoryId}-{model.Version}");
            if (nsdManager == null)
                return ApiStepResult.Fail(BadRequest(Resources.ErrorCreatingManager));

            // get the save proposal, if there was one saved
            var savedProposalResult = await GetSavedProposal(model.ProposalId);
            if (savedProposalResult.Failed)
                return ApiStepResult.Fail(BadRequest(savedProposalResult.ErrorMessage));

            var savedProposal = savedProposalResult.Result;

            // validate dissemination status
            if (!ValidateDisseminationStatus(model, savedProposal))
                return ApiStepResult.Fail(BadRequest(Resources.ErrorProposalStatus));

            // validate the user holding the proposal
            if (savedProposal != null)
            {
                // compare versions
                if (!CompareBytes(model.RowVersion, savedProposal.RowVersion))
                {
                    return ApiStepResult.Fail(BadRequest(Resources.OtherUserWorkingProposal));
                }

                if (savedProposal.Status == model.Status)
                {
                    if (savedProposal.UserId != ApplicationContext.GetUserId())
                        return ApiStepResult.Fail(BadRequest(Resources.OtherUserWorkingProposal));
                }
                else if (savedProposal.Status == NotamProposalStatusCode.Parked || 
                         savedProposal.Status == NotamProposalStatusCode.ParkedPicked || 
                         savedProposal.Status == NotamProposalStatusCode.Picked)
                {
                    return ApiStepResult.Fail(BadRequest(Resources.OtherUserWorkingProposal));
                }

                // When disseminating, the proposal and model number must match
                if (model.Number != savedProposal.Number)
                {
                    return ApiStepResult.Fail(BadRequest(Resources.OtherUserWorkingProposal));
                }
            }

            // get the dissemination status
            var newStatus = await GetDisseminationStatus(savedProposal, nsdManager.SerializeObject(model.Token));

            // validate and generate the ICAO
            var genIcao = await GenerateModelIcao(model, nsdManager);
            if (genIcao.Failed)
                return ApiStepResult.Fail(BadRequest(genIcao.ErrorMessage));

            // map model to a proposal (re-using the saved proposal if existed)
            var proposal = MapModelToProposal(nsdManager, savedProposal, model, orgId, newStatus);

            // get the configured max series number
            var maxSerialNumber = int.Parse(ApplicationContext.GetConfigValue("MaxNotamSerialNumber", "9999"));

            // this is where the dissemination 
            var disseminateResult = await DisseminateProposal(proposal, maxSerialNumber);
            if (disseminateResult.Succeeded)
            {
                proposal.CopyPropertiesTo(model);
                model.ProposalId = proposal.Id;

                SetIcaoText(model);

                Logger.LogInfo(GetType(), $"NOTAM: '{proposal.NotamId}' with ProposalId: '{proposal.Id}' queued for dissemination by '{ApplicationContext.GetUserName()}'.");

                return ApiStepResult.Succeed(Ok(model));
            }
            else
            {
                Logger.LogWarn(GetType(), $"NOTAM FAILED TO DISSEMINATE: {disseminateResult.ErrorMessage}", model);
                return ApiStepResult.Fail(BadRequest(Resources.DisseminateUnexpectedError));
            }
        }

        [Route("DiseminateGroupedProposal")]
        [HttpPost]
        [Authorize(Roles = "NOF_ROLE, NOF_ADMINISTRATOR")]
        public Task<IHttpActionResult> DiseminateGroupedProposal(List<ProposalViewModel> models)
        {
            return LogAction(() => ExecuteDiseminateGroupedProposal(models), "api/nof/DiseminateGroupedProposal");
        }

        [HttpPut]
        [Route("ItemX")]
        public Task<IHttpActionResult> UpdateItemX([FromBody] ProposalItemXViewModel vm)
        {
            return LogTransaction(() => UpdateProposalItemX(vm.ProposalId, vm.Value), "api/nof/ItemX");
        }

        private async Task<ApiStepResult> UpdateProposalItemX(int proposalId, string value)
        {
            var proposal = await Uow.ProposalRepo.GetByIdAsync(proposalId);
            if( proposal != null )
            {
                proposal.ItemX = value;

                await Uow.SaveChangesAsync();

                return ApiStepResult.Succeed(Ok());
            }
            return ApiStepResult.Fail(NotFound());
        }

        private async Task<IHttpActionResult> ExecuteDiseminateGroupedProposal(List<ProposalViewModel> models)
        {
            // model must be valid and contain at least one proposal
            if (models == null || models.Count == 0 || !ModelState.IsValid)
                return BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid);

            // parse and validate the organization
            var orgId = int.Parse(ApplicationContext.GetCookie("org_id", "0"));
            if (orgId == 0)
                return BadRequest(Resources.InvalidOrganizationId);

            // parse and validate the DOA
            var doaId = int.Parse(ApplicationContext.GetCookie("doa_id", "0"));
            if (doaId == 0)
                return BadRequest(Resources.InvalidDOAId);

            // retrieve the manager
            var nsdManager = _managerResolver.GetNsdManager($"{models[0].CategoryId}-{models[0].Version}");
            if (nsdManager == null)
                return BadRequest(Resources.ErrorCreatingManager);

            // get the max series number (by default 9999)
            var maxSeriesNumber = int.Parse(ApplicationContext.GetConfigValue("MaxNotamSerialNumber", "9999"));

            // pick the master group id
            var masterGroupId = models[0].GroupId.HasValue ? models[0].GroupId.Value : Guid.Empty;
            var isGroupMaster = true;

            var responseErrorMessage = string.Empty;

            // create the dissemination cordinator
            var coordinator = DisseminationCoordinator.Create(Uow, DateTime.UtcNow.Year, maxSeriesNumber);
            try
            {
                // remove from the history the proposals that were removed
                await RemoveProposalsFromGroupIfUserDelete(Uow, models, masterGroupId);

                foreach (var model in models)
                {
                    // retrieve the saved proposal
                    var savedProposalResult = await GetSavedProposal(model.ProposalId);
                    if (savedProposalResult.Failed)
                    {
                        responseErrorMessage = savedProposalResult.ErrorMessage;
                        break;
                    }

                    var savedProposal = savedProposalResult.Result;

                    // validate dissemination status
                    if (!ValidateDisseminationStatus(model, savedProposal))
                    {
                        responseErrorMessage = Resources.ErrorProposalStatus;
                        break;
                    }

                    // validate the user holding the proposal
                    if (!ValidateHoldingUser(savedProposal, ApplicationContext.GetUserId()))
                    {
                        responseErrorMessage = Resources.OtherUserWorkingProposal;
                        break;
                    }

                    // get the new dissemination status (based on the tokens)
                    var newStatus = await GetDisseminationStatus(savedProposal, nsdManager.SerializeObject(model.Token));

                    // validate and generate the ICAO
                    var genIcao = await GenerateModelIcao(model, nsdManager);
                    if (genIcao.Failed)
                    {
                        responseErrorMessage = genIcao.ErrorMessage;
                        break;
                    }

                    // map model to a proposal (re-using the saved proposal if existed)
                    var proposal = MapModelToProposal(nsdManager, savedProposal, model, orgId, newStatus);

                    // assign the groupId and master status to the proposal
                    masterGroupId = AssignGroupIdToProposal(proposal, masterGroupId, isGroupMaster);

                    // only the first can be a group master
                    isGroupMaster = false;

                    // validate before disseminate (this is probably a waste of time)
                    var validatePreDisseminate = ValidateProposalToDisseminate(proposal);
                    if (validatePreDisseminate.Failed)
                    {
                        responseErrorMessage = validatePreDisseminate.ErrorMessage;
                        break;
                    }

                    // disseminate the current proposal
                    var disseminateResult = await coordinator.Disseminate(proposal.Series[0], n => ExecuteProposalDissemination(proposal, n));
                    if (disseminateResult.Failed)
                    {
                        responseErrorMessage = disseminateResult.ErrorMessage;
                        break;
                    }

                    // copy the proposal properties back to the model
                    proposal.CopyPropertiesTo(model);
                }

                if (!string.IsNullOrEmpty(responseErrorMessage))
                {
                    coordinator.DiscardGroup();
                    return BadRequest(responseErrorMessage);
                }

                // commit the group transaction
                coordinator.CommitGroup();

                return Ok(models[0]);
            }
            catch (Exception e)
            {
                coordinator.DiscardGroup();
                return InternalServerError(e);
            }
        }

        static Guid AssignGroupIdToProposal(Proposal proposal, Guid groupId, bool isGroupMaster)
        {
            if (!proposal.GroupId.HasValue)
            {
                proposal.GroupId = groupId != Guid.Empty ? groupId : Guid.NewGuid();
                proposal.IsGroupMaster = isGroupMaster;
                proposal.Grouped = !isGroupMaster;
            }
            return proposal.GroupId.Value;
        }

        static bool ValidateHoldingUser(Proposal proposal, string loggedUser) => proposal == null || loggedUser.Equals(proposal.UserId);

        static bool MatchesOne(NotamProposalStatusCode target, params NotamProposalStatusCode[] states) => states.Contains(target);

        static bool ValidateDisseminationStatus(ProposalViewModel model, Proposal proposal)
        {
            if (proposal != null)
            {
                if (!MatchesOne(proposal.Status, NotamProposalStatusCode.Picked, NotamProposalStatusCode.Parked, NotamProposalStatusCode.ParkedDraft, NotamProposalStatusCode.ParkedPicked, NotamProposalStatusCode.Taken, NotamProposalStatusCode.Submitted))
                {
                    var expected = MatchesOne(model.Status, NotamProposalStatusCode.Cancelled, NotamProposalStatusCode.Replaced) && MatchesOne(proposal.Status, NotamProposalStatusCode.Disseminated, NotamProposalStatusCode.DisseminatedModified);
                    if (!expected)
                        return false;
                }
            }
            return true;
        }

        private async Task<NotamProposalStatusCode> GetDisseminationStatus(Proposal savedProposal, string modelTokens)
        {
            if (savedProposal != null)
            {
                if (string.CompareOrdinal(modelTokens, savedProposal.Tokens) != 0)
                    return NotamProposalStatusCode.DisseminatedModified;

                var latestSubmitted = await Uow.ProposalHistoryRepo.GetLatestByStatusProposalIdAsync(savedProposal.Id, NotamProposalStatusCode.Submitted);
                if (latestSubmitted != null && string.CompareOrdinal(modelTokens, latestSubmitted.Tokens) != 0)
                    return NotamProposalStatusCode.DisseminatedModified;
            }
            return NotamProposalStatusCode.Disseminated;
        }

        private async Task<StepResult<Proposal>> GetSavedProposal(int? id)
        {
            if (!id.HasValue || id.Value == 0)
            {
                return StepResult<Proposal>.Succeed(null);
            }
            else
            {
                var proposal = await Uow.ProposalRepo.GetByIdAsync(id.Value);
                return proposal != null ? StepResult<Proposal>.Succeed(proposal) : StepResult<Proposal>.Fail(Resources.ErrorRetrievingProposal);
            }
        }

        private async Task<StepResult> GenerateModelIcao(ProposalViewModel model, INsdManager nsdManager)
        {
            var userDoa = await GeoLocationCache.GetUserDoa(Uow, ApplicationContext.GetUserId());
            var context = await GetProposalContextAsync(model, userDoa);

            await nsdManager.UpdateContext(model, context);

            var validateResult = await nsdManager.Validate(model, context);
            if (validateResult.HasErrors)
                return StepResult.Fail(validateResult.ErrorMessages.Count > 0 ? validateResult.ErrorMessages.FirstOrDefault() : Resources.InternalErrorValidation);

            var icaoResult = await nsdManager.GenerateIcao(model, context);
            if (!icaoResult)
                return StepResult.Fail(Resources.ErrorGeneratingICAO);

            return StepResult.Succeed();
        }

        /// <summary>
        /// The Notam Disseminater process action will try to create a Notam and will warranty:
        /// 1- The (year, series, number) combination of the new Notam is unique, until series rolls. 
        /// 2- The Notam is successfully distributed to the distribution queue.
        /// 3- The last series number is incremented without conflicts and will rollback to zero if it reaches the 'maxSeriesNumber' parameter.
        /// 4- If anything fails, the whole transaction is rolled back. The process is re-tried a max number of times as per the 'maxRetries' parameter.
        /// </summary>
        /// <param name="proposal">Proposal to convert to Notam</param>
        /// <param name="maxSeriesNumber">Max series number to roll back to zero.</param>
        /// <returns>A DisseminateResult object containing the error massesage if the process failed.</returns>
        private async Task<StepResult> DisseminateProposal(Proposal proposal, int maxSeriesNumber)
        {
            var result = ValidateProposalToDisseminate(proposal);

            if (!result.Succeeded)
                return result;

            var coordinator = DisseminationCoordinator.Create(Uow, proposal.Year, maxSeriesNumber);
            try
            {
                result = await coordinator.Disseminate(proposal.Series[0], n => ExecuteProposalDissemination(proposal, n));
                if (result.Succeeded)
                {
                    coordinator.CommitGroup();
                }
                else
                {
                    coordinator.DiscardGroup();
                }
                return result;
            }
            catch (NotamXmlValidationError e)
            {
                coordinator.DiscardGroup();

                var message = $"{Resources.NotamXmlValidationFailed}\n{e.Message}";
                Logger.LogWarn(GetType(), message);

                // abort cycle because the NOTAM XML is not valid (avoids System Down on TaskEngine)
                return StepResult.Fail(message);
            }
            catch (Exception e)
            {
                coordinator.DiscardGroup();

                Logger.LogError(e, GetType(), $"Dissemination of NOTAM failed for proposal with ID: {proposal.Id}");

                return StepResult.Fail(e.Message);
            }
        }

        private async Task<StepResult> ExecuteProposalDissemination(Proposal proposal, int seriesNumber)
        {
            proposal.Number = seriesNumber;

            // update the proposal to disseminate
            await UpdateProposalToDisseminate(Uow, proposal);

            // generate Notam from proposal
            var notamId = await CreateNotamFromProposal(Uow, proposal);

            // place disseminate task request in the queue
            QueueDisseminateNotamMessage(Uow, notamId);

            return StepResult.Succeed();
        }

        private void QueueDisseminateNotamMessage(IUow uow, Guid notamId)
        {
            var settingValue = uow.ConfigurationValueRepo.GetByCategoryandName("NOTAM", "HubDissemination")?.Value;
            if (!"off".Equals(settingValue))
            {
                _dashboardContext.GetQueuePublisher().Publish(uow, CreateMtoForNotamDissemination(notamId));
            }
        }

        static MessageTransferObject CreateMtoForNotamDissemination(Guid notamId)
        {
            var mto = new MessageTransferObject();

            // add unique identifier
            mto.Add(MessageDefs.UniqueIdentifierKey, Guid.NewGuid().ToString(), DataType.String);

            // create Message type
            mto.Add(MessageDefs.MessageTypeKey, (byte)MessageType.WorkFlowTask, DataType.Byte);

            var messageId = (int)MessageTransferObjectId.DisseminateNotam;

            // create inbound type
            mto.Add(MessageDefs.MessageIdKey, messageId, DataType.Int32);

            // add unique identifier
            mto.Add(MessageDefs.EntityIdentifierKey, notamId, DataType.String);

            // date/time when the message was sent
            mto.Add(MessageDefs.MessageTimeKey, DateTime.UtcNow.RemoveSeconds(), DataType.DateTime);

            return mto;
        }

        private async Task<Guid> CreateNotamFromProposal(IUow uow, Proposal proposal)
        {
            if (proposal.ProposalType == NotamType.N)
            {
                if (uow.NotamRepo.CheckForNewNotamWithProposalId(proposal.Id))
                {
                    throw new Exception($"Attempt to create a duplicated NOTAMN on a Proposal with ProposalId: {proposal.Id}");
                }
            }

            var toDisseminate = new Notam();

            // copy all properties including (NotamId)
            proposal.CopyPropertiesTo(toDisseminate);

            toDisseminate.Id = Guid.NewGuid();
            toDisseminate.ProposalId = proposal.Id;
            toDisseminate.Type = proposal.ProposalType;

            toDisseminate.ItemE = AftnHelper.ReformatItemE(proposal.ItemE);
            toDisseminate.ItemEFrench = AftnHelper.ReformatItemE(proposal.ItemEFrench);

            var utcNow = DateTime.UtcNow;
            //Note: We are using [ItemX = ReplaceNotam=1] to determine when a NOF operator is replacing/cancelling a NOTAM directly from the NOTAM panel
            if (proposal.ItemX == "ReplaceNotam=1")
            {
                toDisseminate.Received = utcNow;
            }
            else
            {
                if (proposal.OrganizationType == OrganizationType.Nof)
                {
                    var latestParkedProposal = await Uow.ProposalHistoryRepo.GetLastParkedProposalAsync(proposal.Id);
                    if (latestParkedProposal != null)
                    {
                        toDisseminate.Received = latestParkedProposal.Received;
                    }
                    else
                    {
                        var latestDisseminatedProposal = await Uow.ProposalHistoryRepo.GetLastDisseminatedProposalAsync(proposal.Id);
                        toDisseminate.Received = latestDisseminatedProposal != null ? latestDisseminatedProposal.Received : proposal.Received;
                    }
                }
                else
                {
                    var latestSubmittedProposal = await Uow.ProposalHistoryRepo.GetLastSubmittedProposalAsync(proposal.Id);
                    if (latestSubmittedProposal != null)
                    {
                        toDisseminate.Received = latestSubmittedProposal.Received;
                    }
                }
            }

            toDisseminate.Coordinates = proposal.Coordinates;

            if (IsFirNotam(proposal))
                toDisseminate.EffectArea = CalculateFirsEffectArea(proposal.ItemA);
            else
                toDisseminate.EffectArea = CalculateCircleEffectArea(proposal.Coordinates, proposal.Radius);

            toDisseminate.Published = utcNow;
            if (toDisseminate.Type != NotamType.N)
            {
                toDisseminate.ReferredNotamId = BuildNotamId(proposal.ReferredSeries, proposal.ReferredNumber, proposal.ReferredYear);
                var referredNotam = await Uow.NotamRepo.GetReferredNotamAsync(proposal.ReferredSeries, proposal.ReferredNumber, proposal.ReferredYear);
                if (referredNotam == null)
                    throw new NullReferenceException("This referred Notam was not found in the Database: " + toDisseminate.ReferredNotamId);
                toDisseminate.RootId = referredNotam.Id;
            }
            else
                toDisseminate.RootId = toDisseminate.Id;

            if (toDisseminate.Type == NotamType.C)
            {
                toDisseminate.ItemD = null;
                toDisseminate.ItemF = null;
                toDisseminate.ItemG = null;
                toDisseminate.EndValidity = null;
                toDisseminate.EffectiveEndValidity = proposal.StartActivity; // EffectiveEndValidity must be the same as the StartActivity
            }

            var xmlValidationResult = NDS.Common.Xml.XmlSerializerExtensions.ValidateNotamXml(toDisseminate);
            if (!string.IsNullOrEmpty(xmlValidationResult))
                throw new NotamXmlValidationError(xmlValidationResult);

            // save the Icao Text in the NOTAM
            toDisseminate.IcaoText = ICAOFormatUtils.Generate(toDisseminate, true);

            await RollNotamNumber(uow, proposal.Series, proposal.Number, proposal.Year);
            await UpdateReferredNotam(uow, proposal, toDisseminate.Id);

            uow.NotamRepo.Add(toDisseminate);

            await uow.SaveChangesAsync();

            return toDisseminate.Id;
        }

        static async Task UpdateReferredNotam(IUow uow, Proposal proposal, Guid replaceId)
        {
            if (proposal.ProposalType != NotamType.N)
            {
                var referred = await uow.NotamRepo.GetReferredNotamAsync(proposal.ReferredSeries, proposal.ReferredNumber, proposal.ReferredYear);
                if (referred != null)
                {
                    if (referred.ReplaceNotamId.HasValue)
                    {
                        throw new Exception($"Trying to replace twice NOTAM ({ICAOTextUtils.FormatNotamId(proposal.ReferredSeries, proposal.ReferredNumber, proposal.ReferredYear)})");
                    }
                    referred.ReplaceNotamId = replaceId;
                    referred.EffectiveEndValidity = proposal.StartActivity;
                    uow.NotamRepo.Update(referred);
                }
                else
                {
                    throw new Exception($"Invalid referred NOTAM ({ICAOTextUtils.FormatNotamId(proposal.ReferredSeries, proposal.ReferredNumber, proposal.ReferredYear)})");
                }
            }
        }

        static async Task RollNotamNumber(IUow uow, string series, int number, int year)
        {
            var rolled = await uow.NotamRepo.GetReferredNotamAsync(series, number, year);
            if (rolled != null)
            {
                rolled.NumberRolled = true;
            }
        }

        /// <summary>
        /// Calculates the Effect Area based on center and radius
        /// </summary>
        private DbGeography CalculateCircleEffectArea(string coordinates, int radius)
        {
            var dmsLocation = DMSLocation.FromDMS(coordinates);
            if (dmsLocation == null)
                throw new Exception($"Invalid DMS coordinates ({coordinates}) found.");

            var latitude = dmsLocation.Latitude.AsDecimal;
            var longitude = dmsLocation.Longitude.AsDecimal;
            var point = DbGeography.PointFromText($"POINT({longitude} {latitude})", CommonDefinitions.Srid); // POINT(long, lat)

            return point.Buffer(radius * 1852.0);
        }

        /// <summary>
        /// Calculates the Effect Area based on firs
        /// </summary>
        private DbGeography CalculateFirsEffectArea(string itemA)
        {
            DbGeography geo = null;
            foreach (var d in SplitFirs(itemA))
            {
                var firGeo = Uow.SdoCacheRepo.GetFirGeography(d);
                geo = geo != null ? geo.Union(firGeo) : firGeo;
            }
            return geo;
        }

        const int FirRadius = 999;
        private bool IsFirNotam(INotam notam) => notam.Radius == FirRadius && SplitFirs(notam.ItemA).All(f => Uow.SdoCacheRepo.FirExists(f));
        static string[] SplitFirs(string itemA) => (itemA ?? "").Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

        private async Task UpdateProposalToDisseminate(IUow uow, Proposal proposal)
        {
            if (proposal.ProposalType == NotamType.C)
                proposal.Status = NotamProposalStatusCode.Terminated;

            proposal.Received = DateTime.UtcNow.RemoveSeconds();
            proposal.UserId = ApplicationContext.GetUserId();
            proposal.ModifiedByUsr = ApplicationContext.GetUserName();
            proposal.NotamId = BuildNotamId(proposal.Series, proposal.Number, proposal.Year);

            if (!proposal.StartActivity.HasValue)
                proposal.StartActivity = DateTime.UtcNow.RemoveSeconds();

            if (proposal.Id > 0)
                uow.ProposalRepo.Update(proposal);
            else
                uow.ProposalRepo.Add(proposal);

            uow.ProposalHistoryRepo.Add(HistoryFromProposal(proposal));

            await uow.SaveChangesAsync();
        }

        private StepResult ValidateSystemStatus()
        {
            var systemStatus = Uow.SystemStatusRepo.GetStatus();
            switch (systemStatus.Status)
            {
                case SystemStatusEnum.NDSDown:
                    return StepResult.Fail(Resources.CannotDisseminateWhenSystemDown);
                case SystemStatusEnum.CriticalError:
                    return StepResult.Fail(Resources.CannotDisseminateWhenSystemDown);
                default:
                    return StepResult.Succeed();
            }
        }

        static StepResult ValidateProposalToDisseminate(Proposal proposal)
        {
            if (string.IsNullOrEmpty(proposal.Series) || !char.IsLetter(proposal.Series[0]))
                return StepResult.Fail(Resources.ProposalSerieError);
            return StepResult.Succeed();
        }
    }

    class NotamXmlValidationError : Exception
    {
        public NotamXmlValidationError(string message) : base(message)
        {
        }
    }
}

﻿import { Component, OnInit, OnDestroy, Inject, Injector } from '@angular/core'
import { FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms'

import { DataService } from '../../shared/data.service'
import { LocationService } from '../../shared/location.service'
import { TwyClsdService } from './twyclsd.service'
import { ISdoSubject, ISelectOptions } from '../../shared/data.model'
import { ITwyClsdReason, ITaxiwaySubject, ITwyClsdReasonLanguage} from './twyclsd.model'

import { TOASTR_TOKEN, Toastr } from './../../../common/toastr.service';

import { LocaleService, TranslationService } from 'angular-l10n';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
    templateUrl: '/app/dashboard/nsds/twyclsd/twyclsd.component.html'
})
export class TwyClsdComponent implements OnInit, OnDestroy {
    action: string = "";
    token: any = null;
    nsdForm: FormGroup;
    nsdFrmGroup: FormGroup;
    isReadOnly: boolean = false;
    leafletmap: any;
    isBilingualRegion: boolean = false;

    isFormLoadingData: boolean;
    subjectSelected: ISdoSubject = null;
    subjectId: string = '';
    subjects: ISdoSubject[] = [];
    taxiwayId: string = '-1';

    taxiwaySelected: ITaxiwaySubject = null;
    taxiways: ITaxiwaySubject[] = [];
    taxiwaysSelected: ITaxiwaySubject[] = [];
    taxiwayComboList: ISelectOptions[] = [];
     
    closureReasonId: number = -1;
    closureReasonFreeText: string = '';
    closureReasonFreeTextFr: string = '';

    twyClsdReasons: ITwyClsdReasonLanguage[] = [];

    closureStatus: string = '';
    allTaxiwaysSelected: boolean = false;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private injector: Injector,
        private fb: FormBuilder,
        private locationService: LocationService,
        private twyClsdService: TwyClsdService,
        private dataService: DataService,
        public locale: LocaleService,
        public translation: TranslationService) {

        this.token = this.injector.get('token');
        this.isReadOnly = this.injector.get('isReadOnly');
        this.nsdForm = this.injector.get('form');
        this.action = this.injector.get('type');
    }

    ngOnInit() {
        this.isFormLoadingData = !(!this.token.subjectId);
        const app = this.twyClsdService.app;
        this.leafletmap = app.leafletmap;

        this.closureReasonId = this.token.closureReasonId || -1;
        this.closureReasonFreeText = this.token.closureReasonFreeText || '';
        this.closureReasonFreeTextFr = this.token.closureReasonFreeTextFr || '';

        let frmArr = <FormArray>this.nsdForm.controls['frmArr'];
        if (frmArr.length === 0) {
            if (!this.isFormLoadingData) {
                this.subjectId = '';
                this.allTaxiwaysSelected = false;
                this.taxiways = [];

                this.configureReactiveForm();
                this.twyClsdService.getTwyClsdReasons()
                    .subscribe((reasons: ITwyClsdReason[]) => {
                        const culture = this.twyClsdService.app.cultureInfo;
                        this.twyClsdReasons = reasons.map(twr => this.mapTaxiWayReasonToLanguage(culture, twr));
                        this.isFormLoadingData = !(!this.token.subjectId);
                        if (this.isFormLoadingData) this.setFirstSelectItemInCombo(this.twyClsdReasons, this.token.closureReasonId);
                        else this.setFirstSelectItemInCombo(this.twyClsdReasons, 0);
                        if (!this.isReadOnly) {
                            this.enableReasonSelector();
                        }
                        this.isFormLoadingData = false;
                    });
                this.loadMapLocations(app.doaId, false);
            } else {
                this.subjectId = this.token.subjectId || '';
                this.allTaxiwaysSelected = this.token.allTaxiwaysSelected || false;
                this.taxiwaysSelected = this.token.allTaxiways || [];
                this.configureReactiveFormLoading();
                this.twyClsdService.getTwyClsdReasons()
                    .subscribe((reasons: ITwyClsdReason[]) => {
                        const culture = this.twyClsdService.app.cultureInfo;
                        this.twyClsdReasons = reasons.map(twr => this.mapTaxiWayReasonToLanguage(culture, twr));
                        this.isFormLoadingData = !(!this.token.subjectId);
                        if (this.isFormLoadingData) this.setFirstSelectItemInCombo(this.twyClsdReasons, this.token.closureReasonId);
                        else this.setFirstSelectItemInCombo(this.twyClsdReasons, 0);
                        if (!this.isReadOnly) {
                            this.enableReasonSelector();
                        }
                        this.dataService.getSdoSubject(this.token.subjectId)
                            .subscribe((sdoSubjectResult: ISdoSubject) => {
                                this.subjectSelected = sdoSubjectResult;
                                this.subjects = [];
                                this.subjects.push(sdoSubjectResult);
                                this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);
                                this.loadMapLocations(app.doaId, true);
                                this.updateMap(sdoSubjectResult, true);

                                if (!this.isReadOnly) {
                                    this.enableReasonSelector();
                                }
                                this.twyClsdService.getSdoTwys(this.subjectId)
                                    .subscribe((sdoTwys: ITaxiwaySubject[]) => {
                                        this.taxiways = sdoTwys;
                                        this.prepareTaxiwayComboList();
                                        this.$ctrl('countSelectedTwys').setValue(this.countSelectedTwys);
                                        if (this.allTaxiwaysSelected) {
                                            this.pickTaxiway('0');
                                        } else {
                                            if (this.taxiwaysSelected.length > 0) {
                                                var lastId = this.taxiwaysSelected[this.taxiwaysSelected.length - 1].id;
                                                this.pickTaxiway(lastId);
                                            }
                                        }
                                        this.isFormLoadingData = false;
                                    });
                            });

                    });
                //this.configureReactiveFormLoading();
            }
        }
        else {
            /*
            this is required because of the grouping funtionallity. When the NsdFormGroup 
            was configured and any of items in the group has changed (new set of tokens values)
            the NsdForm Group needs to be updated with the new tokens values */
            this.subjectId = this.token.subjectId || '';

            this.closureReasonId = this.token.closureReasonId || -1;
            this.closureReasonFreeText = this.token.closureReasonFreeText || '';
            this.closureReasonFreeTextFr = this.token.closureReasonFreeTextFr || '';

            this.allTaxiwaysSelected = this.token.allTaxiwaysSelected || false;
            this.taxiwaysSelected = this.token.allTaxiways || [];

            this.dataService.getSdoSubject(this.token.subjectId)
                .subscribe((sdoSubjectResult: ISdoSubject) => {
                    this.subjectSelected = sdoSubjectResult;
                    this.subjects = [];
                    this.subjects.push(sdoSubjectResult);
                    this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);
                    this.loadMapLocations(app.doaId, true);
                    this.updateMap(sdoSubjectResult, true);

                    if (!this.isReadOnly) {
                        this.enableReasonSelector();
                    }
                    this.twyClsdService.getSdoTwys(this.subjectId)
                        .subscribe((sdoTwys: ITaxiwaySubject[]) => {
                            this.taxiways = sdoTwys;
                            this.prepareTaxiwayComboList();
                            this.$ctrl('countSelectedTwys').setValue(this.countSelectedTwys);
                            if (this.allTaxiwaysSelected) {
                                this.pickTaxiway('0');
                            } else {
                                if (this.taxiwaysSelected.length > 0) {
                                    var lastId = this.taxiwaysSelected[this.taxiwaysSelected.length - 1].id;
                                    this.pickTaxiway(lastId);
                                }
                            }
                        });
                });

            this.updateReactiveForm(frmArr);
        }

        this.isFormLoadingData = false;

    }

    ngOnDestroy() {
        //if (this.leafletmap) {
        //    this.leafletmap.dispose();
        //    this.leafletmap = null;
        //}
    }

    mapTaxiWayReasonToLanguage(culture: string, v: ITwyClsdReason): ITwyClsdReasonLanguage {
        const text = culture === "en" ? v.text : v.textFr;
        return { id: v.id, text: text };
    }

    public getTaxiwayName(twy: ITaxiwaySubject): string {
        return twy.sdoEntityName.toUpperCase() + ' ' + twy.designator.toUpperCase();
    }

    public getClosureText(twy: ITaxiwaySubject): string {
        if (twy.full) return this.translation.translate('TableHeaders.FullClosure');
        else return this.translation.translate('TableHeaders.PartialClosure');
    }

    public getBetweenText(twy: ITaxiwaySubject): string {
        if (twy.full) return "";
        else {
            if (this.isBilingualRegion) {
                return twy.start.toUpperCase() + " / " + twy.startFr.toUpperCase();
            } else {
                return twy.start.toUpperCase();
            }
        }
    }

    public getAndText(twy: ITaxiwaySubject): string {
        if (twy.full) return "";
        else {
            if (this.isBilingualRegion) {
                return twy.end.toUpperCase() + " / " + twy.endFr.toUpperCase();
            } else {
                return twy.end.toUpperCase();
            }
        }
    }

    public setClosureStatusText(full: boolean) {
        if (full) this.closureStatus = this.translation.translate('TableHeaders.FullClosure');
        else this.closureStatus = this.translation.translate('TableHeaders.PartialClosure');
    }

    //Enable functions
    private enableReasonSelector() {
        const options = { onlySelf: true, emitEvent: !this.isFormLoadingData };
        let reasonIdCtrl = this.$ctrl('grpClosure.closureReasonId');
        let reasonTextCtrl = this.$ctrl('grpClosure.closureReasonFreeText');
        let reasonTextCtrlFr = this.$ctrl('grpClosure.closureReasonFreeTextFr');
        let grpClosure = this.nsdFrmGroup.get('grpClosure');

        if (this.countSelectedTwys === 0) {
            this.closureReasonFreeText = '';
            this.closureReasonFreeTextFr = '';
            this.closureReasonId = 0;

            grpClosure.patchValue({ closureReasonId: this.closureReasonId }, options);
            grpClosure.patchValue({ closureReasonFreeText: this.closureReasonFreeText }, options);

            reasonIdCtrl.disable();
            reasonTextCtrl.disable();
            if (this.isBilingualRegion) {
                grpClosure.patchValue({ closureReasonFreeTextFr: this.closureReasonFreeTextFr }, options);
                reasonTextCtrlFr.disable();
            }
        } else {
            reasonIdCtrl.enable();
            if (reasonIdCtrl.value !== undefined && reasonIdCtrl.value !== 0) {
                const reasonValueText = this.twyClsdReasons.find(x => x.id === reasonIdCtrl.value).text.toLowerCase();
                if ((reasonValueText === 'other') || (reasonValueText === 'autre')) {
                    reasonTextCtrl.enable();
                    if (this.isBilingualRegion) {
                        reasonTextCtrlFr.enable();
                    }
                } else {
                    this.closureReasonFreeText = '';
                    this.closureReasonFreeTextFr = '';
                    grpClosure.patchValue({ closureReasonFreeText: this.closureReasonFreeText }, options);
                    reasonTextCtrl.disable();
                    if (this.isBilingualRegion) {
                        grpClosure.patchValue({ closureReasonFreeTextFr: this.closureReasonFreeTextFr }, options);
                        reasonTextCtrlFr.disable();
                    }
                }
            } else {
                this.closureReasonFreeText = '';
                this.closureReasonFreeTextFr = '';
                grpClosure.patchValue({ closureReasonFreeText: this.closureReasonFreeText }, options);
                reasonTextCtrl.disable();
                if (this.isBilingualRegion) {
                    grpClosure.patchValue({ closureReasonFreeTextFr: this.closureReasonFreeTextFr }, options);
                    reasonTextCtrlFr.disable();
                }
            }
        }
    }

    public isExcludeButtonDisabled() {
        if (this.isReadOnly) return true;
        if (this.allTaxiwaysSelected) return false;
        if (!this.taxiwaySelected) return true;
        var sel = this.taxiwaysSelected.find(x => x.id === this.taxiwaySelected.id);
        if (sel) return false; 
        else return true;
    }

    public isModifyButtonDisabled() {
        if (this.isReadOnly) return true;
        if (!this.taxiwaySelected) return true;
        if (this.allTaxiwaysSelected) return true;
        var sel = this.taxiwaysSelected.find(x => x.id === this.taxiwaySelected.id);
        if (sel) {
            if (this.validaClosureFields()) return false;
            else return true;
        }
        else return true;
    }

    public isIncludeButtonDisabled() {
        if (this.isReadOnly) return true;
        if (!this.taxiwaySelected) {
            if (!this.isAllTaxiwaysBeingProcessed()) return true;
            else if (this.allTaxiwaysSelected) return true;
            else return false;
        } else {
            var sel = this.taxiwaysSelected.find(x => x.id === this.taxiwaySelected.id);
            if (sel) return true;
            else {
                if (this.validaClosureFields()) return false;
                else return true;
            }
        }
    }

    public isFullClosureDisabled() {
        if (this.isReadOnly) return true;
        if (this.isAllTaxiwaysBeingProcessed()) return true;
        if (this.allTaxiwaysSelected) return true;
        return false;
    }

    public isTaxiwaySelectorDisabled() {
        if (this.allTaxiwaysSelected) return true;
        if (this.isReadOnly) return true;

        return false;
    }

    public isAllTaxiwaysBeingProcessed() {
        if (this.taxiwayId === '0') return true;
        return false;
    }

    public isEditing() {
        return (this.$ctrl('refreshIcao').value === 0);
    }

    public enableEditing() {
        this.$ctrl('refreshIcao').setValue(0);
    }

    public stopEditing() {
        this.$ctrl('refreshIcao').setValue(1);
    }

    public restoreTaxiwayValues() {
        if (this.isEditing() && this.taxiwaySelected) {
            var index = this.taxiways.findIndex(y => y.id === this.taxiwaySelected.id);
            this.taxiways[index].end = this.taxiwaySelected.end;
            this.taxiways[index].endFr = this.taxiwaySelected.endFr;
            this.taxiways[index].full = this.taxiwaySelected.full;
            this.taxiways[index].selected = this.taxiwaySelected.selected;
            this.taxiways[index].start = this.taxiwaySelected.start;
            this.taxiways[index].startFr = this.taxiwaySelected.startFr;
            this.stopEditing();
        }
    }

    public pickTaxiway(id: string) {
        var selected = this.taxiwaysSelected.find(x => x.id === id);
        if (selected) {
            this.taxiwaySelected = selected;
            if (this.isEditing()) {
                this.restoreTaxiwayValues();
            }

            //Restore control values
            var field = this.$ctrl('fullClosure');
            field.patchValue(this.taxiwaySelected.full);
            field = this.$ctrl('startClosure');
            field.patchValue(this.taxiwaySelected.start);
            field = this.$ctrl('startClosureFr');
            field.patchValue(this.taxiwaySelected.startFr);
            field = this.$ctrl('endClosure');
            field.patchValue(this.taxiwaySelected.end);
            field = this.$ctrl('endClosureFr');
            field.patchValue(this.taxiwaySelected.endFr);

            var twySel = this.$ctrl('taxiwayId');
            twySel.setValue(selected.id, {emiltEvent : true});
        }
    }

    public rowColor(id: string) {
        if (!this.taxiwaySelected) {
            if (this.allTaxiwaysSelected) return 'bg-submitted';
            else return '';
        } 
        if (this.taxiwaySelected.id === id) return 'bg-submitted';
        return '';
    }

    public buttonColor() {
        if (!this.isModifyButtonDisabled()) {
            var ctrlFC = this.$ctrl('fullClosure');
            if (ctrlFC.value !== this.taxiwaySelected.full) return 'btn-blue';

            var ctrlSC = this.$ctrl('startClosure');
            var ctrlAC = this.$ctrl('endClosure');
            var ctrlSCF = this.$ctrl('startClosureFr');
            var ctrlACF = this.$ctrl('endClosureFr');

            if (ctrlSC.value !== this.taxiwaySelected.start ||
                ctrlAC.value !== this.taxiwaySelected.end ||
                ctrlSCF.value !== this.taxiwaySelected.startFr ||
                ctrlACF.value !== this.taxiwaySelected.endFr) {
                return 'btn-blue';
            }
        }
        return 'btn-info';
    }

    private cleanTaxiwaysData() {
        for (var i = 0; i < this.taxiways.length; i++) {
            this.taxiways[i].end = '';
            this.taxiways[i].endFr = '';
            this.taxiways[i].start = '';
            this.taxiways[i].startFr = '';
            if (this.taxiways[i].id !== '0') {
                this.taxiways[i].full = false;
                this.taxiways[i].selected = false;
            } else {
                this.taxiways[i].full = true;
            }

        }
    }

    public includeTaxiway() {
        if (this.isAllTaxiwaysBeingProcessed()) {
            this.allTaxiwaysSelected = true;
            //Clean the selected list
            this.taxiwaysSelected = [];
            var twy = this.taxiways.find(t => t.id === '0');
            this.taxiwaysSelected.push(twy);

            this.$ctrl('allTaxiwaysSelected').patchValue(this.allTaxiwaysSelected);
            this.cleanTaxiwaysData();
            this.pickTaxiway(twy.id);

        } else {

            this.$ctrl('allTaxiwaysSelected').patchValue(this.allTaxiwaysSelected);
            var ctrl = this.$ctrl('startClosure');
            this.taxiwaySelected.start = ctrl.value;
            ctrl = this.$ctrl('startClosureFr');
            this.taxiwaySelected.startFr = ctrl.value;
            var ctrl = this.$ctrl('endClosure');
            this.taxiwaySelected.end = ctrl.value;
            ctrl = this.$ctrl('endClosureFr');
            this.taxiwaySelected.endFr = ctrl.value;
            ctrl = this.$ctrl('fullClosure');
            this.taxiwaySelected.full = ctrl.value;

            this.taxiwaySelected.selected = true;

            var twy = Object.assign({}, this.taxiwaySelected);
            this.taxiwaysSelected.push(twy);
            this.pickTaxiway(this.taxiwaySelected.id);
        }
        this.stopEditing();
        this.validateAndTrigger();
        this.$ctrl('allTaxiways').markAsDirty();
        //this.markFrmDirty();
    }

    public modifyTaxiway() {
        var ctrl = this.$ctrl('startClosure');
        this.taxiwaySelected.start = ctrl.value;
        ctrl = this.$ctrl('startClosureFr');
        this.taxiwaySelected.startFr = ctrl.value;
        ctrl = this.$ctrl('endClosure');
        this.taxiwaySelected.end = ctrl.value;
        ctrl = this.$ctrl('endClosureFr');
        this.taxiwaySelected.endFr = ctrl.value;
        ctrl = this.$ctrl('fullClosure');
        this.taxiwaySelected.full = ctrl.value;

        this.taxiwaySelected.selected = true;


        var index = this.taxiwaysSelected.findIndex(x => x.id === this.taxiwaySelected.id);
        this.taxiwaysSelected[index].start = this.taxiwaySelected.start;
        this.taxiwaysSelected[index].startFr = this.taxiwaySelected.startFr;
        this.taxiwaysSelected[index].end = this.taxiwaySelected.end;
        this.taxiwaysSelected[index].endFr = this.taxiwaySelected.endFr;
        this.taxiwaysSelected[index].full = this.taxiwaySelected.full;

        //Set values on taxiways list
        index = this.taxiways.findIndex(y => y.id === this.taxiwaySelected.id);
        this.taxiways[index].end = this.taxiwaySelected.end;
        this.taxiways[index].endFr = this.taxiwaySelected.endFr;
        this.taxiways[index].full = this.taxiwaySelected.full;
        this.taxiways[index].selected = this.taxiwaySelected.selected;
        this.taxiways[index].start = this.taxiwaySelected.start;
        this.taxiways[index].startFr = this.taxiwaySelected.startFr;

        this.stopEditing();

        this.validateAndTrigger();
        this.$ctrl('allTaxiways').markAsDirty();
        //this.markFrmDirty();
         
    }

    public excludeTaxiway() {
        if (this.allTaxiwaysSelected === true) {
            this.allTaxiwaysSelected = false;
            this.$ctrl('allTaxiwaysSelected').patchValue(this.allTaxiwaysSelected);
            this.taxiwaysSelected = [];
            this.taxiwaySelected = null;
            var field = this.$ctrl('taxiwayId');
            field.patchValue('');
            field = this.$ctrl('startClosure');
            field.patchValue('');
            field = this.$ctrl('endClosure');
            field.patchValue('');
            field = this.$ctrl('startClosureFr');
            field.patchValue('');
            field = this.$ctrl('endClosureFr');
            field.patchValue('');
            this.taxiwayId = '-1';
            this.setFirstSelectItemInTaxiways(this.taxiwayComboList, this.taxiwayId, false);

            this.closureStatus = this.translation.translate('TableHeaders.PartialClosure');

        } else {
            const allTwySelValue = this.$ctrl('allTaxiwaysSelected').value;
            if (allTwySelValue !== this.allTaxiwaysSelected)
                this.$ctrl('allTaxiwaysSelected').patchValue(this.allTaxiwaysSelected);

            this.taxiwaySelected.selected = false;
            this.taxiwaySelected.end = '';
            this.taxiwaySelected.endFr = '';
            this.taxiwaySelected.full = false;
            this.taxiwaySelected.start = '';
            this.taxiwaySelected.startFr = '';

            this.stopEditing();
            var index = this.taxiwaysSelected.findIndex(x => x.id === this.taxiwaySelected.id);
            this.taxiwaysSelected.splice(index, 1);
            if (this.taxiwaysSelected.length > 0) {
                this.pickTaxiway(this.taxiwaysSelected[0].id);
            } else {
                //Deleted the last one selected
                this.taxiwaySelected = null;
                var field = this.$ctrl('taxiwayId');
                field.patchValue('');
                field = this.$ctrl('startClosure');
                field.patchValue('');
                field = this.$ctrl('endClosure');
                field.patchValue('');
                field = this.$ctrl('startClosureFr');
                field.patchValue('');
                field = this.$ctrl('endClosureFr');
                field.patchValue('');

                this.taxiwayId = '-1';
                this.setFirstSelectItemInTaxiways(this.taxiwayComboList, this.taxiwayId, false);
                this.closureStatus = this.translation.translate('TableHeaders.PartialClosure');
            }
        }

        this.validateAndTrigger();
        this.$ctrl('allTaxiways').markAsDirty();
        //this.markFrmDirty();
    }

    private configureReactiveForm() {
        const frmArrControls = <FormArray>this.nsdForm.controls['frmArr'];
        this.nsdFrmGroup = this.fb.group({
            subjectId: [this.token.subjectId || '', [Validators.required]],
            subjectLocation: this.token.subjectLocation || '',
            taxiwayId: [{ value: this.taxiwayId, disabled: false }],
            fullClosure: [{ value: false, disabled: (this.isReadOnly) ? true : false }],
            startClosure: [{ value: '', disabled: false }],
            startClosureFr: [{ value: '', disabled: !this.isBilingualRegion }],
            endClosure: [{ value: '', disabled: false }],
            endClosureFr: [{ value: '', disabled: !this.isBilingualRegion }],
            grpClosure: this.fb.group({
                closureReasonId: [{ value: this.token.closureReasonId, disabled: !this.isFormLoadingData }],
                closureReasonFreeText: [{ value: this.token.closureReasonFreeText, disabled: !this.isFormLoadingData }],
                closureReasonFreeTextFr: [{ value: this.token.closureReasonFreeTextFr, disabled: !this.isFormLoadingData }],
            }),
            //allTaxiways: [{ value: this.taxiways, disabled: false }],
            allTaxiways: [{ value: this.taxiwaysSelected, disabled: false }],
            countSelectedTwys: [{ value: 0 }, [countSelectedTwysValidator]],
            allTaxiwaysSelected: [{ value: this.allTaxiwaysSelected, disabled: false }],
            refreshIcao: [{ value: 1 }, [refreshIcaoValidator]],
        });
        frmArrControls.push(this.nsdFrmGroup);
    }

    private configureReactiveFormLoading() {
        //Modify this to upload the data
        var disable = (this.isReadOnly) ? true : !this.isFormLoadingData;
        const frmArrControls = <FormArray>this.nsdForm.controls['frmArr'];
        this.nsdFrmGroup = this.fb.group({
            subjectId: [this.token.subjectId || '', [Validators.required]],
            subjectLocation: this.token.subjectLocation || '',
            taxiwayId: [{ value: this.taxiwayId, disabled: false }],
            fullClosure: [{ value: false, disabled: disable }],
            startClosure: [{ value: this.taxiwaySelected ? this.taxiwaySelected.start : '', disabled: disable }],
            startClosureFr: [{ value: this.taxiwaySelected ? this.taxiwaySelected.startFr : '', disabled: disable }],
            endClosure: [{ value: this.taxiwaySelected ? this.taxiwaySelected.end : '', disabled: disable }],
            endClosureFr: [{ value: this.taxiwaySelected ? this.taxiwaySelected.endFr : '', disabled: disable }],
            grpClosure: this.fb.group({
                closureReasonId: [{ value: this.token.closureReasonId, disabled: disable }],
                closureReasonFreeText: [{ value: this.token.closureReasonFreeText, disabled: disable }],
                closureReasonFreeTextFr: [{ value: this.token.closureReasonFreeTextFr, disabled: disable }],
            }),
            allTaxiways: [{ value: this.taxiwaysSelected, disabled: false }],
            countSelectedTwys: [{ value: this.countSelectedTwys }, [countSelectedTwysValidator]],
            allTaxiwaysSelected: [{ value: this.token.allTaxiwaysSelected, disabled: false }],
            refreshIcao: [{ value: 1 }, [refreshIcaoValidator]],
        });
        frmArrControls.push(this.nsdFrmGroup);
    }

    private updateReactiveForm(formArr: FormArray) {
        this.nsdFrmGroup = <FormGroup>formArr.controls[0];

        //It is better to update only those controls that tokens values have changed.
        this.updateFormControlValue("subjectId", this.token.subjectId);
        this.updateFormControlValue("subjectLocation", this.token.subjectLocation);
        this.updateFormControlValue("taxiwayId", this.taxiwayId);
        this.updateFormControlValue("fullClosure", false);
        this.updateFormControlValue("startClosure", this.taxiwaySelected ? this.taxiwaySelected.start : '');
        this.updateFormControlValue("startClosureFr", this.taxiwaySelected ? this.taxiwaySelected.startFr : '');
        this.updateFormControlValue("endClosure", this.taxiwaySelected ? this.taxiwaySelected.end : '');
        this.updateFormControlValue("endClosureFr", this.taxiwaySelected ? this.taxiwaySelected.endFr : '');
        this.updateFormControlValue("grpClosure.closureReasonId", this.token.closureReasonId);
        this.updateFormControlValue("grpClosure.closureReasonFreeText", this.token.closureReasonFreeText);
        this.updateFormControlValue("grpClosure.closureReasonFreeTextFr", this.token.closureReasonFreeTextFr);
        this.updateFormControlValue("allTaxiways", this.taxiwaysSelected);
        this.updateFormControlValue("countSelectedTwys", this.countSelectedTwys);
        this.updateFormControlValue("allTaxiwaysSelected", this.token.allTaxiwaysSelected);
    }

    private updateFormControlValue(controlName: string, value) {
        let control = this.nsdFrmGroup.get(controlName);
        if (control && control.value !== value) {
            control.setValue(value);
        }
    }

    private loadSdoSubjectTwy(sdoId: string) {
        if (sdoId) {
            let self = this;
            this.twyClsdService.getSdoTwys(sdoId)
                .subscribe((sdoTwys: ITaxiwaySubject[]) => {
                    this.taxiwaySelected = null;
                    this.taxiwaysSelected = [];

                    this.taxiways = sdoTwys;
                    this.prepareTaxiwayComboList();

                    this.$ctrl('countSelectedTwys').setValue(this.countSelectedTwys);
                    this.$ctrl('grpClosure').setValidators(finalClosureValidator(this.twyClsdReasons, this.isBilingualRegion));
                    this.$ctrl('grpClosure').updateValueAndValidity();
                });
        }
    }

    private prepareTaxiwayComboList() {
        this.taxiwayComboList = [];

        if (this.taxiways.length > 1 && this.taxiways.findIndex(t=> t.id === '0') === -1) {
            var twyAll: ITaxiwaySubject = {
                designator: 'TWYS',
                end: '',
                endFr: '',
                full: true,
                id: '0',
                mid: '0',
                sdoEntityName: 'ALL',
                selected: true,
                start: '',
                startFr: '',
                index: 0
            }
            this.taxiways.unshift(twyAll);
        }
        this.taxiwayComboList = this.taxiways.map((item: ITaxiwaySubject) => {
            return {
                id: item.id,
                text: item.sdoEntityName.toUpperCase() + ' ' + item.designator.toUpperCase(),
            }
        });
        //Add empty slot
        this.taxiwayComboList.unshift({
            id: "-1",
            text: "",
        });
    }

    $ctrl(name: string): AbstractControl {
        return this.nsdFrmGroup.get(name);
    }

    public closureReasonOptions = {
        width: "100%",
        placeholder: {
            id: '-1',
            text: this.translation.translate('Nsd.ClosurePlaceholder')
        }
    }

    public taxiwaysOptions = {
        width: "100%",
        placeholder: {
            id: '-1',
            text: this.translation.translate('Nsd.TaxiwayPlaceholder')
        }
    }

    public sdoSubjectWithTwysOptions(): any {
        const self = this;
        const app = this.twyClsdService.app;
        return {
            minimumInputLength: 2,
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.SdoSubjectPlaceholder')
            },
            language: {
                inputTooShort: function (args) {
                    return self.translation.translate('Schedule.InputTooShort');
                },
            },
            ajax: {
                type: 'GET',
                url: app.apiUrl + "taxiways/filterwithtwy",
                dataType: 'json',
                delay: 500,
                headers: {
                    "RequestVerificationToken": app.antiForgeryTokenValue
                },
                data: function (parms, page) {
                    return {
                        Query: parms.term,
                        Size: 200,
                        doaId: app.doaId //set by the app controller
                    }
                },
                processResults: function (data) {
                    return {
                        results: data
                    }
                },
                cache: true
            },
            templateResult: function (data: any) {
                if (data.loading) {
                    return data.text;
                }
                return self.dataService.getSdoExtendedName(data);
            },
            templateSelection: function (selection: ISdoSubject) {
                if (selection && selection.id === "-1") {
                    return selection.text;
                }

                self.updateSubjectSelected(selection);
                return self.dataService.getSdoExtendedName(selection);
            },
        }
    }

    private updateSubjectSelected(sdoSubject: ISdoSubject) {
        const subjectId = this.$ctrl('subjectId').value;
        if (subjectId === sdoSubject.id) {
            //for some weird reasons select2 templateSelection is called multiples 
            //times after selection
            return;
        }
        if (sdoSubject.id !== subjectId) {
            const subjectCtrl = this.$ctrl('subjectId');
            subjectCtrl.setValue(sdoSubject.id);
            subjectCtrl.markAsDirty();
        }
        this.subjectSelected = sdoSubject;
        if (!sdoSubject.subjectGeo) {
            this.dataService.getSdoSubject(sdoSubject.id)
                .subscribe((sdoSubjectResult: ISdoSubject) => {
                    this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);
                    this.loadSdoSubjectTwy(sdoSubject.id);

                    this.updateMap(sdoSubjectResult, true);
                });
        } else {
            this.setBilingualRegion(sdoSubject.isBilingualRegion);
            this.loadSdoSubjectTwy(sdoSubject.id);
        }

    }

    public get colBetweenCaption(): string {
        if (this.isBilingualRegion) return this.translation.translate('TableHeaders.BetweenBiligual');
        else return this.translation.translate('TableHeaders.Between');
    }

    public get colAndCaption(): string {
        if (this.isBilingualRegion) return this.translation.translate('TableHeaders.AndBiligual');
        else return this.translation.translate('TableHeaders.And');
    }

    //OnChange functions
    public fullClosureChange() {
        if (this.isFormLoadingData || this.isReadOnly) return;
        if (this.isAllTaxiwaysBeingProcessed() || this.allTaxiwaysSelected) {
            var ctrl = this.$ctrl('fullClosure');
            const options = { onlySelf: true, emitEvent: !this.isFormLoadingData };
            if (ctrl.value === false) ctrl.patchValue(true, options);
            return;
        }

        var ctrl = this.$ctrl('fullClosure');
        if (ctrl && this.taxiwaySelected) {
            this.setClosureStatusText(ctrl.value);
            this.enableEditing();
            if (ctrl.value) {
                ctrl = this.$ctrl('startClosure');
                ctrl.patchValue('');
                ctrl.disable();
                ctrl = this.$ctrl('endClosure');
                ctrl.patchValue('');
                ctrl.disable();
                if (this.isBilingualRegion) {
                    ctrl = this.$ctrl('startClosureFr');
                    ctrl.patchValue('');
                    ctrl.disable();
                    ctrl = this.$ctrl('endClosureFr');
                    ctrl.patchValue('');
                    ctrl.disable();
                }
            } else {
                ctrl = this.$ctrl('startClosure');
                ctrl.enable();
                ctrl = this.$ctrl('endClosure');
                ctrl.enable();
                if (this.isBilingualRegion) {
                    ctrl = this.$ctrl('startClosureFr');
                    ctrl.enable();
                    ctrl = this.$ctrl('endClosureFr');
                    ctrl.enable();
                }
            }
        }
    }

    public closureReasonChanged($event) {
        if ($event.value !== "-1") {
            let reasonValueId: number = +$event.value;
            if (reasonValueId === this.closureReasonId) {
                //for some weird reasons select2 templateSelection is called multiples 
                //times after selection
                return;
            }
            if (reasonValueId !== -1) {
                const options = { onlySelf: true, emitEvent: false };

                const reasonIdCtrl = this.$ctrl('grpClosure.closureReasonId');
                const reasonValueText = this.twyClsdReasons.find(x => x.id === reasonValueId).text.toLowerCase();
                const reasonTextCtrl = this.$ctrl('grpClosure.closureReasonFreeText');
                const reasonTextCtrlFr = this.$ctrl('grpClosure.closureReasonFreeTextFr');
                if ((reasonValueText === 'other') || (reasonValueText === 'autre')) {
                    // Change the free text
                    reasonTextCtrl.enable();
                    if (this.isBilingualRegion) {
                        reasonTextCtrlFr.enable();
                    }
                } else {
                    this.nsdFrmGroup.get('grpClosure').patchValue({ closureReasonFreeText: '' }, options);
                    reasonTextCtrl.disable();
                    if (this.isBilingualRegion) {
                        this.nsdFrmGroup.get('grpClosure').patchValue({ closureReasonFreeTextFr: '' }, options);
                        reasonTextCtrlFr.disable();
                    }
                }
                this.closureReasonId = reasonValueId;
                this.nsdFrmGroup.get('grpClosure').patchValue({ closureReasonId: reasonValueId }, options);
                if (!this.isFormLoadingData) {
                    this.validateAndTrigger();
                    this.$ctrl('grpClosure.closureReasonId').markAsDirty();
                    //this.markFrmDirty();
                }
            }
        }
    }

    public closureReasonTextChange() {
        this.validateAndTrigger();
        this.$ctrl('grpClosure.closureReasonFreeText').markAsDirty();
        //this.markFrmDirty();
    }

    public closureReasonTextFrChange() {
        this.validateAndTrigger();
        this.$ctrl('grpClosure.closureReasonFreeTextFr').markAsDirty();
        //this.markFrmDirty();
    }

    public selectedTaxiwayChanged($event) {
        if (this.isReadOnly) return;
        var twySel = this.$ctrl('taxiwayId');
        if (twySel) {
            var val: string = $event.value;
            //if (val === twySel.value) return;
            this.taxiwayId = val;

            if (val === '-1') return;
            if (val === '0') {
                //All taxiways
                var field = this.$ctrl('startClosure');
                field.patchValue('');
                field.disable();
                field = this.$ctrl('endClosure');
                field.patchValue('');
                field.disable();
                field = this.$ctrl('startClosureFr');
                field.patchValue('');
                field.disable();
                field = this.$ctrl('endClosureFr');
                field.patchValue('');
                field.disable();
                field = this.$ctrl('fullClosure');
                field.patchValue(true);
                field.disable();
                this.taxiwaySelected = this.taxiways.find(x => x.id === val);

            } else {
                var already = this.taxiwaysSelected.find(x => x.id === val);
                if (already) {
                    //Update the fields
                    var field = this.$ctrl('fullClosure');
                    if (field.disabled) field.enable();
                    field.patchValue(already.full);

                    field = this.$ctrl('startClosure');
                    if (field.disabled) field.enable();
                    if (!already.full) field.patchValue(already.start);
                    else field.patchValue('');

                    field = this.$ctrl('endClosure');
                    if (field.disabled) field.enable();
                    if (!already.full) field.patchValue(already.end);
                    else field.patchValue('');

                    if (this.isBilingualRegion) {
                        field = this.$ctrl('startClosureFr');
                        if (field.disabled) field.enable();
                        if (!already.full) field.patchValue(already.startFr);
                        else field.patchValue('');
                        field = this.$ctrl('endClosureFr');
                        if (field.disabled) field.enable();
                        if (!already.full) field.patchValue(already.endFr);
                        else field.patchValue('');
                    } else {
                        field = this.$ctrl('startClosureFr');
                        if (field.disabled) field.enable();
                        field.patchValue('');
                        field = this.$ctrl('endClosureFr');
                        if (field.disabled) field.enable();
                        field.patchValue('');
                    }

                    this.setClosureStatusText(already.full);

                } else {
                    var field = this.$ctrl('startClosure');
                    field.patchValue('');
                    if (field.enabled) field.disable();
                    field = this.$ctrl('endClosure');
                    field.patchValue('');
                    if (field.enabled) field.disable();
                    field = this.$ctrl('startClosureFr');
                    field.patchValue('');
                    if (field.enabled) field.disable();
                    field = this.$ctrl('endClosureFr');
                    field.patchValue('');
                    if (field.enabled) field.disable();
                    field = this.$ctrl('fullClosure');
                    if (field.disabled) field.enable();
                    field.patchValue(true);

                    this.closureStatus = this.translation.translate('TableHeaders.PartialClosure');
                }
                if (this.isEditing()) {
                    this.restoreTaxiwayValues();
                }
                this.taxiwaySelected = this.taxiways.find(x => x.id === val);
                this.fullClosureChange();
                this.stopEditing();
            }
        }
    }

    public startClosureChange(i: number) {
        if (this.isFormLoadingData || this.isReadOnly) return;
        this.enableEditing();
    }

    public startClosureChangeFr(i: number) {
        if (this.isFormLoadingData || this.isReadOnly) return;
        this.enableEditing();
    }

    public endClosureChange(i: number) {
        if (this.isFormLoadingData || this.isReadOnly) return;
        this.enableEditing();
    }

    public endClosureChangeFr(i: number) {
        if (this.isFormLoadingData || this.isReadOnly) return;
        this.enableEditing();
    }

    private getSelectedTaxiwayInfo(): ITaxiwaySubject {
        if (this.taxiwaySelected) {
            if (this.taxiwaySelected.id === '-1') {
                return this.taxiways.find(t => t.id === '-1');
            } else if (this.taxiwaySelected.id === '0') {
                return this.taxiways.find(t => t.id === '0');
            } else {
                var twy = this.taxiwaysSelected.find(t => t.id === this.taxiwaySelected.id);
                return (!twy) ? this.taxiways.find(t => t.id === this.taxiwaySelected.id) : twy;
                //if (!twy) return this.taxiways.find(t => t.id === this.taxiwaySelected.id);
                //else return twy;
            }
        }
        return null;
    }

    //Validation functions
    public validaClosureFields(): boolean {
        if (this.validateStartClosure() && this.validateStartClosureFr() && this.validateEndClosure() && this.validateEndClosureFr()) return true;
        return false;
    }

    public validateStartClosure(): boolean {
        if (this.isFormLoadingData) return true;
        if (!this.taxiwaySelected) return true;
        var taxiway = this.getSelectedTaxiwayInfo();
        if (!taxiway) return true;
        var ctrl = this.$ctrl('startClosure');
        if (taxiway.id === '0') {
            if (ctrl.value === '') return true;
        } else {
            var fullCtrl = this.$ctrl('fullClosure');
            if (fullCtrl.value === true) {
                if (ctrl.value === '') return true;
            } else {
                if (ctrl.value !== '') return true;
            }
        }
        return false;
    }

    public validateStartClosureFr(): boolean {
        if (this.isFormLoadingData) return true;
        if (!this.taxiwaySelected) return true;
        var taxiway = this.getSelectedTaxiwayInfo();
        if (!taxiway) return true;
        var ctrl = this.$ctrl('startClosureFr');
        if (this.isBilingualRegion) {
            if (taxiway.id === '0') {
                if (ctrl.value === '') return true;
            } else {
                var fullCtrl = this.$ctrl('fullClosure');
                if (fullCtrl.value === true) {
                    if (ctrl.value === '') return true;
                } else {
                    if (ctrl.value !== '') return true;
                }
            }
        } else {
            if (ctrl.value === '') return true;
        }
        return false;
    }

    public validateEndClosure(): boolean {
        if (this.isFormLoadingData) return true;
        if (!this.taxiwaySelected) return true;
        var taxiway = this.getSelectedTaxiwayInfo();
        if (!taxiway) return true;
        var ctrl = this.$ctrl('endClosure');
        if (taxiway.id === '0') {
            if (ctrl.value === '') return true;
        } else {
            var fullCtrl = this.$ctrl('fullClosure');
            if (fullCtrl.value === true) {
                if (ctrl.value === '') return true;
            } else {
                if (ctrl.value !== '') return true;
            }
        }
        return false;
    }

    public validateEndClosureFr(): boolean {
        if (this.isFormLoadingData) return true;
        if (!this.taxiwaySelected) return true;
        var taxiway = this.getSelectedTaxiwayInfo();
        if (!taxiway) return true;
        var ctrl = this.$ctrl('endClosureFr');
        if (this.isBilingualRegion) {
            if (taxiway.id === '0') {
                if (ctrl.value === '') return true;
            } else {
                var fullCtrl = this.$ctrl('fullClosure');
                if (fullCtrl.value === true) {
                    if (ctrl.value === '') return true;
                } else {
                    if (ctrl.value !== '') return true;
                }
            }
        } else {
            if (ctrl.value === '') return true;
        }
        return false;
    }

    public validateSubjectId() {
        const fc = this.$ctrl('subjectId');
        return fc.value || this.nsdFrmGroup.pristine;
    }

    public isRequiredReasonFreeText(): boolean {
        const reasonIdCtrl = this.$ctrl('grpClosure.closureReasonId');
        if (reasonIdCtrl.value === -1) return false;
        let elem = this.twyClsdReasons.find(x => x.id === reasonIdCtrl.value);
        if (elem !== undefined && elem !== null) {
            const reasonValueText = elem.text.toLowerCase();
            if ((reasonValueText === 'other') || (reasonValueText === 'autre')) return true;
        }
        return false;
    }

    public validateReasonFreeText() {
        const reasonTextCtrl = this.$ctrl('grpClosure.closureReasonFreeText');
        const text: string = reasonTextCtrl.value
        if (this.isRequiredReasonFreeText()) {
            if (text === null) return false;
            if (text.trim().length > 0) return true;
        } else {
            if (text === null) return true;
            if (text.trim().length === 0) return true;
        }
        return false;
    }

    public validateReasonFreeTextFr() {
        if (!this.isBilingualRegion) return true;
        const reasonTextCtrlFr = this.$ctrl('grpClosure.closureReasonFreeTextFr');
        const text: string = reasonTextCtrlFr.value
        if (this.isRequiredReasonFreeText()) {
            if (text === null) return false;
            if (text.trim().length > 0) return true;
        } else {
            if (text === null) return true;
            if (text.trim().length === 0) return true;
        }
        return false;
    }

    public validateTaxiway(twy: ITaxiwaySubject): any {
        if (!twy.selected) return null;
        if (twy.full) {
            if (twy.start.length === 0 && twy.startFr.length === 0 && twy.end.length === 0 && twy.endFr.length === 0) return null;
            else return { fullError: { valid: false } };
        } else {
            if (twy.start.length === 0 || twy.end.length === 0) return { partialError: { valid: false } };
            else {
                if (this.isBilingualRegion) {
                    if (twy.startFr.length === 0 || twy.endFr.length === 0) return { partialError: { valid: false } };
                    else return null;
                } else return null;
            }
        }
    }

    public validateAllTaxiways() {
        if (!this.taxiways || !this.taxiwaysSelected) return { selectedTaxiways: { valid: false } }
        var error: any = null;
        if (this.taxiways.length === 0) return { allTaxiways: { valid: false } };
        if (this.taxiwaysSelected.length === 0) return { allTaxiways: { valid: false } };

        for (var i = 0; i < this.taxiwaysSelected.length; i++) {
            error = this.validateTaxiway(this.taxiwaysSelected[i]);
            if (error !== null) {
                i = this.taxiwaysSelected.length + 1;
            }
        }
        return error;
    }

    private validateAndTrigger() {
        let error = this.validateAllTaxiways();
        if (error) {
            this.nsdForm.controls['frmArr'].setErrors(null);
            this.nsdForm.controls['frmArr'].setErrors(error);
            let ctrl = this.$ctrl('countSelectedTwys');
            ctrl.setValue(0);
        } else {
            if (!this.validateReasonFreeText() || !this.validateReasonFreeTextFr()) {
                error = { closureReasonNeeded: { valid: false } };
                this.nsdForm.controls['frmArr'].setErrors(null);
                this.nsdForm.controls['frmArr'].setErrors(error);
                let ctrl = this.$ctrl('countSelectedTwys');
                ctrl.setValue(0);
            }
            else {
                this.nsdForm.controls['frmArr'].setErrors(null);
                let ctrl = this.$ctrl('countSelectedTwys');
                if (ctrl.value === 0)
                    ctrl.setValue(this.countSelectedTwys);

                ctrl = this.$ctrl('allTaxiways');
                if (ctrl.value !== this.taxiwaysSelected)
                    ctrl.setValue(this.taxiwaysSelected);
            }
        }
    }

    // Standard general function

    public get countSelectedTwys(): number {
        return this.taxiwaysSelected.length;
    }

    /*
    private markFrmDirty() {
        this.nsdForm.controls['frmArr'].markAsDirty();
    }
    */

    private updateMap(sdoSubject: ISdoSubject, acceptSubjectLocation: boolean) {
        this.leafletmap.clearFeaturesOnLayers(['DOA']);
        if (sdoSubject.subjectGeoRefPoint) {
            const geojson = JSON.parse(sdoSubject.subjectGeoRefPoint);
            if (geojson.features.length === 1) {
                const subLocation = this.leafletmap.geojsonToTextCordinates(geojson);
                const coordinates = geojson.features[0].geometry.coordinates;
                const markerGeojson = acceptSubjectLocation ? geojson : this.getGeoJsonFromLocation(this.token.subjectLocation);
                if (acceptSubjectLocation) {
                    this.nsdFrmGroup.patchValue({
                        subjectLocation: subLocation,
                        latitude: coordinates[1],
                        longitude: coordinates[0],
                    }, { onlySelf: true, emitEvent: false });
                }
                this.leafletmap.addMarker(geojson, sdoSubject.name, () => {
                    this.leafletmap.addFeature(sdoSubject.subjectGeo, sdoSubject.name, () => {
                        if (markerGeojson) {
                            this.leafletmap.setNewMarkerLocation(markerGeojson);
                        }
                    });
                });
            }
        }

    }

    private setFirstSelectItemInCombo(items: any, itemId: any, insertUndefinedAtZero: boolean = true) {
        if (this.isFormLoadingData) {
            const index = items.findIndex(x => x.id === itemId);
            if (index > 0) { //move selected item to position 0
                items.splice(0, 0, items.splice(index, 1)[0]);
            }
        } else { //insert undefined item at position 0
            if (insertUndefinedAtZero) {
                items.unshift({
                    id: -1,
                    text: '',
                    //textFr: '',
                });
            }
        }
    }

    private setFirstSelectItemInTaxiways(items: any, itemId: any, insertUndefinedAtZero: boolean = true) {
        if (this.isFormLoadingData) {
            const index = items.findIndex(x => x.id === itemId);
            if (index > 0) { //move selected item to position 0
                items.splice(0, 0, items.splice(index, 1)[0]);
            }
        } else { //insert undefined item at position 0
            if (insertUndefinedAtZero) {
                items.unshift({
                    id: "-1",
                    text: '',
                });
            }
        }
    }

    private loadMapLocations(doaId: number, loadingFromProposal: boolean) {
        const self = this;
        this.dataService.getDoaLocation(doaId)
            .subscribe((geoJsonStr: any) => {
                const interval = window.setInterval(function () {
                    if (self.leafletmap && self.leafletmap.isReady()) {
                        window.clearInterval(interval);
                        self.leafletmap.addDoa(geoJsonStr, 'DOA');
                        if (loadingFromProposal) {
                            self.updateMap(self.subjectSelected, false);
                        }
                    };
                }, 100);
            }, (error: any) => {
                this.toastr.error(this.translation.translate('Nsd.DOAArea') + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    private setBilingualRegion(value: boolean) {
        this.token.isBilingual = value;
        this.isBilingualRegion = value;
        if (this.isBilingualRegion) {
            if (!this.isReadOnly) {
                var ctrl = this.$ctrl('startClosureFr');
                ctrl.enable();
                ctrl = this.$ctrl('endClosureFr');
                ctrl.enable();
            }
        } else {
            if (!this.isReadOnly) {
                var ctrl = this.$ctrl('startClosureFr');
                ctrl.disable();
                ctrl = this.$ctrl('endClosureFr');
                ctrl.disable();
            }
        }
    }

    private getGeoJsonFromLocation(value: string) {
        const location = this.locationService.parse(value);
        return location ? this.leafletmap.latitudeLongitudeToGeoJson(location.latitude, location.longitude) : null;
    }
}

function countSelectedTwysValidator(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value === 0) {
        return { 'required': true }
    }
    return null;
}

function refreshIcaoValidator(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value === 0) {
        return { 'required': true }
    }
    return null;
}

function finalClosureValidator(twyClsdReasons: ITwyClsdReasonLanguage[], bilingual: boolean): ValidatorFn {
    return (c: AbstractControl): { [key: string]: any } => {
        let cr = c.get('closureReasonId');
        let crT = c.get('closureReasonFreeText');
        let crTFr = c.get('closureReasonFreeTextFr');

        if (cr.value === 0) return null;
        let elem = twyClsdReasons.find(x => x.id === cr.value);
        if (elem !== undefined && elem !== null) {
            const reasonValueText = elem.text.toLowerCase();
            let text: string = crT.value;
            let textFr: string = crTFr.value;
            if ((reasonValueText === 'other') || (reasonValueText === 'autre')) {
                if (text === null) return { 'missing_reason': true };
                if (text.trim().length === 0) return { 'missing_reason': true };
                if (bilingual) {
                    if (textFr === null) return { 'missing_reason_fr': true };
                    if (textFr.trim().length === 0) return { 'missing_reason_fr': true };
                } 
            } else {
                if (text !== null && text.trim().length > 0) return { 'invalid_reason': true };
                if (textFr !== null && textFr.trim().length > 0) return { 'invalid_reason_fr': true };
            }
        }
        return null;
    }
}


﻿using System.Linq;
using System.Threading.Tasks;

namespace NavCanada.Applications.NotamWiz.Web.Code.IdentityManagers
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin;

    using NavCanada.Core.Data.NotamWiz.EF;
    using NavCanada.Core.Domain.Model.Entitities;

    public class AppRoleManager : RoleManager<ApplicationRole>
    {
        public AppRoleManager(IRoleStore<ApplicationRole, string> roleStore)
            : base(roleStore)
        {

        }

        public static AppRoleManager Create(
            IdentityFactoryOptions<AppRoleManager> options,
            IOwinContext context)
        {
            var manager = new AppRoleManager(
                new RoleStore<ApplicationRole>(
                    context.Get<AppDbContext>()));

            return manager;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using NavCanada.Core.Domain.Model.Entitities;
using System.Threading.Tasks;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Common.Common;
using System.Linq;
using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    public interface INotamRepo : IRepository<Notam>
    {
        Task<Notam> GetByIdAsync(Guid id);
        Task<List<NotamDto>> GetAllAsync(QueryOptions queryOptions);
        Task<List<Notam>> ReportAsync(DateTime start, DateTime end, QueryOptions queryOptions);
        Task<List<Notam>> ReportActiveAsync(DateTime start, DateTime end, QueryOptions queryOptions);
        Task<List<Notam>> ReportCsvAsync(DateTime start, DateTime end);
        Task<List<Notam>> ReportActiveCsvAsync(DateTime start, DateTime end);
        Task<int> ReportCountAsync(DateTime start, DateTime end, QueryOptions queryOptions);
        Task<int> ReportActiveCountAsync(DateTime start, DateTime end, QueryOptions queryOptions);
        Task<List<NotamStatsDto>> StatsAsync(DateTime start, DateTime end, AggregatePeriod aggregatePeriod);
        Task<int> GetAllCountAsync(QueryOptions queryOptions);
        IQueryable<Notam> PrepareQueryFilter(FilterCriteria filterCriteria);
        Task<int> FilterCountAsync(FilterCriteria filterCriteria, IQueryable<Notam> filterQuery);
        Task<int> ReportFilterCountAsync(ReportFilterCriteria filterCriteria, IQueryable<Notam> filterQuery);
        Task<int> ReportActiveFilterCountAsync(ReportFilterCriteria filterCriteria, IQueryable<Notam> filterQuery);
        Task<List<NotamDto>> FilterAsync(FilterCriteria filterCriteria, IQueryable<Notam> filterQuery);
        Task<List<Notam>> ReportFilterAsync(ReportFilterCriteria filterCriteria, IQueryable<Notam> filterQuery);
        Task<List<Notam>> ReportActiveFilterAsync(ReportFilterCriteria filterCriteria, IQueryable<Notam> filterQuery);
        Task<List<Notam>> ReportFilterCsvAsync(DateTime start, DateTime end, IQueryable<Notam> filterQuery);
        Task<List<Notam>> ReportActiveFilterCsvAsync(DateTime start, DateTime end, IQueryable<Notam> filterQuery);
        Task<List<NotamDto>> GetTrackingListByRootIdAsync(Guid rootId);
        Task<List<NotamDto>> GetTrackingListByProposalIdAsync(int proposalId);
        Notam GetReferredNotam(string series, int number, int year);
        Task<int> GetNotamRefCountByNotamIdAsync(string notamId);
        Task<Notam> GetReferredNotamAsync(string series, int number, int year);
        Task<List<Notam>> GetByLocationAsync(string location);
        Task<List<Notam>> GetByLocationAfterDateAsync(string location, DateTime date);
        Task<List<Notam>> GetByLocationBeforeDateAsync(string location, DateTime date);
        Task<List<Notam>> GetByLocationBetweenDatesAsync(string location, DateTime startDate, DateTime endDate);
        IEnumerable<RqnNotamResultDto> GetNotamsByRange(string series, int fromNumber, int toNumber, int year);
        IEnumerable<RqlNotamResultDto> GetActiveNotamNumbersBySeries(string series, bool includeChecklist);
        Notam GetNextBySequenceNumberAndReferenceDate(int lastSequenceNumber, DateTime refDate);
        Notam FindActiveSeriesChecklist(string series);
        Task<bool> FindNotamWithSeriesAsync(string series);
        Task<NotamDto> GetNoltamDtoByIdAsync(Guid notamId);

        Task<string> GetPrevIcaoText(string notamId);
        Task<string> GetCurrentIcaoText(string notamId);
        bool CheckForNewNotamWithProposalId(int proposalId);
    }
}

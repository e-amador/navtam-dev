﻿using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using NAVTAM.NsdEngine.CARSCLSD;
using NAVTAM.ViewModels;
using System.Threading.Tasks;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [RoutePrefix("api/carsclsd")]
    [Authorize]
    public class CarsClsdController : ApiBaseController
    {
        public CarsClsdController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger) : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
        }

        [HttpGet]
        [Route("FilterWithCars")]
        public async Task<IHttpActionResult> FilterWithCars([FromUri] ResourceQuery res)
        {
            return await LogAction(async () =>
            {
                
                if (!ValidUserRequest())
                    return Unauthorized();

                var doaGeoArea = await GeoLocationCache.GetUserDoa(Uow, ApplicationContext.GetUserId());
                var subjects = await Uow.SdoCacheRepo.FilterAerodromesWithCarsAsync(res.Query, doaGeoArea);

                return Ok(subjects);

            }, "api/carsclsd/FilterWithCars");
        }

        [HttpGet]
        [Route("GetCarsClsdStatus/{lang}")]
        public IHttpActionResult GetCarsClsdStatus(string lang)
        {
            return LogAction(() => Ok(CarsClsdStatus.GetCarsStatus(lang)), $"GetCarsClsdStatus/{lang}");
        }
    }
}
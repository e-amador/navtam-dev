﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using Kendo.Mvc.Extensions;
using Microsoft.OData.Client;
using NavCanada.Core.Proxies.AeroRdsProxy.SDOData.Models.SDOEntities;

namespace NavCanada.Applications.NotamWiz.Web.Code.aeroRDS
{
    using System.Configuration;

    using NavCanada.Core.Proxies.AeroRdsProxy;
    using NavCanada.Core.Proxies.AeroRdsProxy.Default;

    public static class AeroRds 
    {

        private static readonly IAeroRdsProxy AeroRdsProxy;

        static AeroRds()
        {
            AeroRdsProxy = new AeroRdsProxy(ConfigurationManager.AppSettings.Get("aeroRDS_Services_URL"));
            
        }

        
        public static Container ODataCtx
        {            
            get
            {
                return AeroRdsProxy.ODataDataContext as Container;
            }
        }

    }

}
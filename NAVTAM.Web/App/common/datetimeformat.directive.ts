﻿import { Directive, HostListener, ElementRef, OnInit } from "@angular/core";
import * as moment from 'moment';

@Directive({
    selector: '[datetimeformat]'
})
export class DateTimeFormatDirective implements OnInit {
    private el: HTMLInputElement;

    constructor(
        private elementRef: ElementRef) {
        this.el = this.elementRef.nativeElement;
    }

    ngOnInit() {
        if (!this.isStringBlank(this.el.value)) {
            if (this.el.value.length < 10) {
                this.el.value = this.el.value;
            }
        } else {
            this.el.value = null;
        }
    }

    @HostListener("blur", ["$event.target.value"])
    onBlur(value) {
        if (!this.isStringBlank(value)) {
            if (value.length !== 10) {
                this.el.value = null;
            } else {
                let momentDate = moment.utc(value, "YYMMDDHHmm");
                if (!momentDate.isValid()) {
                    this.el.value = null;
                }
            }
        }
    }

    @HostListener("keyup", ["$event.target.value"])
    onKeyUp(value) {
        if (value !== null) {
            if (value.length > 10) {
                this.el.value = value.substring(0, 10);
            } else {
                if (this.isANumber(value)) {
                    //switch (value.length) {
                    //    case 1://first year digit
                    //        if (+value[0] < 1 || +value[0] > 2) {
                    //            this.el.value = null;
                    //        }
                    //        break;
                    //    case 2: //second year digit
                    //        if (+value[0] === 1) {
                    //            if (+value[1] < 7) this.el.value = value.slice(0, 1);
                    //        } 
                    //        break; 
                    //    case 3: //first month digit
                    //        if (+value[2] > 1) this.el.value = value.slice(0, 2); break;
                    //    case 4: break; //second month digit;
                    //    case 5: //first day digit
                    //        if (+value[4] > 3) this.el.value = value.slice(0, 4); break;
                    //    case 6: //second day digit
                    //        if (+value[4] === 0) {
                    //            if( +value[5] < 1) this.el.value = value.slice(0, 5);
                    //        } else if (+value[4] === 3){
                    //            if (+value[5] > 1) this.el.value = value.slice(0, 5);
                    //        }
                    //        break;
                    //    case 7: //first hour digit
                    //        if (+value[6] > 2) this.el.value = value.slice(0, 6); break;
                    //    case 8: break; //second hour digit
                    //    case 9: //first minute digit
                    //        if (+value[8] > 5) this.el.value = value.slice(0, 7); break;
                    //    case 10://second minute digit
                    //        let momentDate = moment.utc(value, "YYMMDDHHmm");
                    //        if (!momentDate.isValid()) {
                    //            //this.el.value = null;
                    //        }
                    //        break;
                    //}
                } else {
                    let strDigits = "";
                    for (let i = 0; i < value.length; i++) {
                        if (this.isANumber(value[i])) {
                            strDigits += value[i];
                        }
                    }
                    this.el.value = strDigits;
                }
            }
        }
    }

    private isANumber(str : string) : boolean {
        return !/\D/.test(str);
    }

    private isStringBlank(str) {
        return (!str || /^\s*$/.test(str));
    }
}
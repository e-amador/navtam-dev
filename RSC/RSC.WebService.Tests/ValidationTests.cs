﻿using System;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RSC.WebService.Helpers;
using System.Text.RegularExpressions;
using RSC.Business;
using RSC.WebService.Schemas;

namespace RSC.WebService.Tests
{
    [TestClass]
    public class ValidationTests
    {
        [TestMethod]
        //[DeploymentItem("Data\\cancel-proposal.xml")]
        [DeploymentItem("Data\\many-runways.xml")]
        public void Validate_cancel_proposal()
        {
                                   //var xml = System.IO.File.ReadAllText(@"cancel-proposal.xml");
            var xml = System.IO.File.ReadAllText(@"many-runways.xml");

            var message = XMLValidations.ValidateProposal(xml);

            Assert.IsTrue(string.IsNullOrEmpty(message), message);
        }

        [TestMethod]
        //[DeploymentItem("Data\\cancel-proposal.xml")]
        [DeploymentItem("Data\\RscRequestSample.xml")]
        public void Validate_proposal()
        {
            var xml = System.IO.File.ReadAllText(@"RscRequestSample.xml");

            xml = PrepareXml(xml);
            var message = XMLValidations.ValidateProposal(xml);

            RSCRequestType request = XMLSerializer.Deserialize<RSCRequestType>(xml);

            var proposal = request.RSCProposal.ToModel();
            

            Assert.IsTrue(true);
        }
        
        private static string PrepareXml(string xml)
        {
            var day = DateTime.UtcNow.Day.ToString();
            var dayPattern = @"(?:<UTC_Day>\d{2}<\/UTC_Day>)";
            var dayReplace = $"<UTC_Day>{day}</UTC_Day>";

            var month = DateTime.UtcNow.Month.ToString();
            var monthPattern = @"(?:<UTC_Month>\d{2}<\/UTC_Month>)";
            var monthReplace = $"<UTC_Month>{month}</UTC_Month>";

            var year = DateTime.UtcNow.Year.ToString();
            var yearPattern = @"(?:<UTC_Year>\d{4}<\/UTC_Year>)";
            var yearReplace = $"<UTC_Year>{year}</UTC_Year>";

            var utc_time = DateTime.UtcNow.TimeOfDay.ToString();
            utc_time = utc_time.Substring(0, 2) + utc_time.Substring(3, 2);
            var utc_timePattern = @"(?:<UTC_Time>\d{4}<\/UTC_Time>)";
            var utcReplace = $"<UTC_Time>{utc_time}</UTC_Time>";

            var nextPlannedObsPattern = @"(?:<NextPlannedObs><UTC_Day>\d{1,2}<\/UTC_Day><UTC_Month>\d{1,2}<\/UTC_Month><UTC_Year>\d{4}<\/UTC_Year><UTC_Time>\d{4}<\/UTC_Time><\/NextPlannedObs>)";
            var nextPlannedObsTime = DateTime.UtcNow.AddMinutes(20).ToString();
            nextPlannedObsTime = nextPlannedObsTime.Substring(0, 2) + nextPlannedObsTime.Substring(3, 2);
            var nextPlannedObsReplace = $"(?:<NextPlannedObs><UTC_Day>{day}</UTC_Day><UTC_Month>{month}</UTC_Month><UTC_Year>{year}</UTC_Year><UTC_Time>{nextPlannedObsTime}</UTC_Time></NextPlannedObs>)";


            Regex.Replace(xml, dayPattern, dayReplace);
            Regex.Replace(xml, monthPattern, monthReplace);
            Regex.Replace(xml, yearPattern, yearReplace);
            Regex.Replace(xml, utc_timePattern, utcReplace);
            Regex.Replace(xml, nextPlannedObsPattern, nextPlannedObsReplace);

            return xml;
        }
    }
}

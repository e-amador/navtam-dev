"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProposalFilterComponent = void 0;
var core_1 = require("@angular/core");
var angular_l10n_1 = require("angular-l10n");
var moment = require("moment");
var data_model_1 = require("../shared/data.model");
var data_service_1 = require("../shared/data.service");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var ProposalFilterComponent = /** @class */ (function () {
    function ProposalFilterComponent(dataService, memStorageService, translation, changeDetectionRef) {
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.translation = translation;
        this.changeDetectionRef = changeDetectionRef;
        this.onFilterChanged = new core_1.EventEmitter();
        this.filterColumnStartValue = [];
        this.filterSlotsActivated = [];
        this.loadingFilter = true;
        this.parent = null;
        this.filters = [];
        this.slots = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
        this.filterErrorMessage = [];
        var emptyFilter = {
            field: "",
            operator: data_model_1.FilterOperator.EQ,
            value: "",
            type: "text"
        };
        this.filterQueryOptions = {
            page: 1,
            pageSize: 10,
            sort: "Received" //Default Received Descending
        };
        this.filters.push(emptyFilter);
        this.filterColumnsData = [
            { id: 'EMPTY', text: this.translation.translate('Filter.SelectFilterCriteria'), type: "text", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'NotamId', text: this.translation.translate('Filter.NotamId'), type: "text", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'ItemA', text: this.translation.translate('Filter.ItemA'), type: "text", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'ItemE', text: this.translation.translate('Filter.ItemE'), type: "text", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'ProposalType', text: this.translation.translate('Filter.ProposalType'), type: "combobox", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Series', text: this.translation.translate('Filter.Series'), type: "combobox", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Scope', text: this.translation.translate('Filter.Scope'), type: "combobox", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Code23', text: this.translation.translate('Filter.Code23'), type: "text", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Code45', text: this.translation.translate('Filter.Code45'), type: "text", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Traffic', text: this.translation.translate('Filter.Traffic'), type: "combobox", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Purpose', text: this.translation.translate('Filter.Purpose'), type: "combobox", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Radius', text: this.translation.translate('Filter.Radius'), type: "number", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'LowerLimit', text: this.translation.translate('Filter.LowerLimit'), type: "number", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'UpperLimit', text: this.translation.translate('Filter.UpperLimit'), type: "number", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Received', text: this.translation.translate('Filter.Received'), type: "datetime", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'StartActivity', text: this.translation.translate('Filter.StartActivity'), type: "datetime", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'EndValidity', text: this.translation.translate('Filter.EndValidity'), type: "datetime", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Originator', text: this.translation.translate('Filter.Originator'), type: "text", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'ModifiedByUsr', text: this.translation.translate('Filter.ModifiedByUsr'), type: "text", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'NoteToNof', text: this.translation.translate('Filter.NotesToNof'), type: "text", operators: [], currentOper: data_model_1.FilterOperator.EQ, values: [], currentValue: "" },
        ];
        this.filterColumnStartValue.push(this.filterColumnsData[0].id);
        this.filterOperatorsData = [
            { id: data_model_1.FilterOperator.EQ, text: this.translation.translate('Filter.Equal') },
            { id: data_model_1.FilterOperator.NEQ, text: this.translation.translate('Filter.NotEqual') },
            { id: data_model_1.FilterOperator.LT, text: this.translation.translate('Filter.LessThan') },
            { id: data_model_1.FilterOperator.GT, text: this.translation.translate('Filter.GreaterThan') },
            { id: data_model_1.FilterOperator.LTE, text: this.translation.translate('Filter.LessThanOrEqual') },
            { id: data_model_1.FilterOperator.GTE, text: this.translation.translate('Filter.GreaterThanOrEqual') },
            { id: data_model_1.FilterOperator.SW, text: this.translation.translate('Filter.StartWith') },
            { id: data_model_1.FilterOperator.EW, text: this.translation.translate('Filter.EndWith') },
            { id: data_model_1.FilterOperator.CT, text: this.translation.translate('Filter.Contains') },
        ];
        for (var index = 0; index < this.filterColumnsData.length; index++) {
            this.filterColumnsData[index].operators = this.getAllowFilterOperator(index);
            this.filterErrorMessage.push("");
        }
    }
    ProposalFilterComponent.prototype.ngOnChanges = function () {
    };
    ProposalFilterComponent.prototype.ngOnInit = function () {
        this.getActiveUserQueryFilters();
    };
    ProposalFilterComponent.prototype.changeFilterSort = function (sortField) {
        this.filterQueryOptions.sort = sortField;
    };
    ProposalFilterComponent.prototype.onFilterColumnChanged = function (e, index) {
        this.setFilterStatus(e.value, index, "");
        this.filters[index].field = e.value;
        var operIndex = this.filterColumnsData.findIndex(function (x) { return x.id == e.value; });
        this.filterColumnsData[index].operators = this.getAllowFilterOperator(operIndex);
        this.filterColumnStartValue[index] = e.value;
    };
    ProposalFilterComponent.prototype.getFilterOperators = function (fieldName) {
        var pos = this.filterColumnsData.findIndex(function (x) { return x.id === fieldName; });
        return this.getAllowFilterOperator(pos < 0 ? 0 : pos);
    };
    ProposalFilterComponent.prototype.onFilterValueChanged = function (e, index) {
        this.filters[index].value = e.value;
    };
    ProposalFilterComponent.prototype.onFilterOperatorChanged = function (e, index) {
        this.filters[index].operator = e.value;
        this.filterColumnsData[index].currentOper = e.value;
    };
    ProposalFilterComponent.prototype.isFilterSlotActivated = function (slotNumber) {
        return this.filterSlotsActivated.findIndex(function (x) { return x == slotNumber; }) >= 0;
    };
    ProposalFilterComponent.prototype.isFilterAddButtonEnabled = function (index) {
        if (index < this.filters.length - 1) {
            return false;
        }
        var f = this.filters[index];
        var columnData = this.filterColumnsData.find(function (x) { return x.id == f.field; });
        if (columnData === undefined) {
            this.updateFilterError(index, null); //ignore if not field selected.
            return false;
        }
        if (f.value === "") {
            this.updateFilterError(index, this.translation.translate('Filter.MatchValueRequired'));
            return false;
        }
        switch (columnData.type) {
            case "datetime":
                if (f.field === "StartActivity" && f.value === "IMMEDIATE") {
                    this.updateFilterError(index, null);
                }
                else if (f.field === "EndValidity" && f.value === "EndValidity") {
                    this.updateFilterError(index, null);
                }
                else {
                    this.filterErrorMessage[index] = this.translation.translate('Filter.InvalidDateFormat');
                    if ($.isNumeric(f.value) && f.value.length === 10) {
                        var momentDate = moment(f.value, "YYMMDDHHmm");
                        if (momentDate.isValid()) {
                            this.updateFilterError(index, null);
                        }
                    }
                }
                break;
            case "combobox":
                this.updateFilterError(index, null);
                break;
            case "number":
                if ($.isNumeric(f.value)) {
                    if (f.field !== "EMPTY") {
                        this.updateFilterError(index, null);
                    }
                }
                else {
                    this.updateFilterError(index, this.translation.translate('Filter.NumericValueRequired'));
                }
                break;
            default: //text
                if (f.value !== "" && f.field !== "EMPTY") {
                    this.updateFilterError(index, null);
                }
                break;
        }
        return this.filterErrorMessage[index] === null;
    };
    ProposalFilterComponent.prototype.isApplyFilterButtonDisabled = function () {
        if (this.parent && !this.isFilterAddButtonEnabled(this.filters.length - 1)) {
            return true;
        }
        return this.loadingFilter;
    };
    ProposalFilterComponent.prototype.addFilterCondition = function () {
        this.filters.push({
            field: "",
            operator: data_model_1.FilterOperator.EQ,
            value: "",
            type: "text"
        });
        var index = this.filters.length - 1;
        this.filterColumnStartValue.push(this.filterColumnsData[0].id);
        this.filterColumnsData[index].currentOper = data_model_1.FilterOperator.EQ;
    };
    ProposalFilterComponent.prototype.removeFilterCondition = function (index) {
        this.filters.splice(index, 1);
        this.filterColumnStartValue.splice(index, 1);
        //let pos = this.filterColumnStartValue.length >= index ? this.filterColumnsData.findIndex(x => x.id === this.filterColumnStartValue[index]) : 0;
        this.filterColumnsData[index].currentOper = data_model_1.FilterOperator.EQ;
    };
    ProposalFilterComponent.prototype.activateFilter = function (page) {
        page = page || 1;
        this.memStorageService.save(this.memStorageService.FILTER_APPLIED_KEY, true);
        this.memStorageService.save(this.memStorageService.PROPOSAL_FILTER_KEY, this.filters);
        this.runFilter(true, page, this.pageSize);
    };
    ProposalFilterComponent.prototype.loadFilterState = function () {
        this.filterQueryOptions = this.memStorageService.get(this.memStorageService.FILTER_QUERY_OPTIONS_KEY);
        this.updateFilters(this.memStorageService.get(this.memStorageService.PROPOSAL_FILTER_KEY));
    };
    ProposalFilterComponent.prototype.runFilter = function (togglePannel, page, pageSize) {
        var _this = this;
        this.filterQueryOptions.pageSize = pageSize || this.filterQueryOptions.pageSize;
        this.filterQueryOptions.page = page || this.filterQueryOptions.page;
        this.memStorageService.save(this.memStorageService.FILTER_QUERY_OPTIONS_KEY, this.filterQueryOptions);
        this.showProgressBar();
        this.dataService.filterProposals(this.filters, this.filterQueryOptions)
            .subscribe(function (result) {
            if (togglePannel) {
                _this.toogleFilterPanel();
            }
            _this.hideProgressBar();
            _this.onFilterChanged.emit({
                isFilterApplied: true,
                data: result
            });
        }, function (error) {
            _this.hideProgressBar();
            _this.onFilterChanged.emit({
                error: error
            });
        });
    };
    ProposalFilterComponent.prototype.clearFilter = function (event, openPannel) {
        if (event) {
            event.preventDefault();
        }
        this.filters = [];
        this.addFilterCondition();
        //if (openPannel !== false) {
        //     this.toogleFilterPanel();
        //}
        this.memStorageService.remove(this.memStorageService.FILTER_APPLIED_KEY);
        this.memStorageService.remove(this.memStorageService.PROPOSAL_FILTER_KEY);
        for (var i = 0; i < this.filterColumnsData.length; i++) {
            this.filterColumnsData[i].operators = this.getAllowFilterOperator(i);
            this.filterColumnsData[i].currentOper = data_model_1.FilterOperator.EQ;
        }
        this.filterColumnStartValue = [];
        this.filterColumnStartValue.push(this.filterColumnsData[0].id);
        this.onFilterChanged.emit({ isFilterApplied: false });
        this.filterSlotsActive = -1;
    };
    ProposalFilterComponent.prototype.getUserQueryFilter = function (slotNumber) {
        var _this = this;
        this.dataService.getQueryFilter(slotNumber, this.filterType)
            .subscribe(function (result) {
            _this.updateFilters(result);
            _this.filterSlotsActive = slotNumber;
        }, function (error) {
            //TODO: ERROR
        });
    };
    ProposalFilterComponent.prototype.getActiveUserQueryFilters = function () {
        var _this = this;
        this.dataService.getActiveQueryFilters(this.filterType)
            .subscribe(function (result) {
            _this.filterSlotsActivated = result;
            _this.loadingFilter = false;
        }, function (error) {
            //TODO: ERROR
        });
    };
    ProposalFilterComponent.prototype.saveUserQueryFilter = function (event, slotNumber) {
        event.preventDefault();
        var self = this;
        this.dataService.saveQueryFilter(slotNumber, this.filters, this.filterType)
            .subscribe(function (result) {
            self.filterSlotsActive = slotNumber;
            if (self.filterSlotsActivated.findIndex(function (x) { return x == slotNumber; }) < 0) {
                self.filterSlotsActivated.push(slotNumber);
            }
        }, function (error) {
            //TODO: ERROR
        });
    };
    ProposalFilterComponent.prototype.deleteUserQueryFilter = function (event, slotNumber) {
        event.preventDefault();
        var self = this;
        this.dataService.deleteQueryFilter(slotNumber, this.filterType)
            .subscribe(function (result) {
            self.filterSlotsActive = -1;
            var index = self.filterSlotsActivated.findIndex(function (x) { return x == slotNumber; });
            if (index >= 0) {
                self.filterSlotsActivated.splice(index, 1);
            }
        }, function (error) {
            //TODO: ERROR
        });
    };
    ProposalFilterComponent.prototype.updateFilterError = function (index, errorMessage) {
        if (this.filterErrorMessage[index] !== errorMessage) {
            this.filterErrorMessage[index] = errorMessage;
        }
    };
    ProposalFilterComponent.prototype.updateFilters = function (filters) {
        this.filterColumnStartValue = [];
        this.filters = filters;
        for (var i = 0; i < this.filters.length; i++) {
            this.setFilterStatus(filters[i].field, i, filters[i].value);
            this.filterColumnStartValue.push(filters[i].field);
            this.filterColumnsData[i].currentOper = this.filters[i].operator;
        }
    };
    ProposalFilterComponent.prototype.setFilterStatus = function (field, index, currentValue) {
        switch (field) {
            case "EMPTY":
            case "NotamId":
            case "ItemA":
            case "ItemE":
            case "Originator":
            case "Operator":
            case "ModifiedByUsr":
            case "Code23":
            case "Code45":
            case "NoteToNof":
                this.setNonComboboxFilterData(currentValue, index, "text");
                break;
            case "Radius":
            case "LowerLimit":
            case "UpperLimit":
                this.setNonComboboxFilterData(currentValue, index, "number");
                break;
            case "Received":
            case "StartActivity":
            case "EndValidity":
                this.setNonComboboxFilterData(currentValue, index, "datetime");
                break;
            case "ProposalType":
                this.setComboboxFilterData(["N", "C", "R"], index, currentValue);
                break;
            case "Traffic":
                this.setComboboxFilterData(["I", "V", "IV"], index, currentValue);
                break;
            case "Scope":
                this.setComboboxFilterData(["A", "AE", "E", "K", "W"], index, currentValue);
                break;
            case "Purpose":
                this.setComboboxFilterData(["B", "BO", "K", "M", "NBO"], index, currentValue);
                break;
            case "Series":
                this.setComboboxFilterData(["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"], index, currentValue);
                break;
        }
    };
    ProposalFilterComponent.prototype.setComboboxFilterData = function (data, index, currentValue) {
        this.filterColumnsData[index].type = "combobox";
        this.filterColumnsData[index].operators = this.getAllowFilterOperator(index);
        this.filterColumnsData[index].values = data;
        currentValue = currentValue !== "" ? currentValue : this.filterColumnsData[index].values[0];
        this.filterColumnsData[index].currentValue = currentValue;
        this.filters[index].type = "combobox";
        this.filters[index].valueList = data;
        this.filters[index].value = currentValue;
    };
    ProposalFilterComponent.prototype.setNonComboboxFilterData = function (currentValue, index, type) {
        this.filterColumnsData[index].type = type;
        this.filterColumnsData[index].operators = this.getAllowFilterOperator(index);
        this.filterColumnsData[index].currentValue = currentValue;
        this.filters[index].type = type;
        //this.filters[index].valueList = data;
        this.filters[index].value = currentValue;
    };
    ProposalFilterComponent.prototype.getAllowFilterOperator = function (index) {
        var filterColumnsData = [];
        var filterName = this.filterColumnsData[index].id;
        switch (filterName) {
            case "NotamId":
            case "ItemA":
            case "ItemE":
            case "Code23":
            case "Code45":
            case "Originator":
            case "Operator":
            case "ModifiedByUsr":
            case "NoteToNof":
                filterColumnsData = this.addTextFieldFilters(filterColumnsData);
                break;
            case "StartActivity":
            case "EndValidity":
            case "Received":
                filterColumnsData = this.addDateFieldFilters(filterColumnsData);
                break;
            case "Series":
            case "ProposalType":
                filterColumnsData = this.addBooleanFieldFilters(filterColumnsData);
                break;
            case "Traffic":
            case "Purpose":
            case "Scope":
                filterColumnsData = this.addBooleanExFieldFilters(filterColumnsData);
                break;
            case "Radius":
            case "LowerLimit":
            case "UpperLimit":
                filterColumnsData = this.addNumberFieldFilters(filterColumnsData);
                break;
        }
        return filterColumnsData;
    };
    ProposalFilterComponent.prototype.addAllFilters = function (filterColumnsData) {
        for (var i = 0; i < this.filterOperatorsData.length; i++) {
            filterColumnsData.push(this.filterOperatorsData[i]);
        }
        return filterColumnsData;
    };
    ProposalFilterComponent.prototype.addTextFieldFilters = function (filterColumnsData) {
        filterColumnsData.push(this.filterOperatorsData[0]);
        filterColumnsData.push(this.filterOperatorsData[1]);
        filterColumnsData.push(this.filterOperatorsData[6]);
        filterColumnsData.push(this.filterOperatorsData[7]);
        filterColumnsData.push(this.filterOperatorsData[8]);
        return filterColumnsData;
    };
    ProposalFilterComponent.prototype.addBooleanFieldFilters = function (filterColumnsData) {
        filterColumnsData.push(this.filterOperatorsData[0]);
        filterColumnsData.push(this.filterOperatorsData[1]);
        return filterColumnsData;
    };
    ProposalFilterComponent.prototype.addBooleanExFieldFilters = function (filterColumnsData) {
        filterColumnsData.push(this.filterOperatorsData[0]);
        filterColumnsData.push(this.filterOperatorsData[1]);
        filterColumnsData.push(this.filterOperatorsData[8]);
        return filterColumnsData;
    };
    ProposalFilterComponent.prototype.addDateFieldFilters = function (filterColumnsData) {
        filterColumnsData.push(this.filterOperatorsData[0]);
        filterColumnsData.push(this.filterOperatorsData[1]);
        filterColumnsData.push(this.filterOperatorsData[2]);
        filterColumnsData.push(this.filterOperatorsData[3]);
        filterColumnsData.push(this.filterOperatorsData[4]);
        filterColumnsData.push(this.filterOperatorsData[5]);
        return filterColumnsData;
    };
    ProposalFilterComponent.prototype.addNumberFieldFilters = function (filterColumnsData) {
        filterColumnsData.push(this.filterOperatorsData[0]);
        filterColumnsData.push(this.filterOperatorsData[1]);
        filterColumnsData.push(this.filterOperatorsData[2]);
        filterColumnsData.push(this.filterOperatorsData[3]);
        filterColumnsData.push(this.filterOperatorsData[4]);
        filterColumnsData.push(this.filterOperatorsData[5]);
        return filterColumnsData;
    };
    ProposalFilterComponent.prototype.showProgressBar = function () {
        if (this.parent) {
            this.parent.loadingData = true;
        }
    };
    ProposalFilterComponent.prototype.hideProgressBar = function () {
        if (this.parent) {
            this.parent.loadingData = false;
        }
    };
    ProposalFilterComponent.prototype.toogleFilterPanel = function () {
        $(".panel-toggle").toggleClass("closed").parents(".panel:first").find(".panel-content").slideToggle();
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], ProposalFilterComponent.prototype, "pageSize", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], ProposalFilterComponent.prototype, "filterType", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], ProposalFilterComponent.prototype, "onFilterChanged", void 0);
    ProposalFilterComponent = __decorate([
        core_1.Component({
            selector: 'proposal-filter',
            templateUrl: '/app/dashboard/user/proposal-filter.component.html'
        }),
        __metadata("design:paramtypes", [data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            angular_l10n_1.TranslationService,
            core_1.ChangeDetectorRef])
    ], ProposalFilterComponent);
    return ProposalFilterComponent;
}());
exports.ProposalFilterComponent = ProposalFilterComponent;
//# sourceMappingURL=proposal-filter.component.js.map
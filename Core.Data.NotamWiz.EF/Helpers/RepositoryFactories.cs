﻿namespace NavCanada.Core.Data.NotamWiz.EF.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;

    using Contracts;
    using Contracts.Repositories;
    using Repositories;
    using Data.EF.Repositories;
    using Data.Contracts.Repositories;

    public class RepositoryFactories
    {
        /// <summary>
        /// Get the dictionary of repository factory functions.
        /// </summary>
        /// <remarks>
        /// A dictionary key is a System.Type, typically a repository type.
        /// A value is a repository factory function
        /// that takes a <see cref="DbContext"/> argument and returns
        /// a repository object. Caller must know how to cast it.
        /// </remarks>
        private readonly IDictionary<Type, Func<DbContext, object>> _repositoryFactories;

        /// <summary>
        /// Constructor that initializes with runtime repository factories
        /// </summary>
        public RepositoryFactories()
        {
            this._repositoryFactories = GetFactories();
        }

        /// <summary>
        /// Constructor that initializes with an arbitrary collection of factories
        /// </summary>
        /// <param name="factories">
        /// The repository factory functions for this instance. 
        /// </param>
        /// <remarks>
        /// This ctor is primarily useful for testing this class
        /// </remarks>
        public RepositoryFactories(IDictionary<Type, Func<DbContext, object>> factories)
        {
            this._repositoryFactories = factories;
        }

        /// <summary>
        /// Return the runtime X repository factory functions,
        /// each one is a factory for a repository of a particular type.
        /// </summary>
        /// <remarks>
        /// MODIFY THIS METHOD TO ADD CUSTOM FACTORY FUNCTIONS
        /// </remarks>
        private IDictionary<Type, Func<DbContext, object>> GetFactories()
        {
            return new Dictionary<Type, Func<DbContext, object>>
            {
                { typeof(IAerodromeDisseminationCategoryRepo), dbContext => new AerodromeDisseminationCategoryRepo(dbContext) },
                { typeof(IConfigurationValueRepo), dbContext => new ConfigurationValueRepo(dbContext) },
                { typeof(IDigitalSlaRepo), dbContext => new DigitalSlaRepo(dbContext) },
                { typeof(IDoaRepo), dbContext => new DoaRepo(dbContext) },
                { typeof(IUserDoaRepo), dbContext => new UserDoaRepo(dbContext) },
                { typeof(IOrganizationDoaRepo), dbContext => new OrganizationDoaRepo(dbContext) },
                { typeof(IFeedbackRepo), dbContext => new FeedbackRepo(dbContext) },
                { typeof(IGeoRegionRepo), dbContext => new GeoRegionRepo(dbContext) },
                { typeof(ISeriesAllocationRepo), dbContext => new SeriesAllocationRepo(dbContext) },
                { typeof(IGroupRepo), dbContext => new GroupRepo(dbContext) },
                { typeof(IIcaoSubjectRepo), dbContext => new IcaoSubjectRepo(dbContext) },
                { typeof(IIcaoSubjectConditionRepo), dbContext => new IcaoSubjectConditionRepo(dbContext) },
                { typeof(ISeriesNumberRepo), dbContext => new SeriesNumberRepo(dbContext) },
                { typeof(INotificationTemplateRepo), dbContext => new NotificationTemplateRepo(dbContext) },
                { typeof(INotificationTrackingRepo), dbContext => new NotificationTrackingRepo(dbContext) },
                { typeof(IOrganizationRepo), dbContext => new OrganizationRepo(dbContext) },
                { typeof(IOrganizationNsdRepo), dbContext => new OrganizationNsdRepo(dbContext) },
                { typeof(IPermissionRepo), dbContext => new PermissionRepo(dbContext) },
                { typeof(IRegistrationRepo), dbContext => new RegistrationRepo(dbContext) },
                { typeof(IRolePermissionRepo), dbContext => new RolePermissionRepo(dbContext) },
                { typeof(ISdoCacheRepo), dbContext => new SdoCacheRepo(dbContext) },
                { typeof(IUserPreferenceRepo), dbContext => new UserPreferenceRepo(dbContext) },
                { typeof(IUserProfileRepo), dbContext => new UserProfileRepo(dbContext) },
                { typeof(IApplicationRoleRepo), dbContext => new ApplicationRoleRepo(dbContext) },
                { typeof(IPoisonMtoRepo), dbContext => new PoisonMtoRepo(dbContext) },
                { typeof(IPoisonMtoRecordRepo), dbContext => new PoisonMtoRecordRepo(dbContext) },
                { typeof(IScheduleTaskRepo), dbContext => new ScheduleTaskRepo(dbContext) },

                { typeof(INsdCategoryRepo), dbContext => new NsdCategoryRepo(dbContext) },
                { typeof(IProposalRepo), dbContext => new ProposalRepo(dbContext) },
                { typeof(IProposalHistoryRepo), dbContext => new ProposalHistoryRepo(dbContext) },
                { typeof(IProposalAttachmentRepo), dbContext => new ProposalAttachmentRepo(dbContext) },
                { typeof(IScheduleJobRepo), dbContext => new ScheduleJobRepo(dbContext) },
                { typeof(INotamRepo), dbContext => new NotamRepo(dbContext) },
                { typeof(INotamTopicRepo), dbContext => new NotamTopicRepo(dbContext) },
                { typeof(IUserQueryFilterRepo), dbContext => new UserQueryFilterRepo(dbContext) },
                { typeof(INdsClientItemARepo), dbContext => new NdsClientItemARepo(dbContext) },
                { typeof(INdsClientSeriesRepo), dbContext => new NdsClientSeriesRepo(dbContext) },
                { typeof(INdsClientRepo), dbContext => new NdsClientRepo(dbContext) },
                { typeof(INdsNotamMessageRepo), dbContext => new NdsNotamMessageRepo(dbContext) },
                { typeof(INdsOutgoingMessageRepo), dbContext => new NdsOutgoingMessageRepo(dbContext) },
                { typeof(INdsIncomingRequestRepo), dbContext => new NdsIncomingRequestRepo(dbContext) },
                { typeof(IMessageExchangeQueueRepo), dbContext => new MessageExchangeQueueRepo(dbContext) },
                { typeof(INofTemplateMessageRecordRepo), dbContext => new NofTemplateMessageRecordRepo(dbContext) },
                { typeof(IMessageExchangeTemplateRepo), dbContext => new MessageExchangeTemplateRepo(dbContext) },
                { typeof(IItemDRepo), dbContext => new ItemDRepo(dbContext) },
                { typeof(IDisseminationJobRepo), dbContext => new DisseminationJobRepo(dbContext) },
                { typeof(ISeriesChecklistRepo), dbContext => new SeriesChecklistRepo(dbContext) },
                { typeof(IAeroRDSCacheRepo), dbContext => new AeroRDSCacheRepo(dbContext) },
                { typeof(IAeroRDSCacheStatusRepo), dbContext => new AeroRDSCacheStatusRepo(dbContext) },
                { typeof(IUserRegistrationRepo), dbContext => new UserRegistrationRepo(dbContext) },
                { typeof(INotifySeriesRepo), dbContext => new NotifySeriesRepo(dbContext) },
                { typeof(IOriginatorInfoRepo), dbContext => new OriginatorInfoRepo(dbContext) },
                { typeof(ISystemStatusRepo), dbContext => new SystemStatusRepo(dbContext) },
                { typeof(ILogRepo), dbContext => new LogRepo(dbContext) },
                { typeof(IAerodromeRSCRepo), dbContext => new AerodromeRSCRepo(dbContext) },
            };
        }

        /// <summary>
        /// Get the repository factory function for the type.
        /// </summary>
        /// <typeparam name="T">Type serving as the repository factory lookup key.</typeparam>
        /// <returns>The repository function if found, else null.</returns>
        /// <remarks>
        /// The type parameter, T, is typically the repository type 
        /// but could be any type (e.g., an entity type)
        /// </remarks>
        public Func<DbContext, object> GetRepositoryFactory<T>()
        {
            Func<DbContext, object> factory;
            this._repositoryFactories.TryGetValue(typeof(T), out factory);
            return factory;
        }

        /// <summary>
        /// Get the factory for <see cref="IRepository{T}"/> where T is an entity type.
        /// </summary>
        /// <typeparam name="T">The root type of the repository, typically an entity type.</typeparam>
        /// <returns>
        /// A factory that creates the <see cref="IRepository{T}"/>, given an EF <see cref="DbContext"/>.
        /// </returns>
        /// <remarks>
        /// Looks first for a custom factory in <see cref="_repositoryFactories"/>.
        /// If not, falls back to the <see cref="DefaultEntityRepositoryFactory{T}"/>.
        /// You can substitute an alternative factory for the default one by adding
        /// a repository factory for type "T" to <see cref="_repositoryFactories"/>.
        /// </remarks>
        public Func<DbContext, object> GetRepositoryFactoryForEntityType<T>() where T : class
        {
            return GetRepositoryFactory<T>() ?? DefaultEntityRepositoryFactory<T>();
        }

        /// <summary>
        /// Default factory for a <see cref="IRepository{T}"/> where T is an entity.
        /// </summary>
        /// <typeparam name="T">Type of the repository's root entity</typeparam>
        protected virtual Func<DbContext, object> DefaultEntityRepositoryFactory<T>() where T : class
        {
            return dbContext => new Repository<T>(dbContext);
        }
    }
}
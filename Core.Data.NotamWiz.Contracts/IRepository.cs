﻿namespace NavCanada.Core.Data.NotamWiz.Contracts
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetAll();
        
        Task<List<T>> GetAllAsync();

        T GetById(object id);

        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Delete(int id);
        void Detach(T entity);

        //To create dynamic where expressions
        Expression<Func<T, bool>> BuildWhereExpression(string property, object value, string oper);
    }
}

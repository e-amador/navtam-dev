namespace NavCanada.Core.Data.NotamWiz.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProposalPendingReviewAndOriginatorSize : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Proposal", "PendingReview", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Proposal", "Originator", c => c.String(maxLength: 250));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Proposal", "Originator", c => c.String(maxLength: 50));
            DropColumn("dbo.Proposal", "PendingReview");
        }
    }
}

﻿import { Component, OnInit } from '@angular/core';
import { Localization, LocaleService, TranslationService } from 'angular-l10n';

import { WindowRef } from '../common/windowRef.service';

declare var $: any;

@Component({
    selector: 'sidebar',
    templateUrl: '/app/admin/sidebar.component.html'
})
export class SidebarComponent implements OnInit {

    private txtNavigationHeader: string = "Navigation";
    private txtUserManagment: string = "User Managment";
    private txtOrgManagment: string = "Organizations";
    private txtIcaoSubCond: string = "ICAO Subject & Conditions";
    private txtSeriesManagement: string = "Serie Allocation Management";
    private txtADCManagement: string = "Dissemination Category";
    private txtDoasManagement: string = "Domain of Authority";
    private txtNsdsManagement: string = "Notam Scenario Definitions";
    private txtBilingualManagement: string = "Bilingual Region";
    private txtNorthernManagement: string = "Northern Region";
    private txtGeoRegionManagement: string = "Geographic Region";
    private txtConfigManagement: string = "Configuration";
    private txtCheckList: string = "Checklist";
    private txtAeroRDSSync: string = "AeroRDS Sync";
    private txtSubscriptionManagement: string = "Client Subscriptions";

    constructor(private locale: LocaleService, private translation: TranslationService, private winRef: WindowRef) {}

    ngOnInit() {
        let self = this;
        window.setTimeout(function () {
            self.setLabels();
        }, 1000); 
    }

    isSysAdmin() {
        const appConfig = this.winRef.appConfig;
        return appConfig && appConfig.sysAdmin;
    }

    isCCS() {
        const appConfig = this.winRef.appConfig;
        return appConfig && appConfig.ccs;
    }

    isNofAdmin() {
        const appConfig = this.winRef.appConfig;
        return appConfig && appConfig.nofAdmin;
    }

    isOrgAdmin() {
        const appConfig = this.winRef.appConfig;
        return appConfig && appConfig.orgAdmin;
    }

    private setLabels() {
        this.txtNavigationHeader = this.translation.translate('LeftNavbar.NavHeader');
        this.txtUserManagment = this.translation.translate('LeftNavbar.UserManagment');
        this.txtOrgManagment = this.translation.translate('LeftNavbar.OrgManagment');
        this.txtIcaoSubCond = this.translation.translate('LeftNavbar.IcaoSubCond');
        this.txtSeriesManagement = this.translation.translate('LeftNavbar.SeriesAllocation');
        this.txtADCManagement = this.translation.translate('LeftNavbar.AerodromeDisseminationCategory');
        this.txtDoasManagement = this.translation.translate('LeftNavbar.DomainOfAuthority');
        this.txtNsdsManagement = this.translation.translate('LeftNavbar.NSD');
        this.txtBilingualManagement = this.translation.translate('LeftNavbar.BilingualRegion');
        this.txtNorthernManagement = this.translation.translate('LeftNavbar.NorthernRegion');
        this.txtGeoRegionManagement = this.translation.translate('LeftNavbar.GeoRegion');
        this.txtConfigManagement = this.translation.translate('LeftNavbar.Config');
        this.txtCheckList = this.translation.translate('LeftNavbar.CheckList');
        this.txtAeroRDSSync = this.translation.translate('LeftNavbar.AeroRDS');
        this.txtSubscriptionManagement = this.translation.translate('LeftNavbar.Subscriptions');
    }
}
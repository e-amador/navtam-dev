﻿using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using NAVTAM.NsdEngine.TWYCLSD;
using NAVTAM.ViewModels;
using System.Threading.Tasks;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [Authorize]
    [RoutePrefix("api/taxiways")]
    public class TwyClsdController : ApiBaseController
    {
        public TwyClsdController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger) 
            : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
        }

        [HttpGet]
        [Route("FilterWithTwy")]
        public async Task<IHttpActionResult> FilterWithTwy([FromUri] ResourceQuery res)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                var doaGeoArea = await GeoLocationCache.GetUserDoa(Uow, ApplicationContext.GetUserId());
                var subjects = await Uow.SdoCacheRepo.FilterAerodromesWithTaxiwaysAsync(res.Query, res.Size, doaGeoArea);

                return Ok(subjects);

            }, "api/taxiways/FilterWithTwy");
        }

        [HttpGet]
        [Route("GetTwys")]
        public async Task<IHttpActionResult> GetTwys(string sdoId)
        {
            return await LogAction(async () => Ok(await Uow.SdoCacheRepo.GetAerodromeTaxiwaysAsync(sdoId)), "api/taxiways/GetTwys");
        }

        [HttpGet]
        [Route("GetTwyClsdReasons")]
        public IHttpActionResult GetTwyClsdReasons()
        {
            return LogAction(() => Ok(TwyClosureReasons.GetClosureReasons()), "api/taxiways/GetTwyClsdReasons");
        }
    }
}
﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF.Extensions;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    public class ProposalRepo : Repository<Proposal>, IProposalRepo
    {
        public ProposalRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public async Task<Proposal> GetByIdAsync(int id)
        {
            return await DbSet.SingleOrDefaultAsync(e => e.Id == id);
        }

        public async Task<List<Proposal>> GetGroupedProposalsByGroupIdAsync(Guid groupId)
        {
            return await DbSet.Where(e => e.GroupId == groupId).ToListAsync();
        }

        public async Task<List<ProposalDto>> GetByCategoryAsync(int categoryId, int organizationId, QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e =>
                    !e.Grouped &&
                    e.CategoryId == categoryId &&
                    e.Status != NotamProposalStatusCode.Terminated &&
                    (!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) && //e.Status != NotamProposalStatusCode.Expired
                    e.OrganizationId == organizationId &&
                    !e.Delete)
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalDto>> GetAllAsync(int organizationId, QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e =>
                    !e.Grouped &&
                    e.OrganizationId == organizationId &&
                    e.Status != NotamProposalStatusCode.Terminated &&
                    (!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) && //e.Status != NotamProposalStatusCode.Expired
                    !e.Delete)
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalStatusDto>> GetStatusAsync(int[] ids, DateTime since)
        {
            var culture = new CultureInfo("en-US");
            var proposals = await DbSet
                .Where(e => ids.Contains(e.Id))
                .ToListAsync();
            var Minutes = GetMinutesToExpire();
            var result = new List<ProposalStatusDto>();
            foreach (var p in proposals)
            {
                result.Add(new ProposalStatusDto
                {
                    Id = p.Id,
                    NotamId = p.NotamId,
                    StartActivity = (p.StartActivity.HasValue) ? p.StartActivity.Value.ToString("yyMMddHHmm", culture) : "IMMEDIATE",
                    EndValidity = (p.EndValidity.HasValue) ? p.EndValidity.Value.ToString("yyMMddHHmm", culture) : ((p.ProposalType == NotamType.C) ? "" : "PERM"),
                    Received = p.Received.ToString("yyMMddHHmm", culture),
                    Status = p.Status,
                    GroupId = p.GroupId.HasValue ? p.GroupId.ToString() : null,
                    SoonToExpire = IsSoonToExpire(p,Minutes),
                    ModifiedByUsr = p.ModifiedByUsr,
                    Operator = p.Operator,
                    Originator = p.Originator,
                    ItemE = p.ItemE,
                    ItemA = p.ItemA,
                    SiteId = p.ItemA == "CXXX" ? p.ItemE.Substring(0, 4) : "",
                    Estimated = p.Estimated,
                });
            }

            return result;
        }

        public async Task<int> GetCountByCategoryAsync(int categoryId, int organizationId, QueryOptions queryOptions)
        {
            if (categoryId == 1)
            {
                return await DbSet.CountAsync(e =>
                    e.OrganizationId == organizationId &&
                    !e.Grouped &&
                    e.Status != NotamProposalStatusCode.Terminated &&
                    (!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) && //e.Status != NotamProposalStatusCode.Expired
                    !e.Delete);
            }

            return await DbSet.CountAsync(e =>
                e.CategoryId == categoryId &&
                !e.Grouped &&
                e.Status != NotamProposalStatusCode.Terminated &&
                (!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) && //e.Status != NotamProposalStatusCode.Expired
                e.OrganizationId == organizationId &&
                !e.Delete);
        }

        public async Task<int> GetAllCountAsync(int organizationId, QueryOptions queryOptions)
        {
            return await DbSet.CountAsync(e =>
                e.OrganizationId == organizationId &&
                !e.Grouped &&
                e.Status != NotamProposalStatusCode.Terminated &&
                (!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) && //e.Status != NotamProposalStatusCode.Expired
                !e.Delete);
        }

        #region user proposals

        public async Task<int> GetUserProposalsInRegionCountAsync(DbGeography region, int orgId, OrganizationType orgType, int catId, QueryOptions queryOptions)
        {
            if (orgType == OrganizationType.External)
            {
                return await DbSet.CountAsync(e =>
                    (catId == 1 || e.CategoryId == catId) &&
                    (e.OrganizationId == orgId) &&
                    !e.Grouped &&
                    e.Status != NotamProposalStatusCode.Terminated &&
                    !e.SeriesChecklist &&
                    //When NOTAM is Estimated, we need to show the ones that have been NOT Cancelled, Deleted, Discarded, ModifiedCancelled,
                    //Replaced or Terminated
                    ((e.Estimated && e.Status != NotamProposalStatusCode.Cancelled && e.Status != NotamProposalStatusCode.Deleted &&
                    e.Status != NotamProposalStatusCode.Discarded && e.Status != NotamProposalStatusCode.ModifiedCancelled &&
                    e.Status != NotamProposalStatusCode.Replaced) ||
                    //e.Status != NotamProposalStatusCode.Rejected && 
                    //When NOTAM is not Estimated, we need to show the ones that are STILL VALIDS e.EndValidity > DateTime.UtcNow AND
                    //the ones that are NOT VALID IN TIME BUT have NOT been Disseminated, DisseminatedModified,
                    //Cancelled, Replaced, Deleted or Discarded
                    (!e.Estimated) &&
                    ((!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) ||
                     (
                        (!e.EndValidity.HasValue || e.EndValidity <= DateTime.UtcNow) &&
                        (e.Status != NotamProposalStatusCode.Disseminated &&
                        e.Status != NotamProposalStatusCode.DisseminatedModified &&
                        e.Status != NotamProposalStatusCode.Cancelled &&
                        e.Status != NotamProposalStatusCode.Replaced &&
                        e.Status != NotamProposalStatusCode.Deleted &&
                        e.Status != NotamProposalStatusCode.Expired &&
                        e.Status != NotamProposalStatusCode.Discarded)
                     )
                    )
                    ) && !e.Delete &&
                    e.Location != null &&
                    region.Intersects(e.Location));
            }

            if (orgType == OrganizationType.Fic)
            {
                //Note 1: The FIC user have to see everything, including the NOF Notam that intersect the FIC location
                //Note 2: Eric asked to allow FIC user to see the Proposals that are Expired but have never been
                //          Disseminated, Cancelled, Replaced, Deleted or Discarded
                return await DbSet.CountAsync(e =>
                    (catId == 1 || e.CategoryId == catId) &&
                    !e.Grouped &&
                    e.Status != NotamProposalStatusCode.Terminated &&
                    !e.SeriesChecklist &&
                    //When NOTAM is Estimated, we need to show the ones that have been NOT Cancelled, Deleted, Discarded, ModifiedCancelled,
                    //Replaced or Terminated
                    ((e.Estimated && e.Status != NotamProposalStatusCode.Cancelled && e.Status != NotamProposalStatusCode.Deleted &&
                    e.Status != NotamProposalStatusCode.Discarded && e.Status != NotamProposalStatusCode.ModifiedCancelled &&
                    e.Status != NotamProposalStatusCode.Replaced) ||
                    //e.Status != NotamProposalStatusCode.Rejected && 
                    //When NOTAM is not Estimated, we need to show the ones that are STILL VALIDS e.EndValidity > DateTime.UtcNow AND
                    //the ones that are NOT VALID IN TIME BUT have NOT been Disseminated, DisseminatedModified,
                    //Cancelled, Replaced, Deleted or Discarded
                    (!e.Estimated) &&
                    ((!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) ||
                     (
                        (!e.EndValidity.HasValue || e.EndValidity <= DateTime.UtcNow) &&
                        (e.Status != NotamProposalStatusCode.Disseminated &&
                        e.Status != NotamProposalStatusCode.DisseminatedModified &&
                        e.Status != NotamProposalStatusCode.Cancelled &&
                        e.Status != NotamProposalStatusCode.Replaced &&
                        e.Status != NotamProposalStatusCode.Deleted &&
                        e.Status != NotamProposalStatusCode.Expired &&
                        e.Status != NotamProposalStatusCode.Discarded)
                     )
                    )
                    ) && 
                    !e.Delete &&
                    e.Location != null &&
                    region.Intersects(e.Location));
            }

            // this is for Nof (just in case!)
            return await DbSet.CountAsync(e =>
                (catId == 1 || e.CategoryId == catId) &&
                !e.Grouped &&
                e.Status != NotamProposalStatusCode.Terminated &&
                !e.SeriesChecklist &&
                //When NOTAM is Estimated, we need to show the ones that have been NOT Cancelled, Deleted, Discarded, ModifiedCancelled,
                //Replaced or Terminated
                ((e.Estimated && e.Status != NotamProposalStatusCode.Cancelled && e.Status != NotamProposalStatusCode.Deleted &&
                e.Status != NotamProposalStatusCode.Discarded && e.Status != NotamProposalStatusCode.ModifiedCancelled &&
                e.Status != NotamProposalStatusCode.Replaced) ||
                //e.Status != NotamProposalStatusCode.Rejected && 
                //When NOTAM is not Estimated, we need to show the ones that are STILL VALIDS e.EndValidity > DateTime.UtcNow AND
                //the ones that are NOT VALID IN TIME BUT have NOT been Disseminated, DisseminatedModified,
                //Cancelled, Replaced, Deleted or Discarded
                (!e.Estimated) && 
                ((!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) ||
                 (
                    (!e.EndValidity.HasValue || e.EndValidity <= DateTime.UtcNow) &&
                    (e.Status != NotamProposalStatusCode.Disseminated &&
                    e.Status != NotamProposalStatusCode.DisseminatedModified &&
                    e.Status != NotamProposalStatusCode.Cancelled &&
                    e.Status != NotamProposalStatusCode.Replaced &&
                    e.Status != NotamProposalStatusCode.Expired &&
                    e.Status != NotamProposalStatusCode.Deleted &&
                    e.Status != NotamProposalStatusCode.Discarded)
                 )
                )
                ) && !e.Delete);
        }

        public async Task<List<ProposalDto>> GetUserProposalsInRegionAsync(DbGeography region, int orgId, OrganizationType orgType, int catId, QueryOptions queryOptions)
        {
            List<Proposal> proposals = null;

            if (orgType == OrganizationType.External)
            {
                proposals = await DbSet
                    .Include(e => e.IcaoSubject)
                    .Include(e => e.IcaoSubjectCondition)
                    .Include(e => e.Category)
                    .Where(e =>
                        (catId == 1 || e.CategoryId == catId) &&
                        (e.OrganizationId == orgId) &&
                        !e.Grouped &&
                        e.Status != NotamProposalStatusCode.Terminated &&
                        !e.SeriesChecklist &&
                        //When NOTAM is Estimated, we need to show the ones that have been NOT Cancelled, Deleted, Discarded, ModifiedCancelled,
                        //Replaced or Terminated
                        ((e.Estimated && e.Status != NotamProposalStatusCode.Cancelled && e.Status != NotamProposalStatusCode.Deleted &&
                        e.Status != NotamProposalStatusCode.Discarded && e.Status != NotamProposalStatusCode.ModifiedCancelled &&
                        e.Status != NotamProposalStatusCode.Replaced) ||
                        //e.Status != NotamProposalStatusCode.Rejected && 
                        //When NOTAM is not Estimated, we need to show the ones that are STILL VALIDS e.EndValidity > DateTime.UtcNow AND
                        //the ones that are NOT VALID IN TIME BUT have NOT been Disseminated, DisseminatedModified,
                        //Cancelled, Replaced, Deleted or Discarded
                        (!e.Estimated) &&
                        ((!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) ||
                         (
                            (!e.EndValidity.HasValue || e.EndValidity <= DateTime.UtcNow) &&
                            (e.Status != NotamProposalStatusCode.Disseminated &&
                            e.Status != NotamProposalStatusCode.DisseminatedModified &&
                            e.Status != NotamProposalStatusCode.Cancelled &&
                            e.Status != NotamProposalStatusCode.Replaced &&
                            e.Status != NotamProposalStatusCode.Expired &&
                            e.Status != NotamProposalStatusCode.Deleted &&
                            e.Status != NotamProposalStatusCode.Discarded)
                         )
                        )
                        ) && !e.Delete &&
                        e.Location != null &&
                        region.Intersects(e.Location))
                    .ApplySort(queryOptions.Sort)
                    .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                    .Take(queryOptions.PageSize)
                    .ToListAsync();
            }
            else if (orgType == OrganizationType.Fic)
            {
                //Note 1: The FIC user have to see everything, including the NOF Notam that intersect the FIC location
                //Note 2: Eric asked to allow FIC user to see the Proposals that are Expired but have never been
                //          Disseminated, Cancelled, Replaced, Deleted or Discarded
                proposals = await DbSet
                    .Include(e => e.IcaoSubject)
                    .Include(e => e.IcaoSubjectCondition)
                    .Include(e => e.Category)
                    //.Include(e=> e.User)
                    .Where(e =>
                        (catId == 1 || e.CategoryId == catId) &&
                        !e.Grouped &&
                        e.Status != NotamProposalStatusCode.Terminated &&
                        !e.SeriesChecklist &&
                        //When NOTAM is Estimated, we need to show the ones that have been NOT Cancelled, Deleted, Discarded, ModifiedCancelled,
                        //Replaced or Terminated
                        ((e.Estimated && e.Status != NotamProposalStatusCode.Cancelled && e.Status != NotamProposalStatusCode.Deleted &&
                        e.Status != NotamProposalStatusCode.Discarded && e.Status != NotamProposalStatusCode.ModifiedCancelled &&
                        e.Status != NotamProposalStatusCode.Replaced) || 
                        //e.Status != NotamProposalStatusCode.Rejected && 
                        //When NOTAM is not Estimated, we need to show the ones that are STILL VALIDS e.EndValidity > DateTime.UtcNow AND
                        //the ones that are NOT VALID IN TIME BUT have NOT been Disseminated, DisseminatedModified,
                        //Cancelled, Replaced, Deleted or Discarded
                        (!e.Estimated) &&
                        ((!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) ||
                         (
                            (!e.EndValidity.HasValue || e.EndValidity <= DateTime.UtcNow) &&
                            (e.Status != NotamProposalStatusCode.Disseminated &&
                            e.Status != NotamProposalStatusCode.DisseminatedModified &&
                            e.Status != NotamProposalStatusCode.Cancelled &&
                            e.Status != NotamProposalStatusCode.Replaced &&
                            e.Status != NotamProposalStatusCode.Deleted &&
                            e.Status != NotamProposalStatusCode.Expired &&
                            e.Status != NotamProposalStatusCode.Discarded)
                         )
                        )
                        ) &&
                        !e.Delete &&
                        e.Location != null &&
                        region.Intersects(e.Location))
                    .ApplySort(queryOptions.Sort)
                    .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                    .Take(queryOptions.PageSize)
                    .ToListAsync();
            }
            else
            {
                proposals = await DbSet
                    .Include(e => e.IcaoSubject)
                    .Include(e => e.IcaoSubjectCondition)
                    .Include(e => e.Category)
                    .Where(e =>
                        (catId == 1 || e.CategoryId == catId) &&
                        !e.Grouped &&
                        e.Status != NotamProposalStatusCode.Terminated &&
                        !e.SeriesChecklist &&
                        //When NOTAM is Estimated, we need to show the ones that have been NOT Cancelled, Deleted, Discarded, ModifiedCancelled,
                        //Replaced or Terminated
                        ((e.Estimated && e.Status != NotamProposalStatusCode.Cancelled && e.Status != NotamProposalStatusCode.Deleted &&
                        e.Status != NotamProposalStatusCode.Discarded && e.Status != NotamProposalStatusCode.ModifiedCancelled &&
                        e.Status != NotamProposalStatusCode.Replaced) ||
                        //e.Status != NotamProposalStatusCode.Rejected && 
                        //When NOTAM is not Estimated, we need to show the ones that are STILL VALIDS e.EndValidity > DateTime.UtcNow AND
                        //the ones that are NOT VALID IN TIME BUT have NOT been Disseminated, DisseminatedModified,
                        //Cancelled, Replaced, Deleted or Discarded
                        (!e.Estimated) &&
                        ((!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) ||
                         (
                            (!e.EndValidity.HasValue || e.EndValidity <= DateTime.UtcNow) &&
                            (e.Status != NotamProposalStatusCode.Disseminated &&
                            e.Status != NotamProposalStatusCode.DisseminatedModified &&
                            e.Status != NotamProposalStatusCode.Cancelled &&
                            e.Status != NotamProposalStatusCode.Replaced &&
                            e.Status != NotamProposalStatusCode.Expired &&
                            e.Status != NotamProposalStatusCode.Deleted &&
                            e.Status != NotamProposalStatusCode.Discarded)
                         )
                        )
                        ) && !e.Delete)
                    .ApplySort(queryOptions.Sort)
                    .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                    .Take(queryOptions.PageSize)
                    .ToListAsync();
            }

            return GenerateProposalDto(proposals);
        }

        public async Task<int> GetUserProposalsExpiringSoonCountAsync(DbGeography region, int orgId, OrganizationType orgType, int MinutesToExpire)
        {
            var DateToExpire = DateTime.UtcNow.AddMinutes(MinutesToExpire);
            if (orgType == OrganizationType.External)
            {
                //For External: only the ones that the Org created and intersec their DOA
                return await DbSet.CountAsync(e =>
                    (e.OrganizationId == orgId) &&
                    !e.Grouped &&
                    (e.Status != NotamProposalStatusCode.Cancelled && e.Status != NotamProposalStatusCode.Deleted &&
                        e.Status != NotamProposalStatusCode.Discarded && e.Status != NotamProposalStatusCode.ModifiedCancelled &&
                        e.Status != NotamProposalStatusCode.Rejected && e.Status != NotamProposalStatusCode.Replaced &&
                        e.Status != NotamProposalStatusCode.Terminated) &&
                    !e.SeriesChecklist &&
                    e.Estimated &&
                    (e.EndValidity.HasValue && e.EndValidity.Value < DateToExpire) &&
                    !e.Delete &&
                    e.Location != null &&
                    region.Intersects(e.Location));
            }

            if (orgType == OrganizationType.Fic)
            {
                //For FIC: all the Notams that are in the DOA, doesn't matter if it was created by a NOF
                return await DbSet.CountAsync(e =>
                    //(e.OrganizationType != OrganizationType.Nof) &&
                    !e.Grouped &&
                    (e.Status != NotamProposalStatusCode.Cancelled && e.Status != NotamProposalStatusCode.Deleted &&
                        e.Status != NotamProposalStatusCode.Discarded && e.Status != NotamProposalStatusCode.ModifiedCancelled &&
                        e.Status != NotamProposalStatusCode.Rejected && e.Status != NotamProposalStatusCode.Replaced &&
                        e.Status != NotamProposalStatusCode.Terminated) &&
                    !e.SeriesChecklist &&
                    e.Estimated &&
                    (e.EndValidity.HasValue && e.EndValidity.Value < DateToExpire) &&
                    !e.Delete &&
                    e.Location != null &&
                    region.Intersects(e.Location));
            }

            // For NOF: Only the ones that the NOF created
            return await DbSet.CountAsync(e =>
                !e.Grouped &&
                (e.OrganizationType == OrganizationType.Nof) &&
                    (e.Status != NotamProposalStatusCode.Cancelled && e.Status != NotamProposalStatusCode.Deleted &&
                        e.Status != NotamProposalStatusCode.Discarded && e.Status != NotamProposalStatusCode.ModifiedCancelled &&
                        e.Status != NotamProposalStatusCode.Rejected && e.Status != NotamProposalStatusCode.Replaced &&
                        e.Status != NotamProposalStatusCode.Terminated) &&
                !e.SeriesChecklist &&
                e.Estimated &&
                (e.EndValidity.HasValue && e.EndValidity.Value < DateToExpire) &&
                !e.Delete);
        }

        public async Task<List<ProposalDto>> GetUserProposalsExpiringSoonAsync(DbGeography region, int orgId, OrganizationType orgType, QueryOptions queryOptions, int MinutesToExpire)
        {
            var DateToExpire = DateTime.UtcNow.AddMinutes(MinutesToExpire);
            List<Proposal> proposals = null;

            if (orgType == OrganizationType.External)
            {
                //For External: only the ones that the Org created and intersec their DOA
                proposals = await DbSet
                    .Include(e => e.IcaoSubject)
                    .Include(e => e.IcaoSubjectCondition)
                    .Include(e => e.Category)
                    .Where(e =>
                        (e.OrganizationId == orgId) &&
                        !e.Grouped &&
                        (e.Status != NotamProposalStatusCode.Cancelled && e.Status != NotamProposalStatusCode.Deleted &&
                            e.Status != NotamProposalStatusCode.Discarded && e.Status != NotamProposalStatusCode.ModifiedCancelled &&
                            e.Status != NotamProposalStatusCode.Rejected && e.Status != NotamProposalStatusCode.Replaced &&
                            e.Status != NotamProposalStatusCode.Terminated) &&
                        !e.SeriesChecklist &&
                        e.Estimated &&
                        (e.EndValidity.HasValue && e.EndValidity.Value < DateToExpire) &&
                        !e.Delete &&
                        e.Location != null &&
                        region.Intersects(e.Location))
                    .ApplySort(queryOptions.Sort)
                    .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                    .Take(queryOptions.PageSize)
                    .ToListAsync();
            }
            else if (orgType == OrganizationType.Fic)
            {
                //For FIC: all the Notams that are in the DOA
                proposals = await DbSet
                    .Include(e => e.IcaoSubject)
                    .Include(e => e.IcaoSubjectCondition)
                    .Include(e => e.Category)
                    .Where(e =>
                        !e.Grouped &&
                        (e.Status != NotamProposalStatusCode.Cancelled && e.Status != NotamProposalStatusCode.Deleted &&
                            e.Status != NotamProposalStatusCode.Discarded && e.Status != NotamProposalStatusCode.ModifiedCancelled &&
                            e.Status != NotamProposalStatusCode.Rejected && e.Status != NotamProposalStatusCode.Replaced &&
                            e.Status != NotamProposalStatusCode.Terminated) &&
                        !e.SeriesChecklist &&
                        e.Estimated &&
                        (e.EndValidity.HasValue && e.EndValidity.Value < DateToExpire) &&
                        !e.Delete &&
                        e.Location != null &&
                        region.Intersects(e.Location))
                    .ApplySort(queryOptions.Sort)
                    .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                    .Take(queryOptions.PageSize)
                    .ToListAsync();
            }
            else
            {
                // For NOF: Only the ones that the NOF created
                proposals = await DbSet
                    .Include(e => e.IcaoSubject)
                    .Include(e => e.IcaoSubjectCondition)
                    .Include(e => e.Category)
                    .Where(e =>
                        //(e.OrganizationId == orgId) &&
                        !e.Grouped &&
                        (e.OrganizationType == OrganizationType.Nof) &&
                        (e.Status != NotamProposalStatusCode.Cancelled && e.Status != NotamProposalStatusCode.Deleted &&
                            e.Status != NotamProposalStatusCode.Discarded && e.Status != NotamProposalStatusCode.ModifiedCancelled &&
                            e.Status != NotamProposalStatusCode.Rejected && e.Status != NotamProposalStatusCode.Replaced &&
                            e.Status != NotamProposalStatusCode.Terminated) &&
                        !e.SeriesChecklist &&
                        e.Estimated &&
                        (e.EndValidity.HasValue && e.EndValidity.Value < DateToExpire) &&
                        !e.Delete)
                    .ApplySort(queryOptions.Sort)
                    .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                    .Take(queryOptions.PageSize)
                    .ToListAsync();
            }

            return GenerateProposalDto(proposals);
        }

        public async Task<int> GetCountByChildrenCategoryIdsAsync(DbGeography region, int[] categoryIds, int? orgId, QueryOptions queryOptions)
        {
            return await DbSet.CountAsync(e =>
                (!orgId.HasValue || e.OrganizationId == orgId.Value) &&
                !e.Grouped &&
                e.Status != NotamProposalStatusCode.Terminated &&
                (!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) && //e.Status != NotamProposalStatusCode.Expired
                !e.Delete &&
                categoryIds.Contains(e.CategoryId) &&
                region.Intersects(e.Location));
        }

        public async Task<List<ProposalDto>> GetByChildrenCategoryIdsAsync(DbGeography region, int[] categoryIds, int? orgId, QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e =>
                    (!orgId.HasValue || e.OrganizationId == orgId) &&
                    !e.Grouped &&
                    e.Status != NotamProposalStatusCode.Terminated &&
                    (!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow) && //e.Status != NotamProposalStatusCode.Expired
                    !e.Delete &&
                    categoryIds.Contains(e.CategoryId) &&
                    region.Intersects(e.Location))
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        #endregion user proposals

        public async Task<List<ProposalDto>> GetProposalsGroupedAsync(int organizationId, Guid groupId)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e =>
                    //e.OrganizationId == organizationId && At this time the orgId that created the grouping has to be the NOF
                    e.GroupId == groupId &&
                    !e.IsGroupMaster &&
                    (!e.EndValidity.HasValue || e.EndValidity > DateTime.UtcNow))
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalDto>> GetSubmittedByCategoryAsync(int categoryId, QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e => !e.Grouped && (e.CategoryId == categoryId && e.Status == NotamProposalStatusCode.Submitted))
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<int> GetSubmittedCountByCategoryAsync(int categoryId, QueryOptions queryOptions)
        {
            return await DbSet.CountAsync(e => !e.Grouped && (e.CategoryId == categoryId && e.Status == NotamProposalStatusCode.Submitted));
        }

        // pending queue
        public async Task<int> GetNofPendingQueueCountAsync()
        {
            return await DbSet.CountAsync(e => !e.Grouped && (e.Status == NotamProposalStatusCode.Submitted || e.Status == NotamProposalStatusCode.Picked));
        }

        public async Task<int> GetNofPendingQueueCountByCategoryIdAsync(int categoryId)
        {
            return await DbSet.CountAsync(e => !e.Grouped && e.CategoryId == categoryId && (e.Status == NotamProposalStatusCode.Submitted || e.Status == NotamProposalStatusCode.Picked));
        }

        public async Task<int> GetNofPendingByChildrenQueueCountByCategoryIdAsync(int[] categoryIds)
        {
            return await DbSet.CountAsync(e => !e.Grouped && categoryIds.Contains(e.CategoryId) && (e.Status == NotamProposalStatusCode.Submitted || e.Status == NotamProposalStatusCode.Picked));
        }

        public async Task<List<ProposalDto>> GetNofPendingQueueByCategoryIdAsync(int categoryId, QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e =>
                    !e.Grouped &&
                    e.CategoryId == categoryId &&
                    (e.Status == NotamProposalStatusCode.Submitted || e.Status == NotamProposalStatusCode.Picked))
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalDto>> GetNofPendingQueueByChildrenCategoryIdsAsync(int[] categoryIds, QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e =>
                    !e.Grouped &&
                    categoryIds.Contains(e.CategoryId) &&
                    (e.Status == NotamProposalStatusCode.Submitted || e.Status == NotamProposalStatusCode.Picked))
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalDto>> GetAllNofPendingQueueAsync(QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e => !e.Grouped && (e.Status == NotamProposalStatusCode.Submitted || e.Status == NotamProposalStatusCode.Picked))
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalDto>> GetNofPendingProposalsGroupedAsync(Guid groupId)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e => e.GroupId == groupId && !e.IsGroupMaster && (e.Status == NotamProposalStatusCode.Submitted || e.Status == NotamProposalStatusCode.Picked))
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        // park queue
        public async Task<int> GetNofParkedQueueCountAsync()
        {
            return await DbSet.CountAsync(e => !e.Grouped && (e.Status == NotamProposalStatusCode.Parked || e.Status == NotamProposalStatusCode.ParkedDraft || e.Status == NotamProposalStatusCode.ParkedPicked || e.Status == NotamProposalStatusCode.Taken));
        }

        public async Task<int> GetNofParkedQueueCountByCategoryIdAsync(int categoryId)
        {
            return await DbSet.CountAsync(e => !e.Grouped && e.CategoryId == categoryId && (e.Status == NotamProposalStatusCode.Parked || e.Status == NotamProposalStatusCode.ParkedDraft || e.Status == NotamProposalStatusCode.ParkedPicked || e.Status == NotamProposalStatusCode.Taken));
            //return await DbSet.CountAsync(e => !e.Grouped && e.CategoryId == categoryId && (e.Status == NotamProposalStatusCode.Submitted || e.Status == NotamProposalStatusCode.Picked));
        }

        public async Task<int> GetNofParkByChildrenQueueCountByCategoryIdAsync(int[] categoryIds)
        {
            return await DbSet.CountAsync(e => !e.Grouped && categoryIds.Contains(e.CategoryId) && (e.Status == NotamProposalStatusCode.Parked || e.Status == NotamProposalStatusCode.ParkedDraft || e.Status == NotamProposalStatusCode.ParkedPicked || e.Status == NotamProposalStatusCode.Taken));
        }

        public async Task<List<ProposalDto>> GetNofParkedQueueByCategoryIdAsync(int categoryId, QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e => !e.Grouped && e.CategoryId == categoryId && 
                    (e.Status == NotamProposalStatusCode.Parked || e.Status == NotamProposalStatusCode.ParkedDraft || e.Status == NotamProposalStatusCode.ParkedPicked || e.Status == NotamProposalStatusCode.Taken))
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalDto>> GetNofParkedByChildrenCategoryIdsAsync(int[] categoryIds, QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e => !e.Grouped && categoryIds.Contains(e.CategoryId) && 
                    (e.Status == NotamProposalStatusCode.Parked || e.Status == NotamProposalStatusCode.ParkedDraft || e.Status == NotamProposalStatusCode.ParkedPicked || e.Status == NotamProposalStatusCode.Taken))
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalDto>> GetAllNofParkedQueueByAsync(QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e => !e.Grouped && 
                    (e.Status == NotamProposalStatusCode.Parked || e.Status == NotamProposalStatusCode.ParkedDraft || e.Status == NotamProposalStatusCode.ParkedPicked || e.Status == NotamProposalStatusCode.Taken))
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalDto>> GetNofParkedProposalsGroupedAsync(Guid groupId)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e => e.GroupId == groupId && !e.IsGroupMaster && 
                    (e.Status == NotamProposalStatusCode.Parked || e.Status == NotamProposalStatusCode.ParkedDraft || e.Status == NotamProposalStatusCode.ParkedPicked || e.Status == NotamProposalStatusCode.Taken))
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        // review queue
        public async Task<int> GetNofReviewQueueCountAsync(int currentUserOrgId)
        {
            return await DbSet.CountAsync(e => !e.Grouped && e.OrganizationId == currentUserOrgId && e.Status != NotamProposalStatusCode.Terminated);
        }

        public async Task<int> GetNofReviewQueueCountByCategoryIdAsync(int currentUserOrgId, int categoryId)
        {
            return await DbSet.CountAsync(e => !e.Grouped && e.CategoryId == categoryId && e.OrganizationId == currentUserOrgId && e.Status != NotamProposalStatusCode.Terminated);
        }

        public async Task<int> GetNofReviewByChildrenQueueCountByCategoryIdAsync(int currentUserOrgId, int[] categoryIds)
        {
            return await DbSet.CountAsync(e => !e.Grouped && categoryIds.Contains(e.CategoryId) && e.OrganizationId == currentUserOrgId);
        }

        public async Task<List<ProposalDto>> GetNofReviewQueueByCategoryIdAsync(int currentUserOrgId, int categoryId, QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e => !e.Grouped && e.CategoryId == categoryId && e.OrganizationId == currentUserOrgId && e.Status != NotamProposalStatusCode.Terminated)
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalDto>> GetNofReviewByChildrenCategoryIdsAsync(int currentUserOrgId, int[] categoryIds, QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e => !e.Grouped && categoryIds.Contains(e.CategoryId) && e.OrganizationId == currentUserOrgId)
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalDto>> GetAllNofReviewQueueByAsync(int currentUserOrgId, QueryOptions queryOptions)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e => !e.Grouped && e.OrganizationId == currentUserOrgId && e.Status != NotamProposalStatusCode.Terminated)
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public async Task<List<ProposalDto>> GetNofReviewProposalsGroupedAsync(int currentUserOrgId, Guid groupId)
        {
            var proposals = await DbSet
                .Include(e => e.IcaoSubject)
                .Include(e => e.IcaoSubjectCondition)
                .Include(e => e.Category)
                .Where(e => e.GroupId == groupId && !e.IsGroupMaster && e.OrganizationId == currentUserOrgId)
                .ToListAsync();

            return GenerateProposalDto(proposals);
        }

        public List<Proposal> GetExpiredProposals()
        {
            return DbSet
                .Where(e =>
                    e.EndValidity.HasValue &&
                    !e.Estimated &&
                    (e.Status == NotamProposalStatusCode.ModifiedAndSubmitted ||
                    e.Status == NotamProposalStatusCode.Submitted) &&
                    e.EndValidity < DateTime.UtcNow)
                .ToList();
        }

        static bool IsObsoleteProposal(Proposal prop)
        {
            if (prop.ProposalType == NotamType.C) return true;
            if (prop.Status == NotamProposalStatusCode.Expired) return true;
            if (prop.Status != NotamProposalStatusCode.DisseminatedModified && prop.Status != NotamProposalStatusCode.Disseminated) return false;
            if (!prop.Estimated && prop.EndValidity.HasValue && prop.EndValidity < DateTime.UtcNow) return true;
            return false;
        }

        static bool IsSoonToExpire(Proposal prop, int min)
        {
            var retValue = false;
            if (!IsObsoleteProposal(prop) && prop.Estimated)
            {
                var DateToExpire = DateTime.UtcNow.AddMinutes(min);
                if (prop.Status != NotamProposalStatusCode.Cancelled && prop.Status != NotamProposalStatusCode.Deleted &&
                    prop.Status != NotamProposalStatusCode.Discarded && prop.Status != NotamProposalStatusCode.ModifiedCancelled &&
                    prop.Status != NotamProposalStatusCode.Rejected && prop.Status != NotamProposalStatusCode.Replaced &&
                    prop.Status != NotamProposalStatusCode.Terminated && !prop.SeriesChecklist && !prop.Delete &&
                    (prop.EndValidity.HasValue && prop.EndValidity.Value < DateToExpire)) retValue = true;

                //var DateToExpire = DateTime.UtcNow.AddMinutes(min);
                //if(!prop.EndValidity.HasValue || (prop.EndValidity > DateTime.UtcNow && prop.EndValidity <= DateToExpire))
                //{
                //    retValue = true;
                //}
            }
            return retValue;
        }

        private int GetMinutesToExpire()
        {
            var ctx = AppDbContext;
            if (ctx == null)
                throw new NullReferenceException("Unable to cast DbContext to AppDbContext");
            var ConfigValue = ctx.ConfigurationValues.FirstOrDefault(c => c.Category == "NOTAM" && c.Name == "NOTAMESTExpireTime");
            int Minutes;
            if (ConfigValue == null || !int.TryParse(ConfigValue.Value, out Minutes)) Minutes = CommonDefinitions.DefaultExpireMinutes;
            return Minutes;
        }

        private List<ProposalDto> GenerateProposalDto(List<Proposal> proposals)
        {
            var dtos = new List<ProposalDto>();
            var culture = new CultureInfo("en-US");
            var Minutes = GetMinutesToExpire();

            foreach (var proposal in proposals)
            {
                dtos.Add(new ProposalDto
                {
                    Id = proposal.Id,
                    NotamId = proposal.NotamId,
                    CategoryId = proposal.CategoryId,
                    CategoryName = proposal.Category.Name,
                    ItemA = proposal.ItemA,
                    SiteId = proposal.ItemA == "CXXX" ? proposal.ItemE.Substring(0, 4) : "",
                    IcaoSubjectDesc = proposal.IcaoSubject.Name,
                    IcaoConditionDesc = proposal.IcaoSubjectCondition.Description,
                    StartActivity = (proposal.StartActivity.HasValue) ? proposal.StartActivity.Value.ToString("yyMMddHHmm", culture) : "IMMEDIATE",
                    EndValidity = (proposal.EndValidity.HasValue) ? proposal.EndValidity.Value.ToString("yyMMddHHmm", culture) : ((proposal.ProposalType == NotamType.C) ? "" : "PERM"),
                    Estimated = proposal.Estimated,
                    Received = proposal.Received,
                    ProposalType = proposal.ProposalType,
                    Status = proposal.Status,
                    GroupId = proposal.GroupId.HasValue ? proposal.GroupId.ToString() : null,
                    SeriesChecklist = proposal.SeriesChecklist,
                    IsObsolete = IsObsoleteProposal(proposal),
                    Originator = proposal.Originator,
                    ModifiedByUsr = proposal.ModifiedByUsr,
                    Operator = proposal.Operator,
                    SoonToExpire = IsSoonToExpire(proposal, Minutes),
                    ItemE = proposal.ItemE,
                });
            }
            return dtos;
        }

    }
}
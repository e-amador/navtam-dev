﻿using NavCanada.Core.Domain.Model.Entitities;
using System.Data.Entity.ModelConfiguration;

namespace NavCanada.Core.Data.EF.Configurations
{
    public class SeriesChecklistConfig : EntityTypeConfiguration<SeriesChecklist>
    {
        public SeriesChecklistConfig()
        {
            HasKey(e => e.Series);
            Property(e => e.Series).HasMaxLength(1);
            Property(e => e.Coordinates).HasMaxLength(32);
            Property(e => e.Firs).HasMaxLength(32);
        }
    }
}

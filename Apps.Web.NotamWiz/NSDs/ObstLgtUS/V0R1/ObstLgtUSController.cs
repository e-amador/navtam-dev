using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.NSDs.ObstLgtUS.V0R1
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Spatial;
    using System.Device.Location;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    using NavCanada.Applications.NotamWiz.Web.Code.Extentions;
    using NavCanada.Applications.NotamWiz.Web.Code.GeoTools;
    using NavCanada.Applications.NotamWiz.Web.Code.Helpers;
    using NavCanada.Applications.NotamWiz.Web.Models;
    using NavCanada.Core.Common.Contracts;
    using NavCanada.Core.Data.NotamWiz.Contracts;
    using NavCanada.Core.Domain.Model.Enums;

    public class ObstLgtUSController : NsdBaseController
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ObstLgtUSController));

        private readonly ObstacleTypes obstacleTypes = new ObstacleTypes();

        public ObstLgtUSController(IUow uow, IClusterQueues clusterQueues) : base(uow, clusterQueues)
        {
        }

        public override ActionResult Index(string id)
        {
            TempData["ObstacleTypes"] = (from o in this.obstacleTypes[Thread.CurrentThread.CurrentUICulture.Name] select new ValueText
            {
                Value = o.Key,
                Text = o.Value.Name
            }).ToList();
            ViewBag.ObstacleTypes = Json(TempData["ObstacleTypes"]);
            return View(new ViewModel());
        }

        protected override async Task<Tuple<ProcessResult, NotamProposalViewModel>> GenerateAsync(string proposalType, NotamProposalViewModel notamProposal, NotamProposalViewModelBase notamProposalBase)
        {
            this.log.Object(new
            {
                Method = "GenerateAsync",
                Parameters = new
                {
                    proposalType,
                    notamProposal,
                    notamProposalBase
                }
            });
            var proposal = notamProposalBase as ViewModel;
            if (proposal != null)
            {
                notamProposal.IsCatchAll = false;
                notamProposal.ContainFreeText = false;
                notamProposal.Type = proposalType;
                if (proposalType == "C")
                {
                    notamProposal.ReferredNumber = notamProposal.SubmittedNotam.Number;
                    notamProposal.ReferredSeries = notamProposal.SubmittedNotam.Series;
                    notamProposal.ReferredYear = notamProposal.SubmittedNotam.Year;
                    
                    var itemELatLong = GeoDataService.IsValidDecimalLocation(proposal.Location) ? new GeoCoordinate().FromLocation(proposal.Location).ToDMSLocation() : proposal.Location;
                    notamProposal.Code45 = "AK";
                    notamProposal.ItemE = string.Format("OBST LGT {0} SVCBL", itemELatLong);
                    notamProposal.ItemEFrench = proposal.Bilingual ? notamProposal.ItemE : string.Empty;
                }
                else
                {
                    var itemEObstacleType = this.obstacleTypes["en"][proposal.ObstacleType].EFieldName;
                    var itemEObstacleTypeFrench = this.obstacleTypes["fr"][proposal.ObstacleType].EFieldName;
                    var itemELatLong = proposal.Location;
                    var itemEReferenceFeature = "AD";
                    var itemEReferenceFeatureFrench = "AD";
                    var itemEObstacleHeight = proposal.Height;
                    var itemEObstacleElevation = proposal.Elevation;
                    string alternateAD;
                    var obstacleLocation = new GeoCoordinate().FromLocation(itemELatLong);
                    var subjectLocation = new GeoCoordinate(Subject.SubjectGeoRefLat, Subject.SubjectGeoRefLong);
                    itemELatLong = GeoDataService.IsValidDecimalLocation(itemELatLong) ? obstacleLocation.ToDMSLocation() : itemELatLong;
                    var parts = itemELatLong.Split(' ');
                    var obstacleGeoPoint = DbGeography.PointFromText(string.Format("POINT({0} {1})", GeoDataService.DMSToDecimal(parts[1]), GeoDataService.DMSToDecimal(parts[0])), GeoDataService.SRID);
                    var nearestAD = await GetNearestAd(obstacleGeoPoint);
                    if (nearestAD.Item1 > 5) // FIR 
                    {
                        alternateAD = nearestAD.Item2.Designator.Any(char.IsDigit) ? string.Format("{0} {1}\n", nearestAD.Item2.Designator, nearestAD.Item2.Name) : GetItemEPrefix(Subject);
                        notamProposal.ItemA = Subject.Fir;
                        foreach (var fir in GeoDataService.GetFIRsGeoInfo().Where(a => a.Key != Subject.Fir && a.Value.Distance(obstacleGeoPoint) <= GeoDataService.NMToMeters(2)))
                        {
                            notamProposal.ItemA += string.Format(" {0}", fir.Key);
                        }
                        var ahp = AeroRdsUtility.GetAhpByMid(nearestAD.Item2.Mid);
                        if (ahp == null)
                        {
                            throw new Exception("AHP not found.");
                        }
                        var nearestADRunways = AeroRdsUtility.GetExpandedRwyForAhp(nearestAD.Item2.Mid);
                        var nearestADHasWaterRunways = nearestADRunways.Any() && nearestADRunways.Any(r => r.TxtDesig.StartsWith("RWY-W"));
                        var nearestADIsHeliport = ahp.CodeType.Equals("HP");
                        if (nearestADHasWaterRunways)
                        {
                            itemEReferenceFeature = string.Format("(WATER) {0}", itemEReferenceFeature);
                            itemEReferenceFeatureFrench = string.Format("(HYDRO) {0}", itemEReferenceFeatureFrench);
                        }
                        if (nearestADIsHeliport)
                        {
                            itemEReferenceFeature = string.Format("(HELI) {0}", itemEReferenceFeature);
                            itemEReferenceFeatureFrench = itemEReferenceFeature;
                        }
                        itemEReferenceFeature = string.Format("{0} {1}", nearestAD.Item2.Name, itemEReferenceFeature);
                        itemEReferenceFeatureFrench = string.Format("{0} {1}", nearestAD.Item2.Name, itemEReferenceFeatureFrench);
                        notamProposal.Radius = 2;
                        notamProposal.Scope = "E";
                        var seriesAllocation = (from s in Uow.SeriesAllocationRepo.GetAll() join r in Uow.GeoRegionRepo.GetAll() on s.RegionId equals r.Id where s.QCode == "OL" && s.DisseminationCategory == DisseminationCategory.International && r.Geo.Intersects(obstacleGeoPoint) select s).FirstOrDefault();
                        notamProposal.Series = seriesAllocation == null ? "X" : seriesAllocation.Series;
                        notamProposal.Traffic = "V";
                    }
                    else
                    {
                        alternateAD = nearestAD.Item2.Designator.Any(char.IsDigit) ? string.Format("{0} {1}\n", nearestAD.Item2.Designator, nearestAD.Item2.Name) : GetItemEPrefix(Subject);
                        notamProposal.ItemA = GetUpdatedItemA(nearestAD.Item2.Designator);
                        if (nearestAD.Item2.SubjectGeoRefPoint.Latitude != null)
                        {
                            if (nearestAD.Item2.SubjectGeoRefPoint.Longitude != null)
                            {
                                subjectLocation = new GeoCoordinate((double)nearestAD.Item2.SubjectGeoRefPoint.Latitude, (double)nearestAD.Item2.SubjectGeoRefPoint.Longitude);
                            }
                        }
                        notamProposal.Radius = 5;
                        notamProposal.Scope = "AE";
                        notamProposal.Series = await GetNotamProposalSeriesBySubjectCode(nearestAD.Item2.Designator, "OL");
                        notamProposal.Traffic = "IV";
                    }
                    notamProposal.Code23 = "OL";
                    notamProposal.Code45 = "AS";
                    notamProposal.Purpose = "M";
                    notamProposal.Lower = 0;
                    notamProposal.Upper = (proposal.Elevation + 100) / 100;
                    var itemEDistanceFrom = Math.Round(GeoDataService.MetersToNM(obstacleLocation.GetDistanceTo(subjectLocation)));
                    var itemECardinalPoint = GeoDataService.GetDirection(subjectLocation.BearingTo(obstacleLocation));
                    var itemE = string.Format("OBST LGT U/S {0} {1} (APRX {2}NM {3} {4}) {5}FT AGL, {6}FT AMSL.", itemEObstacleType, itemELatLong, itemEDistanceFrom, itemECardinalPoint, itemEReferenceFeature, itemEObstacleHeight, itemEObstacleElevation);
                    var itemEFrench = string.Format("OBST LGT U/S {0} {1} (APRX {2}NM {3} {4}) {5}FT AGL, {6}FT AMSL.", itemEObstacleTypeFrench, itemELatLong, itemEDistanceFrom, itemECardinalPoint, itemEReferenceFeatureFrench, itemEObstacleHeight, itemEObstacleElevation);
                    if (string.IsNullOrEmpty(alternateAD))
                    {
                        notamProposal.ItemE = itemE;
                        notamProposal.ItemEFrench = proposal.Bilingual ? itemEFrench : string.Empty;
                    }
                    else
                    {
                        notamProposal.ItemE = string.Format("{0}{1}", alternateAD, itemE);
                        notamProposal.ItemEFrench = proposal.Bilingual ? string.Format("{0}{1}", alternateAD, itemEFrench) : string.Empty;
                    }
                    notamProposal.Fir = Subject.Fir;
                    notamProposal.Coordinates = GeoDataService.DMSToRoundedDM(proposal.Location);
                }
            }
            return new Tuple<ProcessResult, NotamProposalViewModel>(new ProcessResult(), notamProposal);
        }

        protected override async Task<NotamProposalViewModel> ProcessNewProposalAsync(NotamProposalViewModel notamProposal)
        {
            return notamProposal;
        }

        protected override async Task<ProcessResult> ValidateAsync(NotamProposalViewModelBase notamProposalBase)
        {
            this.log.Object(new
            {
                Method = "ValidateAsync",
                Parameters = new
                {
                    notamProposalBase
                }
            });
            if (AeroRdsUtility.GetAhpByMid(Subject.SubjectMid) == null)
            {
                return new ProcessResult
                {
                    Success = false,
                    ErrorCode = "AOAC-003",
                    Errors = new List<string>
                    {
                        "Invalid Aerodrome."
                    }
                };
            }
            var validator = new ObstLgtUSValidator(this, DbRes);
            var validationResult = await validator.ValidateAsync(notamProposalBase as ViewModel);
            if (!validationResult.IsValid)
            {
                var result = new ProcessResult
                {
                    Success = false,
                    ErrorCode = "AOAC-001"
                };
                foreach (var failure in validationResult.Errors)
                {
                    result.Errors.Add(failure.ErrorMessage);
                }
                return result;
            }
            return new ProcessResult();
        }

        private class ObstacleType
        {
            public ObstacleType(string name, string eFieldName)
            {
                Name = name;
                EFieldName = eFieldName;
            }

            public string EFieldName
            {
                get;
                private set;
            }

            public string Name
            {
                get;
                private set;
            }
        }

        private class ObstacleTypes
        {
            private readonly Dictionary<string, Dictionary<string, ObstacleType>> dictionary = new Dictionary<string, Dictionary<string, ObstacleType>>
            {
                { "en", new Dictionary<string, ObstacleType>
                {
                    { "Obs01", new ObstacleType("Tower", "TOWER") },
                    { "Obs02", new ObstacleType("NDB Tower", "NDB TOWER") },
                    { "Obs03", new ObstacleType("Smoke Stack", "SMOKE STACK") },
                    { "Obs04", new ObstacleType("Wind Turbine", "WIND TURBINE") },
                    { "Obs05", new ObstacleType("Drill Rig", "DRILL RIG") },
                    { "Obs06", new ObstacleType("Crane", "CRANE") },
                    { "Obs07", new ObstacleType("Bridge", "BRIDGE") },
                    { "Obs08", new ObstacleType("Cable Crossing", "CABLE CROSSING") },
                    { "Obs09", new ObstacleType("Building", "BUILDING") },
                    { "Obs10", new ObstacleType("Rig On Land", "RIG") },
                    { "Obs11", new ObstacleType("Rig On Water", "RIG") },
                    { "Obs12", new ObstacleType("Mobile Crane", "MOBILE CRANE") },
                    { "Obs13", new ObstacleType("Obst", "OBST") },
                    { "Obs14", new ObstacleType("Antenna", "ANTENNA") }
                } },
                { "fr", new Dictionary<string, ObstacleType>
                {
                    { "Obs01", new ObstacleType("Tour", "TOUR") },
                    { "Obs02", new ObstacleType("Tour NDB", "TOUR NDB") },
                    { "Obs03", new ObstacleType("Cheminée", "CHEMINEE") },
                    { "Obs04", new ObstacleType("Éolienne", "EOLIENNE") },
                    { "Obs05", new ObstacleType("Équipement de forage", "EQUIPEMENT DE FORAGE") },
                    { "Obs06", new ObstacleType("Grue", "GRUE") },
                    { "Obs07", new ObstacleType("Pont", "PONT") },
                    { "Obs08", new ObstacleType("Traverse de câbles", "TRAVERSE DE CABLE") },
                    { "Obs09", new ObstacleType("Bâtiment", "BATIMENT") },
                    { "Obs10", new ObstacleType("Derrick", "DERRICK") },
                    { "Obs11", new ObstacleType("Plate-forme", "PLATE-FORME") },
                    { "Obs12", new ObstacleType("Grue mobile", "GRUE MOBILE") },
                    { "Obs13", new ObstacleType("Obst", "OBST") },
                    { "Obs14", new ObstacleType("Antenne", "ANTENNE") }
                } }
            };

            public Dictionary<string, ObstacleType> this[string language]
            {
                get
                {
                    return this.dictionary[language];
                }
            }
        }
    }
}
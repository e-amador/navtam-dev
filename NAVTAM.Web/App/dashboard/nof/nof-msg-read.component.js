"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NofMsgReadComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var angular_l10n_1 = require("angular-l10n");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var toastr_service_1 = require("./../../common/toastr.service");
var windowRef_service_1 = require("../../common/windowRef.service");
var data_service_1 = require("../shared/data.service");
var data_model_1 = require("../shared/data.model");
var MessageType = /** @class */ (function () {
    function MessageType() {
    }
    MessageType.Notification = 0;
    MessageType.Error = 1;
    return MessageType;
}());
var NofMsgReadComponent = /** @class */ (function () {
    function NofMsgReadComponent(toastr, fb, dataService, memStorageService, router, winRef, activatedRoute, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.activatedRoute = activatedRoute;
        this.locale = locale;
        this.translation = translation;
        //model: any = null;
        this.model = null;
        this.userTemplates = [];
        this.maxNumberOfTemplates = 4;
        this.selectedSubscription = null;
        this.searchFilter = {
            clientTypeStr: "",
            client: "",
            address: ""
        };
        this.addresses = [];
        this.addressIndexSelected = -1;
    }
    NofMsgReadComponent.prototype.ngOnInit = function () {
        this.model = this.activatedRoute.snapshot.data['model'];
        this.configureReactiveForm();
    };
    NofMsgReadComponent.prototype.backToMsgList = function () {
        var _this = this;
        this.dataService.unlockMessage(this.model.id)
            .subscribe(function (response) {
            if (response) {
                _this.router.navigate(['/dashboard/']);
            }
            else {
            }
        });
    };
    NofMsgReadComponent.prototype.updateParkStatus = function () {
        var _this = this;
        this.model.prevStatus = this.model.status;
        this.model.status = this.model.status === data_model_1.MessageStatus.Parked ? data_model_1.MessageStatus.None : data_model_1.MessageStatus.Parked;
        this.dataService.updateMessage(this.model)
            .subscribe(function (result) {
            if (result.anyError) {
                _this.model.status = _this.model.prevStatus;
                var error = result.status === data_model_1.MessageStatus.AlreadyTaken
                    ? "This message was already taken by " + result.lastOwner
                    : "The message was changed by " + result.lastOwner + " to " + result.status;
                _this.toastr.error(error, "", { 'positionClass': 'toast-bottom-right' });
                return;
            }
            var message = _this.model.status === data_model_1.MessageStatus.Parked
                ? _this.translation.translate('MessageQueue.SuccessMessageParked')
                : _this.translation.translate('MessageQueue.SuccessMessageUnparked');
            _this.toastr.success(message, "", { 'positionClass': 'toast-bottom-right' });
            _this.router.navigate(['/dashboard/']);
        }, function (error) {
            _this.toastr.error(_this.translation.translate('MessageQueue.FailureMessageParked') + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    NofMsgReadComponent.prototype.updateImportantStatus = function () {
        var _this = this;
        this.model.prevStatus = this.model.status;
        this.model.status = this.model.status === data_model_1.MessageStatus.Important ? data_model_1.MessageStatus.None : data_model_1.MessageStatus.Important;
        this.dataService.updateMessage(this.model)
            .subscribe(function (result) {
            if (result.anyError) {
                _this.model.status = _this.model.prevStatus;
                var error = result.status === data_model_1.MessageStatus.AlreadyTaken
                    ? "This message was already taken by " + result.lastOwner
                    : "The message was changed by " + result.lastOwner + " to " + result.status;
                _this.toastr.error(error, "", { 'positionClass': 'toast-bottom-right' });
                return;
            }
            var message = _this.model.status === data_model_1.MessageStatus.Important
                ? _this.translation.translate('MessageQueue.SuccessMessageImportant')
                : _this.translation.translate('MessageQueue.SuccessMessageUnimportant');
            _this.toastr.success(message, "", { 'positionClass': 'toast-bottom-right' });
            _this.router.navigate(['/dashboard/']);
        }, function (error) {
            _this.toastr.error(_this.translation.translate('MessageQueue.FailureMessageImportant') + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    NofMsgReadComponent.prototype.updateDeleteStatus = function () {
        var _this = this;
        this.model.prevStatus = this.model.status;
        this.model.status = this.model.status === data_model_1.MessageStatus.Deleted ? data_model_1.MessageStatus.None : data_model_1.MessageStatus.Deleted;
        this.dataService.updateMessage(this.model)
            .subscribe(function (result) {
            if (result.anyError) {
                _this.model.status = _this.model.prevStatus;
                var error = result.status === data_model_1.MessageStatus.AlreadyTaken
                    ? "This message was already taken by " + result.lastOwner
                    : "The message was changed by " + result.lastOwner + " to " + result.status;
                _this.toastr.error(error, "", { 'positionClass': 'toast-bottom-right' });
                return;
            }
            var message = _this.model.status === data_model_1.MessageStatus.Deleted
                ? _this.translation.translate('MessageQueue.SuccessMessageDeleted')
                : _this.translation.translate('MessageQueue.SuccessMessageUndeleted');
            _this.toastr.success(message, "", { 'positionClass': 'toast-bottom-right' });
            _this.router.navigate(['/dashboard/']);
        }, function (error) {
            _this.toastr.error(_this.translation.translate('MessageQueue.FailureMessageDeleted') + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    NofMsgReadComponent.prototype.toClientTypeStr = function (clientType) {
        switch (clientType) {
            case data_model_1.ClientType.AFTN: return 'AFTN';
            case data_model_1.ClientType.SWIM: return 'SWIM';
            case data_model_1.ClientType.REST: return 'REST';
            default: return '';
        }
    };
    NofMsgReadComponent.prototype.configureReactiveForm = function () {
        this.msgForm = this.fb.group({
            originator: [{ value: this.model.clientName, disabled: true }],
            recipients: [{ value: this.model.recipients, disabled: true }],
            messageBody: [{ value: this.model.body, disabled: true }],
            priorityCode: [{ value: this.model.priorityCode, disabled: true }],
        });
    };
    NofMsgReadComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/dashboard/nof/nof-msg-read.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], NofMsgReadComponent);
    return NofMsgReadComponent;
}());
exports.NofMsgReadComponent = NofMsgReadComponent;
//# sourceMappingURL=nof-msg-read.component.js.map
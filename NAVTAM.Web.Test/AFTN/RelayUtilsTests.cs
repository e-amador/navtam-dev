﻿using Business.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NDS.Relay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NAVTAM.Web.Test.AFTN
{
    [TestClass]
    public class RelayUtilsTests
    {
        /// <summary>
        /// Sample NOTAM used in many tests.
        /// </summary>
        static Notam GetNotam(string itemE = "RWY 18/0W CLOSED DUE GEESE", string itemEF = "RWY 18/0W FERME AVEC LE CANUCK")
        {
            return new Notam()
            {
                Series = "A",
                ItemA = "CYOW",
                Fir = "CZUL",
                Coordinates = "491140N 1231240W",
                Code23 = "12",
                Code45 = "34",
                Traffic = "IV",
                Purpose = "BO",
                Number = 11,
                Year = 2018,
                Scope = "A",
                Type = NavCanada.Core.Domain.Model.Enums.NotamType.N,
                LowerLimit = 12,
                UpperLimit = 34,
                StartActivity = new DateTime(2018, 03, 06),
                EndValidity = new DateTime(2018, 03, 31),
                ItemE = itemE,
                ItemEFrench = itemEF,
                NotamId = ICAOTextUtils.FormatNotamId("A", 11, 2018)
            };
        }

        const string ExpectedBroadCastNamespace = "NES/B/H/N_V1/A/CYOW/1234/IV/BO/A/012/034";

        [TestMethod]
        public void RelayUtils_GetBroadCastNamespace_test()
        {
            var target = RelayUtils.GetHubBroadcastNamespace(GetNotam());
            Assert.AreEqual(target, ExpectedBroadCastNamespace);
        }

        const string ExpectedAftnBroadCastNamespace = "NES/B/A/N_V1/A/CYOW/1234/IV/BO/A/012/034";

        [TestMethod]
        public void RelayUtils_GetAftnBroadcastNamespace_test()
        {
            var target = RelayUtils.GetAftnBroadcastNamespace(GetNotam());
            Assert.AreEqual(target, ExpectedAftnBroadCastNamespace);
        }

        [TestMethod]
        public void RelayUtils_GetAftnExchangeMessageNamespace_test()
        {
            Assert.AreEqual(RelayUtils.GetAftnExchangeMessageNamespace(), "NES/B/A/M/");
        }

        [TestMethod]
        public void RelayUtils_GetNotamNamespace_test()
        {
            var target = RelayUtils.GetNotamNamespace(GetNotam(), "B", "A");
            Assert.AreEqual(target, "NES/B/A/N_V1/A/CYOW/1234/IV/BO/A/012/034");
        }

        [TestMethod]
        public void RelayUtils_GetHubMetadata_test()
        {
            var metadata = RelayUtils.GetHubMetadata(GetNotam(), Guid.NewGuid().ToString()).ToDictionary(p => p.Name);

            Assert.AreEqual(metadata[RelayFields.MSG_TYPE].Value, RelayValues.XML_NOTAM);
            Assert.AreEqual(metadata[RelayFields.Series].Value, "A");
            Assert.AreEqual(metadata[RelayFields.Item_A].Value, "CYOW");
            Assert.AreEqual(metadata[RelayFields.Q_Code].Value, "1234");
            Assert.AreEqual(metadata[RelayFields.Traffic].Value, "IV");
            Assert.AreEqual(metadata[RelayFields.Purpose].Value, "BO");
            Assert.AreEqual(metadata[RelayFields.Scope].Value, "A");
            Assert.AreEqual(metadata[RelayFields.Lower].Value, 12);
            Assert.AreEqual(metadata[RelayFields.Upper].Value, 34);
        }

        [TestMethod]
        public void RelayUtils_FormatAftnInternalAddresBlock_test()
        {
            var returnAddresses = RelayUtils.FormatAftnInternalAddresBlock("CYNHQNYNX CQZVNYNX CULSNYNX AA BBB CC DD EE", 3);
            Assert.AreEqual("CYNHQNYNX CQZVNYNX CULSNYNX\r\nAA BBB CC\r\nDD EE", returnAddresses);
        }

        [TestMethod]
        public void RelayUtils_GetAftnMetadata_test()
        {
            var messageId = "expected_message_id";
            var metadata = RelayUtils.GetAftnNotamMetadata(GetNotam(), messageId).ToDictionary(p => p.Name);

            Assert.AreEqual(metadata[RelayFields.MSG_TYPE].Value, RelayValues.AFTN_NOTAM);
            Assert.AreEqual(metadata[RelayFields.MSG_ID].Value, messageId);
            Assert.AreEqual(metadata[RelayFields.Series].Value, "A");
            Assert.AreEqual(metadata[RelayFields.Item_A].Value, "CYOW");
        }

        [TestMethod]
        public void RelayUtils_GetAftnExchangeMessageMetadata_test()
        {
            var messageId = "expected_message_id";

            var metadata = RelayUtils.GetAftnMessageMetadata(messageId).ToDictionary(p => p.Name);

            Assert.AreEqual(metadata[RelayFields.MSG_TYPE].Value, RelayValues.AFTN_MESSAGE);
            Assert.AreEqual(metadata[RelayFields.MSG_ID].Value, messageId);
        }

        [TestMethod]
        public void RelayUtils_GetHubBroadCastMessage_test()
        {
            var message = RelayUtils.GetHubBroadCastMessage(GetNotam());

            Assert.AreEqual(message.Format, NdsMessageFormat.Xml);
            Assert.AreEqual(message.Destination, ExpectedBroadCastNamespace);
        }

        [TestMethod]
        public void RelayUtils_GetAftnBroadcastMessages_test()
        {
            var notam = GetNotam();
            notam.ItemE = new string('A', 2000);
            notam.ItemEFrench = string.Empty;

            var addresses = "A B".Split(' ');

            var messages = RelayUtils.GetAftnBroadcastMessages(notam, Guid.NewGuid(), addresses, true, 1).ToList();
            Assert.AreEqual(messages.Count, 4);

            var moreThan31Addresses = "CTSCNDSG CTSCNAAA CTSCNBBB CTSCNCCC CTSCNDDD CTSCNEEE CTSCNFFF CTSCNGGG CTSCNHHH CTSCNIII CTSCNJJJ CTSCNKKK CTSCNLLL CTSCNMMM CTSCNNNN CTSCNOOO CTSCNPPP CTSCNQQQ CTSCNRRR CTSCNSSS CTSCNTTT AFTNCAATS".Split(' ');

            messages = RelayUtils.GetAftnBroadcastMessages(notam, Guid.NewGuid(), moreThan31Addresses, true).ToList();
            Assert.AreEqual(messages.Count, 4);

            Assert.IsTrue(ValidateOutgoingMessages(messages));
        }

        [TestMethod]
        public void RelayUtils_GetAftnBroadcastMessages_with_priority_test()
        {
            var notam = GetNotam();
            notam.ItemE = "TEST";
            notam.ItemEFrench = string.Empty;

            var addresses = "CTSCNDSG".Split(' ');

            var message = RelayUtils.GetAftnBroadcastMessages(notam, Guid.NewGuid(), addresses, true, 1).FirstOrDefault();
            Assert.IsNotNull(message);

            var expectedStart = $"{AftnConsts.SOH}{AftnConsts.TRANS_ID_DATE}{AftnConsts.CRLF}GG";

            Assert.IsTrue(message.Content.StartsWith(expectedStart), "Must start with GG");

            notam.Urgent = true;

            message = RelayUtils.GetAftnBroadcastMessages(notam, Guid.NewGuid(), addresses, true, 1).FirstOrDefault();
            Assert.IsNotNull(message);

            expectedStart = $"{AftnConsts.SOH}{AftnConsts.TRANS_ID_DATE}{AftnConsts.CRLF}DD";

            Assert.IsTrue(message.Content.StartsWith(expectedStart), "Must start with DD");
        }

        [TestMethod]
        public void RelayUtils_GetMessageProperties_test()
        {
            var message = new NdsOutgoingMessage() { Metadata = "MSG_TYPE=AFTN_NOTAM\tSeries=A\tEmpty=" };

            var metadata = message.GetMessageProperties().ToDictionary(p => p.Name);

            Assert.AreEqual(metadata["MSG_TYPE"].Value, "AFTN_NOTAM");
            Assert.AreEqual(metadata["Series"].Value, "A");
            Assert.IsFalse(metadata.ContainsKey("Empty"));
        }

        [TestMethod]
        public void RelayUtils_GetAftnMessageContent_bilingual_test()
        {
            var notam = GetNotam();
            notam.StartActivity = new DateTime(2018, 03, 06);
            notam.EndValidity = new DateTime(2018, 03, 31);

            var expected = "(A0011/18 NOTAMN\r\nQ) CZUL/Q1234/IV/BO/A/012/034/4912N12313W000\r\nA) CYOW B) 1803060000 C) 1803310000\r\nE) RWY 18/0W CLOSED DUE GEESE\r\n\r\nFR:\r\nRWY 18/0W FERME AVEC LE CANUCK)\r\n";
            var content = RelayUtils.GetAftnMessageTextFromNotam(notam, "", true);

            Assert.AreEqual(content, expected);

            notam.ItemF = "abcd";
            notam.ItemG = "1234";
            expected = "(A0011/18 NOTAMN\r\nQ) CZUL/Q1234/IV/BO/A/012/034/4912N12313W000\r\nA) CYOW B) 1803060000 C) 1803310000\r\nE) RWY 18/0W CLOSED DUE GEESE\r\n\r\nFR:\r\nRWY 18/0W FERME AVEC LE CANUCK\r\nF) abcd G) 1234)\r\n";
            content = RelayUtils.GetAftnMessageTextFromNotam(notam, "", true);

            Assert.AreEqual(content, expected);

            notam.ItemD = "ALWAYS";
            notam.ItemF = null;
            notam.ItemG = null;

            expected = "(A0011/18 NOTAMN\r\nQ) CZUL/Q1234/IV/BO/A/012/034/4912N12313W000\r\nA) CYOW B) 1803060000 C) 1803310000\r\nD) ALWAYS\r\nE) RWY 18/0W CLOSED DUE GEESE\r\n\r\nFR:\r\nRWY 18/0W FERME AVEC LE CANUCK)\r\n";
            content = RelayUtils.GetAftnMessageTextFromNotam(notam, "", true);

            Assert.AreEqual(content, expected);


            // test the linebreak standarization
            notam.ItemE = "LINE1\nLINE2\r\nLINE3";
            notam.ItemEFrench = null;
            expected = "(A0011/18 NOTAMN\r\nQ) CZUL/Q1234/IV/BO/A/012/034/4912N12313W000\r\nA) CYOW B) 1803060000 C) 1803310000\r\nD) ALWAYS\r\nE) LINE1\r\nLINE2\r\nLINE3)\r\n";
            content = RelayUtils.GetAftnMessageTextFromNotam(notam, "", true);

            Assert.AreEqual(content, expected);
        }

        [TestMethod]
        public void RelayUtils_GetAftnMessageContent_non_bilingual_test()
        {
            var notam = GetNotam();
            notam.StartActivity = new DateTime(2018, 03, 06);
            notam.EndValidity = new DateTime(2018, 03, 31);

            var expected = "(A0011/18 NOTAMN\r\nQ) CZUL/Q1234/IV/BO/A/012/034/4912N12313W000\r\nA) CYOW B) 1803060000 C) 1803310000\r\nE) RWY 18/0W CLOSED DUE GEESE)\r\n";
            var content = RelayUtils.GetAftnMessageTextFromNotam(notam, "", false);

            Assert.AreEqual(content, expected);

            notam.ItemF = "abcd";
            notam.ItemG = "1234";
            expected = "(A0011/18 NOTAMN\r\nQ) CZUL/Q1234/IV/BO/A/012/034/4912N12313W000\r\nA) CYOW B) 1803060000 C) 1803310000\r\nE) RWY 18/0W CLOSED DUE GEESE\r\nF) abcd G) 1234)\r\n";
            content = RelayUtils.GetAftnMessageTextFromNotam(notam, "", false);

            Assert.AreEqual(content, expected);
        }

        [TestMethod]
        public void RelayUtils_GetAftnMessageContent_with_large_itemD()
        {
            var notam = GetNotam();

            notam.StartActivity = new DateTime(2018, 03, 06);
            notam.EndValidity = new DateTime(2018, 03, 31);
            notam.ItemD = "JAN 11 0000-0215, JAN 12 0200-0615, JAN 13 0115-0515, JAN 14 0030-0445, JAN 15 0130-0915";

            var expected = "(A0011/18 NOTAMN\r\nQ) CZUL/Q1234/IV/BO/A/012/034/4912N12313W000\r\nA) CYOW B) 1803060000 C) 1803310000\r\nD) JAN 11 0000-0215, JAN 12 0200-0615, JAN 13 0115-0515, JAN 14 \r\n0030-0445, JAN 15 0130-0915\r\nE) RWY 18/0W CLOSED DUE GEESE)\r\n";
            var content = RelayUtils.GetAftnMessageTextFromNotam(notam, "", false);

            Assert.AreEqual(content, expected);
        }

        [TestMethod]
        public void RelayUtils_GetAftnMessagePartHeader_test()
        {
            var notam = GetNotam();
            notam.StartActivity = new DateTime(2018, 03, 06);
            notam.EndValidity = new DateTime(2018, 03, 31);

            var expected = "(A0011/18F05 NOTAMN\r\nQ) CZUL/Q1234/IV/BO/A/012/034/4912N12313W000\r\nA) CYOW B) 1803060000 C) 1803310000\r\n";
            var content = RelayUtils.GetAftnMessagePartHeaderFromNotam(notam, "", 5, 5).ToString();
            Assert.AreEqual(content, expected);
        }

        [TestMethod]
        public void RelayUtils_TryToChunkText_test()
        {
            var chunk = RelayUtils.TryToChunkText("", 0, 5);
            Assert.AreEqual(chunk, 0, "expecting 0 -- because its an empty string");

            chunk = RelayUtils.TryToChunkText("01234", 0, 7);
            Assert.AreEqual(chunk, 5, "expecting 5 -- if there are no spaces in string");

            chunk = RelayUtils.TryToChunkText("01 346789abcdef", 0, 5);
            Assert.AreEqual(chunk, 3, "expecting 3 -- because thats where the space is");

            chunk = RelayUtils.TryToChunkText("01346789abcdef", 0, 5);
            Assert.AreEqual(chunk, 5, "expecting 5 -- because no space and thats the provided chunk length");

            chunk = RelayUtils.TryToChunkText("        ", 0, 5);
            Assert.AreEqual(chunk, 5, "expecting 5 -- all spaces");

            var text = "013 467 89abcdef";
            chunk = RelayUtils.TryToChunkText(text, 0, 6);
            Assert.AreEqual(chunk, 4, "expecting 4 -- we break after space");
        }

        [TestMethod]
        public void RelayUtils_TryToChunkText_avoid_breaking_CRLF_test()
        {
            var crlfChunk = RelayUtils.TryToChunkText("123456789\r\nABCD", 0, 10);
            Assert.AreEqual(9, crlfChunk);
        }

        [TestMethod]
        public void RelayUtils_GetTextChunkRanges_test()
        {
            var itemE = "123456789AB CDEFGHJKILOPASDHAZX FGDAGADGADJ ADLHAJDOG HDAJOGDAGAD";

            var chunks = RelayUtils.GetTextChunkRanges(itemE, 20).ToArray();

            Assert.AreEqual(chunks[0].Length, 12);
            Assert.AreEqual(chunks[1].Length, 20);
            Assert.AreEqual(chunks[2].Length, 12);
            Assert.AreEqual(chunks[3].Length, 10);
            Assert.AreEqual(chunks[4].Length, 11);
        }

        [TestMethod]
        public void RelayUtils_GetAftnMessageContentParts_one_part_test()
        {
            var notam = GetNotam();
            var parts = RelayUtils.GetAftnMessageTextParts(notam, "", true).ToArray();

            Assert.AreEqual(parts.Length, 1, "Must produce one part");
        }

        [TestMethod]
        public void RelayUtils_ReformatAftnText_test()
        {
            Assert.AreEqual(RelayUtils.ReformatAftnTextLines("", 10), "");

            Assert.AreEqual(RelayUtils.ReformatAftnTextLines("123456789", 10), "123456789", "Should return the same string as it is shorter than the chunk length");

            Assert.AreEqual(RelayUtils.ReformatAftnTextLines("123456789", 7), "1234567\r\n89", "Should chunk at char 7");

            Assert.AreEqual(RelayUtils.ReformatAftnTextLines("1234 678 9", 7), "1234 \r\n678 9", "Should chunk at proper interval");

            Assert.AreEqual(RelayUtils.ReformatAftnTextLines("1234 5\r\n6789", 7), "1234 5\r\n6789", "Should chunk at correct line breaking");

            Assert.AreEqual(RelayUtils.ReformatAftnTextLines("          ", 7), "       \r\n   ", "Should chunk at correct position when everithing is spaces");

            Assert.AreEqual(RelayUtils.ReformatAftnTextLines
            (
                "CTT5 LA ROMAINE\nTWY A BTN POINT A AND POINT B CLSD DUE REASON WITH NEW LINE\nENGLISH.", 66),
                "CTT5 LA ROMAINE\r\nTWY A BTN POINT A AND POINT B CLSD DUE REASON WITH NEW LINE\r\nENGLISH."
            );

            Assert.AreEqual(RelayUtils.ReformatAftnTextLines
            (
                "CTT5 LA ROMAINE\rTWY A BTN POINT A AND POINT B CLSD DUE REASON WITH NEW LINE\nENGLISH.", 66),
                "CTT5 LA ROMAINE\r\nTWY A BTN POINT A AND POINT B CLSD DUE REASON WITH NEW LINE\r\nENGLISH."
            );
        }

        [TestMethod]
        public void RelayUtils_GetAftnMessageContentParts_many_parts_test()
        {
            var notam = GetNotam();
            notam.ItemE = new string('A', 5000);
            var parts = RelayUtils.GetAftnMessageTextParts(notam, "", true, 1800).ToArray();

            Assert.IsTrue(parts.Length > 0, "Must produce more than one part");
            Assert.IsTrue(parts[0].Length <= 1800, "Must produce less the 1800 chars part");
            Assert.IsTrue(parts[1].Length <= 1800, "Must produce less the 1800 chars part");
        }

        [TestMethod]
        public void RelayUtils_GetAftnMessageContentParts_many_random_parts_test()
        {
            var notam = GetNotam();
            notam.ItemE = GetRandomText();
            var parts = RelayUtils.GetAftnMessageTextParts(notam, "", true, 1800).ToArray();

            Assert.IsTrue(parts.Length > 0, "Must produce more than one part");
            Assert.IsTrue(parts[0].Length <= 1800, "Must produce less the 1800 chars part, check the rug!");
        }

        #region Random messages utils

        static string GetRandomText()
        {
            StringBuilder sb = new StringBuilder();
            Random r = new Random();
            var length = 1800 + r.Next(5000);

            for (int i = 0; i < length; i++)
            {
                sb.Append(GetRandomChar(r));

                if (r.Next(length) < 100)
                    sb.Append("\r\n");
            }

            return sb.ToString();
        }

        const string RandomChars = "0123456789 ABCDEFGH";

        static char GetRandomChar(Random r)
        {
            var max = RandomChars.Length;
            var idx = r.Next(max);
            return RandomChars[idx];
        }

        #endregion Rug

        [TestMethod]
        public void RelayUtils_EstimateAftnTextLength_test()
        {
            Assert.AreEqual(RelayUtils.EstimateAftnTextLength("", 69), 0, "Empty string produces zero length.");

            Assert.AreEqual(RelayUtils.EstimateAftnTextLength("abcd", 69), "abcd".Length, "Short string produces its length.");

            var longerThan69 = new string('A', 70);
            Assert.AreEqual(RelayUtils.EstimateAftnTextLength(longerThan69, 69), longerThan69.Length + 2, "70 chars produces length = 71.");

            var variableLenghtsText1 = $"ABCD\r\n{longerThan69}";
            Assert.AreEqual(RelayUtils.EstimateAftnTextLength(variableLenghtsText1, 69), variableLenghtsText1.Length + 2, "Should produce one char more.");

            var variableLenghtsText2 = $"ABCD\r\n{longerThan69}EFGH\r\n{longerThan69}";
            Assert.AreEqual(RelayUtils.EstimateAftnTextLength(variableLenghtsText2, 69), variableLenghtsText2.Length + 4, "Should produce two chars more.");

            var veryLongText = new string('A', 2000);
            Assert.AreEqual(RelayUtils.EstimateAftnTextLength(veryLongText, 69), veryLongText.Length + 2 * (veryLongText.Length / 69), "Should produce one char extra per every 69 chars.");

            var onlyLinebreaks = "\r\n\r\n\r\n\r\n\r\n\r\n\r\n";
            Assert.AreEqual(RelayUtils.EstimateAftnTextLength(onlyLinebreaks, 69), onlyLinebreaks.Length, "Empty lines should produce no extra length.");
        }

        [TestMethod]
        public void RelayUtils_MetadataSerialization_test()
        {
            var expected = "MSG_ID=123\tMSG_TYPE=AFTN_MESSAGE";

            var props = RelayUtils.DeserializeNdsMetadata(expected);
            var serialized = RelayUtils.SerializeNdsMetadata(props);

            Assert.AreEqual(serialized, expected);
        }

        [TestMethod]
        public void RelayUtils_GetAftnRqnResponseMessages_Active_Notam_test()
        {
            var notam = GetNotam();

            var results = new List<AftnRqnResult>()
            {
                AftnRqnResult.CreateNotamResult(AftnRqnResultType.Active, notam, null)
            };

            var expected = $"\u0001%TRANS_ID_DATE%\r\nGG CTSCNAAA\r\n281721 {AftnConsts.NOFAddress}\r\n\u0002RQR CYHQ A0011/18\r\n(A0011/18 NOTAMN\r\nQ) CZUL/Q1234/IV/BO/A/012/034/4912N12313W000\r\nA) CYOW B) 1803060000 C) 1803310000\r\nE) RWY 18/0W CLOSED DUE GEESE\r\n\r\nFR:\r\nRWY 18/0W FERME AVEC LE CANUCK)\r\n\v\u0003";
            var messages = RelayUtils.GetAftnRqnResponseMessages(results, Guid.NewGuid(), "CTSCNAAA", true).ToList();

            Assert.AreEqual(messages.Count, 1);
            Assert.IsTrue(CompareAftnMessages(expected, messages[0].Content));

            Assert.IsTrue(ValidateOutgoingMessages(messages));
        }

        [TestMethod]
        public void RelayUtils_GetAftnRqnResponseMessages_Replaced_Notam_test()
        {
            var notam = GetNotam();

            var results = new List<AftnRqnResult>()
            {
                AftnRqnResult.CreateNotamResult(AftnRqnResultType.Replaced, notam, "A0021/18")
            };

            var expected = $"\u0001%TRANS_ID_DATE%\r\nGG CTSCNAAA\r\n281738 {AftnConsts.NOFAddress}\r\n\u0002RQR CYHQ A0011/18\r\nNOTAM REPLACED BY/NOTAM REMPLACE PAR A0021/18\r\n(A0011/18 NOTAMN\r\nQ) CZUL/Q1234/IV/BO/A/012/034/4912N12313W000\r\nA) CYOW B) 1803060000 C) 1803310000\r\nE) RWY 18/0W CLOSED DUE GEESE\r\n\r\nFR:\r\nRWY 18/0W FERME AVEC LE CANUCK)\r\n\v\u0003";
            var messages = RelayUtils.GetAftnRqnResponseMessages(results, Guid.NewGuid(), "CTSCNAAA", true).ToList();

            Assert.AreEqual(messages.Count, 1);
            Assert.IsTrue(CompareAftnMessages(expected, messages[0].Content));

            Assert.IsTrue(ValidateOutgoingMessages(messages));
        }

        [TestMethod]
        public void RelayUtils_GetAftnRqnResponseMessages_Canceled_Notam_test()
        {
            var notam = GetNotam();

            var results = new List<AftnRqnResult>()
            {
                AftnRqnResult.CreateNotamResult(AftnRqnResultType.Canceled, notam, "A0021/18")
            };

            var expected = $"\u0001%TRANS_ID_DATE%\r\nGG CTSCNAAA\r\n281738 {AftnConsts.NOFAddress}\r\n\u0002RQR CYHQ A0011/18\r\nNOTAM CANCELLED BY/NOTAM ANNULE PAR A0021/18\r\n(A0011/18 NOTAMN\r\nQ) CZUL/Q1234/IV/BO/A/012/034/4912N12313W000\r\nA) CYOW B) 1803060000 C) 1803310000\r\nE) RWY 18/0W CLOSED DUE GEESE\r\n\r\nFR:\r\nRWY 18/0W FERME AVEC LE CANUCK)\r\n\v\u0003";
            var messages = RelayUtils.GetAftnRqnResponseMessages(results, Guid.NewGuid(), "CTSCNAAA", true).ToList();

            Assert.AreEqual(messages.Count, 1);
            Assert.IsTrue(CompareAftnMessages(expected, messages[0].Content));

            Assert.IsTrue(ValidateOutgoingMessages(messages));
        }

        [TestMethod]
        public void RelayUtils_GetAftnRqnResponseMessages_Expired_Notam_test()
        {
            var notam = GetNotam();
            var notamNo = ICAOTextUtils.FormatNotamId(notam.Series, notam.Number, notam.Year);

            var results = new List<AftnRqnResult>()
            {
                AftnRqnResult.CreateNotamResult(AftnRqnResultType.Expired, notam, null)
            };

            var expected = $"\u0001%TRANS_ID_DATE%\r\nGG CTSCNAAA\r\n281738 {AftnConsts.NOFAddress}\r\n\u0002RQR CYHQ A0011/18\r\nNOTAM EXPIRED/NOTAM EXPIRE\r\n(A0011/18 NOTAMN\r\nQ) CZUL/Q1234/IV/BO/A/012/034/4912N12313W000\r\nA) CYOW B) 1803060000 C) 1803310000\r\nE) RWY 18/0W CLOSED DUE GEESE\r\n\r\nFR:\r\nRWY 18/0W FERME AVEC LE CANUCK)\r\n\v\u0003";
            var messages = RelayUtils.GetAftnRqnResponseMessages(results, Guid.NewGuid(), "CTSCNAAA", true).ToList();

            Assert.AreEqual(messages.Count, 1);
            Assert.IsTrue(CompareAftnMessages(expected, messages[0].Content));

            Assert.IsTrue(ValidateOutgoingMessages(messages));
        }

        [TestMethod]
        public void RelayUtils_GetAftnRqnResponseMessages_Missing_Notam_test()
        {
            var notam = GetNotam();
            var notamNumbers = "A0011/18".Split(' ');

            var results = new List<AftnRqnResult>()
            {
                AftnRqnResult.CreateMissingNotamsResult(notamNumbers)
            };

            var expected = $"\u0001%TRANS_ID_DATE%\r\nGG CTSCNAAA\r\n281800 {AftnConsts.NOFAddress}\r\n\u0002RQR CYHQ A0011/18\r\nNOTAM NOT ISSUED/NOTAM NON EMIS\r\n\v\u0003";
            var messages = RelayUtils.GetAftnRqnResponseMessages(results, Guid.NewGuid(), "CTSCNAAA", true).ToList();

            Assert.AreEqual(messages.Count, 1);
            Assert.IsTrue(CompareAftnMessages(expected, messages[0].Content));

            Assert.IsTrue(ValidateOutgoingMessages(messages));
        }

        [TestMethod]
        public void RelayUtils_GetAftnRqnResponseMessages_TooMany_Notams_test()
        {
            var notam = GetNotam();
            var notamNo = ICAOTextUtils.FormatNotamId(notam.Series, notam.Number, notam.Year);

            var results = new List<AftnRqnResult>()
            {
                AftnRqnResult.CreateLimitReachedResult()
            };

            var expected = $"\u0001%TRANS_ID_DATE%\r\nGG CTSCNAAA\r\n281954 {AftnConsts.NOFAddress}\r\n\u0002RQR CYHQ\r\nYOUR REQ MSG EXCEEDS MAX NR OF 100/VOTRE MSG REQ DEPASSE NR MAX DE \r\n100\r\n\v\u0003";
            var messages = RelayUtils.GetAftnRqnResponseMessages(results, Guid.NewGuid(), "CTSCNAAA", true).ToList();

            Assert.AreEqual(messages.Count, 1);
            Assert.IsTrue(CompareAftnMessages(expected, messages[0].Content));

            Assert.IsTrue(ValidateOutgoingMessages(messages));
        }

        [TestMethod]
        public void RelayUtils_GetAftnRqlResponseMessages_many_series_test()
        {
            var aftnMessage = new AftnMessage
            {
                TransId = "CGA0005",
                TransTime = "29133553",
                Priority = "GG",
                DestinationAddress = AftnConsts.NOFAddress,
                FilingTime = "290935",
                OriginAddress = "CTSCNDSE",
                Headers = $"CGA0005 29133553\r\nGG {AftnConsts.NOFAddress}\r\n290935 CTSCNDSE\r\n",
                Text = "RQL CYHQ A D\r\n"
            };

            var msg = AftnMessageParser.ParseRequestMessage(aftnMessage);
            Assert.IsNotNull(msg);

            var series = msg.RequestBody.Split(new char[] { ' ', '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            Assert.AreEqual(series.Count, 2);

            Func<string, AftnRqlResult> searchLambda = s => new AftnRqlResult(Enumerable.Empty<RqlNotamResultDto>());

            var messages = RelayUtils.GetAftnRqlResponseMessages(series, Guid.NewGuid(), "CTSCNDSE", searchLambda).ToList();

            Assert.AreEqual(messages.Count, 2);
            Assert.IsTrue(ValidateOutgoingMessages(messages));
        }

        [TestMethod]
        public void RelayUtils_GetAftnRqlResponseMessages_oneseries_manynotams_test()
        {
            var results = new List<RqlNotamResultDto>()
            {
                new RqlNotamResultDto()
                {
                    Year = 2018,
                    Numbers = new List<int>() { 1, 2, 3, 4, 2000 }
                }
            };

            Func<string, AftnRqlResult> searchLambda = s => new AftnRqlResult(results);

            var series = "A".Split(' ');
            var messages = RelayUtils.GetAftnRqlResponseMessages(series, Guid.NewGuid(), "CTSCNDSE", searchLambda).ToList();

            Assert.AreEqual(messages.Count, 1);
            Assert.IsTrue(ValidateOutgoingMessages(messages));
        }

        [TestMethod]
        public void RelayUtils_GetAftnRqlResponseMessages_oneseries_manyyears_manynotams_test()
        {
            var results = new List<RqlNotamResultDto>()
            {
                new RqlNotamResultDto() { Year = 2017, Numbers = new List<int>() { 1, 2, 3, 4, 2000 } },
                new RqlNotamResultDto() { Year = 2018, Numbers = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 } }
            };

            Func<string, AftnRqlResult> searchLambda = s => new AftnRqlResult(results);

            var series = "A".Split(' ');
            var messages = RelayUtils.GetAftnRqlResponseMessages(series, Guid.NewGuid(), "CTSCNDSE", searchLambda).ToList();

            Assert.AreEqual(messages.Count, 1);
            Assert.IsTrue(ValidateOutgoingMessages(messages));
        }

        [TestMethod]
        public void RelayUtils_GetAftnRqlResponseMessages_oneseries_oneyear_multipart_test()
        {
            var numbers = Enumerable.Range(1, 9000).ToList();
            var results = new List<RqlNotamResultDto>()
            {
                new RqlNotamResultDto() { Year = 2017, Numbers = numbers },
            };

            Func<string, AftnRqlResult> searchLambda = s => new AftnRqlResult(results);

            var series = "A".Split(' ');
            var messages = RelayUtils.GetAftnRqlResponseMessages(series, Guid.NewGuid(), "CTSCNDSE", searchLambda).ToList();

            Assert.AreEqual(messages.Count, 27);
            Assert.IsTrue(ValidateOutgoingMessages(messages));
        }

        [TestMethod]
        public void RelayUtils_GetAftnRqlResponseMessages_no_valid_notam_in_series_test()
        {
            Func<string, AftnRqlResult> searchLambda = s => new AftnRqlResult(Enumerable.Empty<RqlNotamResultDto>());

            var series = "A".Split(' ');
            var messages = RelayUtils.GetAftnRqlResponseMessages(series, Guid.NewGuid(), "CTSCNDSE", searchLambda).ToList();

            Assert.AreEqual(messages.Count, 1);
            Assert.IsTrue(ValidateOutgoingMessages(messages));
            Assert.IsTrue(messages[0].Content.Contains(AftnConsts.NoValidNotamInDatabase));
        }

        [TestMethod]
        public void RelayUtils_GetAftnRqlResponseMessages_unsupported_series_test()
        {
            Func<string, AftnRqlResult> searchLambda = s => AftnRqlResult.NoSeriesExistsResult;

            var series = "A".Split(' ');
            var messages = RelayUtils.GetAftnRqlResponseMessages(series, Guid.NewGuid(), "CTSCNDSE", searchLambda).ToList();

            Assert.AreEqual(messages.Count, 1);
            Assert.IsTrue(ValidateOutgoingMessages(messages));
            Assert.IsTrue(messages[0].Content.Contains(AftnConsts.SeriesNotManaged));
        }

        [TestMethod]
        public void RelayUtils_GetSingleRqrResponseMessage()
        {
            Assert.IsTrue(ValidateOutgoingMessage(RelayUtils.GetSingleRqrResponseMessage(Guid.NewGuid(), "A", "ABCDEFGH", "TEST MESSAGE")));
        }

        [TestMethod]
        public void RelayUtils_GetAftnExchangeMessages_test()
        {
            var shortText = "TEST";
            var messages = RelayUtils.GetAftnExchangeMessages(Guid.NewGuid(), "ABCDEFGH", "ABCDEFGH".Yield(), shortText).ToList();
            Assert.AreEqual(messages.Count, 1);
            Assert.IsTrue(ValidateOutgoingMessages(messages));

            var blocks = 1800 / 4;
            var longText = string.Join(" ", Enumerable.Range(1, blocks).Select(n => n.ToString().PadLeft(4, '0')));
            messages = RelayUtils.GetAftnExchangeMessages(Guid.NewGuid(), "ABCDEFGH", "ABCDEFGH".Yield(), longText).ToList();
            Assert.AreEqual(messages.Count, 2);
            Assert.IsTrue(ValidateOutgoingMessages(messages));
        }

        [TestMethod]
        public void RelayUtils_CreateChecklistMessage_test()
        {
            var list = new List<RqlNotamResultDto>
            {
                new RqlNotamResultDto { Year = 2016, Numbers = new List<int> { 1, 2, 3, 99 } },
                new RqlNotamResultDto { Year = 2018, Numbers = new List<int> { 1, 2 } },
            };

            var message = RelayUtils.CreateChecklistMessage(list, "CHECKLIST", "FOOTER");
            var expected = "CHECKLIST\r\nYEAR=2016 0001 0002 0003 0099\r\nYEAR=2017 NIL\r\nYEAR=2018 0001 0002\r\nFOOTER\r\n";
            Assert.AreEqual(message, expected, "Failed to generate a multi year list.");

            var emptyList = new List<RqlNotamResultDto>();
            message = RelayUtils.CreateChecklistMessage(emptyList, "CHECKLIST", "FOOTER");
            expected = $"CHECKLIST\r\nYEAR={DateTime.UtcNow.Year} NIL\r\nFOOTER\r\n";

            Assert.AreEqual(message, expected, "Failed to generate an empty year list.");
        }

        [TestMethod]
        public void RelayUtils_GroupOrdering_test()
        {
            var messages = (new int[] { 1, 2, 3, 4 }).Select(n => GetNotam($"NOTAM {n}")).Select(n => RelayUtils.GetHubBroadCastMessage(n));
            var ordering = RelayUtils.CreateMessageGroupOrdering(messages).ToList();

            Assert.AreEqual(ordering[0].GroupOrder, 1);
            Assert.AreEqual(ordering[0].NextId, ordering[1].Id);
            Assert.AreEqual(ordering[1].NextId, ordering[2].Id);
            Assert.AreEqual(ordering[2].NextId, ordering[3].Id);
            Assert.AreEqual(ordering[3].NextId, null);
        }

        static bool ValidateOutgoingMessages(IEnumerable<NdsOutgoingMessage> messages) => messages.All(ValidateOutgoingMessage);

        static bool ValidateOutgoingMessage(NdsOutgoingMessage message)
        {
            var msg = AftnMessageParser.ParseMessage(message.Content);
            if (msg == null)
                return false;

            var text = msg.Text;
            if (text.Length > RelayUtils.AftnTextMaxLength)
                return false;

            var lines = text.Split(new char[] { '\r', '\n' });

            return lines.All(l => l.Length <= RelayUtils.AftnLineLength);
        }

        static bool CompareAftnMessages(string message1, string message2)
        {
            if (message1.Length != message2.Length)
                return false;
            var textIndex = message1.IndexOf('\u0002');
            return textIndex >= 0 && message1.Substring(textIndex) == message2.Substring(textIndex);
        }
    }
}

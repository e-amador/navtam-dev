﻿using System;
using Microsoft.ServiceBus.Messaging;
using NDS.DataBus.Contracts;

namespace NDS.DataBus.ServiceBus
{
    public class QueueClientWrapper : IPostQueue, ISubscription
    {
        public QueueClientWrapper(QueueClient client)
        {
            _queueClient = client;
        }

        public void PostMessage(INdsMessage message)
        {
            var brokeredMessage = message as BrokeredMessageWrapper;
            if (brokeredMessage == null)
                throw new ArgumentException("Unexpected message type!");
            _queueClient.Send(brokeredMessage.InnerMessage);
        }

        public void Unsubscribe()
        {
            _queueClient?.Close();
            _queueClient = null;
        }

        public void Dispose()
        {
            Unsubscribe();
        }

        private QueueClient _queueClient;
    }
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NextObservationComponent = void 0;
var core_1 = require("@angular/core");
var angular_l10n_1 = require("angular-l10n");
var forms_1 = require("@angular/forms");
var rsc_service_1 = require("../rsc.service");
var moment = require("moment");
var toastr_service_1 = require("../../../../common/toastr.service");
var NextObservationComponent = /** @class */ (function () {
    function NextObservationComponent(fb, translation, rscService, toastr) {
        this.fb = fb;
        this.translation = translation;
        this.rscService = rscService;
        this.toastr = toastr;
        this.mainForm = null;
        this.isReadOnly = false;
        this.model = null;
        this.nextScheduledObservationText = "";
        this.dateFormat = "YYMMDDHHmm";
    }
    NextObservationComponent.prototype.ngOnInit = function () {
        this.isFormLoadingData = false;
        this.nextScheduledObservationMoment = moment.utc().add(8, 'hours');
        this.configureReactiveForm();
        //set Measure Date
        this.nextScheduledObservationText = this.nextScheduledObservationMoment.format("YYYY/MM/DD, H:mm") + " UTC";
        var fc = this.frmGroup.get('nextScheduledObservation');
        var formatedDate = this.nextScheduledObservationMoment.format(this.dateFormat);
        if (fc.value !== formatedDate) {
            fc.patchValue(formatedDate);
        }
        this.model.nextScheduledObservation = this.nextScheduledObservationMoment;
    };
    NextObservationComponent.prototype.ngOnChanges = function () {
    };
    NextObservationComponent.prototype.ngOnDestroy = function () {
    };
    NextObservationComponent.prototype.$ctrl = function (name) {
        return this.frmGroup.get(name);
    };
    NextObservationComponent.prototype.configureReactiveForm = function () {
        var frmArrControls = this.mainForm.controls['frmNextScheduledObservationArr'];
        this.frmGroup = this.fb.group({
            generalRemarks: [{ value: this.model.generalRemarks, disabled: this.isReadOnly }, [forms_1.Validators.required]],
            nextScheduledObservation: [{ value: this.nextScheduledObservationMoment, disabled: this.isReadOnly }, [forms_1.Validators.required]],
        });
        frmArrControls.push(this.frmGroup);
    };
    //Measure Date
    NextObservationComponent.prototype.isCalendarActive = function () {
        return true;
    };
    NextObservationComponent.prototype.setNextScheduledObservation = function (date, leaveEmpty) {
        var fc = this.frmGroup.get('nextScheduledObservation');
        if (date) {
            var momentDate = moment.utc(date);
            if (momentDate.isValid()) {
                this.nextScheduledObservationText = momentDate.format("YYYY/MM/DD, H:mm") + " UTC";
                var formatedDate = momentDate.format(this.dateFormat);
                if (fc.value !== formatedDate) {
                    fc.setValue(formatedDate);
                    fc.markAsDirty();
                    this.nextScheduledObservationMoment = momentDate;
                    this.model.nextScheduledObservation = this.nextScheduledObservationMoment;
                }
            }
        }
        else {
            if (leaveEmpty) {
                fc.setValue(null);
                fc.markAsDirty();
                this.model.nextScheduledObservation = null;
            }
            else {
                this.setNextScheduledObservation(moment.utc());
            }
        }
    };
    NextObservationComponent.prototype.validateNextScheduledObservation = function (errorName) {
        return true;
    };
    NextObservationComponent.prototype.onNextScheduledObservationInputChange = function (strDate) {
        if (strDate && strDate.length === 10) {
            var momentDate = moment.utc(strDate, this.dateFormat);
            if (momentDate.isValid()) {
                this.nextScheduledObservationText = momentDate.format("YYYY/MM/DD, H:mm") + " UTC";
                var fc = this.frmGroup.get('nextScheduledObservation');
                var formatedDate = momentDate.format(this.dateFormat);
                if (fc.value !== formatedDate) {
                    fc.setValue(formatedDate);
                    fc.markAsDirty();
                    this.nextScheduledObservationMoment = momentDate;
                    this.model.nextScheduledObservation = this.nextScheduledObservationMoment;
                }
            }
            else {
                this.nextScheduledObservationText = "";
                this.model.nextScheduledObservation = null;
            }
        }
        else {
            this.nextScheduledObservationText = "";
            this.model.nextScheduledObservation = null;
        }
    };
    //General functions
    NextObservationComponent.prototype.isValidNumber = function (value) {
        return ((value != null) &&
            (value !== '') &&
            !isNaN(Number(value.toString())));
    };
    NextObservationComponent.prototype.isANumber = function (str) {
        return !/\D/.test(str);
    };
    //Export functions
    NextObservationComponent.prototype.isFormValid = function () {
        return true;
    };
    NextObservationComponent.prototype.exportComponentInternalData = function () {
        var data = {
            parent: this.model.parent,
            nextScheduledObservation: this.nextScheduledObservationMoment,
            generalRemarks: this.model.generalRemarks,
        };
        return data;
    };
    NextObservationComponent.prototype.clearValues = function () {
        alert('Clear Component Values: NextObservation');
    };
    __decorate([
        core_1.Input('form'),
        __metadata("design:type", forms_1.FormGroup)
    ], NextObservationComponent.prototype, "mainForm", void 0);
    __decorate([
        core_1.Input('isReadOnly'),
        __metadata("design:type", Boolean)
    ], NextObservationComponent.prototype, "isReadOnly", void 0);
    __decorate([
        core_1.Input('model'),
        __metadata("design:type", Object)
    ], NextObservationComponent.prototype, "model", void 0);
    NextObservationComponent = __decorate([
        core_1.Component({
            selector: 'rsc-nextobservation',
            templateUrl: './app/dashboard/nsds/rsc/components/rsc-nextobservation.component.html'
        }),
        __param(3, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [forms_1.FormBuilder,
            angular_l10n_1.TranslationService,
            rsc_service_1.RscService, Object])
    ], NextObservationComponent);
    return NextObservationComponent;
}());
exports.NextObservationComponent = NextObservationComponent;
//# sourceMappingURL=rsc-nextobservation.component.js.map
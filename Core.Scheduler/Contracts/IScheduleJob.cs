﻿using Quartz;

namespace NavCanada.Core.Scheduler.Contracts
{
    public interface IScheduleJob 
    {
        IJobDetail Job { get; }
        ITrigger Trigger { get; }
        IClusterStorage ClusterStorage { get; }
    }
}

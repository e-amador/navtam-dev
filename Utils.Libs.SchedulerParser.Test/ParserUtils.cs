﻿using FParsec;
using Microsoft.FSharp.Core;
using Utils.Libs.SchedulerParser;

namespace ScheduleParserTest
{
    public static class ParserUtils
    {
        public static string CastFailure(CharParsers.ParserResult<Common.ParserResult, Unit> fshartObj)
        {
            var error = ((CharParsers.ParserResult<Common.ParserResult, Unit>.Failure)fshartObj).Item1;
            return error.Replace("\r\n", "").Replace("\n", "").Replace("\r", "");
        }

        public static Common.ParserResult CastSuccess(CharParsers.ParserResult<Common.ParserResult, Unit> fshartObj)
        {
            return ((CharParsers.ParserResult<Common.ParserResult, Unit>.Success)fshartObj).Item1 as Common.ParserResult;
        }

    }
}

﻿namespace NavCanada.Core.TaskEngine.Contracts
{
    public interface IMtoProcessorFactory
    {
        IMtoProcessor CreateMtoProcessor(IClusterStorage repositories, IMessageTaskFactory taskFactory, MtoProcessorSettings mtoProcessorSettings);
    }
}

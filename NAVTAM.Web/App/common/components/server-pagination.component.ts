﻿import {Component, Input, Output, EventEmitter, OnInit, OnChanges, SimpleChanges } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { LocaleService, TranslationService } from 'angular-l10n';

@Component({
    selector: 'server-pagination',
    styles:[`
        .btn-pagination:hover {
            background-color:#37393a;
	        border-color:#37393a
        }
        .btn-pagination{
            background-color:#707474;
	        border-color:#707474;
        }
        .btn-pagination[disabled] {
            background-color:#d6d6d6;
	        border-color:#d6d6d6;
        }
        .btn-pagination.active {
            background-color:#181a1d;
	        color:#ccc;
        }
    `],
    template: `
        <div class="row" *ngIf="totalPages > 1">
            <div class="col-md-6">
                <div class="dataTables_info" role="status" aria-live="polite">
                    <span>
                        {{'Pagination.Showing' | translate:lang}}&nbsp;<span>{{((currentPage - 1) * pageSize) + 1}}</span>&nbsp;{{'Pagination.To' | translate:lang}}&nbsp;
                        <span>{{((currentPage -1) * pageSize) + pageSize}}</span>&nbsp;{{'Pagination.Off' | translate:lang}}&nbsp;
                        <span>{{totalCount}}</span>&nbsp;{{'Pagination.Entries' | translate:lang}}
                    </span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="dataTables_paginate pull-right" id="table2_paginate">
                    <div class="btn-group">
<!--
                        <button class="btn btn-xs btn-primary btn-pagination" style="padding-right: 1px;" type="button"
                                [disabled]="currentSlider === 0"
                                (click)="prevSlider()">
                            <i class="fa fa-angle-double-left"></i>
                        </button>
-->
                        <button class="btn btn-xs btn-primary btn-pagination" style="padding-right: 3px;" type="button"
                                [disabled]="currentPage <= 1"
                                (click)="prevPage()">
                            <i class="fa fa-angle-left"></i>
                        </button>
                        <button *ngFor="let page of pages" class="btn btn-xs btn-primary btn-pagination"
                                [class.active]="currentPage === page"
                                (click)="moveToPage(page)">{{page}}</button>

                        <button class="btn btn-xs btn-primary btn-pagination" style="padding-right: 3px;" type="button"
                                [disabled]="currentPage >= totalPages"
                                (click)="nextPage()">
                            <i class="fa fa-angle-right"></i>
                        </button>
<!--
                        <button class="btn btn-xs btn-primary btn-pagination" style="padding-right: 1px;" type="button"
                                [disabled]="currentSlider >= sliderlastBound - 1"
                                (click)="nextSlider()">
                            <i class="fa fa-angle-double-right"></i>
                        </button>
-->
                    </div>
                </div>
            </div>
        </div>
        <div class="row" *ngIf="totalPages <= 1">
            <div class="col-md-6">
                <div class="dataTables_info" role="status" aria-live="polite">
                    <span>
                        {{'Pagination.Showing' | translate:lang}}&nbsp;<span>{{((currentPage - 1) * pageSize) + 1}}</span>&nbsp;{{'Pagination.To' | translate:lang}}&nbsp;
                        <span>{{((currentPage -1) * pageSize) + pageSize}}</span>&nbsp;{{'Pagination.Off' | translate:lang}}&nbsp;
                        <span>{{totalCount}}</span>&nbsp;{{'Pagination.Entries' | translate:lang}}
                    </span>
                </div>
            </div>
        </div>`
})

export class ServerPaginationComponent implements OnInit, OnChanges {
    @Input('currentPage') currentPage: number;
    @Input('pageSize') pageSize: number;
    @Input('totalCount') totalCount: number;
    @Input('totalPages') totalPages: number;
    @Input('sliderSize') sliderSize: number;

    currentSlider : number = 0;
    sliderLowerBound : number;
    sliderUpperBound : number;
    sliderlastBound : number;
    pages : number[] = [];

    @Output() onPageChange: EventEmitter<number> = new EventEmitter<number>();

    constructor(public locale: LocaleService,
        public translation: TranslationService) {
    }

    ngOnInit() {
        this.sliderSize = this.sliderSize || 3;
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes && changes.currentPage && changes.currentPage.currentValue === 1) {
            if (this.currentSlider > 0) {
                this.currentSlider = 0;
            }
        }

        this.pages = [];
        this.sliderUpperBound = this.pageSize;

        let lowerBound = this.currentSlider * this.sliderSize;
        let upperBound = (this.currentSlider * this.sliderSize) + this.sliderSize;
        if( upperBound > this.totalPages ) {
            upperBound = this.totalPages;
        }
        this.sliderLowerBound = lowerBound;
        this.sliderUpperBound = upperBound;
        this.sliderlastBound = (Math.ceil(this.totalPages / this.sliderSize));

        for (let i = lowerBound ; i < upperBound ; i++) {
            this.pages.push(i + 1);
        }
    }

    prevSlider() {
        if (this.currentSlider > 0) {
            this.onPageChange.emit(this.currentSlider * this.sliderSize);
            this.currentSlider = this.currentSlider - 1;
        }
    }

    nextSlider() {
        if (this.currentSlider < (this.totalPages / this.sliderSize)) {
            this.currentSlider = this.currentSlider + 1;
            this.onPageChange.emit((this.currentSlider * this.sliderSize) + 1);
        }
    }

    prevPage() {
        if (this.currentPage === this.sliderLowerBound + 1 ) {
            this.prevSlider();
        }
        else if (this.currentPage > 1) {
            this.onPageChange.emit(this.currentPage - 1);
        }
    }

    nextPage() {
        if (this.currentPage === this.sliderUpperBound) {
            this.nextSlider();
        }
        else if (this.currentPage < this.totalPages) {
            this.onPageChange.emit(this.currentPage + 1);
        }
    }

    moveToPage(page : number){
        if( page >= 1 && page <= this.totalPages && page !== this.currentPage ) {
            this.onPageChange.emit(page);
        }
    }
}
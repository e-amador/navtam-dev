﻿namespace RSC.Model.Enums
{
    /// <summary>
    /// Distance Unit of Measure: either ft or ins.
    /// </summary>
    public enum UnitOfMeasurement : int
    {
        Inches = 9, // INCHES (INS) / POUCES (PO)
        Feet = 10, // FEET (FT) / PIEDS (PI)
    }
}
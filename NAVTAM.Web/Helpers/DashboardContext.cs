﻿using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Data.NotamWiz.EF.Helpers;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.NsdEngine.Helpers;

namespace NAVTAM.Helpers
{
    public class DashboardContext : IDashboardContext
    {
        public DashboardContext(IRepositoryProvider repositoryProvider, IUow uow, INsdManagerResolver managerResolver, INotamQueuePublisher queuePublisher, IAeroRdsProxy aeroRdsProxy, ILogger logger)
        {
            Uow = uow;
            _repositoryProvider = repositoryProvider;
            _managerResolver = managerResolver;
            _queuePublisher = queuePublisher;
            _aeroRdsProxy = aeroRdsProxy;
            _logger = logger;
        }

        public IUow Uow { get; private set; }

        public IUow GetTempUow()
        {
            return new Uow(_repositoryProvider);
        }

        public INsdManagerResolver GetNsdManagerResolver()
        {
            return _managerResolver;
        }

        public INotamQueuePublisher GetQueuePublisher()
        {
            return _queuePublisher;
        }

        public IAeroRdsProxy GetAeroRdsProxy()
        {
            return _aeroRdsProxy;
        }

        readonly IRepositoryProvider _repositoryProvider;
        readonly INsdManagerResolver _managerResolver;
        readonly INotamQueuePublisher _queuePublisher;
        readonly IAeroRdsProxy _aeroRdsProxy;
        readonly ILogger _logger;
    }
}
﻿namespace NavCanada.Core.Proxies.AimslProxy.Mappers.Converters
{
    using AutoMapper;

    using Domain.Model.Enums;
    using AimslService;

    class NotamProposalStatusType2NotamProposalStatusCode : ITypeConverter<SingleNotamProposalStatusResponseType, NotamProposalStatusCode>
    {
        public NotamProposalStatusCode Convert(ResolutionContext context)
        {
            SingleNotamProposalStatusResponseType source = (SingleNotamProposalStatusResponseType)context.SourceValue;

            NotamProposalStatusCode returendEnum;
            if (System.Enum.TryParse(source.Status, out returendEnum)) 
                return returendEnum;

            switch (source.Status)
            {
                case "Distributed":
                    return NotamProposalStatusCode.Submitted;
                case "Queued":
                    return NotamProposalStatusCode.Queued;
                case "InProgress":
                    return NotamProposalStatusCode.Pending;
                default:
                    return NotamProposalStatusCode.Undefined;
            }

        }

    }
}

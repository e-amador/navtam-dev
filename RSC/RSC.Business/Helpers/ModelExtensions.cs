﻿using RSC.Model;
using RSC.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RSC.Business.Helpers
{
    static class ModelExtensions
    {
        const string UOM_INCHES = "IN";
        const string UOM_FEET = "FT";

        public static bool IsGRF(this Runway rwy)
        {
            return rwy.GetRunwayThirds(false).Length == 3;
        }

        public static string[] GetRunwayDirectionDesignators(this Runway rwy)
        {
            var start = rwy.RunwayId.IndexOf('-') + 1;
            return rwy.RunwayId.Substring(start).Split('/');
        }

        public static ClearedSurfaceThird[] GetRunwayThirds(this Runway rwy, bool reverse)
        {
            var thirds = rwy.ClearedPortion?.RunwayThirds;
            if (thirds == null)
                return new List<ClearedSurfaceThird>().ToArray();

            return reverse ? thirds.Reverse().ToArray() : thirds;
        }

        public static string GetRunwayCCs(this Runway rwy, bool reverse)
        {
            return string.Join("/", rwy.GetRunwayThirds(reverse).Select(t => t.RwyCC));
        }

        public static bool IsClearedByPortions(this Runway rwy) => rwy.ClearedPortionOffsets?.Length == 2;

        public static string ToStringWithUOM(this ContamDepth depth) => InchesToString(depth.ContamInches, UOM_INCHES);

        public static string Translate(this ContaminantWithDepthEnum value, Language lang)
        {
            switch (value)
            {
                case ContaminantWithDepthEnum.StandingWater:
                    return lang == Language.English ? "STANDING WATER" : "EAU STAGNANTE";
                case ContaminantWithDepthEnum.DrySnow:
                    return lang == Language.English ? "DRY SNOW" : "NEIGE SECHE";
                case ContaminantWithDepthEnum.WetSnow:
                    return lang == Language.English ? "WET SNOW" : "NEIGE MOUILLEE";
                case ContaminantWithDepthEnum.Slush:
                    return lang == Language.English ? "SLUSH" : "NEIGE FONDANTE";
                case ContaminantWithDepthEnum.DrySnowOnTopOfIce:
                    return lang == Language.English ? "DRY SNOW ON TOP OF ICE" : "NEIGE SECHE SUR GLACE";
                case ContaminantWithDepthEnum.WetSnowOnTopOfIce:
                    return lang == Language.English ? "WET SNOW ON TOP OF ICE" : "NEIGE MOUILLEE SUR GLACE";
                case ContaminantWithDepthEnum.SlushOnTopOfIce:
                    return lang == Language.English ? "SLUSH ON TOP OF ICE" : "NEIGE FONDANTE SUR GLACE";
                case ContaminantWithDepthEnum.DrySnowOnTopOfCompactedSnow:
                    return lang == Language.English ? "DRY SNOW ON TOP OF COMPACTED SNOW" : "NEIGE SECHE SUR NEIGE COMPACTEE";
                case ContaminantWithDepthEnum.WaterOnTopOfCompactedSnow:
                    return lang == Language.English ? "WATER ON TOP OF COMPACTED SNOW" : "EAU SUR NEIGE COMPACTEE";
                case ContaminantWithDepthEnum.WetSnowOnTopOfCompactedSnow:
                    return lang == Language.English ? "WET SNOW ON TOP OF COMPACTED SNOW" : "NEIGE MOUILLEE SUR NEIGE COMPACTEE";
                default:
                    return value.ToString().ToUpper();
            }
        }

        public static string Translate(this ContaminantWithNoDepthEnum value, Language lang)
        {
            switch (value)
            {
                case ContaminantWithNoDepthEnum.Dry:
                    return lang == Language.English ? "DRY" : "NUE ET SECHE";
                case ContaminantWithNoDepthEnum.Wet:
                    return lang == Language.English ? "WET" : "NUE ET MOUILLEE";
                case ContaminantWithNoDepthEnum.Frost:
                    return lang == Language.English ? "FROST" : "GIVRE";
                case ContaminantWithNoDepthEnum.Ice:
                    return lang == Language.English ? "ICE" : "GLACE";
                case ContaminantWithNoDepthEnum.CompactedSnow:
                    return lang == Language.English ? "COMPACTED SNOW" : "NEIGE COMPACTEE";
                case ContaminantWithNoDepthEnum.WetIce:
                    return lang == Language.English ? "WET ICE" : "GLACE MOUILLEE";
                case ContaminantWithNoDepthEnum.CompactedSnowGravelMix:
                    return lang == Language.English ? "COMPACTED SNOW/GRAVEL MIX" : "MELANGE SN COMPACTEE GRVL";
                case ContaminantWithNoDepthEnum.SlipperyWhenWet:
                    return lang == Language.English ? "SLIPPERY (WHEN) WET" : "GLISSANTE (LORSQUE) MOUILLEE";
                default:
                    return value.ToString().ToUpper(); // this should never happen
            }
        }

        public static string Translate(this OffsetSideEnum value, Language lang)
        {
            switch (value)
            {
                case OffsetSideEnum.East:
                    return lang == Language.English ? "EAST" : "EST";
                case OffsetSideEnum.West:
                    return lang == Language.English ? "WEST" : "OUEST";
                case OffsetSideEnum.North:
                    return lang == Language.English ? "NORTH" : "NORD";
                case OffsetSideEnum.South:
                    return lang == Language.English ? "SOUTH" : "SUD";
                default:
                    return value.ToString().ToUpper();
            }
        }

        public static string ToString(this ContaminantFtIns value, Language lang)
        {
            if (value.HasDepth)
                return $"{value.ContamDepthFI.ToStringWithUOM()} {value.ContamType.Value.Translate(lang)}";
            else
                return $"{value.ContamTypeNoDepth.Value.Translate(lang)}";
        }

        public static string ToStringWithUOM(this ContamDepthFtIn value)
        {
            if (value.ContamTrace)
                return "TRACES"; // >> tbd!

            if (value.ContamInches != 0)
                return InchesToString(value.ContamInches, UOM_INCHES);

            return $"{value.ContamFeet}{UOM_FEET}";
        }

        public static string ToStringWithUOM(this BankHeight value)
        {
            if (value.BankHeightFt > 0)
                return $"{value.BankHeightFt}{UOM_FEET}";

            if (value.BankHeightInch > 0)
                return $"{value.BankHeightInch}{UOM_INCHES}";

            return string.Empty;
        }

        public static string ToString(this BankEdge value, Language lang)
        {
            var outside = lang == Language.English ? "OUTSIDE" : "EXTERIEUR";
            if (value.BankEdgeFt > 0)
                return $"{value.BankEdgeFt}{UOM_FEET} {outside}";

            if (value.BankEdgeIns > 0)
                return $"{value.BankEdgeIns}{UOM_INCHES} {outside}";

            if (value.OnRwyEdge)
                return lang == Language.English ? "ON" : "SUR";

            return ""; // >> should never happen
        }

        public static string Translate(this CardinalSideEnum value, Language lang)
        {
            switch (value)
            {
                case CardinalSideEnum.E:
                    return lang == Language.English ? "EAST" : "EST";
                case CardinalSideEnum.N:
                    return lang == Language.English ? "NORTH" : "NORD";
                case CardinalSideEnum.W:
                    return lang == Language.English ? "WEST" : "OUEST";
                case CardinalSideEnum.S:
                    return lang == Language.English ? "SOUTH" : "SUD";
                case CardinalSideEnum.SE:
                    return lang == Language.English ? "SOUTH-EAST" : "SUD-EST";
                case CardinalSideEnum.NE:
                    return lang == Language.English ? "NORTH-EAST" : "NORD-EST";
                case CardinalSideEnum.SW:
                    return lang == Language.English ? "SOUTH-WEST" : "SUD-OUEST";
                case CardinalSideEnum.NW:
                    return lang == Language.English ? "NORTH-WEST" : "NORD-OUEST";
                default:
                    return value.ToString().ToUpper(); // >> this should never happen
            }
        }

        public static string Translate(this TreatmentEnum value, Language lang)
        {
            switch (value)
            {
                case TreatmentEnum.ChemicallyTreated:
                    return lang == Language.English ? "CHEMICALLY TREATED" : "TRAITEMENT CHIMIQUE";
                case TreatmentEnum.Graded:
                    return lang == Language.English ? "GRADED" : "NIVELEE";
                case TreatmentEnum.LoseSand:
                    return lang == Language.English ? "LOOSE SAND" : "SABLE MEUBLE";
                case TreatmentEnum.Packed:
                    return lang == Language.English ? "PACKED" : "DAMEE";
                case TreatmentEnum.Scarified:
                    return lang == Language.English ? "SCARIFIED" : "SCARIFIEE";
                default:
                    return value.ToString().ToUpper(); // >> this should never happen
            }
        }

        static string InchesToString(decimal value, string uom)
        {
            if (value == 0.13m)
                return $"1/8{uom}";
            if (value == 0.25m)
                return $"1/4{uom}";
            if (value == 0.5m)
                return $"1/2{uom}";
            if (value == 0.75m)
                return $"3/4{uom}";
            if (value == 1.5m)
                return $"1 1/2{uom}";
            return $"{value}{uom}";
        }
    }
}
﻿using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.Areas.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Validation;
    using System.Diagnostics;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    using AutoMapper;

    using Kendo.Mvc.Extensions;
    using Kendo.Mvc.UI;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    using NavCanada.Applications.NotamWiz.Web.Code.Extentions;
    using NavCanada.Applications.NotamWiz.Web.Controllers;
    using NavCanada.Applications.NotamWiz.Web.Models;
    using NavCanada.Core.Common.Common;
    using NavCanada.Core.Common.Contracts;
    using NavCanada.Core.Data.NotamWiz.Contracts;
    using NavCanada.Core.Domain.Model.Entitities;

    using Westwind.Globalization;
    using NavCanada.Core.SqlServiceBroker;
    using NavCanada.Core.Domain.Model.Enums;

    [Authorize(Roles = "Administrator, Organization Administrator, CCS")]
    public class UserManagementController : BaseController
    {
        readonly ILog _log = LogManager.GetLogger(typeof(UserManagementController));

        public UserManagementController(IUow uow, IClusterQueues clusterQueues) 
            : base(uow, clusterQueues)
        {
        }

        // GET: Admin/UserManagement
        public async Task<ActionResult> Index()
        {
            var userId = HttpContext.User.Identity.GetUserId();
            await SetViewBagRolesForCssAndAdminAsync(userId);

            if (await IsUserInRoleAsync(userId, CommonDefinitions.OrgAdminRole))
            {
                var user = await UserManager.FindByIdAsync(userId);
                if (user != null && user.OrganizationId.HasValue)
                {
                    var organization = await Uow.OrganizationRepo.GetByIdAsync(user.OrganizationId.Value, true);
                    if (organization != null)
                    {
                        ViewBag.OrgsList = new[] { new { value = organization.Id, text = organization.Name } }.ToList();

                        
                        var roleIds = new List<string>();
                        foreach (var usr in organization.Users)
                        {
                            
                            foreach (IdentityUserRole userRole in usr.Roles)
                                roleIds.Add(userRole.RoleId);
                        }

                        if (roleIds.Any())
                        {
                            var appRoles = await Uow.ApplicationRoleRepo.GetByRoleIdsAsync(roleIds);
                            ViewBag.RolesList = appRoles
                                .Where(r => r.Name != CommonDefinitions.CcsUser)
                                .Select(r => new
                                {
                                    value = r.Id,
                                    text = r.Name
                                })
                                .ToList();
                        }
                    }
                }
            }
            else
            {
                var rolesLst = await Uow.ApplicationRoleRepo.GetAllAsync();
                var orgs = await Uow.OrganizationRepo.GetAllAsync();

                ViewBag.OrgsList = orgs
                    .Select(r => new
                    {
                        value = r.Id,
                        text = r.Name
                    })
                    .ToList();

                ViewBag.RolesList = rolesLst
                    .Select(r => new
                    {
                        value = r.Id,
                        text = r.Name
                    })
                    .ToList();
            }
            return View();
        }

        //---------------- READ USERS ( POPULATE GRID )
        /// <summary>
        /// Populates table and reads it into the Grid.
        /// </summary>
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> ReadUsers([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")] IEnumerable<UserManagementViewModel> model)
        {
            this._log.Object(new { Method = "ReadUsers", Parameters = new { request = request, userManagement = model } });

            ICollection<UserProfile> userQ = new List<UserProfile>();
            var userId = HttpContext.User.Identity.GetUserId();

            if (await IsUserInRoleAsync(userId, CommonDefinitions.OrgAdminRole))
            {
                var organization = await Uow.OrganizationRepo.GetByUserIdAsync(userId, shouldIncludeUsers: true);
                if (organization != null)
                {
                    foreach (var usr in organization.Users)
                    {
                        if (!await IsUserInRoleAsync(usr.Id, CommonDefinitions.CcsUser))
                            userQ.Add(usr);
                    }

                }
            }
            else
                userQ = await Uow.UserProfileRepo.GetNonDeletedAndFullyRegisteredUsersAsync();


            var userList = userQ.ToList();

            var result = Mapper.Map<List<UserProfile>, List<UserManagementViewModel>>(userList);

            return (Json(result.ToDataSourceResult(request)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public async Task<JsonResult> IsUserRegistered(string username)
        {
            this._log.Object(new { Method = "ReadUsers", Parameters = new { userName = username } });

            var userExist = await UserManager.FindByNameAsync(username);
            return Json(userExist != null, JsonRequestBehavior.AllowGet);
        }


        //-----------   USER EDIT POPUP CONTROLLER
        /// <summary>
        /// Handles Edit GUI window
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"> the VM used to send data</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<JsonResult> UpdateUser([DataSourceRequest] DataSourceRequest request, UserManagementViewModel model)
        {
            this._log.Object(new { Method = "UpdateUser", Parameters = new { request, userManagement = model } });

            var updatedUser = new UserProfile();
            if (ModelState.IsValid)
            {
                try
                {
                    var userProfile = await UserManager.FindByIdAsync(model.UserId);
                    if (userProfile != null )//&& userProfile.RegistrationId.HasValue)
                    {
                        updatedUser = Mapper.Map(model, userProfile);

                        // update users roles
                        var roles = updatedUser.Roles.Select(c => c.RoleId).ToList();
                        string[] userRoles = Uow.ApplicationRoleRepo.GetAll().ToList().Where(a => roles.Contains(a.Id)).Select(r => r.Name).ToArray();
                        UserManager.RemoveFromRoles(updatedUser.Id, userRoles);

                        foreach (string roleId in model.UserRoles)
                        {
                            Core.Domain.Model.Entitities.ApplicationRole role = Uow.ApplicationRoleRepo.GetById(roleId);
                            if (role != null)
                                UserManager.AddToRole(updatedUser.Id, role.Name);
                        }
                        UserManager.SetLockoutEnabled(updatedUser.Id, updatedUser.LockoutEnabled);
                        await UserManager.UpdateAsync(updatedUser);
                    }
                }

                catch (Exception err)
                {
                    ViewBag.errMessage = DbRes.T("ErrorInteralCode:", "UserManagementRes") + err.Message;
                }

            }
            return Json(new[] { Mapper.Map(updatedUser, model) }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllowAnonymous]
        public async Task<JsonResult> ResetPassword(ResetPasswordModel model)
        {
            this._log.Object(new {  Method = "ResetPassword", Parameters = new { model } });

            if (ModelState.IsValid)
            {
                if (model.Password != model.ConfirmPassword)
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return Json("Password and Confirm Password don't match");
                }

                var userProfile = await UserManager.FindByIdAsync(model.UserId);
                if (userProfile == null)
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return Json("UserId is not in the database");
                }

                string code = await UserManager.GeneratePasswordResetTokenAsync(userProfile.Id);
                UserManager.ResetPassword(model.UserId, code, model.Password);

                Response.StatusCode = (int)HttpStatusCode.OK;
                return Json("password was updated");
            }

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json("The request is not valid");
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<JsonResult> CreateUser([DataSourceRequest] DataSourceRequest request, UserManagementViewModel model)
        {
            this._log.Object(new { Method = "CreateUser", Parameters = new { request = request, userManagement = model } });

            var userProfile = Mapper.Map<UserManagementViewModel, UserProfile>(model);
            if (!ModelState.IsValid)
            {
                /*
                Json(
                    new[] {Mapper.Map<UserProfile, UserManagementViewModel>(userProfile)}.ToDataSourceResult(
                        request, ModelState));
                */
                var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(errors); 
            }
            Uow.BeginTransaction();
            try
            {
                userProfile = Mapper.Map<UserManagementViewModel, UserProfile>(model);
                userProfile.Id = Guid.NewGuid().ToString();
                userProfile.OrganizationId = (model.UserOrgId);
                userProfile.EmailConfirmed = true;
                userProfile.LastPasswordChangeDate = DateTime.UtcNow;
                var result = await UserManager.CreateAsync(userProfile, model.Password);
                if (result.Succeeded)
                {
                    await UserManager.SetLockoutEnabledAsync(userProfile.Id, model.IsDisabled);

                    var roleIds = model.UserRoles;
                    if (roleIds.Any())
                    {
                        var appRoles = await Uow.ApplicationRoleRepo.GetByRoleIdsAsync(roleIds);
                        foreach (var appRole in appRoles)
                            UserManager.AddToRole(userProfile.Id, appRole.Name);
                    }
                    //Publish Mto to target Queeue
                    var mto = CreateMtoForUserRegistration(MessageTransferObjectId.RegistrationNotificationEmail, userProfile.Id, 0, RegistrationStatus.Completed, null);
                    PublishMtoToTargetQueue(mto, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings);
                    Uow.Commit();
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    var errors = result.Errors.ToList();
                    Uow.Rollback();
                    return Json(errors); 
                }
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        _log.Error(new
                        {
                            Method = "CreateUser",
                            EntityValidationError = string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage)
                        });
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                //ViewBag.errMessage = DbRes.T("Invalid user data", "UserManagementRes");

                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                var errors = new List<string> {DbRes.T("Invalid user data", "UserManagementRes")};
                Uow.Rollback();
                return Json(errors); 
            }
            catch (Exception err)
            {
/*
                ViewBag.errMessage = DbRes.T("ErrorInteralCode:", "UserManagementRes") + err.Message;
*/
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                var errors = new List<string> { DbRes.T("ErrorInteralCode:", "UserManagementRes") + err.Message };
                Uow.Rollback();
                return Json(errors); 
            }


            return Json(new[] { Mapper.Map<UserProfile, UserManagementViewModel>(userProfile) }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<JsonResult> DeleteUser([DataSourceRequest] DataSourceRequest request, string userId)
        {

            this._log.Object(new { Method = "Delete", Parameters = new { request = request, UserId = userId } });

            var userProfile = await UserManager.FindByIdAsync(userId);

            if (userProfile == null)
            {
                ViewBag.errMessage = DbRes.T("User Not Found", "UserManagementRes");
                return null;
            }

            if (userProfile.UserName == "Admin")
            {
                ViewBag.errMessage = DbRes.T("Can not delete Admin", "UserManagementRes");
                return null;
            }

            DateTime deleationTime = DateTime.UtcNow;
            userProfile.UserName = userProfile.UserName + "<DELETED on: " + deleationTime.ToShortDateString() + " " + deleationTime.ToShortTimeString() + " by:" + User.Identity.Name + ">";
            userProfile.IsDeleted = true;

            await UserManager.UpdateAsync(userProfile);

            return Json(new[] { Mapper.Map<UserProfile, UserManagementViewModel>(userProfile) }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [AllowAnonymous]
        public async Task<JsonResult> IsSamePassword(string username, string password)
        {
            var user = await UserManager.FindByNameAsync(username);

            if (user == null)
                return Json(false);

            var result = UserManager.PasswordHasher.VerifyHashedPassword(user.PasswordHash, password);

            return Json(result != PasswordVerificationResult.Success);
        }
    }
}


﻿namespace RSC.Model
{
    public class RSCText
    {
        public string EnglishRSC { get; set; }
        public string FrenchRSC { get; set; }
    }
}

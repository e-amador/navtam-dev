﻿import { Component, Input, OnChanges, ViewChild, Inject } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { WindowRef } from '../../common/windowRef.service';
//import { NsdFormComponent } from '../nsds/nsd-form.component';
import { NsdGeoRefToolComponent } from '../nsds/nsd-georeftool.component'
import { TranslationService } from 'angular-l10n';
import {
    ProposalType
} from '../shared/data.model';

import { DataService } from '../shared/data.service';
import { TOASTR_TOKEN, Toastr } from '../../common/toastr.service';
//import { forEach } from '@angular/router/src/utils/collection';
//import { RegExp } from 'core-js';
//import { LocationService } from '../shared';

//declare var $: any;

@Component({
    selector: 'nsd-buttonbar',
    templateUrl: '/app/dashboard/nsds/nsd-buttonbar.component.html'
})
export class NsdButtonBarComponent implements OnChanges {
    @Input() model: any;
    @Input('isNOF') isNOF: boolean = false;
    @Input('isReadOnly') isReadOnly: boolean = false;

    @ViewChild(NsdGeoRefToolComponent) geoRefTool: NsdGeoRefToolComponent;

    parent: any = null;
    canDoCatchAll: boolean = false;

    isActionExecuted: boolean = false;
    isCancellation: boolean = false;
    canReject: boolean = true;
    canPark: boolean = true;
    actualOrgId: number = 0;
    source: string = '';
    //isDirectlyDisseminable: boolean = false;

    //points: IPoint[] = [];
    //point: IPoint;

    constructor(private windowRef: WindowRef,
        private activatedRoute: ActivatedRoute,
        private dataService: DataService,
        public translation: TranslationService,
        @Inject(TOASTR_TOKEN) private toastr: Toastr) {

        this.canDoCatchAll = this.windowRef.appConfig.canDoCatchAll;
        this.actualOrgId = this.windowRef.appConfig.orgId;
        this.source = this.activatedRoute.snapshot.params["source"];
    }

    ngOnInit() {
        let notamId = this.model.notamId;
        if (notamId) {
            this.isCancellation = notamId[0] === "C";
        }

        this.canReject = this.model.canBeRejected;
        //this.canReject = true;

        if (this.source && this.canReject) {
            if (this.source === 'notam') {
                this.canReject = false;
            }
        }
        if (this.isNOF) {
            if (this.model.proposalType === ProposalType.Replace || this.isCancellation) this.canPark = false;
        }

        if (this.model.categoryId === 18) {
            //this.isDirectlyDisseminable = true;
            this.canReject = false;
        }


    }

    ngOnChanges() {
    }

    backToProposals() {
        if (this.parent) {
            if (this.isNOF) {
                this.parent.backToNofProposals();
            } else {
                this.parent.backToProposals();
            }
        }
    }

    backToReviewText() {
        if (this.parent) {
            return this.parent.txtBackToReviewList;
        }
        return "";
    }

    submitButtonText(): string {
        if (this.model.categoryId === 18)
            return this.translation.translate('ActionButtons.DisseminateProposal');
        return this.translation.translate('ActionButtons.SubmitProposal');
    }

    enableButtonActions() {
        this.isActionExecuted = false;
    }

    discardProposal() {
        let self = this;
        if (this.parent && !this.isActionExecuted) {
            this.isActionExecuted = true;
            this.parent.discardProposal(error => self.enableButtonActions());
        }
    }

    parkProposal() {
        let self = this;
        if (this.parent && !this.isActionExecuted) {
            this.isActionExecuted = true;
            this.parent.parkProposal(error => self.enableButtonActions());
        }
    }

    saveDraft() {
        if (this.parent) {
            this.parent.saveDraft();
        }
    }

    submitProposal() {
        let self = this;
        if (this.parent && !this.isActionExecuted) {
            this.isActionExecuted = true;
            this.parent.submitProposal(error => self.enableButtonActions());
        }
    }

    disseminateProposal() {
        let self = this;
        if (this.parent && !this.isActionExecuted) {
            this.isActionExecuted = true;
            this.parent.disseminateProposal(error => self.enableButtonActions());
        }
    }

    createProposalGroup() {
        if (this.parent) {
            this.parent.showGrouping();
        }
    }

    isCreateGroupButtonDisabled() {
        return this.parent && this.parent.isGroupingProposalDisable();
    }

    isDisseminateButtonDisabled() {
        return this.parent && this.parent.isSubmitProposalButtonDisabled(); //!this.parent.nsdForm.valid &&
    }

    //User buttons
    isSaveButtonDisabled() {
        return this.parent && this.parent.isSaveDraftButtonDisabled();
    }

    isSubmitButtonDisabled() {
        return this.parent && this.parent.isSubmitProposalButtonDisabled();
    }

    // NOF Buttons
    isParkProposalButtonDisabled() {
        return this.parent && this.parent.isParkProposalDisabled();
    }

    openRejectProposalDialog() {
        if (this.parent) {
            this.parent.openRejectProposalDialog();
        }
    }

    isRejectProposalDisable() {
        return this.parent && this.parent.isRejectProposalDisable();
    }

    showActionButtons() {
        if (this.isReadOnly) return false;

        if (this.model.categoryId === 2) {
            return this.canDoCatchAll;
        }

        return true;
    }

}
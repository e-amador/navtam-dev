﻿import { Component, Inject, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms'
import { WindowRef } from '../../common/windowRef.service'
import { Select2OptionData } from 'ng2-select2';
import { LocaleService, TranslationService } from 'angular-l10n';
import { MemoryStorageService } from '../shared/mem-storage.service'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';
import { DataService } from '../shared/data.service'
import { ISeriesPartial, IGeoRegionPartial, ISelectOptions } from '../shared/data.model';

@Component({
    templateUrl: '/app/admin/series/series-new.component.html'
})
export class SeriesNewComponent implements OnInit {

    model: any = null;
    action: string;

    public seriesForm: FormGroup;

    subjects: ISelectOptions[] = [];
    selectedSubject?: number;
    subjectOptions: Select2Options;

    geoRegions: ISelectOptions[] = [];
    selectedRegionId?: number;
    geoRegionOptions: Select2Options;

    disseminationCategories: ISelectOptions[] = [];
    selectedDisseminationCategory?: string = '-1';
    disseminationCategoriesOptions: Select2Options;

    codes: Select2OptionData[] = [];
    codeOptions: Select2Options;
    selectedCodeId?: string = '-1';

    isReadOnly: boolean = false;
    readOnlyQCode: boolean = false;
    isSubmitting: boolean = false;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private activatedRoute: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService) {

        if (this.winRef.appConfig.ccs)
            this.router.navigate(['/administration']);
    }

    ngOnInit() {
        this.model = this.prepareEmptySerie();
        this.loadSubjects();
        this.loadRegions();
        this.loadDissCategory();
        this.loadQCodes();

        this.subjectOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('SeriesAllocationSession.SelectRegion')
            },
            width: "100%"
        }

        this.geoRegionOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('SeriesAllocationSession.SelectRegion')
            },
            width: "100%"
        }
        this.disseminationCategoriesOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('SeriesAllocationSession.SelectDissCat')
            },
            width: "100%"
        }
        this.codeOptions = {
            placeholder: {
                id: "-1",
                text: this.translation.translate('SeriesAllocationSession.SelectQCode')
            },
            width: "100%"
        }

        this.configureReactiveForm();
    }

    backToSeriesList() {
        this.router.navigate(['/administration/series']);
    }

    onSubmit() {
        if (this.isSubmitting) return;
        this.isSubmitting = true;
        var serieToSave = this.prepareSaveSerie();
        this.dataService.saveSeries(serieToSave)
            .subscribe(data => {
                let msg = this.translation.translate('SeriesAllocationSession.SuccessSerieCreated');
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                this.router.navigate(["/administration/series"]);
            }, error => {
                this.isSubmitting = false;
                let msg = this.translation.translate('SeriesAllocationSession.FailureSerieCreated');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });

    }

    private configureReactiveForm() {
        this.seriesForm = this.fb.group({
            subject: [{ value: this.model.subject, disabled: false }, [validateSubject]],
            regionId: [{ value: this.model.regionId, disabled: false }, [validateRegionId]],
            series: [{ value: this.model.series, disabled: false }, [validateSeries]],
            disseminationCategory: [{ value: this.model.disseminationCategory, disabled: false }, [validateDisseminationCategory]],
            qCode: [{ value: this.model.qCode, disabled: this.readOnlyQCode }, [validateQCode]],
            disseminationCategoryName: [{ value: this.model.disseminationCategoryName, disabled: false }, [Validators.required]],
            regionName: [{ value: this.model.regionName, disabled: false }, [Validators.required]],
        });
    }

    private prepareSaveSerie(): ISeriesPartial {
        const serie: ISeriesPartial = {
            subject: this.seriesForm.get('subject').value.toString(),
            regionName: this.seriesForm.get('regionName').value.toUpperCase(),
            regionId: this.seriesForm.get('regionId').value.toUpperCase(),
            series: this.seriesForm.get('series').value.toUpperCase(),
            disseminationCategory: this.seriesForm.get('disseminationCategory').value.toUpperCase(),
            disseminationCategoryName: this.seriesForm.get('disseminationCategoryName').value.toUpperCase(),
            qCode: this.seriesForm.get('qCode').value.toUpperCase(),
        }
        return serie;
    }


    private prepareEmptySerie(): ISeriesPartial {
        const serie: ISeriesPartial = {
            id: null,
            subject: '0',
            regionName: '',
            regionId: '-1',
            series: '',
            disseminationCategory: '-1',
            disseminationCategoryName: '',
            qCode: '',
        }
        return serie;
    }

    public validateSeriesName() {
        const fc = this.seriesForm.get("series");
        if (validateSeries(fc) !== null) return false;
        return true;
    }

    public validateSubject() {
        const fc = this.seriesForm.get('subject');
        if (validateSubject(fc) !== null) return false;
        return true;
    }

    public validateRegion() {
        const fc = this.seriesForm.get('regionId');
        if (validateRegionId(fc) !== null) return false;
        return true;
    }

    public validateQCode() {
        const fc = this.seriesForm.get('qCode');
        if (validateQCode(fc) !== null) return false;
        return true;
    }

    public validateDissemination() {
        const fc = this.seriesForm.get('disseminationCategory');
        if (validateDisseminationCategory(fc) !== null) return false;
        return true;
    }

    public onDisseminationChanged(data: { value: string }) {
        if (data.value) {
            var diss = this.disseminationCategories.find(x => x.id === data.value);
            if (diss) {
                this.seriesForm.get('disseminationCategory').setValue(diss.id);
                this.seriesForm.get('disseminationCategoryName').setValue(diss.text);
            } else {
                this.seriesForm.get('disseminationCategory').setValue('-1');
                this.seriesForm.get('disseminationCategoryName').setValue('');
            }
        } else {
            this.seriesForm.get('disseminationCategory').setValue('-1');
            this.seriesForm.get('disseminationCategoryName').setValue('');
        }
    }

    public onSubjectChanged(data: { value: string }) {
        if (data.value) {
            this.seriesForm.get('subject').setValue(data.value);
            if (data.value === '2') {
                var code = this.codes.find(c => c.text === 'OB');
                if (code) {
                    this.onSelectedCodeChanged({ value: code.id })
                    this.readOnlyQCode = true;
                }
            } else {
                this.readOnlyQCode = false;
            }
        }
    }

    public onRegionChanged(data: { value: string }) {
        if (data.value) {
            var reg = this.geoRegions.find(x => x.id === data.value);
            if (reg) {
                this.seriesForm.get('regionId').setValue(reg.id);
                this.seriesForm.get('regionName').setValue(reg.text);
            } else {
                this.seriesForm.get('regionId').setValue('-1');
                this.seriesForm.get('regionName').setValue('');
            }
        } else {
            this.seriesForm.get('regionId').setValue('-1');
            this.seriesForm.get('regionName').setValue('');
        }
    }

    public onSelectedCodeChanged(data: { value: string }) {
        if (data.value !== "-1") {
            this.selectedCodeId = data.value;
            this.seriesForm.get('qCode').setValue(data.value);
            this.seriesForm.markAsDirty();
        } else {
            this.seriesForm.get('qCode').setValue("-1");
        }
    }

    private loadSubjects() {
        this.subjects = [
            {
                id: "0",
                text: "Ahp"
            },
            {
                id: "1",
                text: "FIR"
            },
            {
                id: "2",
                text: "FIR (MOB)"
            },
            {
                id: "3",
                text: "Ahp (RSC)"
            }
        ];
    }

    private loadRegions(): void {
        this.dataService.getAllGeoRegions()
            .subscribe((geoR: IGeoRegionPartial[]) => {
                this.geoRegions = geoR.map(
                    function (r: any) {
                        return <Select2OptionData>{
                            id: r.id.toString(),
                            text: r.name
                        };
                    });
                this.geoRegions.unshift({ id: "-1", text: ""});
                this.selectedRegionId = this.model.regionId || '-1';
            });

    }

    private loadDissCategory(): void {
        this.disseminationCategories = [
            { id: '-1', text: '' },
            { id: '0', text: 'National' },
            { id: '1', text: 'US' },
            { id: '2', text: 'International' },
        ];
        this.selectedDisseminationCategory = this.model.disseminationCategory || '-1';
    }

    private loadQCodes(): void {
        this.dataService.getAllIcaoSubjectCodes()
            .subscribe((codes: string[]) => {
                this.codes = codes.map(
                    function (r: any) {
                        return <Select2OptionData>{
                            id: r,
                            text: r
                        };
                    });

                this.codes.unshift({
                    id: "-1",
                    text: this.translation.translate('SeriesAllocationSession.SelectQCode'),
                });

                this.selectedCodeId = this.codes[0].id;
            });
    }

}

function validateSubject(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value === '-1') {
        return { 'required': true }
    }
    return null;
}

function validateRegionId(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value === '-1') {
        return { 'required': true }
    }
    return null;
}

function validateSeries(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value.length !== 1) {
        return { 'required': true }
    }
    if (!hasUpperCase(c.value)) {
        return { 'required': true }
    }
    return null;
}

function validateDisseminationCategory(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value === '-1') {
        return { 'required': true }
    }
    return null;
}

function validateQCode(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value === '-1') {
        return { 'required': true }
    }
    return null;
}

function hasUpperCase(str:string) {
    return (/[A-Za-z]/.test(str));
}

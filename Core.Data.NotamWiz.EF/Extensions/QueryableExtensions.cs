﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace NavCanada.Core.Data.NotamWiz.EF.Extensions
{
    public static class QueryableExtensions
    {
        public static IQueryable<T> ApplySort<T>(this IQueryable<T> source, string sort)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));

            if (sort == null)
                return source;

            var expression = source.Expression;
            int count = 0;

            var lastSort = sort.Split(',');

            //run through the sorting options and create a sort expression string from them
            foreach (var sortOption in lastSort)
            {
                var parameter = Expression.Parameter(typeof(T), "x");
                string method = "";
                MemberExpression selector = null;
                if (sortOption.StartsWith("_"))
                {
                    selector = Expression.PropertyOrField(parameter, sortOption.Remove(0, 1));
                    method = count == 0 ? "OrderByDescending" : "ThenByDescending";
                }
                else
                {
                    selector = Expression.PropertyOrField(parameter, sortOption);
                    method = count == 0 ? "OrderBy" : "ThenBy";
                }

                expression = Expression.Call(typeof(Queryable), method,
                    new Type[] { source.ElementType, selector.Type },
                    expression, Expression.Quote(Expression.Lambda(selector, parameter)));

                count++;
            }

            return count > 0 ? source.Provider.CreateQuery<T>(expression) : source;
        }
    }
}

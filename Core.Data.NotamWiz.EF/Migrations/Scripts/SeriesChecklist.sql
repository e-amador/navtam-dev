﻿IF (NOT Exists(SELECT TOP 1 Series FROM  [dbo].[SeriesChecklist]))
BEGIN
	INSERT INTO [dbo].[SeriesChecklist] ([Series],[Firs],[Coordinates],[Radius]) VALUES(N'C', N'CZVR CZEG',      N'665000N 1104500W', 999)
	INSERT INTO [dbo].[SeriesChecklist] ([Series],[Firs],[Coordinates],[Radius]) VALUES(N'D', N'CZWG CZYZ',      N'524500N 0904000W', 999)
	INSERT INTO [dbo].[SeriesChecklist] ([Series],[Firs],[Coordinates],[Radius]) VALUES(N'E', N'CZUL CZQM CZQX', N'524300N 0532100W', 999)
	INSERT INTO [dbo].[SeriesChecklist] ([Series],[Firs],[Coordinates],[Radius]) VALUES(N'F', N'CZVR CZEG',      N'665000N 1104500W', 999)
	INSERT INTO [dbo].[SeriesChecklist] ([Series],[Firs],[Coordinates],[Radius]) VALUES(N'G', N'CZWG CZYZ',      N'524500N 0904000W', 999)
	INSERT INTO [dbo].[SeriesChecklist] ([Series],[Firs],[Coordinates],[Radius]) VALUES(N'H', N'CZUL CZQM CZQX', N'524300N 0532100W', 999)
	INSERT INTO [dbo].[SeriesChecklist] ([Series],[Firs],[Coordinates],[Radius]) VALUES(N'I', N'CZVR CZEG',      N'665000N 1104500W', 999)
	INSERT INTO [dbo].[SeriesChecklist] ([Series],[Firs],[Coordinates],[Radius]) VALUES(N'J', N'CZWG CZYZ',      N'524500N 0904000W', 999)
	INSERT INTO [dbo].[SeriesChecklist] ([Series],[Firs],[Coordinates],[Radius]) VALUES(N'K', N'CZUL CZQM CZQX', N'524300N 0532100W', 999)
	INSERT INTO [dbo].[SeriesChecklist] ([Series],[Firs],[Coordinates],[Radius]) VALUES(N'L', N'CZVR CZEG',      N'665000N 1104500W', 999)
	INSERT INTO [dbo].[SeriesChecklist] ([Series],[Firs],[Coordinates],[Radius]) VALUES(N'M', N'CZWG CZYZ',      N'524500N 0904000W', 999)
	INSERT INTO [dbo].[SeriesChecklist] ([Series],[Firs],[Coordinates],[Radius]) VALUES(N'N', N'CZUL CZQM CZQX', N'524300N 0532100W', 999)
	INSERT INTO [dbo].[SeriesChecklist] ([Series],[Firs],[Coordinates],[Radius]) VALUES(N'O', N'CZVR CZEG',      N'665000N 1104500W', 999)
	INSERT INTO [dbo].[SeriesChecklist] ([Series],[Firs],[Coordinates],[Radius]) VALUES(N'P', N'CZWG CZYZ',      N'524500N 0904000W', 999)
	INSERT INTO [dbo].[SeriesChecklist] ([Series],[Firs],[Coordinates],[Radius]) VALUES(N'Q', N'CZUL CZQM CZQX', N'524300N 0532100W', 999)
	INSERT INTO [dbo].[SeriesChecklist] ([Series],[Firs],[Coordinates],[Radius]) VALUES(N'R', N'CZVR CZEG',      N'665000N 1104500W', 999)
	INSERT INTO [dbo].[SeriesChecklist] ([Series],[Firs],[Coordinates],[Radius]) VALUES(N'U', N'CZWG CZYZ',      N'524500N 0904000W', 999)
	INSERT INTO [dbo].[SeriesChecklist] ([Series],[Firs],[Coordinates],[Radius]) VALUES(N'V', N'CZUL CZQM CZQX', N'524300N 0532100W', 999)
END
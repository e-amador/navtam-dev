﻿using alatas.GeoJSON4EntityFramework;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity.Owin;
using NavCanada.Core.Common.Common;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Common.Extensions;
using NavCanada.Core.Common.Security;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.App_Start;
using NAVTAM.Helpers;
using NAVTAM.Helpers.XsrfValidator;
using NAVTAM.NsdEngine;
using NAVTAM.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Spatial;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Routing;

namespace NAVTAM.Controllers.api
{
    [ForgeryTokenValidation]
    public class ApiBaseController : ApiController
    {
        protected const int MaxRecordsToReturn = 1000;

        public ApiBaseController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger)
        {
            Uow = uow;
            AeroRdsProxy = aeroRdsProxy;
            GeoLocationCache = geoLocationCache;
            Logger = logger;

            ApplyCurrentCulture();
        }

        public IHttpActionResult LogAction(Func<IHttpActionResult> lambda, string method)
        {
            var threadId = GetThreadId();
            try
            {
                Logger.LogVerbose($"{threadId}) Begin {method}.");
                var result = lambda.Invoke();
                Logger.LogVerbose($"{threadId}) End {method}.");
                return result;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, GetType(), $"Error in {method}");
                return InternalServerError();
            }
        }

        public async Task<IHttpActionResult> LogAction(Func<Task<IHttpActionResult>> lambda, string method)
        {
            var threadId = GetThreadId();
            try
            {
                Logger.LogVerbose($"{threadId}) Begin {method}.");
                var result = await lambda.Invoke();
                Logger.LogVerbose($"{threadId}) End {method}.");
                return result;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, GetType(), $"Error in {method}");
                return InternalServerError();
            }
        }

        public async Task<IHttpActionResult> LogTransaction(Func<Task<ApiStepResult>> lambda, string method)
        {
            var threadId = GetThreadId();
            Uow.BeginTransaction();
            try
            {
                Logger.LogVerbose($"{threadId}) Begin {method}.");

                var result = await lambda.Invoke();

                if (result.Failed)
                {
                    Uow.Rollback();

                    return result.Error;
                }
                else
                {
                    Logger.LogVerbose($"{threadId}) End {method}.");

                    Uow.Commit();

                    return result.Result;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, GetType(), $"Error in {method}");
                Uow.Rollback();
                return InternalServerError();
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? (_userManager = HttpContext.Current?.GetOwinContext().GetUserManager<ApplicationUserManager>());
            }
            set
            {
                _userManager = value;
            }
        }

        public IApplicationContext ApplicationContext
        {
            get
            {
                return _applicationContext ?? (_applicationContext = new WebApiContext(HttpContext.Current, UserManager));
            }
            set
            {
                _applicationContext = value;
            }
        }

        public IGeoLocationCache GeoLocationCache { get; set; }

        protected IUow Uow { get; private set; }
        protected IAeroRdsProxy AeroRdsProxy { get; private set; }
        protected ILogger Logger { get; private set; }

        protected void ApplyCurrentCulture()
        {
            // avoid problems during unit-testing
            if (UserManager != null)
            {
                var cult = GetCulture();
                if (cult != null)
                {
                    Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(cult);
                }
            }
        }

        protected string GetCulture()
        {
            var cultureCookie = ApplicationContext?.GetCookie("_culture", null);
            return cultureCookie;
        }

        protected bool ValidUserRequest()
        {
            return ApplicationContext.IsValidUserRequest("usr_id");
        }

        public async Task<bool> IsValidUserAction(int catId)
        {
            var orgId = int.Parse(ApplicationContext.GetCookie("org_id", "0"));
            return await Uow.OrganizationNsdRepo.OrganizationContainsNsd(orgId, catId);
        }

        protected async Task<bool> IsLoggedUserInRole(string role)
        {
            return await ApplicationContext.IsUserInRole(ApplicationContext.GetUserId(), role);
        }

        protected async Task<bool> IsUserInRole(string userId, string role)
        {
            return await ApplicationContext.IsUserInRole(userId, role);
        }

        protected async Task<ProposalContext> GetProposalContextAsync(ProposalViewModel model, DbGeography userDoa)
        {
            return new ProposalContext(Uow, AeroRdsProxy, userDoa, await GetMinValidityPeriod());
        }

        protected async Task<bool> IsNOFUser()
        {
            var nof = await ApplicationContext.IsUserInRole(ApplicationContext.GetUserId(), CommonDefinitions.NofUserRole);
            var nofAdmin = await ApplicationContext.IsUserInRole(ApplicationContext.GetUserId(), CommonDefinitions.NofAdminRole);
            return nof || nofAdmin;
        }

        const int DefaultMvp = 0;  

        protected async Task<int> GetMinValidityPeriod()
        {
            var mvpValue = await Uow.ConfigurationValueRepo.GetValueAsync("NOTAM", "MinValidityPeriod") ?? "0";
            int mvp;
            return int.TryParse(mvpValue, out mvp) ? mvp : DefaultMvp;
        }

        protected async Task<int> GetExpiringValueMinutes()
        {
            var mvpValue = await Uow.ConfigurationValueRepo.GetValueAsync("NOTAM", "NOTAMESTExpireTime") ?? "0";
            int mvp;
            if (mvpValue == null || !int.TryParse(mvpValue, out mvp)) mvp = CommonDefinitions.DefaultExpireMinutes;
            return mvp;
        }

        protected async Task<List<ProposalAttachmentViewModel>> GetAllProposalAttachments(Proposal proposal)
        {
            var attachmentsVM = new List<ProposalAttachmentViewModel>();
            try
            {
                var attachments = await Uow.ProposalAttachmentRepo.GetByProposalIdAsync(proposal.Id);
                if (attachments != null)
                {
                    attachments = attachments.OrderByDescending(a => a.CreationDate).ToList();
                    foreach (var item in attachments)
                    {
                        var user = await Uow.UserProfileRepo.GetByIdAsync(item.UserId, false);
                        attachmentsVM.Add(FromAttachment(item, user.UserName));
                    }
                }
            }
            catch (Exception)
            {
                // todo log exception
            }
            return attachmentsVM;
        }

        protected Proposal MapModelToProposal(INsdManager manager, Proposal proposal, ProposalViewModel model, int orgId, NotamProposalStatusCode status)
        {
            var organization = Uow.OrganizationRepo.GetById(orgId);

            if (proposal == null)
            {
                proposal = new Proposal();
                proposal.ProposalType = NotamType.N;
                proposal.OrganizationId = model.OrganizationId = orgId;
                proposal.OrganizationType = (organization != null) ? organization.Type : OrganizationType.External;
                proposal.Operator = ApplicationContext.GetUserName();
            }
            else //if (status == NotamProposalStatusCode.Rejected || status == NotamProposalStatusCode.Disseminated)
            {
                // note: we have to garantee the proposal organizationId is not changed.
                model.OrganizationId = proposal.OrganizationId;
            }

            //We need to re-order the annotations
            model.Token.Annotations = SortAnnotationsByAscendingOrder(model.Token.Annotations);

            //After the sort I'm going back to prepare the NoteToNof field in chronological order
            var ntonof = "";
            for (var i = 0; i < model.Token.Annotations.Count; i++)
            {
                ntonof += model.Token.Annotations[i].Note;
                if (i < model.Token.Annotations.Count - 1) ntonof += " || ";
            }
            model.Token.NoteToNof = ntonof;
            model.NoteToNof = ntonof;
            model.ItemX = proposal.ItemX; // We are using ItemX as a patch to solve new requirements without altering the database 

            var pType = model.ProposalType;
            manager.FillProposalFromViewModel(proposal, model);
            if (pType != NotamType.N)
            {
                proposal.ProposalType = pType;
                proposal.NotamId = BuildNotamProposalId(proposal);
            }

            proposal.Status = status;
            proposal.Received = DateTime.UtcNow.RemoveSeconds();
            proposal.UserId = ApplicationContext.GetUserId();
            proposal.ModifiedByOrg = organization?.Name;
            proposal.ModifiedByUsr = ApplicationContext.GetUserName();
            proposal.Year = DateTime.UtcNow.Year;

            proposal.Location = manager.GetProposalLocation(model);
            if (proposal.Location == null)
            {
                // this should never happen but just in case..
                throw new Exception("Invalid location!");
            }

            return proposal;
        }

        protected async Task<string> RetrieveParentNotamId(Proposal proposal)
        {
            var parentNotamId = "";
            if (proposal.ProposalType != NotamType.N)
            {
                var histories = await Uow.ProposalHistoryRepo.GetByProposalIdDescAsync(proposal.Id);
                if (histories != null)
                {
                    var history = histories.FirstOrDefault(h => (h.Status == NotamProposalStatusCode.Disseminated || h.Status == NotamProposalStatusCode.DisseminatedModified) && (h.NotamId != proposal.NotamId) );
                    if (history != null) parentNotamId = history.NotamId;
                }
            }
            return parentNotamId;
        }

        protected string BuildNotamId(ProposalViewModel model)
        {
            if (model.ProposalType != NotamType.N)
                return $"{((model.ProposalType == NotamType.C) ? "C" : "R")}:{model.ReferredSeries}{model.ReferredNumber.ToString().PadLeft(4, '0')}/{model.ReferredYear.ToString().Substring(2)}";
            else if (model.Status == NotamProposalStatusCode.Disseminated || model.Status == NotamProposalStatusCode.DisseminatedModified)
            {
                if (!model.Series.IsNullOrWhiteSpace() && model.Number > 0 && model.Year > 0)
                    return $"{model.Series}{model.Number.ToString().PadLeft(4, '0')}/{model.Year.ToString().Substring(2)}";
                else return null;
            }
            else return null;
        }

        protected string BuildNotamProposalId(Proposal proposal)
        {
            if (proposal.ProposalType != NotamType.N)
                return $"{((proposal.ProposalType == NotamType.C) ? "C" : "R")}:{proposal.ReferredSeries}{proposal.ReferredNumber.ToString().PadLeft(4, '0')}/{proposal.ReferredYear.ToString().Substring(2)}";
            else
            {
                if ((proposal.Status == NotamProposalStatusCode.Disseminated) || (proposal.Status == NotamProposalStatusCode.DisseminatedModified))
                    return $"{proposal.Series}{proposal.Number.ToString().PadLeft(4, '0')}/{proposal.Year.ToString().Substring(2)}";
                else return null;
            }
        }

        protected string BuildNotamId(string series, int number, int year)
        {
            return $"{series}{number.ToString().PadLeft(4, '0')}/{year.ToString().Substring(2)}";
        }

        protected List<ProposalAnnotationViewModel> SortAnnotationsByAscendingOrder(List<ProposalAnnotationViewModel> annotations)
        {
            if (annotations.Count > 0)
            {
                var max = annotations.Max(a => a.Id) + 1;
                foreach (var item in annotations)
                {
                    if (item.Id == 0) {
                        item.Id = max++;
                        item.UserId = ApplicationContext.GetUserName();
                        item.Received = DateTime.UtcNow;
                    }
                }
                //annotations = annotations.OrderByDescending(a => a.Id).ToList();
                annotations = annotations.OrderBy(a => a.Id).ToList();
            }
            return annotations;

        }

        protected ProposalHistory HistoryFromProposal(Proposal proposal, ProposalHistory proposalHistory = null)
        {
            if (proposalHistory == null) proposalHistory = new ProposalHistory();

            var id = proposalHistory.Id;
            proposal.CopyPropertiesTo(proposalHistory);

            proposalHistory.Id = id;
            proposalHistory.ProposalId = proposal.Id;
            proposalHistory.Proposal = proposal;
            return proposalHistory;
        }

        protected Proposal ProposalFromHistory(ProposalHistory proposalHistory, Proposal proposal = null)
        {
            if (proposalHistory == null) return null;
            if (proposal == null) proposal = new Proposal();
            proposalHistory.CopyPropertiesTo(proposal);
            proposal.Id = proposalHistory.ProposalId;
            return proposal;
        }

        protected object ResponsewithPagingEnvelope<T>(QueryOptions queryOptions, string routeName, List<T> data, int totalCount = -1)
        {
            if (totalCount == -1)
                totalCount = data.Count;

            var totalPages = (int)Math.Ceiling((double)totalCount / queryOptions.PageSize);

            var urlHelper = new UrlHelper(Request);
            var prevLink = queryOptions.Page > 1
                ? urlHelper.Link(routeName,
                    new
                    {
                        page = queryOptions.Page - 1,
                        pageSize = queryOptions.PageSize,
                        sort = queryOptions.Sort
                    })
                : "";
            var nextLink = queryOptions.Page < totalPages
                ? urlHelper.Link(routeName,
                    new
                    {
                        page = queryOptions.Page + 1,
                        pageSize = queryOptions.PageSize,
                        sort = queryOptions.Sort
                    })
                : "";

            var paging = new
            {
                currentPage = queryOptions.Page,
                pageSize = queryOptions.PageSize,
                totalCount,
                totalPages,
                previuosPageLink = prevLink,
                nextPageLink = nextLink
            };

            return new
            {
                Data = data,
                Paging = paging
            };
        }

        protected async Task<bool> IsOrgAdmin()
        {
            return await ApplicationContext.IsUserInRole(CommonDefinitions.OrgAdminRole);
        }

        protected async Task<bool> IsNofAdmin()
        {
            return await ApplicationContext.IsUserInRole(CommonDefinitions.NofAdminRole);
        }

        protected async Task<bool> IsAdmin()
        {
            return await ApplicationContext.IsUserInRole(CommonDefinitions.AdminRole);
        }

        protected async Task<int?> GetScopeOrganizationId()
        {
            if (await ApplicationContext.IsUserInRole(CommonDefinitions.OrgAdminRole) || 
                await ApplicationContext.IsUserInRole(CommonDefinitions.NofAdminRole))
                return int.Parse(ApplicationContext.GetCookie("org_id") ?? "0");
            return null;
        }

        protected int GetOrganizationId()
        {
            return int.Parse(ApplicationContext.GetCookie("org_id") ?? "0");
        }

        private ProposalAttachmentViewModel FromAttachment(ProposalAttachment attachment, string userName, bool readOnly = true)
        {
            if (attachment == null)
                return null;

            return new ProposalAttachmentViewModel
            {
                Id = attachment.Id,
                FileName = attachment.FileName,
                UserName = userName,
                ReadOnly = readOnly
            };
        }

        protected static string ConvertToGeoJson(DbGeography geo)
        {
            try
            {
                var featureCol = new FeatureCollection(geo);
                return featureCol.Serialize(prettyPrint: false);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
            try
            {
                var featureCol = new FeatureCollection(geo.Normalize());
                return featureCol.Serialize(prettyPrint: false);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return null;
            }
        }

        protected async Task SendVerificationEmail(string userId, string code)
        {
            var user = await UserManager.FindByIdAsync(userId);

            var host = ConfigurationManager.AppSettings["domain"];
            var fullCode = HttpUtility.UrlEncode(CipherUtils.EncryptText(userId)) + "|" + HttpUtility.UrlEncode(code);
            var callbackUrl = host + String.Format("/Account/ConfirmEmail?code={0}", fullCode );
            //var callbackUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + String.Format("/Account/ConfirmEmail?userid={0}&code={1}", userId, HttpUtility.UrlEncode(code));

            var body = "The NOTAM Entry System created a user account associated with this email address. If you are not the intended recipient, please contact your site administrator. <br/> " +
                            //"The Username is :  " + user.UserName + "<br/>" +
                            "To activate your account, please click <a href=\"" + callbackUrl + "\">here</a>. </br>" +
                            "NOTE: This link only active for a period 24 hours from the time this email was sent. To request another link, please contact your NES System Administrator. <br/> <br/>" +
                            "Le système de saisie NOTAM a créé un compte utilisateur associé à cette adresse électronique. Si vous n'êtes pas le destinataire prévu, veuillez contacter votre administrateur de système. <br/> " +
                            //"Le nom d'utilisateur est: " + user.UserName + "<br/>" +
                            "Pour activer votre compte, veuillez cliquer <a href=\"" + callbackUrl + "\">ici</a>. </br>" +
                            "NOTE: Ce lien n’est actif que pendant une période de 24 heures à partir de l’envoi du courriel. Pour demander un autre lien, veuillez contacter l'administrateur de votre système. <br/>";

            await UserManager.SendEmailAsync(userId, "Confirm User Account for the NOTAM Entry System (NES) / Confirmer le compte utilisateur pour le système de saisie NOTAM (NES)", body);
        }


        static int GetThreadId() => Thread.CurrentThread.ManagedThreadId;

        private ApplicationUserManager _userManager;
        private IApplicationContext _applicationContext;
    }
}

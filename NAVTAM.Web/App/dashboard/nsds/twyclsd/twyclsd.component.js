"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TwyClsdComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var data_service_1 = require("../../shared/data.service");
var location_service_1 = require("../../shared/location.service");
var twyclsd_service_1 = require("./twyclsd.service");
var toastr_service_1 = require("./../../../common/toastr.service");
var angular_l10n_1 = require("angular-l10n");
var TwyClsdComponent = /** @class */ (function () {
    function TwyClsdComponent(toastr, injector, fb, locationService, twyClsdService, dataService, locale, translation) {
        this.toastr = toastr;
        this.injector = injector;
        this.fb = fb;
        this.locationService = locationService;
        this.twyClsdService = twyClsdService;
        this.dataService = dataService;
        this.locale = locale;
        this.translation = translation;
        this.action = "";
        this.token = null;
        this.isReadOnly = false;
        this.isBilingualRegion = false;
        this.subjectSelected = null;
        this.subjectId = '';
        this.subjects = [];
        this.taxiwayId = '-1';
        this.taxiwaySelected = null;
        this.taxiways = [];
        this.taxiwaysSelected = [];
        this.taxiwayComboList = [];
        this.closureReasonId = -1;
        this.closureReasonFreeText = '';
        this.closureReasonFreeTextFr = '';
        this.twyClsdReasons = [];
        this.closureStatus = '';
        this.allTaxiwaysSelected = false;
        this.closureReasonOptions = {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.ClosurePlaceholder')
            }
        };
        this.taxiwaysOptions = {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.TaxiwayPlaceholder')
            }
        };
        this.token = this.injector.get('token');
        this.isReadOnly = this.injector.get('isReadOnly');
        this.nsdForm = this.injector.get('form');
        this.action = this.injector.get('type');
    }
    TwyClsdComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.isFormLoadingData = !(!this.token.subjectId);
        var app = this.twyClsdService.app;
        this.leafletmap = app.leafletmap;
        this.closureReasonId = this.token.closureReasonId || -1;
        this.closureReasonFreeText = this.token.closureReasonFreeText || '';
        this.closureReasonFreeTextFr = this.token.closureReasonFreeTextFr || '';
        var frmArr = this.nsdForm.controls['frmArr'];
        if (frmArr.length === 0) {
            if (!this.isFormLoadingData) {
                this.subjectId = '';
                this.allTaxiwaysSelected = false;
                this.taxiways = [];
                this.configureReactiveForm();
                this.twyClsdService.getTwyClsdReasons()
                    .subscribe(function (reasons) {
                    var culture = _this.twyClsdService.app.cultureInfo;
                    _this.twyClsdReasons = reasons.map(function (twr) { return _this.mapTaxiWayReasonToLanguage(culture, twr); });
                    _this.isFormLoadingData = !(!_this.token.subjectId);
                    if (_this.isFormLoadingData)
                        _this.setFirstSelectItemInCombo(_this.twyClsdReasons, _this.token.closureReasonId);
                    else
                        _this.setFirstSelectItemInCombo(_this.twyClsdReasons, 0);
                    if (!_this.isReadOnly) {
                        _this.enableReasonSelector();
                    }
                    _this.isFormLoadingData = false;
                });
                this.loadMapLocations(app.doaId, false);
            }
            else {
                this.subjectId = this.token.subjectId || '';
                this.allTaxiwaysSelected = this.token.allTaxiwaysSelected || false;
                this.taxiwaysSelected = this.token.allTaxiways || [];
                this.configureReactiveFormLoading();
                this.twyClsdService.getTwyClsdReasons()
                    .subscribe(function (reasons) {
                    var culture = _this.twyClsdService.app.cultureInfo;
                    _this.twyClsdReasons = reasons.map(function (twr) { return _this.mapTaxiWayReasonToLanguage(culture, twr); });
                    _this.isFormLoadingData = !(!_this.token.subjectId);
                    if (_this.isFormLoadingData)
                        _this.setFirstSelectItemInCombo(_this.twyClsdReasons, _this.token.closureReasonId);
                    else
                        _this.setFirstSelectItemInCombo(_this.twyClsdReasons, 0);
                    if (!_this.isReadOnly) {
                        _this.enableReasonSelector();
                    }
                    _this.dataService.getSdoSubject(_this.token.subjectId)
                        .subscribe(function (sdoSubjectResult) {
                        _this.subjectSelected = sdoSubjectResult;
                        _this.subjects = [];
                        _this.subjects.push(sdoSubjectResult);
                        _this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);
                        _this.loadMapLocations(app.doaId, true);
                        _this.updateMap(sdoSubjectResult, true);
                        if (!_this.isReadOnly) {
                            _this.enableReasonSelector();
                        }
                        _this.twyClsdService.getSdoTwys(_this.subjectId)
                            .subscribe(function (sdoTwys) {
                            _this.taxiways = sdoTwys;
                            _this.prepareTaxiwayComboList();
                            _this.$ctrl('countSelectedTwys').setValue(_this.countSelectedTwys);
                            if (_this.allTaxiwaysSelected) {
                                _this.pickTaxiway('0');
                            }
                            else {
                                if (_this.taxiwaysSelected.length > 0) {
                                    var lastId = _this.taxiwaysSelected[_this.taxiwaysSelected.length - 1].id;
                                    _this.pickTaxiway(lastId);
                                }
                            }
                            _this.isFormLoadingData = false;
                        });
                    });
                });
                //this.configureReactiveFormLoading();
            }
        }
        else {
            /*
            this is required because of the grouping funtionallity. When the NsdFormGroup
            was configured and any of items in the group has changed (new set of tokens values)
            the NsdForm Group needs to be updated with the new tokens values */
            this.subjectId = this.token.subjectId || '';
            this.closureReasonId = this.token.closureReasonId || -1;
            this.closureReasonFreeText = this.token.closureReasonFreeText || '';
            this.closureReasonFreeTextFr = this.token.closureReasonFreeTextFr || '';
            this.allTaxiwaysSelected = this.token.allTaxiwaysSelected || false;
            this.taxiwaysSelected = this.token.allTaxiways || [];
            this.dataService.getSdoSubject(this.token.subjectId)
                .subscribe(function (sdoSubjectResult) {
                _this.subjectSelected = sdoSubjectResult;
                _this.subjects = [];
                _this.subjects.push(sdoSubjectResult);
                _this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);
                _this.loadMapLocations(app.doaId, true);
                _this.updateMap(sdoSubjectResult, true);
                if (!_this.isReadOnly) {
                    _this.enableReasonSelector();
                }
                _this.twyClsdService.getSdoTwys(_this.subjectId)
                    .subscribe(function (sdoTwys) {
                    _this.taxiways = sdoTwys;
                    _this.prepareTaxiwayComboList();
                    _this.$ctrl('countSelectedTwys').setValue(_this.countSelectedTwys);
                    if (_this.allTaxiwaysSelected) {
                        _this.pickTaxiway('0');
                    }
                    else {
                        if (_this.taxiwaysSelected.length > 0) {
                            var lastId = _this.taxiwaysSelected[_this.taxiwaysSelected.length - 1].id;
                            _this.pickTaxiway(lastId);
                        }
                    }
                });
            });
            this.updateReactiveForm(frmArr);
        }
        this.isFormLoadingData = false;
    };
    TwyClsdComponent.prototype.ngOnDestroy = function () {
        //if (this.leafletmap) {
        //    this.leafletmap.dispose();
        //    this.leafletmap = null;
        //}
    };
    TwyClsdComponent.prototype.mapTaxiWayReasonToLanguage = function (culture, v) {
        var text = culture === "en" ? v.text : v.textFr;
        return { id: v.id, text: text };
    };
    TwyClsdComponent.prototype.getTaxiwayName = function (twy) {
        return twy.sdoEntityName.toUpperCase() + ' ' + twy.designator.toUpperCase();
    };
    TwyClsdComponent.prototype.getClosureText = function (twy) {
        if (twy.full)
            return this.translation.translate('TableHeaders.FullClosure');
        else
            return this.translation.translate('TableHeaders.PartialClosure');
    };
    TwyClsdComponent.prototype.getBetweenText = function (twy) {
        if (twy.full)
            return "";
        else {
            if (this.isBilingualRegion) {
                return twy.start.toUpperCase() + " / " + twy.startFr.toUpperCase();
            }
            else {
                return twy.start.toUpperCase();
            }
        }
    };
    TwyClsdComponent.prototype.getAndText = function (twy) {
        if (twy.full)
            return "";
        else {
            if (this.isBilingualRegion) {
                return twy.end.toUpperCase() + " / " + twy.endFr.toUpperCase();
            }
            else {
                return twy.end.toUpperCase();
            }
        }
    };
    TwyClsdComponent.prototype.setClosureStatusText = function (full) {
        if (full)
            this.closureStatus = this.translation.translate('TableHeaders.FullClosure');
        else
            this.closureStatus = this.translation.translate('TableHeaders.PartialClosure');
    };
    //Enable functions
    TwyClsdComponent.prototype.enableReasonSelector = function () {
        var options = { onlySelf: true, emitEvent: !this.isFormLoadingData };
        var reasonIdCtrl = this.$ctrl('grpClosure.closureReasonId');
        var reasonTextCtrl = this.$ctrl('grpClosure.closureReasonFreeText');
        var reasonTextCtrlFr = this.$ctrl('grpClosure.closureReasonFreeTextFr');
        var grpClosure = this.nsdFrmGroup.get('grpClosure');
        if (this.countSelectedTwys === 0) {
            this.closureReasonFreeText = '';
            this.closureReasonFreeTextFr = '';
            this.closureReasonId = 0;
            grpClosure.patchValue({ closureReasonId: this.closureReasonId }, options);
            grpClosure.patchValue({ closureReasonFreeText: this.closureReasonFreeText }, options);
            reasonIdCtrl.disable();
            reasonTextCtrl.disable();
            if (this.isBilingualRegion) {
                grpClosure.patchValue({ closureReasonFreeTextFr: this.closureReasonFreeTextFr }, options);
                reasonTextCtrlFr.disable();
            }
        }
        else {
            reasonIdCtrl.enable();
            if (reasonIdCtrl.value !== undefined && reasonIdCtrl.value !== 0) {
                var reasonValueText = this.twyClsdReasons.find(function (x) { return x.id === reasonIdCtrl.value; }).text.toLowerCase();
                if ((reasonValueText === 'other') || (reasonValueText === 'autre')) {
                    reasonTextCtrl.enable();
                    if (this.isBilingualRegion) {
                        reasonTextCtrlFr.enable();
                    }
                }
                else {
                    this.closureReasonFreeText = '';
                    this.closureReasonFreeTextFr = '';
                    grpClosure.patchValue({ closureReasonFreeText: this.closureReasonFreeText }, options);
                    reasonTextCtrl.disable();
                    if (this.isBilingualRegion) {
                        grpClosure.patchValue({ closureReasonFreeTextFr: this.closureReasonFreeTextFr }, options);
                        reasonTextCtrlFr.disable();
                    }
                }
            }
            else {
                this.closureReasonFreeText = '';
                this.closureReasonFreeTextFr = '';
                grpClosure.patchValue({ closureReasonFreeText: this.closureReasonFreeText }, options);
                reasonTextCtrl.disable();
                if (this.isBilingualRegion) {
                    grpClosure.patchValue({ closureReasonFreeTextFr: this.closureReasonFreeTextFr }, options);
                    reasonTextCtrlFr.disable();
                }
            }
        }
    };
    TwyClsdComponent.prototype.isExcludeButtonDisabled = function () {
        var _this = this;
        if (this.isReadOnly)
            return true;
        if (this.allTaxiwaysSelected)
            return false;
        if (!this.taxiwaySelected)
            return true;
        var sel = this.taxiwaysSelected.find(function (x) { return x.id === _this.taxiwaySelected.id; });
        if (sel)
            return false;
        else
            return true;
    };
    TwyClsdComponent.prototype.isModifyButtonDisabled = function () {
        var _this = this;
        if (this.isReadOnly)
            return true;
        if (!this.taxiwaySelected)
            return true;
        if (this.allTaxiwaysSelected)
            return true;
        var sel = this.taxiwaysSelected.find(function (x) { return x.id === _this.taxiwaySelected.id; });
        if (sel) {
            if (this.validaClosureFields())
                return false;
            else
                return true;
        }
        else
            return true;
    };
    TwyClsdComponent.prototype.isIncludeButtonDisabled = function () {
        var _this = this;
        if (this.isReadOnly)
            return true;
        if (!this.taxiwaySelected) {
            if (!this.isAllTaxiwaysBeingProcessed())
                return true;
            else if (this.allTaxiwaysSelected)
                return true;
            else
                return false;
        }
        else {
            var sel = this.taxiwaysSelected.find(function (x) { return x.id === _this.taxiwaySelected.id; });
            if (sel)
                return true;
            else {
                if (this.validaClosureFields())
                    return false;
                else
                    return true;
            }
        }
    };
    TwyClsdComponent.prototype.isFullClosureDisabled = function () {
        if (this.isReadOnly)
            return true;
        if (this.isAllTaxiwaysBeingProcessed())
            return true;
        if (this.allTaxiwaysSelected)
            return true;
        return false;
    };
    TwyClsdComponent.prototype.isTaxiwaySelectorDisabled = function () {
        if (this.allTaxiwaysSelected)
            return true;
        if (this.isReadOnly)
            return true;
        return false;
    };
    TwyClsdComponent.prototype.isAllTaxiwaysBeingProcessed = function () {
        if (this.taxiwayId === '0')
            return true;
        return false;
    };
    TwyClsdComponent.prototype.isEditing = function () {
        return (this.$ctrl('refreshIcao').value === 0);
    };
    TwyClsdComponent.prototype.enableEditing = function () {
        this.$ctrl('refreshIcao').setValue(0);
    };
    TwyClsdComponent.prototype.stopEditing = function () {
        this.$ctrl('refreshIcao').setValue(1);
    };
    TwyClsdComponent.prototype.restoreTaxiwayValues = function () {
        var _this = this;
        if (this.isEditing() && this.taxiwaySelected) {
            var index = this.taxiways.findIndex(function (y) { return y.id === _this.taxiwaySelected.id; });
            this.taxiways[index].end = this.taxiwaySelected.end;
            this.taxiways[index].endFr = this.taxiwaySelected.endFr;
            this.taxiways[index].full = this.taxiwaySelected.full;
            this.taxiways[index].selected = this.taxiwaySelected.selected;
            this.taxiways[index].start = this.taxiwaySelected.start;
            this.taxiways[index].startFr = this.taxiwaySelected.startFr;
            this.stopEditing();
        }
    };
    TwyClsdComponent.prototype.pickTaxiway = function (id) {
        var selected = this.taxiwaysSelected.find(function (x) { return x.id === id; });
        if (selected) {
            this.taxiwaySelected = selected;
            if (this.isEditing()) {
                this.restoreTaxiwayValues();
            }
            //Restore control values
            var field = this.$ctrl('fullClosure');
            field.patchValue(this.taxiwaySelected.full);
            field = this.$ctrl('startClosure');
            field.patchValue(this.taxiwaySelected.start);
            field = this.$ctrl('startClosureFr');
            field.patchValue(this.taxiwaySelected.startFr);
            field = this.$ctrl('endClosure');
            field.patchValue(this.taxiwaySelected.end);
            field = this.$ctrl('endClosureFr');
            field.patchValue(this.taxiwaySelected.endFr);
            var twySel = this.$ctrl('taxiwayId');
            twySel.setValue(selected.id, { emiltEvent: true });
        }
    };
    TwyClsdComponent.prototype.rowColor = function (id) {
        if (!this.taxiwaySelected) {
            if (this.allTaxiwaysSelected)
                return 'bg-submitted';
            else
                return '';
        }
        if (this.taxiwaySelected.id === id)
            return 'bg-submitted';
        return '';
    };
    TwyClsdComponent.prototype.buttonColor = function () {
        if (!this.isModifyButtonDisabled()) {
            var ctrlFC = this.$ctrl('fullClosure');
            if (ctrlFC.value !== this.taxiwaySelected.full)
                return 'btn-blue';
            var ctrlSC = this.$ctrl('startClosure');
            var ctrlAC = this.$ctrl('endClosure');
            var ctrlSCF = this.$ctrl('startClosureFr');
            var ctrlACF = this.$ctrl('endClosureFr');
            if (ctrlSC.value !== this.taxiwaySelected.start ||
                ctrlAC.value !== this.taxiwaySelected.end ||
                ctrlSCF.value !== this.taxiwaySelected.startFr ||
                ctrlACF.value !== this.taxiwaySelected.endFr) {
                return 'btn-blue';
            }
        }
        return 'btn-info';
    };
    TwyClsdComponent.prototype.cleanTaxiwaysData = function () {
        for (var i = 0; i < this.taxiways.length; i++) {
            this.taxiways[i].end = '';
            this.taxiways[i].endFr = '';
            this.taxiways[i].start = '';
            this.taxiways[i].startFr = '';
            if (this.taxiways[i].id !== '0') {
                this.taxiways[i].full = false;
                this.taxiways[i].selected = false;
            }
            else {
                this.taxiways[i].full = true;
            }
        }
    };
    TwyClsdComponent.prototype.includeTaxiway = function () {
        if (this.isAllTaxiwaysBeingProcessed()) {
            this.allTaxiwaysSelected = true;
            //Clean the selected list
            this.taxiwaysSelected = [];
            var twy = this.taxiways.find(function (t) { return t.id === '0'; });
            this.taxiwaysSelected.push(twy);
            this.$ctrl('allTaxiwaysSelected').patchValue(this.allTaxiwaysSelected);
            this.cleanTaxiwaysData();
            this.pickTaxiway(twy.id);
        }
        else {
            this.$ctrl('allTaxiwaysSelected').patchValue(this.allTaxiwaysSelected);
            var ctrl = this.$ctrl('startClosure');
            this.taxiwaySelected.start = ctrl.value;
            ctrl = this.$ctrl('startClosureFr');
            this.taxiwaySelected.startFr = ctrl.value;
            var ctrl = this.$ctrl('endClosure');
            this.taxiwaySelected.end = ctrl.value;
            ctrl = this.$ctrl('endClosureFr');
            this.taxiwaySelected.endFr = ctrl.value;
            ctrl = this.$ctrl('fullClosure');
            this.taxiwaySelected.full = ctrl.value;
            this.taxiwaySelected.selected = true;
            var twy = Object.assign({}, this.taxiwaySelected);
            this.taxiwaysSelected.push(twy);
            this.pickTaxiway(this.taxiwaySelected.id);
        }
        this.stopEditing();
        this.validateAndTrigger();
        this.$ctrl('allTaxiways').markAsDirty();
        //this.markFrmDirty();
    };
    TwyClsdComponent.prototype.modifyTaxiway = function () {
        var _this = this;
        var ctrl = this.$ctrl('startClosure');
        this.taxiwaySelected.start = ctrl.value;
        ctrl = this.$ctrl('startClosureFr');
        this.taxiwaySelected.startFr = ctrl.value;
        ctrl = this.$ctrl('endClosure');
        this.taxiwaySelected.end = ctrl.value;
        ctrl = this.$ctrl('endClosureFr');
        this.taxiwaySelected.endFr = ctrl.value;
        ctrl = this.$ctrl('fullClosure');
        this.taxiwaySelected.full = ctrl.value;
        this.taxiwaySelected.selected = true;
        var index = this.taxiwaysSelected.findIndex(function (x) { return x.id === _this.taxiwaySelected.id; });
        this.taxiwaysSelected[index].start = this.taxiwaySelected.start;
        this.taxiwaysSelected[index].startFr = this.taxiwaySelected.startFr;
        this.taxiwaysSelected[index].end = this.taxiwaySelected.end;
        this.taxiwaysSelected[index].endFr = this.taxiwaySelected.endFr;
        this.taxiwaysSelected[index].full = this.taxiwaySelected.full;
        //Set values on taxiways list
        index = this.taxiways.findIndex(function (y) { return y.id === _this.taxiwaySelected.id; });
        this.taxiways[index].end = this.taxiwaySelected.end;
        this.taxiways[index].endFr = this.taxiwaySelected.endFr;
        this.taxiways[index].full = this.taxiwaySelected.full;
        this.taxiways[index].selected = this.taxiwaySelected.selected;
        this.taxiways[index].start = this.taxiwaySelected.start;
        this.taxiways[index].startFr = this.taxiwaySelected.startFr;
        this.stopEditing();
        this.validateAndTrigger();
        this.$ctrl('allTaxiways').markAsDirty();
        //this.markFrmDirty();
    };
    TwyClsdComponent.prototype.excludeTaxiway = function () {
        var _this = this;
        if (this.allTaxiwaysSelected === true) {
            this.allTaxiwaysSelected = false;
            this.$ctrl('allTaxiwaysSelected').patchValue(this.allTaxiwaysSelected);
            this.taxiwaysSelected = [];
            this.taxiwaySelected = null;
            var field = this.$ctrl('taxiwayId');
            field.patchValue('');
            field = this.$ctrl('startClosure');
            field.patchValue('');
            field = this.$ctrl('endClosure');
            field.patchValue('');
            field = this.$ctrl('startClosureFr');
            field.patchValue('');
            field = this.$ctrl('endClosureFr');
            field.patchValue('');
            this.taxiwayId = '-1';
            this.setFirstSelectItemInTaxiways(this.taxiwayComboList, this.taxiwayId, false);
            this.closureStatus = this.translation.translate('TableHeaders.PartialClosure');
        }
        else {
            var allTwySelValue = this.$ctrl('allTaxiwaysSelected').value;
            if (allTwySelValue !== this.allTaxiwaysSelected)
                this.$ctrl('allTaxiwaysSelected').patchValue(this.allTaxiwaysSelected);
            this.taxiwaySelected.selected = false;
            this.taxiwaySelected.end = '';
            this.taxiwaySelected.endFr = '';
            this.taxiwaySelected.full = false;
            this.taxiwaySelected.start = '';
            this.taxiwaySelected.startFr = '';
            this.stopEditing();
            var index = this.taxiwaysSelected.findIndex(function (x) { return x.id === _this.taxiwaySelected.id; });
            this.taxiwaysSelected.splice(index, 1);
            if (this.taxiwaysSelected.length > 0) {
                this.pickTaxiway(this.taxiwaysSelected[0].id);
            }
            else {
                //Deleted the last one selected
                this.taxiwaySelected = null;
                var field = this.$ctrl('taxiwayId');
                field.patchValue('');
                field = this.$ctrl('startClosure');
                field.patchValue('');
                field = this.$ctrl('endClosure');
                field.patchValue('');
                field = this.$ctrl('startClosureFr');
                field.patchValue('');
                field = this.$ctrl('endClosureFr');
                field.patchValue('');
                this.taxiwayId = '-1';
                this.setFirstSelectItemInTaxiways(this.taxiwayComboList, this.taxiwayId, false);
                this.closureStatus = this.translation.translate('TableHeaders.PartialClosure');
            }
        }
        this.validateAndTrigger();
        this.$ctrl('allTaxiways').markAsDirty();
        //this.markFrmDirty();
    };
    TwyClsdComponent.prototype.configureReactiveForm = function () {
        var frmArrControls = this.nsdForm.controls['frmArr'];
        this.nsdFrmGroup = this.fb.group({
            subjectId: [this.token.subjectId || '', [forms_1.Validators.required]],
            subjectLocation: this.token.subjectLocation || '',
            taxiwayId: [{ value: this.taxiwayId, disabled: false }],
            fullClosure: [{ value: false, disabled: (this.isReadOnly) ? true : false }],
            startClosure: [{ value: '', disabled: false }],
            startClosureFr: [{ value: '', disabled: !this.isBilingualRegion }],
            endClosure: [{ value: '', disabled: false }],
            endClosureFr: [{ value: '', disabled: !this.isBilingualRegion }],
            grpClosure: this.fb.group({
                closureReasonId: [{ value: this.token.closureReasonId, disabled: !this.isFormLoadingData }],
                closureReasonFreeText: [{ value: this.token.closureReasonFreeText, disabled: !this.isFormLoadingData }],
                closureReasonFreeTextFr: [{ value: this.token.closureReasonFreeTextFr, disabled: !this.isFormLoadingData }],
            }),
            //allTaxiways: [{ value: this.taxiways, disabled: false }],
            allTaxiways: [{ value: this.taxiwaysSelected, disabled: false }],
            countSelectedTwys: [{ value: 0 }, [countSelectedTwysValidator]],
            allTaxiwaysSelected: [{ value: this.allTaxiwaysSelected, disabled: false }],
            refreshIcao: [{ value: 1 }, [refreshIcaoValidator]],
        });
        frmArrControls.push(this.nsdFrmGroup);
    };
    TwyClsdComponent.prototype.configureReactiveFormLoading = function () {
        //Modify this to upload the data
        var disable = (this.isReadOnly) ? true : !this.isFormLoadingData;
        var frmArrControls = this.nsdForm.controls['frmArr'];
        this.nsdFrmGroup = this.fb.group({
            subjectId: [this.token.subjectId || '', [forms_1.Validators.required]],
            subjectLocation: this.token.subjectLocation || '',
            taxiwayId: [{ value: this.taxiwayId, disabled: false }],
            fullClosure: [{ value: false, disabled: disable }],
            startClosure: [{ value: this.taxiwaySelected ? this.taxiwaySelected.start : '', disabled: disable }],
            startClosureFr: [{ value: this.taxiwaySelected ? this.taxiwaySelected.startFr : '', disabled: disable }],
            endClosure: [{ value: this.taxiwaySelected ? this.taxiwaySelected.end : '', disabled: disable }],
            endClosureFr: [{ value: this.taxiwaySelected ? this.taxiwaySelected.endFr : '', disabled: disable }],
            grpClosure: this.fb.group({
                closureReasonId: [{ value: this.token.closureReasonId, disabled: disable }],
                closureReasonFreeText: [{ value: this.token.closureReasonFreeText, disabled: disable }],
                closureReasonFreeTextFr: [{ value: this.token.closureReasonFreeTextFr, disabled: disable }],
            }),
            allTaxiways: [{ value: this.taxiwaysSelected, disabled: false }],
            countSelectedTwys: [{ value: this.countSelectedTwys }, [countSelectedTwysValidator]],
            allTaxiwaysSelected: [{ value: this.token.allTaxiwaysSelected, disabled: false }],
            refreshIcao: [{ value: 1 }, [refreshIcaoValidator]],
        });
        frmArrControls.push(this.nsdFrmGroup);
    };
    TwyClsdComponent.prototype.updateReactiveForm = function (formArr) {
        this.nsdFrmGroup = formArr.controls[0];
        //It is better to update only those controls that tokens values have changed.
        this.updateFormControlValue("subjectId", this.token.subjectId);
        this.updateFormControlValue("subjectLocation", this.token.subjectLocation);
        this.updateFormControlValue("taxiwayId", this.taxiwayId);
        this.updateFormControlValue("fullClosure", false);
        this.updateFormControlValue("startClosure", this.taxiwaySelected ? this.taxiwaySelected.start : '');
        this.updateFormControlValue("startClosureFr", this.taxiwaySelected ? this.taxiwaySelected.startFr : '');
        this.updateFormControlValue("endClosure", this.taxiwaySelected ? this.taxiwaySelected.end : '');
        this.updateFormControlValue("endClosureFr", this.taxiwaySelected ? this.taxiwaySelected.endFr : '');
        this.updateFormControlValue("grpClosure.closureReasonId", this.token.closureReasonId);
        this.updateFormControlValue("grpClosure.closureReasonFreeText", this.token.closureReasonFreeText);
        this.updateFormControlValue("grpClosure.closureReasonFreeTextFr", this.token.closureReasonFreeTextFr);
        this.updateFormControlValue("allTaxiways", this.taxiwaysSelected);
        this.updateFormControlValue("countSelectedTwys", this.countSelectedTwys);
        this.updateFormControlValue("allTaxiwaysSelected", this.token.allTaxiwaysSelected);
    };
    TwyClsdComponent.prototype.updateFormControlValue = function (controlName, value) {
        var control = this.nsdFrmGroup.get(controlName);
        if (control && control.value !== value) {
            control.setValue(value);
        }
    };
    TwyClsdComponent.prototype.loadSdoSubjectTwy = function (sdoId) {
        var _this = this;
        if (sdoId) {
            var self_1 = this;
            this.twyClsdService.getSdoTwys(sdoId)
                .subscribe(function (sdoTwys) {
                _this.taxiwaySelected = null;
                _this.taxiwaysSelected = [];
                _this.taxiways = sdoTwys;
                _this.prepareTaxiwayComboList();
                _this.$ctrl('countSelectedTwys').setValue(_this.countSelectedTwys);
                _this.$ctrl('grpClosure').setValidators(finalClosureValidator(_this.twyClsdReasons, _this.isBilingualRegion));
                _this.$ctrl('grpClosure').updateValueAndValidity();
            });
        }
    };
    TwyClsdComponent.prototype.prepareTaxiwayComboList = function () {
        this.taxiwayComboList = [];
        if (this.taxiways.length > 1 && this.taxiways.findIndex(function (t) { return t.id === '0'; }) === -1) {
            var twyAll = {
                designator: 'TWYS',
                end: '',
                endFr: '',
                full: true,
                id: '0',
                mid: '0',
                sdoEntityName: 'ALL',
                selected: true,
                start: '',
                startFr: '',
                index: 0
            };
            this.taxiways.unshift(twyAll);
        }
        this.taxiwayComboList = this.taxiways.map(function (item) {
            return {
                id: item.id,
                text: item.sdoEntityName.toUpperCase() + ' ' + item.designator.toUpperCase(),
            };
        });
        //Add empty slot
        this.taxiwayComboList.unshift({
            id: "-1",
            text: "",
        });
    };
    TwyClsdComponent.prototype.$ctrl = function (name) {
        return this.nsdFrmGroup.get(name);
    };
    TwyClsdComponent.prototype.sdoSubjectWithTwysOptions = function () {
        var self = this;
        var app = this.twyClsdService.app;
        return {
            minimumInputLength: 2,
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.SdoSubjectPlaceholder')
            },
            language: {
                inputTooShort: function (args) {
                    return self.translation.translate('Schedule.InputTooShort');
                },
            },
            ajax: {
                type: 'GET',
                url: app.apiUrl + "taxiways/filterwithtwy",
                dataType: 'json',
                delay: 500,
                headers: {
                    "RequestVerificationToken": app.antiForgeryTokenValue
                },
                data: function (parms, page) {
                    return {
                        Query: parms.term,
                        Size: 200,
                        doaId: app.doaId //set by the app controller
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            templateResult: function (data) {
                if (data.loading) {
                    return data.text;
                }
                return self.dataService.getSdoExtendedName(data);
            },
            templateSelection: function (selection) {
                if (selection && selection.id === "-1") {
                    return selection.text;
                }
                self.updateSubjectSelected(selection);
                return self.dataService.getSdoExtendedName(selection);
            },
        };
    };
    TwyClsdComponent.prototype.updateSubjectSelected = function (sdoSubject) {
        var _this = this;
        var subjectId = this.$ctrl('subjectId').value;
        if (subjectId === sdoSubject.id) {
            //for some weird reasons select2 templateSelection is called multiples 
            //times after selection
            return;
        }
        if (sdoSubject.id !== subjectId) {
            var subjectCtrl = this.$ctrl('subjectId');
            subjectCtrl.setValue(sdoSubject.id);
            subjectCtrl.markAsDirty();
        }
        this.subjectSelected = sdoSubject;
        if (!sdoSubject.subjectGeo) {
            this.dataService.getSdoSubject(sdoSubject.id)
                .subscribe(function (sdoSubjectResult) {
                _this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);
                _this.loadSdoSubjectTwy(sdoSubject.id);
                _this.updateMap(sdoSubjectResult, true);
            });
        }
        else {
            this.setBilingualRegion(sdoSubject.isBilingualRegion);
            this.loadSdoSubjectTwy(sdoSubject.id);
        }
    };
    Object.defineProperty(TwyClsdComponent.prototype, "colBetweenCaption", {
        get: function () {
            if (this.isBilingualRegion)
                return this.translation.translate('TableHeaders.BetweenBiligual');
            else
                return this.translation.translate('TableHeaders.Between');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(TwyClsdComponent.prototype, "colAndCaption", {
        get: function () {
            if (this.isBilingualRegion)
                return this.translation.translate('TableHeaders.AndBiligual');
            else
                return this.translation.translate('TableHeaders.And');
        },
        enumerable: false,
        configurable: true
    });
    //OnChange functions
    TwyClsdComponent.prototype.fullClosureChange = function () {
        if (this.isFormLoadingData || this.isReadOnly)
            return;
        if (this.isAllTaxiwaysBeingProcessed() || this.allTaxiwaysSelected) {
            var ctrl = this.$ctrl('fullClosure');
            var options = { onlySelf: true, emitEvent: !this.isFormLoadingData };
            if (ctrl.value === false)
                ctrl.patchValue(true, options);
            return;
        }
        var ctrl = this.$ctrl('fullClosure');
        if (ctrl && this.taxiwaySelected) {
            this.setClosureStatusText(ctrl.value);
            this.enableEditing();
            if (ctrl.value) {
                ctrl = this.$ctrl('startClosure');
                ctrl.patchValue('');
                ctrl.disable();
                ctrl = this.$ctrl('endClosure');
                ctrl.patchValue('');
                ctrl.disable();
                if (this.isBilingualRegion) {
                    ctrl = this.$ctrl('startClosureFr');
                    ctrl.patchValue('');
                    ctrl.disable();
                    ctrl = this.$ctrl('endClosureFr');
                    ctrl.patchValue('');
                    ctrl.disable();
                }
            }
            else {
                ctrl = this.$ctrl('startClosure');
                ctrl.enable();
                ctrl = this.$ctrl('endClosure');
                ctrl.enable();
                if (this.isBilingualRegion) {
                    ctrl = this.$ctrl('startClosureFr');
                    ctrl.enable();
                    ctrl = this.$ctrl('endClosureFr');
                    ctrl.enable();
                }
            }
        }
    };
    TwyClsdComponent.prototype.closureReasonChanged = function ($event) {
        if ($event.value !== "-1") {
            var reasonValueId_1 = +$event.value;
            if (reasonValueId_1 === this.closureReasonId) {
                //for some weird reasons select2 templateSelection is called multiples 
                //times after selection
                return;
            }
            if (reasonValueId_1 !== -1) {
                var options = { onlySelf: true, emitEvent: false };
                var reasonIdCtrl = this.$ctrl('grpClosure.closureReasonId');
                var reasonValueText = this.twyClsdReasons.find(function (x) { return x.id === reasonValueId_1; }).text.toLowerCase();
                var reasonTextCtrl = this.$ctrl('grpClosure.closureReasonFreeText');
                var reasonTextCtrlFr = this.$ctrl('grpClosure.closureReasonFreeTextFr');
                if ((reasonValueText === 'other') || (reasonValueText === 'autre')) {
                    // Change the free text
                    reasonTextCtrl.enable();
                    if (this.isBilingualRegion) {
                        reasonTextCtrlFr.enable();
                    }
                }
                else {
                    this.nsdFrmGroup.get('grpClosure').patchValue({ closureReasonFreeText: '' }, options);
                    reasonTextCtrl.disable();
                    if (this.isBilingualRegion) {
                        this.nsdFrmGroup.get('grpClosure').patchValue({ closureReasonFreeTextFr: '' }, options);
                        reasonTextCtrlFr.disable();
                    }
                }
                this.closureReasonId = reasonValueId_1;
                this.nsdFrmGroup.get('grpClosure').patchValue({ closureReasonId: reasonValueId_1 }, options);
                if (!this.isFormLoadingData) {
                    this.validateAndTrigger();
                    this.$ctrl('grpClosure.closureReasonId').markAsDirty();
                    //this.markFrmDirty();
                }
            }
        }
    };
    TwyClsdComponent.prototype.closureReasonTextChange = function () {
        this.validateAndTrigger();
        this.$ctrl('grpClosure.closureReasonFreeText').markAsDirty();
        //this.markFrmDirty();
    };
    TwyClsdComponent.prototype.closureReasonTextFrChange = function () {
        this.validateAndTrigger();
        this.$ctrl('grpClosure.closureReasonFreeTextFr').markAsDirty();
        //this.markFrmDirty();
    };
    TwyClsdComponent.prototype.selectedTaxiwayChanged = function ($event) {
        if (this.isReadOnly)
            return;
        var twySel = this.$ctrl('taxiwayId');
        if (twySel) {
            var val = $event.value;
            //if (val === twySel.value) return;
            this.taxiwayId = val;
            if (val === '-1')
                return;
            if (val === '0') {
                //All taxiways
                var field = this.$ctrl('startClosure');
                field.patchValue('');
                field.disable();
                field = this.$ctrl('endClosure');
                field.patchValue('');
                field.disable();
                field = this.$ctrl('startClosureFr');
                field.patchValue('');
                field.disable();
                field = this.$ctrl('endClosureFr');
                field.patchValue('');
                field.disable();
                field = this.$ctrl('fullClosure');
                field.patchValue(true);
                field.disable();
                this.taxiwaySelected = this.taxiways.find(function (x) { return x.id === val; });
            }
            else {
                var already = this.taxiwaysSelected.find(function (x) { return x.id === val; });
                if (already) {
                    //Update the fields
                    var field = this.$ctrl('fullClosure');
                    if (field.disabled)
                        field.enable();
                    field.patchValue(already.full);
                    field = this.$ctrl('startClosure');
                    if (field.disabled)
                        field.enable();
                    if (!already.full)
                        field.patchValue(already.start);
                    else
                        field.patchValue('');
                    field = this.$ctrl('endClosure');
                    if (field.disabled)
                        field.enable();
                    if (!already.full)
                        field.patchValue(already.end);
                    else
                        field.patchValue('');
                    if (this.isBilingualRegion) {
                        field = this.$ctrl('startClosureFr');
                        if (field.disabled)
                            field.enable();
                        if (!already.full)
                            field.patchValue(already.startFr);
                        else
                            field.patchValue('');
                        field = this.$ctrl('endClosureFr');
                        if (field.disabled)
                            field.enable();
                        if (!already.full)
                            field.patchValue(already.endFr);
                        else
                            field.patchValue('');
                    }
                    else {
                        field = this.$ctrl('startClosureFr');
                        if (field.disabled)
                            field.enable();
                        field.patchValue('');
                        field = this.$ctrl('endClosureFr');
                        if (field.disabled)
                            field.enable();
                        field.patchValue('');
                    }
                    this.setClosureStatusText(already.full);
                }
                else {
                    var field = this.$ctrl('startClosure');
                    field.patchValue('');
                    if (field.enabled)
                        field.disable();
                    field = this.$ctrl('endClosure');
                    field.patchValue('');
                    if (field.enabled)
                        field.disable();
                    field = this.$ctrl('startClosureFr');
                    field.patchValue('');
                    if (field.enabled)
                        field.disable();
                    field = this.$ctrl('endClosureFr');
                    field.patchValue('');
                    if (field.enabled)
                        field.disable();
                    field = this.$ctrl('fullClosure');
                    if (field.disabled)
                        field.enable();
                    field.patchValue(true);
                    this.closureStatus = this.translation.translate('TableHeaders.PartialClosure');
                }
                if (this.isEditing()) {
                    this.restoreTaxiwayValues();
                }
                this.taxiwaySelected = this.taxiways.find(function (x) { return x.id === val; });
                this.fullClosureChange();
                this.stopEditing();
            }
        }
    };
    TwyClsdComponent.prototype.startClosureChange = function (i) {
        if (this.isFormLoadingData || this.isReadOnly)
            return;
        this.enableEditing();
    };
    TwyClsdComponent.prototype.startClosureChangeFr = function (i) {
        if (this.isFormLoadingData || this.isReadOnly)
            return;
        this.enableEditing();
    };
    TwyClsdComponent.prototype.endClosureChange = function (i) {
        if (this.isFormLoadingData || this.isReadOnly)
            return;
        this.enableEditing();
    };
    TwyClsdComponent.prototype.endClosureChangeFr = function (i) {
        if (this.isFormLoadingData || this.isReadOnly)
            return;
        this.enableEditing();
    };
    TwyClsdComponent.prototype.getSelectedTaxiwayInfo = function () {
        var _this = this;
        if (this.taxiwaySelected) {
            if (this.taxiwaySelected.id === '-1') {
                return this.taxiways.find(function (t) { return t.id === '-1'; });
            }
            else if (this.taxiwaySelected.id === '0') {
                return this.taxiways.find(function (t) { return t.id === '0'; });
            }
            else {
                var twy = this.taxiwaysSelected.find(function (t) { return t.id === _this.taxiwaySelected.id; });
                return (!twy) ? this.taxiways.find(function (t) { return t.id === _this.taxiwaySelected.id; }) : twy;
                //if (!twy) return this.taxiways.find(t => t.id === this.taxiwaySelected.id);
                //else return twy;
            }
        }
        return null;
    };
    //Validation functions
    TwyClsdComponent.prototype.validaClosureFields = function () {
        if (this.validateStartClosure() && this.validateStartClosureFr() && this.validateEndClosure() && this.validateEndClosureFr())
            return true;
        return false;
    };
    TwyClsdComponent.prototype.validateStartClosure = function () {
        if (this.isFormLoadingData)
            return true;
        if (!this.taxiwaySelected)
            return true;
        var taxiway = this.getSelectedTaxiwayInfo();
        if (!taxiway)
            return true;
        var ctrl = this.$ctrl('startClosure');
        if (taxiway.id === '0') {
            if (ctrl.value === '')
                return true;
        }
        else {
            var fullCtrl = this.$ctrl('fullClosure');
            if (fullCtrl.value === true) {
                if (ctrl.value === '')
                    return true;
            }
            else {
                if (ctrl.value !== '')
                    return true;
            }
        }
        return false;
    };
    TwyClsdComponent.prototype.validateStartClosureFr = function () {
        if (this.isFormLoadingData)
            return true;
        if (!this.taxiwaySelected)
            return true;
        var taxiway = this.getSelectedTaxiwayInfo();
        if (!taxiway)
            return true;
        var ctrl = this.$ctrl('startClosureFr');
        if (this.isBilingualRegion) {
            if (taxiway.id === '0') {
                if (ctrl.value === '')
                    return true;
            }
            else {
                var fullCtrl = this.$ctrl('fullClosure');
                if (fullCtrl.value === true) {
                    if (ctrl.value === '')
                        return true;
                }
                else {
                    if (ctrl.value !== '')
                        return true;
                }
            }
        }
        else {
            if (ctrl.value === '')
                return true;
        }
        return false;
    };
    TwyClsdComponent.prototype.validateEndClosure = function () {
        if (this.isFormLoadingData)
            return true;
        if (!this.taxiwaySelected)
            return true;
        var taxiway = this.getSelectedTaxiwayInfo();
        if (!taxiway)
            return true;
        var ctrl = this.$ctrl('endClosure');
        if (taxiway.id === '0') {
            if (ctrl.value === '')
                return true;
        }
        else {
            var fullCtrl = this.$ctrl('fullClosure');
            if (fullCtrl.value === true) {
                if (ctrl.value === '')
                    return true;
            }
            else {
                if (ctrl.value !== '')
                    return true;
            }
        }
        return false;
    };
    TwyClsdComponent.prototype.validateEndClosureFr = function () {
        if (this.isFormLoadingData)
            return true;
        if (!this.taxiwaySelected)
            return true;
        var taxiway = this.getSelectedTaxiwayInfo();
        if (!taxiway)
            return true;
        var ctrl = this.$ctrl('endClosureFr');
        if (this.isBilingualRegion) {
            if (taxiway.id === '0') {
                if (ctrl.value === '')
                    return true;
            }
            else {
                var fullCtrl = this.$ctrl('fullClosure');
                if (fullCtrl.value === true) {
                    if (ctrl.value === '')
                        return true;
                }
                else {
                    if (ctrl.value !== '')
                        return true;
                }
            }
        }
        else {
            if (ctrl.value === '')
                return true;
        }
        return false;
    };
    TwyClsdComponent.prototype.validateSubjectId = function () {
        var fc = this.$ctrl('subjectId');
        return fc.value || this.nsdFrmGroup.pristine;
    };
    TwyClsdComponent.prototype.isRequiredReasonFreeText = function () {
        var reasonIdCtrl = this.$ctrl('grpClosure.closureReasonId');
        if (reasonIdCtrl.value === -1)
            return false;
        var elem = this.twyClsdReasons.find(function (x) { return x.id === reasonIdCtrl.value; });
        if (elem !== undefined && elem !== null) {
            var reasonValueText = elem.text.toLowerCase();
            if ((reasonValueText === 'other') || (reasonValueText === 'autre'))
                return true;
        }
        return false;
    };
    TwyClsdComponent.prototype.validateReasonFreeText = function () {
        var reasonTextCtrl = this.$ctrl('grpClosure.closureReasonFreeText');
        var text = reasonTextCtrl.value;
        if (this.isRequiredReasonFreeText()) {
            if (text === null)
                return false;
            if (text.trim().length > 0)
                return true;
        }
        else {
            if (text === null)
                return true;
            if (text.trim().length === 0)
                return true;
        }
        return false;
    };
    TwyClsdComponent.prototype.validateReasonFreeTextFr = function () {
        if (!this.isBilingualRegion)
            return true;
        var reasonTextCtrlFr = this.$ctrl('grpClosure.closureReasonFreeTextFr');
        var text = reasonTextCtrlFr.value;
        if (this.isRequiredReasonFreeText()) {
            if (text === null)
                return false;
            if (text.trim().length > 0)
                return true;
        }
        else {
            if (text === null)
                return true;
            if (text.trim().length === 0)
                return true;
        }
        return false;
    };
    TwyClsdComponent.prototype.validateTaxiway = function (twy) {
        if (!twy.selected)
            return null;
        if (twy.full) {
            if (twy.start.length === 0 && twy.startFr.length === 0 && twy.end.length === 0 && twy.endFr.length === 0)
                return null;
            else
                return { fullError: { valid: false } };
        }
        else {
            if (twy.start.length === 0 || twy.end.length === 0)
                return { partialError: { valid: false } };
            else {
                if (this.isBilingualRegion) {
                    if (twy.startFr.length === 0 || twy.endFr.length === 0)
                        return { partialError: { valid: false } };
                    else
                        return null;
                }
                else
                    return null;
            }
        }
    };
    TwyClsdComponent.prototype.validateAllTaxiways = function () {
        if (!this.taxiways || !this.taxiwaysSelected)
            return { selectedTaxiways: { valid: false } };
        var error = null;
        if (this.taxiways.length === 0)
            return { allTaxiways: { valid: false } };
        if (this.taxiwaysSelected.length === 0)
            return { allTaxiways: { valid: false } };
        for (var i = 0; i < this.taxiwaysSelected.length; i++) {
            error = this.validateTaxiway(this.taxiwaysSelected[i]);
            if (error !== null) {
                i = this.taxiwaysSelected.length + 1;
            }
        }
        return error;
    };
    TwyClsdComponent.prototype.validateAndTrigger = function () {
        var error = this.validateAllTaxiways();
        if (error) {
            this.nsdForm.controls['frmArr'].setErrors(null);
            this.nsdForm.controls['frmArr'].setErrors(error);
            var ctrl = this.$ctrl('countSelectedTwys');
            ctrl.setValue(0);
        }
        else {
            if (!this.validateReasonFreeText() || !this.validateReasonFreeTextFr()) {
                error = { closureReasonNeeded: { valid: false } };
                this.nsdForm.controls['frmArr'].setErrors(null);
                this.nsdForm.controls['frmArr'].setErrors(error);
                var ctrl = this.$ctrl('countSelectedTwys');
                ctrl.setValue(0);
            }
            else {
                this.nsdForm.controls['frmArr'].setErrors(null);
                var ctrl = this.$ctrl('countSelectedTwys');
                if (ctrl.value === 0)
                    ctrl.setValue(this.countSelectedTwys);
                ctrl = this.$ctrl('allTaxiways');
                if (ctrl.value !== this.taxiwaysSelected)
                    ctrl.setValue(this.taxiwaysSelected);
            }
        }
    };
    Object.defineProperty(TwyClsdComponent.prototype, "countSelectedTwys", {
        // Standard general function
        get: function () {
            return this.taxiwaysSelected.length;
        },
        enumerable: false,
        configurable: true
    });
    /*
    private markFrmDirty() {
        this.nsdForm.controls['frmArr'].markAsDirty();
    }
    */
    TwyClsdComponent.prototype.updateMap = function (sdoSubject, acceptSubjectLocation) {
        var _this = this;
        this.leafletmap.clearFeaturesOnLayers(['DOA']);
        if (sdoSubject.subjectGeoRefPoint) {
            var geojson = JSON.parse(sdoSubject.subjectGeoRefPoint);
            if (geojson.features.length === 1) {
                var subLocation = this.leafletmap.geojsonToTextCordinates(geojson);
                var coordinates = geojson.features[0].geometry.coordinates;
                var markerGeojson_1 = acceptSubjectLocation ? geojson : this.getGeoJsonFromLocation(this.token.subjectLocation);
                if (acceptSubjectLocation) {
                    this.nsdFrmGroup.patchValue({
                        subjectLocation: subLocation,
                        latitude: coordinates[1],
                        longitude: coordinates[0],
                    }, { onlySelf: true, emitEvent: false });
                }
                this.leafletmap.addMarker(geojson, sdoSubject.name, function () {
                    _this.leafletmap.addFeature(sdoSubject.subjectGeo, sdoSubject.name, function () {
                        if (markerGeojson_1) {
                            _this.leafletmap.setNewMarkerLocation(markerGeojson_1);
                        }
                    });
                });
            }
        }
    };
    TwyClsdComponent.prototype.setFirstSelectItemInCombo = function (items, itemId, insertUndefinedAtZero) {
        if (insertUndefinedAtZero === void 0) { insertUndefinedAtZero = true; }
        if (this.isFormLoadingData) {
            var index = items.findIndex(function (x) { return x.id === itemId; });
            if (index > 0) { //move selected item to position 0
                items.splice(0, 0, items.splice(index, 1)[0]);
            }
        }
        else { //insert undefined item at position 0
            if (insertUndefinedAtZero) {
                items.unshift({
                    id: -1,
                    text: '',
                });
            }
        }
    };
    TwyClsdComponent.prototype.setFirstSelectItemInTaxiways = function (items, itemId, insertUndefinedAtZero) {
        if (insertUndefinedAtZero === void 0) { insertUndefinedAtZero = true; }
        if (this.isFormLoadingData) {
            var index = items.findIndex(function (x) { return x.id === itemId; });
            if (index > 0) { //move selected item to position 0
                items.splice(0, 0, items.splice(index, 1)[0]);
            }
        }
        else { //insert undefined item at position 0
            if (insertUndefinedAtZero) {
                items.unshift({
                    id: "-1",
                    text: '',
                });
            }
        }
    };
    TwyClsdComponent.prototype.loadMapLocations = function (doaId, loadingFromProposal) {
        var _this = this;
        var self = this;
        this.dataService.getDoaLocation(doaId)
            .subscribe(function (geoJsonStr) {
            var interval = window.setInterval(function () {
                if (self.leafletmap && self.leafletmap.isReady()) {
                    window.clearInterval(interval);
                    self.leafletmap.addDoa(geoJsonStr, 'DOA');
                    if (loadingFromProposal) {
                        self.updateMap(self.subjectSelected, false);
                    }
                }
                ;
            }, 100);
        }, function (error) {
            _this.toastr.error(_this.translation.translate('Nsd.DOAArea') + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    TwyClsdComponent.prototype.setBilingualRegion = function (value) {
        this.token.isBilingual = value;
        this.isBilingualRegion = value;
        if (this.isBilingualRegion) {
            if (!this.isReadOnly) {
                var ctrl = this.$ctrl('startClosureFr');
                ctrl.enable();
                ctrl = this.$ctrl('endClosureFr');
                ctrl.enable();
            }
        }
        else {
            if (!this.isReadOnly) {
                var ctrl = this.$ctrl('startClosureFr');
                ctrl.disable();
                ctrl = this.$ctrl('endClosureFr');
                ctrl.disable();
            }
        }
    };
    TwyClsdComponent.prototype.getGeoJsonFromLocation = function (value) {
        var location = this.locationService.parse(value);
        return location ? this.leafletmap.latitudeLongitudeToGeoJson(location.latitude, location.longitude) : null;
    };
    TwyClsdComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/dashboard/nsds/twyclsd/twyclsd.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, core_1.Injector,
            forms_1.FormBuilder,
            location_service_1.LocationService,
            twyclsd_service_1.TwyClsdService,
            data_service_1.DataService,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], TwyClsdComponent);
    return TwyClsdComponent;
}());
exports.TwyClsdComponent = TwyClsdComponent;
function countSelectedTwysValidator(c) {
    if (c.value === 0) {
        return { 'required': true };
    }
    return null;
}
function refreshIcaoValidator(c) {
    if (c.value === 0) {
        return { 'required': true };
    }
    return null;
}
function finalClosureValidator(twyClsdReasons, bilingual) {
    return function (c) {
        var cr = c.get('closureReasonId');
        var crT = c.get('closureReasonFreeText');
        var crTFr = c.get('closureReasonFreeTextFr');
        if (cr.value === 0)
            return null;
        var elem = twyClsdReasons.find(function (x) { return x.id === cr.value; });
        if (elem !== undefined && elem !== null) {
            var reasonValueText = elem.text.toLowerCase();
            var text = crT.value;
            var textFr = crTFr.value;
            if ((reasonValueText === 'other') || (reasonValueText === 'autre')) {
                if (text === null)
                    return { 'missing_reason': true };
                if (text.trim().length === 0)
                    return { 'missing_reason': true };
                if (bilingual) {
                    if (textFr === null)
                        return { 'missing_reason_fr': true };
                    if (textFr.trim().length === 0)
                        return { 'missing_reason_fr': true };
                }
            }
            else {
                if (text !== null && text.trim().length > 0)
                    return { 'invalid_reason': true };
                if (textFr !== null && textFr.trim().length > 0)
                    return { 'invalid_reason_fr': true };
            }
        }
        return null;
    };
}
//# sourceMappingURL=twyclsd.component.js.map
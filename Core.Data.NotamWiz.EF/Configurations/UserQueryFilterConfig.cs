﻿namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    using Domain.Model.Entitities;

    public class UserQueryFilterConfig : EntityTypeConfiguration<UserQueryFilter>
    {
        public UserQueryFilterConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(e => e.FilterName).IsRequired();
            Property(e => e.Username).IsRequired();
            Property(e => e.SlotNumber).IsRequired();
            Property(e => e.UserFilterType).IsRequired();
            Property(e => e.JsonBundle).IsRequired();
        }
    }
}



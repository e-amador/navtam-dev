"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserViewHistoryComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var data_service_1 = require("../shared/data.service");
var nsd_form_component_1 = require("../nsds/nsd-form.component");
var UserViewHistoryComponent = /** @class */ (function () {
    function UserViewHistoryComponent(dataService, activatedRoute, router) {
        this.dataService = dataService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.model = null;
    }
    Object.defineProperty(UserViewHistoryComponent.prototype, "form", {
        set: function (v) {
            this.nsdForm = v;
        },
        enumerable: false,
        configurable: true
    });
    UserViewHistoryComponent.prototype.ngOnInit = function () {
        window['loadLeafletMapScript'].call(this, null);
        this.model = this.activatedRoute.snapshot.data['model'];
    };
    __decorate([
        core_1.ViewChild(nsd_form_component_1.NsdFormComponent),
        __metadata("design:type", nsd_form_component_1.NsdFormComponent),
        __metadata("design:paramtypes", [nsd_form_component_1.NsdFormComponent])
    ], UserViewHistoryComponent.prototype, "form", null);
    UserViewHistoryComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/dashboard/user/user-viewhistory.component.html'
        }),
        __metadata("design:paramtypes", [data_service_1.DataService,
            router_1.ActivatedRoute,
            router_1.Router])
    ], UserViewHistoryComponent);
    return UserViewHistoryComponent;
}());
exports.UserViewHistoryComponent = UserViewHistoryComponent;
//# sourceMappingURL=user-viewhistory.component.js.map
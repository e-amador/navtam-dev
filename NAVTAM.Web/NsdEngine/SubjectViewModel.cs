﻿using NavCanada.Core.Domain.Model.Entitities;

namespace NAVTAM.NsdEngine
{
    public class SubjectViewModel
    {
        public string SubjectId { get; set; }
        public string Designator { get; set; }
        public string Fir { get; set; }
        public string Ad { get; set; }
        public double SubjectGeoRefLong { get; set; }
        public double SubjectGeoRefLat { get; set; }
        public string SubjectMid { get; set; }
        public SdoCache Subject { get; set; }
    }
}
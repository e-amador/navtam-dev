﻿namespace NavCanada.Applications.NotamWiz.Web.Code.Configurations.AutoMapperResolvers
{
    using System;

    using AutoMapper;

    using Models;
    using Core.Domain.Model.Entitities;
    using Core.Domain.Model.Enums;

    public class Notam2VmConvertor : ITypeConverter<Notam, NotamViewModel>
    {
        public NotamViewModel Convert(ResolutionContext context)
        {

            Notam n = context.SourceValue as Notam;

            if (n == null) return null;

            NotamViewModel nvm = new NotamViewModel()
            {
                Id = n.Id,
                Nof = n.Nof,
                Series = n.Series,
                Number = n.Number,
                Year = n.Year,
                Type = n.Type.ToString(),
                ReferredSeries = n.ReferredSeries,
                ReferredNumber = n.ReferredNumber,
                ReferredYear = n.ReferredYear,
                Fir = n.Fir,
                Code23 = n.Code23,
                Code45 = n.Code45,
                Traffic = n.Traffic.ToString(),  
                Purpose = n.Purpose.ToString(),
                Scope = n.Scope.ToString(),
                Lower = n.Lower,
                Upper = n.Upper,
                Coordinates = n.Coordinates,
                Radius = n.Radius,
                ItemA = n.ItemA,
                StartValidity = n.StartValidity,
                EndValidity = n.EndValidity,
                Estimated = n.Estimated,
                ItemD = n.ItemD,
                ItemE = n.ItemE,
                ItemEFrench = n.ItemEFrench,
                ItemF = n.ItemF,
                ItemG = n.ItemG,
                ItemX = n.ItemX,
                Operator = n.Operator                
            };


            return nvm;
        }
    }

    public class Notamvm2MConvertor : ITypeConverter<NotamViewModel, Notam>
    {
        public Notam Convert(ResolutionContext context)
        {

            NotamViewModel nv = context.SourceValue as NotamViewModel;

            if (nv == null) return null;

            Notam n = new Notam()
            {
                Id = nv.Id,
                Nof = nv.Nof,
                Series = nv.Series,
                Number = nv.Number,
                Year = nv.Year,
                Type = (NotamType) Enum.Parse(typeof(NotamType), nv.Type),
                ReferredSeries = nv.ReferredSeries,
                ReferredNumber = nv.ReferredNumber,
                ReferredYear = nv.ReferredYear,                
                Fir = nv.Fir,
                Code23 = nv.Code23,
                Code45 = nv.Code45,
                Traffic = (TrafficType) Enum.Parse(typeof(TrafficType), nv.Traffic),
                Purpose = (PurposeType) Enum.Parse(typeof(PurposeType), nv.Purpose),
                Scope = (ScopeType) Enum.Parse(typeof(ScopeType), nv.Scope),
                Lower = nv.Lower,
                Upper = nv.Upper,
                Coordinates = nv.Coordinates,
                Radius = nv.Radius,
                ItemA = nv.ItemA,
                StartValidity = nv.StartValidity,
                EndValidity = nv.EndValidity,
                Estimated = nv.Estimated,
                ItemD = nv.ItemD,
                ItemE = nv.ItemE,
                ItemEFrench = nv.ItemEFrench,
                ItemF = nv.ItemF,
                ItemG = nv.ItemG,
                ItemX = nv.ItemX,
                Operator = nv.Operator                
            };

            return n;
        }
    }
}
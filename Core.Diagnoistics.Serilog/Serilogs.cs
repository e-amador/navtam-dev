﻿using System;
using System.Configuration;
using System.Threading;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using Log = Serilog.Log;

namespace Core.Diagnoistics.SeriLog
{
    /// <summary>
    /// FYIS
    /// @ forces serialization
    /// $ forces stringified serialization
    /// </summary>
    public class Serilogs : NavCanada.Core.Common.Contracts.ILogger
    {
        static Serilogs()
        {
            lock (typeof(Serilogs))
            {
                InitSerilogs();
            }
        }

        public void ChangeLoggingLevel(int logLevel)
        {
            if (!Log.IsEnabled((LogEventLevel)logLevel))
            {
                lock (typeof(Log))
                {
                    Log.Logger.Warning("Closing and changing log level to {}", (LogEventLevel)logLevel);

                    Log.CloseAndFlush();
                    Thread.Sleep(1000);

                    Log.Logger = Log.Logger = new LoggerConfiguration()
                        .MinimumLevel.Is((LogEventLevel)logLevel)
                        .WriteTo.MSSqlServer(
                            ConfigurationManager.ConnectionStrings["LogConnection"].ConnectionString,
                            "Log", (LogEventLevel)logLevel, 50, null, autoCreateSqlTable: true)
                        //.WriteTo.Seq(ConfigurationManager.AppSettings["SeqEndpoint"])
                        .CreateLogger();

                    Log.Logger.Information("Logger initialized");
                }
            }
        }

        private static void InitSerilogs()
        {
            LoggingLevelSwitch levelSwitch = new LoggingLevelSwitch(LogEventLevel.Verbose);

            Log.Logger = new LoggerConfiguration()
                 .MinimumLevel.ControlledBy(levelSwitch)
                 .WriteTo.MSSqlServer(
                 ConfigurationManager.ConnectionStrings["LogConnection"].ConnectionString,
                 "Log", LogEventLevel.Verbose, 50, null, autoCreateSqlTable: true)
                 //.WriteTo.Seq(ConfigurationManager.AppSettings["SeqEndpoint"])
                 .CreateLogger();

            Log.Logger.Information("Log initialized");
        }

        public void LogDebug(string message)
        {
            if (Log.IsEnabled(LogEventLevel.Debug))
                Log.Logger.Debug(message);
        }

        public void LogDebug(Type type, string message)
        {
            if (Log.IsEnabled(LogEventLevel.Debug))
                Log.Logger.Debug($"{@type} {message}", type, message);
        }

        public void LogWarn(Type type, string message, object context)
        {
            if (Log.IsEnabled(LogEventLevel.Warning))
                Log.Logger.Warning($"{@type} .. {0} .. {context}", type, message, context);
        }

        public void LogErrorWithContext(Exception ex, Type type, string message, object context)
        {
            //if (Log.IsEnabled(LogEventLevel.Error))
            Log.Logger.Error(ex, $"{message} .. {@type}", message, type, context);
        }

        public void LogError(Type type, string message)
        {
            //if (Log.IsEnabled(LogEventLevel.Error))
            Log.Logger.Error($"{@type} {message}", type, message);
        }

        public void LogError(Type type, object context)
        {
            //if (Log.IsEnabled(LogEventLevel.Error))
            Log.Logger.Error($"{@type} {@context}", type, context);
        }

        public void LogError(Type type, Exception ex)
        {
            //if (Log.IsEnabled(LogEventLevel.Error))
            Log.Logger.Error(ex, $"{@type} {@ex}", type, ex);
        }

        public void LogError(Type type, string message, Exception ex)
        {
            Log.Logger.Error(ex, $"Class: {@type}. Message: {message}, Exception: {@ex}", type, message);
        }

        public void LogFatal(Type type, Exception ex)
        {
            Log.Logger.Fatal($"{@type}, {@ex}", type, ex);
        }

        public void LogInformationalWithContext(Type type, string message, object context)
        {
            if (Log.IsEnabled(LogEventLevel.Information))
                Log.Logger.Information($"{@type} {message} {@context}", type, message, context);
        }

        public void LogInformational(Type type, string message)
        {
            if (Log.IsEnabled(LogEventLevel.Information))
                Log.Logger.Information($"{@type} {message}", type, message);
        }

        public void LogWarn(Type type, string v)
        {
            throw new NotImplementedException();
        }

        public void UserLogin(string Name, string UserId, string OrganizationName, int OrganizationId, int DoaId)
        {
            Log.Logger.Write(LogEventLevel.Information, "User {@Name} logged in. UserId: {@UserId}, OrganizationName: {@OrganizationName}, OrganizationId: {@OrganizationId}, DoaId: {@DoaId}", Name, UserId, OrganizationName, OrganizationId, DoaId);
        }

        public void InvalidLogin(Type type, string username)
        {
            Log.Logger.Write(LogEventLevel.Warning, "Invalid user login {@username} in class {@type}", username, type);
        }

        public void LogError(Type type, string message, object obj)
        {
            throw new NotImplementedException();
        }
    }
}
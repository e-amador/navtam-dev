﻿export class PhoneValidator {

    static isValidPhoneNumber(value: string, length: number) {
        value = this.stripFormat(value);
        return value && (!length || value.length == length) && !isNaN(+value);
    }

    static formatTenDigits(value: string): string {
        value = this.stripFormat(value);

        if (!value || value.length != 10)
            return value;

        const area = value.substring(0, 3);
        const block1 = value.substring(3, 6);
        const block2 = value.substring(6, 10);

        return `(${area}) ${block1}-${block2}`;
    }

    static stripFormat(value: string) {
        if (!value)
            return value;

        const numbers = [];
        for (var i = 0; i < value.length; i++)
            if (this.isNumber(value[i]))
                numbers.push(value[i]);

        return numbers.join('');
    }

    static isNumber(c) {
        return c >= '0' && c <= '9';
    }
}
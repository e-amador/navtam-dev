"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ADCEditComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var windowRef_service_1 = require("../../common/windowRef.service");
var forms_1 = require("@angular/forms");
var angular_l10n_1 = require("angular-l10n");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var toastr_service_1 = require("./../../common/toastr.service");
var data_service_1 = require("../shared/data.service");
var location_service_1 = require("../../dashboard/shared/location.service");
var ADCEditComponent = /** @class */ (function () {
    function ADCEditComponent(toastr, fb, dataService, memStorageService, router, activatedRoute, winRef, locale, translation, geoLocation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.winRef = winRef;
        this.locale = locale;
        this.translation = translation;
        this.geoLocation = geoLocation;
        this.model = null;
        this.sdoModel = null;
        this.disseminationCategories = [];
        this.selectedDisseminationCategory = '-1';
        this.isReadOnly = false;
        this.isSubmitting = false;
        if (this.winRef.appConfig.ccs)
            this.router.navigate(['/administration']);
    }
    ADCEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.disseminationCategoriesOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('ADCSession.SelectDissCat')
            },
            width: "100%"
        };
        this.action = this.activatedRoute.snapshot.data[0].action;
        this.model = this.activatedRoute.snapshot.data['model'];
        this.isReadOnly = this.action === "view";
        this.loadDissCategory();
        this.configureReactiveForm();
        this.dataService.getAhpModel(this.model.ahpMid)
            .subscribe(function (sdo) {
            _this.sdoModel = sdo;
            if (_this.model.ahpCodeId !== _this.sdoModel.designator)
                _this.model.ahpCodeId = _this.sdoModel.designator;
            if (_this.model.ahpName !== _this.sdoModel.name)
                _this.model.ahpName = _this.sdoModel.name;
            if (_this.model.latitude !== _this.sdoModel.latitude)
                _this.model.latitude = _this.sdoModel.latitude;
            if (_this.model.longitude !== _this.sdoModel.longitude)
                _this.model.longitude = _this.sdoModel.longitude;
        });
    };
    ADCEditComponent.prototype.backToADCList = function () {
        this.router.navigate(['/administration/adc-tab']);
    };
    ADCEditComponent.prototype.disableSubmit = function () {
        if (this.addAdcForm.pristine)
            return true;
        if (!this.addAdcForm.valid)
            return true;
        if (this.isSubmitting)
            return true;
        return false;
    };
    ADCEditComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.isSubmitting)
            return;
        this.isSubmitting = true;
        var adcToSave = this.prepareADC();
        this.dataService.updateADC(adcToSave)
            .subscribe(function (data) {
            var msg = _this.translation.translate('ADCSession.SuccessADCCreated');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            _this.router.navigate(["/administration/adc-tab"]);
        }, function (error) {
            _this.isSubmitting = false;
            var msg = _this.translation.translate('ADCSession.FailureADCCreated');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    ADCEditComponent.prototype.getLatitude = function (lat) {
        return this.geoLocation.convertLatToDMS(lat);
    };
    ADCEditComponent.prototype.getLongitude = function (long) {
        return this.geoLocation.convertLongToDMS(long);
    };
    ADCEditComponent.prototype.configureReactiveForm = function () {
        this.addAdcForm = this.fb.group({
            designator: [{ value: this.model.ahpCodeId, disabled: true }, []],
            name: [{ value: this.model.ahpName, disabled: true }, []],
            disseminationCategory: [{ value: String(this.model.disseminationCategory), disabled: this.isReadOnly }, [validateDisseminationCategory]],
            disseminationCategoryName: [{ value: this.model.disseminationCategoryName, disabled: this.isReadOnly }, [forms_1.Validators.required]],
        });
    };
    ADCEditComponent.prototype.prepareADC = function () {
        var adc = {
            id: this.model.id,
            ahpCodeId: this.model.ahpCodeId,
            ahpMid: this.model.ahpMid,
            ahpName: this.model.ahpName,
            latitude: this.model.latitude,
            longitude: this.model.longitude,
            disseminationCategory: this.addAdcForm.get('disseminationCategory').value,
            disseminationCategoryName: this.addAdcForm.get('disseminationCategoryName').value.toUpperCase(),
            servedCity: "",
        };
        return adc;
    };
    ADCEditComponent.prototype.validateDissemination = function () {
        var fc = this.addAdcForm.get('disseminationCategory');
        if (validateDisseminationCategory(fc) !== null)
            return false;
        return true;
    };
    ADCEditComponent.prototype.onDisseminationChanged = function (data) {
        if (data.value) {
            var diss = this.disseminationCategories.find(function (x) { return x.id === String(data.value); });
            if (diss) {
                this.addAdcForm.get('disseminationCategory').setValue(diss.id);
                this.addAdcForm.get('disseminationCategoryName').setValue(diss.text);
            }
            else {
                this.addAdcForm.get('disseminationCategory').setValue('-1');
                this.addAdcForm.get('disseminationCategoryName').setValue('');
            }
        }
        else {
            this.addAdcForm.get('disseminationCategory').setValue('-1');
            this.addAdcForm.get('disseminationCategoryName').setValue('');
        }
        if (data.value !== '-1' && this.selectedDisseminationCategory !== this.addAdcForm.get('disseminationCategory').value) {
            this.addAdcForm.markAsDirty();
        }
    };
    ADCEditComponent.prototype.loadDissCategory = function () {
        this.disseminationCategories = [
            { id: '-1', text: '' },
            { id: '0', text: 'National' },
            { id: '1', text: 'US' },
            { id: '2', text: 'International' },
        ];
        this.selectedDisseminationCategory = String(this.model.disseminationCategory) || '-1';
    };
    ADCEditComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/adc/adc-edit.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            router_1.ActivatedRoute,
            windowRef_service_1.WindowRef,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService,
            location_service_1.LocationService])
    ], ADCEditComponent);
    return ADCEditComponent;
}());
exports.ADCEditComponent = ADCEditComponent;
function validateDisseminationCategory(c) {
    if (c.value === '-1') {
        return { 'required': true };
    }
    return null;
}
//# sourceMappingURL=adc-edit.component.js.map
﻿namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    using System.Data.Entity.ModelConfiguration;

    using Domain.Model.Entitities;

    public class PoisonMtoConfig : EntityTypeConfiguration<PoisonMto>
    {
        public PoisonMtoConfig()
        {
            HasKey(e => e.Id);
        }
    }
}

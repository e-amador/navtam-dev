﻿import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'

import { DataService } from '../shared/data.service'

import { NsdFormComponent } from '../nsds/nsd-form.component'

@Component({ 
    templateUrl: '/app/dashboard/nof/nof-create.component.html'
})
export class NofCreateComponent implements OnInit, OnDestroy  {
    
    model : any = null;
    nsdForm : NsdFormComponent;

    @ViewChild(NsdFormComponent)
    set form(v: NsdFormComponent){
        this.nsdForm = v;
    }

    constructor(
        private dataService : DataService, 
        private activatedRoute: ActivatedRoute, 
        private router: Router) {
    }

    ngOnInit() {
        window['loadLeafletMapScript'].call(this, null);
        this.model = this.activatedRoute.snapshot.data['model'];
    }

    ngOnDestroy() {
        if (window["app"].leafletmap) {
            window["app"].leafletmap.dispose();
        }
    }

    get langCulture() {
        return (window["app"].cultureInfo || "en").toUpperCase();
    }   
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NAVTAM.NsdEngine.NAVAIDS
{
    public class NavaidFacilitator
    {
        public NavaidFacilitator(string name, string eFieldName)
        {
            Name = name;
            EFieldName = eFieldName;
        }

        public string Id { get; }
        public string Name { get; }
        public string EFieldName { get; }
    }

    public class RwyClsdReasons
    {
        public static Dictionary<string, NavaidFacilitator> Get(string language) => _dictionary[language];

        public Dictionary<string, NavaidFacilitator> this[string language] => _dictionary[language];

        public static bool HasType(string type)
        {
            return _dictionary["en"].ContainsKey(type);
        }

        static readonly Dictionary<string, Dictionary<string, NavaidFacilitator>> _dictionary = new Dictionary<string, Dictionary<string, NavaidFacilitator>>
        {
            {
                "en", new Dictionary<string, NavaidFacilitator>
                {
                    { "Re1", BuildReasonTypeEN("Unserviceable", "U/S") },
                    { "Re2", BuildReasonTypeEN("Unmonitored", "UNMONITORED") },
                    { "Re3", BuildReasonTypeEN("Operating at 50% Power or Less", "OPR AT 50 PCT PWR OR LESS") },
                    { "Re4", BuildReasonTypeEN("AZM U/S DME AVBL", "AZM U/S DME AVBL") },
                    { "Re5", BuildReasonTypeEN("DME U/S AZM AVBL", "DME U/S AZM AVBL") },
                    { "Re6", BuildReasonTypeEN("DME U/S AZM UNMONITORED", "DME U/S AZM UNMONITORED") },
                    { "Re7", BuildReasonTypeEN("AZM U/S DME UNMONITORED", "AZM U/S DME UNMONITORED") },                    
                }
            },
            {
                "fr", new Dictionary<string, NavaidFacilitator>
                {
                    { "Re1", BuildReasonTypeEN("Unserviceable", "U/S") },
                    { "Re2", BuildReasonTypeEN("Unmonitored", "UNMONITORED") },
                    { "Re3", BuildReasonTypeEN("Operating at 50% Power or Less", "OPR AT 50 PCT PWR OR LESS") },
                    { "Re4", BuildReasonTypeEN("AZM U/S DME AVBL", "AZM U/S DME AVBL") },
                    { "Re5", BuildReasonTypeEN("DME U/S AZM AVBL", "DME U/S AZM AVBL") },
                    { "Re6", BuildReasonTypeEN("DME U/S AZM UNMONITORED", "DME U/S AZM UNMONITORED") },
                    { "Re7", BuildReasonTypeEN("AZM U/S DME UNMONITORED", "AZM U/S DME UNMONITORED") },
                }
            }
        };

        private static NavaidFacilitator BuildReasonTypeEN(string name, string eFieldName)
        {
            return new NavaidFacilitator(name, eFieldName);
        }

        private static NavaidFacilitator BuildReasonTypeFR(string name, string eFieldName)
        {
            return new NavaidFacilitator(name, eFieldName);
        }
    }
}
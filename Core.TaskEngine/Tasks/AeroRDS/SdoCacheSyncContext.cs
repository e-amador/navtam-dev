﻿using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using System;
using System.Collections.Generic;

namespace NavCanada.Core.TaskEngine.Tasks.AeroRDS
{
    internal class SdoCacheSyncContext
    {
        public SdoCacheSyncContext(IUow uow)
        {
            Uow = uow;
            _firs = new Dictionary<string, SdoCache>();
            _aerodromes = new Dictionary<string, SdoCache>();
            _runways = new Dictionary<string, SdoCache>();
            _runwayDirections = new Dictionary<string, SdoCache>();
            _runwayDirectionDeclaredDistances = new Dictionary<string, SdoCache>();
        }

        public IUow Uow { get; set; }

        #region Firs

        public void AddFir(SdoCache fir) => _firs.Add(fir.Id, fir);

        public void AddFirs(IEnumerable<SdoCache> firs) => AddMany(firs, AddFir);

        public SdoCache TryGetFir(string id) => TryGet(_firs, id);

        public void ClearFirs() => _firs.Clear();

        #endregion Firs

        #region Aerodromes

        public void AddAerodrome(SdoCache aerodrome) => _aerodromes.Add(aerodrome.Id, aerodrome);

        public void AddAerodromes(IEnumerable<SdoCache> aerodromes) => AddMany(aerodromes, AddAerodrome);

        public SdoCache TryGetAerodrome(string id) => TryGet(_aerodromes, id);

        public void ClearAerodromes() => _aerodromes.Clear();

        #endregion Aerodromes

        #region Runways

        public void AddRunway(SdoCache runway) => _runways.Add(runway.Id, runway);

        public void AddRunways(IEnumerable<SdoCache> runways) => AddMany(runways, AddRunway);

        public SdoCache TryGetRunway(string id) => TryGet(_runways, id);

        public void ClearRunways() => _runways.Clear();

        #endregion Runways

        #region Runway directions

        public void AddRunwayDirection(SdoCache rwd) => _runwayDirections.Add(rwd.Id, rwd);

        public void AddRunwayDirections(IEnumerable<SdoCache> rwds) => AddMany(rwds, AddRunwayDirection);

        public SdoCache TryGetRunwayDirection(string id) => TryGet(_runwayDirections, id);

        public void ClearRunwayDirections() => _runwayDirections.Clear();

        #endregion Runway directions

        #region Runway direction declared distance

        public void AddRunwayDirectionDeclaredDistance(SdoCache rdd) => _runwayDirectionDeclaredDistances.Add(rdd.Id, rdd);

        public void AddRunwayDirectionDeclaredDistances(IEnumerable<SdoCache> rdds) => AddMany(rdds, AddRunwayDirectionDeclaredDistance);

        public SdoCache TryGetRunwayDirectionDeclaredDistance(string id) => TryGet(_runwayDirectionDeclaredDistances, id);

        public void ClearRunwayDirectionDeclaredDistances() => _runwayDirectionDeclaredDistances.Clear();

        #endregion Runway direction declared distance

        private static void AddMany(IEnumerable<SdoCache> items, Action<SdoCache> addFunc)
        {
            foreach (var item in items)
                addFunc.Invoke(item);
        }

        private static SdoCache TryGet(Dictionary<string, SdoCache> dict, string id)
        {
            SdoCache entity = null;
            return !string.IsNullOrEmpty(id) && dict.TryGetValue(id, out entity) ? entity : null;
        }

        private readonly Dictionary<string, SdoCache> _firs;
        private readonly Dictionary<string, SdoCache> _aerodromes;
        private readonly Dictionary<string, SdoCache> _runways;
        private readonly Dictionary<string, SdoCache> _runwayDirections;
        private readonly Dictionary<string, SdoCache> _runwayDirectionDeclaredDistances;
    }
}
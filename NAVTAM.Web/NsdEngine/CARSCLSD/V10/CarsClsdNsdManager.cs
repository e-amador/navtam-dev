﻿using System;
using System.Linq;
using System.Threading.Tasks;
using NAVTAM.ViewModels;
using NavCanada.Core.Domain.Model.Entitities;
using System.Globalization;

namespace NAVTAM.NsdEngine.CARSCLSD.V10
{
    public class CarsClsdNsdManager : NsdManager
    {
        public CarsClsdNsdManager(int categoryId, string categoryName, string version) : base(categoryId, categoryName, version)
        {
        }

        protected override TokenViewModel CreateTokenViewModel()
        {
            return new CarsClsdViewModel();
        }

        protected override TokenViewModel DeserializeToken(string tokens)
        {
            return Deserialize<CarsClsdViewModel>(tokens);
        }

        protected override string GenerateItemA(TokenViewModel token, ProposalContext context)
        {
            return CovertAerodromeDesignatorToItemA(context.SdoCacheSubject().Designator);
        }

        protected override BilingualText GenerateItemE(TokenViewModel token, ProposalContext context)
        {
            var carsCont = context.SdoCacheSubject();
            //var itemEPrefix = GetItemEPrefix(context.AeroRdsProxy, carsCont.Id, carsCont.Designator, carsCont.Name);
            var itemEPrefix = GetItemEAerodromeNamePrefix(context, carsCont.Name);
            var itemE = !context.IsCancelation ? GenerateNonCancelItemE(context, token as CarsClsdViewModel) : GenerateCancelItemE(context, token as CarsClsdViewModel);
            if (itemEPrefix.English != null)
            {
                itemE.English = itemEPrefix.English + itemE.English;
                if (context.IsBilingual) itemE.French = itemEPrefix.French + itemE.French;
            }
            return itemE;

            //return !context.IsCancelation ? GenerateNonCancelItemE(context, token as CarsClsdViewModel) : GenerateCancelItemE(context, token as CarsClsdViewModel);
        }

        private BilingualText GenerateCancelItemE(ProposalContext context, CarsClsdViewModel carsClsdViewModel)
        {
            var itemE = new BilingualText("", "");
            itemE.English = "COMMUNITY AD RADIO STATION (CARS) RESUMED NORMAL OPS.";
            if(context.IsBilingual) itemE.French = "STATION RADIO D'AD COMMUNAUTAIRE (CARS) RETOUR A LA NORMALE.";
            return itemE;
        }

        private BilingualText GenerateNonCancelItemE(ProposalContext context, CarsClsdViewModel carsClsdViewModel)
        {
            var itemE = GetAmendPubMessagePrefix(carsClsdViewModel);
            itemE.English = "COMMUNITY AD RADIO STATION (CARS) ";
            if (carsClsdViewModel.CarsStatusId == 0) // Closed
            {
                itemE.English = itemE.English + "CLSD";
                if (context.IsBilingual) itemE.French = "STATION RADIO D'AD COMMUNAUTAIRE (CARS) CLSD";
            }
            else if(carsClsdViewModel.CarsStatusId == 1)
            {
                itemE.English = itemE.English + "HR OF OPS:\n" + BuildHoursOperationSchema(carsClsdViewModel);
                if (context.IsBilingual)
                    itemE.French = "HR D'OPS DE LA STATION RADIO D'AD COMMUNAUTAIRE (CARS):\n" + 
                        BuildHoursOperationSchema(carsClsdViewModel, true);
            }
            else
            {
                itemE.English = itemE.English + "HR OF OPS EXTENDED";
                if (context.IsBilingual) itemE.French = "HR D'OPS DE LA STATION RADIO D'AD COMMUNAUTAIRE (CARS) PROLONGEES";
            }

            if(carsClsdViewModel.Reason.Length > 0)
            {
                itemE.English = itemE.English + " DUE " + carsClsdViewModel.Reason.ToUpper().Trim() + ".";
                if (context.IsBilingual) itemE.French = itemE.French + " CAUSE " + carsClsdViewModel.ReasonFr.ToUpper().Trim() + ".";
            }
            else
            {
                itemE.English = itemE.English + ".";
                if (context.IsBilingual) itemE.French = itemE.French + ".";
            }

            return itemE;
        }

        private string BuildHoursOperationSchema(CarsClsdViewModel carsClsdViewMode, bool french = false)
        {
            var retValue = "";
            //Defect 103372: Days of the week are not translated to French, in the french version is used the english name of the day
            //
            var days = (!carsClsdViewMode.Everyday)? BuildDays(carsClsdViewMode.DaysOfWeek, false): "";

            var sTime = carsClsdViewMode.StartHour.Trim();
            var eTime = carsClsdViewMode.EndHour.Trim();

            var dt = "";
            if(carsClsdViewMode.DaylightSavingTime)
            {
                int startHour = 0;
                int endHour = 0;

                bool resS = Int32.TryParse(sTime, out startHour);
                bool resE = Int32.TryParse(eTime, out endHour);
                if( resS && resE)
                {
                    var st1 = DateTime.ParseExact(sTime, "HHmm", CultureInfo.InvariantCulture);
                    var st2 = st1.AddHours(-1);

                    var et1 = DateTime.ParseExact(eTime, "HHmm", CultureInfo.InvariantCulture);
                    var et2 = et1.AddHours(-1);

                    dt = st2.Hour.ToString().PadLeft(2, '0') + 
                        st2.Minute.ToString().PadLeft(2, '0') + 
                        "-" + 
                        et2.Hour.ToString().PadLeft(2, '0') + 
                        et2.Minute.ToString().PadLeft(2, '0');
                }
            }

            retValue = days + sTime + "-" + eTime + ((dt.Length > 0)? " (DT " + dt + ")": "");
            return retValue.Trim();
        }

        private string BuildDays(DaysOfOperation dow, bool french = false)
        {
            var days = "";
            if (dow.Monday) days += (french)? "LUN " : "MON ";
            if (dow.Tuesday) days += (french) ? "MAR " : "TUE ";
            if (dow.Wednesday) days += (french)? "MER " : "WED ";
            if (dow.Thursday) days += (french)? "JEU " : "THU ";
            if (dow.Friday) days += (french)? "VEN " : "FRI ";
            if (dow.Saturday) days += (french)? "SAM " : "SAT ";
            if (dow.Sunday) days += (french)? "DIM " : "SUN ";
            return days;
        }

        protected override string GetCode23(TokenViewModel token, ProposalContext context)
        {
            return "SF";
        }

        protected override string GetCode45(TokenViewModel token, ProposalContext context)
        {
            var carsToken = token as CarsClsdViewModel;
            return !context.IsCancelation ? ((carsToken.CarsStatusId == 0) ? "LC":"AH") : "AK";
        }

        protected override string GetFir(TokenViewModel token, ProposalContext context)
        {
            return context.SdoCacheSubject().Fir;
        }

        protected override NearSdoSubject GetNearestSdoSubject(TokenViewModel token, ProposalContext context)
        {
            return (context.SdoCacheSubject() != null) ? new NearSdoSubject(context.SdoCacheSubject(), 0.0) : null;
            //return new NearSdoSubject(context.SdoCacheSubject(), 0.0);
        }

        protected override string GetNotamLocation(TokenViewModel token)
        {
            return (token as CarsClsdViewModel).SubjectLocation;
        }

        protected override string GetPurpose(TokenViewModel token, ProposalContext context)
        {
            return "B";
        }

        protected override string GetScope(TokenViewModel token, ProposalContext context)
        {
            return "A";
        }

        protected override async Task<string> GetSeries(TokenViewModel token, ProposalContext context)
        {
            return await GetNotamSeriesForSubject(context.Uow, context.SdoCacheSubject(), "SF", false);
        }

        protected override string GetTraffic(TokenViewModel token, ProposalContext context)
        {
            return "IV";
        }

        protected override ValidationResult ValidateModel(ProposalViewModel model, ProposalContext context)
        {
            var token = model.Token as CarsClsdViewModel;
            if (token == null) return ValidationResult.Fail(Resources.InvalidTokenType);

            var subject = context.SdoCacheSubject();
            if (subject == null) return ValidationResult.Fail(Resources.InvalidSubjectId);

            if(token.AddReason)
            {
                if(token.Reason.Length < 1) return ValidationResult.Fail(Resources.CARS_MissingReason);
                var forbiddenWords = context.GetContext<string>("ForbiddenItemEWords");
                if (ContainsForbiddenWord(token.Reason, forbiddenWords))
                    return ValidationResult.Fail(Resources.CARS_ReasonForbiddenWords);

                if (context.IsBilingual) {
                    if(token.ReasonFr.Length < 1) return ValidationResult.Fail(Resources.CARS_MissingReasonFr);
                    if (ContainsForbiddenWord(token.ReasonFr, forbiddenWords))
                        return ValidationResult.Fail(Resources.CARS_ReasonForbiddenWordsFr);
                }
            }

            //Do Validation here
            if(token.CarsStatusId < 0) return ValidationResult.Fail(Resources.CARS_StatusError);
            if (token.CarsStatusId > 2) return ValidationResult.Fail(Resources.CARS_StatusError);
            if( token.CarsStatusId == 1)
            {
                //Hrs of operation
                var success = (token.DaysOfWeek.Monday || token.DaysOfWeek.Tuesday || token.DaysOfWeek.Wednesday || token.DaysOfWeek.Thursday || token.DaysOfWeek.Friday || token.DaysOfWeek.Saturday || token.DaysOfWeek.Sunday);
                if(!success) return ValidationResult.Fail(Resources.CARS_ErrorOnWeekdays);

                int startHour;
                bool result = Int32.TryParse(token.StartHour, out startHour);
                if(!result) return ValidationResult.Fail(Resources.CARS_ErrorStartHours);
                if(startHour > 2359) return ValidationResult.Fail(Resources.CARS_ErrorStartHours);
                if (startHour < 0) return ValidationResult.Fail(Resources.CARS_ErrorStartHours);

                int endHour;
                result = Int32.TryParse(token.EndHour, out endHour);
                if (!result) return ValidationResult.Fail(Resources.CARS_ErrorEndHours);
                if(endHour > 2359) return ValidationResult.Fail(Resources.CARS_ErrorEndHours);
                if (endHour < 0) return ValidationResult.Fail(Resources.CARS_ErrorEndHours);
            }

            return ValidationResult.Succeed();
        }

        protected override async Task UpdateNsdContext(TokenViewModel token, ProposalContext context)
        {
            // Add SdoSubject
            var subjectId = (token as CarsClsdViewModel).SubjectId;
            var sdoCache = await context.Uow.SdoCacheRepo.GetByIdAsync(subjectId);
            context.AddContext("SdoCacheSubject", sdoCache);
        }

        protected override string GetDisplayName()
        {
            return Resources.CarsClsdDisplayName;
        }

        protected override string GetNsdHelpLink()
        {
            return Resources.CarsClsdHelpLink;
        }
    }
    internal static class CarsClsdNsdManagerExt
    {
        internal static SdoCache SdoCacheSubject(this ProposalContext context) => context.GetContext<SdoCache>("SdoCacheSubject");
    }

}
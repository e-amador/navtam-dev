﻿import {Injectable} from '@angular/core';

import { IMemoryStorage } from './data.model';

@Injectable()
export class MemoryStorageService {

    NOTAM_QUERY_OPTIONS_KEY = "NOTAM_QUERY_OPTIONS_KEY";
    REPORT_NOTAM_QUERY_OPTIONS_KEY = "REPORT_NOTAM_QUERY_OPTIONS_KEY";
    REPORT_NOTAM_ACTIVE_QUERY_OPTIONS_KEY = "REPORT_NOTAM_ACTIVE_QUERY_OPTIONS_KEY";
    PROPOSAL_QUERY_OPTIONS_KEY = "PROPOSAL_QUERY_OPTIONS_KEY";    
    NSD_CLIENTS_QUERY_OPTIONS_KEY = "NSD_CLIENTS_QUERY_OPTIONS_KEY";
    MESSAGES_QUEUE_QUERY_OPTIONS_KEY = "MESSAGES_QUEUE_QUERY_OPTIONS_KEY";
    FILTER_APPLIED_KEY = "FILTER_APPLIED_KEY";    
    REPORT_FILTER_APPLIED_KEY = "REPORT_FILTER_APPLIED_KEY";    
    FILTER_QUERY_OPTIONS_KEY = "FILTER_QUERY_OPTIONS_KEY";    
    REPORT_FILTER_QUERY_OPTIONS_KEY = "REPORT_FILTER_QUERY_OPTIONS_KEY";    
    MSG_FILTER_APPLIED_KEY = "MSG_FILTER_APPLIED_KEY";    
    PROPOSAL_FILTER_KEY = "PROPOSAL_FILTER_KEY";    
    NOTAM_FILTER_KEY = "NOTAM_FILTER_KEY";    
    REPORT_NOTAM_FILTER_KEY = "REPORT_NOTAM_FILTER_KEY";    
    PROPOSAL_ID_KEY = "PROPOSAL_ID_KEY";
    NOTAM_ID_KEY = "NOTAM_ID_KEY";
    NSD_CLIENT_ID_KEY = "NSD_CLIENT_ID_KEY";
    MSG_TAB_SELECTED_KEY = "MSG_TAB_SELECTED_KEY";
    MESSAGE_QUEUE_ID_KEY = "MESSAGE_QUEUE_ID_KEY";
    CATEGORY_ID_KEY = "CATEGORY_ID_KEY";
    SUB_CATEGORY_ID_KEY = "SUB_CATEGORY_ID_KEY";
    ACTIVE_QUEUE_KEY = "ACTIVE_QUEUE_KEY";
    MODIFY_ACTION_KEY = "MODIFY_ACTION_KEY"; //0-Update 1-Widtrawn
    PENDING_QUEUE_SOUND_KEY = "PENDING_QUEUE_SOUND_KEY";
    MESSAGE_QUEUE_SOUND_KEY = "MESSAGE_QUEUE_SOUND_KEY";
    PENDING_QUEUE_SOUND_COUNT_KEY = "PENDING_QUEUE_SOUND_COUNT_KEY";
    MESSAGE_QUEUE_SOUND_COUNT_KEY = "MESSAGE_QUEUE_SOUND_COUNT_KEY";

    save(key: string, value: any) {
        const index = this._memoryStorage.findIndex(x => x.key === key);
        if( index >= 0 ) {
            this._memoryStorage[index].value = value;
        } else {
            this._memoryStorage.push({
                key: key, 
                value: value});
        }
    }

    get(key) : any {
        const index = this._memoryStorage.findIndex(x => x.key === key);
        if( index >= 0) {
            return this._memoryStorage[index].value;
        }
        return null;
    }

    remove(key): any {
        const index = this._memoryStorage.findIndex(x => x.key === key);
        if (index >= 0) {
            this._memoryStorage.splice(index,1);
        }
    }

    private _memoryStorage : Array<IMemoryStorage> = [];
}
﻿using NavCanada.Core.Domain.Model.Entitities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    public class SdoCacheConfig : EntityTypeConfiguration<SdoCache>
    {
        public SdoCacheConfig()
        {
            HasKey(e => e.Id);

            Property(e => e.Id).HasMaxLength(32);
            Property(e => e.ParentId).HasMaxLength(32);

            Property(e => e.Name).HasMaxLength(128);
            Property(e => e.Designator).HasMaxLength(16);
            Property(e => e.Keywords).HasMaxLength(128);

            Property(t => t.Name)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_Name", 1) { IsUnique = false }));

            Property(t => t.Designator)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_Designator", 1) { IsUnique = false }));

            Property(t => t.Keywords)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_Keywords", 1) { IsUnique = false }));

            HasOptional(e => e.Parent)
                .WithMany(e => e.Children)
                .HasForeignKey(e => e.ParentId)
                .WillCascadeOnDelete(false);
        }
    }
}


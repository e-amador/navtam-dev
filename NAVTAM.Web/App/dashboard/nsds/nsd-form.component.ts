﻿import { Component, Inject, Input, OnInit, ChangeDetectorRef, AfterViewInit, ViewChildren, ViewChild, QueryList, ElementRef } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { Location } from "@angular/common";
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms'

import { NsdButtonBarComponent } from '../nsds/nsd-buttonbar.component';
import { NsdSchedulerDailyComponent } from '../nsds/nsd-scheduler-daily.component';
import { NsdSchedulerDateComponent } from '../nsds/nsd-scheduler-date.component';
import { NsdSchedulerDaysOfWeekComponent } from '../nsds/nsd-scheduler-days-week.component';
import { NsdSchedulerFreeTextComponent } from '../nsds/nsd-scheduler-free-text.component';

import { NsdGeoRefToolComponent } from '../nsds/nsd-georeftool.component'

//import { Select2OptionData } from 'ng2-select2';

//import { DynamicComponent } from '../nsds/dynamic.component';
import { DataService } from '../shared/data.service';
import { ProposalStatus, ProposalType, IOriginatorInfo, ItemD } from '../shared/data.model';
import { MemoryStorageService } from '../shared/mem-storage.service';
import { WindowRef } from '../../common/windowRef.service';
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';

import { LocaleService, TranslationService } from 'angular-l10n';

import * as moment from 'moment';
import { forEach } from '@angular/router/src/utils/collection';
//import { forEach } from '@angular/router/src/utils/collection';
declare var $: any;

@Component({
    templateUrl: '/app/dashboard/nsds/nsd-form.component.html',
    selector: 'proposal-form',
})
export class NsdFormComponent implements OnInit, AfterViewInit {

    @Input('model') model: any;
    @Input('isReadOnly') isReadOnly: boolean;
    @Input('isNOF') isNOF: boolean = false;

    @ViewChildren(NsdButtonBarComponent) buttonBars: QueryList<NsdButtonBarComponent>;
    @ViewChild(NsdSchedulerDailyComponent) schedulerDaily: NsdSchedulerDailyComponent;
    @ViewChild(NsdSchedulerDateComponent) schedulerDate: NsdSchedulerDateComponent;
    @ViewChild(NsdSchedulerDaysOfWeekComponent) schedulerDayWeeks: NsdSchedulerDaysOfWeekComponent;
    @ViewChild(NsdSchedulerFreeTextComponent) schedulerFreeText: NsdSchedulerFreeTextComponent;

    @ViewChild(NsdGeoRefToolComponent) geoRefTool: NsdGeoRefToolComponent;

    tabActive: number = 1;
    nsdForm: FormGroup;
    annotationFormGroup: FormGroup;

    groupModel: Array<any> = [];
    groupSelected: null;

    endValidityMoment: any;
    startActivityMoment: any;
    endValidityText: string = "";
    startActivityText: string = "";   
    selectedProposalAnnotationId: number = -1;

    isSaveButtonDisabled: boolean = true;
    isSubmitButtonDisabled: boolean = true;
    isGroupingEnabled: boolean = false;
    waitingForGeneratingIcao: boolean = false;
    loadingPrevIcao: boolean = false;
    isPrevIcaoTabVisible: boolean = false;
    prevIcaoText: string = "";
    isWaitingForResponse: boolean = false;

    openNotesToNof: boolean = false;
    seletedOriginatorId: string = "-1";
    originators: Array<IOriginatorInfo> = []; 
    modifiedOriginators: Array<IOriginatorInfo> = []; 

    mapHealthy: boolean = false;

    waitForMapTick: boolean = true;
    schedulerPanelOpen: boolean = false;
    schedulerTab: number = 1;
    hasScheduler: boolean = false;
    isSchedulePanelOpen : boolean = false;
    isScheduleButtonActionVisible: boolean = true;
    isRemoveScheduleAlertShown: boolean = false;
    type: string = "";
    isScheduleOverridden: boolean = false;
    overrideBilingual: boolean = false;

    //modifiedOriginators: Array<any> = [];

    private componentData = null;
    private dateFormat: string = "YYMMDDHHmm";
    private backToReviewListLabel: string = this.translation.translate('ActionButtons.BackToReviewList')

    txtBackToReviewList;
    isNewProposal: boolean = true;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private activeRoute: ActivatedRoute,
        private windowRef: WindowRef,
        private changeDetectionRef: ChangeDetectorRef,
        public translation: TranslationService,
        private location: Location) {
    }


    ngOnInit() {
        this.location.subscribe((x) => {
            this.backToProposals()
        });

        this.configureReactiveForm();

        this.isSaveButtonDisabled = this.isSaveButtonDisabled || this.isReadOnly;

        this.type = this.activeRoute.snapshot.data[0]['type'];
        if (this.windowRef.appConfig.isNof) {
            if (this.model.proposalType === ProposalType.Cancel && !this.isReadOnly) {
                this.type = "cancel"; //re-enforce cancellation for NOF when "Pick" action
            }
        }

        this.dataService.override.subscribe(value => this.overrideBilingual = value);
        if (this.model.status !== 0) {
            this.overrideBilingual = this.model.token.overrideBilingual;
        }

        if (this.isReadOnly) {
            this.txtBackToReviewList = this.backToReviewListLabel;
        } else {
            const fromWidthdrawnAction = this.memStorageService.get(this.memStorageService.MODIFY_ACTION_KEY);
            this.txtBackToReviewList = fromWidthdrawnAction ? this.translation.translate('ActionButtons.CancelAnd') + this.backToReviewListLabel : this.backToReviewListLabel;
        }

        this.model.token.type = this.type;
        this.componentData = {
            catId: this.model.categoryId,
            version: this.model.version,
            inputs: {
                form: this.nsdForm, 
                type: this.type,
                isReadOnly: this.isReadOnly,
                token: this.model.token, //passing the model}
                proposalType: this.model.proposalType
            }
        };

        this.getPrevIcaoText();

        this.loadMatchingOriginators(this.model.token.originator);

        this.openNotesToNof = this.model.token.annotations.length > 0;

        if (this.model.proposalId) { //when loading a saved proposal wait until form is ready

            if (this.model.immediate || this.model.permanent || this.model.estimated) {
                this.isScheduleButtonActionVisible = false;
            } else {
                this.loadScheduler(this.model.proposalId);
            } 

            if (!this.isReadOnly) {
                if (this.type === "cancel" || this.type === "replace") {
                    this.isSaveButtonDisabled = false;
                } else {
                    if (!this.nsdForm.valid) {
                        this.nsdForm.markAsDirty();
                        //todo here
                    }
                    this.isSaveButtonDisabled = !this.nsdForm.valid;
                }

            }
        } else {
            if (this.type === "clone") {
                if (this.model.immediate || this.model.permanent || this.model.estimated) {
                    this.isScheduleButtonActionVisible = false;
                }

                this.isSaveButtonDisabled = false;
                //this.loadScheduler(this.activeRoute.snapshot.params['id']);
            }
        }

        if (this.model.groupId && (this.isNOF || this.isReadOnly)) { // && type !== "cancel" && type !== "replace") {
            this.createGrouping(this.model.groupedProposals);
        }        

        const originatorEmailField = this.nsdForm.get('originatorEmail');
        originatorEmailField.valueChanges
            .forEach(email => {
                if (!email) {
                    originatorEmailField.clearValidators();
                    if (originatorEmailField.errors) {
                        originatorEmailField.updateValueAndValidity();
                    }
                } else {
                    originatorEmailField.setValidators([Validators.email, emailWidthDomainValidator]);
                }
            });
        const originatorPhoneField = this.nsdForm.get('originatorPhone');
        originatorPhoneField.valueChanges
            .forEach(phone => {
                if (!phone) {
                    originatorPhoneField.clearValidators();
                    if (originatorPhoneField.errors) {
                        originatorPhoneField.updateValueAndValidity();
                    }
                } else {
                    originatorPhoneField.setValidators([Validators.pattern('[0-9]+')]);
                }
            });
        this.initMap();
        this.mapHealthCheck();
    }

    check48Hours(): boolean {
        if (this.isReadOnly) {
            return false;
        } else if (this.startActivityText) {
            return moment.duration(moment(this.startActivityText).diff(moment())).asHours() > 48            
        }       
        return false;
    }

    ngAfterViewInit() {
        this.buttonBars.forEach((child) => {
            child.parent = <NsdFormComponent>this;
        });
        this.schedulerDate.parent = this;
        this.schedulerDaily.parent = this;
        this.schedulerDayWeeks.parent = this;
        this.schedulerFreeText.parent = this;

        this.isSubmitButtonDisabled = !this.nsdForm.valid || !this.canSubmitFromStatus(this.model.status);
        if (this.type !== 'draft') {
            this.isSaveButtonDisabled = !this.nsdForm.valid || this.isReadOnly;
        }
        this.changeDetectionRef.detectChanges();
        if (!this.isReadOnly || this.isGroupingEnabled) {
            this.nsdForm.valueChanges
                .debounceTime(2000)
                .subscribe(data => {
                    this.generateIcao(data);
                    if (this.type !== 'draft') this.isNewProposal = false;
                    else if (this.model.proposalId !== null) this.isNewProposal = false;
                    //else this.isNewProposal = true;
                    if (this.isGroupingEnabled) {
                        this.updateCommonGroupItemsValues();
                    }
                });
        }
        if (this.model.proposalType === ProposalType.Cancel) {
            window.setTimeout(() => this.generateIcao(null), 2000);
        }

        //Uncomment the html with modal-warning-date definition
        //let ctrl = this.nsdForm.get('grpDatePeriod');
        //if (ctrl.invalid && this.model.proposalId !== null) {
        //    $('#modal-warning-date').modal({});
        //}
    }   

    get isBilingualRegion(): boolean {

        return this.model && this.model.token && this.model.token.isBilingual;
    }

    activateTab(tab) {
        switch (tab) {
            case 'map':
                this.tabActive = 0;
                break;
            case 'icao-text':
                this.tabActive = 1;
                break;
            case 'icao-format':
                if (this.nsdForm.valid) {
                    this.tabActive = 2;
                }
                break;
            case 'geoMap':
                this.tabActive = 3;
                break;
            case 'last-icao-format':
                if (this.nsdForm.valid) {
                    this.tabActive = 4;
                }
                break;
            default:
                this.tabActive = 1;
        }
    }

    get mvpPeriodErrorMessage(): string {
        let mvp = this.windowRef.appConfig.mvp || '30';
        let msg: string = this.translation.translate('ErrorMsgs.EndValidityFutureMins');
        return msg.replace("{mvp}", mvp);
    }

    backToProposals() {
        this.isSaveButtonDisabled = true;
        if (this.isGroupingEnabled && !this.model.proposalId) {
            this.model = this.groupModel[0];
        }
        if (this.model.proposalId) {
            this.dataService.restoreProposalStatus(this.model.proposalId, this.model.statusUpdated)
                .subscribe(response => {
                    if (!response) {
                        this.toastr.error(this.translation.translate('ErrorMsgs.UnexpectedError'), "", { 'positionClass': 'toast-bottom-right' });
                    }
                    this.disposeMap();
                    this.router.navigate(['/dashboard']);
                }, error => {
                    this.toastr.error(this.translation.translate('ErrorMsgs.InternalServerError') + error.message, "", { 'positionClass': 'toast-bottom-right' });
                    this.disposeMap();
                    this.router.navigate(['/dashboard']);
                });
        } else {
            this.disposeMap();
            this.router.navigate(['/dashboard']);
        }
    }

    discardProposal(onError: Function) {
        this.isSaveButtonDisabled = true;
        this.dataService.discardProposal(this.model.proposalId)
            .subscribe(response => {
                let msg = this.translation.translate('Dashboard.MessageDiscardSuccess')
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                this.disposeMap();
                this.router.navigate(['/dashboard']);
            }, error => {
                this.isSaveButtonDisabled = false;
                if (onError)
                    onError(error);
                let msg = this.translation.translate('Dashboard.MessageDiscardSuccess')
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    backToNofProposals() {
        this.isSaveButtonDisabled = true;
        if (this.isGroupingEnabled && !this.model.proposalId) {
            this.model = this.groupModel[0];
        }
        if (this.model.proposalId) {
            this.dataService.restoreNofProposalStatus(this.model.proposalId, this.model.statusUpdated)
                .subscribe(response => {
                    if (!response) {
                        this.toastr.error(this.translation.translate('ErrorMsgs.UnexpectedError'), "", { 'positionClass': 'toast-bottom-right' });
                    }
                    this.disposeMap();
                    this.router.navigate(['/dashboard']);
                }, error => {
                    this.toastr.error(this.translation.translate('ErrorMsgs.InternalServerError') + error.message, "", { 'positionClass': 'toast-bottom-right' });
                    this.disposeMap();
                    this.router.navigate(['/dashboard']);
                });
        } else {
            this.disposeMap();
            this.router.navigate(['/dashboard']);
        }
    }

    showGrouping() {
        let copyModel = JSON.parse(JSON.stringify(this.model));
        copyModel.proposalId = 0;

        this.groupModel = [];
        this.model.isSelected = true;
        this.groupSelected = this.model;

        this.groupModel.push(this.model);
        this.groupModel.push(copyModel);

        this.isGroupingEnabled = true;
    }

    createGrouping(groupedProposals: any[]) {
        this.groupModel = [];
        groupedProposals.forEach((p) => {
            if (p.proposalId === this.model.proposalId) {
                this.model.isSelected = true;
                this.groupSelected = this.model;
                this.groupModel.push(this.model);
            } else {
                p.proposalId = p.proposalId || 0;
                p.isSelected = false;
                this.groupModel.push(p);
            }
        });
        this.isGroupingEnabled = true;
    }

    isGroupingActionButtonEnabled() {
        return this.nsdForm.valid && !this.isReadOnly && !this.waitingForGeneratingIcao
    }

    addToGroup() {
        this.groupModel.forEach((item) => { item.isSelected = false });
        let copyModel = JSON.parse(JSON.stringify(this.model));
        copyModel.proposalId = 0;
        this.groupModel.push(copyModel);
        this.onGroupItemSelected(this.groupModel.length - 1);
    }

    onGroupItemSelected(index) {
        if (this.groupModel[index].isSelected) {
            return; //already selected
        }

        if (this.nsdForm.valid) {
            this.groupModel.forEach((item) => { item.isSelected = false });
            this.groupModel[index].isSelected = true;
            this.groupSelected = this.groupModel[index];

            this.waitingForGeneratingIcao = true;
            this.model = this.groupSelected;
            this.updateGroupItem();
        }
    }

    removeFromGroup() {
        const index = this.groupModel.findIndex(x => x.isSelected === true);
        if (index >= 2) {
            this.groupModel.splice(index, 1);
            if (this.groupModel.length > 0) {
                this.model = this.groupModel[0];
                this.model.isSelected = true;
                this.groupSelected = this.model;
                this.updateGroupItem();
            }
        }
    }

    updateGroupItem() {
        this.updateFormControlValue("grpDatePeriod.immediate", this.model.token.immediate);
        this.updateFormControlValue("grpDatePeriod.permanent", this.model.token.permanent);
        this.updateFormControlValue("grpDatePeriod.estimated", this.model.token.estimated);
        this.updateFormControlValue("originator", this.model.token.originator);
        this.updateFormControlValue("originatorEmail", this.model.token.originatorEmail);
        this.updateFormControlValue("originatorPhone", this.model.token.originatorPhone);

        this.updateFormControlValue("itemD", this.model.token.itemD);

        this.updateFormControlValue("urgent", this.model.token.urgent);
        this.updateFormControlValue("amendPubMessage", this.model.token.amendPubMessage);
        this.updateFormControlValue("amendPubMessageFrench", this.model.token.amendPubMessageFrench);

        this.setStartActivityDate(this.model.token.startActivity, true);
        this.setEndValidityDate(this.model.token.endValidity, true);

        let type = this.componentData.inputs.type;
        this.componentData = {
            catId: this.model.categoryId,
            version: this.model.version,
            inputs: {
                form: this.nsdForm,
                type: type,
                isReadOnly: this.isReadOnly,
                token: this.model.token
            }
        };

        this.changeDetectionRef.detectChanges();
        this.nsdForm.markAsDirty();
    }

    updateCommonGroupItemsValues() {
        this.groupModel.forEach((item) => {
            if (!item.isSelected) {
                item.token.immediate = this.model.token.immediate;
                item.token.permanent = this.model.token.permanent;
                item.token.originator = this.model.token.originator;
                item.token.originatorPhone = this.model.token.originatorPhone;
                item.token.originatorEmail = this.model.token.originatorEmail;
                item.token.itemD = this.model.token.itemD;
                item.token.annotations = this.model.token.annotations;
                item.token.estimated = this.model.token.estimated;
                item.token.startActivity = this.model.token.startActivity;
                item.token.endValidity = this.model.token.endValidity;
                item.token.urgent = this.model.token.urgent;
                item.token.amendPubMessage = this.model.token.amendPubMessage;
                item.token.amendPubMessageFrench = this.model.token.amendPubMessageFrench;
            }
        });

    }

    clearSchedule(check, callbackFunc, deleteScheduleEventsEstimated = false) {
        if (check !== null) {
            this.isScheduleButtonActionVisible = !check;
        }
        if (this.hasScheduler) {
            const el: any = $('#modal-remove-schedule');
            const $modal = el.modal();

            el.data('funcAttr', callbackFunc);

            let self = this;

            $modal.modal("show");

            if (!this.isRemoveScheduleAlertShown) {
                el.on('shown.bs.modal', function (e) {
                }).on('hide.bs.modal', function (e) {
                    let callback = $("#modal-remove-schedule").data("funcAttr");

                    let pressedButton = $(document.activeElement).attr('id');
                    if (pressedButton === "btn-remove") {
                        if (check) {
                            $('#schedule-panel').hide();
                        }

                        self.hasScheduler = check !== null ? false : true;
                        self.nsdForm.get('grpDatePeriod.startActivity').enable();
                        self.nsdForm.get('grpDatePeriod.endValidity').enable();

                        switch (self.schedulerTab) {
                            case 1:
                                self.schedulerDaily.deleteEvents(null, deleteScheduleEventsEstimated);
                                self.schedulerDaily.toggleShedule(null, deleteScheduleEventsEstimated);
                                self.hasScheduler = false;
                                break;
                            case 2:
                                self.schedulerDayWeeks.deleteEvents(null, deleteScheduleEventsEstimated);
                                self.schedulerDayWeeks.toggleShedule(null, deleteScheduleEventsEstimated);
                                self.hasScheduler = false;
                                break;
                            case 3:
                                self.schedulerDate.deleteEvents(null, deleteScheduleEventsEstimated);
                                self.schedulerDate.toggleShedule(null, deleteScheduleEventsEstimated);
                                self.hasScheduler = false;
                                break;
                            case 4:
                                if (self.schedulerDaily.dateEvents.length > 0) {
                                    self.schedulerDaily.deleteEvents(null, true);
                                    self.schedulerDaily.toggleShedule(null, true);
                                    self.schedulerDaily.slideScheduleClose();
                                    self.hasScheduler = false;
                                } else if (self.schedulerDayWeeks.hasEvents()) {
                                    self.schedulerDayWeeks.deleteEvents(null, true);
                                    self.schedulerDayWeeks.toggleShedule(null, true);
                                    self.schedulerDayWeeks.slideScheduleClose();
                                    self.hasScheduler = false;
                                } else if (self.schedulerDate.hasEvents()) {
                                    self.schedulerDate.deleteEvents(null, true);
                                    self.schedulerDate.toggleShedule(null, true);
                                    self.schedulerDate.slideScheduleClose();
                                    self.hasScheduler = false;
                                }
                                self.schedulerFreeText.clearFormControls();
                                self.updateFormControlValue('itemD', "");
                                self.resetSchedulersStatus();
                                break;
                        }

                        callback(true); 
                    } else {
                        if (callback) {
                            callback(false);
                        }
                    }
                })
            }
            this.isRemoveScheduleAlertShown = true;
        } else {
            if (check) {
                if (this.schedulerTab == 4) {
                    this.schedulerFreeText.clearFormControls();
                    this.updateFormControlValue('itemD', "");
                }

                $('#schedule-panel').hide();
            }

            if (callbackFunc) {
                callbackFunc(true);
            }
        }
    }

    resetSchedulersStatus() {
        this.hasScheduler = false;
        this.schedulerDaily.resetClearScheduleFlag();
        this.schedulerDayWeeks.resetClearScheduleFlag();
        this.schedulerDate.resetClearScheduleFlag();
    }

    isStartActivityCalendarActive() {
        if (this.nsdForm.get('grpDatePeriod.startActivity').disabled) {
            return false;
        }

        return this.model.proposalType !== ProposalType.Cancel && this.nsdForm.get('grpDatePeriod.immediate').value !== true;
    }

    isEndValidityCalendarActive() {
        if (this.nsdForm.get('grpDatePeriod.endValidity').disabled) {
            return false;
        }

        return this.model.proposalType !== ProposalType.Cancel && this.nsdForm.get('grpDatePeriod.permanent').value !== true;
    }

    setStartActivityDate(date: any, leaveEmpty?: boolean) {
        const fc = this.nsdForm.get('grpDatePeriod.startActivity');
        if (date) {         
            let momentDate = moment.utc(date);
            this.startActivityMoment = momentDate.format(this.dateFormat);
            if (fc.value !== this.startActivityMoment) {
                fc.setValue(this.startActivityMoment);
                fc.markAsDirty();
                this.startActivityText = momentDate.format("YYYY/MM/DD, H:mm") + " UTC";
            }
        } else {
            if (leaveEmpty) {
                fc.setValue(null);
                fc.markAsDirty();
            } else {
                this.setStartActivityDate(moment.utc());
            }
        }
    }

    onStartActivityDateInputChange(strDate: string) {
        if (strDate && strDate.length === 10) {
            let momentDate = moment.utc(strDate, "YYMMDDHHmm");
            if (momentDate.isValid()) {
                this.startActivityText = momentDate.format("YYYY/MM/DD, H:mm") + " UTC";
                const fc = this.nsdForm.get('grpDatePeriod.startActivity');
                const formatedDate = momentDate.format(this.dateFormat);
                if (fc.value !== formatedDate) {
                    fc.setValue(formatedDate);
                    this.startActivityMoment = formatedDate;
                }
            } else {
                this.startActivityText = "";
            }
        } else {
            this.startActivityText = "";
        }
    }

    setEndValidityDate(date: any, leaveEmpty?: boolean) {
        const fc = this.nsdForm.get('grpDatePeriod.endValidity');
        if (date) {
            let momentDate = moment.utc(date);
            this.endValidityMoment = momentDate.format(this.dateFormat);
            if (fc.value !== this.endValidityMoment) {
                fc.setValue(this.endValidityMoment);
                fc.markAsDirty();
                this.endValidityText = momentDate.format("YYYY/MM/DD, H:mm") + " UTC";
            }
        } else {
            if (leaveEmpty) {
                fc.setValue(null);
                fc.markAsDirty();
            } else {
                this.setEndValidityDate(moment.utc());
            }
        }
    }

    onEndValidityInputChange(strDate: string) {
        if (strDate && strDate.length === 10) {
            let momentDate = moment.utc(strDate, "YYMMDDHHmm");
            if (momentDate.isValid()) {
                this.endValidityText = momentDate.format("YYYY/MM/DD, H:mm") + " UTC";
                const fc = this.nsdForm.get('grpDatePeriod.endValidity');
                const formatedDate = momentDate.format(this.dateFormat);

                if (fc.value !== formatedDate) {
                    fc.setValue(formatedDate);
                    this.endValidityMoment = formatedDate;
                }
            } else {
                this.endValidityText = ""
            }
        } else {
            this.endValidityText = ""
        }
    }

    immediateChanged(checked: boolean) {
        if (checked) {
            let self = this;
            this.clearSchedule(checked, function (actionResult) {
                if (actionResult) {
                    const fc = self.nsdForm.get('grpDatePeriod.startActivity');
                    fc.setValue(null);
                    fc.markAsDirty();
                    fc.disable();
                    self.model.token.startActivity = null;
                    self.startActivityText = ""

                    self.setIcaoInfoTab.apply(self);
                } else {
                    const switchCtrl = self.nsdForm.get('grpDatePeriod.immediate');
                    switchCtrl.setValue(!checked);
                }
            });
        } else {
            if (this.checkScheduleButtonVisiblity()) {
                this.isScheduleButtonActionVisible = true;
                this.schedulerTab = 4;
                let endValidity = this.endValidityText;
                this.schedulerFreeText.clear();
                this.nsdForm.get('grpDatePeriod.endValidity').setValue(moment(moment(endValidity, 'YYYY/MM/DD, HH:mm')).format(this.dateFormat));
            }
            this.nsdForm.get("grpDatePeriod.startActivity").enable();
        }
    }

    permanentChanged(checked: boolean) {
        const estimatedCtrl = this.nsdForm.get('grpDatePeriod.estimated');
        if (checked) {
            let self = this;
            this.clearSchedule(checked, function (actionResult) {
                if (actionResult) {
                    const endValCtrl = self.nsdForm.get('grpDatePeriod.endValidity');
                    endValCtrl.setValue(null);
                    endValCtrl.markAsDirty();
                    endValCtrl.disable();
                    estimatedCtrl.disable();

                    if (estimatedCtrl.value) {
                        estimatedCtrl.setValue(false);
                        estimatedCtrl.markAsDirty();
                    }

                    self.endValidityText = ""
                    self.model.token.endValidity = null;
                    self.setIcaoInfoTab.apply(self);
                } else {
                    const permanentCtrl = self.nsdForm.get('grpDatePeriod.permanent');
                    permanentCtrl.setValue(!checked);
                }
            });
        } else {
            if (this.checkScheduleButtonVisiblity()) {
                this.isScheduleButtonActionVisible = true;
                this.schedulerTab = 4;

                let startActivity = this.startActivityText;
                this.schedulerFreeText.clear();
                this.nsdForm.get('grpDatePeriod.startActivity').setValue(moment(moment(startActivity, 'YYYY/MM/DD HH:mm')).format(this.dateFormat));
            }

            this.nsdForm.get("grpDatePeriod.endValidity").enable();
            estimatedCtrl.enable();
        }
    }

    setIcaoInfoTab(enforce: boolean = false) {
        if (this.tabActive == 2 || enforce) {
            this.tabActive = 1;
        }
    }

    get isPermanent(): boolean {
        const ctrl = this.nsdForm.get('grpDatePeriod.permanent');
        return ctrl && ctrl.value;
    }

    estimatedChanged(checked: boolean) {
        const permanentCtrl = this.nsdForm.get('grpDatePeriod.permanent');
        if (checked) {
            let self = this;
            this.clearSchedule(checked, function (actionResult) {
                if (actionResult) {
                    permanentCtrl.disable();
                    permanentCtrl.setValue(false);
                    permanentCtrl.markAsDirty();

                    self.setIcaoInfoTab.apply(self);
                } else {
                    const estimatedCtrl = self.nsdForm.get('grpDatePeriod.estimated');
                    estimatedCtrl.setValue(!checked);
                }
            },
            checked); //delete scheduler events because proposal is estimated
        }
        else {
            if (this.checkScheduleButtonVisiblity()) {
                this.isScheduleButtonActionVisible = true;
                this.schedulerTab = 4;

                let endValidity = this.endValidityText;
                let startActivity = this.startActivityText;
                this.schedulerFreeText.clear();
                this.nsdForm.get('grpDatePeriod.startActivity').setValue(moment(moment(startActivity, 'YYYY/MM/DD HH:mm')).format(this.dateFormat));
                this.nsdForm.get('grpDatePeriod.endValidity').setValue(moment(moment(endValidity, 'YYYY/MM/DD, HH:mm')).format(this.dateFormat));
            }

            permanentCtrl.enable();
        }
    }

    validateOriginator() {
        if (this.isReadOnly) return true;
        const originatorCtrl = this.nsdForm.get("originator");
        return originatorCtrl.valid || this.nsdForm.pristine;
    }

    validateStartActivity(errorName: string) {
        let hasErrors = this.nsdForm.get('grpDatePeriod').errors;
        if (hasErrors) {
            if (errorName) {
                hasErrors = this.nsdForm.get('grpDatePeriod').errors[errorName];
            } else {
                hasErrors = this.nsdForm.get('grpDatePeriod').errors["startActivityRequired"] ||
                    this.nsdForm.get('grpDatePeriod').errors["validateOnlyInFuture"] ||
                    this.nsdForm.get('grpDatePeriod').errors["invalidStartActivityTenDigitDate"] ||
                    this.nsdForm.get('grpDatePeriod').errors["validateOneDayInAdvance"];
            }
        }

        if (this.isNewProposal) {
            return !hasErrors || this.nsdForm.pristine;
        } else {
            return !hasErrors;
        }
    }

    isStartApproaching(): boolean {
        if (this.isReadOnly) return false;

        let hasErrors = this.nsdForm.get('grpDatePeriod').errors;
        if (!hasErrors) {
            const immediateCtrl = this.nsdForm.get("grpDatePeriod.immediate");
            if (immediateCtrl) {
                if (!immediateCtrl.value) {
                    const startActivityCtrl = this.nsdForm.get("grpDatePeriod.startActivity");
                    const startDate = startActivityCtrl.value ? moment.utc(startActivityCtrl.value, 'YYMMDDHHmm') : null;
                    if (startDate && startDate.isValid()) {
                        const now = moment.utc(moment().utc().format("MM/DD/YYYY, H:mm"));
                        const minutes = moment.utc(startDate, "MM/DD/YYYY, H:mm").diff(now, 'minutes');
                        if (minutes < 30) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    validateEndValidity(errorName: string) {
        let hasErrors = this.nsdForm.get('grpDatePeriod').errors;
        if (hasErrors) {
            if (errorName) {
                hasErrors = this.nsdForm.get('grpDatePeriod').errors[errorName];
            } else {
                hasErrors = this.nsdForm.get('grpDatePeriod').errors["endValidityRequired"] ||
                    this.nsdForm.get('grpDatePeriod').errors["validateLaterThan30MinStartingDate"] ||
                    this.nsdForm.get('grpDatePeriod').errors["invalidEndValidityTenDigitDate"] ||
                    this.nsdForm.get('grpDatePeriod').errors["validateLaterThan92Days"] ||
                    this.nsdForm.get('grpDatePeriod').errors["validateLaterThanStartingDate"];
            }
        }
        if (this.isNewProposal) {
            return !hasErrors || this.nsdForm.pristine;
        } else {
            return !hasErrors;
        }
    }

    ignoreUserInput(event: any) {
        event.preventDefault();
    }

    saveDraft() {
        const payload = this.setPayload(false);

        function success(data): void {
            this.isSaveButtonDisabled = true;
            this.txtBackToReviewList = this.backToReviewListLabel;

            this.updateModel(data);
            //this.updateReviewListFilter(data.proposalId);
            //This need to be done here, not on the updateModel function, because this is only happening when the proposal is saved
            //and the updateModel happen everytime that Icao is generated
            if (this.model.token.annotations) {
                //Reset the annotations control
                for (let i = this.annotations.length - 1; i >= 0; i--) {
                    this.annotations.removeAt(i);
                }
                this.selectedProposalAnnotationId = -1;
                //Rebuild the annotations control
                for (let i = 0; i < this.model.token.annotations.length; i++) {
                    this.annotations.push(this.buildAnnotation(this.model.token.annotations[i]));
                }
            }

            this.toastr.success(this.translation.translate('Dashboard.MessageSaveSuccess'), "", { 'positionClass': 'toast-bottom-right' });
        }

        function failure(error) {
            this.toastr.error(this.translation.translate('Dashboard.MessageSaveFailure') + error.message, "", { 'positionClass': 'toast-bottom-right' });
        }

        if (this.model.groupId) {
            this.dataService.saveDraftGroupProposal(payload)
                .subscribe(data => {
                    success.call(this, data);
                }, error => {
                    failure.call(this, error);
                });
        }
        else {
            this.dataService.saveDraftProposal(payload)
                .subscribe(data => {
                    this.saveScheduler(data);
                    success.call(this, data);
                }, error => {
                    failure.call(this, error);
                });
        }
    }

    submitProposal(onError: Function) {
        this.isSaveButtonDisabled = true;
        this.isWaitingForResponse = true;

        const payload = this.setPayload(false);

        function success(data): void {
            this.router.navigate(['/dashboard']);
        }

        function failure(error) {
            this.isSaveButtonDisabled = false;
            this.isWaitingForResponse = false;
           if (onError)
                onError.call(error);
            this.toastr.error(this.translation.translate('Dashboard.MessageSubmitFailure') + error.message, "", { 'positionClass': 'toast-bottom-right' });
        }

        if (this.model.groupId) {
            this.dataService.submitGroupedProposal(payload)
                .subscribe(data => {
                    success.call(this, data);
                }, error => {
                    failure.call(this, error);
                });
        } else {
            this.dataService.submitProposal(payload)
                .subscribe(data => {
                    this.saveScheduler(data);
                    success.call(this, data);
                }, error => {
                    failure.call(this, error);
                });
        }
    }

    parkProposal(onError: Function) {
        this.isWaitingForResponse = true;
        this.isSaveButtonDisabled = true;
        if (this.isGroupingEnabled) {
            const payload = this.setGroupPayload(false);
            return this.dataService.parkGroupedProposal(payload)
                .subscribe(data => {
                    this.nsdForm.markAsPristine();
                    this.router.navigate(['/dashboard']);
                }, error => {
                    this.isWaitingForResponse = false;
                    if (onError)
                        onError.call(error);
                    this.toastr.error(this.translation.translate('Dashboard.MessageParkFailure') + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        }
        else {
            const payload = this.setPayload(false);
             return this.dataService.parkProposal(payload)
                .subscribe(data => {
                    this.isSaveButtonDisabled = true;
                    this.updateModel(data);
                    this.saveScheduler(data);

                    this.nsdForm.markAsPristine();
                    this.router.navigate(['/dashboard']);
                 }, error => {
                     this.isWaitingForResponse = false;
                     this.isSaveButtonDisabled = false;

                     if (onError)
                         onError.call(error);

                    this.toastr.error(this.translation.translate('Dashboard.MessageParkFailure') + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        }
    }

    openRejectProposalDialog() {
        const el: any = $('#modal-reject-reason');
        const $modal = el.modal();
        $modal.modal("show");
    }

    rejectProposal() {
        this.isWaitingForResponse = true;
        this.isSaveButtonDisabled = true;
        const payload = this.setPayload(false);
        this.model.token.rejectedReason =
            this.dataService.rejectProposal(payload)
                .subscribe(data => {
                    this.model.token.rejectedReason = null;

                    this.nsdForm.markAsPristine();
                    this.router.navigate(['/dashboard']);
                }, error => {
                    this.isWaitingForResponse = false;
                    this.isSaveButtonDisabled = true;
                    this.toastr.error(this.translation.translate('Dashboard.MessageRejectFailure') + error.message, "", { 'positionClass': 'toast-bottom-right' });
                    this.model.token.rejectedReason = null;
                });
    }

    disseminateProposal(onError: Function) {
        this.isWaitingForResponse = true;
        if (this.isGroupingEnabled) {
            const payload = this.setGroupPayload(false);
            this.dataService.disseminateGroupedProposal(payload)
                .subscribe(data => {
                    //TODO implement scheduler grouping this.saveScheduler(data);
                    this.isSaveButtonDisabled = true;
                    this.nsdForm.markAsPristine();
                    this.router.navigate(['/dashboard']);
                }, error => {
                    this.isWaitingForResponse = false;
                    if (onError)
                        onError.call(error);
                    this.toastr.error(this.translation.translate('Dashboard.MessageDisseminateFailure') + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        }
        else {
            const payload = this.setPayload(false);
            this.dataService.disseminateProposal(payload)
                .subscribe(data => {
                    this.saveScheduler(data);
                    this.isSaveButtonDisabled = true;
                    this.nsdForm.markAsPristine();
                    this.router.navigate(['/dashboard']);
                }, error => {
                    this.isWaitingForResponse = false;
                    if (onError)
                        onError.call(error);
                    this.toastr.error(this.translation.translate('Dashboard.MessageDisseminateFailure') + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        }
    }

    saveProposalAttchments() {
        const winObj = this.windowRef.nativeWindow;
        if (winObj["FormData"] !== undefined) {
            const fileUpload = $("#attachments").get(0);
            const files = fileUpload.files;


            var fileData = new FormData();

            // Looping over all files and add it to FormData object  
            for (let i = 0; i < files.length; i++) {
                fileData.append(files[i].name, files[i]);
            }

            fileData.append("proposalId", this.model.proposalId);
            fileData.append("userId", this.windowRef.appConfig.usrId);

            this.dataService.saveProposalAttachments(fileData)
                .subscribe(data => {
                    this.txtBackToReviewList = this.backToReviewListLabel;
                    this.model.attachments = this.model.attachments || [];
                    for (let iter = 0; iter < data.length; iter++) {
                        this.model.attachments.push({
                            id: data[iter].id,
                            userName: data[iter].userName,
                            fileName: data[iter].fileName,
                            readOnly: false
                        });
                    }
                    //const message = data.length > 1 
                    //    ? `All ${data.length} attachments where already uploaded to the server.`
                    //    : "The attachment was already uploaded to the server.";
                    const message = data.length > 1
                        ? this.translation.translate('Dashboard.MessageSaveAttachment1') + data.length + this.translation.translate('Dashboard.MessageSaveAttachment2')
                        : this.translation.translate('Dashboard.MessageSaveAttachment3');
                    this.toastr.success(message, "", { 'positionClass': 'toast-bottom-right' });

                    $("#attachments").val('');

                }, error => {
                    this.toastr.error(this.translation.translate('Dashboard.MessageSaveAttachmentError') + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        } else {
            this.toastr.error(this.translation.translate('Dashboard.MessageSaveAttachmentBrowser1'), this.translation.translate('Dashboard.MessageSaveAttachmentBrowser2'), { 'positionClass': 'toast-bottom-right' });
        }
    }

    downloadAttachment(id: string) {
        try {
            const url = `${window.location.protocol}//${window.location.host}/api/proposals/attachments/${id}`;
            const a = document.createElement('a');
            a.style.display = 'none';
            a.href = url;
            a.download = 'attachment';
            document.body.appendChild(a);
            a.click();
            window.URL.revokeObjectURL(url);
        }
        catch(e) {
            //td..
        }
    }

    deleteProposalAttchment(attachment: any) {
        this.dataService.deleteProposalAttachment(attachment)
            .subscribe(data => {
                const index = this.model.attachments.findIndex(x => x.id === attachment.id);
                if (index >= 0) {
                    this.model.attachments.splice(index, 1);
                }
            }, error => {
                this.toastr.error(this.translation.translate('Dashboard.MessageDeleteAttachmentError') + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    validateProposalAnnotation(index: number) {
        const control = this.annotations.at(index);
        return control.valid || this.nsdForm.pristine;
    }

    addProposalAnnotation() {
        this.annotations.push(this.buildAnnotation({}));
    }

    selectProposalAnnotation(index: number) {
        for (let i = 0; i < this.annotations.length; i++) {
            const control = this.annotations.at(i);
            if (i == index) {
                this.selectedProposalAnnotationId = control.value.id;
            }
            control.patchValue({ selected: i === index });
            control.markAsDirty();
        }
    }

    isDateTimePeriodDisable() {
        return this.isReadOnly || this.model.proposalType === ProposalType.Cancel;
    }

    get annotations(): FormArray {
        return <FormArray>this.nsdForm.get('annotations');
    };

    deleteProposalAnnotation() {
        const index = this.annotations.value.findIndex(x => x.selected === true);
        if (index >= 0) {
            this.annotations.removeAt(index);
            this.selectedProposalAnnotationId = -1;
        }
    }

    isRejectProposalDisable() {
        return !this.model.proposalId || this.isWaitingForResponse; // || !this.nsdForm.valid; // || this.model.proposalType === ProposalType.Cancel || this.model.proposalType === ProposalType.Replace; 
    }

    isParkProposalDisabled() {
        return !this.nsdForm.valid || this.isWaitingForResponse; // || this.model.proposalType !== ProposalType.New;
    }

    isGroupingProposalDisable() {
        return !this.nsdForm.valid || this.isGroupingEnabled;
    }

    toggleScheduler(event: any) {
        event.preventDefault();
    }

    selectSchedulerTab(tab, e) {
        switch (tab) {
            case 1:
                if (this.schedulerDate.hasEvents() || this.schedulerDayWeeks.hasEvents()) {
                    e.preventDefault();
                    e.stopPropagation();

                    return 0;
                }
                this.schedulerDaily.closePanelIfOpen();
                break;
            case 2: 
                if (this.schedulerDate.hasEvents() || this.schedulerDaily.hasEvents()) {
                    e.preventDefault();
                    e.stopPropagation();

                    return 0;
                }
                this.schedulerDayWeeks.closePanelIfOpen();
                break;
            case 3: 
                if (this.schedulerDayWeeks.hasEvents() || this.schedulerDaily.hasEvents()) {
                    e.preventDefault();
                    e.stopPropagation();
                    return 0;
                }
                this.schedulerDate.closePanelIfOpen();
                break;
            case 4:
                if (this.hasScheduler) {
                    let start = null;
                    let end = null;
                    let starttime = null;
                    let endtime = null;
                    switch (this.schedulerTab) {
                        case 1:
                            start = this.schedulerDaily.start;
                            end = this.schedulerDaily.end;
                            starttime = this.schedulerDaily.starttime;
                            endtime = this.schedulerDaily.endtime;
                            break;
                        case 2:
                            start = this.schedulerDayWeeks.start;
                            end = this.schedulerDayWeeks.end;
                            starttime = this.schedulerDayWeeks.starttime;
                            endtime = this.schedulerDayWeeks.endtime;
                            break;
                        case 3:
                            start = this.schedulerDate.start;
                            end = this.schedulerDate.end;
                            starttime = this.schedulerDate.starttime;
                            endtime = this.schedulerDate.endtime;
                            break;
                    }

                    if (start && end && starttime && endtime) {
                        this.schedulerFreeText.setDate(start, end, starttime, endtime);
                    }
                }
                break;
        }

        if (tab !== this.schedulerTab) {
            this.schedulerTab = tab;
        }
    }

    disableActivePeridDates(start, end, itemD?: string) {
        this.hasScheduler = true;
        this.nsdForm.get('grpDatePeriod.startActivity').disable();
        this.nsdForm.get('grpDatePeriod.endValidity').disable();

        this.nsdForm.get('grpDatePeriod.startActivity').setValue(moment(moment(start, 'DD/MM/YYYY HH:mm')).format(this.dateFormat));
        this.nsdForm.get('grpDatePeriod.endValidity').setValue(moment(moment(end, 'DD/MM/YYYY HH:mm')).format(this.dateFormat));

        this.updateFormControlValue('itemD', itemD);

        this.schedulerFreeText.setFreeText(itemD);
        this.nsdForm.markAsDirty();
    }

    enableActivePeridDates() {
        this.isScheduleOverridden = false;
        this.hasScheduler = false;

        let ctrlStartActivity = this.nsdForm.get('grpDatePeriod.startActivity');
        let ctrlEndValidity = this.nsdForm.get('grpDatePeriod.endValidity');

        ctrlStartActivity.setValue(null);
        ctrlEndValidity.setValue(null);

        ctrlStartActivity.enable();
        ctrlEndValidity.enable();

        this.clearModelItemD();
        this.schedulerFreeText.clearFormControls();
    }

    select2Options(): any {
        const self = this;
        const app = this.windowRef.nativeWindow['app'];

        return {
            language: {
                inputTooShort: function (args) {
                    return self.translation.translate('Schedule.InputTooShort');
                },
            },
            minimumInputLength: 2,
            maximumInputLength: 50,
            width: "100%",
            tags: true,
            createTag: function (params) {
                return {
                    id: params.term,
                    text: params.term,
                    newOption: true
                }
            },
            placeholder: {
                id: '-1',
                text: 'FullName'//this.translation.translate('Nsd.SdoSubjectPlaceholder')
            },
            ajax: {
                type: 'GET',
                url: app.apiUrl + "originatorinfo/filter",
                dataType: 'json',
                delay: 500,
                cache: false,
                headers: {
                    "RequestVerificationToken": app.antiForgeryTokenValue
                },
                data: function (parms, page) {
                    return {
                        Query: parms.term,
                        Size: 200,
                        OrgId: app.orgId
                    }
                },
                processResults: function (data) {
                    return {
                        results: data
                    }
                },
            },
            templateResult: function (data: any) {

                self.clearOriginatorPanel();

                let $result = $("<span></span>");

                let fullName = data.fullname ? data.fullname : '';
                let email = data.email ?  (' | ' + data.email) : '';
                let phone = data.phone ? (' | ' + data.phone) : '';

                $result.text(data.newOption ? data.text : (fullName + email + phone));

                return $result; 
            },
            templateSelection: function (selection: any, container) {
                if (selection && selection.id === "-1") {
                    return selection.fullname;
                }

                let modifiedOriginatorIdx = self.modifiedOriginators.findIndex(x => x.fullname == selection.fullname);
                if (modifiedOriginatorIdx >= 0) {
                    selection.phone = self.modifiedOriginators[modifiedOriginatorIdx].phone;
                    selection.email = self.modifiedOriginators[modifiedOriginatorIdx].email;
                }

                if (selection.newOption) {
                    self.updateOriginatorPanel("-1", selection.id, null, null);
                    return selection.text;
                } else if (selection.id && selection.fullname) {
                    self.updateOriginatorPanel(selection.id, selection.fullname, selection.email, selection.phone);
                } else {
                    self.updateOriginatorPanel("-1", selection.id, null, null);
                }
                 
                return selection.fullname;
            },
        }
    }

    isSaveDraftButtonDisabled() {
        //if (!this.isScheduleValid) {
        //    return true; 
        //}

        return this.isWaitingForResponse || this.isSaveButtonDisabled;
    }

    isSubmitProposalButtonDisabled() {
        //if (!this.isScheduleValid) {
        //    return true;
        //}

        return this.isWaitingForResponse || this.isSubmitButtonDisabled;
    }

    isAnyFullCalendarActive() {
        if (this.schedulerDaily.dateEvents.length > 0) {
            return true;
        }

        if (this.schedulerDayWeeks.hasEvents()) {
            return true;
        }

        if (this.schedulerDate.hasEvents()) {
            return true;
        }

        return false;
    }

    updateFormControlValue(controlName: string, value) {
        let control = this.nsdForm.get(controlName);
        if (control && control.value !== value) {
            if (controlName === "itemD") {
                this.model.itemD = value;
                this.model.token.itemD = value;
            }
            control.setValue(value);
        }
    }

    isAddOriginatorButtonDisabled() {
        let val = this.nsdForm.get('originator').value;
        return val === null || val === "60000000";
    }

    addOriginator() {
        const app = this.windowRef.nativeWindow['app'];

        let data: IOriginatorInfo = {
            organizationId: app.orgId,
            fullname: this.nsdForm.get('originator').value,
            email: this.nsdForm.get('originatorEmail').value,
            phone: this.nsdForm.get('originatorPhone').value
        } 

        let modifiedOriginatorIdx = this.modifiedOriginators.findIndex(x => x.fullname === data.fullname);
        if (modifiedOriginatorIdx >= 0) {
            this.modifiedOriginators[modifiedOriginatorIdx] = data;
        }
        else {
            this.modifiedOriginators.push(data);
        }

        this.dataService.addOriginatorInfo(data)
            .subscribe(data => {
                this.toastr.success(this.translation.translate('Nsd.SaveOriginatorSuccess'), "", { 'positionClass': 'toast-bottom-right' });
            }, error => {
                this.toastr.error(this.translation.translate('ErrorMsgs.AddOriginatorError'), "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    removeOriginator() {
        let modifiedOriginatorIdx = this.modifiedOriginators.findIndex(x => x.id == this.seletedOriginatorId);
        if (modifiedOriginatorIdx >= 0) {
            this.modifiedOriginators.splice(modifiedOriginatorIdx, 1);
        }

        this.dataService.removeOriginatorInfo(+this.seletedOriginatorId)
            .subscribe(data => {
                this.toastr.success(this.translation.translate('Nsd.RemoveOriginatorSuccess'), "", { 'positionClass': 'toast-bottom-right' });
                this.clearOriginatorPanel();
            }, error => {
                this.toastr.error("ErrorMsgs.RemoveOriginatorError", "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    clearOriginatorPanel() {
        this.seletedOriginatorId = "-1";

        this.nsdForm.get('originator').setValue(null);
        this.nsdForm.get('originatorEmail').setValue(null);
        this.nsdForm.get('originatorPhone').setValue(null);
    }

    updateOriginatorPanel(id, name, email, phone) {
        this.seletedOriginatorId = id;

        this.nsdForm.get('originator').setValue(name);
        this.nsdForm.get('originatorEmail').setValue(email);
        this.nsdForm.get('originatorPhone').setValue(phone);
    }

    loadMatchingOriginators(originatorName) {
        if (originatorName !== null) {
            this.dataService.findMatchingOriginatorInfos(originatorName, this.model.organizationId)
                .subscribe(data => {
                    if (data.length > 0) {
                        this.originators = data;
                        let index = this.originators.findIndex((x => x.fullname === originatorName));
                        this.seletedOriginatorId = index < 0 ? "-1" : this.originators[index].id + "";
                    } else {
                        this.setOriginator(originatorName);
                    }
                }, error => {
                    this.toastr.error(this.translation.translate('Dashboard.MessageIcaoError'), "", { 'positionClass': 'toast-bottom-right' });
                });
        } else {
            this.setOriginator(originatorName);
        }
    }

    getPrevIcaoText() {
        if (!this.model.notamId) {
            return;
        }

        let action = -1;

        if (this.model.status == ProposalStatus.Replaced ||
            this.model.status == ProposalStatus.Cancelled ||
            this.model.status == ProposalStatus.Submitted ||
            this.model.status == ProposalStatus.Withdrawn ||
            this.model.status == ProposalStatus.Rejected ||
            (this.model.status == ProposalStatus.Picked && (this.model.proposalType == ProposalType.Replace || this.model.proposalType == ProposalType.Cancel))) {
            action = 1;
        }

        this.loadingPrevIcao = true;
        this.dataService.getPrevNotamIcaoText(this.model.notamId, action)
            .subscribe(data => {
                if (data) {
                    this.isPrevIcaoTabVisible = true;
                }
                this.prevIcaoText = data;
                this.loadingPrevIcao = false;
            }, error => {

                this.loadingPrevIcao = false;
            });
    }

    clearModelItemD() {
        this.model.token.itemD = "";
        this.model.itemD = "";

        this.updateFormControlValue("itemD", this.model.token.itemD);

        this.schedulerFreeText.clearFormControls();
    }

    private setOriginator(originatorName) {
        this.originators = [{ id: "60000000", fullname: originatorName }];
        this.seletedOriginatorId = "60000000";
    }

    private generateIcao(data) {
        if (this.model.proposalType === ProposalType.Cancel || this.nsdForm.dirty && (this.isGroupingEnabled || !this.isReadOnly)) {
            this.isSaveButtonDisabled = !this.nsdForm.valid;
            this.isSubmitButtonDisabled = !this.nsdForm.valid;
            if (this.nsdForm.valid) {
                this.isSubmitButtonDisabled = this.model.status === ProposalStatus.Submitted || this.model.status === ProposalStatus.Terminated;
                this.resetIcaoModel();
                const payload = this.setPayload(true);

                this.waitingForGeneratingIcao = true;
                this.dataService.generateIcao(payload)
                    .subscribe(data => {
                        this.model.token.overrideBilingual = this.overrideBilingual;
                        this.model.token.isBilingual = data.token.isBilingual; // get the isBilingual flag from the ICAO generation
                        data.token = this.model.token; //don't override tokens with server response
                        data.groupId = this.model.groupId;
                        data.rejectionReason = this.model.rejectionReason;
                        this.updateModel(data)
                        this.waitingForGeneratingIcao = false;
                        //this.activateTab('icao');
                    }, error => {
                        this.waitingForGeneratingIcao = false;
                        this.isSaveButtonDisabled = true;
                        this.isSubmitButtonDisabled = true;
                        let errorMessage = error.message;
                        if (error.exceptionMessage) {
                            errorMessage += `${errorMessage} ${error.exceptionMessage}`;
                        }
                        this.toastr.error(this.translation.translate('Dashboard.MessageIcaoError') + errorMessage, "", { 'positionClass': 'toast-bottom-right' });
                    });
            } else {
                this.waitingForGeneratingIcao = false;               
                this.model.itemE = "";
                this.model.itemEFrench = "";                
            }
        } else {
            this.waitingForGeneratingIcao = false;
        }
    }

    private setPayload(toGenerateIcao: boolean): any {
        const grpDatePeriod: FormControl = <FormControl>this.nsdForm.controls["grpDatePeriod"];

        this.model.token.overrideBilingual = this.overrideBilingual;

        const immediate = grpDatePeriod.get('immediate');
        if (immediate) {
            this.model.token.immediate = immediate.value;
        }
        const permanent = grpDatePeriod.get('permanent');
        if (permanent) {
            this.model.token.permanent = permanent.value;
        }
        const estimated = this.nsdForm.get("grpDatePeriod.estimated");
        if (estimated) {
            this.model.token.estimated = estimated.value;
        }

        if (this.hasScheduler) {
            const itemD = this.schedulerFreeText.getFreeText();
            if (itemD) {
                if (estimated.value || permanent.value || immediate.value) {
                    this.model.itemD = "";
                    this.model.token.itemD = "";
                } else {
                    this.model.itemD = itemD;
                    this.model.token.itemD = itemD;
                }
            }
        }
        const originator = this.nsdForm.get("originator");
        if (originator) {
            this.model.token.originator = originator.value;
        }
        const originatorEmail = this.nsdForm.get("originatorEmail");
        if (originatorEmail) {
            this.model.token.originatorEmail = originatorEmail.value;
        }
        const originatorPhone = this.nsdForm.get("originatorPhone");
        if (originatorPhone) {
            this.model.token.originatorPhone = originatorPhone.value;
        }

        if (!toGenerateIcao) {
            this.model.token.annotations = this.model.token.annotations || [];
            for (let i = 0; i < this.annotations.value.length; i++) {
                const annotation = this.annotations.value[i];
                if (annotation.id > 0) {
                    const tokenAnnotationIndex = this.model.token.annotations.findIndex(x => x.id === annotation.id);
                    if (tokenAnnotationIndex >= 0) { //should always exist
                        this.model.token.annotations[tokenAnnotationIndex].note = annotation.note;
                    }
                } else { //add when new
                    this.model.token.annotations.push(annotation);
                }
            }
        }


        const endValidity = grpDatePeriod.get('endValidity');
        if (endValidity && endValidity.value) {
            this.model.token.endValidity = moment.utc(endValidity.value, 'YYMMDDHHmm');
        }
        const startActivity = grpDatePeriod.get('startActivity');
        if (startActivity && startActivity.value) {
            this.model.token.startActivity = moment.utc(startActivity.value, 'YYMMDDHHmm');
        }

        const urgent = this.nsdForm.get("urgent");
        this.model.token.urgent = urgent && urgent.value === true;

        const amendPubMessage = this.nsdForm.get("amendPubMessage");
        if (amendPubMessage) {
            this.model.token.amendPubMessage = amendPubMessage.value;
        }

        const amendPubMessageFrench = this.nsdForm.get("amendPubMessageFrench");
        if (amendPubMessageFrench) {
            this.model.token.amendPubMessageFrench = amendPubMessageFrench.value;
        }

        this.setNsdPayload();

        let payload: any = {
            categoryId: this.model.categoryId,
            version: this.model.version,
            name: this.model.name,
            token: this.model.token,
            referredSeries: this.model.referredSeries,
            referredNumber: this.model.referredNumber,
            referredYear: this.model.referredYear,
            proposalType: this.model.proposalType,
            organizationId: this.model.organizationId,
            proposalId: this.model.proposalId,
            status: this.model.status,
            statusUpdated: this.model.statusUpdated,
            parentNotamId: this.model.parentNotamId,
            notamId: this.model.notamId,
            groupId: this.model.groupId,
            number: this.model.number,
            year: this.model.year,
            rowVersion: this.model.rowVersion
        }

        if (!toGenerateIcao) {
            //payload.organizationId = this.model.organizationId;
            //payload.proposalId = this.model.proposalId;
            //payload.status = this.model.status;
        }

        return payload;
    }    

    private setGroupPayload(toGenerateIcao: boolean): any {
        let payload = [];

        let data = this.setPayload(toGenerateIcao);
        for (let model of this.groupModel) {
            if (!model.isSelected) {
                payload.push(model);
            } else {
                //data.isSelected = true;
                data.groupId = model.groupId;
                data.isGroupMaster = model.isGroupMaster;
                data.grouped = model.grouped;
                payload.push(data);
            }
        }

        return payload;
    }

    private checkScheduleButtonVisiblity(): boolean {
        const immediateCtrl = this.nsdForm.get('grpDatePeriod.immediate');
        const permanentCtrl = this.nsdForm.get('grpDatePeriod.permanent');
        const estimatedCtrl = this.nsdForm.get('grpDatePeriod.estimated');

        if (!immediateCtrl.value && !permanentCtrl.value && !estimatedCtrl.value) {
            return true;
        }

        return false;
    }

    private loadScheduler(proposalId) {
        this.dataService.loadItemD(proposalId)
            .subscribe(data => {
                if (data.length > 0) {
                    this.hasScheduler = true;
                    this.isSchedulePanelOpen = true;
                    this.schedulerTab = data[0].calendarType;
                    switch (this.schedulerTab) {
                        case 1:
                            this.schedulerDaily.initialize(data, this.model.startActivity, this.model.endValidity, null);
                            this.schedulerDaily.parseEvents();
                            this.schedulerDaily.slideScheduleClose();
                            break;
                        case 2:
                            this.schedulerDayWeeks.initialize(data, this.model.startActivity, this.model.endValidity, null);
                            this.schedulerDayWeeks.parseEvents();
                            this.schedulerDayWeeks.slideScheduleClose();
                            break;
                        case 3:
                            this.schedulerDate.initialize(data, this.model.startActivity, this.model.endValidity, null);
                            this.schedulerDate.parseEvents();
                            this.schedulerDate.slideScheduleClose();
                            break;
                        case 4:
                            this.isScheduleOverridden = true;
                            this.schedulerFreeText.initialize(data, this.model.startActivity, this.model.endValidity);
                            this.schedulerDaily.slideScheduleClose();
                            this.schedulerDayWeeks.slideScheduleClose();
                            this.schedulerDate.slideScheduleClose();
                            break;
                    }
                }
            }, error => {
                console.log(error);
            });
    }

    private getMasterScheduler(itemDs: ItemD[], calendarId, proposalId): ItemD[] {
        let cEvents = this.schedulerDate.getCalendarEvents(calendarId);

        for (let i = 0; i < cEvents.length; i++) {
            let cEvent = cEvents[i];

            let event = {
                id: cEvent.id,
                start: moment.utc(cEvent.start).format(),
                end: cEvent.end ? moment.utc(cEvent.end).format() : null,
                linked: cEvent.linked,
                groupId: cEvent.groupId,
                eventColor: cEvent.eventColor,
                extendedHours: cEvent.extendedHours,
                previousColor: cEvent.previousColor
            };

            let itemD = {
                calendarType: 3,
                proposalId: proposalId,
                calendarId: calendarId,
                event: JSON.stringify(event)
            };

            itemDs.push(itemD);
        }
        return itemDs;
    }

    private getDetailScheduler(itemDs: ItemD[], calendarId, proposalId): ItemD[] {
        let cEvents = this.schedulerDate.getCalendarEvents(calendarId);

        for (let i = 0; i < cEvents.length; i++) {
            let cEvent = cEvents[i];

            let event = {
                id: cEvent._id || cEvent.id,
                start: moment.utc(cEvent.start).format(),
                end: moment.utc(cEvent.end).format(),
                linked: cEvent.linked,
                groupId: cEvent.groupId,
                eventColor: cEvent.eventColor,
                previousColor: cEvent.previousColor,
                parentId: cEvent.parentId,
                s_sunrise: cEvent.s_sunrise,
                s_sunset: cEvent.s_sunset,
                e_sunrise: cEvent.e_sunrise,
                e_sunset: cEvent.e_sunset,
                exStarttime: cEvent.exStarttime,
                exEndtime: cEvent.exEndtime,
                extendedHours: cEvent.extendedHours
            };

            let itemD = {
                calendarType: 3,
                proposalId: proposalId,
                calendarId: calendarId,
                eventId: event.id,
                event: JSON.stringify(event)
            };

            itemDs.push(itemD);

        }
        return itemDs;
    }

    private saveDailyScheduler(proposalId) {
        let itemDs : ItemD[] = [];

        let cEvents = this.schedulerDaily.getCalendarEvents();
        for (let i = 0; i < cEvents.length; i++) {
            let cEvent = cEvents[i];

            let event = {
                id: cEvent._id || cEvent.id,
                start: moment.utc(cEvent.start).format(),
                end: moment.utc(cEvent.end).format(),
                linked: cEvent.linked,
                groupId: cEvent.groupId,
                eventColor: cEvent.eventColor,
                previousColor: cEvent.previousColor,
                parentId: cEvent.parentId,
                s_sunset: cEvent.s_sunset,
                s_sunrise: cEvent.s_sunrise,
                e_sunset: cEvent.e_sunset,
                e_sunrise: cEvent.e_sunrise,
                exStarttime: cEvent.exStarttime,
                exEndtime: cEvent.exEndtime,
                extendedHours: cEvent.extendedHours
            };

            let itemD = {
                calendarType: 1,
                proposalId: proposalId,
                calendarId: -1,
                eventId: event.id,
                itemD: this.model.itemD, 
                event: JSON.stringify(event)
            };

            itemDs.push(itemD);
        }
        this.dataService.saveItemD(itemDs)
            .subscribe(data => {
            }, error => {
            });

    }

    private saveWeeklyScheduler(proposalId) {
        let cEvents = this.schedulerDayWeeks.getCalendarEvents();

        let itemDs: ItemD[] = [];
        for (let i = 0; i < cEvents.length; i++) {
            let cEvent = cEvents[i];

            let event = {
                id: cEvent._id || cEvent.id,
                start: moment.utc(cEvent.start).format(),
                end: moment.utc(cEvent.end).format(),
                linked: cEvent.linked,
                groupId: cEvent.groupId,
                eventColor: cEvent.eventColor,
                previousColor: cEvent.previousColor,
                parentId: cEvent.parentId,
                s_sunset: cEvent.s_sunset,
                s_sunrise: cEvent.s_sunrise,
                e_sunset: cEvent.e_sunset,
                e_sunrise: cEvent.e_sunrise,
                s_setTime: cEvent.s_setTime,
                e_setTime: cEvent.e_setTime
            };

            let itemD = {
                calendarType: 2,
                proposalId: proposalId,
                calendarId: -1,
                eventId: event.id,
                itemD: this.model.itemD, 
                event: JSON.stringify(event)
            };

            itemDs.push(itemD);
        }
        this.dataService.saveItemD(itemDs)
            .subscribe(data => {
            }, error => {
            });
    }

    private saveFreeTextScheduler(proposalId) {
        let freeText = this.schedulerFreeText.getFreeText();

        let itemDs: ItemD[] = [];

        let event = {
            itemD: freeText
        };
        let itemD = {
            calendarType: 4,
            proposalId: proposalId,
            calendarId: -1,
            eventId: -1,
            itemD: this.model.itemD, 
            event: JSON.stringify(event)
        };

        itemDs.push(itemD);

        this.dataService.saveItemD(itemDs)
            .subscribe(data => {
            }, error => {
            });
    }

    private updateReviewListFilter(proposalId) {
        //updating the filter to show and select the latest item saved in the review list;
        const queryOptions = this.memStorageService.get(this.memStorageService.PROPOSAL_QUERY_OPTIONS_KEY);
        if (queryOptions) {
            queryOptions.sort = "Received";
            queryOptions.page = 1;
            this.memStorageService.save(this.memStorageService.PROPOSAL_QUERY_OPTIONS_KEY, queryOptions);
        }
        this.memStorageService.save(this.memStorageService.PROPOSAL_ID_KEY, proposalId);
    }

    private setNsdPayload(): void {

        const frmArrControl = <FormArray>this.nsdForm.controls['frmArr'];
        const frmArrControls = <FormArray>frmArrControl.controls[0];

        for (let key in frmArrControls.controls) {
            //first we need to clean all the Array controls on the token
            if (this.model.token[key] instanceof Array) {
                this.model.token[key] = [];
            }
            if (frmArrControls.controls.hasOwnProperty(key)) {
                const control: FormControl = <FormControl>frmArrControls.controls[key];
                if (control && control.value !== null) {
                    if (typeof (control.value) === "object") {
                        var grp: FormGroup = <FormGroup>frmArrControls.controls[key];
                        const grpObject = grp.controls;
                        if (key in this.model.token) {
                            const content = typeof (control) === "object" ? control.value : control;
                            this.model.token[key] = content.value ? content.value : content;
                        }
                        else {
                            for (let grpKey in grpObject) {
                                if (grpObject.hasOwnProperty(grpKey)) {
                                    const grpControl: FormControl = <FormControl>grpObject[grpKey];
                                    if (grpControl !== null) {
                                        if (control.value instanceof Array && key in this.model.token) {
                                            this.model.token[key].push(control.value[grpKey]);
                                        }
                                        else {
                                            if (grpKey in this.model.token) {
                                                const value = typeof (grpControl) === "object" ? grpControl.value : grpControl;
                                                this.model.token[grpKey] = value;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else {
                        if (key in this.model.token) {
                            const value = typeof (control) === "object" ? control.value : control;
                            this.model.token[key] = control.value;
                        }
                    }
                }
            }
        }
    }

    private resetIcaoModel() {
        this.model.code23 = "";
        this.model.code45 = "";
        this.model.radius = 0;
        this.model.lowerLimit = 0;
        this.model.upperLimit = 0;
        this.model.scope = "";
        this.model.series = "";
        this.model.startActivity = null;
        this.model.endValidity = null;
        this.model.fir = "";
        this.model.itemA = "";
        this.model.itemD = "";
        this.model.itemE = "";
        this.model.itemEFrench = "";
        this.model.itemF = 0;
        this.model.itemF = 0;
    }

    private updateModel(data: any) {
        data.organizationId = data.organizationId ? data.organizationId : this.model.organizationId;
        data.proposalId = data.proposalId ? data.proposalId : this.model.proposalId;

        const attachments = this.model.attachments;

        this.model = data;
        this.model.attachments = this.model.attachments || attachments;
    }

    private configureReactiveForm() {
        if (this.model.proposalType === ProposalType.Cancel) {
            this.model.token.startActivity = null;
            this.model.token.immediate = true;
            this.model.token.endValidity = null;
            this.model.token.permanent = false;
            this.model.token.estimated = false;
            this.model.token.itemD = null;
        }
        else {
            if (this.model.token.startActivity) this.startActivityText = moment.utc(this.model.token.startActivity).format("YYYY/MM/DD, H:mm") + " UTC";
            if (this.model.token.endValidity) this.endValidityText = moment.utc(this.model.token.endValidity).format("YYYY/MM/DD, H:mm") + " UTC";
        }

        this.startActivityMoment = this.model.token.startActivity ? moment.utc(this.model.token.startActivity).format(this.dateFormat) : null;
        this.endValidityMoment = this.model.token.endValidity ? moment.utc(this.model.token.endValidity).format(this.dateFormat) : null;
        this.nsdForm = this.fb.group({
            originator: [{ value: this.model.token.originator, disabled: this.isReadOnly }, [Validators.required, Validators.pattern('[a-zA-Z].*')]],
            originatorEmail: [{ value: this.model.token.originatorEmail, disabled: this.isReadOnly }],
            originatorPhone: [{ value: this.model.token.originatorPhone, disabled: this.isReadOnly }],
            grpDatePeriod: this.fb.group({
                immediate: { value: this.model.token.immediate, disabled: this.isReadOnly || this.model.proposalType === ProposalType.Cancel },
                startActivity: [{ value: this.startActivityMoment, disabled: this.isReadOnly || this.model.proposalType === ProposalType.Cancel || this.model.token.immediate }],
                permanent: { value: this.model.token.permanent, disabled: this.isReadOnly || this.model.proposalType === ProposalType.Cancel },
                endValidity: { value: this.endValidityMoment, disabled: this.isReadOnly || this.model.proposalType === ProposalType.Cancel || this.model.token.permanent },
                estimated: { value: this.model.token.estimated, disabled: this.isReadOnly || this.model.proposalType === ProposalType.Cancel },
            }, { validator: dateRangeValidator }),
            amendPubMessage: { value: this.model.token.amendPubMessage, disabled: this.isReadOnly },
            amendPubMessageFrench: { value: this.model.token.amendPubMessageFrench, disabled: this.isReadOnly },
            urgent: { value: this.model.token.urgent, disabled: this.isReadOnly },
            annotations: this.fb.array([]),
            //itemD: { value: this.model.token.itemD, disabled: this.isReadOnly || this.model.proposalType === ProposalType.Cancel},
            itemD: { value: this.model.token.itemD, disabled: false },
            frmArr: this.fb.array([]),
            overrideBilingual: [{ value: this.model.token.overrideBilingual, disabled: this.model.token.isBilingualRegion }],
        });

        if (this.model.token.annotations) {
            for (let i = 0; i < this.model.token.annotations.length; i++) {
                this.annotations.push(this.buildAnnotation(this.model.token.annotations[i]));
            }
        }
    }


    private buildAnnotation(annotation: any) {
        return this.fb.group({
            id: annotation.id || 0,
            note: [{ value: annotation.note || "", disabled: this.isReadOnly }, [Validators.required]],
            userId: annotation.userId || this.windowRef.appConfig.usrName,
            created: moment.utc(annotation.received) || moment(),
            selected: false,
        });
    }

    private canSubmitFromStatus(status: number): boolean {
        var result = (status === ProposalStatus.Draft ||
            status === ProposalStatus.Withdrawn ||
            status === ProposalStatus.Cancelled ||
            status === ProposalStatus.Picked ||
            status === ProposalStatus.ParkedPicked ||
            status === ProposalStatus.Taken);
        return result;
        //return status === ProposalStatus.Draft || status === ProposalStatus.Withdrawn || status === ProposalStatus.Cancelled || status === ProposalStatus.Picked || status === ProposalStatus.ParkedPicked;
    }

    private isANumber(str: string): boolean {
        return !/\D/.test(str);
    }

    // this is nessecary because of how we load the map in a modal window
    private initMap() {
        const winObj = this.windowRef.nativeWindow;
        const leafletmap = winObj['app'].leafletmap;

        if (leafletmap) {
            leafletmap.initMap();
        }
    }

    private mapHealthCheck() {
        this.dataService.getMapHealth().subscribe(response => {
            if (response) {
                this.mapHealthy = true;
            }
        }, error => {
            this.mapHealthy = false;
            return false;
        });
    }

    mapAvailable(): boolean {
        return this.mapHealthy;
    }

    private validateMap() {
        const winObj = this.windowRef.nativeWindow;
        const leafletmap = winObj['app'].leafletmap;
        if (leafletmap) {
            leafletmap.resizeMapForModal();
        }
    }

    private validateGeoMap() {
        const winObj = this.windowRef.nativeWindow;
        const leafletgeomap = winObj['app'].leafletgeomap;
        if (leafletgeomap) {
            leafletgeomap.resizeGeoMapForModal();
        }
    }


    private disposeMap() {
        const winObj = this.windowRef.nativeWindow;
        const leafletmap = winObj['app'].leafletmap;
//        const leafletgeomap = winObj['app'].leafletgeomap;
        if (leafletmap) {
            leafletmap.dispose();
        }
        //if (leafletgeomap) {
        //    leafletgeomap.disposeGeoMap();
        //}
    }

    private getActiveSchedule() {
        if (this.schedulerDaily.hasEvents()) {
            return 1;
        } else if (this.schedulerDayWeeks.hasEvents()) {
            return 2;
        } else if (this.schedulerDate.hasEvents()) {
            return 3;
        } else {
            return this.schedulerTab //this should be itemD text (4)
        }
    }

    private saveScheduler(data) {
        if (this.hasScheduler) {
            let activeSchedule = this.getActiveSchedule();
            switch (activeSchedule) {
                case 1:
                    this.saveDailyScheduler(data.proposalId); break;
                case 2:
                    this.saveWeeklyScheduler(data.proposalId); break;
                case 3:
                    let itemDs: ItemD[] = [];
                    itemDs = this.getMasterScheduler(itemDs, 1, data.proposalId);
                    itemDs = this.getMasterScheduler(itemDs, 2, data.proposalId);
                    itemDs = this.getMasterScheduler(itemDs, 3, data.proposalId);
                    itemDs = this.getDetailScheduler(itemDs, 4, data.proposalId);

                    this.dataService.saveItemD(itemDs)
                        .subscribe(data => {
                        }, error => {
                        });

                    break;
                case 4:
                    this.saveFreeTextScheduler(data.proposalId); break;
            }
        }
    }

    // Force Bilingual
    displayBilingual(): boolean {

        if (this.model.status === 0) {
            this.dataService.override.subscribe(value => this.overrideBilingual = value);
        }
        const value = this.isBilingualRegion || this.overrideBilingual;
        return value;
    }
}

//Custom Validators
function dateRangeValidator(c: AbstractControl): { [key: string]: boolean } | null {
    const startActivityCtrl = c.get("startActivity");
    const endValidityCrl = c.get("endValidity");
    const immediateCtrl = c.get("immediate");
    const permanentCtrl = c.get("permanent");

    let startActivityErrors: { [key: string]: boolean } | null = null;
    let endValidityErrors: { [key: string]: boolean } | null = null;

    if (!immediateCtrl.value ) {

        if (startActivityCtrl.value && startActivityCtrl.value.length > 0 && startActivityCtrl.value.length < 10) {
            startActivityErrors = {
                'invalidStartActivityTenDigitDate': true
            };
        }

        const startDate = startActivityCtrl.value ? moment.utc(startActivityCtrl.value, 'YYMMDDHHmm') : null;
        if (startDate && startDate.isValid()) {
            const now = moment.utc(moment().utc().format("MM/DD/YYYY, H:mm"));
            const minutes = moment.utc(startDate, "MM/DD/YYYY, H:mm").diff(now, 'minutes');
            if (minutes < 0) {
                if (startActivityErrors === null) {
                    startActivityErrors = {
                        'validateOnlyInFuture': true
                    };
                }
            }
        } else {
            if (startActivityErrors === null) {
                startActivityErrors = {
                    'invalidStartActivityTenDigitDate': true
                };
            }
        }
    }

    if (!permanentCtrl.value) {
        if (endValidityCrl.value && endValidityCrl.value.length > 0 && endValidityCrl.value.length < 10) {
            endValidityErrors = {
                'invalidEndValidityTenDigitDate': true
            };
        }

        let startDate = immediateCtrl.value
            ? moment.utc()
            : (startActivityCtrl.value ? moment.utc(startActivityCtrl.value, 'YYMMDDHHmm') : moment.utc())

        const endValidityDate = endValidityCrl.value ? moment.utc(endValidityCrl.value, 'YYMMDDHHmm') : null;

        if (endValidityDate && endValidityDate.isValid()) {
            const minutes = endValidityDate.diff(startDate, 'minutes');
            const days = endValidityDate.diff(startDate, 'days');

            if (days > 92) {
                endValidityErrors = {
                    'validateLaterThan92Days': true
                };
            }
            if (endValidityCrl.value) {
                let mvp = +window["app"].mvp || 30;
                if (minutes < mvp) {
                    if (endValidityErrors === null) {
                        endValidityErrors = {
                            'validateLaterThan30MinStartingDate': true
                        };
                    }
                }
            }
        } else {
            if (endValidityErrors === null) {
                endValidityErrors = {
                    'invalidEndValidityTenDigitDate': true
                };
            }
        }
    }

    if (startActivityErrors === null && endValidityErrors === null)
        return null;

    if (startActivityErrors !== null && endValidityErrors !== null) {
        return Object.assign(startActivityErrors, endValidityErrors);
    } else if (startActivityErrors !== null) {
        return startActivityErrors;
    } else {
        return endValidityErrors;
    }

}

Number.prototype["pad"] = function (size) {
    let s = String(this);
    while (s.length < (size || 2)) {
        s = "0" + s;
    }
    return s;
}

function emailWidthDomainValidator(c: AbstractControl): { [key: string]: boolean } | null {
    const email = c.value;
    var re = /\S+@\S+\.\S+/;
    if (email && !re.test(email))
        return { email: true };
    return null;
}
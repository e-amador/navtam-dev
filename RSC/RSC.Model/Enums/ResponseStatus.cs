﻿namespace RSC.Model.Enums
{
    public enum ResponseStatus : int
    {
        Ok = 0,
        Warning = 1,
        Error = 2
    }
}

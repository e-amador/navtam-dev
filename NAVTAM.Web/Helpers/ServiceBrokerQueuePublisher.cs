﻿using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.SqlServiceBroker;
using NavCanada.Core.TaskEngine.Mto;
using System;

namespace NAVTAM.Helpers
{
    public class ServiceBrokerQueuePublisher : INotamQueuePublisher
    {
        public void Publish(IUow uow, MessageTransferObject mto)
        {
            try
            {
                // run steve's exe to test queues
                var taskEngineQueue = new SqlServiceBrokerQueue(uow.DbContext, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings);
                var initiatorQueue = new SqlServiceBrokerQueue(uow.DbContext, SqlServiceBrokerQueueSettings.TaskEngineToInitiatorSsbSettings);
                taskEngineQueue.Publish(mto);
            }
            catch (Exception ex)
            {
                var message = $"Error publishing Mto to the Task Engine queue. Details: {ex.Message}";
                throw new Exception(message, ex);
            }
        }
    }
}
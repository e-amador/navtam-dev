﻿import { Routes } from '@angular/router';

import {
    UserListComponent,
    UserEditComponent,
    UserNewComponent,
    UserResetPasswordComponent,
    OrgListComponent,
    OrgNewComponent,
    OrgEditComponent,
    IcaoSubjectsListComponent,
    IcaoSubjectEditComponent,
    IcaoSubjectNewComponent,
    IcaoConditionEditComponent,
    IcaoConditionNewComponent,
    SeriesListComponent,
    SeriesNewComponent,
    SeriesEditComponent,
    ADCTabListComponent,
    ADCAddSdoComponent,
    ADCEditComponent,
    DoasListComponent,
    DoaNewComponent,
    DoaEditComponent,
    DoaEditModifyComponent,
    BilingualRegionEditComponent,
    NorthernRegionEditComponent,
    NsdListComponent,
    GeoRegionListComponent,
    GeoRegionShowComponent,
    GeoRegionNewComponent,
    ConfigListComponent,
    ConfigNewComponent,
    ChecklistComponent,
    SeriesChecklistNewComponent,
    AerordsComponent,
    SubscComponent,
    SubscEditComponent,
    SubscCreateComponent,
    //resolvers
    RolesResolver,
    UserModelResolver,
    OrgModelResolver,
    IcaoSubjectModelResolver,
    IcaoConditionModelResolver,
    SeriesModelResolver,
    AhpModelResolver,
    AdcModelResolver,
    DoaModelResolver,
    DoaCopyModelResolver,
    GeoRegionModelResolver,
    BilingualRegionModelResolver,
    NorthernRegionModelResolver,
    ConfigModelResolver,
    ChecklistModelResolver,
    SeriesChecklistResolver,
    SubscriptionResolver,
    //NsdResolver
} from './index'

export const adminRoutes: Routes = [
    { path: 'administration', redirectTo: 'administration/users', pathMatch: 'full' },
    { path: 'path/*', redirectTo: 'administration/users', pathMatch: 'full' },

    // USERS
    { path: 'administration/users', component: UserListComponent, resolve: { roles: RolesResolver } },
    { path: 'administration/users/create', component: UserNewComponent },
    {
        path: 'administration/users/edit/:id', component: UserEditComponent,
        data: [{ action: 'edit' }],
        resolve: { model: UserModelResolver }
    },
    {
        path: 'administration/users/view/:id', component: UserEditComponent,
        data: [{ action: 'view' }],
        resolve: { model: UserModelResolver }
    },
    { path: 'administration/users/resetpassword/:id', component: UserResetPasswordComponent, resolve: { model: UserModelResolver } },

    //ORG
    { path: 'administration/orgs', component: OrgListComponent },
    { path: 'administration/orgs/create', component: OrgNewComponent },
    {
        path: 'administration/orgs/edit/:id', component: OrgEditComponent,
        data: [{ action: 'edit' }],
        resolve: { model: OrgModelResolver }
    },
    {
        path: 'administration/orgs/view/:id', component: OrgEditComponent,
        data: [{ action: 'view' }],
        resolve: { model: OrgModelResolver }
    },

    //ICAO Subjects & Conditions
    { path: 'administration/icaosubjects', component: IcaoSubjectsListComponent },
    { path: 'administration/icaosubjects/create', component: IcaoSubjectNewComponent },
    { path: 'administration/icaosubjects/createcondition/:subjectId', component: IcaoConditionNewComponent },
    {
        path: 'administration/icaosubjects/edit/:id', component: IcaoSubjectEditComponent,
        data: [{ action: 'edit' }],
        resolve: { model: IcaoSubjectModelResolver }
    },
    {
        path: 'administration/icaosubjects/view/:id', component: IcaoSubjectEditComponent,
        data: [{ action: 'view' }],
        resolve: { model: IcaoSubjectModelResolver }
    },
    {
        path: 'administration/icaosubjects/editcondition/:id', component: IcaoConditionEditComponent,
        data: [{ action: 'edit' }],
        resolve: { model: IcaoConditionModelResolver }
    },
    {
        path: 'administration/icaosubjects/viewcondition/:id', component: IcaoConditionEditComponent,
        data: [{ action: 'view' }],
        resolve: { model: IcaoConditionModelResolver }
    },

    //SERIES ALLOCATION
    { path: 'administration/series', component: SeriesListComponent },
    { path: 'administration/series/create', component: SeriesNewComponent },
    {
        path: 'administration/series/edit/:id', component: SeriesEditComponent,
        data: [{ action: 'edit' }],
        resolve: { model: SeriesModelResolver }
    },
    {
        path: 'administration/series/view/:id', component: SeriesEditComponent,
        data: [{ action: 'view' }],
        resolve: { model: SeriesModelResolver }
    },

    //AERODROME DISSEMINATION CATEGORY
    { path: 'administration/adc-tab', component: ADCTabListComponent },
    {
        path: 'administration/adc/add/:id', component: ADCAddSdoComponent,
        data: [{ action: 'add' }],
        resolve: { model: AhpModelResolver }
    },
    {
        path: 'administration/adc/edit/:id', component: ADCEditComponent,
        data: [{ action: 'edit' }],
        resolve: { model: AdcModelResolver }
    },

    //DOMAIN OF AUTHORITY
    { path: 'administration/doas', component: DoasListComponent },
    {
        path: 'administration/doas/create', component: DoaNewComponent,
        data: [{ action: 'create' }],
    },
    {
        path: 'administration/doas/view/:id', component: DoaEditComponent,
        data: [{ action: 'view' }],
        resolve: { model: DoaModelResolver }
    },
    {
        path: 'administration/doas/edit-modify/:id', component: DoaEditModifyComponent,
        data: [{ action: 'edit' }],
        resolve: { model: DoaModelResolver }
    },
    {
        path: 'administration/doas/copy/:id', component: DoaEditModifyComponent, 
        data: [{ action: 'copy' }],
        resolve: { model: DoaCopyModelResolver }
    },
    //NOTAM SCENARIO DEFINITIONS
    { path: 'administration/nsds', component: NsdListComponent },
    //Bilingual Region
    {
        path: 'administration/bregion/edit', component: BilingualRegionEditComponent,
        data: [{ action: 'edit' }],
        resolve: { model: BilingualRegionModelResolver }
    },
    //Northern Region
    {
        path: 'administration/nregion/edit', component: NorthernRegionEditComponent,
        data: [{ action: 'edit' }],
        resolve: { model: NorthernRegionModelResolver }
    },
    //GeoRegion
    { path: 'administration/georegion', component: GeoRegionListComponent },
    {
        path: 'administration/georegion/view/:id', component: GeoRegionShowComponent,
        data: [{ action: 'view' }],
        resolve: { model: GeoRegionModelResolver }
    },
    {
        path: 'administration/georegion/create', component: GeoRegionNewComponent,
        data: [{ action: 'create' }],
    },
    {
        path: 'administration/georegion/create/:id', component: GeoRegionNewComponent,
        data: [{ action: 'edit' }],
        resolve: { model: GeoRegionModelResolver }
    },
    //Subscription
    { path: 'administration/subscription', component: SubscComponent },
    {
        path: 'administration/subscription/view/:id', component: SubscEditComponent,
        data: [{ action: 'view' }],
        resolve: { model: SubscriptionResolver }
    },
    {
        path: 'administration/subscription/create', component: SubscCreateComponent,
        data: [{ action: 'create' }],
    },
    {
        path: 'administration/subscription/edit/:id', component: SubscEditComponent,
        data: [{ action: 'edit' }],
        resolve: { model: SubscriptionResolver }
    },
    //CONFIG
    { path: 'administration/config', component: ConfigListComponent },
    {
        path: 'administration/config/create', component: ConfigNewComponent,
        data: [{ action: 'create' }],
    },
    {
        path: 'administration/config/edit/:category/:name', component: ConfigNewComponent,
        data: [{ action: 'edit' }],
        resolve: { model: ConfigModelResolver }
    },
    //Checklist
    {
        path: 'administration/checklist', component: ChecklistComponent,
        resolve: { model: ChecklistModelResolver }
    },
    {
        path: 'administration/checklist/create', component: SeriesChecklistNewComponent,
        data: [{ action: 'create' }]
    },
    {
        path: 'administration/checklist/edit/:series', component: SeriesChecklistNewComponent,
        data: [{ action: 'edit' }],
        resolve: { model: SeriesChecklistResolver }
    },
    //AeroRDS
    { path: 'administration/aerords', component: AerordsComponent },

]
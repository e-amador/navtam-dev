﻿namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    using Domain.Model.Entitities;

    public class RegistrationConfig : EntityTypeConfiguration<Registration>
    {
        public RegistrationConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(e => e.Name).IsRequired();
            Property(e => e.Address).IsRequired();
            Property(e => e.EmailAddress).IsRequired();
            Property(e => e.Telephone).IsRequired();
            Property(e => e.TypeOfOperation).IsRequired();
            Property(e => e.Status).IsRequired();
            Property(e => e.StatusTime).IsRequired();
            Property(e => e.RegisteredUserName).IsRequired();
        }
    }
}


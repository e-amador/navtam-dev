﻿using NavCanada.Applications.NotamWiz.Web.Code.Helpers;
using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.NSDs.Obst.V0R1
{
    using System.Device.Location;
    using System.Threading.Tasks;

    using FluentValidation;

    using NavCanada.Applications.NotamWiz.Web.Code.Extentions;
    using NavCanada.Applications.NotamWiz.Web.Code.GeoTools;

    

    public class ObstValidator : AbstractValidator<ViewModel>
    {

        private readonly ILog log = LogManager.GetLogger(typeof(ObstValidator));

        public ObstValidator(NsdBaseController controller, IDbResWrapper dbRes)
        {
            this.log.Object(new
            {
                Method = "ObstValidator",
                Parameters = new
                {
                    controller
                }
            });
            Controller = controller;
            RuleFor(v => v.Height).GreaterThan(0).LessThanOrEqualTo(3000).WithMessage(dbRes.T("Invalid Height", "NSD-Obst"));
            RuleFor(v => v.Elevation).GreaterThan(0).LessThanOrEqualTo(99999).WithMessage(dbRes.T("Invalid Elevation", "NSD-Obst"));
            RuleFor(v => v.Lighted).Matches("[YN]").WithMessage(dbRes.T("Invalid Lighted Value", "NSD-Obst"));
            RuleFor(v => v.Painted).Matches("[YNO]").WithMessage(dbRes.T("Invalid Painted Value", "NSD-Obst"));
            RuleFor(v => v.Location).Must(GeoDataService.IsValidLocation).WithMessage(dbRes.T("Invalid Location", "NSD-Obst")).MustAsync(BeWithinDoa).WithMessage(dbRes.T("Obstacle Location is not Within your Domain of Authority.", "NSD-Obst"));
        }

        private NsdBaseController Controller
        {
            get;
            set;
        }

        private async Task<bool> BeWithinDoa(string strlocation)
        {

            var location = new GeoCoordinate().FromLocation(strlocation);
            this.log.Object(new
            {
                Method = "BeWithinDoa",
                Parameters = new
                {
                    location
                }
            });
            var doa = await Controller.GetUserDoaAsync();
            return GeoDataService.IsLocationWithinDOA(location, doa);
        }
    }
}
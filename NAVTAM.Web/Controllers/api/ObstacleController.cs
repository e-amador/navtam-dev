﻿using Core.Common.Geography;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using NAVTAM.NsdEngine;
using System;
using System.Globalization;
using System.Linq;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [Authorize]
    [RoutePrefix("api/obstacles")]
    public class ObstacleController : ApiBaseController
    {
        public ObstacleController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger) : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
        }

        [HttpGet]
        [Route("simple")]
        public IHttpActionResult GetSimple()
        {
            return LogAction(() => 
            {
                var obstTypes = new SimpleObstacles();
                var culture = GetEFCulture();
                var results = obstTypes[culture].Select(pair => new { Id = pair.Key, Name = pair.Value.Name, EFieldName = pair.Value.EFieldName });
                return Ok(results);
            }, "api/obstacles/get");
        }

        [HttpGet]
        [Route("complex")]
        public IHttpActionResult GetComplex()
        {
            return LogAction(() =>
            {
                var mobstTypes = new ComplexObstacles();
                var culture = GetEFCulture();
                var results = mobstTypes[culture].Select(pair => new { Id = pair.Key, Name = pair.Value.Name, EFieldName = pair.Value.EFieldName });
                return Ok(results);
            }, "api/obstacles/getmobst");
        }

        [HttpGet]
        [Route("CalcCenter")]
        public IHttpActionResult CalcCenter(string from, string to)
        {
            return LogAction(() =>
            {
                var fromGeo = DMSLocation.FromKnownFormat(from?.ToUpper())?.ToGeoCoordinate();
                var toGeo = DMSLocation.FromKnownFormat(to?.ToUpper())?.ToGeoCoordinate();
                if (fromGeo != null && toGeo != null)
                {
                    var midGeo = GeoCoordinateExt.GetCentralGeoCoordinate(fromGeo, toGeo);
                    var radius = MetersToNM(fromGeo.GetDistanceTo(midGeo));
                    var center = $"{midGeo.Latitude.ToString(CultureInfo.InvariantCulture)} {midGeo.Longitude.ToString(CultureInfo.InvariantCulture)}";
                    return Ok(new { Center = center, Radius = radius, Valid = true });
                }
                else
                {
                    return Ok(new { Center = "", Radius = 0, Valid = false });
                }
            }, "api/obstacles/CalcCenter");
        }

        static double MetersToNM(double nms) => nms * 0.000539957;

        private string GetEFCulture()
        {
            var culture = ApplicationContext.GetCookie("_culture", "en");
            return "fr".Equals(culture, StringComparison.OrdinalIgnoreCase) ? "fr" : "en";
        }
    }
}
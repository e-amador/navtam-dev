﻿//using System.Web.Http.OData.Routing;

namespace NavCanada.Applications.NotamWiz.Web.oData
{
    using System.Linq;
    using System.Web.Http;
    using System.Web.OData;

    using NavCanada.Core.Data.NotamWiz.Contracts;
    using NavCanada.Core.Domain.Model.Entitities;

    [Authorize(Roles = "User")]
    public class SDOSubjectsController : BaseODataController
    {

        public SDOSubjectsController(IUow uow)
            : base(uow)
        {
        }

        // GET: odata/SDOSubjects
        [EnableQuery]
        public IQueryable<SdoSubject> GetSDOSubjects()
        {
            Doa doa = GetUserDOA();
            if (doa == null)
                return null;

            return Uow.SdoSubjectRepo.GetAll().Where(s => s.SubjectGeoRefPoint.Intersects(doa.DoaGeoArea));                
        }

        // GET: odata/SDOSubjects(5)
        [EnableQuery]
        public SingleResult<SdoSubject> GetSDOSubject([FromODataUri] string key)
        {
            Doa doa = GetUserDOA();
            if (doa == null)
                return null;

            SdoSubject subject = Uow.SdoSubjectRepo.GetById(key);
            

            return SingleResult.Create( Uow.SdoSubjectRepo.GetAll().Where(s=>s.Id == key && s.SubjectGeoRefPoint.Intersects(doa.DoaGeoArea)));
        }

        // GET: odata/SDOSubjects(5)/Children
        [EnableQuery]
        public IQueryable<SdoSubject> GetChildren([FromODataUri] string key)
        {
            Doa doa = GetUserDOA();
            if (doa == null)
                return null;

            return Uow.SdoSubjectRepo.GetAll().Where(m => m.SubjectGeo.Intersects(doa.DoaGeoArea) && m.Id == key).SelectMany(m => m.Children);
        }

        // GET: odata/SDOSubjects(5)/NOTAMPackages
        [EnableQuery]
        public IQueryable<NotamPackage> GetNOTAMPackages([FromODataUri] string key)
        {
            Doa doa = GetUserDOA();
            if (doa == null)
                return null;

            return Uow.SdoSubjectRepo.GetAll().Where(m => m.SubjectGeo.Intersects(doa.DoaGeoArea) && m.Id == key).SelectMany(m => m.NotamPackages);
        }

        // GET: odata/SDOSubjects(5)/Parent
        [EnableQuery]
        public SingleResult<SdoSubject> GetParent([FromODataUri] string key)
        {
            Doa doa = GetUserDOA();
            if (doa == null)
                return null;

            return SingleResult.Create(Uow.SdoSubjectRepo.GetAll().Where(m => m.SubjectGeo.Intersects(doa.DoaGeoArea) && m.Id == key).Select(m => m.Parent));
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Uow.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SDOSubjectExists(string key)
        {
            Doa doa = GetUserDOA();
            if (doa == null)
                return false;

            return Uow.SdoSubjectRepo.GetAll().Any(e => e.SubjectGeo.Intersects(doa.DoaGeoArea) && e.Id == key) ;
        }
    }
}

﻿using NavCanada.Core.Domain.Model.Entitities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace NavCanada.Core.Data.EF.Configurations
{
    public class NdsOutgoingMessageConfig : EntityTypeConfiguration<NdsOutgoingMessage>
    {
        public NdsOutgoingMessageConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(e => e.GroupId).IsRequired();
            Property(e => e.GroupOrder).IsRequired();
            Property(e => e.Destination).IsRequired();
            Property(e => e.Metadata).IsRequired();
            Property(e => e.Content).IsRequired();
        }
    }
}

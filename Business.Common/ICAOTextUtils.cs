﻿using Core.Common.Geography;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using System;
using System.Text;

namespace Business.Common
{
    public static class ICAOTextUtils
    {
        public static bool CanGenerateText(INotam notam)
        {
            // validate coordinates
            if (DMSLocation.FromDMS(notam.Coordinates) == null)
                return false;

            // validate start activity
            if (!notam.Immediate && !notam.StartActivity.HasValue)
                return false;

            // validate end validity
            if (notam.ReferenceType != NotamType.C && !notam.Permanent && !notam.EndValidity.HasValue)
                return false;

            return true;
        }

        public static string FormatNotamNumber(INotam notam, string splitNumber = "")
        {
            if (!notam.ReferenceType.HasValue || string.IsNullOrEmpty(notam.Series) || notam.Number == 0 || notam.Year == 0)
                return string.Empty;

            var notamId = FormatNotamId(notam.Series, notam.Number, notam.Year);

            if (notam.ReferenceType.Value == NotamType.N)
                return $"{notamId}{splitNumber} NOTAMN";

            var notamType = $"NOTAM{notam.ReferenceType.Value}";
            var notamRefId = FormatNotamId(notam.ReferredSeries, notam.ReferredNumber, notam.ReferredYear);

            return $"{notamId}{splitNumber} {notamType} {notamRefId}";
        }

        public static string FormatMultipartNotamNumber(INotam notam, int number, int total)
        {
            return FormatNotamNumber(notam, GetNotamSplitNumber(number, total));
        }

        public static string FormatNotamId(string series, int number, int year)
        {
            return $"{series}{number.ToString().PadLeft(4, '0')}/{year.ToString().PadLeft(2, '0').Substring(2)}";
        }

        public static string FormatNotamNumber(int number)
        {
            return number.ToString().PadLeft(4, '0');
        }

        public static bool TryParseNotamId(string notamId, out string series, out int number, out int year)
        {
            series = "";
            number = 0;
            year = 0;

            // expects a 8 chars long number e.g.: A0001/18
            if ((notamId ?? "").Length != 8)
                return false;

            if (notamId[0] < 'A' || notamId[0] > 'Z')
                return false;

            series = $"{notamId[0]}";

            if (!int.TryParse(notamId.Substring(1, 4), out number) || number < 0)
                return false;

            if (notamId[5] != '/')
                return false;

            if (!int.TryParse(notamId.Substring(6, 2), out year) || year < 0)
                return false;

            year += 2000;

            return true;
        }

        public static string FormatQLine(INotam notam)
        {
            var qLine = new StringBuilder();
            qLine.Append(GetFir(notam)).Append("/Q")
                 .Append(notam.Code23)
                 .Append(notam.Code45).Append('/')
                 .Append(notam.Traffic).Append('/')
                 .Append(notam.Purpose).Append('/')
                 .Append(notam.Scope).Append('/')
                 .Append(notam.LowerLimit.ToString("000")).Append('/')
                 .Append(notam.UpperLimit.ToString("000")).Append('/')
                 .Append(RoundDMSCoordinates(notam.Coordinates, string.Empty))
                 .Append(notam.Radius.ToString("000"));

            return qLine.ToString();
        }

        public static string NotamDateFormat = "yyMMddHHmm";

        public static string FormatStartActivity(bool immediate, DateTime? date)
        {
            return immediate ? "IMMEDIATE" : FormatDate(date, NotamDateFormat);
        }

        public static string FormatNotamDate(DateTime? date) => FormatDate(date, NotamDateFormat);

        public static string FormatEndValidity(INotam notam)
        {
            return notam.ReferenceType != NotamType.C ? FormatEndValidity(notam.Permanent, notam.Estimated, notam.EndValidity) : null;
        }

        public static string FormatEndValidity(bool permanent, bool estimated, DateTime? date)
        {
            if (permanent) return "PERM";

            if (!date.HasValue)
                throw new Exception("Not-permanente NOTAM with NULL EndValidity date detected!");

            var dateAsText = FormatDate(date, NotamDateFormat);
            return estimated ? $"{dateAsText}EST" : dateAsText;
        }

        public static string RoundDMSCoordinates(string coords, string separator)
        {
            var dmsLocation = DMSLocation.FromDMS(coords);
            return dmsLocation != null ? dmsLocation.Round().ToDM(separator) : coords;
        }

        static string GetNotamSplitNumber(int number, int total)
        {
            var letter = (char)('A' + number);
            var part = total.ToString().PadLeft(2, '0');
            return $"{letter}{part}";
        }

        static string FormatDate(DateTime? date, string format) => date?.ToString(format);

        static string GetFir(INotam notam) => (notam.ItemA ?? "").Trim().Split(' ').Length > 1 ? "CZXX" : notam.Fir;
    }
}

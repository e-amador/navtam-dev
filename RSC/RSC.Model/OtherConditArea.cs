﻿using RSC.Model.Enums;

namespace RSC.Model
{
    /// <summary>
    /// Structure to define area type 'Other Conditions' ON the runway and which do NOT
    /// have an associated height.
    /// </summary>
    public class OtherConditArea
    {
        /// <summary>
        /// Type of additional surface condition. 'L' = Left HMI pane.
        /// </summary>
        public OtherConditionsAreaEnum ConditTypeL { get; set; }
        /// <summary>
        /// The distance, in feet, between the location and the threshold specified by ThresholdNum.
        /// </summary>
        public int DistFromThreshold { get; set; }
        /// <summary>
        /// Runway number corresponding to the threshold from which the location is measured.
        /// </summary>
        public int ThresholdNum { get; set; }
    }
}
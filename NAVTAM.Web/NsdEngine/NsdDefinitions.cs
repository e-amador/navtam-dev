﻿namespace NAVTAM.NsdEngine
{
    public static class NsdDefinitions
    {
        /* Item F & G */
        public static string SFC = "SFC";
        public static string FL = "FL";
        public static string AMSL = "AMSL";
        public static string AGL = "AGL";
        public static string UNL = "UNL";
        /* Purpose Types */
        public static string PurposeN = "N";
        public static string PurposeB = "B";
        public static string PurposeO = "O";
        public static string PurposeM = "M";
        /* Proposal Types */
        public static string NewNotam = "N";
        public static string CancelledNotam = "C";
        public static string CatchAll = "CATCH ALL";
        /* Traffic */
        public static string TrafficI = "I";
        public static string TrafficV = "V";
    }
}
﻿namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    using System.Data.Entity;
    using System.Threading.Tasks;

    using Contracts.Repositories;
    using Domain.Model.Entitities;

    public class RegistrationRepo : Repository<Registration>, IRegistrationRepo
    {
        public RegistrationRepo(DbContext dbContext)
            : base(dbContext)
        {
        }

        public Task<Registration> GetByIdAsync(int id)
        {
            return DbSet.FirstOrDefaultAsync(e => e.Id == id);
        }

        public Task<Registration> GetByUserIdAsync(string userId)
        {
            return DbSet.FirstOrDefaultAsync(e => e.RegisteredUserId == userId);
        }

        public Task<Registration> GetByNameAsync(string name)
        {
            return DbSet.FirstOrDefaultAsync(e => e.Name == name);
        }

        public Task<bool> ExistAsync(string name)
        {
            return DbSet.AnyAsync(e => e.Name == name);
        }
    }
}

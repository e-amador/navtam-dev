﻿import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms';
import { Component, Inject, OnInit, Input, OnDestroy, AfterViewInit, ViewChildren, ViewChild, QueryList } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { WindowRef } from '../../../common/windowRef.service';
import { MemoryStorageService } from '../../shared/mem-storage.service';
import { TOASTR_TOKEN, Toastr } from './../../../common/toastr.service';

import { LocationService } from '../../shared/location.service';
import { RscService } from './rsc.service';

import { NsdButtonBarComponent } from '../../nsds/nsd-buttonbar.component';
import { NsdGeoRefToolComponent } from '../../nsds/nsd-georeftool.component';
import { ProposalStatus, ProposalType } from '../../shared/data.model';

import { DataService } from '../../shared/data.service';
import { TranslationService } from 'angular-l10n';
import { ISdoSubject, ISelectOptions } from '../../shared/data.model';
import { PhoneValidator } from '../../../admin/shared/phone.validator';
import { Select2OptionData } from 'ng2-select2';

//Entry data models
import { IRunwaySubject, IBilingualText, ITreatmentsModel, ISnowbanksModel } from './rsc.model'; 
//View model 
import { IRSCRunway, IRunwayClearedPortion, IClearedPortion, IClearedSurface, IClearedSurfaceThird } from './rsc.model';
import { IRwyFriction, IContaminantComponentModel, IRwyFrictionModel, IClearedWidthModel, IClearedPortionOffset } from './rsc.model';
import { IRemainingWidthModel, INextObservationModel, IWindrowsModel, ITaxiwaysRemarksModel, IApronRemarksModel } from './rsc.model';
import { ICleaningOperationsModel, CardinalSideEnum, ISnowbanksRunway, ISnowbank, IBankHeight, SelectContaminantOptionData } from './rsc.model';
//To delete models
import { IRSCData } from './rsc.model';

import { ClearedWidthComponent } from './components/rsc-clearedwidth.component';
import { RscContaminantsComponent } from './components/rsc-contaminants.component';
import { RwyFrictionComponent } from './components/rsc-rwyfriction.component';
import { RemainingWidthRemarksComponent } from './components/rsc-remainingwidthremarks.component';
import { ClearingOperationsComponent } from './components/rsc-clearingoperations.component';
import { SnowBanksComponent } from './components/rsc-snowbanks.component';
import { RscTreatmentsComponent } from './components/rsc-treatments.component';
import { WindrowsComponent } from './components/rsc-windrows.component';
import { TaxiwaysRemarksComponent } from './components/rsc-taxiwaysremarks.component';
import { ApronRemarksComponent } from './components/rsc-apronremarks.component';
import { NextObservationComponent } from './components/rsc-nextobservation.component';


import * as moment from 'moment';
import { forEach } from '@angular/router/src/utils/collection';
declare var $: any;

@Component({ 
    templateUrl: './app/dashboard/nsds/rsc/rsc.component.html'
})
export class RscComponent implements OnInit, OnDestroy, AfterViewInit  {

    @Input('isNOF') isNOF: boolean = false;

    @ViewChildren(NsdButtonBarComponent) buttonBars: QueryList<NsdButtonBarComponent>;
    @ViewChild(NsdGeoRefToolComponent) geoRefTool: NsdGeoRefToolComponent;

    @ViewChild(ClearedWidthComponent) clearedWidthComponent: ClearedWidthComponent;
    @ViewChildren(RscContaminantsComponent) contaminantComponentsList: QueryList<RscContaminantsComponent>;
    @ViewChild(RwyFrictionComponent) rwyFriction: RwyFrictionComponent;
    @ViewChild(RemainingWidthRemarksComponent) remainingWidth: RemainingWidthRemarksComponent;
    @ViewChild(ClearingOperationsComponent) clearingOperationsComponent: ClearingOperationsComponent;
    @ViewChild(SnowBanksComponent) snowBanksComponent: SnowBanksComponent;
    @ViewChild(RscTreatmentsComponent) treatmentsComponent: RscTreatmentsComponent;
    @ViewChild(WindrowsComponent) windrowsComponent: WindrowsComponent;
    @ViewChild(TaxiwaysRemarksComponent) taxiwaysRemarksComponent: TaxiwaysRemarksComponent;
    @ViewChild(ApronRemarksComponent) apronRemarksComponent: ApronRemarksComponent;
    @ViewChild(NextObservationComponent) nextObservation: NextObservationComponent;

    model: any = null;
    token: any = null;
    nsdForm: FormGroup; // this is the name neccessary to reuse the old code
    isReadOnly: boolean = false;
    leafletmap: any;
    tabActive: number = 1;
    culture: string = 'en';
    isBilingualRegion: boolean = false;
    isFormLoadingData: boolean = false;

    //Enums definitions
    contaminants: SelectContaminantOptionData[];
    depths: Select2OptionData[];
    coefficients: Select2OptionData[];
    coverages: Select2OptionData[];
    conditionCodes: Select2OptionData[];
    conditionCodeAction: Select2OptionData[];
    unitOfMeasurement: Select2OptionData[];
    cardinalSideEnum: Select2OptionData[];
    measurementDevice: Select2OptionData[];
    ocLocation: Select2OptionData[];
    offsetSide: Select2OptionData[];
    otherConditionsArea: Select2OptionData[];
    otherConditionsEdge: Select2OptionData[];
    treatment: Select2OptionData[];
    cardinalPointsByDirections: any;
    //End enum definitions

    subjectSelected: ISdoSubject = null;
    subjectId: string = '';
    subjects: ISdoSubject[] = [];

    //Data definitions
    runwaysRSCData: IRSCRunway[] = [];
    runwayFrictionData: IRwyFriction = null;
    nextObservationData: any = null;
    remainingWidthData: any = null;
    windrowsRemarksData: any = null;


    runwaySelected: IRunwaySubject = null;
    runwayId: string = '-1';
    runways: IRunwaySubject[] = [];
    //End Data definitions


    runwayComboList: ISelectOptions[] = [];

    maintenance: boolean = false;
    grfValue: boolean = true;
    waitingForGeneratingIcao: boolean = false;

    txtBackToReviewList;
    isSaveButtonDisabled: boolean = true;
    isSubmitButtonDisabled: boolean = true;
    isWaitingForResponse: boolean = false;
    isRunwaySelectorDisabled: boolean = true;

    mapHealthy: boolean = false;
    componentData = null;

    observationDateMoment: any;
    observationDateText: string = "";
    dateFormat: string = "YYMMDDHHmm";

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService : DataService, 
        private activatedRoute: ActivatedRoute, 
        private windowRef: WindowRef,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private locationService: LocationService,
        private rscService: RscService,
        public translation: TranslationService) {
    }

    ngOnInit() {
        window['loadLeafletMapScript'].call(this, null);
        this.model = this.activatedRoute.snapshot.data['model'];

        this.observationDateMoment = moment.utc();

        this.model.canReject = false;
        this.token = this.model.token;
        this.isFormLoadingData = !(!this.token.subjectId);
        this.isNOF = this.activatedRoute.snapshot.data[0]['isNOF'];
        this.leafletmap = this.rscService.app.leafletmap;
        this.culture = this.rscService.app.cultureInfo;

        //Definition of ENUMS
        this.contaminants = [
            { id: '00', text: this.translation.translate('ContaminantsComponent.SelectContaminant'), contaminatId: '00', needDepth: false, temperature: 0 },
            { id: '20', text: this.translation.translate('ContaminantWithNoDepth.Dry'), contaminatId: '20', needDepth: false, temperature: 0 },
            { id: '22', text: this.translation.translate('ContaminantWithNoDepth.Wet'), contaminatId: '22', needDepth: false, temperature: 0 },
            { id: '23', text: this.translation.translate('ContaminantWithNoDepth.Frost'), contaminatId: '23', needDepth: false, temperature: 0 },
            { id: '24', text: this.translation.translate('ContaminantWithNoDepth.Ice'), contaminatId: '24', needDepth: false, temperature: 0 },
            { id: '25', text: this.translation.translate('ContaminantWithNoDepth.CompactedSnow20'), contaminatId: '25', needDepth: false, temperature: -20 },
            { id: '26', text: this.translation.translate('ContaminantWithNoDepth.CompactedSnow10'), contaminatId: '25', needDepth: false, temperature: -10 },
            { id: '27', text: this.translation.translate('ContaminantWithNoDepth.WetIce'), contaminatId: '27', needDepth: false, temperature: 0 },
            { id: '29', text: this.translation.translate('ContaminantWithNoDepth.CompactedSnowGravelMix'), contaminatId: '29', needDepth: false, temperature: 0 },
            { id: '32', text: this.translation.translate('ContaminantWithNoDepth.SlipperyWhenWet'), contaminatId: '32', needDepth: false, temperature: 0 },
            { id: '60', text: this.translation.translate('ContaminantWithDepth.StandingWater'), contaminatId: '60', needDepth: true, temperature: 0 },
            { id: '61', text: this.translation.translate('ContaminantWithDepth.DrySnow'), contaminatId: '61', needDepth: true, temperature: 0 },
            { id: '62', text: this.translation.translate('ContaminantWithDepth.WetSnow'), contaminatId: '62', needDepth: true, temperature: 0 },
            { id: '63', text: this.translation.translate('ContaminantWithDepth.Slush'), contaminatId: '63', needDepth: true, temperature: 0 },
            { id: '65', text: this.translation.translate('ContaminantWithDepth.DrySnowOnTopOfIce'), contaminatId: '65', needDepth: true, temperature: 0 },
            { id: '66', text: this.translation.translate('ContaminantWithDepth.WetSnowOnTopOfIce'), contaminatId: '66', needDepth: true, temperature: 0 },
            { id: '67', text: this.translation.translate('ContaminantWithDepth.SlushOnTopOfIce'), contaminatId: '67', needDepth: true, temperature: 0 },
            { id: '68', text: this.translation.translate('ContaminantWithDepth.DrySnowOnTopOfCompactedSnow'), contaminatId: '68', needDepth: true, temperature: 0 },
            { id: '71', text: this.translation.translate('ContaminantWithDepth.WaterOnTopOfCompactedSnow'), contaminatId: '71', needDepth: true, temperature: 0 },
            { id: '72', text: this.translation.translate('ContaminantWithDepth.WetSnowOnTopOfCompactedSnow'), contaminatId: '72', needDepth: true, temperature: 0 },
        ];
        this.depths = [
            { id: '00.00', text: '0' },
            { id: '00.13', text: '1/8' },
            { id: '00.25', text: '1/4' },
            { id: '00.50', text: '1/2' },
            { id: '00.75', text: '3/4' },
            { id: '01.00', text: '1' },
            { id: '01.50', text: '1.5' },
            { id: '02.00', text: '2' },
            { id: '03.00', text: '3' },
            { id: '04.00', text: '4' },
            { id: '05.00', text: '5' },
            { id: '06.00', text: '6' },
            { id: '07.00', text: '7' },
            { id: '08.00', text: '8' },
            { id: '09.00', text: '9' },
            { id: '10.00', text: '10' },
            { id: '11.00', text: '11' },
            { id: '12.00', text: '12' },
            { id: '13.00', text: '13' },
            { id: '14.00', text: '14' },
            { id: '15.00', text: '15' },
            { id: '16.00', text: '16' },
            { id: '17.00', text: '17' },
            { id: '18.00', text: '18' },
            { id: '19.00', text: '19' },
            { id: '20.00', text: '20' },
            { id: '21.00', text: '21' },
            { id: '22.00', text: '22' },
            { id: '23.00', text: '23' },
            { id: '24.00', text: '24' },
            { id: '25.00', text: '25' },
            { id: '26.00', text: '26' },
            { id: '27.00', text: '27' },
            { id: '28.00', text: '28' },
            { id: '29.00', text: '29' },
            { id: '30.00', text: '30' },
            { id: '31.00', text: '31' },
            { id: '32.00', text: '32' },
            { id: '33.00', text: '33' },
            { id: '34.00', text: '34' },
            { id: '35.00', text: '35' },
            { id: '36.00', text: '36' },
            { id: '37.00', text: '37' },
            { id: '38.00', text: '38' },
            { id: '39.00', text: '39' },
            { id: '40.00', text: '40' },
            { id: '41.00', text: '41' },
            { id: '42.00', text: '42' },
            { id: '43.00', text: '43' },
            { id: '44.00', text: '44' },
            { id: '45.00', text: '45' },
            { id: '46.00', text: '46' },
            { id: '47.00', text: '47' },
            { id: '48.00', text: '48' },
            { id: '49.00', text: '49' },
            { id: '50.00', text: '50' },
            { id: '51.00', text: '51' },
            { id: '52.00', text: '52' },
            { id: '53.00', text: '53' },
            { id: '54.00', text: '54' },
            { id: '55.00', text: '55' },
            { id: '56.00', text: '56' },
            { id: '57.00', text: '57' },
            { id: '58.00', text: '58' },
            { id: '59.00', text: '59' },
            { id: '60.00', text: '60' }
        ];
        this.coefficients = [
            { id: '00', text: '.00' },
            { id: '01', text: '.01' },
            { id: '02', text: '.02' },
            { id: '03', text: '.03' },
            { id: '04', text: '.04' },
            { id: '05', text: '.05' },
            { id: '06', text: '.06' },
            { id: '07', text: '.07' },
            { id: '08', text: '.08' },
            { id: '09', text: '.09' },
            { id: '10', text: '.10' },
            { id: '11', text: '.11' },
            { id: '12', text: '.12' },
            { id: '13', text: '.13' },
            { id: '14', text: '.14' },
            { id: '15', text: '.15' },
            { id: '16', text: '.16' },
            { id: '17', text: '.17' },
            { id: '18', text: '.18' },
            { id: '19', text: '.19' },
            { id: '20', text: '.20' },
            { id: '21', text: '.21' },
            { id: '22', text: '.22' },
            { id: '23', text: '.23' },
            { id: '24', text: '.24' },
            { id: '25', text: '.25' },
            { id: '26', text: '.26' },
            { id: '27', text: '.27' },
            { id: '28', text: '.28' },
            { id: '29', text: '.29' },
            { id: '30', text: '.30' },
            { id: '31', text: '.31' },
            { id: '32', text: '.32' },
            { id: '33', text: '.33' },
            { id: '34', text: '.34' },
            { id: '35', text: '.35' },
            { id: '36', text: '.36' },
            { id: '37', text: '.37' },
            { id: '38', text: '.38' },
            { id: '39', text: '.39' },
            { id: '40', text: '.40' },
            { id: '41', text: '.41' },
            { id: '42', text: '.42' },
            { id: '43', text: '.43' },
            { id: '44', text: '.44' },
            { id: '45', text: '.45' },
            { id: '46', text: '.46' },
            { id: '47', text: '.47' },
            { id: '48', text: '.48' },
            { id: '49', text: '.49' },
            { id: '50', text: '.50' },
            { id: '51', text: '.51' },
            { id: '52', text: '.42' },
            { id: '53', text: '.53' },
            { id: '54', text: '.54' },
            { id: '55', text: '.55' },
            { id: '56', text: '.56' },
            { id: '57', text: '.57' },
            { id: '58', text: '.58' },
            { id: '59', text: '.59' },
            { id: '60', text: '.60' },
            { id: '61', text: '.61' },
            { id: '62', text: '.62' },
            { id: '63', text: '.63' },
            { id: '64', text: '.64' },
            { id: '65', text: '.65' },
            { id: '66', text: '.66' },
            { id: '67', text: '.67' },
            { id: '68', text: '.68' },
            { id: '69', text: '.69' },
            { id: '70', text: '.70' },
            { id: '71', text: '.71' },
            { id: '72', text: '.72' },
            { id: '73', text: '.73' },
            { id: '74', text: '.74' },
            { id: '75', text: '.75' },
            { id: '76', text: '.76' },
            { id: '77', text: '.77' },
            { id: '78', text: '.78' },
            { id: '79', text: '.79' },
            { id: '80', text: '.80' },
            { id: '81', text: '.81' },
            { id: '82', text: '.82' },
            { id: '83', text: '.83' },
            { id: '84', text: '.84' },
            { id: '85', text: '.85' },
            { id: '86', text: '.86' },
            { id: '87', text: '.87' },
            { id: '88', text: '.88' },
            { id: '89', text: '.89' },
            { id: '90', text: '.90' },
            { id: '91', text: '.91' },
            { id: '92', text: '.92' },
            { id: '93', text: '.93' },
            { id: '94', text: '.94' },
            { id: '95', text: '.95' },
            { id: '96', text: '.96' },
            { id: '97', text: '.97' },
            { id: '98', text: '.98' },
            { id: '99', text: '.99' }
        ];
        this.coverages = [
            { id: '010', text: '10' },
            { id: '020', text: '20' },
            { id: '025', text: '25' },
            { id: '030', text: '30' },
            { id: '040', text: '40' },
            { id: '050', text: '50' },
            { id: '060', text: '60' },
            { id: '070', text: '70' },
            { id: '075', text: '75' },
            { id: '080', text: '80' },
            { id: '090', text: '90' },
            { id: '100', text: '100' }
        ];
        this.conditionCodes = [
            { id: '0', text: '0' },
            { id: '1', text: '1' },
            { id: '2', text: '2' },
            { id: '3', text: '3' },
            { id: '4', text: '4' },
            { id: '5', text: '5' },
            { id: '6', text: '6' }
        ];
        this.conditionCodeAction = [
            { id: '0', text: this.translation.translate('ContaminantsComponent.ConfirmRWYCC') },
            { id: '1', text: this.translation.translate('ContaminantsComponent.DowngradeRWYCC') },
            { id: '2', text: this.translation.translate('ContaminantsComponent.UpgradeRWYCC') }
        ];
        this.unitOfMeasurement = [
            { id: '9', text: this.translation.translate('UnitOfMeasurement.Inches'), additional: null },
            { id: '10', text: this.translation.translate('UnitOfMeasurement.Feet'), additional: null }
        ];
        this.cardinalSideEnum = [
            { id: '250', text: 'E', additional: null },
            { id: '251', text: 'SE', additional: null },
            { id: '252', text: 'S', additional: null },
            { id: '253', text: (this.culture === 'en') ? 'SW' : 'SO', additional: null },
            { id: '254', text: (this.culture === 'en') ? 'W' : 'O', additional: null },
            { id: '255', text: (this.culture === 'en') ? 'NW' : 'NO', additional: null },
            { id: '256', text: 'N', additional: null },
            { id: '257', text: 'NE', additional: null },
        ];
        this.measurementDevice = [
            { id: '130', text: this.translation.translate('MeasurementDevice.TapleyMeter'), additional: null },
            { id: '131', text: this.translation.translate('MeasurementDevice.JamesDecelerometer'), additional: null },
            { id: '132', text: this.translation.translate('MeasurementDevice.Bowmonk'), additional: null },
            { id: '133', text: this.translation.translate('MeasurementDevice.DecelerometerTES'), additional: null },
        ];
        this.ocLocation = [
            { id: '127', text: this.translation.translate('OCLocationEnum.AlongInsideRedl'), additional: null },
            { id: '128', text: this.translation.translate('OCLocationEnum.AlongInsideRwyEdge'), additional: null },
            { id: '129', text: this.translation.translate('OCLocationEnum.AlongClearedWidth'), additional: null },
            { id: '134', text: this.translation.translate('OCLocationEnum.FMCL'), additional: null },
        ];
        this.offsetSide = [
            { id: '11', text: this.translation.translate('OffsetSide.East'), additional: null },
            { id: '12', text: this.translation.translate('OffsetSide.West'), additional: null },
            { id: '13', text: this.translation.translate('OffsetSide.North'), additional: null },
            { id: '14', text: this.translation.translate('OffsetSide.South'), additional: null },
        ];
        this.otherConditionsArea = [
            { id: '122', text: this.translation.translate('OtherConditionsArea.IcePatches'), additional: null },
            { id: '123', text: this.translation.translate('OtherConditionsArea.CompactedSnowPatches'), additional: null },
            { id: '124', text: this.translation.translate('OtherConditionsArea.StandingWaterPatches'), additional: null },
        ];
        this.otherConditionsEdge = [
            { id: '120', text: this.translation.translate('OtherConditionsEdge.SnowDrifts'), additional: null },
            { id: '121', text: this.translation.translate('OtherConditionsEdge.Windrows'), additional: null },
            { id: '122', text: this.translation.translate('OtherConditionsEdge.SnowBanks'), additional: null },
        ];
        this.treatment = [
            { id: '100', text: this.translation.translate('Treatment.LoseSand'), additional: null },
            { id: '104', text: this.translation.translate('Treatment.Graded'), additional: null },
            { id: '106', text: this.translation.translate('Treatment.Packed'), additional: null },
            { id: '108', text: this.translation.translate('Treatment.ChemicallyTreated'), additional: null },
            { id: '109', text: this.translation.translate('Treatment.Scarified'), additional: null },
        ];
        this.cardinalPointsByDirections = [
            //Runway Designator,  Cardinal direction right of centreline,  Cardinal direction left of centreline
            {
                designator: "01,02",
                right: this.translation.translate('CardinalValues.East'),
                left: this.translation.translate('CardinalValues.West')
            },
            {
                designator: "03,04,05,06",
                right: this.translation.translate('CardinalValues.SouthEast'),
                left: this.translation.translate('CardinalValues.NorthWest')
            },
            {
                designator: "07,08,09,10,11",
                right: this.translation.translate('CardinalValues.South'),
                left: this.translation.translate('CardinalValues.North')
            },
            {
                designator: "12,13,14,15",
                right: this.translation.translate('CardinalValues.SouthWest'),
                left: this.translation.translate('CardinalValues.NorthEast')
            },
            {
                designator: "16,17,18,19,20",
                right: this.translation.translate('CardinalValues.West'),
                left: this.translation.translate('CardinalValues.East')
            },
            {
                designator: "21,22,23,24",
                right: this.translation.translate('CardinalValues.NorthWest'),
                left: this.translation.translate('CardinalValues.SouthEast')
            },
            {
                designator: "25,26,27,28,29",
                right: this.translation.translate('CardinalValues.North'),
                left: this.translation.translate('CardinalValues.South')
            },
            {
                designator: "30,31,32,33",
                right: this.translation.translate('CardinalValues.NorthEast'),
                left: this.translation.translate('CardinalValues.SouthWest')
            },
            {
                designator: "34,35,36",
                right: this.translation.translate('CardinalValues.East'),
                left: this.translation.translate('CardinalValues.West')
            },
        ];
        //End definition od ENUMS

        this.configureReactiveForm();

        this.observationDateText = this.observationDateMoment.format("YYYY/MM/DD, H:mm") + " UTC";
        let fc = this.$ctrl('observationDate');
        let formatedDate = this.observationDateMoment.format(this.dateFormat);
        if (fc.value !== formatedDate) {
            fc.patchValue(formatedDate);
        }

        const backLabel = this.translation.translate('ActionButtons.BackToReviewList');
        if (this.isReadOnly) {
            this.txtBackToReviewList = backLabel;
        } else {
            const fromWidthdrawnAction = this.memStorageService.get(this.memStorageService.MODIFY_ACTION_KEY);
            this.txtBackToReviewList = fromWidthdrawnAction ? this.translation.translate('ActionButtons.CancelAnd') + backLabel : backLabel;
        }
        this.isSaveButtonDisabled = this.isSaveButtonDisabled || this.isReadOnly;

        if (this.isFormLoadingData) {
            this.loadSubject(this.token.subjectId);
        }

        this.initMap();
        this.mapHealthCheck();
    }

    ngOnDestroy() {
        if (window["app"].leafletmap) {
            window["app"].leafletmap.dispose();
        }
    }

    ngAfterViewInit() {
        this.buttonBars.forEach((child) => {
            child.parent = <RscComponent>this;

        });

        if (!this.isReadOnly) {
            this.nsdForm.valueChanges
                .debounceTime(2000)
                .subscribe(() => {
                    if (this.validateForm()) this.generateIcao();
                });
        }
    }

    isCalendarActive() {
        return true;
    }

    setObservationDate(date: any, leaveEmpty?: boolean) {
        const fc = this.$ctrl('observationDate');
        if (date) {
            let momentDate = moment.utc(date);
            if (momentDate.isValid()) {
                this.observationDateText = momentDate.format("YYYY/MM/DD, H:mm") + " UTC";
                const formatedDate = momentDate.format(this.dateFormat);
                if (fc.value !== formatedDate) {
                    fc.setValue(formatedDate);
                    fc.markAsDirty();
                    this.observationDateMoment = momentDate;
                }

            }
        } else {
            if (leaveEmpty) {
                fc.setValue(null);
                fc.markAsDirty();
            } else {
                this.setObservationDate(moment.utc());
            }
        }
    }

    validateObservationDate(errorName: string) {
        return true;
    }

    onObservationDateInputChange(strDate: string) {
        if (strDate && strDate.length === 10) {
            let momentDate = moment.utc(strDate, this.dateFormat);
            if (momentDate.isValid()) {
                this.observationDateText = momentDate.format("YYYY/MM/DD, H:mm") + " UTC";
                const fc = this.$ctrl('observationDate');
                const formatedDate = momentDate.format(this.dateFormat);
                if (fc.value !== formatedDate) {
                    fc.setValue(formatedDate);
                    fc.markAsDirty();
                    this.observationDateMoment = momentDate;
                }
            } else {
                this.observationDateText = "";
            }
        } else {
            this.observationDateText = "";
        }
    }

    public createClearedWidthModel(): IClearedWidthModel {
        let firstOffset: IClearedPortionOffset = {
            offsetFT: 0,
            offsetSide: CardinalSideEnum.E,
        };

        let secondOffset: IClearedPortionOffset = {
            offsetFT: 0,
            offsetSide: CardinalSideEnum.E,
        };

        let data: IClearedWidthModel = {
            parent: this,
            runway: this.runways.find( r => r.runwayId === this.runwayId),
            clearedFull: false,
            centredClearedWidth: 0,
            clearedPortionOffsets: [firstOffset, secondOffset],
        };

        return data;
    }

    public createContaminantModel(id: string): IContaminantComponentModel {

        let data: IContaminantComponentModel = {
            parent: this,
            id: id,
            data: {
                firstContaminant: {
                    contaminantId: '-1',
                    id: '-1',
                    depth: '-1',
                    coverage: '-1'
                },
                secondContaminant: {
                    contaminantId: '-1',
                    id: '-1',
                    depth: '-1',
                    coverage: '-1'
                },
                rwyCC: '-1',
                rwyCCCalculated: '',
                rwyCCActionSelected: '-1',
                coefficient: '-1',
                temperature: -1,
            }
        };
        return data;
    }

    private createRwyFrictionModel(): IRwyFrictionModel {
        let data: IRwyFrictionModel = {
            parent: this,
            measureDate: null,
            coefficient: '-1',
            temperature: '-1',
        };
        return data;
    }

    private createClearingOperationsModel(): ICleaningOperationsModel {
        let text: IBilingualText = {
            english: '',
            french: '',
        };
        let data: ICleaningOperationsModel = {
            parent: this,
            clearingUnderway: false,
            startAt: '',
            completedBy: '',
            remarks: text,
        };

        return data;
    }

    private createSnowBanksModel(): ISnowbanksModel {
        let first: ISnowbank = {
            bankHeight: {
                bankHeightFt: 0,
                bankHeightInch: 0
            },
            bankEdgeFt: 0,
            bankSide: CardinalSideEnum.E,
        };
        let second: ISnowbank = {
            bankHeight: {
                bankHeightFt: 0,
                bankHeightInch: 0
            },
            bankEdgeFt: 0,
            bankSide: CardinalSideEnum.E,
        };
        let data: ISnowbanksModel = {
            parent: this,
            runway: this.runways.find(r => r.runwayId === this.runwayId),
            firstSnowbank: first,
            secondSnowbank: second,
        };
        return data;
    }

    private createTreatmentsModel(): ITreatmentsModel {
        let data: ITreatmentsModel = {
            parent: this,
            sand: false,
            sandTimeApplied: '',
            chemicallyTreated: false,
            chemicalTimeApplied: '',
            remarks: '',
        };
        return data;
    }

    private createRemainingWidthRemarksModel(): IRemainingWidthModel {
        let data: IRemainingWidthModel = {
            parent: this,
            remainingWidthRemarks: '',
        };
        return data;
    }

    private createNextObservationModel(): INextObservationModel {
        let data: INextObservationModel = {
            parent: this,
            nextScheduledObservation: moment.utc().add(8, 'hours'),
            generalRemarks: '',
        };

        return data;
    }

    private createWindrowsModel(): IWindrowsModel {
        let data: IWindrowsModel = {
            parent: this,
            windrowsRemarks: '',
        };

        return data;
    }

    private createTaxiwaysRemarksModel(): ITaxiwaysRemarksModel {
        let data: ITaxiwaysRemarksModel = {
            parent: this,
            taxiwaysRemarks: '',
        };
        return data;
    }

    private createApronRemarksModel(): IApronRemarksModel {
        let data: IApronRemarksModel = {
            parent: this,
            apronRemarks: '',
        };
        return data;
    }

    private configureReactiveForm() {
        this.nsdForm = this.fb.group({
            subjectId: [this.token.subjectId || '', [Validators.required]],
            subjectLocation: this.token.subjectLocation || '',
            conditionsChanging: [{ value: this.token.conditionsChanging, disabled: false }],
            telephone: [{ value: this.token.telephone, disabled: this.isReadOnly }],
            extension: [{ value: this.token.extension, disabled: this.isReadOnly }],
            maintenance: [{ value: this.maintenance, disabled: false }],
            observationDate: [{ value: this.observationDateMoment, disabled: this.isReadOnly }],
            grf: [{ value: this.grfValue, disabled: false }],
            runwayId: [{ value: this.runwayId, disabled: this.isRunwaySelectorDisabled }],

            refreshIcao: [{ value: 0 }, [refreshIcaoValidator]],

            frmClearedWithArr: this.fb.array([]),

            frmContaminantArr: this.fb.array([]),

            frmAverageCRFIArr: this.fb.array([]),

            frmRemainingWidthRemarksArr: this.fb.array([]),

            frmCleaningOperationsArr: this.fb.array([]),

            frmSnowBanksArr: this.fb.array([]),

            frmTreatmentsArr: this.fb.array([]),

            frmWindrowsArr: this.fb.array([]),

            frmTaxiwaysRemarksArr: this.fb.array([]),

            frmApronRemarksArr: this.fb.array([]),

            frmNextScheduledObservationArr: this.fb.array([]),

        });

        this.nsdForm.get('subjectId').valueChanges.subscribe(value => this.subjectIdChanged(value));
    }

    private subjectIdChanged(value: string) {
        this.token.subjectId = value;
    }

    private loadSubject(subjectId: string) {
        this.dataService.getSdoSubject(subjectId)
            .subscribe((sdoSubjectResult: ISdoSubject) => {
                this.subjectSelected = sdoSubjectResult;
                this.subjects = [];
                this.subjects.push(sdoSubjectResult);
                this.loadSdoSubjectRwy(this.subjectSelected.id);
            });
    }

    public sdoSubjectWithTwysOptions(): any {
        const self = this;
        const app = this.rscService.app;
        return {
            minimumInputLength: 2,
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.SdoSubjectPlaceholder')
            },
            language: {
                inputTooShort: function (args) {
                    return self.translation.translate('Schedule.InputTooShort');
                },
            },
            ajax: {
                type: 'GET',
                url: app.apiUrl + "rsc/filterwithrwy",
                dataType: 'json',
                delay: 500,
                headers: {
                    "RequestVerificationToken": app.antiForgeryTokenValue
                },
                data: function (parms, page) {
                    return {
                        Query: parms.term,
                        Size: 200,
                        doaId: app.doaId //set by the app controller
                    }
                },
                processResults: function (data) {
                    return {
                        results: data
                    }
                },
                cache: true
            },
            templateResult: function (data: any) {
                if (data.loading) {
                    return data.text;
                }
                return self.dataService.getSdoExtendedName(data);
            },
            templateSelection: function (selection: ISdoSubject) {
                if (selection && selection.id === "-1") {
                    return selection.text;
                }

                self.updateSubjectSelected(selection);
                return self.dataService.getSdoExtendedName(selection);
            },
        }
    }

    public runwaysOptions = {
        width: "100%",
        placeholder: {
            id: '-1',
            text: this.translation.translate('RSC.SelectRunway')
        }
    }

    private updateSubjectSelected(sdoSubject: ISdoSubject) {
        const subjectId = this.$ctrl('subjectId').value;
        if (subjectId === sdoSubject.id) {
            return;
        }
        if (sdoSubject.id !== subjectId) {
            const subjectCtrl = this.$ctrl('subjectId');
            subjectCtrl.setValue(sdoSubject.id);
            subjectCtrl.markAsDirty();
        }
        this.subjectSelected = sdoSubject;

        if (!sdoSubject.subjectGeo) {
            this.dataService.getSdoSubject(sdoSubject.id)
                .subscribe((sdoSubjectResult: ISdoSubject) => {
                    this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);
                    this.cleanAllSelectedData();
                    this.loadSdoSubjectRwy(sdoSubject.id);
                    this.updateMap(sdoSubjectResult, true);
                });
        } else {
            this.setBilingualRegion(sdoSubject.isBilingualRegion);
            this.loadSdoSubjectRwy(sdoSubject.id);
        }
    }

    private cleanAllSelectedData() {
        this.runways = [];
        this.runwaySelected = null;
        this.runwayComboList = [];
        this.runwayId = '-1';
        this.runwaysRSCData = []

        this.cleanRunwayData();

        this.$ctrl('refreshIcao').setValue(0);
    }

    private cleanRunwayData() {
        this.maintenance = false;
        this.grfValue = true;

        this.$ctrl('maintenance').setValue(this.maintenance);
        this.$ctrl('grf').setValue(this.grfValue);
        this.$ctrl('runwayId').setValue(this.runwayId);

        //Clean the Components of the Runway

    }

    private loadSdoSubjectRwy(sdoId: string) {
        if (sdoId) {
            let self = this;
            this.rscService.getSdoRwys(sdoId)
                .subscribe((sdoData: IRunwaySubject[]) => {
                    //this.cleanSelectedData();
                    this.runways = sdoData;
                    this.prepareRunwayComboList();
                    this.isRunwaySelectorDisabled = (this.$ctrl('subjectId').value === '');
                    if (this.isFormLoadingData) {
                        this.runwaysRSCData = this.model.token.rscRunways;
                        this.runwaySelected = this.runways.find(x => x.runwayId === this.runwaysRSCData[0].runwayId);
                        this.runwayId = this.runwaySelected.runwayId;
                        this.pickRunway(this.runwayId);
                        this.isFormLoadingData = false;
                    }
                });
        }
    }

    private prepareRunwayComboList() {
        this.runwayComboList = [];

        this.runwayComboList = this.runways.map((item: IRunwaySubject) => {
            return {
                id: item.runwayId,
                text: item.designator.toUpperCase(),
            }
        });
        if (!this.isFormLoadingData) {
            this.runwayComboList.unshift({
                id: "-1",
                text: "",
            });
        }
    }

    public wipeComponentData(): void {
        this.model.clearedWidth = null;
        this.model.contaminants = [];
        this.model.rwyFriction = null;
        this.model.remainingWidthRemarks = null;
        this.model.clearingOperations = null;
        //Snowbanks
        //Treatments
        this.model.windrowsRemarks = null;
        this.model.taxiwaysRemarks = null;
        this.model.apronRemarks = null;
        this.model.nextObservation = null;
    }

    public maintenanceChange() {
        if (this.$ctrl('subjectId').value === '') return;
        this.maintenance = !this.maintenance;
        this.$ctrl('maintenance').patchValue(this.maintenance);

        this.wipeComponentData();
        if (!this.maintenance) {
            this.grfValue = false;
            this.$ctrl('grf').setValue(this.grfValue);
            this.setObservationDate(null);

        } else {
            this.grfValue = true;
            this.$ctrl('grf').setValue(this.grfValue);

            this.observationDateMoment = moment.utc().format("YYYY/MM/DD, H:mm");
            this.setObservationDate(this.observationDateMoment);

            //Components
            this.model.clearedWidth = this.createClearedWidthModel();

            this.model.contaminants.push(this.createContaminantModel('Touchdown'));
            this.model.contaminants.push(this.createContaminantModel('Midpoint'));
            this.model.contaminants.push(this.createContaminantModel('Rollout'));

            this.model.rwyFriction = this.createRwyFrictionModel();
            this.model.remainingWidthRemarks = this.createRemainingWidthRemarksModel();
            this.model.clearingOperations = this.createClearingOperationsModel();
            this.model.snowbanks = this.createSnowBanksModel();
            this.model.treatments = this.createTreatmentsModel();
            this.model.windrowsRemarks = this.createWindrowsModel();
            this.model.taxiwaysRemarks = this.createTaxiwaysRemarksModel();
            this.model.apronRemarks = this.createApronRemarksModel();
            this.model.nextObservation = this.createNextObservationModel();
        }
    }

    public grfChange() {
        if (this.$ctrl('subjectId').value === '') return;
        this.grfValue = !this.grfValue;
        this.$ctrl('grf').patchValue(this.grfValue);
        this.model.contaminants = [];

        (this.nsdForm.get('frmContaminantArr') as any).controls.splice(0, this.grfValue ? 1:3);

        if (this.grfValue) {
            this.model.contaminants.push(this.createContaminantModel('Touchdown'));
            this.model.contaminants.push(this.createContaminantModel('Midpoint'));
            this.model.contaminants.push(this.createContaminantModel('Rollout'));
        } else {
            this.model.contaminants.push(this.createContaminantModel('Average'));
        }
    }

    public populateContaminantValues(actual: string): void {
        if (actual === 'Touchdown' || actual === 'Midpoint') {
            let next = '';

            if (actual === 'Touchdown') {
                next = 'Midpoint';
            } else {
                next = 'Rollout';
            }

            var contaminantsCtrl = this.contaminantComponentsList.find(c => c.model.id === actual);
            if (contaminantsCtrl) {
                let nItem = contaminantsCtrl.exportComponentInternalData();
                //next component
                nItem.id = next;
                contaminantsCtrl = this.contaminantComponentsList.find(c => c.model.id === next);
                if (contaminantsCtrl)
                    contaminantsCtrl.refreshValues(nItem);
            }
        }

    }

    public clearContaminantValues(id: string): void {
        if (id.length > 0) {
            var contaminantsCtrl = this.contaminantComponentsList.find(c => c.model.id === id);
            if (contaminantsCtrl) {
                contaminantsCtrl.processClean();
            }
        }
    }

    public clearClearedWidthValues(): void {
        if (this.clearedWidthComponent)
            this.clearedWidthComponent.clearValues();
    }

    public clearAverageRunwayCRFIValues(): void {
        if (this.rwyFriction)
            this.rwyFriction.cleanValues();
    }

    public clearRemainingWidthRemarksValues(): void {
        if (this.remainingWidth)
            this.remainingWidth.clearValues();
    }

    public clearClearingOperationsValues(): void {
        if (this.clearingOperationsComponent)
            this.clearingOperationsComponent.clearValues();
    }

    public clearSnowbanksValues(): void {
        if (this.snowBanksComponent)
            this.snowBanksComponent.clearValues();
    }

    public clearTreatmentsValues(): void {
        if (this.treatmentsComponent)
            this.treatmentsComponent.clearValues();
    }

    public clearWindrowsRemarksValues(): void {
        if (this.windrowsComponent)
            this.windrowsComponent.clearValues();
    }

    public clearTaxiwaysRemarksValues(): void {
        if (this.taxiwaysRemarksComponent)
            this.taxiwaysRemarksComponent.clearValues();
    }

    public clearApronRemarksValues(): void {
        if (this.apronRemarksComponent)
            this.apronRemarksComponent.clearValues();
    }

    public clearNextScheduleObservationValues(): void {
        if (this.nextObservation)
            this.nextObservation.clearValues();
    }

    public selectedRunwayChanged($event) {
        if (this.isReadOnly || !$event.value) return;
        var twySel = this.$ctrl('runwayId');
        if (twySel) {
            var val: string = $event.value;
            if (this.runwayId === val) return;
            this.runwayId = val;
            twySel.patchValue(this.runwayId);
            if (val === '-1') return;

            var runway = this.runwaysRSCData.find(x => x.runwayId === val);
            if (runway) {
                //Update the fields
                this.updateRunwayFields(runway);
            } else {
                if (this.runwaySelected) this.runwaySelected.selected = false;
                //Clean the fields
                this.cleanRunwayData();
                this.maintenance = false;
                this.maintenanceChange();
            }
            this.runwaySelected = this.runways.find(x => x.runwayId === val);
        }
    }

    private getContaminantComponent(name: string): RscContaminantsComponent {
        let ctrl: RscContaminantsComponent = null;
        if (name.trim().toUpperCase() === 'TOUCHDOWN') {
            //Touchdown
            ctrl = this.contaminantComponentsList.find(c => c.model.id === 'Touchdown');
        } else if (name.trim().toUpperCase() === 'MIDPOINT') {
            //Midpoint
            ctrl = this.contaminantComponentsList.find(c => c.model.id === 'Midpoint');
        } else if (name.trim().toUpperCase() === 'ROLLOUT') {
            //Rollout
            ctrl = this.contaminantComponentsList.find(c => c.model.id === 'Rollout');
        } else if (name.trim().toUpperCase() === 'AVERAGE') {
            //Average
            ctrl = this.contaminantComponentsList.find(c => c.model.id === 'Average');
        }
        return ctrl;
    }

    private setClearedPortionData(): IClearedPortion {
        let data: IClearedPortion;
        if (!this.grfValue) {
            //Average
            let dataSfc: IClearedSurface = this.getContaminantComponent('average').exportClearedSurfaceData();

            data = {
                isAverage: true,
                average: dataSfc,
                runwayThirds: null,
            };
        } else {
            //Thirds
            let thirds: IClearedSurfaceThird[] = [];

            //Touchdown
            let dataSfc: IClearedSurfaceThird = this.getContaminantComponent('touchdown').exportClearedSurfaceThirdData();
            thirds.push(dataSfc);

            //Midpoint
            dataSfc = this.getContaminantComponent('midpoint').exportClearedSurfaceThirdData();
            thirds.push(dataSfc);

            //Rollout
            dataSfc = this.getContaminantComponent('rollout').exportClearedSurfaceThirdData();
            thirds.push(dataSfc);

            data = {
                isAverage: false,
                average: null,
                runwayThirds: thirds,
            };
        }

        return data;
    }

    private setRunwayClearedPortion(): IRunwayClearedPortion {
        let cpd = this.setClearedPortionData();
        let data: IRunwayClearedPortion = {
            average: cpd.average,
            isAverage: cpd.isAverage,
            runwayThirds: cpd.runwayThirds,
            averageRwyFriction: null,
            contaminantComment: null,
            otherConditions: null,
            //treatments: null
        };

        return data;
    }

    private setRSCRunwayData(): IRSCRunway {
        let data: IRSCRunway = {
            runwayId: this.runwayId,
            obsTimeUTC: this.observationDateMoment,

            //Data from the ClearedWithComponent
            clearedFull: true, 
            centredClearedWidth: 0,
            clearedPortionOffsets: null,

            //Data from the Contaminants
            clearedPortion: this.setRunwayClearedPortion(),

            //Data from the Remaining Width Remarks TextBox
            remainingWidth: null,

            noWinterMaintenance: !this.maintenance,

            //Data from SnowBanksComponent
            snowbanks: null,

            //Data from ClearingOperationsComponent
            clearingOperations: null,

            //Data from TreatmentsComponent
            treaments: null,
        };

        return data;
    }

    private setRSCPayload(): any {

        const app = this.rscService.app;
        var time = moment().add(8, 'hours');
        let nextObsComponent = this.nextObservation.exportComponentInternalData();

        let token: any = {
            immediate: true,
            permanent: false,
            originator: app.usrName,
            endValidity: moment.utc(time, 'YYMMDDHHmm'),

            subjectId: this.subjectId,
            subjectLocation: this.$ctrl('subjectLocation').value,
            isBilingual: this.isBilingualRegion,

            aerodrome: this.subjectSelected.designator,
            aprons: null,
            conditionsChanging: this.token.conditionsChanging,
            telephone: (this.token.conditionsChanging) ? this.token.telephone : '',
            extension: (this.token.conditionsChanging) ? this.token.extension : '',

            generalRemarks: nextObsComponent.generalRemarks,
            nextPlannedObs: nextObsComponent.nextScheduledObservation,

            taxiways: null,
            rscRunways: this.runwaysRSCData
        };

        return token;
    }

    public includeRunway() {
        if (!this.runwaySelected) return;
        var sel = this.runwaysRSCData.findIndex(x => x.runwayId === this.runwaySelected.runwayId);
        if (sel < 0) {
            this.runwaySelected.selected = true;
            let rwyData = this.setRSCRunwayData();
            this.runwaysRSCData.push(rwyData);
            if (this.validateForm()) this.generateIcao();
        }
    }

    public modifyRunway() {
        if (!this.runwaySelected) return;
        var sel = this.runwaysRSCData.findIndex(x => x.runwayId === this.runwaySelected.runwayId);
        if (sel > -1) {
            this.runwaySelected.selected = true;
            let rwyData = this.setRSCRunwayData();
            this.runwaysRSCData.splice(sel, 1, rwyData);
            if (this.validateForm()) this.generateIcao();
        }

    }

    public excludeRunway() {
        if (!this.runwaySelected) return;
        var sel = this.runwaysRSCData.findIndex(x => x.runwayId === this.runwaySelected.runwayId);
        if (sel > -1) {
            this.runwaySelected.selected = false;
            this.runwaysRSCData.splice(sel, 1);
            if (this.validateForm()) this.generateIcao();
        }
    }

    private updateRunwayFields(runway: IRSCRunway) {
        this.grfValue = true;
        this.maintenance = !runway.noWinterMaintenance;
        if (this.maintenance) {
            this.grfValue = !runway.clearedPortion.isAverage;
            //Do something
            if (this.grfValue) {
            } else {
            }
        }

        this.$ctrl('maintenance').patchValue(this.maintenance);
        this.$ctrl('grf').patchValue(this.grfValue);

        if (this.isBilingualRegion) {
        } else {
        }
    }

    public pickRunway(val: string) {
        this.runwayId = val;
        this.$ctrl('runwayId').patchValue(this.runwayId);

        var runwayData = this.runwaysRSCData.find(x => x.runwayId === val);
        if (runwayData) {
            //Update the fields
            this.updateRunwayFields(runwayData);
        } else {
            if (this.runwaySelected) this.runwaySelected.selected = false;
            this.cleanRunwayData();
        }
        this.runwaySelected = this.runways.find(x => x.runwayId === val);
    }

    public getRunwayInfoToShow(rwy: IRSCData): string {
        if (rwy) {
            if (rwy.noMaintenance) {
                return 'NO WINTER MAINTENANCE';
            }
            if (rwy.average) {
                return rwy.full.data;
            } else {
                return '1/3 ' + rwy.direction.firstData.data + ' 2/3 ' + rwy.direction.middleData.data + ' 3/3 ' + rwy.direction.endData.data;
            }
        }
        return "";
    }

    public isExcludeButtonDisabled() {
        if (this.isReadOnly) return true;
        if (!this.runwaySelected) return true;
        var sel = this.runwaysRSCData.find(x => x.runwayId === this.runwaySelected.runwayId);
        if (sel) return false;
        else return true;
    }

    public isModifyButtonDisabled() {
        if (this.isReadOnly) return true;
        if (!this.runwaySelected) return true;
        var sel = this.runwaysRSCData.find(x => x.runwayId === this.runwaySelected.runwayId);
        if (sel) {
            //Do something
            return false;
        }
        else return true;
    }

    public isIncludeButtonDisabled() {
        if (this.isReadOnly) return true;
        if (!this.runwaySelected) return true;
        var sel = this.runwaysRSCData.find(x => x.runwayId === this.runwaySelected.runwayId);
        //Ifthe runway is NOT included yet on the list, we need to validate the data
        if (!sel) return !this.validateData();
        return true;
    }

    private validateData(): boolean {
        //Fake data
        if (this.$ctrl('maintenance').value === true) {
            let valid = false;

            //Validate Contaminants component
            valid = this.$ctrl('frmContaminantArr').valid;
            if (!valid) return false;
            if (this.$ctrl('frmContaminantArr').pristine) return false;

            if (this.$ctrl('grf').value === true) {
                //Validate the data
                valid = this.$ctrl('frmContaminantArr').valid;
                if (!valid) return false;

                // End validate data
            } else {
                // Validate the components
                valid = this.$ctrl('frmContaminantArr').valid;
                if (!valid) return false;
                // Keep validating other components

                //End validate components
            }
        }
        return true;
    }

    private validateForm(): boolean {
        this.subjectId = this.$ctrl('subjectId').value;
        this.$ctrl('refreshIcao').setValue(0);
        if (!this.subjectId || this.subjectId.length === 0 || this.subjectId === '-1') 
            return false;

        if (this.$ctrl('conditionsChanging').value === true) {
            if (this.$ctrl('telephone').valid) {
                this.$ctrl('refreshIcao').setValue(1);
                return true;
            }
            return false;
        }

        if (!this.runways || !this.runwaysRSCData)
            return false;
        if (this.runwaysRSCData.length < 1)
            return false;

        //To DO:
        //For each runwaysSelectedData, validate the Data

        //For now, it is always not valid
        //this.$ctrl('refreshIcao').setValue(1);
        //return true;

        return false;
    }

    public rowColor(id: string) {
        if (!this.runwaySelected) {
            return '';
        }
        if (this.runwaySelected.runwayId === id) return 'bg-submitted';
        return '';
    }

    public buttonColor() {
        if (!this.isModifyButtonDisabled()) {
            //do something
        }
        return 'btn-info';
    }

    private setBilingualRegion(value: boolean) {
        this.token.isBilingual = value;
        this.isBilingualRegion = value;
        if (this.isBilingualRegion) {
            if (!this.isReadOnly) {
                //
            }
        } else {
            if (!this.isReadOnly) {
                //
            }
        }
    }

    private resetIcaoModel() {
        this.model.code23 = "";
        this.model.code45 = "";
        this.model.radius = 0;
        this.model.lowerLimit = 0;
        this.model.upperLimit = 0;
        this.model.scope = "";
        this.model.series = "";
        this.model.startActivity = null;
        this.model.endValidity = null;
        this.model.fir = "";
        this.model.itemA = "";
        this.model.itemD = "";
        this.model.itemE = "";
        this.model.itemEFrench = "";
        this.model.itemF = 0;
        this.model.itemF = 0;
    }

    private updateModel(data) {
        data.organizationId = data.organizationId ? data.organizationId : this.model.organizationId;
        data.proposalId = data.proposalId ? data.proposalId : this.model.proposalId;

        const attachments = this.model.attachments;

        this.model = data;
        this.model.attachments = this.model.attachments || attachments;
        this.token = data.token;

    }

    private generateIcao() {
        if (this.model.proposalType === ProposalType.Cancel || this.nsdForm.dirty && !this.isReadOnly) {
            if (this.waitingForGeneratingIcao) return;
            this.isSaveButtonDisabled = (this.token.conditionsChanging) ? true : !this.nsdForm.valid;
            this.isSubmitButtonDisabled = !this.nsdForm.valid;
            if (this.nsdForm.valid) {
                this.isSubmitButtonDisabled = this.model.status === ProposalStatus.Submitted || this.model.status === ProposalStatus.Terminated;
                this.resetIcaoModel();
                const payload = this.setPayload(true);

                this.waitingForGeneratingIcao = true;
                this.dataService.generateIcao(payload)
                    .subscribe(data => {
                        this.model.token.isBilingual = data.token.isBilingual; // get the isBilingual flag from the ICAO generation
                        data.token = this.model.token; //don't override tokens with server response
                        data.groupId = this.model.groupId;
                        data.rejectionReason = this.model.rejectionReason;
                        this.updateModel(data)
                        this.waitingForGeneratingIcao = false;
                    }, error => {
                        this.waitingForGeneratingIcao = false;
                        this.isSaveButtonDisabled = true;
                        this.isSubmitButtonDisabled = true;
                        let errorMessage = error.message;
                        if (error.exceptionMessage) {
                            errorMessage += `${errorMessage} ${error.exceptionMessage}`;
                        }
                        this.toastr.error(this.translation.translate('Dashboard.MessageIcaoError') + errorMessage, "", { 'positionClass': 'toast-bottom-right' });
                    });
            } else {
                this.waitingForGeneratingIcao = false;

                this.model.itemE = "";
                this.model.itemEFrench = "";
            }
        } else {
            this.waitingForGeneratingIcao = false;
        }
    }

    private setPayload(toGenerateIcao: boolean): any {

        this.model.token = this.setRSCPayload();

        let payload: any = {
            categoryId: this.model.categoryId,
            version: this.model.version,
            name: this.model.name,
            token: this.model.token,
            referredSeries: this.model.referredSeries,
            referredNumber: this.model.referredNumber,
            referredYear: this.model.referredYear,
            proposalType: this.model.proposalType,
            organizationId: this.model.organizationId,
            proposalId: this.model.proposalId,
            status: this.model.status,
            statusUpdated: this.model.statusUpdated,
            parentNotamId: this.model.parentNotamId,
            notamId: this.model.notamId,
            groupId: this.model.groupId,
            number: this.model.number,
            year: this.model.year,
            rowVersion: this.model.rowVersion
        }

        return payload;
    }    

    public isEditing() {

        return (this.$ctrl('refreshIcao').value === 0);
    }

    public enableEditing() {
        this.$ctrl('refreshIcao').setValue(0);
    }

    public stopEditing() {
        this.$ctrl('refreshIcao').setValue(1);
    }

    conditionsChangingClick() {
        if (this.$ctrl('subjectId').value === '') return;
        this.token.conditionsChanging = !this.token.conditionsChanging;
        this.$ctrl('conditionsChanging').patchValue(this.token.conditionsChanging);
        if (this.token.conditionsChanging) {
            this.$ctrl('telephone').setValidators(phoneNumberValidator);
        } else {
            this.$ctrl('telephone').clearValidators();
        }
    }

    phoneNumberFocus() {
        const phoneCtrl = this.$ctrl("telephone");
        const value = PhoneValidator.stripFormat(phoneCtrl.value);
        phoneCtrl.setValue(value);
    }

    phoneNumberBlur() {
        const phoneCtrl = this.$ctrl("telephone");
        const value = PhoneValidator.formatTenDigits(phoneCtrl.value);
        phoneCtrl.setValue(value);
    }

    validateTelephone() {
        if (this.token.conditionsChanging) {
            const phoneCtrl = this.$ctrl("telephone");
            return phoneCtrl.valid || this.nsdForm.pristine;
        }
        return true;
    }


    // For the NsdButtonBarComponent form

    isSaveDraftButtonDisabled() {
        this.isSaveButtonDisabled = (this.$ctrl('conditionsChanging').value === true) ? true : !this.validateForm();
        return this.isSaveButtonDisabled;
    }

    isSubmitProposalButtonDisabled() {
        this.isSubmitButtonDisabled = !this.validateForm();
        return this.isSubmitButtonDisabled || this.isWaitingForResponse;
    }

    isRejectProposalDisable() {
        return true;
    }

    isParkProposalDisabled() {
        this.isSaveButtonDisabled = (this.$ctrl('conditionsChanging').value === true) ? true : !this.validateForm();
        return this.isSaveButtonDisabled;
    }

    saveDraft() {
        const payload = this.setPayload(false);
        function success(data): void {
            this.isSaveButtonDisabled = true;
            //this.txtBackToReviewList = this.backToReviewListLabel;

            this.updateModel(data);

            this.toastr.success(this.translation.translate('Dashboard.MessageSaveSuccess'), "", { 'positionClass': 'toast-bottom-right' });
        }

        function failure(error) {
            this.toastr.error(this.translation.translate('Dashboard.MessageSaveFailure') + error.message, "", { 'positionClass': 'toast-bottom-right' });
        }

        this.dataService.saveDraftProposal(payload)
            .subscribe(data => {
                success.call(this, data);
            }, error => {
                failure.call(this, error);
            });
    }

    submitProposal(onError: Function) {
        this.disseminateProposal(onError);
    }

    discardProposal(onError: Function) {
        this.dataService.discardProposal(this.model.proposalId)
            .subscribe(response => {
                let msg = this.translation.translate('Dashboard.MessageDiscardSuccess')
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                this.isSaveButtonDisabled = true;
                this.disposeMap();
                this.router.navigate(['/dashboard']);
            }, error => {
                if (onError)
                    onError(error);
                let msg = this.translation.translate('Dashboard.MessageDiscardSuccess')
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    parkProposal(onError: Function) {
        this.isWaitingForResponse = true;
        const payload = this.setPayload(false);
        return this.dataService.parkProposal(payload)
            .subscribe(data => {
                this.isSaveButtonDisabled = true;

                this.nsdForm.markAsPristine();
                this.router.navigate(['/dashboard']);
            }, error => {
                this.isWaitingForResponse = false;
                if (onError)
                    onError.call(error);
                this.toastr.error(this.translation.translate('Dashboard.MessageParkFailure') + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    disseminateProposal(onError: Function) {
        this.isWaitingForResponse = true;
        const payload = this.setPayload(false);
        this.dataService.disseminateProposal(payload)
            .subscribe(data => {
                this.isSaveButtonDisabled = true;
                this.nsdForm.markAsPristine();
                this.router.navigate(['/dashboard']);
            }, error => {
                this.isWaitingForResponse = false;
                if (onError)
                    onError.call(error);
                this.toastr.error(this.translation.translate('Dashboard.MessageDisseminateFailure') + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    backToNofProposals() {
        this.disposeMap();
        this.router.navigate(['/dashboard']);
    }

    backToProposals() {
        this.disposeMap();
        this.router.navigate(['/dashboard']);
    }

    // End for NsdButtonBarComponent form


    // this is nessecary because of how we load the map in a modal window
    private initMap() {
        const winObj = this.windowRef.nativeWindow;
        const leafletmap = winObj['app'].leafletmap;

        if (leafletmap) {
            leafletmap.initMap();
        }
    }

    private mapHealthCheck() {
        this.dataService.getMapHealth().subscribe(response => {
            if (response) {
                this.mapHealthy = true;
            }
        }, error => {
            this.mapHealthy = false;
            return false;
        });
    }

    mapAvailable(): boolean {
        return this.mapHealthy;
    }

    private updateMap(sdoSubject: ISdoSubject, acceptSubjectLocation: boolean) {
        this.leafletmap.clearFeaturesOnLayers(['DOA']);
        if (sdoSubject.subjectGeoRefPoint) {
            const geojson = JSON.parse(sdoSubject.subjectGeoRefPoint);
            if (geojson.features.length === 1) {
                const subLocation = this.leafletmap.geojsonToTextCordinates(geojson);
                const coordinates = geojson.features[0].geometry.coordinates;
                const markerGeojson = acceptSubjectLocation ? geojson : this.getGeoJsonFromLocation(this.token.subjectLocation);
                if (acceptSubjectLocation) {
                    this.nsdForm.patchValue({
                        subjectLocation: subLocation,
                        latitude: coordinates[1],
                        longitude: coordinates[0],
                    }, { onlySelf: true, emitEvent: false });
                }
                this.leafletmap.addMarker(geojson, sdoSubject.name, () => {
                    this.leafletmap.addFeature(sdoSubject.subjectGeo, sdoSubject.name, () => {
                        if (markerGeojson) {
                            this.leafletmap.setNewMarkerLocation(markerGeojson);
                        }
                    });
                });
            }
        }

    }

    private getGeoJsonFromLocation(value: string) {
        const location = this.locationService.parse(value);
        return location ? this.leafletmap.latitudeLongitudeToGeoJson(location.latitude, location.longitude) : null;
    }

    activateTab(tab) {
        switch (tab) {
            case 'map':
                this.tabActive = 0;
                break;
            case 'icao-text':
                this.tabActive = 1;
                break;
            case 'icao-format':
                if (this.nsdForm.valid) {
                    this.tabActive = 2;
                }
                break;
            case 'geoMap':
                this.tabActive = 3;
                break;
            case 'last-icao-format':
                if (this.nsdForm.valid) {
                    this.tabActive = 4;
                }
                break;
            default:
                this.tabActive = 1;
        }
    }

    private validateMap() {
        const winObj = this.windowRef.nativeWindow;
        const leafletmap = winObj['app'].leafletmap;
        if (leafletmap) {
            leafletmap.resizeMapForModal();
        }
    }

    private validateGeoMap() {
        const winObj = this.windowRef.nativeWindow;
        const leafletgeomap = winObj['app'].leafletgeomap;
        if (leafletgeomap) {
            leafletgeomap.resizeGeoMapForModal();
        }
    }

    private disposeMap() {
        const winObj = this.windowRef.nativeWindow;
        const leafletmap = winObj['app'].leafletmap;
        if (leafletmap) {
            leafletmap.dispose();
        }
    }

    $ctrl(name: string): AbstractControl {
        return this.nsdForm.get(name);
    }

    get langCulture() {
        return (window["app"].cultureInfo || "en").toUpperCase();
    }   
}

function refreshIcaoValidator(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value === 0) {
        return { 'required': true }
    }
    return null;
}

function phoneNumberValidator(c: AbstractControl): { [key: string]: boolean } | null {
    const value = c.value;
    if (!value)
        return { 'required': true }

    if (!PhoneValidator.isValidPhoneNumber(c.value, 10)) {
        return { 'pattern': true }
    }

    return null;
}

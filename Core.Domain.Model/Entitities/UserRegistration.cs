﻿namespace NavCanada.Core.Domain.Model.Entitities
{
    public class UserRegistration
    {
        public int Id { get; set; }
        public string UserProfileId { get; set; }
        public UserProfile UserProfile { get; set; }
        public string VerificationCode { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace NavCanada.Core.TaskEngine.Contracts
{
    public interface IMessageTaskFactory
    {
        List<IMessageTask> GetTasks(IMessageTaskContext context);
    }
}

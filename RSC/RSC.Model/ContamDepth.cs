﻿namespace RSC.Model
{
    /// <summary>
    /// Contaminant depth as Trace or inches.
    /// </summary>
    public class ContamDepth
    {
        /// <summary>
        /// Depth of Contamination in inches.
        /// </summary>
        public decimal ContamInches { get; set; }
        /// <summary>
        /// There is just a TRACE of the contaminant.
        /// </summary>
        public bool ContamTrace { get; set; }
    }
}
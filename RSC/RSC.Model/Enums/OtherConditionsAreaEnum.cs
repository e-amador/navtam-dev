﻿namespace RSC.Model.Enums
{
    public enum OtherConditionsAreaEnum : int
    {
        IcePatches = 122,
        CompactedSnowPatches = 123,
        StandingWaterPatches = 124
    }
}

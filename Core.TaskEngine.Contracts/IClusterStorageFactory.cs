﻿namespace NavCanada.Core.TaskEngine.Contracts
{
    public interface IClusterStorageFactory
    {
        bool NewStorageConnection(out IClusterStorage clusterStorage);
    }
}

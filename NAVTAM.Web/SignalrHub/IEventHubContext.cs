﻿using Microsoft.AspNet.SignalR;

namespace NAVTAM.SignalrHub
{
    public interface IEventHubContext
    {

        IHubContext Context { get; }
    }
}

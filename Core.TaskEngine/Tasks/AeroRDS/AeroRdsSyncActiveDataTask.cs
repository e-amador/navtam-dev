﻿using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NavCanada.Core.TaskEngine.Contracts;
using NavCanada.Core.TaskEngine.Helpers;
using NavCanada.Core.TaskEngine.Mto;
using System;
using System.Diagnostics;

namespace NavCanada.Core.TaskEngine.Tasks.AeroRDS
{
    public class AeroRdsSyncActiveDataTask : IMessageTask
    {
        public void Execute(IMessageTaskContext context)
        {
            var logger = context.ExternalServices.Logger;
            var loggerType = GetType();

            // if this were to fail we don't want to put the system down
            context.Mto.Add(MessageDefs.OverridesCriticalFailureKey, true, DataType.Boolean);

            try
            {
                logger.LogDebug(loggerType, "AeroRds Sync active data task STARTED excecuting...");

                var sw = Stopwatch.StartNew();

                var airacDateStr = context.Mto.GetProperty(MessageDefs.NextAiracCycleDate, string.Empty);
                if ("GlobalSync".Equals(airacDateStr))
                {
                    // perform a global sync on the current active date
                    ExecuteGlobalSync(context.ExternalServices.AeroRdsFeatureProxy, context.ClusterStorage.Uow, context.ExternalServices.Logger);
                }
                else
                {
                    var airacDate = ParseAiracDate(airacDateStr);
                    if (airacDate < DateTime.UtcNow)
                    {
                        // perform the sync on an EffectiveDate
                        ExecuteEffectiveDateSync(context.ExternalServices.AeroRdsFeatureProxy, context.ClusterStorage.Uow, context.ExternalServices.Logger, airacDate);
                        // queue the sync for the next AIRAC cycle
                        QueueNextEffectiveDateCycle(context.ClusterStorage.Queues.TaskEngineQueue, airacDate);
                    }
                }

                sw.Stop();

                logger.LogDebug(loggerType, $"AeroRds Sync active data task COMPLETED in {sw.Elapsed}!");
            }
            catch (Exception ex)
            {
                logger.LogError(ex, loggerType, "AeroRds Sync active data task FAILED!");
                throw;
            }
        }

        static void ExecuteGlobalSync(IAeroRdsFeatureProxy aeroRdsProxy, IUow uow, ILogger logger)
        {
            PerformDataSync(new AeroRDSActiveDataProvider(aeroRdsProxy), uow, logger);
        }

        static void ExecuteEffectiveDateSync(IAeroRdsFeatureProxy aeroRdsProxy, IUow uow, ILogger logger, DateTime airacDate)
        {
            PerformDataSync(new AeroRDSEffectiveDataProvider(aeroRdsProxy, airacDate), uow,logger);
        }

        static void PerformDataSync(IAeroRDSDataProvider dataProvider, IUow uow, ILogger logger)
        {
            var syncEngine = new SdoCacheSyncEngine(dataProvider, uow, logger);
            syncEngine.Execute();
        }

        static void QueueNextEffectiveDateCycle(IQueue queue, DateTime airacDate)
        {
            var nextDate = airacDate.AddDays(28).ToShortDateString();
            var mto = MtoHelper.CreateMtoMessage(MessageTransferObjectId.AeroRDSSyncActiveDataMsg, m => m.Add(MessageDefs.NextAiracCycleDate, nextDate, DataType.String));
            queue.Publish(mto);
        }

        static DateTime ParseAiracDate(string value)
        {
            DateTime date;
            return DateTime.TryParse(value, out date) ? date : DateTime.UtcNow.AddDays(1);
        }
    }
}
﻿import { Component, Inject, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormGroup, FormBuilder, AbstractControl, Validators, ValidatorFn } from '@angular/forms'
import { WindowRef } from '../../common/windowRef.service';
import { LocaleService, TranslationService } from 'angular-l10n';
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';
import { DataService } from '../shared/data.service'
import { MemoryStorageService } from '../shared/mem-storage.service'
import { IIcaoSubjectCondition } from '../shared/data.model';

@Component({
    templateUrl: '/app/admin/icaosubjects/icaoconditions-edit.component.html'
})
export class IcaoConditionEditComponent implements OnInit {

    model: any = null;
    action: string;
    isReadOnly: boolean;

    icaoConditionForm: FormGroup;

    radiusLimit: number = 999;
    lowerLimit: number = 998;
    upperLimit: number = 999;
    descriptionTextLimit: number = 150;

    isSubmitting: boolean = false;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private winRef: WindowRef,
        private memStorageService: MemoryStorageService,
        public locale: LocaleService,
        public translation: TranslationService) {

        if (this.winRef.appConfig.ccs)
            this.router.navigate(['/administration']);
    }

    ngOnInit() {
        this.action = this.activatedRoute.snapshot.data[0].action;
        this.model = this.activatedRoute.snapshot.data['model'];

        this.isReadOnly = this.action === "view";


        this.configureReactiveForm();
    }

    onSelectedCodeChanged(data: { value: string }) {
        if (data.value !== "-1") {
            this.icaoConditionForm.get('code').setValue(data.value);
            this.icaoConditionForm.markAsDirty();
        } else {
            this.icaoConditionForm.get('code').setValue("-1");
        }
    }

    backToIcaoSubjectList() {
        this.router.navigate(['/administration/icaosubjects']);
    }

    onSubmit() {
        if (this.isSubmitting) return;
        this.isSubmitting = true;
        const icaoCondition = this.prepareUpdateIcaoCondition();
        this.dataService.updateIcaoCondition(icaoCondition)
            .subscribe(data => {
                let msg = this.translation.translate('IcaoSubjectSession.SuccessConditionUpdated');
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                this.backToIcaoSubjectList();
            }, error => {
                this.isSubmitting = false;
                let msg = this.translation.translate('IcaoSubjectSession.FailureConditionUpdated');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    validateDescription() {
        const ctrl = this.icaoConditionForm.get("description");
        return ctrl.valid || this.icaoConditionForm.pristine;
    }

    validateDescriptionLength(ctrlName: string) {
        const descCtrl = this.icaoConditionForm.get(ctrlName);
        let DescriptionLength: string = descCtrl.value;
        return DescriptionLength.length > this.descriptionTextLimit;
    }
    
    validateDescriptionFrench() {
        const ctrl = this.icaoConditionForm.get("descriptionFrench");
        return ctrl.valid || this.icaoConditionForm.pristine;
    }

    validateCode45() {
        if (this.icaoConditionForm.pristine) return true;
        const fc = this.icaoConditionForm.get("code");
        if (fc.errors) return false;
        if (!fc.value) return false;
        if (!isALetter(fc.value)) return false;
        if (fc.value.length !== 2) return false;

        return true;
    }

    validateRadius() {
        if (this.icaoConditionForm.pristine) return true;
        const fc = this.icaoConditionForm.get('radius');
        if (fc.errors) return false;
        if (fc.value) {
            if (fc.value > this.radiusLimit) return false;
            if (fc.value < 1) return false;
        } else return false;
        return true;
    }

    validateLowerLimit() {
        if (this.icaoConditionForm.pristine) return true;
        const ll = this.icaoConditionForm.get('lowerLimit');
        const ul = this.icaoConditionForm.get('upperLimit');
        if (ll.value === undefined) return false;
        if (!isANumber(ll.value)) return false;
        if (+ll.value > this.lowerLimit) return false;
        if (+ul.value && isANumber(ul.value)) {
            if (+ll.value < +ul.value) return true;
            else return false;
        }
        return true;
    }

    validateUpperLimit() {
        if (this.icaoConditionForm.pristine) return true;
        const ll = this.icaoConditionForm.get('lowerLimit');
        const ul = this.icaoConditionForm.get('upperLimit');
        if (!ul.value) return false;
        if (!isANumber(ul.value)) return false;
        if (+ul.value > this.upperLimit) return false;
        if (+ll.value && isANumber(ll.value)) {
            if (+ll.value >= +ul.value) return false;
        }
        return true;
    }

    disableSubmit(): boolean {
        if (this.icaoConditionForm.pristine || !this.icaoConditionForm.valid) return true;
        if (!this.validateCode45() || !this.validateLowerLimit() || !this.validateUpperLimit()) return true;
        if (this.isSubmitting) return true;
        return false;
    }

    checkboxChanged(controlName: string, checked: boolean) {
        const control = this.icaoConditionForm.get(controlName);
        control.setValue(checked);
        control.markAsDirty();
    }

    decValue(fieldName) {
        let value = 0;
        switch (fieldName) {
            case 'lowerLimit':
                value = +this.icaoConditionForm.get('lowerLimit').value;
                if (value > 0) {
                    this.icaoConditionForm.get('lowerLimit').setValue(value - 1);
                    this.icaoConditionForm.get('lowerLimit').updateValueAndValidity();
                    this.icaoConditionForm.get('upperLimit').updateValueAndValidity();
                }
                break;
            case 'upperLimit':
                value = +this.icaoConditionForm.get('upperLimit').value;
                if (value > 1) {
                    this.icaoConditionForm.get('upperLimit').setValue(value - 1);
                    this.icaoConditionForm.get('lowerLimit').updateValueAndValidity();
                    this.icaoConditionForm.get('upperLimit').updateValueAndValidity();
                }
                break;
            case 'radius':
                value = +this.icaoConditionForm.get('radius').value;
                if (value > 1) {
                    this.icaoConditionForm.get('radius').setValue(value - 1);
                    this.icaoConditionForm.get('radius').markAsDirty()
                }
                break;
        }
    }

    incValue(fieldName) {
        let value = 0;
        switch (fieldName) {
            case 'lowerLimit':
                value = +this.icaoConditionForm.get('lowerLimit').value;
                if (value < this.lowerLimit) {
                    this.icaoConditionForm.get('lowerLimit').setValue(value + 1);
                    this.icaoConditionForm.get('lowerLimit').updateValueAndValidity();
                }
                break;
            case 'upperLimit':
                value = +this.icaoConditionForm.get('upperLimit').value;
                if (value < this.upperLimit) {
                    this.icaoConditionForm.get('upperLimit').setValue(value + 1);
                    this.icaoConditionForm.get('upperLimit').updateValueAndValidity();
                }
                break;
            case 'radius':
                value = +this.icaoConditionForm.get('radius').value;
                if (value < this.radiusLimit) {
                    this.icaoConditionForm.get('radius').setValue(value + 1);
                    this.icaoConditionForm.get('radius').markAsDirty();
                }
                break;
        }
    }

    public validateKeyPressNumber(evt: any) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
        }
    }

    public validateKeyPressLetter(evt: any) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (key !== '\u0000' && !isALetter(key)) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
        }
    }

    private configureReactiveForm() {
        this.icaoConditionForm = this.fb.group({
            description: [{ value: this.model.description, disabled: this.isReadOnly }, [Validators.required]],
            descriptionFrench: [{ value: this.model.descriptionFrench, disabled: this.isReadOnly }, [Validators.required]],
            code: [{ value: this.model.code, disabled: this.isReadOnly }, [Validators.required]],
            radius: [{ value: this.model.radius, disabled: this.isReadOnly }, []],
            lowerLimit: [{ value: this.model.lower, disabled: this.isReadOnly }, []],
            upperLimit: [{ value: this.model.upper, disabled: this.isReadOnly }, []],
            grpPurposes: this.fb.group({
                purposeB: this.model.b,
                purposeM: this.model.m,
                purposeN: this.model.n,
                purposeO: this.model.o,
            }),
            grpTraffic: this.fb.group({
                trafficI: this.model.i,
                trafficV: this.model.v,
            }),
            requiresItemFG: [{ value: this.model.requiresItemFG, disabled: this.isReadOnly }],
            requiresPurpose: [{ value: this.model.requiresPurpose, disabled: this.isReadOnly }],
            cancellationOnly: [{ value: this.model.cancellationOnly, disabled: this.isReadOnly }],
            active: [{ value: this.model.active, disabled: this.isReadOnly }]
        });

        this.icaoConditionForm.get("radius").setValidators(radiusValidatorFn(this.radiusLimit));
        this.icaoConditionForm.get("grpPurposes").setValidators(purposeAllowValidator);
        this.icaoConditionForm.get("grpTraffic").setValidators(trafficValidator);
    }

    private prepareUpdateIcaoCondition(): IIcaoSubjectCondition {
        var code45 = this.icaoConditionForm.get('code').value;
        code45 = code45.toString().toUpperCase();
        const icaoCondition: IIcaoSubjectCondition = {
            id: this.model.id,
            subjectId: this.model.subjectId,
            description: this.icaoConditionForm.get('description').value,
            descriptionFrench: this.icaoConditionForm.get('descriptionFrench').value,
            code: code45,
            radius: this.icaoConditionForm.get('radius').value,
            lower: this.icaoConditionForm.get('lowerLimit').value,
            upper: this.icaoConditionForm.get('upperLimit').value,
            b: this.icaoConditionForm.get('grpPurposes.purposeB').value,
            m: this.icaoConditionForm.get('grpPurposes.purposeM').value,
            o: this.icaoConditionForm.get('grpPurposes.purposeO').value,
            n: this.icaoConditionForm.get('grpPurposes.purposeN').value,
            i: this.icaoConditionForm.get('grpTraffic.trafficI').value,
            v: this.icaoConditionForm.get('grpTraffic.trafficV').value,
            requiresItemFG: this.icaoConditionForm.get('requiresItemFG').value,
            requiresPurpose: this.icaoConditionForm.get('requiresPurpose').value,
            cancellationOnly: this.icaoConditionForm.get('cancellationOnly').value,
            active: this.icaoConditionForm.get('active').value
        }

        return icaoCondition;
    }
}

function radiusValidatorFn(maxRadius: number): ValidatorFn {
    return (c: AbstractControl): { [key: string]: any } => {
        if (typeof (c.value) === "string") {
            if (!c.value) {
                return { 'required': true }
            }
            if (c.value === "") {
                return { 'required': true }
            }
        }
        if (isANumber(c.value)) {
            if (+c.value < 1 || +c.value > maxRadius) {
                return { 'invalid': true }
            }
            var v: number = +c.value;
            if (v > maxRadius) {
                return { 'invalid': true }
            }
            return null;
        }
        return { 'invalid': true }
    };
}

function trafficValidator(c: AbstractControl): { [key: string]: boolean } | null {
    const i = c.get("trafficI");
    const v = c.get("trafficV");

    if (i.value || v.value) {
        return null
    }

    return { 'trafficRequired': true }
}

function purposeAllowValidator(c: AbstractControl): { [key: string]: boolean } | null {
    const nCtrl = c.get("purposeN");
    const bCtrl = c.get("purposeB");
    const oCtrl = c.get("purposeO");
    const mCtrl = c.get("purposeM");

    const n = nCtrl.value;
    const b = bCtrl.value;
    const o = oCtrl.value;
    const m = mCtrl.value;

    if (n || b || o || m) {
        if (n && b && !o && !m) return null; //NB
        if (b && o && !m && !n) return null; //BO
        if (m && !o && !b && !n) return null; //M
        if (b && !o && !m && !n) return null; //B
        if (n && b && o && !m) return null; //NBO

        return { 'purposeInvalid': true }
    }
    else {
        return { 'purposeRequired': true }
    }
}

function isANumber(str: string): boolean {
    return !/\D/.test(str);
}

function isALetter(str: string): boolean {
    return !/[^a-z]/i.test(str);
}

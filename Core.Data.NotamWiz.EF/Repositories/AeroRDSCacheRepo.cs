﻿using NavCanada.Core.Data.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Domain.Model.Entitities;
using System.Data.Entity;

namespace NavCanada.Core.Data.EF.Repositories
{
    public class AeroRDSCacheRepo : Repository<AeroRDSCache>, IAeroRDSCacheRepo
    {
        public AeroRDSCacheRepo(DbContext dbContext) : base(dbContext)
        {
        }
    }
}

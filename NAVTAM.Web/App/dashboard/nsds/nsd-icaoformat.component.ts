﻿import { Component, Input } from '@angular/core'

import * as moment from 'moment';

//TODO: Move ICAO Generation Logic back to server
@Component({
    selector: 'icaoformat',
    templateUrl: '/app/dashboard/nsds/nsd-icaoformat.component.html'
})
export class NsdIcaoFormatComponent {

    @Input() icaoText : any;
    @Input() loading: boolean;

    //get icaoNotamText(): string {
    //    //return this.model && this.model.icaoText ? this.model.icaoText.aftnText : null;
    //    return this.model ? this.model.icaoText : null;
    //}

    get icaoNotamText(): string {
        return this.icaoText;
    }

    copyIcaoText() {
        this.copyToClipBoard(this.icaoText);
    }

    private copyToClipBoard(text) {
        const el = document.createElement('textarea');
        el.value = text;
        el.setAttribute('readonly', '');
        el.style.position = 'absolute';
        el.style.left = '-9999px';
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    }
}

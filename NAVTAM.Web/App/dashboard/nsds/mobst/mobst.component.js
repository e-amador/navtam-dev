"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MObstComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var data_service_1 = require("../../shared/data.service");
var location_service_1 = require("../../shared/location.service");
var mobst_service_1 = require("./mobst.service");
var angular_l10n_1 = require("angular-l10n");
var MObstComponent = /** @class */ (function () {
    function MObstComponent(injector, fb, locationService, mobstService, dataService, translation) {
        this.injector = injector;
        this.fb = fb;
        this.locationService = locationService;
        this.mobstService = mobstService;
        this.dataService = dataService;
        this.translation = translation;
        this.action = "";
        this.token = null;
        this.isReadOnly = false;
        this.obstacleTypes = [];
        this.isLine = false;
        this.isOther = false;
        this.units = "(NM)";
        this.options = { onlySelf: true, emitEvent: false };
        this.upperLimitNM = 999;
        this.upperLimitFT = 6076;
        this.maxElevation = 99999;
        this.maxHeight = 9999;
        this.token = this.injector.get('token');
        this.isReadOnly = this.injector.get('isReadOnly');
        this.nsdForm = this.injector.get('form');
        this.action = this.injector.get('type');
    }
    MObstComponent.prototype.ngOnInit = function () {
        this.isFormLoadingData = !(!this.token.subjectId);
        var app = this.mobstService.app;
        this.leafletmap = app.leafletmap;
        if (this.token.radius === 0) {
            if (this.token.radiusUnit === 0)
                this.token.radius = 5;
            else
                this.token.radius = 1;
        }
        this.units = this.token.radiusUnit === 0 ? '(NM)' : '(FT)';
        this.isLine = (this.token.areaType === 1);
        this.isOther = (this.token.obstacleType === 'Other');
        this.createLightOptions();
        this.createPaintOptions();
        this.createBallMarksOptions();
        var frmArr = this.nsdForm.controls['frmArr'];
        if (frmArr.length === 0) {
            this.configureReactiveForm();
        }
        else {
            /*
            this is required because of the grouping funtionallity. When the NsdFormGroup
            was configured and any of items in the group has changed (new set of tokens values)
            the NsdForm Group needs to be updated with the new tokens values */
            this.updateReactiveForm(frmArr);
        }
        this.loadMObstTypes();
        this.showDoaMapRegion(app.doaId);
    };
    MObstComponent.prototype.ngOnDestroy = function () {
        //if (this.leafletmap) {
        //    this.leafletmap.dispose();
        //    this.leafletmap = null;
        //}
    };
    Object.defineProperty(MObstComponent.prototype, "isBilingualRegion", {
        get: function () {
            return this.token && this.token && this.token.isBilingual;
        },
        enumerable: false,
        configurable: true
    });
    MObstComponent.prototype.$ctrl = function (name) {
        return this.nsdFrmGroup.get(name);
    };
    MObstComponent.prototype.createLightOptions = function () {
        this.lightValues = [
            { id: '0', text: this.translation.translate('Nsd.LightValue0') },
            { id: '1', text: this.translation.translate('Nsd.LightValue1') },
            { id: '2', text: this.translation.translate('Nsd.LightValue2') }
        ];
    };
    MObstComponent.prototype.createPaintOptions = function () {
        this.paintValues = [
            { id: '0', text: this.translation.translate('Nsd.PaintValue0') },
            { id: '1', text: this.translation.translate('Nsd.PaintValue1') },
            { id: '2', text: this.translation.translate('Nsd.PaintValue2') }
        ];
    };
    MObstComponent.prototype.createBallMarksOptions = function () {
        this.ballMarksValues = [
            { id: '0', text: this.translation.translate('Nsd.BallMarksValue0') },
            { id: '1', text: this.translation.translate('Nsd.BallMarksValue1') },
            { id: '2', text: this.translation.translate('Nsd.BallMarksValue2') }
        ];
    };
    MObstComponent.prototype.objectTypeSelectOptions = function () {
        return {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.ObjectTypePlaceholder')
            }
        };
    };
    MObstComponent.prototype.loadMObstTypes = function () {
        var _this = this;
        this.mobstService
            .getMObstTypes()
            .subscribe(function (mobstTypes) {
            var obstacles = mobstTypes.map(function (item) {
                return {
                    id: item.id,
                    text: item.name
                };
            });
            // make the 'token.obstacleType' value the first on the list for "optionless convenience" or angular will throw an exception!
            _this.makeFirstElement(obstacles, function (obst) { return obst.id === _this.token.obstacleType; }, true);
            _this.obstacleTypes = obstacles;
        });
    };
    MObstComponent.prototype.setObjectType = function ($event) {
        var mobstTypeCtrl = this.$ctrl('obstacleType');
        var newValue = $event.value;
        if (newValue !== "-1") {
            this.isOther = (newValue === 'Other');
            if (newValue !== mobstTypeCtrl.value) {
                mobstTypeCtrl.setValue(newValue);
                mobstTypeCtrl.markAsDirty();
            }
            this.refreshMap();
        }
    };
    MObstComponent.prototype.setLightType = function ($event) {
        var ctrl = this.$ctrl('lighted');
        var newValue = $event.value;
        if (newValue !== "-1") {
            if (newValue !== ctrl.value) {
                ctrl.setValue(newValue);
                ctrl.markAsDirty();
            }
        }
    };
    MObstComponent.prototype.setPaintType = function ($event) {
        var ctrl = this.$ctrl('painted');
        var newValue = $event.value;
        if (newValue !== "-1") {
            if (newValue !== ctrl.value) {
                ctrl.setValue(newValue);
                ctrl.markAsDirty();
            }
        }
    };
    MObstComponent.prototype.setBallmarksType = function ($event) {
        var ctrl = this.$ctrl('ballmarks');
        var newValue = $event.value;
        if (newValue !== "-1") {
            if (newValue !== ctrl.value) {
                ctrl.setValue(newValue);
                ctrl.markAsDirty();
            }
        }
    };
    MObstComponent.prototype.clearLocations = function () {
        var loc = this.$ctrl('location');
        var rad = this.$ctrl('radius');
        var uni = this.$ctrl('radiusUnit');
        var locFrom = this.$ctrl('fromPoint');
        var locTo = this.$ctrl('toPoint');
        loc.patchValue('', this.options);
        rad.patchValue(5, this.options);
        uni.patchValue(false, this.options);
        locFrom.patchValue('', this.options);
        locTo.patchValue('', this.options);
        loc.setErrors(null);
        rad.setErrors(null);
        uni.setErrors(null);
        locFrom.setErrors(null);
        locTo.setErrors(null);
    };
    MObstComponent.prototype.changeMode = function () {
        if (this.isFormLoadingData)
            return;
        var fc = this.$ctrl('areaType');
        this.clearLocations();
        this.isLine = fc.value;
    };
    MObstComponent.prototype.changeUnits = function () {
        var fc = this.$ctrl('radiusUnit');
        var rad = this.$ctrl('radius');
        if (fc.value === false) {
            this.units = "(NM)";
            rad.patchValue(5, this.options);
        }
        else {
            this.units = "(FT)";
            rad.patchValue(50, this.options);
        }
        rad.setErrors(null);
    };
    MObstComponent.prototype.decRadiusValue = function () {
        this.changeRadiusValue(-1);
    };
    MObstComponent.prototype.incRadiusValue = function () {
        this.changeRadiusValue(1);
    };
    MObstComponent.prototype.changeRadiusValue = function (delta) {
        var fc = this.$ctrl('radius');
        var newValue = this.incIntPart(+fc.value, delta);
        if (this.validateRadiusByUnits(newValue)) {
            fc.patchValue(+newValue);
            this.triggerFormChangeEvent(newValue + Math.random() * 1000);
        }
    };
    MObstComponent.prototype.incIntPart = function (value, delta) {
        var strValue = value.toString();
        if (!strValue)
            return delta;
        var parts = strValue.split('.');
        parts[0] = (+parts[0] + delta).toString();
        strValue = parts.length > 1 ? parts[0] + "." + parts[1] : parts[0];
        return +strValue;
    };
    MObstComponent.prototype.validateRadiusKeyPressNumber = function (evt) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key) && key !== '.') {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            if (this.units === "(FT)" && key === '.') {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
        }
    };
    MObstComponent.prototype.decHeightValue = function () {
        var radCtrl = this.$ctrl('height');
        if (+radCtrl.value > 0) {
            radCtrl.setValue(+radCtrl.value - 1);
            radCtrl.markAsDirty();
        }
    };
    MObstComponent.prototype.incHeightValue = function () {
        var radCtrl = this.$ctrl('height');
        if (+radCtrl.value < this.maxHeight) {
            radCtrl.setValue(+radCtrl.value + 1);
            radCtrl.markAsDirty();
        }
    };
    MObstComponent.prototype.decElevationValue = function () {
        var radCtrl = this.$ctrl('elevation');
        if (+radCtrl.value > 0) {
            radCtrl.setValue(+radCtrl.value - 1);
            radCtrl.markAsDirty();
        }
    };
    MObstComponent.prototype.incElevationValue = function () {
        var radCtrl = this.$ctrl('elevation');
        if (+radCtrl.value < this.maxElevation) {
            radCtrl.setValue(+radCtrl.value + 1);
            radCtrl.markAsDirty();
        }
    };
    MObstComponent.prototype.validateKeyPressNumber = function (evt) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
        }
    };
    MObstComponent.prototype.radioButtonChanged = function (name) {
        this.$ctrl(name).markAsDirty();
    };
    MObstComponent.prototype.validateLocation = function () {
        if (this.isLine || this.nsdForm.pristine)
            return true;
        var fc = this.$ctrl('location');
        return !fc.errors;
    };
    MObstComponent.prototype.validateObstacleName = function () {
        var fc = this.$ctrl('otherObstacleNameE');
        return !fc.errors || this.nsdForm.pristine;
    };
    MObstComponent.prototype.validateDescriptionEng = function () {
        var fc = this.$ctrl('obstacleDescriptionE');
        return !fc.errors || this.nsdForm.pristine;
    };
    MObstComponent.prototype.validateFromPoint = function () {
        if (!this.isLine || this.nsdForm.pristine)
            return true;
        var fc = this.$ctrl('fromPoint');
        return !fc.errors;
    };
    MObstComponent.prototype.validateToPoint = function () {
        if (!this.isLine || this.nsdForm.pristine)
            return true;
        var fc = this.$ctrl('toPoint');
        return !fc.errors;
    };
    MObstComponent.prototype.invalidMObstType = function () {
        if (this.nsdForm.pristine)
            return false;
        var ctrl = this.$ctrl('obstacleType');
        return !this.isReadOnly && this.$ctrl('obstacleType').invalid;
    };
    MObstComponent.prototype.validateMObstTypeId = function () {
        var fc = this.$ctrl('obstacleType');
        return fc.value !== -1 || this.nsdForm.pristine;
    };
    MObstComponent.prototype.validateRadius = function () {
        if (this.isLine || this.nsdForm.pristine)
            return true;
        var fc = this.$ctrl('radius');
        if (fc.errors)
            return false;
        return this.validateRadiusByUnits(+fc.value);
    };
    MObstComponent.prototype.validateRadiusByUnits = function (radius) {
        return this.units === "(NM)" ? this.validateRadiusNM(radius) : this.validateRadiusFT(radius);
    };
    MObstComponent.prototype.validateRadiusNM = function (radius) {
        return radius >= 1 && radius <= this.upperLimitNM;
    };
    MObstComponent.prototype.validateRadiusFT = function (radius) {
        return radius >= 1 && radius <= this.upperLimitFT;
    };
    MObstComponent.prototype.validateHeight = function () {
        if (this.nsdForm.pristine)
            return true;
        var fc = this.$ctrl('height');
        if (fc.errors)
            return false;
        if (fc.value !== null) {
            if (+fc.value > this.maxHeight)
                return false;
            if (+fc.value < 0)
                return false;
        }
        else
            return false;
        return true;
    };
    MObstComponent.prototype.validateElevation = function () {
        if (this.nsdForm.pristine)
            return true;
        var fc = this.$ctrl('elevation');
        if (fc.errors)
            return false;
        if (fc.value !== null) {
            if (+fc.value > this.maxElevation)
                return false;
            if (+fc.value < 0)
                return false;
        }
        else
            return false;
        return true;
    };
    MObstComponent.prototype.locationChanged = function (value) {
        var _this = this;
        var tmp = this.$ctrl('location').value;
        if (tmp !== null && hasLowerCase(tmp))
            this.$ctrl('location').patchValue(tmp.toUpperCase());
        if (value !== null && hasLowerCase(value))
            value = value.toUpperCase();
        if (this.leafletmap === null)
            this.leafletmap = this.mobstService.app.leafletmap;
        var pair = this.locationService.parse(value);
        var geojson = pair ? this.leafletmap.latitudeLongitudeToGeoJson(pair.latitude, pair.longitude) : null;
        if (geojson) {
            this.mobstService
                .validateLocation(value)
                .subscribe(function (result) {
                var locCtrl = _this.$ctrl('location');
                var currentLocation = locCtrl.value;
                if (currentLocation === value) {
                    if (result.inDoa) {
                        var rad = _this.getRadiusInMeters();
                        _this.leafletmap.addLocationAndRadius(pair.latitude, pair.longitude, +rad);
                        locCtrl.setErrors(null);
                        // ?? this.triggerFormChangeEvent(value);
                    }
                    else {
                        locCtrl.setErrors({ invalidLocation: true });
                    }
                }
            }, function (error) {
                var locCtrl = _this.$ctrl('location');
                locCtrl.setErrors({ invalidLocation: true });
            });
        }
    };
    MObstComponent.prototype.changeRadius = function () {
        var tmp = this.$ctrl('location').value;
        this.locationChanged(tmp);
    };
    MObstComponent.prototype.getRadiusInMeters = function () {
        var rad = this.$ctrl('radius').value;
        var radU = this.$ctrl('radiusUnit').value;
        if (radU)
            rad = this.locationService.feetToMeters(+rad);
        else
            rad = this.locationService.nmToMeters(+rad);
        return +rad;
    };
    MObstComponent.prototype.locationToFromChanged = function (ctrlName, value) {
        var _this = this;
        var fc_from = this.$ctrl('fromPoint').value;
        var fc_to = this.$ctrl('toPoint').value;
        if (fc_from !== null && fc_to !== null) {
            if (this.validateFromPoint() && this.validateToPoint()) {
                this.mobstService.calcCenterLocation(fc_from, fc_to)
                    .subscribe(function (result) {
                    if (result.valid) {
                        _this.$ctrl('location').patchValue(result.center, _this.options);
                        _this.$ctrl('radius').patchValue(result.radius, _this.options);
                        _this.$ctrl('radiusUnit').patchValue(false, _this.options);
                        if (_this.leafletmap === null)
                            _this.leafletmap = _this.mobstService.app.leafletmap;
                        var pair_ini = _this.locationService.parse(fc_from);
                        var pair_end = _this.locationService.parse(fc_to);
                        _this.leafletmap.addPointToPointLine(pair_ini.latitude, pair_ini.longitude, pair_end.latitude, pair_end.longitude);
                        var locCtrl = _this.$ctrl(ctrlName);
                        locCtrl.setErrors(null);
                        _this.triggerFormChangeEvent(value);
                    }
                    else {
                        var locCtrl = _this.$ctrl(ctrlName);
                        locCtrl.setErrors({ invalidLocation: true });
                    }
                }, function (error) {
                    var locCtrl = _this.$ctrl(ctrlName);
                    locCtrl.setErrors({ invalidLocation: true });
                });
            }
        }
    };
    MObstComponent.prototype.otherObstacleNameChange = function (value) {
        var fc_eng = this.$ctrl('otherObstacleNameE');
        var fc_fr = this.$ctrl('otherObstacleNameF');
        if (fc_fr.value && !fc_eng.value) {
            fc_eng.setErrors({ required: true });
        }
        else {
            fc_eng.setErrors(null);
            this.triggerFormChangeEvent(value);
        }
    };
    MObstComponent.prototype.obstacleDescriptionChange = function (value) {
        var fc_eng = this.$ctrl('obstacleDescriptionE');
        var fc_fr = this.$ctrl('obstacleDescriptionF');
        if (fc_fr.value && !fc_eng.value) {
            fc_eng.setErrors({ required: true });
        }
        else {
            fc_eng.setErrors(null);
            this.triggerFormChangeEvent(value);
        }
    };
    MObstComponent.prototype.triggerFormChangeEvent = function (value) {
        this.$ctrl('refreshDummy').setValue(value);
        this.$ctrl('refreshDummy').markAsDirty();
    };
    MObstComponent.prototype.getSelectedObstacleName = function () {
        var obstId = this.$ctrl('obstacleType').value;
        if (!obstId || !this.obstacleTypes)
            return "MOBST";
        var obstacle = this.obstacleTypes.find(function (obst) { return obst.id === obstId; });
        return obstacle ? obstacle.id : "MOBST";
    };
    MObstComponent.prototype.configureReactiveForm = function () {
        var _this = this;
        var frmArrControls = this.nsdForm.controls['frmArr'];
        this.nsdFrmGroup = this.fb.group({
            obstacleType: [{ value: this.token.obstacleType || -1, disabled: this.isReadOnly }, [mobstTypeValidator]],
            otherObstacleNameE: [{ value: this.token.otherObstacleNameE, disabled: this.isReadOnly }, []],
            otherObstacleNameF: [{ value: this.token.otherObstacleNameF, disabled: this.isReadOnly }, []],
            areaType: [{ value: this.token.areaType, disabled: this.isReadOnly }, []],
            location: [{ value: this.token.location, disabled: this.isReadOnly }, [validatePointLocation(this)]],
            radius: [{ value: this.token.radius, disabled: this.isReadOnly }, [radiusValidator(this)]],
            radiusUnit: [{ value: this.token.radiusUnit, disabled: this.isReadOnly }, []],
            fromPoint: [{ value: this.token.fromPoint, disabled: this.isReadOnly }, [validateLinePoint(this)]],
            toPoint: [{ value: this.token.toPoint, disabled: this.isReadOnly }, [validateLinePoint(this)]],
            obstacleDescriptionE: [{ value: this.token.obstacleDescriptionE, disabled: this.isReadOnly }, []],
            obstacleDescriptionF: [{ value: this.token.obstacleDescriptionF, disabled: this.isReadOnly }, []],
            elevation: [{ value: this.token.elevation, disabled: this.isReadOnly }, []],
            height: [{ value: this.token.height, disabled: this.isReadOnly }, []],
            lighted: [{ value: this.token.lighted || 0, disabled: this.isReadOnly }],
            painted: [{ value: this.token.painted || 0, disabled: this.isReadOnly }],
            ballmarks: [{ value: this.token.ballmarks || 0, disabled: this.isReadOnly }],
            refreshDummy: ""
        });
        this.$ctrl("elevation").setValidators(elevationValidatorFn(this.maxElevation));
        this.$ctrl("height").setValidators(elevationValidatorFn(this.maxHeight));
        this.$ctrl("areaType").valueChanges.debounceTime(1000).subscribe(function () { return _this.changeMode(); });
        this.$ctrl("lighted").valueChanges.subscribe(function (value) { return _this.radioButtonChanged("lighted"); });
        this.$ctrl("painted").valueChanges.subscribe(function (value) { return _this.radioButtonChanged("painted"); });
        this.$ctrl("ballmarks").valueChanges.subscribe(function (value) { return _this.radioButtonChanged("ballmarks"); });
        this.$ctrl("location").valueChanges.debounceTime(1000).subscribe(function (value) { return _this.locationChanged(value); });
        this.$ctrl("radius").valueChanges.debounceTime(1000).subscribe(function () { return _this.changeRadius(); });
        this.$ctrl("fromPoint").valueChanges.debounceTime(1000).subscribe(function (value) { return _this.locationToFromChanged('fromPoint', value); });
        this.$ctrl("toPoint").valueChanges.debounceTime(1000).subscribe(function (value) { return _this.locationToFromChanged('toPoint', value); });
        this.$ctrl("otherObstacleNameE").valueChanges.debounceTime(1000).subscribe(function (value) { return _this.otherObstacleNameChange(value); });
        this.$ctrl("otherObstacleNameF").valueChanges.debounceTime(1000).subscribe(function (value) { return _this.otherObstacleNameChange(value); });
        this.$ctrl("obstacleDescriptionE").valueChanges.debounceTime(1000).subscribe(function (value) { return _this.obstacleDescriptionChange(value); });
        this.$ctrl("obstacleDescriptionF").valueChanges.debounceTime(1000).subscribe(function (value) { return _this.obstacleDescriptionChange(value); });
        frmArrControls.push(this.nsdFrmGroup);
    };
    MObstComponent.prototype.updateReactiveForm = function (formArr) {
        this.nsdFrmGroup = formArr.controls[0];
        //It is better to update only those controls that tokens values have changed.
        this.updateFormControlValue("obstacleType", this.token.obstacleType);
        this.updateFormControlValue("otherObstacleNameE", this.token.otherObstacleNameE);
        this.updateFormControlValue("otherObstacleNameF", this.token.otherObstacleNameF);
        this.updateFormControlValue("areaType", this.token.areaType);
        this.updateFormControlValue("location", this.token.location);
        this.updateFormControlValue("radius", this.token.radius);
        this.updateFormControlValue("radiusUnit", this.token.radiusUnit);
        this.updateFormControlValue("fromPoint", this.token.fromPoint);
        this.updateFormControlValue("toPoint", this.token.toPoint);
        this.updateFormControlValue("obstacleDescriptionE", this.token.obstacleDescriptionE);
        this.updateFormControlValue("obstacleDescriptionF", this.token.obstacleDescriptionF);
        this.updateFormControlValue("elevation", this.token.elevation);
        this.updateFormControlValue("height", this.token.height);
        this.updateFormControlValue("lighted", this.token.lighted);
        this.updateFormControlValue("painted", this.token.painted);
        this.updateFormControlValue("ballmarks", this.token.ballmarks);
    };
    MObstComponent.prototype.updateFormControlValue = function (controlName, value) {
        var control = this.$ctrl(controlName);
        if (control && control.value !== value) {
            control.setValue(value);
        }
    };
    MObstComponent.prototype.makeFirstElement = function (arr, makeFirst, insertUndefinedAtZero) {
        if (insertUndefinedAtZero === void 0) { insertUndefinedAtZero = true; }
        var len = arr.length;
        for (var i = 0; i < len; i++) {
            if (makeFirst(arr[i])) {
                this.swapArrayPositions(arr, 0, i);
                return;
            }
        }
        if (insertUndefinedAtZero) {
            arr.unshift({
                id: -1,
                text: '',
            });
        }
    };
    MObstComponent.prototype.swapArrayPositions = function (arr, p1, p2) {
        var temp = arr[p1];
        arr[p1] = arr[p2];
        arr[p2] = temp;
    };
    MObstComponent.prototype.showDoaMapRegion = function (doaId) {
        var _this = this;
        this.dataService
            .getDoaLocation(doaId)
            .subscribe(function (geoJsonStr) { return _this.renderMap(geoJsonStr); });
    };
    MObstComponent.prototype.refreshMap = function () {
        var _this = this;
        if (this.isLine) {
            window.setTimeout(function () { return _this.locationToFromChanged('toPoint', _this.$ctrl('toPoint').value); }, 1000);
        }
        else {
            window.setTimeout(function () { return _this.locationChanged(_this.$ctrl('location').value); }, 1000);
        }
    };
    MObstComponent.prototype.renderMap = function (geoJsonStr) {
        var self = this;
        var interval = window.setInterval(function () {
            if (self.leafletmap && self.leafletmap.isReady()) {
                window.clearInterval(interval);
                self.leafletmap.addDoa(geoJsonStr, 'DOA');
            }
        }, 100);
    };
    MObstComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/dashboard/nsds/mobst/mobst.component.html'
        }),
        __metadata("design:paramtypes", [core_1.Injector,
            forms_1.FormBuilder,
            location_service_1.LocationService,
            mobst_service_1.MObstService,
            data_service_1.DataService,
            angular_l10n_1.TranslationService])
    ], MObstComponent);
    return MObstComponent;
}());
exports.MObstComponent = MObstComponent;
function mobstTypeValidator(c) {
    if (c.value === -1) {
        return { 'required': true };
    }
    return null;
}
function validatePointLocation(controller) {
    return function (ctrl) {
        var location = ctrl.value;
        var isLineValue = controller.isLine;
        if (!isLineValue) {
            if (!location)
                return { required: true };
            if (controller.locationService.parse(location) === null)
                return { format: true };
        }
        return null;
    };
}
function validateLinePoint(controller) {
    return function (ctrl) {
        var location = ctrl.value;
        var isLineValue = controller.isLine;
        if (isLineValue) {
            if (!location)
                return { required: true };
            if (controller.locationService.parse(location) === null)
                return { format: true };
        }
        return null;
    };
}
function elevationValidatorFn(maxElevation) {
    return function (c) {
        if (typeof (c.value) === "string") {
            if (!c.value) {
                return { 'required': true };
            }
            if (c.value === "") {
                return { 'required': true };
            }
        }
        if (isANumber(c.value)) {
            if (+c.value < 0 || +c.value > maxElevation) {
                return { 'invalid': true };
            }
            var v = +c.value;
            if (v > maxElevation) {
                return { 'invalid': true };
            }
            return null;
        }
        return { 'invalid': true };
    };
}
function radiusValidator(controller) {
    return function (c) {
        if (controller.isLine)
            return null;
        if (!c.value)
            return { 'required': true };
        if (controller.validateRadiusByUnits(+c.value))
            return null;
        return { 'invalid': true };
    };
}
function isANumber(str) {
    return !/\D/.test(str);
}
function hasLowerCase(str) {
    return (/[a-z]/.test(str));
}
//# sourceMappingURL=mobst.component.js.map
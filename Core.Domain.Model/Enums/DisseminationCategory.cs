﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum DisseminationCategory
    {
        National = 0,
        US = 1,
        International = 2
    }
}

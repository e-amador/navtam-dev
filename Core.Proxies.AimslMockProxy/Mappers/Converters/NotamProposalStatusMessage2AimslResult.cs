﻿namespace NavCanada.Core.Proxies.AimslMockProxy.Mappers.Converters
{
    using System;

    using AutoMapper;

    using NavCanada.Core.Common.Responses;
    using NavCanada.Core.Domain.Model.Entitities;
    using NavCanada.Core.Domain.Model.Enums;

    public class NotamProposalStatusMessage2AismlResponse : ITypeConverter<Proxy.NotamProposalStatusMessage, AismlResponse>
    {
        public AismlResponse Convert(ResolutionContext context)
        {
            Proxy.NotamProposalStatusMessage source = context.SourceValue as Proxy.NotamProposalStatusMessage;

            var result = new AismlResponse
            {
                Code = ApiStatusCode.Success,
                ProposalStatus = new NotamProposalStatus
                {
                    Status = Mapper.Map<Proxy.NotamProposalStatusType, NotamProposalStatusCode>(source.Status),
                    RejectionReason = source.RejectionReason,
                    StatusDateTime = DateTime.Now,
                    ProposalId = source.NotamProposalId.ToString(),
                    InoNotamProposalId = source.NotamProposalId,
                    SubmittedNotam = Mapper.Map <Proxy.Notam, Notam>(source.SubmittedNotam)
                }
            };

            return result;
        }
    }
}

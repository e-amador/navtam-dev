﻿namespace NavCanada.Core.Proxies.AimslMockProxy
{
    using System;

    using AutoMapper;

    using log4net;

    using Common.Contracts;
    using Common.Responses;
    using Domain.Model.Enums;
    using Mappers;
    using Proxy;

    public class AimslProxyMock : IAimslProxy
    {
        readonly ILog _logger = LogManager.GetLogger(typeof(AimslProxyMock));

        public AimslProxyMock()
        {
            AutoMapperConfig.RegisterMaps();    
        }

        public AismlResponse Submit(Core.Domain.Model.Entitities.NotamProposal proposal, string accountableSource)
        {
            try
            {
                //TODO: Update AimslMock (Proxy.NotamProposal) to include the accountableSource parameter to match the real AimlsService.
                //AccountableSource will be ignored at this moment
                var cli = new AIMSLSoapClient();
                NotamProposalStatusMessage msg = cli.SubmitNotamProposal(Mapper.Map<Core.Domain.Model.Entitities.NotamProposal, NotamProposal>(proposal));

                return Mapper.Map<NotamProposalStatusMessage, AismlResponse>(msg);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return new AismlResponse
                {
                    Code = ApiStatusCode.ConnectionError,
                    ErrorMessage = e.Message
                };
            }
        }


        public AismlResponse CheckStatus(long notamProposalId)
        {
            try
            {
                var cli = new AIMSLSoapClient();
                NotamProposalStatusMessage msg = cli.GetNotamProposalStatus(notamProposalId);

                return Mapper.Map<NotamProposalStatusMessage, AismlResponse>(msg);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return new AismlResponse
                {
                    Code = ApiStatusCode.ConnectionError,
                    ErrorMessage = e.Message
                };
            }
        }

        public AismlResponse Widthraw(long notamProposalId)
        {
            try
            {
                var cli = new AIMSLSoapClient();
                NotamProposalStatusMessage msg = cli.WithdrawNotamProposal(notamProposalId);

                return Mapper.Map<NotamProposalStatusMessage, AismlResponse>(msg);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return new AismlResponse
                {
                    Code = ApiStatusCode.ConnectionError,
                    ErrorMessage = e.Message
                };
            }
        }
    }
}

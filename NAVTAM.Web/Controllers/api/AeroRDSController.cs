﻿using NavCanada.Core.Common.Airac;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NavCanada.Core.TaskEngine.Mto;
using NAVTAM.Helpers;
using NAVTAM.ViewModels;
using System;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [RoutePrefix("api/sdo")]
    [Authorize(Roles = "Administrator")]
    public class AeroRDSController : ApiBaseController
    {
        readonly INotamQueuePublisher _queuePublisher;

        public AeroRDSController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, INotamQueuePublisher queuePublisher, ILogger logger) : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
            _queuePublisher = queuePublisher;
        }

        [HttpGet]
        [Route("status")]
        public IHttpActionResult GetStatus()
        {
            return LogAction(() => 
            {
                var airacDate = AIRACUtils.GetActiveAIRACCycleDate(DateTime.Now);
                var status = Uow.AeroRDSCacheStatusRepo.GetStatus();

                var mapped = new AeroRDSCacheStatusViewModel()
                {
                    AiracActiveDate = FormatDate(airacDate),
                    AiracEffectiveDate = FormatDate(airacDate.AddDays(28)),
                    LastActiveDate = FormatDate(status?.LastActiveDate),
                    LastActiveUpdated = FormatDate(status?.LastActiveUpdated),
                    LastEffectiveDate = FormatDate(status?.LastEffectiveDate),
                    LastEffectiveUpdated = FormatDate(status?.LastEffectiveUpdated),
                };

                return Ok(mapped);

            }, "api/sdo/status");
        }

        [HttpPost]
        [Route("globalsync")]
        public IHttpActionResult TriggerGlobalSyncAse()
        {
            return LogAction(() =>
            {
                var nextDate = Uow.SdoCacheRepo.GetLatestEffectiveDate();
                var nextDateStr = nextDate.HasValue ? GetNextAiracCycle(nextDate.Value).ToShortDateString() : "GlobalSync";

                PublishMessageQueue(MessageTransferObjectId.AeroRDSSyncActiveDataMsg, nextDateStr);

                Logger.LogInfo(GetType(), $"AeroRDS Global Sync ('{nextDateStr}') triggered by '{ApplicationContext.GetUserName()}'.");

                return Ok("Done!");

            }, "api/sdo/globalsync");
        }

        [HttpPost]
        [Route("effectivecachesync")]
        public IHttpActionResult TriggerEffectiveDateSync()
        {
            return LogAction(() =>
            {
                PublishMessageQueue(MessageTransferObjectId.AeroRDSCacheEffectiveDataMsg);

                Logger.LogInfo(GetType(), $"AeroRDS Cache Sync triggered by '{ApplicationContext.GetUserName()}'.");

                return Ok("Done!");

            }, "api/sdo/effectivecachesync");
        }

        [HttpPost]
        [Route("effectivecacheload")]
        public IHttpActionResult TriggerActiveSyncAse()
        {
            return LogAction(() =>
            {
                var nowUtc = DateTime.Now;
                var nowBareDate = new DateTime(nowUtc.Year, nowUtc.Month, nowUtc.Day);
                var airacDate = AIRACUtils.GetActiveAIRACCycleDate(nowBareDate);

                var status = Uow.AeroRDSCacheStatusRepo.GetStatus();

                if (status != null && status.LastEffectiveDate != airacDate)
                    return BadRequest(Resources.TooEarlyToLoadSDOCache);

                PublishMessageQueue(MessageTransferObjectId.AeroRDSLoadCachedDataMsg);

                Logger.LogInfo(GetType(), $"AeroRDS Load Cache Data triggered by '{ApplicationContext.GetUserName()}'.");

                return Ok("Done!");

            }, "api/sdo/effectivecacheload");
        }

        private void PublishMessageQueue(MessageTransferObjectId messageId, string nextDate = null)
        {
            var mto = MtoHelper.CreateMessage((int)messageId, m => 
            {
                m.Add(MessageDefs.ManuallyScheduledTaskKey, true, DataType.Boolean);
                if (nextDate != null)
                {
                    m.Add(MessageDefs.NextAiracCycleDate, nextDate, DataType.String);
                }
            });
            _queuePublisher.Publish(Uow, mto);
        }

        static string FormatDate(DateTime? date) => date.HasValue ? date.Value.ToString("yyyy-MM-dd") : "?";

        static DateTime GetNextAiracCycle(DateTime date) => AIRACUtils.GetActiveAIRACCycleDate(date).AddDays(28);
    }
}
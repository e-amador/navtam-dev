﻿using AutoMapper;
using NavCanada.SE.NOTAMWiz.Models;
using System;
using System.Linq;
using NavCanada.SE.NotamWiz.UI.Wiz.Web.Models;

namespace NavCanada.SE.NOTAMWiz.App_Start.AutoMapperResolvers
{
    public class NSDVersion2VMConvertor : ITypeConverter<NSDVersion, NSDVersionVM>
    {
        public NSDVersionVM Convert(ResolutionContext context)
        {
            //DBContext db = (DBContext)context.Options.Items["db"];

            NSDVersion ver = context.SourceValue as NSDVersion;

            NSDVersionVM vm = new NSDVersionVM()
            {
                Description = ver.Description,
                Id = ver.Id,
                NsdId = ver.NsdId,
                ReleaseDate = ver.ReleaseDate,
                ReleasedBy = ver.ReleasedBy,
                SubjectsFilter = ver.SubjectsFilter,
                SupportedSubjects = ver.SupportedSubjects,
                Version = ver.Version,
                HasView = ver.HasView
            };

            return vm;


        }
    }

    public class VM2NSDVersionConvertor : ITypeConverter<NSDVersionVM, NSDVersion>
    {
        public NSDVersion Convert(ResolutionContext context)
        {
            NOTAMWizDBContext db = (NOTAMWizDBContext)context.Options.Items["db"];

            NSDVersionVM vm = context.SourceValue as NSDVersionVM;

            NSDVersion ver = new NSDVersion()
            {
                Description = vm.Description,
                
                Id = vm.Id,
                NsdId = vm.NsdId,
                ReleaseDate = vm.ReleaseDate,
                ReleasedBy = vm.ReleasedBy,
                SubjectsFilter = vm.SubjectsFilter,
                SupportedSubjects = vm.SupportedSubjects,
                Version = vm.Version,
                HasView = vm.HasView
            };

            return ver;

        }
    }
}
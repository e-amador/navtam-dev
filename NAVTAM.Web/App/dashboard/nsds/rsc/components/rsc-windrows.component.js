"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WindrowsComponent = void 0;
var core_1 = require("@angular/core");
var angular_l10n_1 = require("angular-l10n");
var forms_1 = require("@angular/forms");
var rsc_service_1 = require("../rsc.service");
var toastr_service_1 = require("../../../../common/toastr.service");
var WindrowsComponent = /** @class */ (function () {
    function WindrowsComponent(fb, translation, rscService, toastr) {
        this.fb = fb;
        this.translation = translation;
        this.rscService = rscService;
        this.toastr = toastr;
        this.mainForm = null;
        this.isReadOnly = false;
        this.model = null;
    }
    WindrowsComponent.prototype.ngOnInit = function () {
        this.isFormLoadingData = false;
        this.configureReactiveForm();
    };
    WindrowsComponent.prototype.ngOnChanges = function () {
    };
    WindrowsComponent.prototype.ngOnDestroy = function () {
    };
    WindrowsComponent.prototype.$ctrl = function (name) {
        return this.frmGroup.get(name);
    };
    WindrowsComponent.prototype.configureReactiveForm = function () {
        var frmArrControls = this.mainForm.controls['frmWindrowsArr'];
        this.frmGroup = this.fb.group({
            windrowsRemarks: [{ value: this.model.windrowsRemarks, disabled: this.isReadOnly }, []],
        });
        frmArrControls.push(this.frmGroup);
    };
    //Export functions
    WindrowsComponent.prototype.isFormValid = function () {
        return true;
    };
    WindrowsComponent.prototype.exportComponentInternalData = function () {
        var data = {
            parent: this.model.parent,
            windrowsRemarks: this.model.windrowsRemarks,
        };
        return data;
    };
    WindrowsComponent.prototype.clearValues = function () {
        alert('Clear component values: Windrows');
    };
    __decorate([
        core_1.Input('form'),
        __metadata("design:type", forms_1.FormGroup)
    ], WindrowsComponent.prototype, "mainForm", void 0);
    __decorate([
        core_1.Input('isReadOnly'),
        __metadata("design:type", Boolean)
    ], WindrowsComponent.prototype, "isReadOnly", void 0);
    __decorate([
        core_1.Input('model'),
        __metadata("design:type", Object)
    ], WindrowsComponent.prototype, "model", void 0);
    WindrowsComponent = __decorate([
        core_1.Component({
            selector: 'rsc-windrows',
            templateUrl: './app/dashboard/nsds/rsc/components/rsc-windrows.component.html'
        }),
        __param(3, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [forms_1.FormBuilder,
            angular_l10n_1.TranslationService,
            rsc_service_1.RscService, Object])
    ], WindrowsComponent);
    return WindrowsComponent;
}());
exports.WindrowsComponent = WindrowsComponent;
//# sourceMappingURL=rsc-windrows.component.js.map
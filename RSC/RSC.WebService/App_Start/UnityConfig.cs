﻿using Microsoft.Practices.Unity;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Data.NotamWiz.EF.Helpers;
using System.Data.Entity;

namespace RSC.WebService
{
    public static class UnityConfig
    {
        public static void RegisterComponents(IUnityContainer container)
        {
            container.RegisterType<DbContext, AppDbContext>(new InjectionConstructor("DefaultConnection"));
            
            // DBContext
            container.RegisterType<IRSCServiceContext, RSCServiceContext>(new TransientLifetimeManager());

            // Repository Factories
            container.RegisterInstance(new RepositoryFactories(), new ContainerControlledLifetimeManager());

            // Repository Provider
            container.RegisterInstance(new RepositoryProvider(container.Resolve<RepositoryFactories>()));
            container.RegisterType<IRepositoryProvider, RepositoryProvider>(new TransientLifetimeManager());

            // UOW
            container.RegisterType<IUow, Uow>(new TransientLifetimeManager());
        }
    }
}
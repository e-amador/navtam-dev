﻿namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Domain.Model.Entitities;
    using Domain.Model.Dtos;
    using Common.Common;

    public interface IUserProfileRepo : IRepository<UserProfile>
    {
        UserProfile GetById(string userId);

        Task<UserProfile> GetByIdAsync(string userId, bool shouldIncludeRegistration);

        UserProfile GetByUserName(string userName);
        Task<UserProfile> GetByUserNameAsync(string userName);

        Task<int> GetUserCountInOrganization(int orgId, bool includeDisabled, bool includeDeleted);
        Task<int> GetUserCountInOrganization(int orgId, bool includeDisabled, bool includeDeleted, QueryOptions queryOptions);

        Task<List<UserProfile>> GetUsersInOrganization(int orgId, bool includeDisabled, bool includeDeleted);

        Task<List<UserProfile>> GetUnapprovedAsync();

        Task<List<UserProfile>> GetUnconfirmedEmailUsersAsync();

        Task<List<UserProfile>> GetNonDeletedAndFullyRegisteredUsersAsync();

        List<UserProfile> GetUsersbyRole(string roleId);

        Task<List<UserRoleDto>> GetRolesAsync();

        Task<int> GetAllCountAsync(bool includeDisabled, bool includeDeleted, QueryOptions queryOptions);

        Task<List<UserDto>> GetAllAsync(QueryOptions queryOptions, int? orgId, bool includeDisabled, bool includeDeleted);

        Task<UserDto> GetUserInfoAsync(string userName);
    }
}
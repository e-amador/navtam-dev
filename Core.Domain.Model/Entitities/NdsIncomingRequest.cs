﻿using System;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class NdsIncomingRequest
    {
        public Guid Id { get; set; }
        public string MessageType { get; set; }
        public string MessageId { get; set; }
        public string Metadata { get; set; }
        public string Body { get; set; }
        public DateTime RecievedDate { get; set; }
        public DateTime? ProcessedDate { get; set; }
    }
}

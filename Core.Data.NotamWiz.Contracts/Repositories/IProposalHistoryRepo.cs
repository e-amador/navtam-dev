﻿namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    using Common.Common;
    using Domain.Model.Entitities;
    using Domain.Model.Enums;
    using NavCanada.Core.Domain.Model.Dtos;
    using System.Collections.Generic;
    using System.Data.Entity.Spatial;
    using System.Linq;
    using System.Threading.Tasks;

    public interface IProposalHistoryRepo : IRepository<ProposalHistory>
    {
        Task<ProposalHistory> GetByIdAsync(int id);
        Task<ProposalHistory> GetLatestByProposalIdAsync(int proposalId);
        Task<ProposalHistory> GetByProposalIdAndNotamIdAsync(string notamId, int proposalId);
        Task<List<ProposalDto>> GetByProposalIdPartialAsync(int proposalId);
        Task<List<ProposalHistory>> GetByProposalIdAsync(int proposalId);
        Task<ProposalHistory> GetModifyByUserAsync(int proposalId);
        Task<ProposalHistory> GetLastSubmittedProposalAsync(int proposalId);
        Task<ProposalHistory> GetLastDisseminatedProposalAsync(int proposalId);
        Task<ProposalHistory> GetLastParkedProposalAsync(int proposalId);
        Task<List<ProposalHistory>> GetByProposalIdDescAsync(int proposalId);
        Task<int> FilterCountAsync(FilterCriteria filterCriteria, int orgId, OrganizationType orgType, DbGeography region, IQueryable<ProposalHistory> filterQuery);
        Task<List<ProposalDto>> FilterAsync(FilterCriteria filterCriteria, int orgId, OrganizationType orgType, DbGeography region, IQueryable<ProposalHistory> filterQuery);
        Task<ProposalHistory> GetLatestByStatusProposalIdAsync(int proposalId, NotamProposalStatusCode status);
        IQueryable<ProposalHistory> PrepareQueryFilter(FilterCriteria filterCriteria);
        Task<int> GetCountBySeries(string series);
    }
}

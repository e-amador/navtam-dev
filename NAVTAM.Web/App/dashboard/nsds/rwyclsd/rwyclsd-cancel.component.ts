import { Component, OnInit, OnDestroy, Inject, Injector, ChangeDetectorRef } from '@angular/core'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators } from '@angular/forms'

import { LocationService } from '../../shared/location.service';
import { ISdoSubject, ISelectOptions } from '../../shared/data.model';
import { DataService } from '../../shared/data.service';
import { RwyClsdService, IClosureType, IRwy, IAerodrome } from './rwyclsd.service';

import { TOASTR_TOKEN, Toastr } from './../../../common/toastr.service';


@Component({
    templateUrl: '/app/dashboard/nsds/rwyclsd/rwyclsd-cancel.component.html'
})

export class RwyClsdCancelComponent implements OnInit, OnDestroy {
    token: any = null;
    nsdForm: FormGroup;
    nsdFrmGroup: FormGroup;
    isReadOnly: boolean = false;
   
    
    closureReasons: ISelectOptions[] = [];
    closureReasonId: string = '';
    closureQuestion: boolean = false;

    isBilingualRegion: boolean = false;

    subjectSelected: ISdoSubject = null;
    
    subjects: ISdoSubject[];
    rwySelections: ISdoSubject[];       

    leafletmap: any;

    disableIcaoSubjectSelect: boolean = true;
    disableIcaoConditionSelect: boolean = true;

    isFormLoadingData: boolean;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private injector: Injector,
        private fb: FormBuilder,
        private locationService: LocationService,
        private closureService: RwyClsdService,
        private changeDetectionRef: ChangeDetectorRef,
        private dataService: DataService) {

        this.token = this.injector.get('token');
        this.isReadOnly = this.injector.get('isReadOnly');
        this.nsdForm = this.injector.get('form');
    }

    ngOnInit() {
        this.isFormLoadingData = !(!this.token.subjectId);
        const app = this.closureService.app;
        this.leafletmap = app.leafletmap;
        this.configureReactiveForm();
        //This "dirty trick" is important to make the parent form not
        //to run the generateIcao until everything is loaded.
        this.$ctrl('refreshDummy').setValue(0);
        this.nsdForm.controls['frmArr'].setErrors({ 'required': true });

        this.loadClosureTypes();
        //this.showDoaMapRegion(app.doaId);
        this.setRadioButtonValues();

        

        //itemA
        this.dataService.getSdoSubject(this.token.effectedAerodrome)
            .subscribe((sdoSubjectResult: ISdoSubject) => {
                this.subjectSelected = sdoSubjectResult;
                this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);
                this.subjects = [];
                this.subjects.push(sdoSubjectResult);                

                this.loadMapLocations(app.doaId, true);
                this.updateMap(sdoSubjectResult, true);                               

                //rwy
                this.dataService.getSdoSubject(this.token.effectedRunway)
                    .subscribe((sdoSubjectResult: ISdoSubject) => {
                        this.rwySelections = [];
                        this.rwySelections.push(sdoSubjectResult);

                        this.$ctrl('refreshDummy').setValue(1);
                    });
            });

        
        //this.isFormLoadingData = false;
        //this.triggerFormChangeEvent("refresh");
    }
    
    ngOnDestroy() {
        if (this.leafletmap) {
            this.leafletmap.dispose();
            this.leafletmap = null;
        }
    }

    $ctrl(name: string): AbstractControl {
        return this.nsdFrmGroup.get(name);
    }    

    getEffectedAerodrome(): any {
        const self = this;
        const app = this.closureService.app;

        return {
            minimumInputLength: 2,
            width: "100%",
            placeholder: {
                id: '-1',
                text: 'Start typing to search...'
            },
            ajax: {
                type: 'GET',
                url: app.apiUrl + "sdosubjects/rwyclosurefilter",
                dataType: 'json',
                delay: 500,
                headers: {
                    "RequestVerificationToken": app.antiForgeryTokenValue
                },
                data: function (params, page) {
                    return {
                        Query: params.term,
                        Size: 200,
                        doaId: app.doaId
                    }
                },
                processResults: function (data) {
                    return {
                        results: data
                    }
                },
                cache: true
            },
            templateResult: function (data: any) {
                if (data.loading) {
                    return data.text;
                }
                return self.dataService.getSdoExtendedName(data);
            },
            templateSelection: function (selection: ISdoSubject) {
                if (selection && selection.id === '-1') {

                    return selection.text;
                }
                return self.dataService.getSdoExtendedName(selection);
            },
        }
    }
 
    getRunway(): any {
        const self = this;
        const app = this.closureService.app;
        
        return {
            minimumInputLength: 2,
            width: "100%",
            placeholder: {
                id: '-1',
                text: 'Getting effected runway...'
            },
            ajax: {
                type: 'GET',
                url: app.apiUrl + "sdosubjects/rwyclosurefilter",
                dataType: 'json',
                delay: 500,
                headers: {
                    "RequestVerificationToken": app.antiForgeryTokenValue
                },
                data: function (params, page) {
                    return {
                        Query: params.term,
                        Size: 200,
                        doaId: app.doaId
                    }
                },
                processResults: function (data) {
                    return {
                        results: data
                    }
                },
                cache: true
            },
            templateResult: function (data: any) {
                if (data.loading) {
                    return data.text;
                }
                return data.suffix + data.designator + ' ' + data.name;
            },
            templateSelection: function (selection: ISdoSubject) {
                if (selection && selection.id === '-1') {

                    return selection.text;
                }
                return selection.designator;
            },
        }
        
    }

    getEffectedRunway() {
        return {
            width: "100%",
            placeholder: {
                id: '-1',
                text: 'Select a runway'
            }
        }
    }   

    IsBilingualRegion() {
        return this.isBilingualRegion;
    }

    loadClosureTypes() {
        this.closureService
            .getClosureReasons()
            .subscribe((closureReasons: IClosureType[]) => {
                let closures = closureReasons.map((item: IClosureType) => {
                    if (item.id == this.token.closureReasonId) {
                        return {
                            id: item.id,
                            text: item.name
                        }
                    }
                }).filter(Boolean);

                this.closureReasons = closures;
                this.isFormLoadingData = false;
            });
    }

    setClosureType($event) {
        const closureTypeCtrl = this.$ctrl('closureReasonId');
        const newValue = $event.value;
        if (newValue !== "-1") {
            if (newValue !== closureTypeCtrl.value) {
                closureTypeCtrl.setValue(newValue);
                closureTypeCtrl.markAsDirty();
            }
        }
    }

    checkClosureQuestion() {
        return this.$ctrl('closureQuestion').value;
    }

    private checkBothAircraftRestrictions(): boolean {
        const weight = this.$ctrl('acftWeightRestrictions').value;
        const wing = this.$ctrl('acftWingspanRestrictions').value;

        if (weight | wing) {
            return false;
        }

        return true;
    }

    getOtherReason() {
        const closureQuestionValue = this.$ctrl('closureQuestion');
        const closureTypeCtrl = this.$ctrl('closureReasonId');
        if (closureQuestionValue.value && closureTypeCtrl.value === "Re9") { //if it's other reason'
            return true;
        }
        return false;
    }

    getAvailAsTxyway() {
        return this.$ctrl('availableAsTaxiway').value;
    }

    private updateSubjectSelected(sdoSubject: ISdoSubject) {
        const subjectId = this.nsdFrmGroup.get('subjectId').value;
        if (subjectId === sdoSubject.id) {
            return;
        }

        if (sdoSubject.id !== subjectId) {
            const subjectCtrl = this.nsdFrmGroup.get('subjectId');
            subjectCtrl.setValue(sdoSubject.id);
            subjectCtrl.markAsDirty();
        }

        this.subjectSelected = sdoSubject;

        if (!sdoSubject.subjectGeo) {
            this.dataService.getSdoSubject(sdoSubject.id)
                .subscribe((sdoSubjectResult: ISdoSubject) => {                    
                    this.updateMap(sdoSubjectResult, true);
                });
        }

    }

    private configureReactiveForm() {
        const frmArrControls = <FormArray>this.nsdForm.controls['frmArr'];

        this.nsdFrmGroup = this.fb.group({
            subjectLocation: this.token.subjectLocation || '',
            subjectId: this.token.effectedAerodome,

            closureReasonId: [{ value: this.token.closureReasonId, disabled: true }],
            addClosureReason: [{ value: this.token.addClosureReason, disabled: true }],
            closureQuestion: [{ value: this.token.closureQuestion, disabled: true }],

            effectedAerodrome: [{ value: this.token.effectedAerodrome, disabled: true }],
            effectedRunway: [{ value: this.token.effectedRunway, disabled: true }], 

            availableAsTaxiway: [{ value: this.token.availableAsTaxiway, disabled: true }],
            txyRestrictions: [{ value: this.token.txyRestrictions, disabled: true }],
            freeTextTaxiwayRestriction: [{ value: this.token.freeTextTaxiwayRestriction, disabled: true }],

            otherReason: [{ value: this.token.otherReason, disabled: true }],
            closureReasonOtherFreeText: [{ value: this.token.closureReasonOtherFreeText, disabled: true }],
            closureReasonOtherFreeTextFr: [{ value: this.token.closureReasonOtherFreeTextFr, disabled: true }],

            acftRestrictions: [{ value: this.token.acftRestrictions, disabled: true}],
            maxWingSpanFt: [{ value: this.token.maxWingSpanFt, disabled: true }],
            maxWeightLbs: [{ value: this.token.maxWeightLbs, disabled: true }],
            refreshDummy: [{ value: 0 }, [refreshDummyValidator]],

        });

        frmArrControls.push(this.nsdFrmGroup);
    }
    private setBilingualRegion(value: boolean) {
        this.token.isBilingual = value;
        this.isBilingualRegion = value;
        if (this.isBilingualRegion) {
            if (!this.isReadOnly) {
            }
        } else {
            if (!this.isReadOnly) {
            }
        }
    }


    private setRadioButtonValues() {
        this.$ctrl('availableAsTaxiway').setValue(this.token.availableAsTaxiway);
        this.$ctrl('txyRestrictions').setValue(this.token.txyRestrictions);
        this.$ctrl('closureQuestion').setValue(this.token.closureQuestion);
        this.$ctrl('acftRestrictions').setValue(this.token.acftRestrictions);
    }    
  
    private renderMap(geoJsonStr: string) {
        const self = this;
        const interval = window.setInterval(function () {
            if (self.leafletmap && self.leafletmap.isReady()) {
                window.clearInterval(interval);
               // self.leafletmap.addDoa(geoJsonStr, 'DOA');
            }
        }, 100);
    }

    private updateMap(sdoSubject: ISdoSubject, acceptSubjectLocation: boolean) {
        if (sdoSubject.subjectGeoRefPoint) {
            let geojson = JSON.parse(sdoSubject.subjectGeoRefPoint);
            if (geojson.features.length === 1) {
                this.leafletmap.clearFeaturesOnLayers(['DOA']);
                const subLocation = this.leafletmap.geojsonToTextCordinates(geojson);
                const coordinates = geojson.features[0].geometry.coordinates;
                const markerGeojson = acceptSubjectLocation ? geojson : this.getGeoJsonFromLocation(this.token.subjectLocation);
                if (acceptSubjectLocation) {
                    this.nsdFrmGroup.patchValue({
                        subjectLocation: subLocation,
                        latitude: coordinates[1],
                        longitude: coordinates[0],
                    }, { onlySelf: true, emitEvent: false });
                }
                this.leafletmap.addMarker(geojson, sdoSubject.designator, () => {
                    this.leafletmap.addFeature(sdoSubject.subjectGeo, sdoSubject.designator, () => {
                        if (markerGeojson) {
                            this.leafletmap.setNewMarkerLocation(markerGeojson);
                        }
                    });
                });
            }
        }
    }

    private loadMapLocations(doaId: number, loadingFromProposal: boolean) {
        const self = this;
        this.dataService.getDoaLocation(doaId)
            .subscribe((geoJsonStr: any) => {
                const interval = window.setInterval(function () {
                    if (self.leafletmap && self.leafletmap.isReady()) {
                        window.clearInterval(interval);
                       // self.leafletmap.addDoa(geoJsonStr, 'DOA');
                        if (loadingFromProposal) {
                            self.updateMap(self.subjectSelected, false);
                        }
                    };
                }, 100);
            }, (error: any) => {
                this.toastr.error("DOA Geographic Area Error: " + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    private getGeoJsonFromLocation(value: string) {
        const location = this.locationService.parse(value);
        return location ? this.leafletmap.latitudeLongitudeToGeoJson(location.latitude, location.longitude) : null;
    }

    private makeFirstElement(arr: any[], makeFirst: Function, insertUndefinedAtZero: boolean = true) {
        let len = arr.length;
        for (let i = 0; i < len; i++) {
            if (makeFirst(arr[i])) {
                this.swapArrayPositions(arr, 0, i);
                return;
            }
        }
        if (insertUndefinedAtZero) {
            arr.unshift({
                id: -1,
                text: '',
            });
        }
    }

    private markFrmDirty() {
        this.nsdForm.controls['frmArr'].markAsDirty();
    }

    private swapArrayPositions(arr: any[], p1: number, p2: number) {
        const temp = arr[p1];
        arr[p1] = arr[p2];
        arr[p2] = temp;
    }
}

function refreshDummyValidator(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value === 0) {
        return { 'required': true }
    }
    return null;
}

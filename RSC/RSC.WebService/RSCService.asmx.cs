﻿using Microsoft.Practices.Unity;
using NavCanada.Core.Data.NotamWiz.Contracts;
using UserProfile = NavCanada.Core.Domain.Model.Entitities.UserProfile;
using RSC.Business.SDO;
using RSC.Model;
using RSC.Model.Enums;
using RSC.WebService.Helpers;
using RSC.WebService.Schemas;
using System;
using System.Configuration;
using System.Diagnostics;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using RSC.NDS;

#pragma warning disable CS0618

namespace RSC.WebService
{
    /// <summary>
    /// Summary description for RSCService
    /// </summary>
    [WebService(Namespace = "http://www.navcanada.ca/nes-rsc/webservice")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class RSCService : BaseService<RSCService>
    {
        static RSCService()
        {
            _serviceEnabled = "true".Equals(ConfigurationManager.AppSettings["RSCServiceEnabled"] ?? "false");
            _dummyMode = ConfigurationManager.AppSettings["RSC_DUMMY_MODE"] == "1";
        }

        #region Injected Dependencies

        [Dependency]
        public IUow Uow { get; set; }

        #endregion

        [SoapHeader("Authentication", Required = true)]
        [WebMethod]
        public RSCResponseType reportRSC(RSCRequestType request)
        {
            ConfigureCaching();
            try
            {
                // verify service is enabled
                if (!_serviceEnabled)
                {
                    Context.Response.StatusCode = 405;
                    return CreateError(20300, $"RSC Webservice processing issue: Service not available.");
                }

                // validate authentication
                if (!ValidateAuthentication())
                {
                    Context.Response.StatusCode = 401;
                    return CreateError(11201, "Username or password is wrong.");
                }

                // validate proposal vs schema
                var proposalXml = XMLSerializer.Serialize(request.RSCProposal);
                var validationMessage = XMLValidations.ValidateProposal(proposalXml);
                if (!string.IsNullOrEmpty(validationMessage))
                {
                    //Context.Response.StatusCode = 400;
                    return CreateError(20000, $"RSC Webservice schema validation issue: {validationMessage}");
                }

                // >> todo: validate user, user's role, aerodrome and RSC permissions

                // convert to RSC model
                var proposalModel = request.RSCProposal.ToModel();

                // create the RSC processing engine
                var rscEngine = new Business.RSCEngine(new SDOCacheRepository(Uow), _dummyMode);

                // process request
                var response = rscEngine.Process(proposalModel, request.RetrieveRSC);

                // >> todo: disseminate RSC here...
                if (response.Status != ResponseStatus.Error)
                {
                    var user = GetLoggedUser();
                    if (user != null)
                    {
                        var startActivity = DateTime.UtcNow; // >> tbd
                        var endValidity = startActivity.AddHours(24); // >> tbd

                        var rscDisseminator = new RSCDisseminationManager(Uow, user);
                        rscDisseminator.Disseminate(
                            proposalModel.Aerodrome,
                            response.RSC.EnglishRSC,
                            response.RSC.FrenchRSC,
                            startActivity,
                            endValidity);
                    }
                }

                return response.ToSchema();
            }
            catch (RSCException ex)
            {
                //Context.Response.StatusCode = 500;
                return CreateError(ex.ErrorCode, ex.Message);
            }
            catch (Exception ex)
            {
                //Context.Response.StatusCode = 500;
                TraceException(ex, $"-----Error processing request-----");
                return CreateError(20300, $"RSC Webservice processing issue.");
            }
        }

        [SoapHeader("Authentication", Required = true)]
        [WebMethod]
        public AvailabilityResponseType checkAvailability()
        {
            ConfigureCaching();
            // verify service is enabled
            if (!_serviceEnabled)
            {
                Context.Response.StatusCode = 405;
                return new AvailabilityResponseType()
                {
                    Status = AvailabilityResponseTypeStatus.Item2 // error
                };
            }

            if (ValidateAuthentication())
            {
                return new AvailabilityResponseType()
                {
                    Status = AvailabilityResponseTypeStatus.Item0, // OK
                    Message = new AvailabilityResponseTypeMessage[]
                    {
                        new AvailabilityResponseTypeMessage
                        {
                            MsgNumber = "10000",
                            Description = "System is up and running."
                        }
                    }
                };
            }
            else
            {
                Context.Response.StatusCode = 401;
                return new AvailabilityResponseType()
                {
                    Status = AvailabilityResponseTypeStatus.Item2 // error
                };
            }
        }

        /// <summary>
        /// Soap header will be injected automatically here.
        /// </summary>
        public AUTHHEADER Authentication;

        private UserProfile GetLoggedUser()
        {
            return Uow.UserProfileRepo.GetByUserName(Authentication.USERNAME);
        }

        private void ConfigureCaching()
        {
            Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);  // HTTP 1.1.
            Context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            Context.Response.AppendHeader("Pragma", "no-cache"); // HTTP 1.0.
            Context.Response.AppendHeader("Expires", "0"); // Proxies.
        }

        const string tempPassword = "Nc@2020Trial!";

        private bool ValidateAuthentication()
        {
            // >> todo: fix!! 
            var userName = Authentication?.USERNAME ?? string.Empty;
            var user = !string.IsNullOrEmpty(userName) ? Uow.UserProfileRepo.GetByUserName(userName) : null;
            return user != null && tempPassword.Equals(Authentication?.PASSWORD); //return userName.StartsWith("vendor") && tempPassword.Equals(Authentication?.PASSWORD);
        }

        static RSCResponseType CreateError(int errorCode, string errorMessage)
        {
            var rscResponse = new RSCResponse() { Status = ResponseStatus.Error };
            rscResponse.Messages.Add(new RSCResponseMessage()
            {
                Status = ResponseStatus.Error,
                Number = errorCode,
                Description = errorMessage
            });
            return rscResponse.ToSchema();
        }

        static void TraceException(Exception ex, string header)
        {
            Trace.WriteLine(header);
            Trace.WriteLine(ex.Message);
            Trace.WriteLine($"Stack:\n{ex.StackTrace}");
            if (ex.InnerException != null)
                TraceException(ex.InnerException, "INNER EXCEPTION:");
        }

        /// <summary>
        /// Internal class to handle the soap authentication headers.
        /// </summary>
        /// <remarks>DO NOT rename this class or SOAP headers wont get serialized!</remarks>
        public class AUTHHEADER : SoapHeader
        {
            public string USERNAME;
            public string PASSWORD;
        }

        static bool _serviceEnabled;
        static bool _dummyMode;
    }
}
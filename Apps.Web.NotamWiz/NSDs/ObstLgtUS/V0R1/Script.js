﻿define(["jquery", "Resources", "utilities", "mapping", "jsonpath", "kendoExt", "kendoCustomDataBinders", "javaExt"], function($, res, Util)
{
    var RES = res.getResources("NSD-ObstLgtUS");
    var viewModel = null;
    var self = null;
    var conditionValidator = null;
    var obstacleTypes = $("#nsdData").data("obstacletypes");
    var nsdEditor = null;
    return {
        obstPushPin: null,
        validationErrorMessage: null,
        init: function(editor, model)
        {
            self = this;
            nsdEditor = editor;
            viewModel = model;
            viewModel.createNOTAMViewModel($.extend($("#nsdModel").data("model"), {
                Bilingual: viewModel.Options.Bilingual,
                ObstacleType: "",
                Location: Util.decimalToDMS(viewModel.subject.SubjectGeoRefLat, "lat") + " " + Util.decimalToDMS(viewModel.subject.SubjectGeoRefLong, "long"),
                Height: 10,
                Elevation: 10
            }));
            self.initUI();
            self.setupUpNSDEditor();
            conditionValidator = $("#nsdConditionContainer").data("kendoValidator");
            editor.conditionValidator = conditionValidator;
            viewModel.nsdTokens.bind("change", self.validate);
        },
        initUI: function()
        {
            self.obstPushPin = nsdEditor.bingMap.addPushPin(
                viewModel.subject.SubjectGeoRefLat,
                viewModel.subject.SubjectGeoRefLong,
                {
                    draggable: false,
                    height: 50,
                    width: 50,
                    anchor: new Microsoft.Maps.Point(0, 50),
                    icon: "/Images/BluePushpin.png"
                });
            $("#obstacleTypes").kendoDropDownList({
                dataTextField: "Text",
                dataValueField: "Value",
                dataSource: obstacleTypes.Data,
                index: 0
            });
            $("#obstacleLocation").kendoMaskedTextBox({
                change: function()
                {
                    self.updateObstacleLocation();
                }
            });
            $("#obstacleHeight").kendoNumericTextBox({
                decimals: 0,
                format: "#",
                min: 10,
                max: 3000
            });
            $("#obstacleElevation").kendoNumericTextBox({
                decimals: 0,
                format: "#",
                min: 10,
                max: 99999
            });
            $("#nsdConditionContainer").kendoValidator({
                rules: {
                    all: function(input)
                    {
                        var inputName = input.attr("name");
                        if (!inputName && input.hasClass("k-formatted-value") && input.siblings("input"))
                        {
                            inputName = $(input.siblings("input")[0]).attr("name");
                        }
                        switch (inputName)
                        {
                            case "obstacleTypes":
                                return self.setValidationError(function()
                                {
                                    return $("#obstacleTypes").data("kendoDropDownList").dataItem().id != "";
                                }, RES.dbRes("Obstacle type not defined."));
                            case "obstacleLocation":
                                var obstacleLocation = $("#obstacleLocation").val();
                                if (self.setValidationError(function()
                                {
                                    return Util.IsValidLocation(obstacleLocation);
                                }, RES.dbRes("Invalid obstacle location.")))
                                {
                                    return self.setValidationError(function()
                                    {
                                        return viewModel.IsWithinDOA(obstacleLocation);
                                    }, RES.dbRes("Obstacle is outside your Domain of Authority."));
                                }
                                return false;
                        }
                        return true;
                    }
                },
                messages: {
                    all: function(input)
                    {
                        return self.validationErrorMessage;
                    }
                }
            });
        },
        setValidationError: function(condition, error)
        {
            var result = condition();
            if (result)
            {
                self.validationErrorMessage = "";
            }
            else
            {
                self.validationErrorMessage = error;
            }
            return result;
        },
        validate: function(e)
        {
            viewModel.validModel = false;
            Util.setConditionValid(false);
            if (!conditionValidator.validate())
            {
                return;
            }
            viewModel.generateNOTAM();
        },
        setupUpNSDEditor: function()
        {
            Util.hidePermement();
        },
        updateObstacleLocation: function()
        {
            var loc = viewModel.nsdTokens.Location;
            if (!Util.IsValidLocation(loc))
            {
                return;
            }
            var parts = Util.locationToDecimal(loc).split(" ");
            var location = new Microsoft.Maps.Location(parts[0], parts[1]);
            self.obstPushPin.setLocation(location);
            nsdEditor.bingMap.centerMap(location);
        }
    };
});
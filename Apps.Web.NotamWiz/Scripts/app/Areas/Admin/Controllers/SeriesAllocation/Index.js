﻿
define(["jquery", "Resources", "kendoExt"], function ($, res) {

    var RES = res.getResources("SeriesAllocation");
    var geoRegions = null;
    var disseminationLevels = null;
    var thisModel = null;
    var labels = {};
    return {
        init: function (lbs) {
            labels = lbs;

            thisModel = this;
            geoRegions = $("#geoRegions").data("model");
            disseminationLevels = $("#disseminationLevels").data("model");

            thisModel.initUI();
        },

        initUI: function () {
            var grid = $("#seriesAllocationGrid").kendoGrid({
                pageable: {
                    refresh: true,
                    pageSize: 25,
                    pageSizes: [5, 10, 25, 99999],
                    buttonCount: 5,
                    messages: {
                        display: labels.displayMask1,
                        empty: labels.noData,
                        page: labels.enterPage,
                        of: labels.ofMask,
                        itemsPerPage: labels.dataItemsPerPage,
                        first: labels.firstPage,
                        last: labels.lastPage,
                        next: labels.nextPage,
                        previous: labels.prevPage,
                        refresh: labels.refreshGrid,
                        morePages: labels.morePages
                    }
                },
                width: "100%",
                scrollable: true,
                filterable: true,
                sortable: true,
                columns: [
                    { field: "RegionId", title: labels.regionLabel, width: "*", values: geoRegions },
                    { field: "Series", title: labels.seriesLabel, width: 100 },
                    { field: "DisseminationCategory", title: labels.desseminationCategory, width: 200, values: disseminationLevels },
                    { field: "QCode", title: labels.qCodeLabel, width: 100 },
                    {
                        command: [{
                            name: "edit", text: {
                                edit: labels.editLabel, update: labels.saveLabel, cancel: labels.cancelLabel
                            }
                        },
                            {
                                name: "exDestroy",
                                text: labels.deleteLabel,
                                click: function (e) {
                                    var tr = $(e.target).closest("tr"); //get the row for deletion
                                    var userRecord = this.dataItem(tr); //get the row data so it can be referred later

                                    $.when(kendo.ui.ExtOkCancelDialog.show({
                                        title: labels.confirmLabel,
                                        message: labels.deleteConfirmation,
                                        width: '400px',
                                        height: '150px',
                                        icon: "k-Ext-warning",
                                    })).done(function (response) {
                                        if (response.button === 'OK') {
                                            debugger;
                                            grid.dataSource.remove(userRecord);
                                            grid.dataSource.sync();
                                        }

                                    });
                                }
                            }
                        ],
                        width: 100
                    }
                ],
                toolbar: [
                    { name: "create", text: labels.addLabel }
                ],
                editable: {
                    mode: "inline",
                },
                dataSource: {
                    serverPaging: true,
                    schema: {
                        data: function (data) { return data.Data; },
                        total: 'Total',
                        model: {
                            id: "Id",
                            fields: {
                                Id: { editable: true },
                                RegionId: { editable: true },
                                Series: { editable: true },
                                DisseminationCategory: { field: "DisseminationCategory", type: "number", defaultValue: 0, validation: { required: true } },
                                QCode: { editable: true },
                            }
                        }
                    },
                    transport: {
                        create: { url: "SeriesAllocation/SeriesAllocation_Create", dataType: "json", type: "POST" },
                        read: { url: "SeriesAllocation/SeriesAllocation_Read", dataType: "json", type: "POST" },
                        update: { url: "SeriesAllocation/SeriesAllocation_Update", dataType: "json", type: "POST" },
                        destroy: { url: "SeriesAllocation/SeriesAllocation_Destroy", dataType: "json", type: "POST" }

                    }
                },
                edit: function (e) {
                    //$("#seriesAllocationGrid").find(".k-grid-save-changes").show();
                    // $("#seriesAllocationGrid").find(".k-grid-cancel-changes").show();
                },
                dataBound: function (e) {
                    // $("#seriesAllocationGrid").find(".k-grid-save-changes").hide();
                    //$("#seriesAllocationGrid").find(".k-grid-cancel-changes").hide();
                },



            }).data("kendoGrid");
        },



    };
});

﻿namespace NDS.DataBus.Contracts
{
    public class Property
    {
        public static Property Create(string name, object value) => new Property() { Name = name, Value = value };
        public string Name { get; set; }
        public object Value { get; set; }
    }
}
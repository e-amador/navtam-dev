﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RSC.WebService.Helpers
{
    public class SchemaFieldAssigner<T> where T : IComparable
    {
        private SchemaFieldAssigner(T[] itemTypes, object[] items)
        {
            _itemTypes = itemTypes;
            _items = items;
            _assigners = new Dictionary<T, Action<object>>();
        }

        public static SchemaFieldAssigner<T> For(T[] itemTypes, object[] items) => new SchemaFieldAssigner<T>(itemTypes, items);

        public SchemaFieldAssigner<T> On(T itemType, Action<object> assigner)
        {
            _assigners[itemType] = assigner;
            return this;
        }

        public void Apply()
        {
            for (var i = 0; i < _itemTypes.Length; i++)
            {
                var itemType = _itemTypes[i];
                if (_assigners.ContainsKey(itemType))
                {
                    _assigners[itemType].Invoke(_items[i]);
                }
            }
        }

        readonly T[] _itemTypes;
        readonly object[] _items;
        readonly Dictionary<T, Action<object>> _assigners;
    }
}
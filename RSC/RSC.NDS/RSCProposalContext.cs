﻿using NavCanada.Core.Domain.Model.Entitities;
using RSC.Model;
using System;

namespace RSC.NDS
{
    public class RSCProposalContext
    {
        public RSCProposal Proposal { get; set; }
        public UserProfile User { get; set; }
        public RSCText ItemE { get; set; }
        public DateTime StartActivity { get; set; }
        public DateTime EndValidity { get; set; }
    }
}

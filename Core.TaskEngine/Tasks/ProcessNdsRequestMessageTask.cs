﻿using NavCanada.Core.TaskEngine.Contracts;
using NavCanada.Core.TaskEngine.Mto;
using NavCanada.Core.TaskEngine.NdsTasks;
using System;

namespace NavCanada.Core.TaskEngine.Tasks
{
    public class ProcessNdsRequestMessageTask : IMessageTask
    {
        public void Execute(IMessageTaskContext context)
        {
            var logger = context.ExternalServices.Logger;
            var loggerType = GetType();

            try
            {
                // if this were to fail we don't want to put the system down
                context.Mto.Add(MessageDefs.OverridesCriticalFailureKey, true, DataType.Boolean);

                logger.LogDebug(GetType(), "ProcessNdsRequestMessageTask task start excecuting.");

                if (context.Mto[MessageDefs.EntityIdentifierKey] == null)
                {
                    logger.LogWarn(loggerType, "Missing entity identifier");
                    return;
                }

                var obj = context.Mto[MessageDefs.EntityIdentifierKey].Value;
                Guid id;
                if (!Guid.TryParse(obj.ToString(), out id))
                    throw new InvalidOperationException("ProcessNdsRequestMessageTask task failed to deserialize RequestMessage Id");

                var uow = context.ClusterStorage.Uow;
                var message = uow.NdsIncomingRequestRepo.GetById(id);

                if (message == null)
                    throw new Exception($"ProcessNdsRequestMessageTask could not read NDS Incoming Messsage with ID: {id}");

                NdsTaskHandler.Execute(message, context.ClusterStorage, context.ExternalServices.Logger);

                logger.LogDebug(GetType(), $"ProcessNdsRequestMessageTask: Task completed.");
            }
            catch (Exception e)
            {
                logger.LogError(e, loggerType, "Error with ProcessNdsRequestMessageTask");
                throw;
            }
        }
    }
}
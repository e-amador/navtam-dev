﻿using System.Collections.Generic;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Proxies.AimslProxy.Common;

namespace NavCanada.Core.Proxies.AimslProxy
{
    public interface IAimslProxy
    {
        AismlResponse Submit(NotamProposal proposal, string accountableSource);

        AismlResponse CheckStatus(long notamProposalId);

        List<AismlResponse> CheckStatus(long[] notamProposalIds);

        AismlResponse Widthraw(long notamProposalId);

        //AismlResponse Cancel(long notamProposalId);
    }
}

﻿using NavCanada.Core.Domain.Model.Entitities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace NavCanada.Core.Data.EF.Configurations
{
    public class UserRegistrationConfig : EntityTypeConfiguration<UserRegistration>
    {
        public UserRegistrationConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            HasRequired(e => e.UserProfile)
                .WithMany()
                .HasForeignKey(e => e.UserProfileId)
            .WillCascadeOnDelete(false);
        }
    }
}

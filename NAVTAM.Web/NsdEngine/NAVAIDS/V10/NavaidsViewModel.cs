﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NAVTAM.NsdEngine.NAVAIDS.V10
{
    public class NavaidsViewModel : TokenViewModel
    {
        public string NavaidName { get; set; }
        public string NavaidType { get; set; } //Fix Type
        public string Identifier { get; set; } //CodeId
        public string Frequency { get; set; }
        public string Reason { get; set; }
        public string Location { get; set; }
        public string Condition { get; set; }

    }
}
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CatchAllService = void 0;
var core_1 = require("@angular/core");
var Rx_1 = require("rxjs/Rx");
var http_1 = require("@angular/http");
var windowRef_service_1 = require("../../../common/windowRef.service");
//import { ISdoSubject } from './catchall.model';
var CatchAllService = /** @class */ (function () {
    function CatchAllService(http, winRef) {
        this.http = http;
        this.app = winRef.nativeWindow['app'];
    }
    //getSdoSubject(subId: string): Observable<ISdoSubject>{
    //    return this.http.get(this.app.apiUrl + `sdosubjects/find/?subId=${subId}`).map((response: Response) => {
    //        return response.json();
    //    }).catch(this.handleError);
    //}
    CatchAllService.prototype.handleError = function (error) {
        return Rx_1.Observable.throw(error.statusText);
    };
    CatchAllService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http, windowRef_service_1.WindowRef])
    ], CatchAllService);
    return CatchAllService;
}());
exports.CatchAllService = CatchAllService;
//# sourceMappingURL=catchall.service.js.map
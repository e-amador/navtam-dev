﻿namespace NavCanada.Core.Domain.Model.Entitities
{
    public class NdsClientSeries
    {
        public int Id { get; set; }
        public int NdsClientId { get; set; }
        public NdsClient NdsClient { get; set; }
        public string Series { get; set; }
    }
}

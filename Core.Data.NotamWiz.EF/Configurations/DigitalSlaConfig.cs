﻿namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    using Domain.Model.Entitities;

    public class DigitalSlaConfig : EntityTypeConfiguration<DigitalSla>
    {
        public DigitalSlaConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(e => e.Version).IsRequired();
            Property(e => e.Content).IsRequired();
        }
    }
}

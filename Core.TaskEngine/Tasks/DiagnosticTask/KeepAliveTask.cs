﻿namespace NavCanada.Core.TaskEngine.Tasks.DiagnosticTask
{
    using Domain.Model.Enums;
    using Contracts;
    using Mto;

    public class KeepAliveTask : IMessageTask
    {
        public void Execute(IMessageTaskContext context)
        {
            context.ExternalServices.Logger.LogDebug(GetType(), "KeepAliveTask: Task start excecuting...");

            var diagnosticProxy = context.ExternalServices.DiagnosticProxy;
            if (diagnosticProxy != null)
            {
                var instanceName = context.Mto[MessageDefs.EntityIdentifierKey].Value;
                if (instanceName != null)
                    diagnosticProxy.PostKeepAliveMessage(instanceName.ToString(), TickType.KeepAlive);
            }

            context.ExternalServices.Logger.LogDebug(GetType(), "KeepAliveTask: Task finish excecuting...");
        }
    }
}
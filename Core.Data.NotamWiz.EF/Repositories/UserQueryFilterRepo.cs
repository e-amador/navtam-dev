﻿using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.EF.Repositories
{
    public class UserQueryFilterRepo : Repository<UserQueryFilter>, IUserQueryFilterRepo
    {
        public UserQueryFilterRepo(DbContext dbContext)
            : base(dbContext)
        {
        }

        public async Task<UserQueryFilter> GetProposalFiltersBySlotAndUsernameAsync(int slotNumber, string username)
        {
            return await DbSet
                .SingleOrDefaultAsync(e => 
                    e.Username == username && 
                    e.UserFilterType == UserFilterType.ProposalHistory && 
                    e.SlotNumber == slotNumber);
        }

        public async Task<List<UserQueryFilter>> GetAllProposalFiltersByUsernameAsync(string username)
        {
            return await DbSet
                .Where(e => e.Username == username && e.UserFilterType == UserFilterType.ProposalHistory)
                .ToListAsync();
        }

        public async Task<UserQueryFilter> GetNotamFiltersBySlotAndUsernameAsync(int slotNumber, string username, UserFilterType filterType)
        {
            return await DbSet
                .SingleOrDefaultAsync(e =>
                    e.Username == username &&
                    e.UserFilterType == filterType &&
                    e.SlotNumber == slotNumber);
        }

        public async Task<List<UserQueryFilter>> GetAllNotamFiltersByUsernameAsync(string username, UserFilterType filterType)
        {
            return await DbSet
                .Where(e => e.Username == username && e.UserFilterType == filterType)
                .ToListAsync();
        }

    }

}

﻿using System.Data.Entity.Spatial;

namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    using Common.Common;
    using Contracts.Repositories;
    using Domain.Model.Entitities;
    using Microsoft.SqlServer.Types;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;
    using System;
    using System.Collections.Generic;
    using Domain.Model.Dtos;
    using Extensions;

    public class GeoRegionRepo : Repository<GeoRegion>, IGeoRegionRepo
    {
        public GeoRegionRepo(DbContext dbContext)
            : base(dbContext)
        {
        }

        public Task<GeoRegion> GetByIdAsync(int id)
        {
            return DbSet.FirstOrDefaultAsync(e => e.Id == id);
        }

        public GeoRegion GetByName(string name)
        {
            return DbSet.FirstOrDefault(e => e.Name == name); 
        }

        public GeoRegion GetByGeoLocation(DbGeography geoLocation)
        {
            var loc = DbGeography.FromText(SqlGeography.Parse(geoLocation.AsText()).MakeValid().ToString(), CommonDefinitions.Srid);
            return DbSet.FirstOrDefault(e => e.Geo.Intersects(loc));
        }

        public async Task<GeoRegion> GetByGeoLocationAsync(DbGeography geoLocation)
        {
            var loc = DbGeography.FromText(SqlGeography.Parse(geoLocation.AsText()).MakeValid().ToString(), CommonDefinitions.Srid);
            return await DbSet.FirstOrDefaultAsync(e => e.Geo.Intersects(loc));
        }

        public async Task<List<GeoRegionDto>> GetAllGeoRegionsAsync()
        {
            var ctx = DbContext as AppDbContext;

            var geoRegions = await DbSet.ToListAsync();

            var dtos = new List<GeoRegionDto>();

            foreach (var geoRegion in geoRegions)
            {
                dtos.Add(new GeoRegionDto
                {
                    Id = geoRegion.Id,
                    Name = geoRegion.Name,
                });
            }
            return dtos;
        }

        public async Task<int> GetAllCountAsync()
        {
            return await DbSet.CountAsync();
        }

        public async Task<List<GeoRegionDto>> GetGeoRegionPartials(QueryOptions queryOptions)
        {
            var regions = await DbSet.Select(d => new GeoRegionDto() { Id = d.Id, Name = d.Name })
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return regions;
        }

        public async Task<bool> Exists(int id, string name)
        {
            return (id < 1) ? await DbSet.AnyAsync(d => d.Name == name) : await DbSet.AnyAsync(d => d.Name == name && d.Id != id);
        }

    }
}

﻿namespace NavCanada.Services.Api.Diagnostics.Data
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;

    using NavCanada.Services.Api.Diagnostics.Data.Configurations;
    using NavCanada.Services.Api.Diagnostics.Data.Contracts;
    using NavCanada.Services.Api.Diagnostics.Models;

    public class DiagnosticDbContext : DbContext, IDiagnosticDbContext
    {
        public DiagnosticDbContext(string connectionStringName)
            : base(nameOrConnectionString: connectionStringName ?? "DefaultDiagnosticConnStr")
        {

        }

        public DiagnosticDbContext()
            : this("DefaultDiagnosticConnStr")
        {
        }

        //Use oo configure Owin Auth
        public static DiagnosticDbContext Create()
        {

            return new DiagnosticDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Use singular table names
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Configurations.Add(new LogConfig());
            modelBuilder.Configurations.Add(new KeepAliveConfig());
        }

        public IDbSet<Log> Logs { get; set; }
        public IDbSet<KeepAlive> KeepAlives { get; set; }
    }
}

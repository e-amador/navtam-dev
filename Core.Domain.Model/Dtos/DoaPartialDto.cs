﻿using System.Collections.Generic;
using System.Data.Entity.Spatial;

namespace NavCanada.Core.Domain.Model.Dtos
{
    public class DoaPartialDto
    {
        public int Id { get; set;  }
        public string Name { get; set; }
    }

    public class DoaAdminPartialDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Organizations { get; set; }
        public int OrgCount { get; set; }
    }

    public class DoaAdminDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string DoaGeoArea { get; set; }
    }

    public class DoaOrganizationsDto
    {
        public int DoaId { get; set; }
        public List<string> Organizations { get; set; }
    }
}

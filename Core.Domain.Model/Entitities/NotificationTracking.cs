﻿using System;
using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class NotificationTracking
    {
        public int Id { get; set; }

        public NotificationType NotifiationType { get; set; }

        public TemplateType TemplateType { get; set; }

        public DateTime? SentDatetime { get; set; }

        public long? PackageId { get; set; }
    }
}
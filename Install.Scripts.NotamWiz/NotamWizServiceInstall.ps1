Add-Type -Assembly "System.IO.Compression.FileSystem";
Set-PSDebug -Trace 1

$scriptPath = split-path -parent $MyInvocation.MyCommand.Definition

# Backup old copy;;
$date = (Get-Date).Month
$dateDay = (Get-Date).Day
$timeHour = (Get-Date).Hour
$timeMinute = (Get-Date).Minute
[System.IO.Compression.ZipFile]::CreateFromDirectory("C:\Service\Install", "C:\Service\Backup_" + $date +"_" +$dateDay + "_" + $timeHour + "_" + $timeMinute +".zip");


# uninstall old taks engine Services.Win.TaskEngine.exe - uninstall 
# sc delete Services.Win.TaskEngine
$uninstallCommand = "C:\Service\Install\Services.Win.TaskEngine.exe -uninstall"

 
Invoke-Expression $uninstallCommand

start-sleep -s 90

#Copy new copy
Remove-Item "C:\Service\Install\*" -Recurse 

start-sleep -s 10

$copyPath = $scriptPath+"\*"
Copy-Item -Path $copyPath  -Destination "C:\Service\Install" -Recurse 

start-sleep -s 5

#install new task engine Services.Win.TaskEngine.exe - install
$installCommand = "C:\Service\Install\Services.Win.TaskEngine.exe -install"
Invoke-Expression $installCommand
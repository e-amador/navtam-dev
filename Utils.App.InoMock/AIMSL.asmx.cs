﻿using AutoMapper;

using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace INOMock
{
    using NavCanada.Utils.Apps.InoMock;
    using NavCanada.Utils.Apps.InoMock.Models;
    using NavCanada.Utils.Apps.InoMock.WebServiceMessages;

    /// <summary>
    /// Summary description for AIMSL
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class AIMSL : System.Web.Services.WebService
    {
        DBContext db = new DBContext();
        static string autoApprove = ConfigurationManager.AppSettings.Get("AutoApprove");
        
        [WebMethod]
        public NotamProposalStatusMessage SubmitNotamProposal(NotamProposal proposal)
        {
            if (autoApprove == "true")
            {
                proposal.Status = NotamProposalStatusType.Submitted;
                proposal.StatusTime = DateTime.Now;
                db.NotamProposals.Add(proposal);
                proposal.SubmittedNotam = Mapper.Map<NotamProposal, Notam>(proposal);

                db.SaveChanges();
            }
            else { 
                proposal.Status = NotamProposalStatusType.Queued;
                proposal.StatusTime = DateTime.Now;
                db.NotamProposals.Add(proposal);
                db.SaveChanges();
            }

            


            //Update client
            IHubContext _hubContext = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
            _hubContext.Clients.All.refreshData();

            return new NotamProposalStatusMessage() { NotamProposalId = proposal.Id, Status = proposal.Status, StatusTime = proposal.StatusTime, SubmittedNotam = proposal.SubmittedNotam, RejectionReason = proposal.RejectionReason };
        }

        [WebMethod]
        public NotamProposalStatusMessage GetNotamProposalStatus(long id)
        {


            NotamProposal proposal = (from p in db.NotamProposals.Include("SubmittedNotam") where p.Id == id select p).FirstOrDefault();

            if (proposal == null)
                return new NotamProposalStatusMessage() { NotamProposalId = -1, Status = NotamProposalStatusType.Undefined, StatusTime = DateTime.Now, SubmittedNotam = null };
            


            return new NotamProposalStatusMessage()
            {
                NotamProposalId = proposal.Id,
                Status = proposal.Status,
                StatusTime = proposal.StatusTime,
                SubmittedNotam = proposal.SubmittedNotam,
                RejectionReason = proposal.RejectionReason
            };
        }


        [WebMethod]
        public NotamProposalStatusMessage WithdrawNotamProposal(long id)
        {


            NotamProposal proposal = (from p in db.NotamProposals where p.Id == id select p).FirstOrDefault();

            if (proposal == null)
                return new NotamProposalStatusMessage() { NotamProposalId = -1, Status = NotamProposalStatusType.Undefined, StatusTime = DateTime.Now, SubmittedNotam = null };

            if(proposal.Status ==NotamProposalStatusType.Queued)
            {
                proposal.Status = NotamProposalStatusType.Withdrawn;
                db.SaveChanges();

                //Update client
                IHubContext _hubContext = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
                _hubContext.Clients.All.refreshData();

                return new NotamProposalStatusMessage() { NotamProposalId = proposal.Id, Status = proposal.Status, StatusTime = proposal.StatusTime, SubmittedNotam = proposal.SubmittedNotam, RejectionReason = proposal.RejectionReason };
            }


            return new NotamProposalStatusMessage() { NotamProposalId = proposal.Id, Status = proposal.Status, StatusTime = proposal.StatusTime, SubmittedNotam = proposal.SubmittedNotam, RejectionReason = proposal.RejectionReason };
        }
    }
}

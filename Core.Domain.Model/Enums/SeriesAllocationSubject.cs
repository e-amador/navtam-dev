﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum SeriesAllocationSubject
    {
        Aerodrome,
        FIR,
        FIRMOB,
        RSC
    }
}

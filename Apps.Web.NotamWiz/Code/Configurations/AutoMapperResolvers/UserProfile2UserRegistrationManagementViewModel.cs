﻿using System;
using AutoMapper;
using NavCanada.SE.NOTAMWiz.Models;
using System.Linq;
using log4net;
using System.Security.Claims;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using NavCanada.SE.NotamWiz.UI.Wiz.Web.Models;


namespace NavCanada.SE.NOTAMWiz.App_Start.AutoMapperResolvers
{
    public class UserProfile2UserRegistrationManagementVM : ITypeConverter<UserProfile, UserRegistrationManagementVM>
    {
        private NOTAMWizDBContext db = new NOTAMWizDBContext();
        ILog log = LogManager.GetLogger(typeof(UserProfile2UserRegistrationManagementVM));

        public UserRegistrationManagementVM Convert(ResolutionContext context)
        {
            UserProfile userProfile = context.SourceValue as UserProfile;
            try
            {
                Registration userRegistrationsInfo = db.Registrations.SingleOrDefault(c => c.RegistrationOrgId == userProfile.RegistrationOrganizationId);
                return new UserRegistrationManagementVM()
                {

                    userVM = Mapper.Map<UserProfile,UserManagementVM>(userProfile),
                    orgVM =  Mapper.Map<Registration,OrganizationViewModel>(userRegistrationsInfo),
                    
                };


            }
            catch (Exception ex)
            {
                log.Error("Exception converting from UserProfile (" + userProfile.UserName + ") to UserManagementVM", ex);
                return null;
            }
        }


    }
}
﻿using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;

namespace NAVTAM.NsdEngine
{
    public class NearSdoSubject
    {
        public NearSdoSubject(SdoCache subject, double distance, DisseminationCategory category = DisseminationCategory.International)
        {
            Subject = subject;
            Distance = distance;
            DisseminationCategory = category;
        }

        public SdoCache Subject { get; set; }
        public double Distance { get; set; }
        public DisseminationCategory DisseminationCategory { get; set; }
    }
}
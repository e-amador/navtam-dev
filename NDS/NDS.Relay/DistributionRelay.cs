﻿using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NDS.Common.Xml;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NDS.Relay
{
    public class DistributionRelay
    {
        public DistributionRelay(IUow uow, IMessagePublisher publisher)
        {
            _uow = uow;
            _publisher = publisher;
        }

        public void Disseminate(Notam notam, bool distribute)
        {
            var message = RelayUtils.GetHubBroadCastMessage(notam);

            // ensure the xml is valid according to the ICD schema
            ValidateMessageXml(message.Content);

            _uow.NdsOutgoingMessageRepo.Add(message);
            _uow.SaveChanges();

            // distribute to subscriptions if needed
            if (distribute)
            {
                _publisher.QueueNotamForDistribution(notam.Id);
            }

            _publisher.PublishHubMessage(message);
        }

        public int DistributeToAftn(Notam notam, Predicate<NdsClientIdAddressDto> testAddress, bool onlyActive)
        {
            var distributedCount = 0;
            var addresses = new HashSet<string>();

            #region Distribute based on Series & ItemA

            var itemA = GetNotamItemA(notam);

            // we don't want to send the checklist unless the subscription specify a series
            var allowAllSeries = !notam.SeriesChecklist;

            // get bilingual clients (english & french)
            var bilingualAddresses = _uow.NdsClientRepo
                                         .GetMatchingAftnAddresses(itemA, notam.Series, allowAllSeries, true, onlyActive)
                                         .Where(a => testAddress(a))
                                         .Select(a => a.Address)
                                         .Distinct()
                                         .ToList();

            if (bilingualAddresses.Count > 0)
            {
                distributedCount += bilingualAddresses.Count;
                DistributeAftnMessageGroup(notam, bilingualAddresses, bilingual: true);

                addresses.UnionWith(bilingualAddresses);
                bilingualAddresses.Clear();
            }

            // get monolingual clients (english only)
            var monolingualAddresses = _uow.NdsClientRepo
                                           .GetMatchingAftnAddresses(itemA, notam.Series, allowAllSeries, false, onlyActive)
                                           .Where(a => testAddress(a) && !addresses.Contains(a.Address))
                                           .Select(a => a.Address)
                                           .Distinct()
                                           .ToList();

            if (monolingualAddresses.Count > 0)
            {
                distributedCount += monolingualAddresses.Count;
                DistributeAftnMessageGroup(notam, monolingualAddresses, bilingual: false);

                addresses.UnionWith(monolingualAddresses);
                monolingualAddresses.Clear();
            }

            #endregion

            // checklist not distributed to geo ref clients
            if (notam.SeriesChecklist)
                return distributedCount;

            #region Distribute based on Geo referencing

            // get bilingual geo ref clients (english & french)
            bilingualAddresses = _uow.NdsClientRepo
                                     .GetMatchingAftnAddressesByGeoRef(notam.EffectArea, notam.LowerLimit, notam.UpperLimit, true, onlyActive)
                                     .Where(a => testAddress(a) && !addresses.Contains(a.Address))
                                     .Select(a => a.Address)
                                     .Distinct()
                                     .ToList();

            if (bilingualAddresses.Count > 0)
            {
                DistributeAftnMessageGroup(notam, bilingualAddresses, bilingual: true);
                distributedCount += bilingualAddresses.Count;

                addresses.UnionWith(bilingualAddresses);
                bilingualAddresses.Clear();
            }

            // get monolingual clients (english only)
            monolingualAddresses = _uow.NdsClientRepo
                                       .GetMatchingAftnAddressesByGeoRef(notam.EffectArea, notam.LowerLimit, notam.UpperLimit, false, onlyActive)
                                       .Where(a => testAddress(a) && !addresses.Contains(a.Address))
                                       .Select(a => a.Address)
                                       .Distinct()
                                       .ToList();

            if (monolingualAddresses.Count > 0)
            {
                DistributeAftnMessageGroup(notam, monolingualAddresses, bilingual: false);
                distributedCount += monolingualAddresses.Count;

                monolingualAddresses.Clear();
            }

            #endregion

            // total addresses sent
            return distributedCount;
        }

        private void DistributeAftnMessageGroup(Notam notam, IEnumerable<string> addresses, bool bilingual)
        {
            var groupId = Guid.NewGuid();
            var messages = RelayUtils.GetAftnBroadcastMessages(notam, groupId, addresses, bilingual);

            // create message group with ordering
            var group = RelayUtils.CreateMessageGroupOrdering(messages);

            foreach (var message in group)
            {
                _uow.NdsOutgoingMessageRepo.Add(message);
                _uow.NdsNotamMessageRepo.Add(new NdsNotamMessage() { MessageId = message.Id, NotamId = notam.Id });
            }
            _uow.SaveChanges();

            var firstMessage = group.First();

            // queue message to start publishing the group
            _publisher.QueueMessageGroupForPublishing(firstMessage.Id);
        }

        static string GetNotamItemA(Notam notam)
        {
            if (notam.SeriesChecklist)
                return string.Empty;

            if ("CXXX".Equals(notam.ItemA))
                return ParseNotamItemA(notam.ItemE, notam.ItemA);

            return notam.ItemA;
        }

        static string ParseNotamItemA(string itemE, string defaultItemA)
        {
            var trimmedItemE = (itemE ?? "").Trim();
            return trimmedItemE.Length >= 4 ? trimmedItemE.Substring(0, 4) : defaultItemA;
        }

        static void ValidateMessageXml(string xml)
        {
            var validationMessage = XmlSerializerExtensions.ValidateNotamXml(xml);
            if (!string.IsNullOrEmpty(validationMessage))
                throw new Exception(validationMessage);
        }

        readonly IUow _uow;
        readonly IMessagePublisher _publisher;
    }
}

﻿using System.Collections.Generic;

namespace NAVTAM.NsdEngine.AREADEFINITION.V10
{
    public class AreaDefinitionClosureReasons
    {
        public AreaDefinitionClosureReasons(string reason, string qcode)
        {
            Reason = reason;
            Qcode = qcode;
        }
        
        public string Reason { get; }
        public string Qcode { get; }
    }

    public class AreaDefinitionReasons
    {
        static readonly Dictionary<string, Dictionary<int, AreaDefinitionClosureReasons>> _dictionary = new Dictionary<string, Dictionary<int, AreaDefinitionClosureReasons>>
        {
            {
                "en", new Dictionary<int, AreaDefinitionClosureReasons>
                {
                    { 1, BuildReasonEn("HELI-LOGGING", "XXXX") },
                    { 2, BuildReasonEn("SKYLINE LOGGING", "XXXX") },
                    { 3, BuildReasonEn("BLASTING", "WHLW") },
                    { 4, BuildReasonEn("MIL", "RMCA") },
                    { 5, BuildReasonEn("SAR", "WELW")},
                    { 6, BuildReasonEn("SAR EXER", "WELW")},
                    { 7, BuildReasonEn("PARAMOTORS", "XXXX")},
                    { 8, BuildReasonEn("FIREWORKS", "XXXX")},
                    { 9, BuildReasonEn("PYROTECHNICS", "XXXX")},
                    { 10, BuildReasonEn("LASER LGT", "XXXX")},
                    { 11, BuildReasonEn("PARAJUMPS", "WPLW")},
                    { 12, BuildReasonEn("HOT AIR BALLOONS", "WLLW")},
                    { 13, BuildReasonEn("MODEL FLYING", "WZLW")},
                    { 14, BuildReasonEn("MODEL ROCKET", "WZLW")},
                    { 15, BuildReasonEn("FLT INSPECTION", "XXXX")},
                    { 16, BuildReasonEn("SNOWBIRDS ARR SEQUENCE", "WALA")},
                    { 17, BuildReasonEn("VOLCANO ADVISORY", "WWLW")},
                    { 18, BuildReasonEn("AIRSHOW", "WALW")},
                    { 19, BuildReasonEn("GLIDER", "WGLW")},
                    { 20, BuildReasonEn("TETHERED BALLOON", "WCLW")},
                    { 21, BuildReasonEn("_____ BALLOON LAUNCH", "WLLW")},
                    { 22, BuildReasonEn("GAS VENTING", "WSLW")},
                    { 23, BuildReasonEn("UNMANNED AIR VEHICLE (UAV)", "WULW")},
                    { 24, BuildReasonEn("FOREST FIRE", "ROLP")}
                }
            },
            {
                "fr", new Dictionary<int, AreaDefinitionClosureReasons>
                {
                    { 1, BuildReasonFr("HELIDEBARDAGE", "XXXX") },
                    { 2, BuildReasonFr("DEBARDAGE PAR CABLE", "XXXX") },
                    { 3, BuildReasonFr("DYNAMITAGE", "WHLW") },
                    { 4, BuildReasonFr("MIL", "RMCA") },
                    { 5, BuildReasonFr("SAR", "WELW")},
                    { 6, BuildReasonFr("EXER SAR", "WELW")},
                    { 7, BuildReasonFr("PARAMOTEURS", "XXXX")},
                    { 8, BuildReasonFr("FEUX D'ARTIFICE", "XXXX")},
                    { 9, BuildReasonFr("FEUX D'ARTIFICE", "XXXX")},
                    { 10, BuildReasonFr("LUMIERE LASER", "XXXX")},
                    { 11, BuildReasonFr("PARACHUTAGE", "WPLW")},
                    { 12, BuildReasonFr("MONTGOLFIERES", "WLLW")},
                    { 13, BuildReasonFr("AEROMODELISME", "WZLW")},
                    { 14, BuildReasonFr("MODELES REDUITS DE FUSEES", "WZLW")},
                    { 15, BuildReasonFr("INSPECTION EN VOL", "XXXX")},
                    { 16, BuildReasonFr("SEQUENCE D'ARR DES SNOWBIRDS", "WALA")},
                    { 17, BuildReasonFr("AVIS VOLCANIQUE", "WWLW")},
                    { 18, BuildReasonFr("SPECTACLE AERIEN", "WALW")},
                    { 19, BuildReasonFr("DE PLANEURS", "WGLW")},
                    { 20, BuildReasonFr("BALLON CAPTIF", "WCLW")},
                    { 21, BuildReasonFr("LANCEMENT DE BALLON______", "WLLW")},
                    { 22, BuildReasonFr("EVACUATION DE GAZ", "WSLW")},
                    { 23, BuildReasonFr("VEHICULE AERIEN NON HABITE (UAV)", "WULW")},
                    { 24, BuildReasonFr("INCENDIE DE FORET", "ROLP")}
                }

            }            
        };

        private static AreaDefinitionClosureReasons BuildReasonFr(string reason, string qcode) => new AreaDefinitionClosureReasons(reason, qcode);
        private static AreaDefinitionClosureReasons BuildReasonEn(string reason, string qcode) => new AreaDefinitionClosureReasons(reason, qcode);
    }
}
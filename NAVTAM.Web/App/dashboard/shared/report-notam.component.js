"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReportNotamComponent = void 0;
var core_1 = require("@angular/core");
var toastr_service_1 = require("../../common/toastr.service");
var router_1 = require("@angular/router");
var angular_l10n_1 = require("angular-l10n");
var data_service_1 = require("../shared/data.service");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var windowRef_service_1 = require("../../common/windowRef.service");
var data_model_1 = require("../shared/data.model");
var moment = require("moment");
var report_filter_component_1 = require("./report-filter.component");
var ReportNotamComponent = /** @class */ (function () {
    function ReportNotamComponent(toastr, dataService, memStorageService, router, winRef, route, translation) {
        this.toastr = toastr;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.route = route;
        this.translation = translation;
        this.paging = {
            currentPage: 1,
            pageSize: 0,
            totalCount: 0,
            totalPages: 0,
        };
        this.totalPages = 0;
        this.loadingData = true;
        this.isFilterApplied = false;
        this.filterPannelOpen = false;
        this.filterByActive = 'All';
        this.start = null;
        this.end = null;
        this.tableHeaders = {
            sorting: [
                { columnName: 'notamId', direction: 0 },
                { columnName: 'type', direction: 0 },
                { columnName: 'referredNotamId', direction: 0 },
                { columnName: 'series', direction: 0 },
                { columnName: 'itemA', direction: 0 },
                { columnName: 'siteId', direction: 0 },
                { columnName: 'permanent', direction: 0 },
                { columnName: 'estimated', direction: 0 },
                { columnName: 'itemE', direction: 0 },
                { columnName: 'originator', direction: 0 },
                { columnName: 'modifiedByUsr', direction: 0 },
                { columnName: 'startActivity', direction: 0 },
                { columnName: 'endValidity', direction: 0 },
                { columnName: 'received', direction: 0 },
                { columnName: 'published', direction: 1 }
            ],
            itemsPerPage: [
                { id: '10', text: '10' },
                { id: '25', text: '25' },
                { id: '50', text: '50' },
                { id: '100', text: '100' },
                { id: '1000', text: '1000' },
            ]
        };
        this.paging.pageSize = +this.tableHeaders.itemsPerPage[3].id;
        this.queryOptions = {
            page: 1,
            pageSize: this.paging.pageSize,
            sort: "_Published"
        };
    }
    ReportNotamComponent.prototype.ngOnInit = function () {
        var self = this;
        $('.rpt-notam-datepicker').each(function () {
            $(this).bootstrapDatepicker({
                autoclose: false,
                todayHighlight: true,
                orientation: "top",
                startView: 0,
                language: "en",
                forceParse: false,
                daysOfWeekDisabled: "",
                calendarWeeks: false,
                toggleActive: true,
                multidate: false,
            }).on('changeDate', function (e) {
                self.dateChanged(e);
                $(this).bootstrapDatepicker('hide');
            });
        });
        this.isFilterApplied = this.memStorageService.get(this.memStorageService.REPORT_FILTER_QUERY_OPTIONS_KEY);
        if (this.isFilterApplied) {
            this.filter.loadFilterState();
        }
        else {
            var queryOptions = this.memStorageService.get(this.memStorageService.REPORT_NOTAM_QUERY_OPTIONS_KEY);
            if (queryOptions !== null) {
                this.queryOptions = queryOptions;
            }
        }
        if (this.isFilterApplied) {
            this.filter.runReportFilter(false, this.start.toDate(), this.end.toDate(), this.queryOptions.page, this.queryOptions.pageSize);
        }
        else {
            this.getNotams(this.queryOptions.page);
        }
    };
    ReportNotamComponent.prototype.ngAfterViewInit = function () {
        this.filter.parent = this;
    };
    ReportNotamComponent.prototype.itemsPerPageChanged = function (e) {
        this.queryOptions.pageSize = e.value;
        if (this.isFilterApplied) {
            this.loadingData = true;
            this.filter.runFilter(false, 1, this.queryOptions.pageSize);
        }
        else {
            this.getNotams(1);
        }
    };
    ReportNotamComponent.prototype.onFilterChanged = function (event) {
        if (event.error) {
            this.toastr.error("Filter error: " + event.error.message, "", { 'positionClass': 'toast-bottom-right' });
        }
        else {
            this.isFilterApplied = event.isFilterApplied;
            this.filterPannelOpen = false;
            if (this.isFilterApplied) {
                this.processNotams(event.data);
            }
            else {
                this.queryOptions = this.memStorageService.get(this.memStorageService.REPORT_NOTAM_QUERY_OPTIONS_KEY);
                this.getNotams(this.queryOptions.page);
            }
        }
        this.loadingData = false;
    };
    ReportNotamComponent.prototype.onPageChange = function (page) {
        this.getNotams(page);
    };
    ReportNotamComponent.prototype.dateChanged = function (ev) {
        switch (ev.currentTarget.id) {
            case "rpt-notam-startdate":
                this.start = moment(new Date(ev.currentTarget.value));
                if (this.end && this.end.isValid()) {
                    var el = $('#rpt-notam-startdate');
                    if (this.start.isAfter(this.end)) {
                        this.start = null;
                        var x = el.bootstrapDatepicker('getDate');
                        el.bootstrapDatepicker('update', '');
                        el.css({
                            "border-color": "red",
                            "border-width": "1px",
                            "border-style": "solid"
                        });
                    }
                    else {
                        el.css({
                            "border-color": "#E2E2E2",
                            "border-width": "1px",
                        });
                    }
                }
                break;
            case "rpt-notam-enddate":
                this.end = moment(new Date(ev.currentTarget.value));
                if (this.start && this.start.isValid()) {
                    var el = $('#rpt-notam-enddate');
                    if (this.start.isAfter(this.end)) {
                        this.end = null;
                        el.bootstrapDatepicker('update', '');
                        el.css({
                            "border-color": "red",
                            "border-width": "1px",
                            "border-style": "solid"
                        });
                    }
                    else {
                        el.css({
                            "border-color": "#E2E2E2",
                            "border-width": "1px",
                        });
                    }
                }
                break;
        }
        $(this).datepicker('hide');
    };
    ReportNotamComponent.prototype.sort = function (index) {
        var dir = this.tableHeaders.sorting[index].direction;
        for (var i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }
        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            if (this.isFilterApplied) {
                this.filter.changeFilterSort('_' + this.tableHeaders.sorting[index].columnName);
            }
            else {
                this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
            }
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            if (this.isFilterApplied) {
                this.filter.changeFilterSort(this.tableHeaders.sorting[index].columnName);
            }
            else {
                this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
            }
        }
        if (this.isFilterApplied) {
            this.loadingData = true;
            this.filter.runFilter(false, 1);
        }
        else {
            this.getNotams(1);
        }
    };
    ReportNotamComponent.prototype.sortingClass = function (index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';
        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';
        return 'sorting';
    };
    ReportNotamComponent.prototype.onRowSelected = function (index) {
        var notam = this.notams[index];
        if (this.selectedNotam) {
            this.selectedNotam.isSelected = false;
        }
        this.selectedNotam = notam;
        this.selectedNotam.isSelected = true;
    };
    ReportNotamComponent.prototype.isStartHasValue = function () {
        return $('#rpt-notam-startdate').bootstrapDatepicker('getDate') != null;
    };
    ReportNotamComponent.prototype.isEndHaveValue = function () {
        return $('#rpt-notam-enddate').bootstrapDatepicker('getDate') != null;
    };
    ReportNotamComponent.prototype.isRunReportButtonEnabled = function () {
        return this.isStartHasValue() && this.isEndHaveValue();
    };
    ReportNotamComponent.prototype.runReport = function () {
        this.getNotams(1);
    };
    ReportNotamComponent.prototype.exportReport = function () {
        var _this = this;
        var filename = window.prompt(this.translation.translate('Reports.PromptFilename'));
        if (filename) {
            filename = filename.replace('.csv', '') + '.csv';
            this.loadingData = true;
            var payload = {
                Conditions: this.isFilterApplied ? this.filter.filters : null,
                queryOptions: this.queryOptions,
                start: this.start.toDate(),
                end: this.end.toDate()
            };
            this.dataService.exportNofReportNotams(payload)
                .subscribe(function (response) {
                var csvData = new Blob([response], { type: 'text/csv;charset=utf-8;' });
                var csvURL = window.URL.createObjectURL(csvData);
                var tempLink = document.createElement('a');
                tempLink.href = csvURL;
                tempLink.setAttribute('download', filename);
                tempLink.click();
                _this.loadingData = false;
            });
        }
    };
    ReportNotamComponent.prototype.filterByActives = function (status) {
        this.filterByActive = status;
    };
    ReportNotamComponent.prototype.notamList = function () {
        if (this.notams) {
            if (this.filterByActive == 'All') {
                return this.notams;
            }
            if (this.filterByActive == 'Active') {
                var activeNotams = this.notams.filter(function (item) {
                    return !(item.isObsolete);
                });
                return activeNotams;
            }
            if (this.filterByActive == 'Inactive') {
                var activeNotams = this.notams.filter(function (item) {
                    return (item.isObsolete);
                });
                return activeNotams;
            }
        }
    };
    ReportNotamComponent.prototype.getNotams = function (page) {
        var _this = this;
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            if (this.isFilterApplied) {
                this.filter.runReportFilter(false, this.start.toDate(), this.end.toDate(), page, this.queryOptions.pageSize);
            }
            else {
                this.memStorageService.save(this.memStorageService.REPORT_NOTAM_QUERY_OPTIONS_KEY, this.queryOptions);
                this.dataService.getNofReportNotams(this.start.toDate(), this.end.toDate(), this.queryOptions)
                    .subscribe(function (response) {
                    _this.processNotams(response);
                });
            }
        }
        catch (e) {
            this.loadingData = false;
        }
    };
    ReportNotamComponent.prototype.processNotams = function (notams) {
        this.paging = notams.paging;
        this.totalPages = notams.paging.totalPages;
        for (var iter = 0; iter < notams.data.length; iter++) {
            notams.data[iter].index = iter;
            notams.data[iter].typeStr = this.toNotamTypeStr(notams.data[iter].notamType);
            //notams[iter].itemEFull = notams[iter].itemE;
            //notams[iter].itemE = notams[iter].itemE.length > 3 ? notams[iter].itemE.substr(0, 3) + ' ...' : notams[iter].itemE;
        }
        var lastNotamSelected = this.memStorageService.get(this.memStorageService.NOTAM_ID_KEY);
        if (notams.data.length > 0) {
            this.notams = notams.data;
            var index = 0;
            if (lastNotamSelected) {
                index = this.notams.findIndex(function (x) { return x.id === lastNotamSelected; });
                if (index < 0)
                    index = 0; // when not found first should be selected;
            }
            this.notams[index].isSelected = true;
            this.selectedNotam = this.notams[index];
            this.loadingData = false;
            //this.onRowSelected(0);
        }
        else {
            this.notams = [];
            this.selectedNotam = null;
            this.loadingData = false;
        }
    };
    ReportNotamComponent.prototype.toNotamTypeStr = function (notamType) {
        switch (notamType) {
            case data_model_1.ProposalType.New: return 'N';
            case data_model_1.ProposalType.Replace: return 'R';
            case data_model_1.ProposalType.Cancel: return 'C';
            default: return '';
        }
    };
    __decorate([
        core_1.ViewChild(report_filter_component_1.ReportFilterComponent),
        __metadata("design:type", report_filter_component_1.ReportFilterComponent)
    ], ReportNotamComponent.prototype, "filter", void 0);
    __decorate([
        core_1.Input('exportFunc'),
        __metadata("design:type", Object)
    ], ReportNotamComponent.prototype, "exportFunc", void 0);
    ReportNotamComponent = __decorate([
        core_1.Component({
            selector: 'report-notam',
            templateUrl: '/app/dashboard/shared/report-notam.component.html',
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.TranslationService])
    ], ReportNotamComponent);
    return ReportNotamComponent;
}());
exports.ReportNotamComponent = ReportNotamComponent;
//# sourceMappingURL=report-notam.component.js.map
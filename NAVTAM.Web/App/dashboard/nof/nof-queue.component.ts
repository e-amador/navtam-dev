﻿import { Component, OnInit, OnDestroy, OnChanges, Inject, Input, Output, EventEmitter, HostListener } from '@angular/core'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';
import { ActivatedRoute, Router } from '@angular/router'

import { Select2OptionData } from 'ng2-select2';

import { TranslationService } from 'angular-l10n';
import { DataService } from '../shared/data.service'
import { NofQueues } from '../shared/data.model'
import { MemoryStorageService } from '../shared/mem-storage.service'
import { WindowRef } from '../../common/windowRef.service'

import * as moment from 'moment';

import {
    IProposal,
    IQueryOptions,
    INsdCategory,
    IPaging,
    ISorting,
    ITableHeaders,
    ProposalStatus,
    ProposalType,
    NsdCategory } from '../shared/data.model'

declare var $: any;

const syncFrecuency: number = 10; //seconds

@Component({
    selector: 'nof-queue',
    templateUrl: '/app/dashboard/nof/nof-queue.component.html',
    styleUrls: ['app/dashboard/nof/nof-queue.component.css']
})
export class NofQueueComponent implements OnInit, OnDestroy, OnChanges  {
    @Input('name') queueName: string;
    @Input('count') queueCount: number;

    proposals : IProposal[];  
    proposalHistory : IProposal[];  
    queryOptions : IQueryOptions;
    selectedProposal: IProposal;
    paging : IPaging = {
        currentPage: 1,
        pageSize: 0,
        totalCount: 0,
        totalPages: 0,
    };

    totalPages : number= 0;

    currentCategoryId: number;
    currentSubCategoryId: number;
    categoriesData: Array<any>;
    categoryStartValue: string;
    subCategoryStartValue: string;

    parentCategories: Array<any> = [];
    childCategories: Array<any> = [];

    lastSyncTime?: moment.Moment = null;

    itemsPerPageData: Array<any>;
    loadingData: boolean = false;
    tableHeaders: ITableHeaders; 
    queryStoreName: string;

    frsPrevKeyCodeEvent: any;
    sndPrevKeyCodeEvent: any;

    handler: any = null;

    intervalHandler = null;
    expiringCount: number = 0;
    runExpiringBellAnimation: boolean = false;
    expiringApplied: boolean = false;
    isLeavingForm: boolean = false;


    alarms = [
        { id: 0, name: 'Bell Default', ext: 'wav' },
        { id: 1, name: 'Chime Default', ext: 'wav' },
        { id: 2, name: 'Bell Sound', ext: 'mp3' },
        { id: 3, name: 'Police Warning', ext: 'mp3' },
        { id: 4, name: 'Burglary Alarm', ext: 'mp3' },
        { id: 5, name: 'Security Alarm', ext: 'mp3' },
    ];

    currentEstimatedQueueSound: any;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private dataService: DataService, 
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private translation: TranslationService,
        private route: ActivatedRoute) {

        this.tableHeaders = {
            sorting:
                [
                    {columnName: 'NotamId', direction: 0},
                    {columnName: 'CategoryId', direction: 0},
                    {columnName: 'ItemA', direction: 0},
                    {columnName: 'SiteID', direction: 0 },
                    {columnName: 'ItemE', direction: 0 },
                    {columnName: 'StartActivity', direction: 0 },
                    {columnName: 'EndValidity', direction: 0 },
                    {columnName: 'Estimated', direction: 0 },
                    {columnName: 'Received', direction: 1 },
                    {columnName: 'Operator', direction: 0 },
                    {columnName: 'Originator', direction: 0 },
                    {columnName: 'ModifiedByUsr', direction: 0 },
                    {columnName: 'Status', direction: 0}
                ],
            itemsPerPage: 
                [
                    { id:'10' , text: '10' }, 
                    { id:'25', text: '25' },                
                    { id:'50', text: '50' },
                    { id: '100', text: '100' },
                    { id: '1000', text: '1000' }                
                ]
        };
        this.paging.pageSize = +this.tableHeaders.itemsPerPage[2].id;
        this.queryOptions = {
            page: 1,
            pageSize: this.paging.pageSize,
            sort: this.tableHeaders.sorting[8].columnName //Default Received Ascending
        }
    }

    ngOnInit() {
        //If we want to change the default, we need to work here
        this.currentEstimatedQueueSound = this.alarms[0];

        this.categoriesData = this.route.snapshot.data['categories'];
        this.addAllToComboFilter(this.parentCategories);
        for (let i = 0; i < this.categoriesData.length; i++) {
            const category = this.categoriesData[i];
            this.addItemToComboFilter(this.parentCategories, category);
        }

        this.categoryStartValue = this.categoriesData[0].id;
        this.currentCategoryId = +this.categoryStartValue;

        //Set latest combo box status 
        let catId = this.memStorageService.get(this.memStorageService.CATEGORY_ID_KEY);
        let subCatId = this.memStorageService.get(this.memStorageService.SUB_CATEGORY_ID_KEY);

        this.categoryStartValue = catId ? catId : this.parentCategories[0].id;
        this.currentCategoryId = +this.categoryStartValue;

        if (subCatId) {
            this.childCategories = this.addChildCategoryToComboFilter(this.childCategories, this.currentCategoryId);
            this.currentSubCategoryId = subCatId;
            this.subCategoryStartValue = subCatId;
        }

        //Set latest query options
        this.queryStoreName = this.queueName + "QUERY_OPTIONS_KEY";
        const queryOptions = this.memStorageService.get(this.queryStoreName);
        if( queryOptions ) {
            this.queryOptions = queryOptions;
        }

        this.getProposalsByCategory(this.queryOptions.page);
        if (this.queueName === NofQueues.Pending) {
           this.getParkingQueueStats();
        } else {
           this.getPendingQueueStats();
        }

        this.checkProposalStatus();
        this.getExpiringQueueStats();
        this.checkExpireCounter();
    }

    ngOnDestroy() {
        if (this.handler) {
            clearInterval(this.handler);
        }
        if (this.intervalHandler) {
            window.clearInterval(this.intervalHandler);
        }
    }

    ngOnChanges() {
        switch (this.queueName) {
            case NofQueues.Pending:
                this.getPendingQueueStats();
                break;
            case NofQueues.Park:
                this.getParkingQueueStats();
                break;
        }
    }

    getItemEString(proposal: IProposal, history: boolean): string {
        const maxsize = history ? 90 : 110;
        var result = proposal.itemE.substring(0, maxsize);
        if (proposal.itemE.length > maxsize) result += '...';
        return result;
    }

    getOriginatorString(proposal: IProposal, history: boolean): string {
        const maxsize = history ? 10 : 10;
        var result = proposal.originator.substring(0, maxsize);
        if (proposal.originator.length > maxsize) result += '...';
        return result;
    }

    getOperatorString(proposal: IProposal): string {
        if (proposal.modifiedByUsr === null || proposal.modifiedByUsr.length === 0) return proposal.operator;
        else return proposal.modifiedByUsr;
    }

    getApproverString(proposal: IProposal): string {
        if (proposal.status === ProposalStatus.Disseminated ||
            proposal.status === ProposalStatus.DisseminatedModified ||
            proposal.status === ProposalStatus.Rejected) return proposal.modifiedByUsr;
        else return '';
    }

    getApproverStringHistory(proposal: IProposal): string {
        if (proposal.status === ProposalStatus.Disseminated ||
            proposal.status === ProposalStatus.DisseminatedModified ||
            proposal.status === ProposalStatus.Rejected) return proposal.modifiedByUsr;
        else return '';
    }

    private checkExpireCounter() {
        const self = this;
        this.intervalHandler = window.setInterval(function () {
            self.getExpiringQueueStats.call(self);
        }, syncFrecuency * 1000);
    }

    getExpiringQueueStats() {
        try {
            //Run it just if the user is on the Review Queue
            if (this.queueName === 'REVIEW_QUEUE') {
                this.dataService.getExpiringTotal()
                    .subscribe((count: any) => {
                        const self = this;
                        let audioElem: any;
                        let shouldPlaySound = false;

                        if (count != this.expiringCount) {
                            if (count > this.expiringCount) {
                                shouldPlaySound = true;
                                audioElem = document.getElementById('estimated-queue-sound');
                                audioElem.load();
                                audioElem.play();
                            }

                            this.expiringCount = count;

                            self.runExpiringBellAnimation = true;
                            setTimeout(function () {
                                self.runExpiringBellAnimation = false;
                                if (shouldPlaySound) {
                                    audioElem.pause();
                                    audioElem.currentTime = 0;
                                }
                            }, 10000);

                            if (this.expiringApplied)
                                this.getProposalsByCategory(this.queryOptions.page);
                        }
                        //Here we need to hide the button and switch to the normal query
                        if (this.expiringCount === 0 && this.expiringApplied) {
                            this.executeFilter();
                        }
                    });
            }
        }
        catch (e) {
        }
    }

    executeFilter(): void {
        this.expiringApplied = !this.expiringApplied;
        if (this.expiringApplied) {
            this.getProposalsByCategory(this.queryOptions.page);
        } else {
            this.changeCategory(this.parentCategories[0].id);
        }
    }

    runCloneAction(): void {
        if (this.selectedProposal) {
            this.isLeavingForm = true;
            this.router.navigate(["/dashboard/clone", this.selectedProposal.id, false]);
        }
    }

    @HostListener('window:keyup', ['$event'])
    onKeyup($event: any) {
        //this seems like it will be messy in the future
        switch ($event.code) {
            case "ArrowUp"://NAVIGATE TABLE ROW UP
                if (this.selectedProposal.index > 0) {
                    this.selectProposal(this.proposals[this.selectedProposal.index - 1]);
                }
                break;
            case "ArrowDown": //NAVIGATE TABLE ROW DOWN
                if (this.selectedProposal.index < this.proposals.length - 1) {
                    this.selectProposal(this.proposals[this.selectedProposal.index + 1]);
                }
                break;
            case "KeyV"://ALT-V: REVIEW
                if ($event.altKey && this.selectedProposal) {
                    this.router.navigate(["/dashboard/view", this.selectedProposal.id]);
                }
                break;
            case "Enter": //ENTER: PICK
                if (this.selectedProposal && !this.isPickedButtonDisabled()) {
                    this.router.navigate(["/dashboard/pick", this.selectedProposal.id]);
                }
                break;
            case "KeyP": //ALT-P: PICK
                if ($event.altKey) {
                    if (this.selectedProposal && !this.isPickedButtonDisabled()) {
                        this.router.navigate(["/dashboard/pick", this.selectedProposal.id]);
                    }
                }
                break;
            case "KeyH"://ALT-H: HISTORY
                if ($event.altKey && this.selectedProposal && !this.isHistoryButtonDisabled()) {
                    this.showHistory(this.selectedProposal.id);
                }
                break;
            case "KeyZ"://ALT-Z: DISCARD
                if ($event.altKey && this.selectedProposal) {
                    if (!this.isDiscardButtonHidden(this.selectedProposal.status)) {
                        this.confirmDiscardProposal();
                    }
                }
                break;
            case "KeyC":
                if ($event.altKey && this.selectedProposal) {//ALT-C CLONE PROPOSAL
                    this.router.navigate(["/dashboard/clone", this.selectedProposal.id, false]);
                } else { //CTRL-C START NOTAM CREATION
                    if ($event.ctrlKey) {
                        this.frsPrevKeyCodeEvent = $event;
                    } else {
                        if (this.frsPrevKeyCodeEvent) { //CTRL-C C: CATCHALL
                            this.currentCategoryId = NsdCategory.CatchAll;
                            this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                        }
                    }
                }
                break;
            case "KeyO": //CTRL-C O: OBSTACLE
                if (this.frsPrevKeyCodeEvent) {
                    this.sndPrevKeyCodeEvent = $event;
                }
                break;
            case "KeyR": //CTRL-C R: RUNWAY
                if (this.frsPrevKeyCodeEvent) {
                    this.sndPrevKeyCodeEvent = $event;
                }
                break;
            case "KeyT": // CTRL-C T: Taxiway
                if (this.frsPrevKeyCodeEvent) {
                    this.sndPrevKeyCodeEvent = $event;
                }
                break;
            case "KeyA": // CTRL-C T: 
                if (this.frsPrevKeyCodeEvent) {
                    this.sndPrevKeyCodeEvent = $event;
                }
                break;
            case "Digit1":
                //CTRL-C O 1: OBST
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyO") {
                    this.currentCategoryId = NsdCategory.Obst;
                    this.currentSubCategoryId = 4;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                // CTRL-C R 1: RWY CLOSED
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyR") {
                    this.currentCategoryId = NsdCategory.RwyClsd;
                    this.currentSubCategoryId = 7;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                //CTRL-C T 1: TWY CLOSED
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyT") {
                    debugger;
                    this.currentCategoryId = NsdCategory.TwyClsd;
                    this.currentSubCategoryId = 11;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                //CTRL-C A 1: CARS CLOSED
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyA") {
                    this.currentCategoryId = NsdCategory.CarsClsd;
                    this.currentSubCategoryId = 13;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                break;
            case "Digit2":
                //CTRL-C O 2: OBST LGT U/S
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyO") {
                    this.currentCategoryId = NsdCategory.ObstLGT;
                    this.currentSubCategoryId = 5;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                break;
            case "Digit3":
                //CTRL-C O 3: MULTI OBST
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyO") {
                    this.currentCategoryId = NsdCategory.MultiObst;
                    this.currentSubCategoryId = 14;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                break;
            case "Digit4":
                //CTRL-C O 3: MULTI OBST LGT U/S
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyO") {
                    this.currentCategoryId = NsdCategory.MultiObstLGT;
                    this.currentSubCategoryId = 15;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                break;
        }
    }

    onCategoryChanged(e: any): void {
        this.changeCategory(e.value);
    }

    onChildCategoryChanged(e: any): void {
        this.changeChildCategory(e.value);
    }

    sort(index) {
        const dir = this.tableHeaders.sorting[index].direction;

        for (let i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }

        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
        }

        this.getProposalsByCategory(this.queryOptions.page);
    }

    sortingClass(index) {
        if( this.tableHeaders.sorting[index].direction > 0 )
            return 'sorting sorting_asc';

        if( this.tableHeaders.sorting[index].direction < 0 )
            return 'sorting sorting_desc';

        return 'sorting';
    }

    confirmDiscardProposal() {
        $('#modal-delete').modal({});
    }

    discardProposal() {
        if (this.selectedProposal) {
            this.isLeavingForm = true;
            this.dataService.discardNofProposal(this.selectedProposal.id)
                .subscribe(response => {
                    this.getProposalsByCategory(this.queryOptions.page);
                    this.toastr.success("Proposal successfully discarded!", "", { 'positionClass': 'toast-bottom-right' });
                }, error => {
                    this.isLeavingForm = false;
                    this.toastr.error("Discard proposal error: " + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        }
    }

    resolveCategoryId() {
        if (this.childCategories.length > 0) {
            return this.currentSubCategoryId !== NsdCategory.All ? this.currentSubCategoryId : this.currentCategoryId;
        }

        return this.currentCategoryId;
    }

    isCreateButtonDisable() {
        return this.childCategories.length > 0 ? this.currentSubCategoryId === NsdCategory.All : this.currentCategoryId === NsdCategory.All;
    }

    cssIcon(proposal) {
        switch (proposal.status) {
            case ProposalStatus.Draft: return 'fa-save';
            case ProposalStatus.Submitted: return 'fa-upload';
            case ProposalStatus.Withdrawn: return 'fa-download';
            case ProposalStatus.Picked: return 'fa-hourglass-half';
            case ProposalStatus.Expired: return 'fa-clock-o';
            case ProposalStatus.Replaced: return 'fa-code-fork';
            case ProposalStatus.Parked: return 'fa-lock';
            case ProposalStatus.ParkedDraft: return 'fa-lock';
            case ProposalStatus.ParkedPicked: return 'fa-lock';
            case ProposalStatus.Disseminated: return 'fa-wifi';
            case ProposalStatus.DisseminatedModified: return 'fa-wifi';
            case ProposalStatus.Cancelled: return 'fa-hand-paper-o';
            case ProposalStatus.Rejected: return 'fa-ban';
            case ProposalStatus.Discarded: return 'fa-trash';
            case ProposalStatus.Terminated: return 'fa-hand-paper-o';
        }
    }

    rowColor(proposal) {
        if (proposal.grouped) {
            return "bg-rejected";
        }

        if (proposal.soonToExpire && this.queueName === 'REVIEW_QUEUE') {
            return 'bg-soontoexpire';
        }

        switch (proposal.status) {
            case ProposalStatus.Draft: return 'bg-saved';
            case ProposalStatus.Submitted: return '';
            case ProposalStatus.Withdrawn: return 'bg-widthdrawn';
            case ProposalStatus.Picked: return 'bg-picked';
            case ProposalStatus.Parked: return 'bg-picked';
            case ProposalStatus.ParkedDraft: return 'bg-picked';
            case ProposalStatus.ParkedPicked: return 'bg-picked';
            case ProposalStatus.Replaced: return 'bg-replace';
            case ProposalStatus.Rejected: return 'bg-rejected fg-white';
            case ProposalStatus.Expired: return 'bg-expired';
            case ProposalStatus.Cancelled: return 'bg-cancel';
            case ProposalStatus.Terminated: return 'bg-cancel';
            case ProposalStatus.DisseminatedModified: return 'c-orange';
            default: return '';
        }
    }

    isDiscardButtonHidden(proposal) {
        if (proposal) {
            return proposal.status === ProposalStatus.Picked ||
                proposal.status === ProposalStatus.Parked ||
                proposal.status === ProposalStatus.ParkedDraft ||
                proposal.status === ProposalStatus.ParkedPicked;
        }

        return false;
    }

    isReviewButtonDissabled() {
        if (this.isLeavingForm) {
            return true;
        }
        if (!this.selectedProposal) {
            return true;
        }

        if (this.selectedProposal.seriesChecklist) {
            return true;
        }

        return false;
    }

    isCloneButtonDisabled() {
        if (this.isLeavingForm) {
            return true;
        }
        if (!this.selectedProposal) {
            return true;
        }
        if (this.selectedProposal.seriesChecklist) {
            return true;
        }
        if (this.selectedProposal.proposalType === ProposalType.Cancel) {
            return true;
        }

        return false;
    }

    isReplaceButtonDisabled() {
        if (this.isLeavingForm) {
            return true;
        }
        if (!this.selectedProposal) {
            return true;
        }
        if (this.selectedProposal.seriesChecklist) {
            return true;
        }
        if (this.selectedProposal.grouped && !this.selectedProposal.groupId) {
            return true;
        }
        if (this.selectedProposal.isObsolete) return true;

        return this.selectedProposal.status === ProposalStatus.Terminated ||
            (this.selectedProposal.status !== ProposalStatus.Disseminated && this.selectedProposal.status !== ProposalStatus.DisseminatedModified);
    }

    isCancelButtonDisabled() {
        if (this.isLeavingForm) {
            return true;
        }
        if (!this.selectedProposal) {
            return true;
        }
        if (this.selectedProposal.seriesChecklist) {
            return true;
        }
        if (this.selectedProposal.proposalType === ProposalType.Cancel || this.selectedProposal.proposalType === ProposalStatus.Expired) {
            return true;
        }
        if (this.selectedProposal.isObsolete) return true;

        return this.selectedProposal.status === ProposalStatus.Terminated ||
            (this.selectedProposal.status !== ProposalStatus.Disseminated && this.selectedProposal.status !== ProposalStatus.DisseminatedModified);
    }

    itemsPerPageChanged(e: any): void {
        this.queryOptions.pageSize = e.value;
        this.getProposalsByCategory(NsdCategory.All);
    }

    selectProposal(proposal) {
        if( this.selectedProposal ) {
            this.selectedProposal.isSelected = false;
        }
        this.selectedProposal = proposal
        this.selectedProposal.isSelected = true;
        this.memStorageService.save(this.memStorageService.PROPOSAL_ID_KEY, this.selectedProposal.id);
    }

    showGroup(rowIndex: number) {
        let pos = rowIndex + 1;
        if (this.proposals[rowIndex].groupOpen) {
            if (this.proposals[rowIndex].group.length > 0) {
                this.proposals.splice(pos, this.proposals[rowIndex].group.length);
            }
            this.proposals[rowIndex].groupOpen = false;
        } else {
            this.proposals[rowIndex].groupOpen = true;
        }

        this.proposals[rowIndex].group = [];
        if (this.proposals[rowIndex].groupOpen) {
            this.dataService.getNofGroupedProposals(this.proposals[rowIndex].groupId, this.queueName)
                .subscribe((proposals: IProposal[]) => {

                    if (proposals.length > 0) {
                        for (let i = 0; i < proposals.length; i++) {
                            proposals[i].isSelected = false;
                            proposals[i].grouped = true;
                            proposals[i].groupId = null;
                            this.proposals[rowIndex].group.push(proposals[i]);
                        }

                        let pos = rowIndex + 1;
                        if (this.proposals[rowIndex].group) {
                            let isLastRow = this.proposals.length === pos;
                            for (let i = 0; i < this.proposals[rowIndex].group.length; i++) {
                                let pg = this.proposals[rowIndex].group[i];
                                if (isLastRow) {
                                    this.proposals.push(pg);
                                } else {
                                    this.proposals.splice(pos + i, 0, pg);
                                }
                            }
                        }
                    }
                });
        } 
    }

    changeRoute(routeValue) {
        this.isLeavingForm = true;
        this.router.navigate(routeValue).catch(() => {
            this.isLeavingForm = false;
            this.toastr.error("This action is not allowed at this time.", "Action failed!", { 'positionClass': 'toast-bottom-right' });
        }); 
    }

    pickProposal(proposal: IProposal) {
        //changeRoute(['/dashboard/pick', selectedProposal?.id])
        this.selectProposal(proposal);
        if (!this.isPickedButtonDisabled()) {
            if (proposal.categoryId === 18) //RSC
            {
                this.router.navigate(["/dashboard/pick/rsc", this.selectedProposal.id]);
            }
            else {
                this.router.navigate(["/dashboard/pick", this.selectedProposal.id]);
            }
        }
    }

    onPageChange(page: number) {
        this.getProposalsByCategory(page);
    }

    showHistory(proposalId : number) {
        $('#proposal-history').modal({});
            this.dataService.getProposalHistory(proposalId)
                .subscribe((proposals: IProposal[] ) => {
                    if (proposals.length > 0) {
                        this.proposalHistory = proposals;
                        this.proposalHistory[0].isSelected = true;
                    } else {
                        this.proposalHistory = [];
                    }
                });
    }

    isPickedButtonDisabled() {
        if (this.isLeavingForm) {
            return true;
        }

        if (!this.selectedProposal) {
            return true;
        }
        if (this.selectedProposal.seriesChecklist) {
            return true;
        }

        return this.selectedProposal.status === ProposalStatus.Disseminated;
    }

    isHistoryButtonDisabled() {
        if (!this.selectedProposal) {
            return true;
        }

        if (this.selectedProposal.seriesChecklist) {
            return true;
        }

        return this.selectedProposal.status === ProposalStatus.Draft;
    }

    isNSDAllowed(catName: string): boolean {
        var id = this.categoriesData.findIndex(x => x.name === catName);
        if (id === -1) return false;
        return true;
    }

    private changeCategory(catId: string): void {
        this.currentCategoryId = +catId;
        this.childCategories = this.addChildCategoryToComboFilter(this.childCategories, this.currentCategoryId);
        const queryOptions = this.memStorageService.get(this.queryStoreName);

        this.memStorageService.save(this.memStorageService.CATEGORY_ID_KEY, this.currentCategoryId);
        this.memStorageService.save(this.memStorageService.SUB_CATEGORY_ID_KEY, NsdCategory.All); //set all by default

        this.getProposalsByCategory(NsdCategory.All);
    }

    private changeChildCategory(subCatId: string): void {
        this.currentSubCategoryId = +subCatId;
        this.memStorageService.save(this.memStorageService.SUB_CATEGORY_ID_KEY, this.currentSubCategoryId);

        this.getProposalsByCategory(NsdCategory.All);
    }

    private getProposalsByCategory(page) {
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            this.memStorageService.save(this.queryStoreName, this.queryOptions);
            if (this.childCategories.length > 0) {
                if (this.currentSubCategoryId !== NsdCategory.All) {
                    this.dataService.getNofProposalsByQueueName(this.currentSubCategoryId, this.queueName, this.queryOptions)
                        .subscribe((proposals: any) => {
                            this.processProposals(proposals);
                        });
                } else {
                    this.dataService.getNOfProposalsByParentCategoryAndQueueName(this.currentCategoryId, this.queueName, this.queryOptions)
                        .subscribe((proposals: any) => {
                            this.processProposals(proposals);
                        });
                }
            } else if (this.expiringApplied) {
                this.dataService.getExpiringProposals(this.queryOptions)
                    .subscribe((proposals: any) => {
                        this.processProposals(proposals);
                    });
            } else {
                this.dataService.getNofProposalsByQueueName(this.currentCategoryId, this.queueName, this.queryOptions)
                    .subscribe((proposals: any) => {
                        this.processProposals(proposals);
                    });
            }
        }
        catch (e) {
            this.loadingData = false;
        }
    }

    private processProposals(proposals: any) {
        this.paging = proposals.paging;
        this.totalPages = proposals.paging.totalPages;

        for (var iter = 0; iter < proposals.data.length; iter++) proposals.data[iter].index = iter;

        const lastProposalSelected = this.memStorageService.get(this.memStorageService.PROPOSAL_ID_KEY);
        if (proposals.data.length > 0) {

            this.proposals = proposals.data;
            let index = 0;
            if (lastProposalSelected) {
                index = this.proposals.findIndex(x => x.id === lastProposalSelected);
                if (index < 0) index = 0; // when not found first should be selected;
            }
            this.proposals[index].isSelected = true;
            this.selectedProposal = this.proposals[index];
            this.loadingData = false;

        } else {
            this.proposals = [];
            this.selectedProposal = null;
            this.loadingData = false;
        }
    }

    private checkProposalStatus() {
        let self = this;
        this.handler = window.setInterval(function () {
            if (self.proposals !== undefined) {
                if (self.proposals.length > 0) {
                    let proposalsIds = self.proposals
                        .map(function (p: IProposal) {
                            return p.id;
                        });
                    self.dataService.getProposalStatus(proposalsIds, syncFrecuency, self.lastSyncTime)
                        .subscribe((response: any) => {
                            var proposalStatus = response.proposalStatus;
                            self.lastSyncTime = response.lastSyncTime;
                            for (var p of proposalStatus) {
                                self.updateProposalStatus(p);
                            }
                        });
                }
            }
        }, syncFrecuency * 1000);
    }

    private updateProposalStatus(data: any) {
        if (data) {
            const index = this.proposals.findIndex(x => x.id === data.id);
            if (index >= 0) {
                if (data.status === ProposalStatus.Deleted || data.status === ProposalStatus.Discarded ||
                    data.status === ProposalStatus.Expired || data.status === ProposalStatus.Terminated ||
                    data.status === ProposalStatus.Withdrawn) {
                    this.deleteRow(index);
                }
                else {
                    const $rowElem = $("#p_" + data.id);

                    let iniBgColorClass = this.rowColor(this.proposals[index]);

                    this.proposals[index].notamId = data.notamId;
                    if (data.startActivity) {
                        this.proposals[index].startActivity = data.startActivity;
                    }
                    this.proposals[index].endValidity = data.endValidity; //clean endValidity when cancellation...
                    this.proposals[index].status = data.status;
                    this.proposals[index].soonToExpire = data.soonToExpire;
                    this.proposals[index].modifiedByUsr = data.modifiedByUsr;
                    this.proposals[index].originator = data.originator;
                    this.proposals[index].operator = data.operator;
                    this.proposals[index].itemE = data.itemE;
                    this.proposals[index].itemA = data.itemA;
                    this.proposals[index].siteId = data.siteId;
                    this.proposals[index].estimated = data.estimated;

                    let endBgColorClass = this.rowColor(this.proposals[index]);

                    if (iniBgColorClass !== endBgColorClass) {
                        $rowElem.removeClass(iniBgColorClass, { duration: 500 });
                    }

                    if (iniBgColorClass !== endBgColorClass) {
                        $rowElem.addClass(endBgColorClass, { duration: 500 });
                    }
                }
            }
        }
    } 

    private deleteRow(index: number) {
        this.proposals.splice(index, 1);
        const selectedIndex = this.proposals.findIndex(x => x.isSelected);
        if (this.proposals.length > 0) {
            this.selectedProposal = this.proposals[0];
            if (selectedIndex >= 0 && this.selectedProposal.id !== selectedIndex) {
                this.proposals[selectedIndex].isSelected = false;
            }
            this.selectedProposal.isSelected = true;
        }
    }

    private addItemToComboFilter(comboData: Array<any>, category: INsdCategory) {
        comboData.push({
            groupCategoryId: category.groupCategoryId,
            id: category.id,
            isSelected: category.isSelected,
            name: category.name,
            text: category.name
        });
    }

    private addChildCategoryToComboFilter(comboData: Array<any>, currentCategoryId: number) {
        comboData = [];
        const categoryIndex = this.categoriesData.findIndex(x => x.id === currentCategoryId);
        if (categoryIndex >= 0) {
            if (this.categoriesData[categoryIndex].children) {
                this.addAllToComboFilter(comboData);
                for (let i = 0; i < this.categoriesData[categoryIndex].children.length; i++) {
                    const subCategory = this.categoriesData[categoryIndex].children[i];
                    this.addItemToComboFilter(comboData, subCategory);
                }
            }
        }
        this.currentSubCategoryId = NsdCategory.All;

        return comboData;
    }

    private addAllToComboFilter(comboData: Array<any>) {
        comboData.push({
            groupCategoryId: 0,
            id: 1,
            isSelected: true,
            name: 'Root',
            text: this.translation.translate('Dashboard.MessageSaveAttachment1')
        });
    }

    getPendingQueueStats() {
        if (this.proposals) {
            var proposalsWithGroupOpen = this.proposals.filter(x => x.groupOpen);
            var childrenProposals = 0;
            for (var p of proposalsWithGroupOpen) {
                childrenProposals += p["group"].length;
            }
            if (this.proposals.length - childrenProposals !== this.queueCount) {
                this.getLatestProposals(this.queueCount);
            }
        }
    }

    getParkingQueueStats() {
        if (this.proposals) {
            var proposalsWithGroupOpen = this.proposals.filter(x => x.groupOpen);
            var childrenProposals = 0;
            for (var p of proposalsWithGroupOpen) {
                childrenProposals += p["group"].length;
            }
            if (this.proposals.length - childrenProposals !== this.queueCount) {
                this.getLatestProposals(this.queueCount);
            }
        }
    }

    private getLatestProposals(totalCount : number) {
        try {
            if (this.childCategories.length > 0) {
                if (this.currentSubCategoryId !== NsdCategory.All) {
                    this.dataService.getNofProposalsByQueueName(this.currentSubCategoryId, this.queueName, this.queryOptions)
                        .subscribe((proposals: any) => {
                            this.refreshProposals(proposals, totalCount);
                        });
                } else {
                    this.dataService.getNOfProposalsByParentCategoryAndQueueName(this.currentCategoryId, this.queueName, this.queryOptions)
                        .subscribe((proposals: any) => {
                            this.refreshProposals(proposals, totalCount);
                        });
                }
            } else {
                this.dataService.getNofProposalsByQueueName(this.currentCategoryId, this.queueName, this.queryOptions)
                    .subscribe((proposals: any) => {
                        this.refreshProposals(proposals, totalCount);
                    });
            }
        }
        catch (e) {
        }
    }

    private refreshProposals(proposals: any, queueCount: number) {
        if (proposals) {
            //let difference = proposals.data.filter(x => !this.proposals.find(y => y.id === x.id));
            //for (let d of difference) {
            //    const index = proposals.data.findIndex(x => x.id === d.id);
            //    if (index > 0) {
            //        proposals.data.splice(index, 1);
            //        proposals.data.unshift(d);
            //    }
            //}

            for (var iter = 0; iter < proposals.data.length; iter++) {
                proposals.data[iter].isSelected = false;
                proposals.data[iter].index = iter;
            }

            if (proposals.data.length > 0) {
                const currentSelectedId = this.selectedProposal ? this.selectedProposal.id : -1;
                const lastProposalSelected = proposals.data.find(x => x.id == currentSelectedId);
                if (lastProposalSelected) {
                    this.selectedProposal = lastProposalSelected;
                    this.selectedProposal.isSelected = true;
                } else {
                    this.selectedProposal = proposals.data[0];
                    this.selectedProposal.isSelected = true;
                }
                this.proposals = proposals.data;
            } else {
                this.proposals = [];
            }
        }
    } 

    private animateIncomingProposals( newProposals ) {
        if (newProposals) {
            window.setTimeout(function () {
                for (let p of newProposals) {
                    const rowElem = $("#data-response #p_" + p.id);
                    if (rowElem.length) {
                        rowElem.addClass('bg-received', 250, 'linear', function (elm, classes) {
                            window.setTimeout(function () {
                                elm.removeClass('bg-received');
                            }, 10000);
                        }(rowElem));
                    }
                }
            }, 500);
        }
    }
}

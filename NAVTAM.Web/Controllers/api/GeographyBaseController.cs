﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using System.Threading.Tasks;
using Core.Common.Geography;
using NavCanada.Core.Domain.Model.Enums;
using System.IO;
using System.Data.Entity.Spatial;
using NAVTAM.ViewModels;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    public class GeographyBaseController : ApiBaseController
    {
        /// <summary>
        /// This is the aproximate area of the NOF DOA, equivalent to Canada aerospace area
        /// </summary>
        const double CanadaArea = 17939288765826.473;

        public GeographyBaseController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger) : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
        }

        protected async Task<StepResult<string>> ValidateAndConvertToPoint(string value)
        {
            if (string.IsNullOrEmpty(value))
                return StepResult<string>.Fail(string.Empty);

            var locationResult = await TryGetAerodromeLocation(value, Uow);
            if (locationResult.Succeeded)
                return locationResult;

            var cleanValue = DMSLocation.CleanValidLocationSeparators(value);

            return DMSLocation.IsKnownFormat(value) ? StepResult<string>.Succeed(cleanValue) : StepResult<string>.Fail(string.Empty);
        }

        protected StepResult<string> ValidateAndConvertToPolygon(string points)
        {
            // parse the coordinates
            var coordinates = (points ?? "")
                .Split(',')
                .Select(l => l.Trim())
                .ToList();

            // if the last coordinate is the same as the first then we count one less
            var count = coordinates[0].Equals(coordinates[coordinates.Count - 1]) ? coordinates.Count - 1 : coordinates.Count;

            // must be at least 3 differente points and all are valid decimal locations
            if (count >= 3 && coordinates.All(l => DMSLocation.IsValidDecimal(l)))
            {
                // validate the polygon has a non-empty area
                if (ValidPolygonArea(coordinates))
                    return StepResult<string>.Succeed(string.Join(", ", coordinates));
            }

            return StepResult<string>.Fail(string.Empty);
        }

        protected StepResult<string> ValidateUploadedGeographyFile()
        {
            var request = HttpContext.Current.Request;

            if (request.Files.Count == 0)
                return StepResult<string>.Fail(Resources.ErrorFileNotFound);

            var file = request.Files[0];
            if (file.ContentLength > 52428800)
                return StepResult<string>.Fail(Resources.ErrorFileTooBig);

            var fileContent = GetGeographyFileContent(file);
            var validateResult = ValidateGeography(fileContent);

            //if (validateResult.Failed)
            //    return StepResult<string>.Fail(validateResult.ErrorMessage);

            return validateResult;
        }

        protected IHttpActionResult ConvertToGeoFeatures(RegionMapRenderViewModel model)
        {
            var geoResult = CreateRegionGeography(model.Region, Uow);
            if (geoResult.Failed)
                return BadRequest(geoResult.ErrorMessage);

            var geo = ModifyDoaGeography(geoResult.Result, model.Changes);
            var featureGeo = ConvertToGeoJson(geo);

            return Ok(featureGeo);
        }

        protected static async Task<StepResult<string>> TryGetAerodromeLocation(string designator, IUow uow)
        {
            if (designator.Length == 4 && designator.All(c => char.IsLetterOrDigit(c)))
            {
                var ahp = await uow.SdoCacheRepo.GetByDesignatorAsync(designator);
                if (ahp != null && ahp.Type == SdoEntityType.Aerodrome)
                {
                    var lat = ahp.RefPoint?.Latitude ?? 0.0;
                    var lng = ahp.RefPoint?.Longitude ?? 0.0;
                    return StepResult<string>.Succeed($"{lat} {lng}");
                }
            }
            return StepResult<string>.Fail(string.Empty);
        }

        protected static StepResult<DbGeography> CreateRegionGeography(RegionViewModel region, IUow uow)
        {
            try
            {
                switch (region.Source)
                {
                    case RegionSource.Geography:
                        return StepResult<DbGeography>.Succeed(ParseGeography(region.Geography));
                    case RegionSource.FIR:
                        return StepResult<DbGeography>.Succeed(GetFirGeography(region.Fir, uow));
                    case RegionSource.Points:
                        return StepResult<DbGeography>.Succeed(GetPolygonGeography(region.Points));
                    case RegionSource.PointAndRadius:
                        return StepResult<DbGeography>.Succeed(GetPointAndRadiusGeography(region.Location, region.Radius));
                    default:
                        return StepResult<DbGeography>.Fail(Resources.DoaGeographyNotImplemented);
                }
            }
            catch (Exception ex)
            {
                return StepResult<DbGeography>.Fail(ex.Message);
            }
        }

        protected static DbGeography ModifyDoaGeography(DbGeography geo, List<DoaRegionChange> changes)
        {
            if (changes != null)
            {
                foreach (var p in changes.Where(c => c.ChangeType == DoaRegionChangeType.Add))
                {
                    var area = GetPointAndRadiusGeography(p.Location, p.Radius);
                    geo = geo.Union(area);
                }
                foreach (var p in changes.Where(c => c.ChangeType == DoaRegionChangeType.Remove))
                {
                    var area = GetPointAndRadiusGeography(p.Location, p.Radius);
                    geo = geo.Difference(area);
                }
            }
            return geo;
        }

        protected static bool IsAreaLargerThanCanada(DbGeography geo)
        {
            if (geo.Area > CanadaArea) return true;
            return false;
        }

        static DbGeography ParseGeography(string text)
        {
            var geoText = ConvertToGeoText(text);
            return DbGeography.FromText(geoText.Text, geoText.Srid);
        }

        static DbGeography GetFirGeography(string firId, IUow uow)
        {
            var fir = uow.SdoCacheRepo.GetById(firId);

            if (fir == null || fir.Type != SdoEntityType.Fir)
                throw new Exception(String.Format(Resources.FIRNotFound, firId));

            return fir.Geography;
        }

        static DbGeography GetPolygonGeography(string points)
        {
            // parse location list from points
            var locations = (points ?? "").Split(',').Select(s => s.Trim()).ToList();

            // validate number of points
            if (locations.Count < 3)
                throw new Exception(String.Format(Resources.DoaGeoMore3Points, points));

            if (locations.Exists(l => !DMSLocation.IsValidDecimal(l)))
                throw new Exception(String.Format(Resources.DoaGeoInvalidPoint, points));

            // close the polygon if not closed
            if (locations.First() != locations.Last())
                locations.Add(locations[0]);

            // make points counterclockwise
            if (CalcSignedArea(locations.Select(l => new DecimalLocation(l)).ToList()) > 0)
                locations.Reverse();

            // assemble the points again
            var poly = string.Join(",", locations.Select(l => DMSLocation.FlipCoordinates(l)));

            return DbGeography.PolygonFromText($"POLYGON(({poly}))", DbGeography.DefaultCoordinateSystemId);
        }

        static DbGeography GetPointAndRadiusGeography(string location, int radius)
        {
            if (!DMSLocation.IsKnownFormat(location))
                throw new Exception(String.Format(Resources.DoaGeoInvalidPoint, location));

            var geoPoint = GetGeoPointFromLocation(DMSLocation.ToDecimalLatitudeLongitude(location));
            var nmRadius = GeoDataService.NMToMeters(radius);

            return geoPoint.Buffer(nmRadius);
        }

        static DbGeography GetGeoPointFromLocation(string location)
        {
            return DbGeography.PointFromText($"POINT({DMSLocation.FlipCoordinates(location)})", DbGeography.DefaultCoordinateSystemId);
        }

        static bool ValidPolygonArea(List<string> points)
        {
            return Math.Abs(CalcSignedArea(points.Select(l => new DecimalLocation(l)).ToList())) > 1.0;
        }

        static double CalcSignedArea(List<DecimalLocation> points)
        {
            var signedArea = 0.0;
            for (var i = 0; i < points.Count - 1; i++)
            {
                var current = points[i];
                var next = points[i + 1];
                signedArea += current.Latitude * next.Longitude - next.Latitude * current.Longitude;
            }
            return signedArea;
        }

        static string GetGeographyFileContent(HttpPostedFile file)
        {
            using (var reader = new StreamReader(CopyToMemory(file.InputStream)))
            {
                return reader.ReadToEnd();
            }
        }

        static Stream CopyToMemory(Stream input)
        {
            var stream = new MemoryStream();
            input.CopyTo(stream);
            stream.Seek(0, SeekOrigin.Begin);
            return stream;
        }

        static StepResult<string> ValidateGeography(string value)
        {
            try
            {
                var geoText = ConvertToGeoText(value);
                var geo = DbGeography.FromText(geoText.Text, geoText.Srid).MakeValid();

                var result = IsAreaLargerThanCanada(geo);

                if(result)
                    return StepResult<string>.Fail(Resources.ErrorGeoFileClockwise); //It is clockwise
                else
                    return geo != null ? StepResult<string>.Succeed(geo.ToString()) : StepResult<string>.Fail(Resources.DoaGeoCantParse);
            }
            catch (Exception ex)
            {
                return StepResult<string>.Fail(ex.Message);
            }
        }

        static GeoText ConvertToGeoText(string text)
        {
            if ((text ?? "").StartsWith("SRID="))
            {
                var geoPos = text.IndexOf(';') + 1;
                var srid = ParseSrid(text.Substring(0, geoPos - 1));
                return new GeoText() { Srid = srid, Text = text.Substring(geoPos) };
            }
            return new GeoText() { Srid = DbGeography.DefaultCoordinateSystemId, Text = text };
        }

        static int ParseSrid(string text)
        {
            var sridText = text.Substring("SRID=".Length);
            int srid;
            return int.TryParse(sridText, out srid) ? srid : DbGeography.DefaultCoordinateSystemId;
        }
    }

    struct GeoText
    {
        public int Srid;
        public string Text;
    }
}
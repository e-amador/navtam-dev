﻿using System;
using System.Collections.Generic;

namespace NavCanada.Core.Proxies.AeroRdsProxy.Model
{
    public class Taxiway
    {
        public string TaxiwayId { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string Status { get; set; }
        public string FK_Aerodrome { get; set; }
        public string Designator { get; set; }
        public string Type { get; set; }
        public string Width { get; set; }
        public string UOMWidth { get; set; }
        public string SurfaceComposition { get; set; }
        public string SurfacePreparationMethod { get; set; }
        public string SurfaceCondition { get; set; }
        public string PCNValue { get; set; }
        public string PCNPavementType { get; set; }
        public string PCNPavementSubgrade { get; set; }
        public string PCNMaxTirePressureCode { get; set; }
        public string PCNMaxTirePressureValue { get; set; }
        public string PCNEvaluationMethod { get; set; }
        public string PCNNotes { get; set; }
        public string LCNValue { get; set; }
        public string SIWLWeight { get; set; }
        public string UOMSIWLWeight { get; set; }
        public string SIWLTirePressure { get; set; }
        public string UOMSIWLTirePressure { get; set; }
        public string AUWWeight { get; set; }
        public string UOMAUWWeight { get; set; }
        public string OperationalStatus { get; set; }
        public string Marking { get; set; }
        public string Remark { get; set; }
        public int SEQ { get; set; }
    }

    public class Taxiways
    {
        public IList<Taxiway> Value { get; set; }
    }
}

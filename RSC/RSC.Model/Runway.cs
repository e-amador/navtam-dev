﻿using System;

namespace RSC.Model
{
    /// <summary>
    /// Data elements related to a single runway.
    /// </summary>
    public class Runway
    {
        public Runway(string runwayId)
        {
            RunwayId = runwayId;
        }

        /// <summary>
        /// Runway ident in the form 'RWY-xx/yy'. This ident must exist in the ADMS.
        /// </summary>
        public string RunwayId { get; set; }

        #region Maintenance Details
        /// <summary>
        /// Time that this observation was made for this runway.
        /// </summary>
        public DateTime? ObsTimeUTC { get; set; }
        /// <summary>
        /// Complex structure describing the surface condition of the cleared portion.  
        /// This structure allows reporting in thirds or as an average across the entire surface.
        /// </summary>
        public RunwayClearedPortion ClearedPortion { get; set; }
        /// <summary>
        /// Portion of the width of the runway that has been cleared. 
        /// If present must be set to 'true' indicating FULL width cleared.
        /// </summary>
        public bool ClearedFull { get; set; }

        #region Partially Cleared
        /// <summary>
        /// The runway is not completely cleared but that portion which
        /// is cleared is centred around the runway centreline.This field is used to
        /// indicate the number of feet that is cleared.Must be less than the width of
        /// the runway.
        /// </summary>
        public int CentredClearedWidth { get; set; }
        /// <summary>
        /// The portion that is cleared is offset from the centre line of the runway.
        /// </summary>
        public ClearedPortionOffset[] ClearedPortionOffsets { get; set; }
        /// <summary>
        /// Complex structure describing the surface condition of the remaining width of the runway.
        /// </summary>
        public RemainingWidth RemainingWidth { get; set; }
        #endregion

        /// <summary>
        /// Significant snowbanks.
        /// </summary>
        public SnowbanksRunway Snowbanks { get; set; }
        #endregion

        /// <summary>
        /// Attribute of 'true' if there is no winter maintenance.
        /// </summary>
        public bool NoWinterMaintenance { get; set; }

        public ClearingOperation ClearingOperations { get; set; }

        public RwyTreatment Treatments { get; set; }
    }

    /// <summary>
    /// Need to modify the schema to acomodate this new data on the official form
    /// </summary>
    public class ClearingOperation
    {
        public DateTime? StartAt { get; set; }
        public DateTime? CompleteBy { get; set; }
        public FreeformText Remarks { get; set; }
    }

    /// <summary>
    /// Need to modify the schema to acomodate this new data on the official form
    /// </summary>
    public class RwyTreatment
    {
        public bool Sand { get; set; }
        public DateTime? SandTimeApplied { get; set; }
        public bool ChemicallyTreated { get; set; }
        public DateTime? ChemicalTimeApplied { get; set; }
        public FreeformText Remarks { get; set; }
    }


}
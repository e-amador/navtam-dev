﻿import { Component, OnInit, Inject } from '@angular/core'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';

import { ActivatedRoute, Router } from '@angular/router'

import { Select2OptionData } from 'ng2-select2';

import { DataService } from '../shared/data.service'
import { MemoryStorageService } from '../shared/mem-storage.service'
import { WindowRef } from '../../common/windowRef.service'

import { LocaleService, TranslationService } from 'angular-l10n';

import { PhoneValidator } from '../shared/phone.validator'

import {
    IQueryOptions,
    IPaging,
    IOrgPartial,
    ITableHeaders
} from '../shared/data.model'

declare var $: any; 

@Component({ 
    templateUrl: '/app/admin/org/org-list.component.html'
})
export class OrgListComponent implements OnInit {

    orgs: IOrgPartial[];
    queryOptions: IQueryOptions;
    selectedOrg: IOrgPartial = null;
    paging: IPaging = {
        currentPage: 1,
        pageSize: 0,
        totalCount: 0,
        totalPages: 0,
    };
    totalPages: number = 0;

    itemsPerPageData: Array<any>;
    itemsPerPageStartValue: string;
    loadingData: boolean = false;
    tableHeaders: ITableHeaders;
    canbeDeleted: boolean = false;
    orgListForm: FormGroup; 

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService, 
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private route: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService) {

        if (this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);

        this.tableHeaders = {
            sorting:
            [
                { columnName: 'Name', direction: 0 },
                { columnName: 'Address', direction: 0 },
                { columnName: 'EmailAddress', direction: 0 },
                { columnName: 'Telephone', direction: 0 },
                { columnName: 'TypeOfOperation', direction: 0 }
            ],
            itemsPerPage:
            [
                { id: '10', text: '10' },
                { id: '20', text: '20' },
                { id: '50', text: '50' },
                { id: '100', text: '100' },
                { id: '200', text: '200' },
            ]
        };

        this.paging.pageSize = +this.tableHeaders.itemsPerPage[0].id;
        this.queryOptions = {
            entity: "org",
            page: 1,
            pageSize: this.paging.pageSize,
            sort: this.tableHeaders.sorting[0].columnName, //Default Received Descending
            filterValue: ""
        }
    };

    ngOnInit() {
        //Set latest query options
        const queryOptions = this.memStorageService.get(this.memStorageService.ITEM_QUERY_OPTIONS_KEY);
        if (queryOptions && queryOptions.entity === "org") {
            this.queryOptions = queryOptions;
        }
        this.configureReactiveForm();
        this.getOrgsInPage(this.queryOptions.page);
    }

    get langCulture() {
        return (window["app"].cultureInfo || "en").toUpperCase();
    }

    private configureReactiveForm() {
        this.orgListForm = this.fb.group({
            searchOrg: [{ value: this.queryOptions.filterValue, disabled: false }, []],
        });
        this.$ctrl("searchOrg").valueChanges.debounceTime(1000).subscribe(() => this.updateOrgList());
    }

    updateOrgList(): void {
        if (this.loadingData) return;
        const subject = this.$ctrl('searchOrg');
        this.queryOptions.filterValue = subject.value;
        this.getOrgsInPage(this.queryOptions.page);
    }

    $ctrl(name: string): AbstractControl {
        return this.orgListForm.get(name);
    }

    selectOrg(org) {
        this.canbeDeleted = false;

        if (this.selectedOrg) {
            this.selectedOrg.isSelected = false;
        }

        this.selectedOrg = org;
        this.selectedOrg.isSelected = true;
        this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, this.selectedOrg.id);

        this.dataService.getOrgUserCount(this.selectedOrg.id)
            .subscribe((count: number) => {
                this.canbeDeleted = count === 0;
            });

    }

    itemsPerPageChanged(e: any): void {
        this.queryOptions.pageSize = e.value;
        this.getOrgsInPage(1);
    }

    onPageChange(page: number) {
        this.getOrgsInPage(page);
    }

    sort(index) {
        const dir = this.tableHeaders.sorting[index].direction;

        for (let i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }

        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
        }

        this.getOrgsInPage(this.queryOptions.page);
    }

    sortingClass(index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';

        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';

        return 'sorting';
    }

    confirmDeleteOrg() {
        $('#modal-delete').modal({});
    }

    deleteOrg() {
        if (this.selectedOrg) {
            this.dataService.deleteOrg(this.selectedOrg.id)
                .subscribe(result => {
                    this.getOrgsInPage(this.paging.currentPage);
                    let msg = this.translation.translate('OrgSession.SuccessOrgDeleted');
                    this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                }, error => {
                    let msg = this.translation.translate('OrgSession.FailureOrgDeleted');
                    this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        }
    }

    private getOrgsInPage(page) {
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, this.queryOptions);

            this.dataService.getRegisteredOrgs(this.queryOptions)
                .subscribe((orgs: any) => {
                    this.processOrgs(orgs);
                });
        }
        catch (e) {
            this.loadingData = false;
        }
    }

    private processOrgs(orgs: any) {
        this.paging = orgs.paging;
        this.totalPages = orgs.paging.totalPages;

        for (var iter = 0; iter < orgs.data.length; iter++) {
            const org = orgs.data[iter];
            org.index = iter;
            org.telephone = PhoneValidator.formatTenDigits(org.telephone);
        }

        const lastOrgIdSelected = this.memStorageService.get(this.memStorageService.ITEM_ID_KEY);
        if (orgs.data.length > 0) {
            this.orgs = orgs.data;
            let index = 0;
            if (lastOrgIdSelected) {
                index = this.orgs.findIndex(x => x.id === lastOrgIdSelected);
                if (index < 0)
                    index = 0; // when not found first should be selected;
            }
            this.orgs[index].isSelected = true;
            this.selectedOrg = this.orgs[index];
            this.loadingData = false;
        } else {
            this.orgs = [];
            this.selectedOrg = null;
            this.loadingData = false;
        }
    }


}

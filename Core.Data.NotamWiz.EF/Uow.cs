﻿namespace NavCanada.Core.Data.NotamWiz.EF
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Core.Objects;
    using System.Data.Entity.Infrastructure;
    using System.Threading.Tasks;

    using Contracts;
    using Contracts.Repositories;
    using Helpers;
    using Domain.Model.Entitities;
    using Data.Contracts.Repositories;

    /// <summary>
    /// The "Unit of Work"
    ///     1) decouples the repos from the consumers
    ///     2) decouples the DbContext and EF from the consumers
    ///     3) manages the UoW
    /// </summary>
    /// <remarks>
    /// This class implements the "Unit of Work" pattern in which
    /// the "UoW" serves as a facade for querying and saving to the database.
    /// Querying is delegated to "repositories".
    /// Each repository serves as a container dedicated to a particular
    /// root entity type such as a <see cref="BilingualRegion"/>.
    /// A repository typically exposes "Get" methods for querying and
    /// will offer add, update, and delete methods if those features are supported.
    /// The repositories rely on their parent UoW to provide the interface to the
    /// data layer (which is the EF DbContext in AppDbContext).
    /// </remarks>
    public class Uow : IUow
    {
        private DbContextTransaction _transaction;

        public Uow(IRepositoryProvider repositoryProvider)
            :this(new AppDbContext(), repositoryProvider)
        {
        }

        public Uow(DbContext context, IRepositoryProvider repositoryProvider)
        {
            DbContext = context;
            ConfigureDbContext();
            repositoryProvider.DbContext = DbContext;
            RepositoryProvider = repositoryProvider;
        }

        /*
        public Uow(IRepositoryProvider repositoryProvider, AppDbContext ctx)
        {
            DbContext = ctx;
            ConfigureDbContext();

            repositoryProvider.DbContext = DbContext;
            RepositoryProvider = repositoryProvider;
        }
        */

        public DbContext DbContext
        {
            get;
            private set;
        }

        private IRepositoryProvider RepositoryProvider
        {
            get;
            set;
        }

        private void ConfigureDbContext()
        {
            //creating the database scheme.
            //DbContext.Database.Initialize(true);

            // This flag allow UOW to detect changes in navigation properties, don't set to false 
            DbContext.Configuration.AutoDetectChangesEnabled = true;

            // Do NOT enable proxied entities, else serialization fails
            DbContext.Configuration.ProxyCreationEnabled = true;

            // Load navigation properties explicitly (avoid serialization trouble)
            DbContext.Configuration.LazyLoadingEnabled = true;

            // Because Web API will perform validation, we don't need/want EF to do so
            DbContext.Configuration.ValidateOnSaveEnabled = false;

            //DbContext.Configuration.AutoDetectChangesEnabled = false;
            // We won't use this performance tweak because we don't need 
            // the extra performance and, when autodetect is false,
            // we'd have to be careful. We're not being that careful.
        }

        private IRepository<T> GetStandardRepo<T>() where T : class
        {
            return RepositoryProvider.GetRepositoryForEntityType<T>();
        }

        private T GetRepo<T>() where T : class
        {
            return RepositoryProvider.GetRepository<T>();
        }

        #region IUoW

        public int SaveChanges(bool clientsWin = false)
        {
            if (clientsWin)
            {
                bool saveFailed;
                int result =0;
                do
                {
                    saveFailed = false;
                    try
                    {
                        result = DbContext.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        saveFailed = true;

                        // Update original values from the database 
                        foreach (var entry in ex.Entries)
                        {
                            entry.OriginalValues.SetValues(entry.GetDatabaseValues());
                        }
                    }

                } while (saveFailed);

                return result;
            }
            else
            {
                return DbContext.SaveChanges();
            }
        }

        public Task<int> SaveChangesAsync()
        {
            return DbContext.SaveChangesAsync();
        }

        //public void FlagModified(object entity)
        //{
        //    DbContext.Entry(entity).State = EntityState.Modified;
        //}

        public void Detach()
        {
            var context = ((IObjectContextAdapter)DbContext).ObjectContext;
            foreach (var change in DbContext.ChangeTracker.Entries())
            {
                if (change.State == EntityState.Modified)
                {
                    context.Refresh(RefreshMode.StoreWins, change.Entity);
                }
                if (change.State == EntityState.Added)
                {
                    context.Detach(change.Entity);
                }
            }
        }

        public void BeginTransaction(System.Data.IsolationLevel isolationLevel = System.Data.IsolationLevel.RepeatableRead)
        {
            _transaction = DbContext.Database.BeginTransaction(System.Data.IsolationLevel.RepeatableRead);
        }

        public void Commit()
        {
            try
            {
                DbContext.SaveChanges();
                if (_transaction != null)
                {
                    _transaction.Commit();
                }
            }
            finally
            {
                if (_transaction != null)
                {
                    _transaction.Dispose();
                    _transaction = null;
                }
            }
        }

        public void Rollback()
        {
            try
            {
                if (_transaction != null)
                {
                    _transaction.Rollback();
                }
            }
            catch (Exception)
            {
                // avoid throwning exceptions again
            }
            finally
            {
                if (_transaction != null)
                {
                    _transaction.Dispose();
                    _transaction = null;
                }
            }
        }

        public IAerodromeDisseminationCategoryRepo AerodromeDisseminationCategoryRepo
        {
            get
            {
                return GetRepo<IAerodromeDisseminationCategoryRepo>();
            }
        }

        public IConfigurationValueRepo ConfigurationValueRepo
        {
            get
            {
                return GetRepo<IConfigurationValueRepo>();
            }
        }

        public IDigitalSlaRepo DigitalSlaRepo
        {
            get
            {
                return GetRepo<IDigitalSlaRepo>();
            }
        }

        public IDoaRepo DoaRepo
        {
            get
            {
                return GetRepo<IDoaRepo>();
            }
        }

        public IUserDoaRepo UserDoaRepo => GetRepo<IUserDoaRepo>();

        public IOrganizationDoaRepo OrganizationDoaRepo => GetRepo<IOrganizationDoaRepo>();

        public IFeedbackRepo FeedbackRepo
        {
            get
            {
                return GetRepo<IFeedbackRepo>();
            }
        }

        public IGeoRegionRepo GeoRegionRepo
        {
            get
            {
                return GetRepo<IGeoRegionRepo>();
            }
        }

        public ISeriesAllocationRepo SeriesAllocationRepo
        {
            get
            {
                return GetRepo<ISeriesAllocationRepo>();
            }
        }

        public IGroupRepo GroupRepo
        {
            get
            {
                return GetRepo<IGroupRepo>();
            }
        }

        public IIcaoSubjectConditionRepo IcaoSubjectConditionRepo
        {
            get
            {
                return GetRepo<IIcaoSubjectConditionRepo>();
            }
        }

        public IIcaoSubjectRepo IcaoSubjectRepo
        {
            get
            {
                return GetRepo<IIcaoSubjectRepo>();
            }
        }

        public ISeriesNumberRepo SeriesNumberRepo
        {
            get
            {
                return GetRepo<ISeriesNumberRepo>();
            }
        }

        public INotamRepo NotamRepo
        {
            get
            {
                return GetRepo<INotamRepo>();
            }
        }

        public INotamTopicRepo NotamTopicRepo
        {
            get
            {
                return GetRepo<INotamTopicRepo>();
            }
        }

        public INotificationTemplateRepo NotificationTemplateRepo
        {
            get
            {
                return GetRepo<INotificationTemplateRepo>();
            }
        }

        public INotificationTrackingRepo NotificationTrackingRepo
        {
            get
            {
                return GetRepo<INotificationTrackingRepo>();
            }
        }

        //public INsdRepo NsdRepo
        //{
        //    get
        //    {
        //        return GetRepo<INsdRepo>();
        //    }
        //}

        //public INsdVersionRepo NsdVersionRepo
        //{
        //    get
        //    {
        //        return GetRepo<INsdVersionRepo>();
        //    }
        //}

        public IOrganizationRepo OrganizationRepo
        {
            get
            {
                return GetRepo<IOrganizationRepo>();
            }
        }

        public IOrganizationNsdRepo OrganizationNsdRepo => GetRepo<IOrganizationNsdRepo>();

        public IPermissionRepo PermissionRepo
        {
            get
            {
                return GetRepo<IPermissionRepo>();
            }
        }

        public IRegistrationRepo RegistrationRepo
        {
            get
            {
                return GetRepo<IRegistrationRepo>();
            }
        }

        public IRolePermissionRepo RolePermissionRepo
        {
            get
            {
                return GetRepo<IRolePermissionRepo>();
            }
        }

        public ISdoCacheRepo SdoCacheRepo => GetRepo<ISdoCacheRepo>();

        public IUserPreferenceRepo UserPreferenceRepo
        {
            get
            {
                return GetRepo<IUserPreferenceRepo>();
            }
        }

        public IUserProfileRepo UserProfileRepo
        {
            get
            {
                return GetRepo<IUserProfileRepo>();
            }
        }

        public IApplicationRoleRepo ApplicationRoleRepo
        {
            get
            {
                return GetRepo<IApplicationRoleRepo>();
            }
        }

        public IPoisonMtoRecordRepo PoisonMtoRecordRepo
        {
            get
            {
                return GetRepo<IPoisonMtoRecordRepo>();
            }
        }

        public IPoisonMtoRepo PoisonMtoRepo
        {
            get
            {
                return GetRepo<IPoisonMtoRepo>();
            }
        }

        public IScheduleTaskRepo ScheduleTaskRepo
        {
            get
            {
                return GetRepo<IScheduleTaskRepo>();
            }
        }

        //New Added

        public INsdCategoryRepo NsdCategoryRepo
        {
            get
            {
                return GetRepo<INsdCategoryRepo>();
            }
        }

        public IProposalRepo ProposalRepo
        {
            get
            {
                return GetRepo<IProposalRepo>();
            }
        }

        public IProposalHistoryRepo ProposalHistoryRepo
        {
            get
            {
                return GetRepo<IProposalHistoryRepo>();
            }
        }

        public IProposalAttachmentRepo ProposalAttachmentRepo
        {
            get
            {
                return GetRepo<IProposalAttachmentRepo>();
            }
        }

        public IScheduleJobRepo ScheduleJobRepo
        {
            get
            {
                return GetRepo<IScheduleJobRepo>();
            }
        }

        public IUserQueryFilterRepo UserQueryFilterRepo
        {
            get
            {
                return GetRepo<IUserQueryFilterRepo>();
            }
        }

        public INdsClientItemARepo NdsClientItemARepo => GetRepo<INdsClientItemARepo>();
        public INdsClientSeriesRepo NdsClientSeriesRepo => GetRepo<INdsClientSeriesRepo>();
        public INdsClientRepo NdsClientRepo => GetRepo<INdsClientRepo>();
        public INdsNotamMessageRepo NdsNotamMessageRepo => GetRepo<INdsNotamMessageRepo>();
        public INdsOutgoingMessageRepo NdsOutgoingMessageRepo => GetRepo<INdsOutgoingMessageRepo>();
        public INdsIncomingRequestRepo NdsIncomingRequestRepo => GetRepo<INdsIncomingRequestRepo>();
        public IMessageExchangeQueueRepo MessageExchangeQueueRepo => GetRepo<IMessageExchangeQueueRepo>();
        public IMessageExchangeTemplateRepo MessageExchangeTemplateRepo => GetRepo<IMessageExchangeTemplateRepo>();
        public INofTemplateMessageRecordRepo NofTemplateMessageRecordRepo => GetRepo<INofTemplateMessageRecordRepo>();
        public IItemDRepo ItemDRepo => GetRepo<IItemDRepo>();
        public IDisseminationJobRepo DisseminationJobRepo => GetRepo<IDisseminationJobRepo>();
        public ISeriesChecklistRepo SeriesChecklistRepo => GetRepo<ISeriesChecklistRepo>();
        public IAeroRDSCacheRepo AeroRDSCacheRepo => GetRepo<IAeroRDSCacheRepo>();
        public IAeroRDSCacheStatusRepo AeroRDSCacheStatusRepo => GetRepo<IAeroRDSCacheStatusRepo>();
        public IUserRegistrationRepo UserRegistrationRepo => GetRepo<IUserRegistrationRepo>();
        public INotifySeriesRepo NotifySeriesRepo => GetRepo<INotifySeriesRepo>();
        public IOriginatorInfoRepo OriginatorInfoRepo => GetRepo<IOriginatorInfoRepo>();
        public ISystemStatusRepo SystemStatusRepo => GetRepo<ISystemStatusRepo>();
        public ILogRepo LogRepo => GetRepo<ILogRepo>();
        public IAerodromeRSCRepo AerodromeRSCRepo => GetRepo<IAerodromeRSCRepo>();

        #endregion

        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (DbContext != null)
                {
                    DbContext.Dispose();
                }
            }
        }

        #endregion
    }
}
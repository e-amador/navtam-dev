﻿namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    using System.Data.Entity;

    using Contracts.Repositories;
    using Domain.Model.Entitities;

    public class PoisonMtoRecordRepo : Repository<PoisonMtoRecord>, IPoisonMtoRecordRepo
    {
        public PoisonMtoRecordRepo(DbContext dbContext)
            : base(dbContext)
        {
        }
    }
}

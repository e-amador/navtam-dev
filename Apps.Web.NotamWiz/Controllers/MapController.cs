﻿namespace NavCanada.Applications.NotamWiz.Web.Controllers
{
    using System.Linq;
    using System.Web.Mvc;

    using NavCanada.Applications.NotamWiz.Web.Code.aeroRDS;
    using NavCanada.Applications.NotamWiz.Web.Code.GeoTools;
    using NavCanada.Applications.NotamWiz.Web.Filters;
    using NavCanada.Core.Domain.Model.Dtos;

    public class MapController : Controller
    {


        public ActionResult GetMapView()
        {
            return PartialView("_MapControl");
        }


        public ActionResult GetFIRs()
        {
            var result = AeroRds.ODataCtx.Ase.Where(a => a.CodeType == "FIR").Select(a => new { Name = a.CodeId + " -" + a.TxtName.Substring(0, a.TxtName.IndexOf(" ")), Id = a.CodeId }).ToList();
            return Json(result);            
        }


        [HttpPost]
        [CompressFilter]
        public ActionResult GetADGeo(string mid, double radius)
        {
            return GeoDataService.GetADGeo(mid, radius);

        }

        [HttpPost]
        [CompressFilter]
        public ActionResult GetLocationGeo(double lat, double lon, double radius)
        {

            return GeoDataService.GetLocationGeo(lat, lon, radius);

        }

        [HttpPost]
        [CompressFilter]
        public ActionResult GetFIRGeo(string fir)
        {
            return GeoDataService.GetFIRGeo(fir);
        }

        [HttpPost]
        [CompressFilter]
        public ActionResult Include(MultiPolygonDto mutliPolygon, MultiPolygonDto includedRegion)
        {

            return GeoDataService.Include(mutliPolygon, includedRegion, "MODEL");

        }

        [HttpPost]
        [CompressFilter]
        public ActionResult Exculde(MultiPolygonDto mutliPolygon, MultiPolygonDto excludedRegion)
        {
            return GeoDataService.Exculde(mutliPolygon, excludedRegion, "MODEL");

        }
    }
}
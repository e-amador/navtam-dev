﻿using System;
using System.Collections.Generic;
using RSCRunway = RSC.Model.Runway;


namespace NAVTAM.NsdEngine.RSC.V10
{
    public class RSCViewModel : TokenViewModel
    {
        public RSCViewModel()
        {
            RSCRunways = new List<RSCRunway>();
        }

        public string SubjectLocation { get; set; }

        #region RSC Proposal

        public string Aerodrome { get; set; }

        public bool ConditionsChanging { get; set; }
        public string Telephone { get; set; }
        public string Extension { get; set; }

        public BilingualText Taxiways { get; set; }
        public BilingualText Aprons { get; set; }
        public BilingualText GeneralRemarks { get; set; }

        public DateTime? NextPlannedObs { get; set; }

        public List<RSCRunway> RSCRunways { get; set; }

        public DateTime ObservationDate { get; set; }

        #endregion
    }

    #region Going to the client
    public class RunwayViewModel
    {
        public string RunwayId { get; set; }
        public string Designator { get; set; }
        public string Status { get; set; }
        public string RunwayLength { get; set; }
        public string RunwayLengthInFeet { get; set; }
        public string RunwayWidth { get; set; }
        public string RunwayWidthInFeet { get; set; }
        public List<RunwayDirectionViewModel> RunwayDirections { get; set; }

        public bool Selected { get; set; }
    }

    public class RunwayDirectionViewModel
    {
        public string RunwayDirectionId { get; set; }
        public string Status { get; set; }
        public string Designator { get; set; }

        public bool Selected { get; set; }
    }
    #endregion
}
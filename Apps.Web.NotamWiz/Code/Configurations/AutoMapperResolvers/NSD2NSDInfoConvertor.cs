﻿namespace NavCanada.Applications.NotamWiz.Web.Code.Configurations.AutoMapperResolvers
{
    using System.Linq;

    using AutoMapper;

    using NavCanada.Applications.NotamWiz.Web.Models;
    using NavCanada.Core.Domain.Model.Entitities;

    public class NSD2NSDInfoConvertor : ITypeConverter<Nsd, NsdInfoViewModel>
    {
        public NsdInfoViewModel Convert(ResolutionContext context)
        {

            Nsd d = context.SourceValue as Nsd;
            NsdVersion latest = d.Versions.Last();

            return new NsdInfoViewModel()
            {
                Id = d.Id,
                Name = d.Name,
                Category = d.Category,
                Description = d.Description,
                FileLocation = d.FilesLocation,
                Version = latest.Version,
                HasView = latest.HasView,
                ForSubjectType = d.ForSubjectType
            };

        }
    }
}
namespace NavCanada.Utils.Apps.InoMock.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.NotamProposal",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        NOF = c.String(),
                        Series = c.String(),
                        Number = c.Int(nullable: false),
                        Year = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        ReferredSeries = c.String(),
                        ReferredNumber = c.Int(nullable: false),
                        ReferredYear = c.Int(nullable: false),
                        QLine_FIR = c.String(),
                        QLine_Code23 = c.String(),
                        QLine_Code45 = c.String(),
                        QLine_Traffic = c.Int(nullable: false),
                        QLine_Purpose = c.Int(nullable: false),
                        QLine_Scope = c.Int(nullable: false),
                        QLine_Lower = c.Int(nullable: false),
                        QLine_Upper = c.Int(nullable: false),
                        Coordinates = c.String(),
                        Radius = c.Int(nullable: false),
                        ItemA = c.String(),
                        StartValidity = c.String(),
                        EndValidity = c.String(),
                        Estimated = c.Boolean(nullable: false),
                        ItemD = c.String(),
                        ItemE = c.String(),
                        ItemEFrench = c.String(),
                        ItemF = c.String(),
                        ItemG = c.String(),
                        ItemX = c.String(),
                        Operator = c.String(),
                        Status = c.Int(nullable: false),
                        StatusTime = c.DateTime(nullable: false),
                        SubmittedNotam_Id = c.Long(),
                        RejectionReason = c.String(),
                        NoteToNOF = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NOTAM", t => t.SubmittedNotam_Id)
                .Index(t => t.SubmittedNotam_Id);
            
            CreateTable(
                "dbo.NOTAM",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        NOF = c.String(),
                        Series = c.String(),
                        Number = c.Int(nullable: false),
                        Year = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        ReferredSeries = c.String(),
                        ReferredNumber = c.Int(nullable: false),
                        ReferredYear = c.Int(nullable: false),
                        QLine_FIR = c.String(),
                        QLine_Code23 = c.String(),
                        QLine_Code45 = c.String(),
                        QLine_Traffic = c.Int(nullable: false),
                        QLine_Purpose = c.Int(nullable: false),
                        QLine_Scope = c.Int(nullable: false),
                        QLine_Lower = c.Int(nullable: false),
                        QLine_Upper = c.Int(nullable: false),
                        Coordinates = c.String(),
                        Radius = c.Int(nullable: false),
                        ItemA = c.String(),
                        StartValidity = c.String(),
                        EndValidity = c.String(),
                        Estimated = c.Boolean(nullable: false),
                        ItemD = c.String(),
                        ItemE = c.String(),
                        ItemF = c.String(),
                        ItemEFrench = c.String(),
                        ItemG = c.String(),
                        ItemX = c.String(),
                        Operator = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.NotamProposal", "SubmittedNotam_Id", "dbo.NOTAM");
            DropIndex("dbo.NotamProposal", new[] { "SubmittedNotam_Id" });
            DropTable("dbo.NOTAM");
            DropTable("dbo.NotamProposal");
        }
    }
}

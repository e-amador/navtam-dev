﻿using System.Data.Entity.ModelConfiguration;
using NavCanada.Core.Domain.Model.Entitities;
using System.ComponentModel.DataAnnotations.Schema;

namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    class NotamTopicConfig : EntityTypeConfiguration<NotamTopic>
    {
        public NotamTopicConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            HasRequired(e => e.Notam)
                .WithMany()
                .HasForeignKey(e => e.NotamId)
                .WillCascadeOnDelete(false);
        }

    }
}

﻿import { Injectable } from '@angular/core';
import { Resolve,  ActivatedRouteSnapshot } from '@angular/router';

import { INsdCategory } from './../shared/data.model';
import { DataService } from './../shared/data.service'

@Injectable()
export class ProposalReplaceModelResolver implements Resolve<any> {

    constructor( private dataService : DataService ) {
    }

    resolve( route : ActivatedRouteSnapshot ) {
        const id = +route.params['id'];
        return this.dataService.replaceProposal(id).map(model => model);
    }
}
﻿namespace NavCanada.Applications.NotamWiz.Web.oData
{
    using System.Linq;
    using System.Web.Http;
    using System.Web.OData;

    using NavCanada.Core.Data.NotamWiz.Contracts;
    using NavCanada.Core.Domain.Model.Entitities;

    [Authorize(Roles = "User")]
    public class NSDVersionsController : BaseODataController
    {
        public NSDVersionsController(IUow uow)
            : base(uow)
        {
        }

        // GET: odata/NSDVersions
        [EnableQuery]
        public IQueryable<NsdVersion> GetNSDVersions()
        {
            return Uow.NsdVersionRepo.GetAll();
        }

        // GET: odata/NSDVersions(5)
        [EnableQuery]
        public SingleResult<NsdVersion> GetNSDVersion([FromODataUri] long key)
        {
            return SingleResult.Create(Uow.NsdVersionRepo.GetAll().Where(nSDVersion => nSDVersion.Id == key));
        }
        
        [EnableQuery]
        public SingleResult<Nsd> GetNSD([FromODataUri] long key)
        {
            return SingleResult.Create(Uow.NsdVersionRepo.GetAll().Where(m => m.Id == key).Select(m => m.Nsd));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Uow.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool NSDVersionExists(long key)
        {
            return Uow.NsdVersionRepo.GetAll().Any(e => e.Id == key);
        }
    }
}

﻿using NavCanada.Core.Domain.Model.Enums;
using System.Collections.Generic;

namespace NavCanada.Core.Domain.Model.Dtos
{
    public class OrgDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string EmailAddress { get; set; }
        public string EmailDistribution { get; set; }
        public string HeadOffice { get; set; }
        public string Telephone { get; set; }
        public string TypeOfOperation { get; set; }
        public OrganizationType Type { get; set; }
        public List<int> DoaIds { get; set; }
        public List<int> NsdIds { get; set; }
    }
}
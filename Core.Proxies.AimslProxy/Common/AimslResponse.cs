﻿using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Core.Proxies.AimslProxy.Common
{
    public class AismlResponse
    {
        public ApiStatusCode Code { get; set; }

        public string ErrorMessage { get; set; }

        public NotamProposalStatus ProposalStatus { get; set; }
    }
}

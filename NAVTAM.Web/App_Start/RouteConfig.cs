﻿
using System.Web.Mvc;
using System.Web.Routing;

namespace NAVTAM
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                constraints: new { Controller = "Home|Account|Manage|Error|Feedback|Developer|UserGuide" });

            // This is a catch-all for when no other routes matched. Let the Angular 2 router take care of it
            routes.MapRoute(
                name: "angular-app",
                url: "dashboard/{*pathinfo}",
                defaults: new { controller = "Dashboard", action = "Index" } // The view that bootstraps Angular 2
            );

            routes.MapRoute(
                name: "angular-admin",
                url: "administration/{*pathinfo}",
                defaults: new { controller = "Administration", action = "Index" } // The view that bootstraps Angular 2
            );
        }
    }
}

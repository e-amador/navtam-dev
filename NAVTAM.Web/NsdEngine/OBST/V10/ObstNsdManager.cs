﻿using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AeroRdsProxy.Model;
using NAVTAM.Helpers;
using NAVTAM.ViewModels;
using System;
using System.Device.Location;
using System.Linq;
using System.Threading.Tasks;

namespace NAVTAM.NsdEngine.OBST.V10
{
    public class ObstNsdManager : ObstNsdBase
    {
        public ObstNsdManager(int categoryId, string categoryName, string version) : base(categoryId, categoryName, version)
        {
        }

        protected override TokenViewModel CreateTokenViewModel()
        {
            return new ObstViewModel();
        }

        protected override TokenViewModel DeserializeToken(string tokens)
        {
            return Deserialize<ObstViewModel>(tokens);
        }

        protected override ValidationResult ValidateModel(ProposalViewModel model, ProposalContext context)
        {
            var token = model.Token as ObstViewModel;
            if (token == null)
                return ValidationResult.Fail(Resources.InvalidTokenType);

            var obstTypes = new SimpleObstacles();
            if (!obstTypes.HasType(token.ObstacleType))
                return ValidationResult.Fail(Resources.InvalidObstacleType);

            if (context.NearestAD() == null)
                return ValidationResult.Fail(Resources.NoNearbyAerodromeFound);

            return ValidationResult.Succeed();
        }

        protected override string GetNotamLocation(TokenViewModel token)
        {
            return (token as ObstViewModel).Location;
        }

        protected override async Task UpdateNsdContext(TokenViewModel token, ProposalContext context)
        {
            // add nearest aerodrome
            var nearestSubject = await GetNearestAerodrome(context.GeoLocation(), context.Uow);
            var aerodrome = await SdoCacheBroker.GetAerodromeCache(context.Uow, nearestSubject.Subject, true, false);
            context.AddContext("nearestAD", nearestSubject);
            context.AddContext("nearestADCache", aerodrome);
        }

        protected override NearSdoSubject GetNearestSdoSubject(TokenViewModel token, ProposalContext context)
        {
            return context.NearestAD();
        }

        protected override string GetFir(TokenViewModel token, ProposalContext context)
        {
            var nearestAD = context.NearestAD();
            return nearestAD.Distance > 5 ? GetLocationFir(context.GeoLocation(), context.Uow) : nearestAD.Subject.Fir;
        }

        protected override string GenerateItemA(TokenViewModel token, ProposalContext context)
        {
            var nearestAD = context.NearestAD();
            if (nearestAD.Distance > 5)
            {
                // all nearby firs
                var nearbyFirs = GetAlternateNearbyFirs(context.GeoLocation(), string.Empty, context.Uow);
                return string.Join(" ", nearbyFirs);
            }
            // Aerodrome Item A
            return CovertAerodromeDesignatorToItemA(nearestAD.Subject.Designator);
        }

        protected override BilingualText GenerateItemE(TokenViewModel token, ProposalContext context)
        {
            return !context.IsCancelation ? GenerateNonCancelItemE(context, token as ObstViewModel) : GenerateCancelItemE(context, token as ObstViewModel);
        }

        protected override int GetRadius(TokenViewModel token, ProposalContext context)
        {
            return context.NearestAD().Distance > 5 ? 2 : 5;
        }

        protected override string GetScope(TokenViewModel token, ProposalContext context)
        {
            return context.NearestAD().Distance > 5 ? "E" : "AE";
        }

        protected override async Task<string> GetSeries(TokenViewModel token, ProposalContext context)
        {
            var nearestAD = context.NearestAD();
            return await GetNotamSeriesForSubject(context.Uow, nearestAD.Subject, "OB", nearestAD.Distance > 5);
        }

        protected override string GetTraffic(TokenViewModel token, ProposalContext context)
        {
            return context.NearestAD().Distance > 5 ? "V" : "IV";
        }

        protected override string GetCode23(TokenViewModel token, ProposalContext context)
        {
            return "OB";
        }

        protected override string GetCode45(TokenViewModel token, ProposalContext context)
        {
            return !context.IsCancelation ? "CE" : "CN";
        }

        protected override string GetPurpose(TokenViewModel token, ProposalContext context)
        {
            return "M";
        }

        protected override int GetUpperLimit(TokenViewModel token, ProposalContext context)
        {
            var elevation = (token as ObstViewModel).Elevation;
            return RoundUpperLimitToFlightLevel(elevation);
        }

        const int BeamLineThreshold = 50;
        const int RoundingDistance = 10;

        private BilingualText GenerateNonCancelItemE(ProposalContext context, ObstViewModel token)
        {
            var nearestAD = context.NearestAD();
            var centerLocation = context.Location;

            var aerodrome = context.NearestAerodromeCache();

            var obstacleLocation = centerLocation.ToGeoCoordinate();
            var subjectLocation = new GeoCoordinate(nearestAD.Subject.RefPoint.Latitude ?? 0, nearestAD.Subject.RefPoint.Longitude ?? 0);
            var distanceFromSubject = obstacleLocation.GetDistanceTo(subjectLocation);

            var bearings = subjectLocation.BearingTo(obstacleLocation) - NSDUtilities.GetAerodromeMagneticVariation(context.Uow, aerodrome, nearestAD.Subject.RefPoint);

            var itemECardinalPoint = GeoDataService.GetDirection(bearings);

            var nearestADRunways = aerodrome.Runways;
            var nearestADHasWaterRunways = nearestADRunways.Any(r => r.Designator.StartsWith("RWY-W"));
            var nearestADIsHeliport = "HP".Equals(aerodrome.Type);

            BilingualText itemEAprxLine = null;
            BilingualText itemEPrefix = GetAmendPubMessagePrefix(token);

            if (nearestAD.Distance > 5) // FIR 
            {
                var refFeature = GenerateItemEReferenceFeature(context);
                itemEAprxLine = BuildObstacleCenterAproxLine(refFeature, distanceFromSubject, itemECardinalPoint);
            }
            else
            {
                itemEPrefix = GetItemEAerodromeNamePrefix(context, nearestAD.Subject.Name).Append(itemEPrefix);
                var distanceFromSubjectInNM = Math.Round(GeoDataService.MetersToNM(distanceFromSubject));
                if (distanceFromSubjectInNM > 3 || !RunwaysContainTHRInfo(nearestADRunways) || nearestADHasWaterRunways || nearestADIsHeliport)
                {
                    var refFeature = new BilingualText("AD", "AD");
                    itemEAprxLine = BuildObstacleCenterAproxLine(refFeature, distanceFromSubject, itemECardinalPoint);
                }
                else
                {
                    itemEAprxLine = BuildObstacleCenterAproxLineFromTHR(nearestADRunways, obstacleLocation, subjectLocation);
                }
            }

            var obstacleTypes = new SimpleObstacles();
            var obstacleTypeE = obstacleTypes["en"][token.ObstacleType];
            var obstacleTypeF = obstacleTypes["fr"][token.ObstacleType];

            var itemEObstacleType = obstacleTypeE.EFieldName;
            var itemEObstacleTypeFrench = obstacleTypeF.EFieldName;

            var itemELatLong = centerLocation.ToDMS();

            var obstStatusTextE = GetObstacleStatusesText(token.Lighted, token.Painted, SwitchState.Unknown, obstacleTypeE.LightedText, obstacleTypeE.PaintedText, obstacleTypeE.Ballmarked);
            var obstStatusTextF = GetObstacleStatusesText(token.Lighted, token.Painted, SwitchState.Unknown, obstacleTypeF.LightedText, obstacleTypeF.PaintedText, obstacleTypeF.Ballmarked);

            var itemEObstacleHeight = token.Height;
            var itemEObstacleElevation = token.Elevation;

            var itemE = $"{itemEPrefix.English}{itemEObstacleType} {itemELatLong} {itemEAprxLine.English}. {itemEObstacleHeight}FT AGL {itemEObstacleElevation}FT AMSL. {obstStatusTextE}".Trim();
            var itemEFrench = $"{itemEPrefix.French}{itemEObstacleTypeFrench} {itemELatLong} {itemEAprxLine.French}. {itemEObstacleHeight}FT AGL {itemEObstacleElevation}FT AMSL. {obstStatusTextF}".Trim();

            return new BilingualText()
            {
                English = itemE,
                French = context.IsBilingual ? itemEFrench : string.Empty
            };
        }

        private BilingualText GenerateCancelItemE(ProposalContext context, ObstViewModel token)
        {
            var nearestAD = context.NearestAD();
            var adNamePrefix = nearestAD.Distance <= 5 ? GetItemEAerodromeNamePrefix(context, nearestAD.Subject.Name) : new BilingualText();

            var obstacleTypes = new SimpleObstacles();
            var itemEObstacleType = obstacleTypes["en"][token.ObstacleType].EFieldName;
            var itemEObstacleTypeFrench = obstacleTypes["fr"][token.ObstacleType].EFieldName;
            var itemELatLong = context.Location.ToDMS();

            var itemEEnglish = $"{adNamePrefix.English}{itemEObstacleType} {itemELatLong} {obstacleTypes["en"][token.ObstacleType].RemovedText}";
            var itemEFrench = $"{adNamePrefix.French}{itemEObstacleTypeFrench} {itemELatLong} {obstacleTypes["fr"][token.ObstacleType].RemovedText}";

            return new BilingualText(itemEEnglish, context.IsBilingual ? itemEFrench : string.Empty);
        }

        protected override string GetDisplayName()
        {
            return Resources.ObstDisplayName;
        }

        protected override string GetNsdHelpLink()
        {
            return Resources.ObstHelpLink;
        }
    }

    internal static class ObstNsdManagerExt
    {
        internal static NearSdoSubject NearestAD(this ProposalContext context)
        {
            return context?.GetContext<NearSdoSubject>("nearestAD");
        }

        internal static Aerodrome NearestAerodromeCache(this ProposalContext context)
        {
            return context?.GetContext<Aerodrome>("nearestADCache");
        }
    }
}

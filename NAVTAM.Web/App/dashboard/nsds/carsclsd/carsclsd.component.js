"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CarsClsdComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var data_service_1 = require("../../shared/data.service");
var location_service_1 = require("../../shared/location.service");
var carsclsd_service_1 = require("./carsclsd.service");
var windowRef_service_1 = require("./../../../common/windowRef.service");
var toastr_service_1 = require("./../../../common/toastr.service");
var angular_l10n_1 = require("angular-l10n");
var CarsClsdComponent = /** @class */ (function () {
    function CarsClsdComponent(toastr, injector, fb, locationService, carsClsdService, winRef, dataService, locale, translation) {
        this.toastr = toastr;
        this.injector = injector;
        this.fb = fb;
        this.locationService = locationService;
        this.carsClsdService = carsClsdService;
        this.winRef = winRef;
        this.dataService = dataService;
        this.locale = locale;
        this.translation = translation;
        this.action = "";
        this.token = null;
        this.isReadOnly = false;
        this.isBilingualRegion = false;
        this.btnMondayClass = 'btn-white';
        this.subjectSelected = null;
        this.carsStatus = [];
        this.carsStatusId = -1;
        this.disabledDayCheckbox = true;
        this.addReason = false;
        this.selectedLanguage = '';
        this.carsStatusOptions = {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.CarsStatusPlaceholder')
            }
        };
        this.token = this.injector.get('token');
        this.isReadOnly = this.injector.get('isReadOnly');
        this.nsdForm = this.injector.get('form');
        this.action = this.injector.get('type');
    }
    CarsClsdComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.isFormLoadingData = !(!this.token.subjectId);
        var app = this.carsClsdService.app;
        this.selectedLanguage = this.winRef.appConfig.cultureInfo;
        this.leafletmap = app.leafletmap;
        var frmArr = this.nsdForm.controls['frmArr'];
        if (frmArr.length === 0) {
            if (this.isFormLoadingData === false) {
                this.configureReactiveForm();
                this.loadMapLocations(app.doaId, false);
            }
            else {
                this.configureReactiveFormLoading();
            }
        }
        else {
            /*
            this is required because of the grouping funtionallity. When the NsdFormGroup
            was configured and any of items in the group has changed (new set of tokens values)
            the NsdForm Group needs to be updated with the new tokens values */
            this.updateReactiveForm(frmArr);
        }
        this.carsClsdService.getCarsClsdStatus(this.selectedLanguage)
            .subscribe(function (status) {
            _this.carsStatus = status;
            if (_this.isFormLoadingData) {
                _this.carsStatusId = _this.token.carsStatusId;
                _this.setFirstSelectItemInCombo(_this.carsStatus, _this.token.carsStatusId);
                _this.dataService.getSdoSubject(_this.token.subjectId)
                    .subscribe(function (sdoSubjectResult) {
                    _this.subjectSelected = sdoSubjectResult;
                    _this.subjects = [];
                    _this.subjects.push(sdoSubjectResult);
                    _this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);
                    _this.loadMapLocations(app.doaId, true);
                    _this.updateMap(sdoSubjectResult, true);
                    _this.isFormLoadingData = false;
                    var val = _this.$ctrl('refreshIcao').value;
                    _this.$ctrl('refreshIcao').setValue((+val) + 1);
                    _this.$ctrl('refreshIcao').setValidators(refreshIcaoValidator);
                    _this.$ctrl('refreshIcao').updateValueAndValidity();
                });
            }
            else {
                _this.setFirstSelectItemInCombo(_this.carsStatus, -1);
                _this.$ctrl('refreshIcao').setValidators(refreshIcaoValidator);
                _this.$ctrl('refreshIcao').updateValueAndValidity();
            }
        });
    };
    CarsClsdComponent.prototype.ngOnDestroy = function () {
        //if (this.leafletmap) {
        //    this.leafletmap.dispose();
        //    this.leafletmap = null;
        //}
    };
    Object.defineProperty(CarsClsdComponent.prototype, "IsHrsOperationSelected", {
        get: function () {
            return (this.$ctrl('carsStatusId').value === 1);
        },
        enumerable: false,
        configurable: true
    });
    CarsClsdComponent.prototype.disableDayCheck = function () {
        if (this.isFormLoadingData)
            return false;
        if (this.$ctrl('subjectId').value === '')
            return true;
        if (this.$ctrl('carsStatusId').value !== 1)
            return true;
        var fc = this.$ctrl('everyday');
        if (fc.value === true)
            return true;
        return false;
    };
    Object.defineProperty(CarsClsdComponent.prototype, "IsAllDaysChecked", {
        get: function () {
            if (this.isFormLoadingData)
                return false;
            if (this.$ctrl('subjectId').value === '')
                return false;
            if (this.$ctrl('carsStatusId').value !== 1)
                return false;
            if (this.$ctrl('daysOfWeek.monday').value === false ||
                this.$ctrl('daysOfWeek.tuesday').value === false ||
                this.$ctrl('daysOfWeek.wednesday').value === false ||
                this.$ctrl('daysOfWeek.thursday').value === false ||
                this.$ctrl('daysOfWeek.friday').value === false ||
                this.$ctrl('daysOfWeek.saturday').value === false ||
                this.$ctrl('daysOfWeek.sunday').value === false) {
                return false;
            }
            return true;
        },
        enumerable: false,
        configurable: true
    });
    CarsClsdComponent.prototype.selectAllDays = function () {
        var everydayCtrl = this.$ctrl('everyday');
        this.$ctrl('daysOfWeek').setValue({
            monday: everydayCtrl.value, tuesday: everydayCtrl.value, wednesday: everydayCtrl.value, thursday: everydayCtrl.value, friday: everydayCtrl.value, saturday: everydayCtrl.value, sunday: everydayCtrl.value
        });
        //if (everydayCtrl.value === true) {
        //    this.$ctrl('daysOfWeek').setValue({
        //        monday: true, tuesday: true, wednesday: true, thursday: true, friday: true, saturday: true, sunday: true
        //    });
        //} 
        this.validateAndTrigger();
    };
    CarsClsdComponent.prototype.checkboxChanged = function (controlName, checked) {
        if (this.isFormLoadingData)
            return;
        var control = this.nsdFrmGroup.get(controlName);
        control.setValue(checked);
        this.$ctrl('everyday').patchValue(this.IsAllDaysChecked);
        this.validateAndTrigger();
    };
    CarsClsdComponent.prototype.changeDaylightSavingTime = function () {
        if (this.isFormLoadingData)
            return;
        this.validateAndTrigger();
    };
    CarsClsdComponent.prototype.validateKeyPressHours = function (evt, ctrlName) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var num = +key;
            var pos = evt.currentTarget.selectionStart;
            if (pos === 0) { //val.length
                //First number
                if (key !== '0' && key !== '1' && key !== '2') {
                    evt.returnValue = false;
                    if (evt.preventDefault)
                        evt.preventDefault();
                    return;
                }
            }
            else if (pos === 1) { //val.length === 1
                //Second number
                var fc = this.$ctrl(ctrlName);
                var val = fc.value;
                if (val[0] === '2' && num > 3) {
                    evt.returnValue = false;
                    if (evt.preventDefault)
                        evt.preventDefault();
                    return;
                }
            }
            else if (pos === 2) { //val.length === 2
                //Third number
                if (num > 5) {
                    evt.returnValue = false;
                    if (evt.preventDefault)
                        evt.preventDefault();
                    return;
                }
            }
        }
    };
    CarsClsdComponent.prototype.carsClosedHoursChange = function () {
        if (this.isFormLoadingData)
            return;
        this.validateAndTrigger();
    };
    CarsClsdComponent.prototype.carsStatusChanged = function (e) {
        if (this.isFormLoadingData)
            return;
        if (e.value !== -1) {
            var statusId = +e.value;
            if (statusId === this.carsStatusId) {
                //for some weird reasons select2 templateSelection is called multiples 
                //times after selection
                return;
            }
            if (statusId !== -1) {
                this.carsStatusId = statusId;
                this.$ctrl('carsStatusId').setValue(statusId);
                this.enableDisableCtrls();
                if (!this.isFormLoadingData) {
                    this.validateAndTrigger();
                }
            }
            else {
                this.$ctrl('reason').patchValue('');
                this.$ctrl('reasonFr').patchValue('');
                this.$ctrl('reason').disable();
                this.$ctrl('reasonFr').disable();
            }
        }
    };
    CarsClsdComponent.prototype.enableDisableCtrls = function () {
        if (this.isFormLoadingData)
            return;
        this.disabledDayCheckbox = this.disableDayCheck();
        if (this.disabledDayCheckbox) {
            this.$ctrl('startHour').patchValue('');
            this.$ctrl('startHour').disable();
            this.$ctrl('endHour').patchValue('');
            this.$ctrl('endHour').disable();
            this.$ctrl('daylightSavingTime').patchValue(false);
            this.$ctrl('daylightSavingTime').disable();
            this.$ctrl('everyday').patchValue(false);
            this.$ctrl('everyday').disable();
            this.$ctrl('daysOfWeek').patchValue({
                monday: false, tuesday: false, wednesday: false, thursday: false, friday: false, saturday: false, sunday: false
            });
        }
        else {
            this.$ctrl('startHour').enable();
            this.$ctrl('endHour').enable();
            this.$ctrl('daylightSavingTime').enable();
            this.$ctrl('everyday').enable();
            this.$ctrl('everyday').patchValue(true);
            this.$ctrl('daysOfWeek').patchValue({
                monday: true, tuesday: true, wednesday: true, thursday: true, friday: true, saturday: true, sunday: true
            });
        }
        if (this.$ctrl('subjectId').value === '') {
            this.$ctrl('reason').patchValue('');
            this.$ctrl('reasonFr').patchValue('');
            this.$ctrl('reason').disable();
            this.$ctrl('reasonFr').disable();
        }
        else {
            if (this.$ctrl('carsStatusId').value === -1) {
                this.$ctrl('reason').patchValue('');
                this.$ctrl('reasonFr').patchValue('');
                this.$ctrl('reason').disable();
                this.$ctrl('reasonFr').disable();
            }
            else {
                this.$ctrl('reason').enable();
                this.$ctrl('reasonFr').enable();
            }
        }
    };
    CarsClsdComponent.prototype.addAReason = function (e) {
        var fc = this.$ctrl('addReason');
        if (!fc.value) {
            this.$ctrl('reason').patchValue('');
            this.$ctrl('reasonFr').patchValue('');
            if (this.addReason !== fc.value) {
                this.addReason = fc.value;
                if (this.isFormLoadingData)
                    return;
                this.validateAndTrigger();
            }
        }
        else {
            this.addReason = fc.value;
            this.setFrmError({ reasonNeeded: { valid: false } });
        }
    };
    CarsClsdComponent.prototype.changeReason = function () {
        if (this.isFormLoadingData)
            return;
        this.validateAndTrigger();
    };
    CarsClsdComponent.prototype.updateSubjectSelected = function (sdoSubject) {
        var _this = this;
        var subjectId = this.$ctrl('subjectId').value;
        if (subjectId === sdoSubject.id) {
            //for some weird reasons select2 templateSelection is called multiples 
            //times after selection
            return;
        }
        if (sdoSubject.id !== subjectId) {
            var subjectCtrl = this.$ctrl('subjectId');
            subjectCtrl.setValue(sdoSubject.id);
            subjectCtrl.markAsDirty();
        }
        this.subjectSelected = sdoSubject;
        if (!sdoSubject.subjectGeo) {
            this.dataService.getSdoSubject(sdoSubject.id)
                .subscribe(function (sdoSubjectResult) {
                _this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);
                _this.updateMap(sdoSubjectResult, true);
                _this.setInitialStatus();
                _this.validateAndTrigger();
            });
        }
        else {
            this.setBilingualRegion(sdoSubject.isBilingualRegion);
            //Here the code to upload
        }
    };
    CarsClsdComponent.prototype.configureReactiveForm = function () {
        var frmArrControls = this.nsdForm.controls['frmArr'];
        this.disabledDayCheckbox = true;
        this.nsdFrmGroup = this.fb.group({
            subjectId: ['', [forms_1.Validators.required]],
            subjectLocation: '',
            carsStatusId: [{ value: -1, disabled: true }],
            addReason: this.addReason,
            everyday: [{ value: false, disabled: true }],
            daysOfWeek: this.fb.group({
                monday: [{ value: false, disabled: this.disabledDayCheckbox }],
                tuesday: [{ value: false, disabled: this.disabledDayCheckbox }],
                wednesday: [{ value: false, disabled: this.disabledDayCheckbox }],
                thursday: [{ value: false, disabled: this.disabledDayCheckbox }],
                friday: [{ value: false, disabled: this.disabledDayCheckbox }],
                saturday: [{ value: false, disabled: this.disabledDayCheckbox }],
                sunday: [{ value: false, disabled: this.disabledDayCheckbox }],
            }),
            reason: [{ value: '', disabled: true }],
            reasonFr: [{ value: '', disabled: true }],
            startHour: [{ value: '', disabled: true }],
            endHour: [{ value: '', disabled: true }],
            daylightSavingTime: [{ value: false, disabled: true }],
            refreshIcao: '0',
        });
        frmArrControls.push(this.nsdFrmGroup);
    };
    CarsClsdComponent.prototype.configureReactiveFormLoading = function () {
        var frmArrControls = this.nsdForm.controls['frmArr'];
        var disableCtrl = (this.isReadOnly) ? true : (this.token.carsStatusId === 1) ? false : true;
        this.disabledDayCheckbox = (this.isReadOnly) ? true : (this.token.carsStatusId !== 1) ? true : false;
        if (this.token.reason && this.token.reason.length > 0)
            this.addReason = true;
        if (this.token.reasonFr && this.token.reasonFr.length > 0)
            this.addReason = true;
        this.nsdFrmGroup = this.fb.group({
            subjectId: [this.token.subjectId, [forms_1.Validators.required]],
            subjectLocation: this.token.subjectLocation,
            carsStatusId: [{ value: this.token.carsStatusId, disabled: this.isReadOnly }],
            addReason: [{ value: this.addReason, disabled: this.isReadOnly }],
            everyday: [{ value: this.token.everyday, disabled: disableCtrl }],
            daysOfWeek: this.fb.group({
                monday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.monday : true, disabled: this.disabledDayCheckbox }],
                tuesday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.tuesday : true, disabled: this.disabledDayCheckbox }],
                wednesday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.wednesday : true, disabled: this.disabledDayCheckbox }],
                thursday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.thursday : true, disabled: this.disabledDayCheckbox }],
                friday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.friday : true, disabled: this.disabledDayCheckbox }],
                saturday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.saturday : true, disabled: this.disabledDayCheckbox }],
                sunday: [{ value: (!this.token.everyday) ? this.token.daysOfWeek.sunday : true, disabled: this.disabledDayCheckbox }],
            }),
            reason: [{ value: this.token.reason || '', disabled: this.isReadOnly }],
            reasonFr: [{ value: this.token.reasonFr || '', disabled: this.isReadOnly }],
            startHour: [{ value: this.token.startHour || '', disabled: disableCtrl }],
            endHour: [{ value: this.token.endHour || '', disabled: disableCtrl }],
            daylightSavingTime: [{ value: this.token.daylightSavingTime, disabled: disableCtrl }],
            refreshIcao: '0',
        });
        frmArrControls.push(this.nsdFrmGroup);
    };
    CarsClsdComponent.prototype.updateReactiveForm = function (formArr) {
        this.nsdFrmGroup = formArr.controls[0];
        this.disabledDayCheckbox = (this.isReadOnly) ? true : (this.token.carsStatusId !== 1) ? true : false;
        if (this.token.reason && this.token.reason.length > 0)
            this.addReason = true;
        if (this.token.reasonFr && this.token.reasonFr.length > 0)
            this.addReason = true;
        //It is better to update only those controls that tokens values have changed.
        this.updateFormControlValue("subjectId", this.token.subjectId);
        this.updateFormControlValue("subjectLocation", this.token.subjectLocation);
        this.updateFormControlValue("carsStatusId", this.token.carsStatusId);
        this.updateFormControlValue("addReason", this.addReason);
        this.updateFormControlValue("everyday", this.token.everyday);
        this.updateFormControlValue("daysOfWeek.monday", (!this.token.everyday) ? this.token.daysOfWeek.monday : true);
        this.updateFormControlValue("daysOfWeek.tuesday", (!this.token.everyday) ? this.token.daysOfWeek.tuesday : true);
        this.updateFormControlValue("daysOfWeek.wednesday", (!this.token.everyday) ? this.token.daysOfWeek.wednesday : true);
        this.updateFormControlValue("daysOfWeek.thursday", (!this.token.everyday) ? this.token.daysOfWeek.thursday : true);
        this.updateFormControlValue("daysOfWeek.friday", (!this.token.everyday) ? this.token.daysOfWeek.friday : true);
        this.updateFormControlValue("daysOfWeek.saturday", (!this.token.everyday) ? this.token.daysOfWeek.saturday : true);
        this.updateFormControlValue("daysOfWeek.sunday", (!this.token.everyday) ? this.token.daysOfWeek.sunday : true);
        this.updateFormControlValue("reason", this.token.reason || '');
        this.updateFormControlValue("reasonFr", this.token.reasonFr || '');
        this.updateFormControlValue("startHour", this.token.startHour || '');
        this.updateFormControlValue("endHour", this.token.endHour || '');
        this.updateFormControlValue("daylightSavingTime", this.token.daylightSavingTime);
        //this.updateFormControlValue("refreshIcao", '0');
    };
    CarsClsdComponent.prototype.updateFormControlValue = function (controlName, value) {
        var control = this.nsdFrmGroup.get(controlName);
        if (control && control.value !== value) {
            control.setValue(value);
        }
    };
    //Validation functions
    CarsClsdComponent.prototype.validateSubjectId = function () {
        var fc = this.$ctrl('subjectId');
        return fc.value || this.nsdFrmGroup.pristine;
    };
    CarsClsdComponent.prototype.validateCarsStatusId = function () {
        var fc = this.$ctrl('carsStatusId');
        return (fc.value > -1) || this.nsdFrmGroup.pristine;
    };
    CarsClsdComponent.prototype.validateHoursEmptyCtrls = function (ctrlName) {
        if (this.$ctrl('carsStatusId').value === 1) {
            var fc = this.$ctrl(ctrlName);
            if (!fc.value || fc.value === null || fc.value === '' || fc.value.length != 4)
                return false;
        }
        return true;
    };
    CarsClsdComponent.prototype.validateReasonCtrls = function (ctrlName) {
        if (this.$ctrl(ctrlName).value.length < 1)
            return false;
        return true;
    };
    CarsClsdComponent.prototype.setFrmError = function (err) {
        if (err !== null) {
            this.nsdForm.controls['frmArr'].setErrors(err);
            this.$ctrl('refreshIcao').setValue(0);
            //this.nsdForm.controls['frmArr'].markAsDirty();
        }
        else {
            this.nsdForm.controls['frmArr'].setErrors(null);
            var val = this.$ctrl('refreshIcao').value;
            this.$ctrl('refreshIcao').setValue((+val) + 1);
            this.nsdForm.controls['frmArr'].markAsDirty();
        }
    };
    CarsClsdComponent.prototype.validateAndTrigger = function () {
        if (this.$ctrl('subjectId').value === '') {
            this.setFrmError({ subjectIdNeeded: { valid: false } });
            return;
        }
        if (this.$ctrl('carsStatusId').value === -1) {
            this.setFrmError({ statusIdNeeded: { valid: false } });
            return;
        }
        if (this.$ctrl('carsStatusId').value === 1) {
            //Validate the Hours and Days of the Week
            var fc = this.$ctrl('startHour');
            if (!fc.value || fc.value === null || fc.value === '' || fc.value.length !== 4) {
                this.setFrmError({ startHourNeeded: { valid: false } });
                return;
            }
            fc = this.$ctrl('endHour');
            if (!fc.value || fc.value === null || fc.value === '' || fc.value.length !== 4) {
                this.setFrmError({ endHourNeeded: { valid: false } });
                return;
            }
            fc = this.$ctrl('everyday');
            if (fc.value === false) {
                //Validate each day
                if (this.$ctrl('daysOfWeek.monday').value === false &&
                    this.$ctrl('daysOfWeek.tuesday').value === false &&
                    this.$ctrl('daysOfWeek.wednesday').value === false &&
                    this.$ctrl('daysOfWeek.thursday').value === false &&
                    this.$ctrl('daysOfWeek.friday').value === false &&
                    this.$ctrl('daysOfWeek.saturday').value === false &&
                    this.$ctrl('daysOfWeek.sunday').value === false) {
                    this.setFrmError({ dayOfWeekNeeded: { valid: false } });
                    return;
                }
            }
        }
        if (this.addReason) {
            if (!this.$ctrl('reason').value || this.$ctrl('reason').value.length < 1) {
                this.setFrmError({ reasonNeeded: { valid: false } });
                return;
            }
            if (this.isBilingualRegion) {
                if (!this.$ctrl('reasonFr').value || this.$ctrl('reasonFr').value.length < 1) {
                    this.setFrmError({ reasonNeeded: { valid: false } });
                    return;
                }
            }
        }
        this.setFrmError(null);
    };
    //Miscelaneas functions
    CarsClsdComponent.prototype.$ctrl = function (name) {
        return this.nsdFrmGroup.get(name);
    };
    CarsClsdComponent.prototype.setInitialStatus = function () {
        this.carsStatusId = -1;
        this.$ctrl('carsStatusId').patchValue(this.carsStatusId);
        this.$ctrl('carsStatusId').enable();
        this.$ctrl('startHour').patchValue('');
        this.$ctrl('startHour').disable();
        this.$ctrl('endHour').patchValue('');
        this.$ctrl('endHour').disable();
        this.$ctrl('daylightSavingTime').patchValue(false);
        this.$ctrl('daylightSavingTime').disable();
        this.$ctrl('everyday').patchValue(false);
        this.$ctrl('everyday').disable();
        this.$ctrl('daysOfWeek').patchValue({
            monday: false, tuesday: false, wednesday: false, thursday: false, friday: false, saturday: false, sunday: false
        });
        this.$ctrl('reason').patchValue('');
        this.$ctrl('reasonFr').patchValue('');
        this.$ctrl('reason').disable();
        this.$ctrl('reasonFr').disable();
        this.disabledDayCheckbox = true;
    };
    CarsClsdComponent.prototype.sdoSubjectWithCarsOptions = function () {
        var self = this;
        var app = this.carsClsdService.app;
        return {
            language: {
                inputTooShort: function (args) {
                    return self.translation.translate('Schedule.InputTooShort');
                },
            },
            minimumInputLength: 2,
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.SdoSubjectPlaceholder')
            },
            ajax: {
                type: 'GET',
                url: app.apiUrl + "carsclsd/filterwithcars",
                dataType: 'json',
                delay: 500,
                headers: {
                    "RequestVerificationToken": app.antiForgeryTokenValue
                },
                data: function (parms, page) {
                    return {
                        Query: parms.term,
                        Size: 200,
                        doaId: app.doaId //set by the app controller
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            templateResult: function (data) {
                if (data.loading) {
                    return data.text;
                }
                return self.dataService.getSdoExtendedName(data);
            },
            templateSelection: function (selection) {
                if (selection && selection.id === "-1") {
                    return selection.text;
                }
                self.updateSubjectSelected(selection);
                return self.dataService.getSdoExtendedName(selection);
            },
        };
    };
    CarsClsdComponent.prototype.updateMap = function (sdoSubject, acceptSubjectLocation) {
        var _this = this;
        this.leafletmap.clearFeaturesOnLayers(['DOA']);
        if (sdoSubject.subjectGeoRefPoint) {
            var geojson = JSON.parse(sdoSubject.subjectGeoRefPoint);
            if (geojson.features.length === 1) {
                var subLocation = this.leafletmap.geojsonToTextCordinates(geojson);
                var coordinates = geojson.features[0].geometry.coordinates;
                var markerGeojson_1 = acceptSubjectLocation ? geojson : this.getGeoJsonFromLocation(this.token.subjectLocation);
                if (acceptSubjectLocation) {
                    this.nsdFrmGroup.patchValue({
                        subjectLocation: subLocation,
                        latitude: coordinates[1],
                        longitude: coordinates[0],
                    }, { onlySelf: true, emitEvent: false });
                }
                this.leafletmap.addMarker(geojson, sdoSubject.name, function () {
                    _this.leafletmap.addFeature(sdoSubject.subjectGeo, sdoSubject.name, function () {
                        if (markerGeojson_1) {
                            _this.leafletmap.setNewMarkerLocation(markerGeojson_1);
                        }
                    });
                });
            }
        }
    };
    CarsClsdComponent.prototype.setFirstSelectItemInCombo = function (items, itemId, insertUndefinedAtZero) {
        if (insertUndefinedAtZero === void 0) { insertUndefinedAtZero = true; }
        if (this.isFormLoadingData) {
            var index = items.findIndex(function (x) { return x.id === itemId; });
            if (index > 0) { //move selected item to position 0
                items.splice(0, 0, items.splice(index, 1)[0]);
            }
        }
        else { //insert undefined item at position 0
            if (insertUndefinedAtZero) {
                items.unshift({
                    id: -1,
                    text: '',
                    textFr: '',
                });
            }
        }
    };
    CarsClsdComponent.prototype.loadMapLocations = function (doaId, loadingFromProposal) {
        var _this = this;
        var self = this;
        this.dataService.getDoaLocation(doaId)
            .subscribe(function (geoJsonStr) {
            var interval = window.setInterval(function () {
                if (self.leafletmap && self.leafletmap.isReady()) {
                    window.clearInterval(interval);
                    self.leafletmap.addDoa(geoJsonStr, 'DOA');
                    if (loadingFromProposal) {
                        self.updateMap(self.subjectSelected, false);
                    }
                }
                ;
            }, 100);
        }, function (error) {
            _this.toastr.error(_this.translation.translate('Nsd.DOAArea') + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    CarsClsdComponent.prototype.setBilingualRegion = function (value) {
        this.isBilingualRegion = value;
        if (this.isBilingualRegion) {
            if (!this.isReadOnly) {
                //Do something
            }
        }
        else {
            if (!this.isReadOnly) {
                //Do something
            }
        }
    };
    CarsClsdComponent.prototype.getGeoJsonFromLocation = function (value) {
        var location = this.locationService.parse(value);
        return location ? this.leafletmap.latitudeLongitudeToGeoJson(location.latitude, location.longitude) : null;
    };
    CarsClsdComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/dashboard/nsds/carsclsd/carsclsd.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, core_1.Injector,
            forms_1.FormBuilder,
            location_service_1.LocationService,
            carsclsd_service_1.CarsClsdService,
            windowRef_service_1.WindowRef,
            data_service_1.DataService,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], CarsClsdComponent);
    return CarsClsdComponent;
}());
exports.CarsClsdComponent = CarsClsdComponent;
function isANumber(str) {
    return !/\D/.test(str);
}
function refreshIcaoValidator(c) {
    var val = +c.value;
    if (val === 0) {
        return { 'required': true };
    }
    return null;
}
//# sourceMappingURL=carsclsd.component.js.map
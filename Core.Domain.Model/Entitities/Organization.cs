﻿using NavCanada.Core.Domain.Model.Enums;
using System.Collections.Generic;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class Organization
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string EmailAddress { get; set; }
        public string EmailDistribution { get; set; }
        public string Telephone { get; set; }
        public string TypeOfOperation { get; set; }
        public bool IsReadOnlyOrg { get; set; }
        public OrganizationType Type { get; set; }
        public string HeadOffice { get; set; }
        public virtual ICollection<UserProfile> Users { get; set; }
        public bool Deleted { get; set;  }
        public virtual ICollection<OrganizationDoa> Doas { get; set; }
        public virtual ICollection<OrganizationNsdCategory> Nsds { get; set; }
    }
}
﻿using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.Code.Configurations.AutoMapperResolvers
{
    using System.Linq;

    using AutoMapper;

    using NavCanada.Applications.NotamWiz.Web.Code.Extentions;
    using NavCanada.Applications.NotamWiz.Web.Code.GeoTools;
    using NavCanada.Applications.NotamWiz.Web.Models;
    using NavCanada.Core.Data.NotamWiz.Contracts;
    using NavCanada.Core.Domain.Model.Entitities;

    public class SDOSubject2NOTAMSubjectVMConvertor : ITypeConverter<SdoSubject, NotamSubjectViewModel>
    {
        ILog log = LogManager.GetLogger(typeof(SDOSubject2NOTAMSubjectVMConvertor));
        IUow db { get; set; }

        public NotamSubjectViewModel Convert(ResolutionContext context)
        {
            this.log.Object(new { Method = "Convert", Parameters = new { context = context } });

            this.db = (IUow)context.Options.Items["db"];

            SdoSubject subject = context.SourceValue as SdoSubject;
            SdoSubject parentAhp = GetParentAhp(subject);
            return new NotamSubjectViewModel()
            {
                Name = subject.Name,
                SubjectGeoJson = GeoDataService.GetJsonGeoFromDBGeography(subject.SubjectGeo, "Subject", subject.Name),
                SubjectGeoRefLat = (double)subject.SubjectGeoRefPoint.Latitude,
                SubjectGeoRefLong = (double)subject.SubjectGeoRefPoint.Longitude,
                Designator = subject.Designator,
                Fir = subject.Fir,
                Ad = parentAhp.Designator,
                AdName = parentAhp.Name,
                SubjectMid = subject.Mid,
                SubjectType = subject.SdoEntityName,
                SubjectId = subject.Id,
                SdoAttributes = subject.SdoAttributes
            };
        }

        private SdoSubject GetParentAhp(SdoSubject subject)
        {
            var topParents = new string[]
            {
                "Ahp",
                "FIR"
            };

            var tmp = subject;
            while (tmp != null && !topParents.Any(s => s == tmp.SdoEntityName))
            {
                tmp = db.SdoSubjectRepo.GetById(tmp.ParentId);
            }

            return tmp ?? subject;
        }

    }
}
﻿using System;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class SeriesNumber
    {
        public int Id { get; set; }
        public string Series { get; set; }
        public int Year { get; set; }
        public int Number { get; set; }
        public Guid RowVersion { get; set; }
    }
}

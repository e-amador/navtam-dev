﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NavCanada.Applications.NotamWiz.Web.Code.IdentityManagers;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.TaskEngine.SqlServerTypes;
using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web
{
    using System;
    using System.Globalization;
    using System.Threading;
    using System.Web;
    using System.Web.Configuration;
    using System.Web.Hosting;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;

    using Hangfire;

    using Code.Configurations;
    using Code.Extentions;
    using Code.Services.ViewProviders;
    using Core.Common.Common;
    using Core.Domain.Model.Enums;
    using Core.Proxies.DiagnosticProxy;
    //using Core.TaskEngine.SqlServerTypes;
    using Core.Data.NotamWiz.EF;

    public class WebApp : HttpApplication
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(WebApp));

        private BackgroundJobServer _backgroundJobServer;
        //private IDiagnosticProxy diagnosticProxy1;
        // Any connection or hub wire up and configuration should go here            

        protected void Application_Start()
        {
            ConfigureOdata();
            RegisterSpecialRoutes();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapperConfig.RegisterMaps();
            GlobalConfig();

            ConfigureHangFire();
        }

        protected void Application_End()
        {
            var diagnosticProxy = CreateDiagnosticProxy();

            if (diagnosticProxy != null)
            {
                var instanceName = WebConfigurationManager.AppSettings["NotmWizInstanceName"];
                if (instanceName != null)
                    diagnosticProxy.PostKeepAliveMessage(instanceName, TickType.Stop).Wait();
            }

            if (_backgroundJobServer != null)
            {
                _backgroundJobServer.Dispose();
            }
        }

        private void GlobalConfig()
        {
            //AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
            Utilities.LoadNativeAssemblies(Server.MapPath(".") + @"\bin");

            // Log4Net initialization
            Log4NetManager.InitializeLog4Net(WebConfigurationManager.ConnectionStrings["LogConnection"].ConnectionString);
            Log.Object("NOTAMWiz Starting Up.");
        }

        private void ConfigureOdata()
        {
            System.Web.Http.GlobalConfiguration.Configure(config =>
            {
                ODataConfig.Register(config);
                WebApiConfig.Register(config);
            });
            ModelMetadataProviders.Current = new CustomDataAnnotationsProvider();
        }

        private void RegisterSpecialRoutes()
        {
            RouteTable.Routes.MapRoute("SDO", "SDO/{param}", new
            {
                controller = "SDO",
                action = "ProcessRequest"
            });
            RouteTable.Routes.MapRoute("NsdsControllers", "NSD/{controller}/{version}/{action}/{id}", 
                new
                {
                    action = "Index",
                    id = UrlParameter.Optional
                });

            //ControllerBuilder.Current.SetControllerFactory(new CustomControllerFactory(ControllerBuilder.Current.GetControllerFactory()));
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());
            ViewEngines.Engines.Add(new NSDViewEngine());

            HostingEnvironment.RegisterVirtualPathProvider(new NSDViewProvider());
        }

        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
            if (HttpContext.Current.Session != null)
            {
                var culture = Session["Culture"] as CultureInfo;
                if (culture == null)
                {
                    var lang = "en";
                    if (HttpContext.Current.Request.UserLanguages != null && HttpContext.Current.Request.UserLanguages.Length != 0)
                    {
                        lang = HttpContext.Current.Request.UserLanguages[0].Substring(0, 2);
                    }
                    culture = new CultureInfo(lang);
                    Session["Culture"] = culture;
                }
                Thread.CurrentThread.CurrentUICulture = culture;
            }
        }

        private void ConfigureHangFire()
        {
            var diagnosticEndPoint = WebConfigurationManager.AppSettings["DianosticServiceEndpoint"];

            //don't use Hangfire when isn't needed.
            if (string.IsNullOrEmpty(diagnosticEndPoint))
                return;

            try
            {
                GlobalConfiguration.Configuration.UseSqlServerStorage("DefaultConnection");
            }
            catch (Exception ehe)
            {

                Log.Error("Hangfire Configuration failed.", ehe);
                return;
            }
            
            try
            {
                var instanceName = WebConfigurationManager.AppSettings["NotmWizInstanceName"];
                if (!string.IsNullOrEmpty(instanceName))
                {

                    _backgroundJobServer = new BackgroundJobServer();
                    var diagnosticProxy = CreateDiagnosticProxy();
                    if (diagnosticProxy != null)
                        RecurringJob.AddOrUpdate(() => KeepAlive(instanceName), Cron.Minutely);
                }
            }
            catch (Exception e)
            {
                Log.Error("Diagnostic Service error.", e);
            }
        }

        void Application_PostAuthenticateRequest(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                using (var db = new AppDbContext())
                {
                    // get user
                    var userId = User.Identity.GetUserId();
                    UserProfile user = db.Users.Find(userId);
                    // has their password expired?
                    if (user != null
                        && user.LastPasswordChangeDate.Date.AddDays(365) < DateTime.Now.Date
                        && (!Request.Path.EndsWith("/Account/ResetPassword") && !Request.Path.Contains("__browserLink/requestData/")))
                    {
                        var userManager = HttpContext.Current.GetOwinContext().GetUserManager<AppUserManager>();
                        var tokenConfirmation = userManager.GeneratePasswordResetToken(user.Id);

                        var url = string.Format("/Account/ResetPassword?userId={0}&token={1}&passwordExpired=true",
                            user.Id,
                            Uri.EscapeDataString(tokenConfirmation));

                        Response.Redirect(url);
                    }
                }
            }
        }

        private IDiagnosticProxy CreateDiagnosticProxy()
        {
            IDiagnosticProxy diagnosticProxy = null;

            var diagnosticEndpoint = WebConfigurationManager.AppSettings["DianosticServiceEndpoint"];
            if (!string.IsNullOrEmpty(diagnosticEndpoint))
                diagnosticProxy = new DiagnosticProxy(diagnosticEndpoint + "api/keepalive");

            return diagnosticProxy;
        }

        public void KeepAlive(string instanceName)
        {
            try
            {
                var diagnosticProxy = CreateDiagnosticProxy();
                if (diagnosticProxy != null)
                    diagnosticProxy.PostKeepAliveMessage(instanceName, TickType.KeepAlive).Wait();
            }
            catch (Exception e)
            {
                Log.Error("Diagnostic Service PostKeepAlive error.", e);
            }
        }
    }
}
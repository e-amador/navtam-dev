﻿using NavCanada.Core.Common.Common;
using System;

namespace NAVTAM.ViewModels
{
    public class NofReportViewModel
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public QueryOptions QueryOptions { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }
}
﻿namespace NAVTAM.ViewModels
{
    public class ProposalItemXViewModel
    {
        public int ProposalId { get; set; }
        public string Value { get; set; }
    }
}
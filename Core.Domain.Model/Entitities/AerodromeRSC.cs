﻿using System;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class AerodromeRSC : IRowVersion
    {
        public int Id { get; set; }
        /// <summary>
        /// Aerodrome designator
        /// </summary>
        public string Designator { get; set; }
        /// <summary>
        /// Current NOTAM Proposal for this RSC
        /// </summary>
        public int? ProposalId { get; set; }
        /// <summary>
        /// Current NOTAM for this RSC
        /// </summary>
        public Guid? NotamId { get; set; }
        /// <summary>
        /// Last updated date/time
        /// </summary>
        public DateTime? ExpirationDate { get; set; }
        /// <summary>
        /// RSC proposal JSON representation
        /// </summary>
        public string Tokens { get; set; }
        /// <summary>
        /// Concurrency token
        /// </summary>
        public byte[] RowVersion { get; set; }
    }
}

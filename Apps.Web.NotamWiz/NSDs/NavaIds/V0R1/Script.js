﻿define(["jquery", "Resources", "utilities", "NSDFormManager", "kendo/kendo.dropdownlist.min", "kendo/kendo.validator.min", "kendoCustomDataBinders"], function ($, res, Util, nsdManager) {
    //var resources = res.getResources("NSD-NavaIds");
    var self;
    var vm = null;
    var kendoValidator = null;
    return {
        init: function(editor, model) {
            self = this;
            vm = model;

            self.navaTypes = $("#navaTypes").data("navas").Data;
            self.allNavaOptions = $("#navaOptions").data("options").Data;
            self.navaOptions = [];


            vm.createNOTAMViewModel($.extend($("#nsdModel").data("model"), {
                bilingual: vm.Options.Bilingual,
                navaType: self.navaTypes[0],
                navaOption: self.allNavaOptions[0],
            }));
            Util.hidePermement();
            self.initUI(self.navaTypes, self.allNavaOptions);
            self.loadSdoData();
            kendoValidator = $("#frm-navaids").data("kendoValidator");
            editor.conditionValidator = kendoValidator;
            vm.nsdTokens.bind("change", self.validate);
        },
        initUI: function(navaTypes, allNavaOptions) {

            $("#cbx-navatype").kendoDropDownList({
                dataTextField: "Text",
                dataValueField: "Value",
                dataSource: navaTypes,
                change: function (e) {
                    vm.nsdTokens.navaOption.Value = allNavaOptions[0].Value;
                    self.changeOptionsSelection(allNavaOptions, this.text());
                },
                index: 0
            });

            self.changeOptionsSelection(allNavaOptions, self.navaTypes[0].Text);

            $("#frm-navaids").kendoValidator({
                messages: {
                },
                rules: {
                }
            });
        },

        changeOptionsSelection: function (allNavaOptions, navaType) {

            self.navaOptions.splice(0, self.navaOptions.length);
            for (var i = 0; i < allNavaOptions.length; i++) {
                if (allNavaOptions[i].Group === navaType)
                    self.navaOptions.push(allNavaOptions[i]);
            }

            vm.nsdTokens.navaOption.Text = allNavaOptions[0].Text;
            vm.nsdTokens.navaOption.Group = allNavaOptions[0].Group;

            var kDropDownList = $("#cbx-navaoption");

            kDropDownList.kendoDropDownList({
                dataTextField: "Text",
                dataValueField: "Value",
                dataSource: self.navaOptions,
                change: function (e) {
                },
                index: 0
            });

            var data = kDropDownList.data('kendoDropDownList');
            data.value(self.navaOptions[0]);
            data.select(0);
        },
        loadSdoData: function () {
            //vm.nsdTokens.set("rwyDesgCode", vm.subject.Designator.split("-")[1]);
            vm.NOTAM.set("ItemA", vm.subject.Designator.match(/\d+/g) !== null ? "CXXX" : vm.subject.Designator);
        },
        validate: function (e) {
            vm.validModel = kendoValidator.validate();
            vm.generateNOTAM("/NSD/NavaIds/V0R1/GenerateNotam");
        }
    };
});
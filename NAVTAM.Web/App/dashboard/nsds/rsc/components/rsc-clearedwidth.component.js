"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClearedWidthComponent = void 0;
var core_1 = require("@angular/core");
var angular_l10n_1 = require("angular-l10n");
var forms_1 = require("@angular/forms");
var rsc_model_1 = require("../rsc.model");
var rsc_service_1 = require("../rsc.service");
var toastr_service_1 = require("../../../../common/toastr.service");
var ClearedWidthComponent = /** @class */ (function () {
    function ClearedWidthComponent(fb, translation, rscService, toastr) {
        this.fb = fb;
        this.translation = translation;
        this.rscService = rscService;
        this.toastr = toastr;
        this.mainForm = null;
        this.model = null;
        this.isReadOnly = false;
        this.clearedType = 0;
        this.cardinalOptions = {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('ClearedWidthComponent.Select')
            }
        };
        this.firstOffset = null;
        this.secondOffset = null;
        this.rwyWidth = 0;
    }
    ClearedWidthComponent.prototype.ngOnInit = function () {
        this.isFormLoadingData = false;
        this.cardinalValues = this.model.parent.cardinalSideEnum;
        this.cardinalPointsByDirections = this.model.parent.cardinalPointsByDirections;
        this.firstOffset = this.model.clearedPortionOffsets[0];
        this.secondOffset = this.model.clearedPortionOffsets[1];
        this.rwyWidth = +this.model.runway.runwayWidthInFeet;
        this.configureReactiveForm();
    };
    ClearedWidthComponent.prototype.ngOnChanges = function () {
    };
    ClearedWidthComponent.prototype.ngOnDestroy = function () {
    };
    ClearedWidthComponent.prototype.$ctrl = function (name) {
        return this.frmGroup.get(name);
    };
    ClearedWidthComponent.prototype.clearedTypeChanged = function (value) {
        this.clearedType = value;
        if (this.clearedType === 0) {
            this.$ctrl('centredClearedWidth').patchValue(0);
            this.firstOffset.offsetFT = 0;
            this.firstOffset.offsetSide = rsc_model_1.CardinalSideEnum.E;
            this.secondOffset.offsetFT = 0;
            this.secondOffset.offsetSide = rsc_model_1.CardinalSideEnum.E;
            this.model.clearedFull = true;
            this.model.centredClearedWidth = 0;
            this.model.clearedPortionOffsets = [];
            this.model.clearedPortionOffsets.push(this.firstOffset);
            this.model.clearedPortionOffsets.push(this.secondOffset);
        }
        else if (this.clearedType === 1) {
            this.firstOffset.offsetFT = 0;
            this.firstOffset.offsetSide = rsc_model_1.CardinalSideEnum.E;
            this.secondOffset.offsetFT = 0;
            this.secondOffset.offsetSide = rsc_model_1.CardinalSideEnum.E;
            this.model.clearedFull = false;
            this.model.clearedPortionOffsets = [];
            this.model.clearedPortionOffsets.push(this.firstOffset);
            this.model.clearedPortionOffsets.push(this.secondOffset);
        }
        else {
            this.$ctrl('centredClearedWidth').patchValue(0);
            this.model.clearedFull = false;
            this.model.centredClearedWidth = 0;
            this.model.clearedPortionOffsets = [];
            this.model.clearedPortionOffsets.push(this.firstOffset);
            this.model.clearedPortionOffsets.push(this.secondOffset);
        }
    };
    ClearedWidthComponent.prototype.configureReactiveForm = function () {
        var frmArrControls = this.mainForm.controls['frmClearedWithArr'];
        this.frmGroup = this.fb.group({
            clearedType: [{ value: this.clearedType, disabled: this.isReadOnly }, []],
            centredClearedWidth: [{ value: this.model.centredClearedWidth, disabled: this.isReadOnly }, []],
            grpFirstOffset: this.fb.group({
                offsetFT: this.firstOffset.offsetFT,
                offsetSide: this.firstOffset.offsetSide,
            }),
            grpSecondOffset: this.fb.group({
                offsetFT: this.secondOffset.offsetFT,
                offsetSide: this.secondOffset.offsetSide,
            }),
        });
        frmArrControls.push(this.frmGroup);
    };
    ClearedWidthComponent.prototype.onOffsetFTChange = function (groupName) {
        if (groupName === 'first') {
            this.firstOffset.offsetFT = this.$ctrl('grpFirstOffset.offsetFT').value;
        }
        else {
            this.secondOffset.offsetFT = this.$ctrl('grpSecondOffset.offsetFT').value;
        }
    };
    ClearedWidthComponent.prototype.onOffsetSideChanged = function (groupName, e) {
        if (e.value) {
            var cardinalId = e.value;
            if (groupName === 'first') {
                var control = this.frmGroup.get('grpFirstOffset.offsetSide');
                if (this.isFormLoadingData) {
                    cardinalId = control.value;
                }
                else {
                    if (cardinalId !== control.value) {
                        control.setValue(cardinalId);
                        control.markAsDirty();
                        this.firstOffset.offsetSide = cardinalId;
                    }
                }
            }
            else {
                var control = this.frmGroup.get('grpSecondOffset.offsetSide');
                if (this.isFormLoadingData) {
                    cardinalId = control.value;
                }
                else {
                    if (cardinalId !== control.value) {
                        control.setValue(cardinalId);
                        control.markAsDirty();
                        this.secondOffset.offsetSide = cardinalId;
                    }
                }
            }
        }
    };
    ClearedWidthComponent.prototype.validaClearedWidth = function () {
        if (this.clearedType === 0)
            return true;
        else if (this.clearedType === 1 && this.model.centredClearedWidth < this.rwyWidth)
            return true;
        else if (this.clearedType === 2) {
            var totalCleared = this.firstOffset.offsetFT + this.secondOffset.offsetFT;
            if (totalCleared < this.rwyWidth)
                return true;
        }
        return false;
    };
    //Export functions
    ClearedWidthComponent.prototype.isFormValid = function () {
        if (!this.validaClearedWidth())
            return false;
        //Other validations
        return true;
    };
    ClearedWidthComponent.prototype.exportComponentInternalData = function () {
        var data = {
            parent: this.model.parent,
            runway: this.model.runway,
            clearedFull: false,
            centredClearedWidth: 0,
            clearedPortionOffsets: [],
        };
        if (this.clearedType === 0)
            data.clearedFull = true;
        else if (this.clearedType === 1) {
            data.centredClearedWidth = +this.$ctrl('centredClearedWidth').value;
        }
        else {
            data.clearedPortionOffsets.push(this.firstOffset);
            data.clearedPortionOffsets.push(this.secondOffset);
        }
        return data;
    };
    ClearedWidthComponent.prototype.clearValues = function () {
        this.clearedTypeChanged(0);
    };
    __decorate([
        core_1.Input('form'),
        __metadata("design:type", forms_1.FormGroup)
    ], ClearedWidthComponent.prototype, "mainForm", void 0);
    __decorate([
        core_1.Input('model'),
        __metadata("design:type", Object)
    ], ClearedWidthComponent.prototype, "model", void 0);
    __decorate([
        core_1.Input('isReadOnly'),
        __metadata("design:type", Boolean)
    ], ClearedWidthComponent.prototype, "isReadOnly", void 0);
    ClearedWidthComponent = __decorate([
        core_1.Component({
            selector: 'rsc-cleared-width',
            templateUrl: './app/dashboard/nsds/rsc/components/rsc-clearedwidth.component.html'
        }),
        __param(3, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [forms_1.FormBuilder,
            angular_l10n_1.TranslationService,
            rsc_service_1.RscService, Object])
    ], ClearedWidthComponent);
    return ClearedWidthComponent;
}());
exports.ClearedWidthComponent = ClearedWidthComponent;
//# sourceMappingURL=rsc-clearedwidth.component.js.map
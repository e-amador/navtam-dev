﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    using NavCanada.Core.Domain.Model.Dtos;
    using NavCanada.Core.Domain.Model.Enums;

    public class NotamSubjectViewModel
    {
        public string Name { get; set; }
        public DashboardItemType SubjectCategory { get; set; }
        public string SubjectType { get; set; }
        public string SubjectId { get; set; }
        public string Designator { get; set; }
        public string Fir { get; set; }
        public string Ad { get; set; }
        public string AdName { get; set; }
        public MapDataDto SubjectGeoJson { get; set; }
        public double SubjectGeoRefLong { get; set; }
        public double SubjectGeoRefLat { get; set; }
        public string SubjectMid { get; set; }
        public string SdoAttributes { get; set; }
        
    }
}
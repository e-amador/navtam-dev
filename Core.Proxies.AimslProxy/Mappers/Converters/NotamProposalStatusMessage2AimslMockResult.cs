﻿using System;
using AutoMapper;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AimslProxy.Common;


namespace NavCanada.Core.Proxies.AimslProxy.Mappers.Converters
{
    public class NotamProposalStatusMessage2AimslMockResultMock : ITypeConverter<AimslServiceMock.NotamProposalStatusMessage, AismlResponse>
    {
        public AismlResponse Convert(ResolutionContext context)
        {
            AimslServiceMock.NotamProposalStatusMessage source = context.SourceValue as AimslServiceMock.NotamProposalStatusMessage;

            var result = new AismlResponse
            {
                Code = ApiStatusCode.Success,
                ProposalStatus = new NotamProposalStatus
                {
                    Status = Mapper.Map<AimslServiceMock.NotamProposalStatusType, NotamProposalStatusCode>(source.Status),
                    RejectionReason = source.RejectionReason,
                    StatusDateTime = DateTime.Now,
                    ProposalId = source.NotamProposalId.ToString(),
                    InoNotamProposalId = source.NotamProposalId,
                    SubmittedNotam = Mapper.Map<AimslServiceMock.Notam, Domain.Model.Entitities.Notam>(source.SubmittedNotam)
                }
            };

            return result;
        }
    }
}

﻿import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { XsrfTokenService } from '../../../common/xsrf-token.service'
import { WindowRef } from '../../../common/windowRef.service'

export interface IObstacleLgUsType {
    id: string,
    name: string
}

export interface IInDoaValidationResultLgUs {
    location: string,
    inDoa: boolean
}

@Injectable()
export class ObstacleLgUsService {
    constructor(private http: Http, winRef: WindowRef, private xsrfTokenService: XsrfTokenService) {
        this.app = winRef.nativeWindow['app'];
        if (this.app.antiForgeryTokenValue) {
            this.xsrfTokenService.addForgeryTokenToHeader(this.app.antiForgeryTokenValue);
        }
    }

    public app: any;

    getObstacleTypes(): Observable<IObstacleLgUsType[]> {
        return this.http
            .get(`${this.app.apiUrl}obstacles/simple`)
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    validateLocation(location: string): Observable<IInDoaValidationResultLgUs> {
        return this.http
            .get(`${this.app.apiUrl}doas/indoa?location=${location}`)
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    getDoaLocation(doaId: number): Observable<any> {
        return this.http.get(`${this.app.apiUrl}doas/GeoJson/?doaId=${doaId}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    private handleError(error: Response) {
        return Observable.throw(error.statusText);
    }
}
﻿namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    using System.Threading.Tasks;

    using Domain.Model.Entitities;

    public interface IDigitalSlaRepo : IRepository<DigitalSla>
    {
        Task<DigitalSla> GetLastVersionAsync();
        Task<DigitalSla> GetByIdAsync(int id);
        DigitalSla GetLastVersion();
    }
}

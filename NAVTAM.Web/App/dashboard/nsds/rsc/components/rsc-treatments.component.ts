﻿import { Component, OnInit, OnChanges, OnDestroy, Inject, Input } from '@angular/core';
import { TranslationService } from 'angular-l10n';
import { FormGroup, FormBuilder, FormArray, AbstractControl, Validators } from '@angular/forms';
import { RscService } from '../rsc.service';
import { ITreatmentsModel } from '../rsc.model';

import { TOASTR_TOKEN, Toastr } from '../../../../common/toastr.service';

@Component({
    selector: 'rsc-treatments',
    templateUrl: './app/dashboard/nsds/rsc/components/rsc-treatments.component.html'
})
export class RscTreatmentsComponent implements OnInit, OnChanges, OnDestroy {
    @Input('form') mainForm: FormGroup = null;
    @Input('model') model: ITreatmentsModel = null;
    @Input('isReadOnly') isReadOnly: boolean = false;

    frmGroup: FormGroup;
    isFormLoadingData: boolean;

    constructor(
        private fb: FormBuilder,
        public translation: TranslationService,
        private rscService: RscService,
        @Inject(TOASTR_TOKEN) private toastr: Toastr) {
    }

    ngOnInit() {
        this.isFormLoadingData = false;

        this.configureReactiveForm();

        if (!this.model.sand) {
            this.$ctrl('sandTimeApplied').disable();
        }
        if (!this.model.chemicallyTreated) {
            this.$ctrl('chemicalTimeApplied').disable();
        }
    }

    ngOnChanges() {
    }

    ngOnDestroy() {
    }

    $ctrl(name: string): AbstractControl {
        return this.frmGroup.get(name);
    }

    private configureReactiveForm() {
        const frmArrControls = <FormArray>this.mainForm.controls['frmTreatmentsArr'];

        this.frmGroup = this.fb.group({
            sandTreated: [{ value: this.model.sand, disabled: this.isReadOnly }, []],
            sandTimeApplied: [{ value: this.model.sandTimeApplied, disabled: false }, []],
            chemicallyTreated: [{ value: this.model.chemicallyTreated, disabled: this.isReadOnly }, []],
            chemicalTimeApplied: [{ value: this.model.chemicalTimeApplied, disabled: false }, []],
            remarks: [{ value: this.model.remarks, disabled: this.isReadOnly }, []],
        });

        frmArrControls.push(this.frmGroup);

    }

    sandTreatedChangingClick(): void {
        this.model.sand = !this.model.sand;
        let ctrl = this.$ctrl('sandTimeApplied');
        if (!this.isSandTimeAppliedEnabled()) {
            this.model.sandTimeApplied = '';
            ctrl.patchValue(this.model.sandTimeApplied);
            ctrl.disable();
        }
        else ctrl.enable();
    }

    setSandTimeApplied(): void {
    }

    chemicallyTreatedChangingClick(): void {
        this.model.chemicallyTreated = !this.model.chemicallyTreated;
        let ctrl = this.$ctrl('chemicalTimeApplied');
        if (!this.isChemicalTimeAppliedEnabled()) {
            this.model.chemicalTimeApplied = '';
            ctrl.patchValue(this.model.chemicalTimeApplied);
            ctrl.disable();
        }
        else ctrl.enable();
    }

    setChemicalTimeApplied(): void {
    }

    isSandTimeAppliedEnabled(): boolean {
        return !this.isReadOnly && this.model.sand; 
    }

    isChemicalTimeAppliedEnabled(): boolean {
        return !this.isReadOnly && this.model.chemicallyTreated;
    }
    
    public validateKeyPressHours(evt: any, ctrlName: string) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var num = +key;
            var pos: number = (<any>evt).currentTarget.selectionStart;
            if (pos === 0) { //val.length
                //First number
                if (key !== '0' && key !== '1' && key !== '2') {
                    evt.returnValue = false;
                    if (evt.preventDefault) evt.preventDefault();
                    return;
                }
            } else if (pos === 1) { //val.length === 1
                //Second number
                const fc = this.$ctrl(ctrlName);
                const val: string = fc.value;
                if (val[0] === '2' && num > 3) {
                    evt.returnValue = false;
                    if (evt.preventDefault) evt.preventDefault();
                    return;
                }
            } else if (pos === 2) { //val.length === 2
                //Third number
                if (num > 5) {
                    evt.returnValue = false;
                    if (evt.preventDefault) evt.preventDefault();
                    return;
                }
            }
        }
    }


    //Export functions
    public isFormValid(): boolean {

        return true;
    }

    public exportComponentInternalData(): ITreatmentsModel {
        let data: ITreatmentsModel = {
            parent: this.model.parent,
            sand: this.model.sand,
            sandTimeApplied: this.model.sandTimeApplied,
            chemicallyTreated: this.model.chemicallyTreated,
            chemicalTimeApplied: this.model.chemicalTimeApplied,
            remarks: this.model.remarks,
        };
        return data;
    }

    public clearValues(): void {
        alert('Clear Component Values: Treatments');
    }


}

function isANumber(str: string): boolean {
    return !/\D/.test(str);
}

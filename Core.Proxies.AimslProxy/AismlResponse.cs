﻿namespace NavCanada.Core.Common.Responses
{
    using Domain.Model.Entitities;
    using Domain.Model.Enums;

    public class AismlResponse
    {
        public ApiStatusCode Code { get; set; }

        public string ErrorMessage { get; set; }

        public NotamProposalStatus ProposalStatus { get; set; }
    }
}

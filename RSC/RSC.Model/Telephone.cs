﻿namespace RSC.Model
{
    /// <summary>
    /// Telephone Number
    /// </summary>
    public class Telephone
    {
        /// <summary>
        /// Three digits area code
        /// </summary>
        public string AreaCode { get; set; }
        /// <summary>
        /// Seven digit number.
        /// </summary>
        public string Number { get; set; }
        /// <summary>
        /// Extension.
        /// </summary>
        public string Extension { get; set; }
    }
}

﻿using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.Contracts.Repositories
{
    public interface INdsClientItemARepo : IRepository<NdsClientItemA>
    {
        Task<List<NdsClientItemA>> GetByClientIdAsync(int clientId);
    }
}

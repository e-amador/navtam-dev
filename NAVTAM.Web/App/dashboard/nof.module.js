"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NofModule = exports.initLocalization = exports.LocalizationConfig = void 0;
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var ng2_select2_1 = require("ng2-select2");
var ng2_date_time_picker_1 = require("../common/components/ng2-date-time-picker");
//import { StartupService } from './startup.service';
var angular_l10n_1 = require("angular-l10n");
var index_1 = require("../common/index");
var nof_component_1 = require("./nof.component");
var nof_index_1 = require("./nof.index");
var nof_routes_1 = require("./nof.routes");
//declare let $: Object;
//export function startupServiceFactory(startupService: StartupService): Function {
//    return () => startupService.load();
//}
//export function createConfig(): SignalRConfiguration {
//    const c = new SignalRConfiguration();
//    c.hubName = 'SignalRHub';
//    //c.qs = { user: 'user.subscribed' };
//    c.url = window.location.origin;
//    c.logging = true;
//    return c;
//}
var LocalizationConfig = /** @class */ (function () {
    function LocalizationConfig(locale, translation) {
        this.locale = locale;
        this.translation = translation;
    }
    LocalizationConfig.prototype.load = function () {
        var _this = this;
        this.locale.addConfiguration()
            .addLanguage('en', 'ltr')
            .addLanguage('fr', 'ltr')
            //.setCookieExpiration(30)
            .defineLanguage(window['app'].cultureInfo)
            .defineCurrency("CAD")
            .defineDefaultLocale('en', 'CA');
        this.locale.init();
        this.translation.addConfiguration()
            .addProvider('./app/resources/locale-');
        var promise = new Promise(function (resolve) {
            _this.translation.translationChanged.subscribe(function () {
                resolve(true);
            });
        });
        this.translation.init();
        return promise;
    };
    LocalizationConfig = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [angular_l10n_1.LocaleService, angular_l10n_1.TranslationService])
    ], LocalizationConfig);
    return LocalizationConfig;
}());
exports.LocalizationConfig = LocalizationConfig;
// AoT compilation requires a reference to an exported function.
function initLocalization(localizationConfig) {
    return function () { return localizationConfig.load(); };
}
exports.initLocalization = initLocalization;
var NofModule = /** @class */ (function () {
    function NofModule() {
    }
    NofModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                http_1.HttpModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                router_1.RouterModule.forRoot(nof_routes_1.nofRoutes),
                ng2_select2_1.Select2Module,
                ng2_date_time_picker_1.DateTimePickerModule,
                index_1.Ng2ICheckModule,
                //SignalRModule.forRoot(createConfig),
                angular_l10n_1.LocalizationModule.forRoot() // New instance of TranslationService
            ],
            declarations: [
                nof_component_1.NofComponent,
                //Shared
                index_1.DatexPipe,
                index_1.DateTimeFormatDirective,
                index_1.ModalTriggerDirective,
                index_1.SimpleModalComponent,
                index_1.ServerPaginationComponent,
                nof_index_1.ReportNotamComponent,
                nof_index_1.ReportStatsComponent,
                nof_index_1.ReportContainerComponent,
                nof_index_1.ReportFilterComponent,
                nof_index_1.ReportActiveComponent,
                //NOF,
                nof_index_1.NsdFormComponent,
                nof_index_1.NsdButtonBarComponent,
                nof_index_1.NsdGeoRefToolComponent,
                nof_index_1.NsdIcaoComponent,
                nof_index_1.NsdIcaoFormatComponent,
                nof_index_1.NsdSchedulerDailyComponent,
                nof_index_1.NsdSchedulerDateComponent,
                nof_index_1.NsdSchedulerDaysOfWeekComponent,
                nof_index_1.NsdSchedulerFreeTextComponent,
                //NOF,
                nof_index_1.NofPortalComponent,
                nof_index_1.NofQueueComponent,
                nof_index_1.NofPickComponent,
                nof_index_1.NofCreateComponent,
                nof_index_1.NofViewComponent,
                nof_index_1.NofViewHistoryComponent,
                nof_index_1.NofNotamComponent,
                nof_index_1.NofNotamFilterComponent,
                //NofMsgComponent,
                nof_index_1.NofMsgQueueComponent,
                nof_index_1.NofMsgComposeComponent,
                nof_index_1.NofMsgReadComponent,
                nof_index_1.NofMsgTemplateListComponent,
                nof_index_1.NofMsgTemplateCreateComponent,
                nof_index_1.NofMsgTemplateEditComponent,
                nof_index_1.SubscriptionFilterPipe,
                nof_index_1.DynamicComponent,
                // CatchAll
                nof_index_1.CatchAllDraftComponent,
                nof_index_1.CatchAllCancelComponent,
                // Obstacle
                nof_index_1.ObstComponent,
                nof_index_1.ObstCancelComponent,
                // Obstacle LG US
                nof_index_1.ObstLgUsComponent,
                nof_index_1.ObstLgUsCancelComponent,
                //Multiple Obstacle
                nof_index_1.MObstComponent,
                nof_index_1.MObstCancelComponent,
                //Multiple Obstacle LG US
                nof_index_1.MObstLgUsComponent,
                nof_index_1.MObstLgUsCancelComponent,
                // Runway closed
                nof_index_1.RwyClsdComponent,
                nof_index_1.RwyClsdCancelComponent,
                // Taxyway closed
                nof_index_1.TwyClsdComponent,
                nof_index_1.TwyClsdCancelComponent,
                // CARS closed
                nof_index_1.CarsClsdComponent,
                nof_index_1.CarsClsdCancelComponent,
                // Area Definition
                nof_index_1.AreaDefinitionNsdComponent,
                nof_index_1.AreaDefinitionCancelNsdComponent,
                //Rsc
                nof_index_1.RscComponent,
                nof_index_1.RscContaminantsComponent,
                nof_index_1.ClearedWidthComponent,
                nof_index_1.ClearingOperationsComponent,
                nof_index_1.SnowBanksComponent,
                nof_index_1.RscTreatmentsComponent,
                nof_index_1.RwyFrictionComponent,
                nof_index_1.RemainingWidthRemarksComponent,
                nof_index_1.NextObservationComponent,
                nof_index_1.WindrowsComponent,
                nof_index_1.TaxiwaysRemarksComponent,
                nof_index_1.ApronRemarksComponent,
            ],
            providers: [
                nof_index_1.DataService,
                nof_index_1.NotifyService,
                nof_index_1.MemoryStorageService,
                nof_index_1.LocationService,
                nof_index_1.CatchAllService,
                nof_index_1.ObstacleService,
                nof_index_1.ObstacleLgUsService,
                nof_index_1.MObstService,
                nof_index_1.RwyClsdService,
                nof_index_1.TwyClsdService,
                nof_index_1.CarsClsdService,
                nof_index_1.AreaDefinitionNsdService,
                nof_index_1.RscService,
                nof_index_1.CategoriesResolver,
                nof_index_1.ProposalModelResolver,
                nof_index_1.ProposalReadOnlyModelResolver,
                nof_index_1.NotamProposalReadOnlyModelResolver,
                nof_index_1.ProposalCreateModelResolver,
                nof_index_1.ProposalCancelModelResolver,
                nof_index_1.ProposalReplaceModelResolver,
                nof_index_1.ProposalCloneModelResolver,
                nof_index_1.NotamCloneModelResolver,
                nof_index_1.ProposalHistoryModelResolver,
                nof_index_1.NofMessageResolver,
                nof_index_1.NofMessageTemplateResolver,
                //StartupService,
                LocalizationConfig,
                {
                    provide: core_1.APP_INITIALIZER,
                    useFactory: initLocalization,
                    deps: [LocalizationConfig],
                    multi: true
                },
                //common
                //{
                //    // Provider for APP_INITIALIZER
                //    provide: APP_INITIALIZER,
                //    useFactory: startupServiceFactory,
                //    deps: [StartupService],
                //    multi: true
                //},
                { provide: index_1.TOASTR_TOKEN, useValue: toastr },
                { provide: 'canDeactivateCreateEvent', useValue: checkDirtyState },
                index_1.XsrfTokenService,
                index_1.WindowRef
            ],
            bootstrap: [nof_component_1.NofComponent]
        })
    ], NofModule);
    return NofModule;
}());
exports.NofModule = NofModule;
function checkDirtyState(component) {
    var el = $('#modal-unsave-changes');
    var $modal = el.modal();
    return new Promise(function (resolve, reject) {
        var isRSC = (component.nsdForm.nsdForm === undefined);
        if (isRSC) {
            var comp = component;
            comp = comp;
            if (!comp.isSaveButtonDisabled && comp.nsdForm.dirty) {
                var alreadyResolved_1 = false;
                $("#btn-yes").click(function () {
                    resolve(true);
                    alreadyResolved_1 = true;
                });
                $("#btn-no").click(function () {
                    resolve(false);
                    alreadyResolved_1 = true;
                });
                el.on('hide.bs.modal', function (e) {
                    if (!alreadyResolved_1) {
                        resolve(false);
                    }
                });
                $modal.modal("show");
            }
            else {
                resolve(true);
            }
        }
        else {
            if (!component.nsdForm.isSaveButtonDisabled && component.nsdForm.nsdForm.dirty) {
                var alreadyResolved_2 = false;
                $("#btn-yes").click(function () {
                    resolve(true);
                    alreadyResolved_2 = true;
                });
                $("#btn-no").click(function () {
                    resolve(false);
                    alreadyResolved_2 = true;
                });
                el.on('hide.bs.modal', function (e) {
                    if (!alreadyResolved_2) {
                        resolve(false);
                    }
                });
                $modal.modal("show");
            }
            else {
                resolve(true);
            }
        }
        //if (!component.nsdForm.isSaveButtonDisabled && component.nsdForm.nsdForm.dirty) {
        //    let alreadyResolved = false;
        //    $("#btn-yes").click(function () {
        //        resolve(true);
        //        alreadyResolved = true;
        //    });
        //    $("#btn-no").click(function () {
        //        resolve(false);
        //        alreadyResolved = true;
        //    });
        //    el.on('hide.bs.modal', function (e) {
        //        if (!alreadyResolved) { resolve(false); }
        //    })
        //    $modal.modal("show");
        //}
        //else {
        //    resolve(true);
        //}
    });
}
//# sourceMappingURL=nof.module.js.map
﻿import { Select2OptionData } from 'ng2-select2';

export interface IQueryOptions {
    entity?: string,
    page: number,
    pageSize: number,
    sort: string,
    filterValue?: string,
    filterField?: string,
}

export interface IADCQueryOptions {
    entity?: string,
    page: number,
    pageSize: number,
    sort: string,
    filterValue?: string,
    filterField?: string,
    lastId: number,
}

export interface IUserQueryOptions {
    entity?: string,
    page: number,
    pageSize: number,
    sort: string,
    includeDisabled: boolean,
    includeDeleted: boolean,
    filterValue?: string,
    filterField?: string,
}

export interface IPaging {
    currentPage: number,
    pageSize: number,
    totalCount: number,
    totalPages: number,
    nextPageLink?: string,
    previuosPageLink?: string,
}

export interface ISorting {
    columnName: string;
    direction: number; // -1|0|+1
}

export interface ISelectOptions {
    id: string,
    text: string
}

export interface ITableHeaders {
    sorting: ISorting[],
    itemsPerPage: Select2OptionData[]
}

export interface IMemoryStorage {
    key: string,
    value: any
}

export interface IUserPartial {
    id: string,
    username: string,
    firstName: string,
    lastName: string,
    organization: string,
    roles: string,
    disabled: boolean,
    deleted: boolean,
    isSelected: boolean
}

export interface IUsersDelete {
    usernames: string[]
}

export interface IOrgPartial {
    id: number,
    name: string,
    address: string,
    emailAddress: string,
    phoneNumber: string,
    operType: string,
    emailDistribiution: string,
    isSelected: boolean
}

// Series
export interface ISeriesPartial {
    id?: number,
    subject: string,
    regionName: string,
    regionId: string,
    series: string,
    disseminationCategory: string,
    disseminationCategoryName: string,
    qCode: string,
    isSelected?: boolean
}
//End series

export interface ISelectOptions {
    id: string,
    text: string
}

//Aerodrome Dissemination Category
export interface IAerodromeDisseminationCategory {
    id: number,
    ahpMid: string,
    ahpCodeId: string,
    ahpName: string,
    disseminationCategory: string,
    disseminationCategoryName: string,
    latitude: number,
    longitude: number,
    servedCity: string,
    isSelected?: boolean
}
export interface IAhpNoDC {
    id: string,
    mid: string,
    designator: string,
    name: string,
    fir: string,
    latitude: number,
    longitude: number,
    servedCity: string,
    isSelected?: boolean
}
//End Aerodrome dissemination Category

export interface IIcaoSubjectPartial {
    id: number;
    name: string,
    nameFrench: string,
    entityCode: string,
    code: string,
    scope: string,
    requiresItemA: boolean,
    requiresScope: boolean,
    requiresSeries: boolean,
    isSelected?: boolean
}

export interface IIcaoSubjectConditionPartial {
    id: number;
    description: string,
    descriptionFrench: string,
    code: string,
    traffic: string,
    purpose: string,
    lower: number,
    upper: number,
    radius: number,
    requiresItemFG: boolean,
    requiresPurpose: boolean,
    cancellationOnly: boolean,
    isActive: boolean,
    isSelected: boolean
}

export interface IIcaoSubjectCondition {
    id: number;
    subjectId: number,
    description: string,
    descriptionFrench: string,
    code: string,
    lower: number,
    upper: number,
    radius: number,
    i: boolean,
    v: boolean,
    n: boolean,
    b: boolean,
    o: boolean,
    m: boolean,
    requiresItemFG: boolean,
    requiresPurpose: boolean,
    cancellationOnly: boolean,
    active: boolean,
    isSelected?: boolean
}

//Geographic Region

export interface IGeoRegionPartial {
    id: number,
    name: string,
    isSelected: boolean
}

export interface IGeoRegion {
    id: number,
    name: string,
    region: IDoaRegion,
    changes?: IDoaPointModel[]
}
//End Geographic Region

export class RegionSource {
    static None: number = 0;
    static Geography: number = 1;
    static FIR: number = 2;
    static Points: number = 3;
    static PointAndRadius: number = 4;
}

export interface IRegionViewModel {
    source: RegionSource,
    geography: string,
    fir?: string,
    points?: string,
    location?: string,
    radius?: number
    geoFeatures?: string,
}

export interface IRegionMapRender {
    region: IRegionViewModel,
    changes?: IRegionPointModel[]
}

export interface IRegionPointModel {
    source: string,
    location: string,
    inDoa: boolean,
    radius?: number,
}

export interface INdsClient {
    id?: number,
    clientType: number,
    address: string,
    itemsA: string[],
    series: string[],
    french: boolean,
    client: string,
    allowQueries: boolean,
    active: boolean,
    isRegionSubscription: boolean,
    region: IRegionViewModel,
    lowerLimit: number,
    upperLimit: number,
}

export interface INdsClientPartial {
    id: number,
    clientType: number,
    clientTypeStr?: string,
    address: string,
    itemA: string,
    french: boolean,
    client: string,
    isSelected?: boolean,
    index?: number,
    subscriptionSelected?: boolean,
    updated: string,
    lastSynced: string
}

export interface INdsClientFilter {
    clientTypeStr?: string,
    clientType?: number,
    address: string,
    client: string,
    isSelected?: boolean,
    index?: number,
}

export class ClientType {
    static AFTN = 0;
    static SWIM = 1;
    static REST = 2;
}

export interface IDoaPartial {
    id: number,
    name: string
}

export interface IDoaAdminPartial {
    id: number,
    name: string,
    description: string,
    organizations: string,
    orgCount: number,
    isSelected: boolean
}

export interface IDoaAdmin {
    id: number,
    name: string,
    description: string,
    region: IDoaRegion,
    doaType: RegionType,
    changes?: IDoaPointModel[]
}

export class DoaRegionSource {
    static None: number = 0;
    static Geography: number = 1;
    static FIR: number = 2;
    static Points: number = 3;
    static PointAndRadius: number = 4;
}

export interface IFIR {
    id: string,
    name: string,
    designator: string
}

export interface IRegionMapRender {
    region: IDoaRegion,
    changes?: IDoaPointModel[]
}

export interface IDoaRegion {
    source: DoaRegionSource,
    geography: string,
    fir?: string,
    points?: string,
    location?: string,
    radius?: number
    geoFeatures?: string,
}

export interface IDoaPointModel {
    source: string,
    location: string,
    inDoa: boolean,
    radius?: number,
}

export interface IRegionLocationModel {
    region: IDoaRegion,
    location: string
}

export interface IUser {
    username: string,
    position: string,
    address: string,
    fax: string,
    phoneNumber: string,
    firstName: string,
    lastName: string,
    email: string,
    approved: boolean,
    disabled: boolean,
    deleted: boolean,
    password?: string,
    confirmPassword?: string,
    roleId: string,
    organizationId: number,
    doaIds: string
}

export interface IRole {
    roleId: string,
    roleName: string
}

export interface IOrganization {
    id?: string,
    name: string,
    address: string,
    emailAddress: string,
    emailDistribution: string,
    telephone: string,
    typeOfOperation: string,
    isReadOnlyOrg?: boolean,
    headOffice?: string,
    assignedDoaIds?: number[],
    assignedNsdIds?: number[],
    type?: number,
    adminUserName?: string,
    adminFirstName?: string,
    adminLastName?: string,
    adminPassword?: string,
    adminConfirmPassword?: string,
    isSelected? : boolean
}

export interface INsdPartial {
    id: number,
    name: string
}

export interface INsdCategories {
    name: string,
    ParentCategory: string,
    id?: string
}

export interface INsdNodeTree {
    Id?: string;
    GroupCategoryId?: string;
    Name: string;
    Text: string;
    isSelected: boolean;
    Children: INsdNodeTree[];
}

export interface INsdManagementDto {
    id: string;
    Name: string;
    isSelected: boolean;
    orgId: string;
    operational: boolean;
}

export interface IConfigValuePartial {
    name: string,
    description: string,
    value: string,
    category: string,
    action: string,
    isSelected?: boolean
}

export interface IConfigValuePair {
    name: string,
    category: string,
}

export interface ISeriesChecklist {
    series: string,
    firs: string,
    coordinates: string,
    radius: number,
    isSelected?: boolean
}

export class RegionType {
    static Doa: number = 0;
    static Bilingual: number = 1;
    static Northern: number = 2;
}

export class ProposalStatus {
    static Undefined = 0;

    // User Proposal status
    static Draft = 1;
    static Submitted = 2;
    static Withdrawn = 3;

    // NOF Proposal status
    static Picked = 10;
    static Parked = 11;
    static Disseminated = 12;
    static Rejected = 13;
    static SoontoExpire = 14;
    static Expired = 15;
    static ParkedDraft = 16;
    static Terminated = 17;
    static DisseminatedModified = 18;
    static ParkedPicked = 19;

    //Everybody
    static Cancelled = 20;
    static Replaced = 21;
    static Deleted = 22;
    static Discarded = 23;

    static Taken = 255;
}
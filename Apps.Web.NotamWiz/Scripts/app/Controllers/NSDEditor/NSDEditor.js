﻿define(["jquery", "utilities", "ProposalModel", "Resources", "Schedule", "bingMaps", "javaExt", "kendoExt"], function ($, Util, proposal, res, SCH, bingMap) {

    var toDate, reviewed, additionalInfoReviewed, dataValidator, scheduleErrorMessage, thisNSDEditor, timeSchedule;
    var labels = {};

    return {
        proposalVM: null,
        utcDiff: null,
        bingMap: null,
        conditionValidator: null,
        skipEarlyDateValidation: false,
        beginDateErrorMessage: "",

        init: function (lbls) {
            //debugger;
            labels = lbls;
            thisNSDEditor = this;

            // Setup ajax working overlay
            $(document).ajaxStart(function () {
                kendo.ui.progress($("#container"), true);
            });

            $(document).ajaxStop(function () {
                kendo.ui.progress($("#container"), false);
            });
            
            //RES = res.getResources("NSDEditor");
            //debugger;
            var model = $("#nsdEditordataModel").data("model");
            if (model.Error) {
                kendo.ui.ExtAlertDialog.show({
                    title: labels.nsEditor_error_label,
                    message: model.Message,
                    icon: "k-Ext-error"
                });
                $("#NSDEditor-Popup").data("kendoWindow").close();
                return;
            }

            thisNSDEditor.proposalVM = proposal.init(model, $("#nsdEditordataModel").data("user"), {});

            toDate = null;
            reviewed = false;
            additionalInfoReviewed = false;
            dataValidator = null;
            scheduleErrorMessage = "";

            timeSchedule = null;
            thisNSDEditor.bingMap = bingMap;
            if (thisNSDEditor.proposalVM.nsdTokens && thisNSDEditor.proposalVM.nsdTokens.ItemDChangedByNof) {
                $("#scheduleText").addClass("k-invalid");
                $("#scheduleText").val(labels.nsEditor_warningSchedule);
            }

            if (thisNSDEditor.proposalVM.IsFicRole === true) {
                $("#originatorPanel").show();
            }
            else {
                $("#originatorPanel").hide();
            }
            thisNSDEditor.proposalVM.NSDURL = '/NSD/' + thisNSDEditor.proposalVM.nsd.FileLocation + ('/V' + thisNSDEditor.proposalVM.nsd.Version).replace('.', 'R'); // + "/Index?id=" + thisNSDEditor.proposalVM.subject.SubjectId + "^" + thisNSDEditor.proposalVM.subject.SubjectType;
            var viewCodeFile = thisNSDEditor.proposalVM.NSDURL + "/GetScript";
            $.ajax({
                url: thisNSDEditor.proposalVM.NSDURL,
                //data: {
                //    id: thisNSDEditor.proposalVM.subject.SubjectId + "^" + thisNSDEditor.proposalVM.subject.SubjectType
                //},
                cache: false,
                async: false,
                type: "GET",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    $("#conditionContainerView").html(data);

                    //$.ajax({
                    //    url: thisNSDEditor.proposalVM.NSDURL + "/SubjectSelector",
                    //    cache: false,
                    //    async: false,
                    //    type: "GET",
                    //    dataType: "html",
                    //    success: function(data, textStatus, XMLHttpRequest) {
                    //        $("#subjectSelectorContainer").html(data);
                    //    }
                    //});


                    require([viewCodeFile], function (nsdCode) {
                        nsdCode.init(thisNSDEditor, thisNSDEditor.proposalVM);
                        if (thisNSDEditor.proposalVM.ReadOnly) {
                            $("#notamProposalForm :input").prop("disabled", true);
                            $("#notamProposalForm .k-button").remove();
                        }
                        thisNSDEditor.validateOnBehalfOfTab();
                    });
                }
            });

            thisNSDEditor.initUI();


        },

        initUI: function () {

            // bind NOTAM to ui
            kendo.bind($('#notamForm'), thisNSDEditor.proposalVM.NOTAM);
            kendo.bind($('#notamSimpleViewForm'), thisNSDEditor.proposalVM.NOTAM);
            kendo.bind($('#modifiedNotamForm'), thisNSDEditor.proposalVM.NOTAM.SubmittedNotam);

            var TimeScheduleAPIWindow = $("#TimeScheduleAPI-Popup").kendoWindow({
                height: 800,
                width: 800,
                resizable: false,
                visible: false,
                title: labels.nsEditor_activitySchedule,
                modal: true,
            }).data("kendoWindow");

            TimeScheduleAPIWindow.bind("refresh", function () {
                $("#TimeScheduleAPI-Popup").data("kendoWindow").center();
                $("#TimeScheduleAPI-Popup").data("kendoWindow").open();
            });


            var panelBar = $("#tabStrip").kendoPanelBar({
                activate: function (e) {


                    if ($(e.item).attr("aria-controls") === "tabStrip-4") {
                        if (thisNSDEditor.proposalVM.validModel) {
                            $("#reviewSectionIcon").attr("src", "/Images/yes.png");
                            reviewed = true;
                        }
                    }
                    else if ($(e.item).attr("aria-controls") === "tabStrip-5") {
                        $("#additionalInfoIcon").attr("src", "/Images/yes.png");
                        additionalInfoReviewed = true;
                    }
                    else if ($(e.item).attr("aria-controls") === "tabStrip-2") {
                        thisNSDEditor.validateActivePeriodTab();
                    }

                }
            });

            panelBar.data("kendoPanelBar").expand($(".k-item"), true);

            var files = $.map(thisNSDEditor.proposalVM.NOTAM.NotamProposalAttachments, function (elem, index) { return [{ user: elem.Owner, Id: elem.Id, name: elem.FileName }]; });

            $("#attachmentsFiles").kendoUpload({
                async: {
                    saveUrl: "/NsdEditor/SaveAttachement",
                    removeUrl: "/NsdEditor/RemoveAttachement",
                    autoUpload: true,
                },
                localization: {
                    select: labels.nsEditor_selectFiles_label,
                    cancel: labels.nsEditor_cancel_label,
                    dropFilesHere: labels.nsEditor_dropFilesHere_label,
                    headerStatusUploaded: labels.nsEditor_done_label,
                    headerStatusUploading: labels.nsEditor_uploading_message,
                    remove: labels.nsEditor_remove_label,
                    retry: labels.nsEditor_retry_label,
                    statusFailed: labels.nsEditor_failed_status,
                    statusUploaded: labels.nsEditor_uploaded_status,
                    statusUploading: labels.nsEditor_uploading_status,
                    statusWarning: labels.nsEditor_warning_status,
                    uploadSelectedFiles: labels.nsEditor_uploadFiles_title
                },
                enabled: thisNSDEditor.proposalVM.NOTAM.Id > 0,
                upload: function (e) {
                    //debugger;
                    e.data = {
                        proposalId: thisNSDEditor.proposalVM.NOTAM.Id,
                        fileIds: e.files.map(function (f) { return f.Id; })
                    };
                },
                complete: function (e) {

                },
                remove: function (e) {

                    e.data = { proposalId: thisNSDEditor.proposalVM.NOTAM.Id, fileId: e.files[0].Id };
                },
                select: function (e) {
                    var sizeOk = true;
                    $.each(e.files, function (k, v) {
                        v.user = thisNSDEditor.proposalVM.user;
                        v.Id = v.uid;
                        if (v.size > 1000000) {
                            sizeOk = false;
                        }
                    });

                    if (!sizeOk) {
                        kendo.ui.ExtAlertDialog.show({
                            title: labels.nsEditor_error_label,
                            message: labels.nsEditor_errorSizeAllowed,
                            icon: "k-Ext-warning"
                        });

                        e.preventDefault();

                    }



                    return sizeOk;
                },
                success: function (e) {

                    //e.files[0].Id = e.response.fileId;
                },
                files: files,
                multiple: true,
                showFileList: true,
                template: kendo.template($('#fileTemplate').html())
            });


            var textEditorInitialize = function (container, options) {
                $('<textarea name="' + options.field + '" style="width: ' + container.width() + 'px;height:' + container.height() + 'px" />')
                .appendTo(container);
            };


            $('#additionalInfoGrid').kendoGrid({
                editable: { mode: "incell", confirmation: false },
                toolbar: [{ name: "create", text: labels.nsEditor_newAnnotation_message }],
                dataBound: function (e) {
                    if (thisNSDEditor.proposalVM.ReadOnly) {
                        $("#additionalInfoGrid").find(".k-button").hide();
                    }
                },
                save: function (e) {
                    this.dataSource.sync();
                },
                dataSource: {
                    data: thisNSDEditor.proposalVM.NOTAM.NotamProposalsInfos,
                    schema: {
                        model: {
                            id: "Id",
                            fields: {
                                Id: { editable: false, nullable: false },
                                Owner: { editable: false, validation: { required: true }, defaultValue: thisNSDEditor.proposalVM.user },
                                Note: { editable: !thisNSDEditor.proposalVM.ReadOnly, validation: { required: true } },
                                CreationDate: { editable: false, type: "date", defaultValue: Util.getCurrentUTCTime() },
                            }
                        }
                    },
                    // pageSize: 20
                },
                cancel: function (e) { this.refresh(); },
                height: 200,
                scrollable: true,
                // sortable: true,
                //filterable: true,
                //pageable: {
                //     input: true,
                //    numeric: false
                // },
                columns: [

                    { field: "Owner", title: labels.nsEditor_fieldOwner_title, width: "75px" },
                    { field: "Note", title: labels.nsEditor_fieldNote_title, width: "~250px", editor: textEditorInitialize },
                    { field: "CreationDate", title: labels.nsEditor_fieldCreationDate_title, width: "110px", format: "{0:dd/MM/yyyy HH:mm}" },

                ]
            });

            $("#scheduleViewer").kendoScheduler({
                date: new Date("2013/6/6"),
                views: ["day", "week", "month"],
                editable: false,
                height: "100%",
                dataSource: [
                  {
                      id: 1,
                      start: new Date("2013/6/6 08:00 AM"),
                      end: new Date("2013/6/6 09:00 AM"),
                      isAllDay: true,
                      title: labels.nsEditor_NOTAM_title,
                      attendees: [1, 2]
                  }
                ],

            });

            // Create map.
            var mapOptions = {
                zoom: 5,
                enableClickableLogo: false,
                showTools: true,
            };

            if (thisNSDEditor.proposalVM.nsdTokens && thisNSDEditor.proposalVM.nsdTokens.Bilingual) {
                $(".eFieldFrench").show();
            } else {
                $(".eFieldFrench").hide();
            }

            bingMap.init($("#subjectLocationMap"), { showLoadingProgressBar: false }, this.loadMapEntity);
            $('#reviewTab').kendoTabStrip();


            var reviewTab = $("#reviewTab").data("kendoTabStrip");
            if (!thisNSDEditor.proposalVM.NOTAM.ModifiedByNof) {
                reviewTab = $("#reviewTab").data("kendoTabStrip");
                //if (reviewTab.items()) {
                    $(reviewTab.items()[2]).attr("style", "display:none");
                //}
            }
            reviewTab.enable(reviewTab.tabGroup.children().eq(1), thisNSDEditor.proposalVM.ReadOnly);


            thisNSDEditor.updateTitle();
            $("#beginDatetime").kendoDateTimePicker({

                format: "dd MMM yyyy HHmm",
                timeFormat: "HH:mm",
            });

            $("#expiryDatetime").kendoDateTimePicker({
                format: "dd MMM yyyy HHmm ",
                timeFormat: "HH:mm",
                change: function (e) {
                    toDate = $("#expiryDatetime").val();
                }
            });


            setTimeout(function() {
                    var splitter = $('#horizontal').kendoSplitter({
                        orientation: "horizontal",
                        panes: [
                            { collapsible: true, resizable: true },
                            { collapsible: true, resizable: true, size: "40%" }
                        ]

                    }).data("kendoSplitter");

                    var rightSplitter = $('#verticalSplitter').kendoSplitter({
                        orientation: "vertical",
                        panes: [
                            { collapsible: false, resizable: true },
                            { collapsible: false, resizable: true, size: "50%" }
                        ]

                    }).data("kendoSplitter");
                }, 100
            );

            toDate = $("#expiryDatetime").val();
            $("#originatorPanel").kendoValidator({
                rules: {
                    //implement your custom date validation      
                    validateOriginator: function (e) {
                        if (!e.is("[name=originator]") || !thisNSDEditor.proposalVM.IsFicRole) {
                            return true;
                        }
                        if ($(e).val() === "") {
                            return false;
                        }
                        return true;
                    }
                },
                messages: {
                    validateOriginator: function () {
                        return labels.nsEditor_validateOriginator;
                    }
                }
            });
            $("#chkSetBeginDateAtSubmit").on('change', function (e) {
                if ($('#chkSetBeginDateAtSubmit').is(':checked')) {
                    $("#beginDatetime").data("kendoDateTimePicker").enable(false);
                    $("#beginDatetime").val("");
                }
                else {
                    $("#beginDatetime").data("kendoDateTimePicker").enable(true);
                    $("#beginDatetime").val("");
                }
            });

            function skipDateValidationUntilEventsProgate() {
                var start = kendo.parseDate($("#beginDatetime").val().insertAt(14, ":"), ["dd MMM yyyy HH:mm"]);
                var end = kendo.parseDate($("#expiryDatetime").val().insertAt(14, ":"), ["dd MMM yyyy HH:mm"]);
                var chkSetBeginDateAtSubmitIsChecked = $('#chkSetBeginDateAtSubmit').is(':checked');

                if (end && !start && !chkSetBeginDateAtSubmitIsChecked)
                    return true;

                var chkEstimateIsChecked = $("#cbEstimated").is(':checked');
                if (start && !end && !chkEstimateIsChecked)
                    return true;

                return false;
            }

            $("#activePeriodContainer").kendoValidator({
                rules: {
                    validBeginDate: function (e) {
                        try {
                            if (skipDateValidationUntilEventsProgate())
                                return false;

                            if (thisNSDEditor.proposalVM.PropsalType === "C") {
                                return true;
                            }

                            if (!e.is("[name=beginDatetime]")) {
                                return true;
                            }
                            if ($('#chkSetBeginDateAtSubmit').is(':checked')) {
                                return true;
                            }
                            var currentDate = kendo.parseDate($(e).val().insertAt(14, ":"), ["dd MMM yyyy HH:mm"]);
                            //Check if Date parse is successful
                            if (!currentDate) {
                                this.beginDateErrorMessage = labels.nsEditor_beginDate_error;
                                return false;
                            }
                            if (!this.utcDiff) {
                                this.utcDiff = Util.getUTCDiff();
                            }
                            var utc = Util.getUTCTime(this.utcDiff);
                            if (utc > currentDate) {
                                this.beginDateErrorMessage = labels.nsEditor_beginDate_error;
                                return false;
                            }
                            return ValidateTimeSechedule(e);
                        }
                        catch (ex) {
                            return false;
                        }
                    },
                    //implement your custom date validation
                    validEndDate: function (e) {
                        try {
                            if (skipDateValidationUntilEventsProgate())
                                return false;


                            if (thisNSDEditor.proposalVM.PropsalType === "C") {
                                return true;
                            }
                            if (!e.is("[name=expiryDatetime]")) {
                                return true;
                            }
                            var currentDate = kendo.parseDate($(e).val().insertAt(14, ":"), ["dd MMM yyyy HH:mm"]);
                            //Check if Date parse is successful
                            if (!currentDate) {
                                EndDateErrorMessage = labels.nsEditor_endDate_error;
                                return false;
                            }
                            if (!this.utcDiff) {
                                this.utcDiff = Util.getUTCDiff();
                            }
                            var utc = Util.getUTCTime(this.utcDiff);
                            if (utc > currentDate) {
                                EndDateErrorMessage = labels.nsEditor_endDate_error;
                                return false;
                            }
                            return ValidateTimeSechedule(e);
                        }
                        catch (ex) {
                            return false;
                        }
                    },
                    validateDateDiff: function (e) {

                        if (skipDateValidationUntilEventsProgate())
                            return false;

                        if (thisNSDEditor.proposalVM.PropsalType === "C") {
                            return true;
                        }
                        if (!e.is("[name=expiryDatetime]")) {
                            return true;
                        }
                        var fromDate = kendo.parseDate($("#beginDatetime").val().insertAt(14, ":"), ["dd MMM yyyy HH:mm"]);
                        if ($('#chkSetBeginDateAtSubmit').is(':checked')) {
                            fromDate = new Date();
                        }
                        var tod = kendo.parseDate($("#expiryDatetime").val().insertAt(14, ":"), ["dd MMM yyyy HH:mm"]);
                        if (!e.is("[name=chkSetBeginDateAtSubmit]") && !tod) {
                            return true;
                        }

                        if (fromDate && tod && fromDate < tod) {
                            var diff = tod.valueOf() - fromDate.valueOf();
                            var diffHour = diff / 1000 / 60 / 60;
                            if (diffHour < 2208)
                                return true;
                            else
                                DiffDateErrorMessage = labels.nsEditor_diffDate_error;
                        }
                        else {
                            DiffDateErrorMessage = labels.nsEditor_diffFromToDate_error;
                        }

                        return false;
                    },
                },
                messages: {
                    validBeginDate: function () {
                        return this.beginDateErrorMessage;
                        //return RES.dbRes("Invalid From(UTC) date");
                    },
                    validEndDate: function () {
                        return EndDateErrorMessage;
                        //return RES.dbRes("Invalid To(UTC) date");
                    },
                    validateDateDiff: function () {
                        return DiffDateErrorMessage;
                        //RES.dbRes("From(UTC) date should be earlier than To(UTC) data");
                    },
                    validateSchedule: function () { return scheduleErrorMessage; }
                }
            });

            $("#originatorPanel").kendoValidator({
                rules: {
                    validateOnBehalfOfTab: function (e) {

                        // Originator is only required for Fic user
                        if (!thisNSDEditor.proposalVM.IsFicRole)
                            return true;

                        if (e.val()) {
                            $("#OnBehalfOfIcon").attr("src", "/Images/yes.png");
                            return true;
                        }
                        $("#OnBehalfOfIcon").attr("src", "/Images/no.png");
                        return false;
                    }
                },
                messages: {
                    validateOnBehalfOfTab: function() {
                        return labels.nsEditor_validateOriginator;
                    }
                }
            });


            function ValidateTimeSechedule(e) {
                if (thisNSDEditor.proposalVM.PropsalType === "C") {
                    return true;
                }
                if ($("#scheduleText").val().trim() === "") {
                    return true;
                }

                if (thisNSDEditor.proposalVM.nsdTokens.ItemDChangedByNof) {
                    return true;
                }

                var start = kendo.parseDate($("#beginDatetime").val().insertAt(14, ":"), ["dd MMM yyyy HH:mm"]);
                if ($('#chkSetBeginDateAtSubmit').is(':checked'))
                {
                    start = new Date();
                }

                var end = kendo.parseDate($("#expiryDatetime").val().insertAt(14, ":"), ["dd MMM yyyy HH:mm"]);
                if (!start || !end) {
                    scheduleErrorMessage = labels.nsEditor_validitySchedule_error;
                    return false;
                }

                if (e.is("[name=beginDatetime]")) {
                    var beginDatetime = kendo.parseDate($("#beginDatetime").val().insertAt(14, ":"), ["dd MMM yyyy"]);
                    if (beginDatetime) {
                        for (var idx = 0; idx < thisNSDEditor.proposalVM.nsdTokens.TimeScheduleData.length; idx++) {
                            var datearray = thisNSDEditor.proposalVM.nsdTokens.TimeScheduleData[idx].DateId.split(',');
                            for (var idx2 = 0; idx2 < datearray.length; idx2++) {
                                if (datearray[idx2].length === 8) {
                                    var currentDate = kendo.parseDate(datearray[idx2], "yyyyMMdd");
                                    if (currentDate) {
                                        if (beginDatetime > currentDate) {
                                            this.beginDateErrorMessage = labels.nsEditor_startSchedule_error;
                                            return false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if (e.is("[name=expiryDatetime]")) {
                    var expiryDatetime = kendo.parseDate($("#expiryDatetime").val().insertAt(14, ":"), ["dd MMM yyyy"]);
                    if (expiryDatetime) {
                        for (var idx = 0; idx < thisNSDEditor.proposalVM.nsdTokens.TimeScheduleData.length; idx++) {
                            var datearray = thisNSDEditor.proposalVM.nsdTokens.TimeScheduleData[idx].DateId.split(',');
                            for (var idx2 = 0; idx2 < datearray.length; idx2++) {
                                if (datearray[idx2].length === 8) {
                                    var currentDate = kendo.parseDate(datearray[idx2], "yyyyMMdd");
                                    if (currentDate) {
                                        if (currentDate > expiryDatetime) {
                                            EndDateErrorMessage = labels.nsEditor_endSchedule_error;
                                            return false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return true;
            }
            dataValidator = $("#activePeriodContainer").data("kendoValidator");

            $("#activePeriodContainer :input").blur(function () {
                thisNSDEditor.validateActivePeriodTab();
            });

            //
            // bind events
            //

            //$("#beginDatetime").on('change', function (e) {
            //    thisNSDEditor.proposalVM.nsdTokens.set("TimeScheduleData", "");
            //    $("#scheduleText").val("");
            //});

            //$("#expiryDatetime").on('change', function (e) {
            //    thisNSDEditor.proposalVM.nsdTokens.set("TimeScheduleData", "");
            //    $("#scheduleText").val("");
            //});


			$("#cbEstimated").on('change', function () {
                var est = $("#cbEstimated").is(':checked');
                thisNSDEditor.proposalVM.NOTAM.set("Estimated", est);
                
                if (est) {
                    $("#cbPermanent").attr('checked', false);
                    thisNSDEditor.proposalVM.NOTAM.set("Permanent", false);
                    /// if estimated time, disable the ability to add a schedule
                    $("#btnTimeScheduleAPI").hide();
                    $("#scheduleTr").prop('disabled', true);
                } else {
                    if ($('#scheduleText').is(':visible')) {
                        $("#scheduleText").attr("disabled", false);
                    }
                    
                    $("#btnTimeScheduleAPI").show();
                    $("#scheduleTr").prop('disabled', false);
                }

                $("#cbPermanent").attr("disabled", est);

            });
            
            $("#cbPermanent").on('change', function () {
                var perm = $("#cbPermanent").is(':checked');
                if (perm) {
                    if ($("#expiryDatetime").val() !== "") {


                        confirm(
                            labels.nsEditor_clearToDateWarning_title,
                            {
                                title: labels.nsEditor_confirm_label,
                                message: labels.nsEditor_clearToDateWarning_title,
                                icon: " k-Ext-warning",
                                buttons: [
                                    { name: labels.nsEditor_yes_label },
                                    { name: labels.nsEditor_no_label }],
                                onDone: function (button) {

                                    if (button === labels.nsEditor_yes_label) {
                                        thisNSDEditor.setPERM(true);
                                    } else {
                                        $("#cbPermanent").attr('checked', false);
                                    }
                                }

                            }
                            );

                    } else {
                        thisNSDEditor.setPERM(true);
                    }
                } else {
                    thisNSDEditor.setPERM(false);
                }
            });
            $("#btnTimeScheduleAPI").on('click', function ()
            {

                if (dataValidator.validate() || dataValidator.errors().length == 0)
                {
                    // clear change by NOF required validation 
                    if (thisNSDEditor.proposalVM.nsdTokens && thisNSDEditor.proposalVM.nsdTokens.ItemDChangedByNof)
                    {
                        thisNSDEditor.proposalVM.nsdTokens.ItemDChangedByNof = false;
                        $("#scheduleText").removeClass("k-invalid");
                        $("#scheduleText").val("");
                    }


                    $("#TimeScheduleAPI-Popup").data("kendoWindow")
                                   .content("")
                                   .refresh({
                                       url: "../NsdEditor/GetTimeScheduleAPI",
                                       type: "POST",
                                       complete: function ()
                                       {
                                           //debugger; 355
                                           require(["c/NsdEditor/TimeSchedule"], function (TimeSchedule)
                                           {
                                               var _TimeScheduleData = JSON.stringify((typeof (thisNSDEditor.proposalVM.nsdTokens.TimeScheduleData) === "undefined" ? "" : thisNSDEditor.proposalVM.nsdTokens.TimeScheduleData));
                                               //debugger;
                                               var _BeginDatetime = kendo.parseDate($("#beginDatetime").val().insertAt(14, ":"), ["dd MMM yyyy HH:mm"]);
                                               if ($('#chkSetBeginDateAtSubmit').is(':checked'))
                                               {
                                                   _BeginDatetime = new Date();
                                               }
                                               var _ExpiryDatetime = kendo.parseDate($("#expiryDatetime").val().insertAt(14, ":"), ["dd MMM yyyy HH:mm"]);
                                               TimeSchedule.init(_TimeScheduleData, _BeginDatetime, _ExpiryDatetime, thisNSDEditor.proposalVM);
                                               TimeSchedule.initUI(labels);
                                               TimeSchedule.PostInitUI();
                                               document.getElementById("TimeSchedulecontainer").style.visibility = "visible";
                                           })

                                       }
                                   });
                }
            });


            $("#btnCancelEditProposal").on('click', function (e) {
                $("#NSDEditor-Popup").data("kendoWindow").close();
            });

            var timer;
            $("#scheduleText").on("keyup", function () {
                if ($('#cbEstimated').is(':visible')) {
                    if ($("#scheduleText").val() !== "") {
                        $("#cbEstimated").attr('checked', false);
                        $("#cbEstimated").attr("disabled", true);
                    } else {
                        $("#cbEstimated").attr("disabled", false);
                    }
                }


                clearTimeout(timer);
                timer = setTimeout(function () {
                    dataValidator.validate();
                }, 1000);

            });

            //$("#scheduleText").prop("disabled", "disabled");

            if (thisNSDEditor.proposalVM.PropsalType === "C") {

                /*$("#startDateLable").text(RES.dbRes("At"))
                $("#reviewStartDateLabel").text(RES.dbRes("At"))*/

                $(".hideOnCancelNOTAM").hide();
                $("#chkSetBeginDateAtSubmit").prop("disabled", "disabled");
                $("#chkSetBeginDateAtSubmit").prop('checked', true);
                $("#beginDatetime").data("kendoDateTimePicker").enable(false);
                $("#datesSectionIcon").attr("src", "/Images/yes.png");
                //$("#subjectSectionIcon").attr("src", "/Images/yes.png");
                $("#subjectSelectorContainer :input").prop("disabled", "disabled");

                /*
                $("#btnTimeScheduleAPI").hide();
                $("#cbPermanent").hide();
                $("#cbEstimated").hide();
                $("#scheduleText").hide();
                $(".hideOnCancelNOTAM").hide();
                $("#expiryDatetime").data("kendoDateTimePicker").wrapper.hide();
                $("#beginDatetime").data("kendoDateTimePicker").enable(false);
                
                */
            }

            $("#notamType").val(thisNSDEditor.proposalVM.PropsalType === "N" ? labels.nsEditor_newUpper_label : thisNSDEditor.proposalVM.PropsalType == "C" ? labels.nsEditor_cancelUpper_label : labels.nsEditor_replaceUpper_label);


            if (thisNSDEditor.proposalVM.ReadOnly) {
                $(".tabStatusIcon").hide();
                $("#btnSaveProposal").hide();
                $("#btnSubmitProposal").hide();
                $("#notamProposalForm :input").prop("disabled", true);
                $("#beginDatetime").data("kendoDateTimePicker").enable(false);
                $("#expiryDatetime").data("kendoDateTimePicker").enable(false);
                $("#attachmentsFiles").data("kendoUpload").enable(false);
                $("#attachmentsFiles").data("kendoUpload").enable(false);
                //$('#notamProposalForm').find('input, textarea, select').attr('readonly', true);

            } else {

                if (thisNSDEditor.proposalVM.PropsalType !== "N") {
                    $("#btnSaveProposal").hide();
                }else{
                    $("#btnSaveProposal").on('click', function (e) {

                    if (!thisNSDEditor.proposalVM.nsdTokens.SdoEntityId ) {
                        kendo.ui.ExtAlertDialog.show({
                            title: labels.nsEditor_saveError_message,
                            message: labels.nsEditor_invalidSubject_error,
                            icon: "k-Ext-information"
                        });
                        return;
                    }

                    var newNOTAM = thisNSDEditor.proposalVM.NOTAM.toJSON();
                    newNOTAM.Tokens = JSON.stringify(thisNSDEditor.proposalVM.nsdTokens);

                    kendo.ui.progress($('#container'), true);

                    $.ajax({
                        url: '/NSDEditor/SaveNotamProposal',
                        type: 'POST',
                        contentType: 'application/json',
                        dataType: 'json',
                        async: false,
                        cache: false,
                        data: JSON.stringify({ notamProposal: newNOTAM, nsdId: thisNSDEditor.proposalVM.nsd.Id, version: thisNSDEditor.proposalVM.nsd.Version, subjectId: thisNSDEditor.proposalVM.nsdTokens.SdoEntityId }),
                        success: function (data) {
                            if (data.Success) {
                                thisNSDEditor.proposalVM.NOTAM.set("Id", data.ProposalId);
                                thisNSDEditor.proposalVM.NOTAM.set("PackageId", data.PackageId);

                                thisNSDEditor.updateTitle();

                                $("#attachmentsFiles").data("kendoUpload").enable();

                                kendo.ui.progress($('#container'), false);
                                $("#NSDEditor-Popup").trigger("DataChanged", ["Save"]);
                                kendo.ui.ExtAlertDialog.show({
                                    title: labels.nsEditor_completed_label,
                                    message: labels.nsEditor_saveSuccessful_message,
                                    icon: "k-Ext-information"
                                });

                                //$('#additonalInfoTable .k-upload-selected').trigger('click');


                            } else {
                                kendo.ui.progress($('#container'), false);

                                Util.showErrorMessage(labels.nsEditor_saveError_message, data);
                            }

                        },

                    }).fail(function (jqXHR, textStatus, err) {
                        kendo.ui.progress($('#container'), false);

                        Util.showErrorMessage(labels.nsEditor_saveError_message, new { Error: err, Code: "JS" });


                    });

                });
                }

                $("#btnSubmitProposal").on('click', function (e) {
                    if (!thisNSDEditor.validateNSD()) { return; }

                    kendo.ui.progress($('#container'), true);

                    var submitURL = '/NSD/' + thisNSDEditor.proposalVM.nsd.FileLocation + ('/V' + thisNSDEditor.proposalVM.nsd.Version).replace('.', 'R') + "/SubmitNOTAMProposal";

                    //var newNOTAM = thisNSDEditor.proposalVM.NOTAM.toJSON();
                    //newNOTAM.Tokens = JSON.stringify(thisNSDEditor.proposalVM.nsdTokens);

                    var packageId = thisNSDEditor.proposalVM.PackageId > 0 ? thisNSDEditor.proposalVM.PackageId : thisNSDEditor.proposalVM.NOTAM.PackageId;

                    $.ajax({
                        url: submitURL,
                        type: 'POST',
                        contentType: 'application/json',
                        dataType: 'json',
                        async: false,
                        cache: false,
                        data: JSON.stringify({ proposalType: thisNSDEditor.proposalVM.PropsalType, packageId: packageId, tokens: JSON.stringify(thisNSDEditor.proposalVM.nsdTokens), subjectId: thisNSDEditor.proposalVM.nsdTokens.SdoEntityId }),

                        success: function (data) {
                            //debugger;
                            if (data.Success) {

                                kendo.ui.progress($('#container'), false);
                                thisNSDEditor.proposalVM.NOTAM.set("Id", data.id);
                                thisNSDEditor.proposalVM.NOTAM.set("PackageId", data.packageId);
                                $('#additonalInfoTable .k-upload-selected').trigger('click');
                                $("#NSDEditor-Popup").trigger("DataChanged", ["Submit"]);
                                $("#NSDEditor-Popup").data("kendoWindow").close();
                            } else {
                                kendo.ui.progress($('#container'), false);
                                Util.showErrorMessage(labels.nsEditor_submitError_message, data);
                            }

                        },

                    }).fail(function (jqXHR, textStatus, err) {
                        kendo.ui.progress($('#container'), false);
                        Util.showErrorMessage(labels.nsEditor_submitError_message, new { Error: err, Code: "JS" });
                    });

                });
            }
        },

        loadMapEntity: function () {
            bingMap.addMultiPolygonToLayer(thisNSDEditor.proposalVM.doaGeo, null, bingMap.drawingOptions["DOA"]);
            //if (thisNSDEditor.proposalVM.subject.SubjectGeoJson !== null) {
            //    bingMap.addGeoToLayer(thisNSDEditor.proposalVM.subject.SubjectGeoJson,
            //        bingMap.displayLayer,
            //            {
            //                strokeColor: new Microsoft.Maps.Color(70, 0, 0, 0),
            //                strokeWidth: 4,
            //                fillColor: new Microsoft.Maps.Color(50, 100, 25, 25)
            //            });
            //}
            //if (thisNSDEditor.proposalVM.subject.SubjectGeoRefLat !== null) {
            //    bingMap.addPushPin(parseFloat(thisNSDEditor.proposalVM.subject.SubjectGeoRefLat), parseFloat(thisNSDEditor.proposalVM.subject.SubjectGeoRefLong), {
            //        'draggable': false
            //    });
            //}
            bingMap.showAll();
        },

        setPERM: function (perm) {

            if (perm) {
                thisNSDEditor.proposalVM.NOTAM.set("Estimated", false);
                thisNSDEditor.proposalVM.NOTAM.set("Permanent", true);

                $("#cbEstimated").attr('checked', false);
                toDate = $("#expiryDatetime").val();
                $("#expiryDatetime").val("");

                thisNSDEditor.proposalVM.NOTAM.set("EndValidity", "PERM");

            } else {
                $("#expiryDatetime").val(toDate);

                thisNSDEditor.proposalVM.NOTAM.set("EndValidity", Util.formatNOTAMDate(toDate));
            }
            $("#cbEstimated").attr("disabled", perm);
            $("#expiryDatetime").data("kendoDateTimePicker").enable(!perm);
        },

        updateTitle: function () {

            var subjectIdentifier = thisNSDEditor.proposalVM.subject == null?"": thisNSDEditor.proposalVM.subject.Designator + "-" + thisNSDEditor.proposalVM.subject.Name;
            $('#notamSubject').val(subjectIdentifier);
            //$('#notamSubject').inputfit();
            var title = (thisNSDEditor.proposalVM.PackageId === 0 ? "[New] " : "[ " + thisNSDEditor.proposalVM.PackageId + " ] ") + thisNSDEditor.proposalVM.nsd.Name + " " + subjectIdentifier;
            $("#NSDEditor-Popup").data("kendoWindow").title(title);
        },

        validateConditionTab: function () {
            //debugger;
            if (thisNSDEditor.conditionValidator) {
                return thisNSDEditor.conditionValidator.validate();
            }
            return thisNSDEditor.proposalVM.validModel;
        },

        validateReviewTab: function () {
            return true;
        },

        validateAdditionalInfoTab: function () {
            return true;
        },

        validateOnBehalfOfTab: function () {
            var validator = $("#originatorPanel").data("kendoValidator");
            validator.hideMessages();

            // validate dates
            if (validator.validate() || validator.errors().length === 0) {
                $("#OnBehalfOfIcon").attr("src", "/Images/yes.png");
                return true;
            } else {
                $("#OnBehalfOfIcon").attr("src", "/Images/no.png");
            }

            return false;

        },
        validateActivePeriodTab: function () {
            dataValidator.hideMessages();

            // validate dates
            if (dataValidator.validate() || dataValidator.errors().length === 0) {
                $("#datesSectionIcon").attr("src", "/Images/yes.png");
                return true;
            } else {
                $("#datesSectionIcon").attr("src", "/Images/no.png");
            }

            return false;
        },

        validateNSD: function () {
            var tab = $("#tabStrip").data("kendoPanelBar");

            if (!thisNSDEditor.validateActivePeriodTab()) {
                return false;
            }

            if (!thisNSDEditor.validateConditionTab()) {
                //debugger;
                return false;
            }

            if (!thisNSDEditor.validateOnBehalfOfTab()) {
                return false;
            }

            return true;

        },

    }
});



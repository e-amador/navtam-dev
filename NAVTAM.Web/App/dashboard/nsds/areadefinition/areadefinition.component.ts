﻿import { Component, OnInit, OnDestroy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'

import { DataService } from '../../shared/data.service';
import { NgModel } from '@angular/forms';
import { AreaDefinitionNsdService, AreaDefinitionModel} from './areadefinition.service';

@Component({
    templateUrl: './app/dashboard/nsds/areadefinition/areadefinition.component.html'
})
export class AreaDefinitionNsdComponent implements OnInit, OnDestroy {

    model: any = null;

    points: string[] = [];


    //> todo refactor to one viewmodel
    AreaVm: AreaDefinitionModel;
    polygonEntry: boolean = false;
    polyBuffer: string; 

    pointAndRadiusEntry: boolean = false;
    pointAndRadiusRadius: string = "";
    
    bufferedLineEntry: boolean = false;
    bufferedLineBuffer: string = "";

    lowerLimit: string = "";
    upperLimit: string = "";


    constructor(
        private dataService: DataService,
        private activatedRoute: ActivatedRoute,
        private service: AreaDefinitionNsdService,
        private router: Router) {
    }

    ngOnInit() {
        window['loadLeafletMapScript'].call(this, null);
        this.model = this.activatedRoute.snapshot.data['model'];
    }

    ngOnDestroy() {
        if (window["app"].leafletmap) {
            window["app"].leafletmap.dispose();
        }
    }

    polygonEntryChange($e) {
        if (this.pointAndRadiusEntry) this.pointAndRadiusEntry = false;
        if (this.bufferedLineEntry) this.bufferedLineEntry = false;
        if ($e) {
            this.polygonEntry = true
        } else {
            this.polygonEntry = false
        };
    }

    pointAndRadiusEntryChange($e) {
        if (this.bufferedLineEntry) this.bufferedLineEntry = false;
        if (this.polygonEntry) this.polygonEntry = false;
        if ($e) {
            this.pointAndRadiusEntry = true
        } else {
            this.pointAndRadiusEntry = false;
        };
    }

    bufferedLineEntryChange($e) {
        if (this.polygonEntry) this.polygonEntry = false;
        if (this.pointAndRadiusEntry) this.pointAndRadiusEntry = false;
        if ($e) {
            this.bufferedLineEntry = true;
        } else {
            this.bufferedLineEntry = false;
        }
    }

    isGeoPointInputEmpty() : boolean {
        if (this.points.length == 0)
            return true;

        return false;
    }

    calculateGeos() {        
        this.service.calculate(this.AreaVm);
        // do stuff with result
    }  

    get langCulture() {
        return (window["app"].cultureInfo || "en").toUpperCase();
    }

    validateKeyPressNumber(evt: any) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!this.isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
        }
    }

    private isANumber(str: string): boolean {
        return !/\D/.test(str);
    }
}
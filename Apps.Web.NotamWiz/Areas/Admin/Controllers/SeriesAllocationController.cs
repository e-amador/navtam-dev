﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using log4net;
using NavCanada.Applications.NotamWiz.Web.Code.Extentions;
using NavCanada.Applications.NotamWiz.Web.Controllers;
using NavCanada.Applications.NotamWiz.Web.Models;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Applications.NotamWiz.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class SeriesAllocationController : BaseController
    {
        private readonly ILog log = LogManager.GetLogger(typeof (SeriesAllocationController));

        public SeriesAllocationController(IUow uow, IClusterQueues clusterQueues) 
            : base(uow, clusterQueues)
        {
        }

        // GET: Admin/SeriesAllocation
        public async Task<ActionResult> Index()
        {
            log.Object(new {Method = "Index", Parameters = new {}});

            var geoRegiones = await Uow.GeoRegionRepo.GetAllAsync();

            ViewBag.GeoRegions = geoRegiones.Select( r=> 
                new {
                    value = r.Id,
                    text = r.Name
                })
            .ToList();

            ViewBag.DisseminationLevels = new List<object>
            {
                new { value = (int) DisseminationCategory.National, text = "National" },
                new { value = (int) DisseminationCategory.US, text = "US"},
                new { value = (int) DisseminationCategory.International, text = "International"}
            };

            return View();
        }


        public ActionResult SeriesAllocation_Read([DataSourceRequest] DataSourceRequest request)
        {
            log.Object(new {Method = "SeriesAllocation_Read", Parameters = new {request}});

            var serieAllocations = Uow.SeriesAllocationRepo.GetAll();
            var result = serieAllocations.Project().To<SeriesAllocationViewModel>();

            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Create SeriesAllocation
        /// </summary>
        /// <param name="request">DataSourceRequest from Kendo</param>
        /// <param name="adc">series vm</param>
        public async Task<ActionResult> SeriesAllocation_Create([DataSourceRequest] DataSourceRequest request, SeriesAllocationViewModel adc)
        {
            log.Object(new {Method = "SeriesAllocation_Create", Parameters = new {request, adc}});
            var dsr = new List<SeriesAllocation>();
            try
            {
                var entity = Mapper.Map<SeriesAllocationViewModel, SeriesAllocation>(adc);
                if (SeriesAllocationModelValidator(entity))
                {
                    Uow.SeriesAllocationRepo.Add(entity);
                    await Uow.SaveChangesAsync();
                    dsr.Add(entity);
                }
            }
            catch (Exception e)
            {
                log.Object(new {Method = "SeriesAllocation_Create"}, Log4NetExt.LogLevel.ERROR, e);
                return null;
            }

           return Json(dsr.ToDataSourceResult(request, ModelState, Mapper.Map<SeriesAllocation, SeriesAllocationViewModel>));
        }
        /// <summary>
        /// Update series allocation
        /// </summary>
        /// <param name="request">datasourcerequest from kendogrid</param>
        /// <param name="adc">singular vm of series</param>
        public async Task<ActionResult> SeriesAllocation_Update([DataSourceRequest] DataSourceRequest request, SeriesAllocationViewModel adc)
        {
            log.Object(new {Method = "SeriesAllocation_Update", Parameters = new {request, acd = adc}});
            var dsr = new List<SeriesAllocation>();
            try
            {
                var entity = Mapper.Map<SeriesAllocationViewModel, SeriesAllocation>(adc);
                if (SeriesAllocationModelValidator(entity))
                {
                    Uow.SeriesAllocationRepo.Update(entity);
                    await Uow.SaveChangesAsync();
                    dsr.Add(entity);
                }
            }
            catch (Exception e)
            {
                log.Object(new { Method = "SeriesAllocation_Update"}, Log4NetExt.LogLevel.ERROR, e);
                return null;
            }

            return Json(dsr.ToDataSourceResult(request, ModelState, Mapper.Map<SeriesAllocation, SeriesAllocationViewModel>));
        }
        /// <summary>
        /// Destroys entities
        /// </summary>
        /// <param name="request">dsr kendogid</param>
        /// <param name="id">entity id</param>
        public async Task<ActionResult> SeriesAllocation_Destroy([DataSourceRequest] DataSourceRequest request, int id)
        {
            log.Object(new {Method = "SeriesAllocation_Destroy", Parameters = new {request, id}});
            try
            {
                var serieAllocation = await Uow.SeriesAllocationRepo.GetByIdAsync(id);
                if (serieAllocation == null)
                    return null;

                Uow.SeriesAllocationRepo.Delete(serieAllocation);
                Uow.SaveChanges();
            }
            catch (Exception e)
            {
                log.Object(new {Method = "SeriesAllocation_Destry"}, Log4NetExt.LogLevel.ERROR, e);
                return null;
            }

            var entities = Uow.SeriesAllocationRepo.GetAll();
            return Json(entities.ToDataSourceResult(request, ModelState, Mapper.Map<SeriesAllocation, SeriesAllocationViewModel>));
        }


        private bool SeriesAllocationModelValidator(SeriesAllocation sa)
        {
            return sa.GetType().GetProperties().All(pi => pi.Name == "Id" || pi.Name == "Region" || pi.GetValue(sa) != null);
        }

        internal class ValueText
        {
            public int value { get; set; }
            public string text { get; set; }
        }
    }
}
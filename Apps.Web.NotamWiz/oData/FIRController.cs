﻿namespace NavCanada.Applications.NotamWiz.Web.oData
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http.OData;

    using NavCanada.Applications.NotamWiz.Web.Models.CustomValidations;
    using NavCanada.Core.Data.NotamWiz.Contracts;

    public class FIRController : BaseODataController
    {

        public FIRController(IUow uow)
            : base(uow)
        {
        }

        

        // GET: odata/FIR
        [EnableQuery]
        public List<FirViewModel> GetFIR()
        {
            var firs = Uow.SdoSubjectRepo.GetFirs().Select(f=> new FirViewModel()
            {
                Id = f.Id,
                Designator =  f.Designator,
                Name = f.Name
            }).ToList();

            return firs;

        }

      
    }
}

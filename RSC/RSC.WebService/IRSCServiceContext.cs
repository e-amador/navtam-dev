﻿namespace RSC.WebService
{
    public interface IRSCServiceContext
    {
        string Name { get; set; }
    }

    public class RSCServiceContext : IRSCServiceContext
    {
        public string Name { get; set; } = "Hellow there...";
    }
}
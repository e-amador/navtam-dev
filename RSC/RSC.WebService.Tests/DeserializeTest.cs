﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RSC.WebService.Helpers;
using RSC.WebService.Schemas;
using System.IO;

namespace RSC.WebService.Tests
{
    [TestClass]
    public class DeserializeTest
    {
        [TestMethod]
        [DeploymentItem("Data\\RSCProposal-0003.xml")]
        public void Deserialize_Proposal_test()
        {
            var xml = File.ReadAllText("RSCProposal-0003.xml");
            var proposal = XMLSerializer.Deserialize<RSCProposalType>(xml);
            Assert.IsNotNull(proposal);
        }
    }
}

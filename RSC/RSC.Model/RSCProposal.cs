﻿using System;
using System.Collections.Generic;

namespace RSC.Model
{
    /// <summary>
    /// RSC Information to import into NAV CANADA NOTAM Entry System application.
    /// </summary>
    public class RSCProposal
    {
        public RSCProposal()
        {
        }

        /// <summary>
        /// Attritute: The version of the RSC schema to which the message conforms.
        /// </summary>
        public string Version { get; set; }
        /// <summary>
        /// Attritute: The originator (source) of the message. This should consist of a unique company name, application name and version id.
        /// </summary>
        public string Origin { get; set; }
        /// <summary>
        /// Attritute: The date and time when the message was created.
        /// </summary>
        public DateTime Created { get; set; }
        /// <summary>
        /// Aerodrome to which this RSC relates
        /// </summary>
        public string Aerodrome { get; set; }
        /// <summary>
        /// Runway observations. All Runways must exist in the NAV CANADA
        /// Aeronautical Data Management System(ADMS).
        /// </summary>
        public Runway[] Runways { get; set; }
        /// <summary>
        /// Taxiway Observations freetext.
        /// </summary>
        public FreeformText Taxiways { get; set; }
        /// <summary>
        /// Apron Observations - freetext.
        /// </summary>
        public FreeformText Aprons { get; set; }
        /// <summary>
        /// General remarks - freetext.
        /// </summary>
        public FreeformText GeneralRemarks { get; set; }
        /// <summary>
        /// Provide dat/time for next planned observation. Can be any time at or after the current time.
        /// </summary>
        public DateTime? NextPlannedObs { get; set; }
        /// <summary>
        /// Conditions are changing rapidly. A contact must be provided.
        /// </summary>
        public Telephone ConditionsChangingRapidly { get; set; }
        /// <summary>
        /// Cancel current RSC.
        /// </summary>
        public bool CancelCurrentRSC { get; set; }
    }
}

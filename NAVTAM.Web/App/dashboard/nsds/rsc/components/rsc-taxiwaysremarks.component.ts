﻿import { Component, OnInit, OnChanges, OnDestroy, Inject, Input } from '@angular/core';
import { TranslationService } from 'angular-l10n';
import { FormGroup, FormBuilder, FormArray, AbstractControl, Validators } from '@angular/forms';
import { RscService } from '../rsc.service';
import { ITaxiwaysRemarksModel } from '../rsc.model';

import { TOASTR_TOKEN, Toastr } from '../../../../common/toastr.service';

@Component({
    selector: 'rsc-taxiways-remarks',
    templateUrl: './app/dashboard/nsds/rsc/components/rsc-taxiwaysremarks.component.html'
})
export class TaxiwaysRemarksComponent implements OnInit, OnChanges, OnDestroy {
    @Input('form') mainForm: FormGroup = null;
    @Input('model') model: ITaxiwaysRemarksModel = null;
    @Input('isReadOnly') isReadOnly: boolean = false;

    frmGroup: FormGroup;
    isFormLoadingData: boolean;

    constructor(
        private fb: FormBuilder,
        public translation: TranslationService,
        private rscService: RscService,
        @Inject(TOASTR_TOKEN) private toastr: Toastr) {
    }

    ngOnInit() {
        this.isFormLoadingData = false;

        this.configureReactiveForm();
    }

    ngOnChanges() {
    }

    ngOnDestroy() {
    }

    $ctrl(name: string): AbstractControl {
        return this.frmGroup.get(name);
    }

    private configureReactiveForm() {
        const frmArrControls = <FormArray>this.mainForm.controls['frmTaxiwaysRemarksArr'];

        this.frmGroup = this.fb.group({
            taxiwaysRemarks: [{ value: this.model.taxiwaysRemarks, disabled: this.isReadOnly }, [Validators.required]],
        });

        frmArrControls.push(this.frmGroup);

    }

    //Export functions
    public isFormValid(): boolean {

        return true;
    }

    public exportComponentInternalData(): ITaxiwaysRemarksModel {
        let data: ITaxiwaysRemarksModel = {
            parent: this.model.parent,
            taxiwaysRemarks: this.model.taxiwaysRemarks,
        };
        return data;
    }

    public clearValues(): void {
        alert('Clear Component Values: TaxiwaysRemarks');
    }


}
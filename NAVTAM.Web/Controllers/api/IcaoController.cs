﻿using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using NAVTAM.NsdEngine;
using NAVTAM.NsdEngine.Helpers;
using NDS.Relay;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [Authorize]
    [RoutePrefix("api/icao")]
    public class IcaoController : ApiBaseController
    {
        public IcaoController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, INsdManagerResolver managerResolver, ILogger logger) : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
            _managerResolver = managerResolver;
        }

        [Route("GenerateIcao")]
        [HttpPost]
        public async Task<IHttpActionResult> GenerateIcao(ProposalViewModel model)
        {
            return await LogAction(async () =>
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                if (!ValidUserRequest())
                    return Unauthorized();

                var nsdManager = _managerResolver.GetNsdManager($"{model.CategoryId}-{model.Version}");
                if (nsdManager == null)
                    return BadRequest("Error creating nsdManager.");

                var doaGeoArea = await GeoLocationCache.GetUserDoa(Uow, ApplicationContext.GetUserId());

                var context = await GetProposalContextAsync(model, doaGeoArea);

                await nsdManager.UpdateContext(model, context);

                var validateResult = await nsdManager.Validate(model, context);
                if (validateResult.HasErrors)
                    return BadRequest(validateResult.ErrorMessages.Count > 0 ? validateResult.ErrorMessages.FirstOrDefault() : Resources.InternalErrorValidation);

                try
                {
                    var icaoResult = await nsdManager.GenerateIcao(model, context);
                    if (!icaoResult)
                        return BadRequest("Error generating ICAO.");
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }

                model.IcaoText = ICAOFormatUtils.Generate(model, context.IsBilingual || model.Token.OverrideBilingual);
                //model.IcaoText = ICAOFormatUtils.Generate(model, context.IsBilingual);
                model.Token.IsBilingual = context.IsBilingual;

                return Ok(model);

            }, "api/icao/GenerateIcao");
        }

        readonly INsdManagerResolver _managerResolver;
    }
}

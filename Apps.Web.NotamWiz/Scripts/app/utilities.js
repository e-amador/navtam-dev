﻿define(['jquery', 'Resources', 'kendoExt'], function ($, res) {

    var RES = res.getResources("NSD-COMMON");


    return {

        fromDotNetDate: function (dateStr) {
            if (typeof dateStr === "string" && dateStr.indexOf("Date(")) {
                return new Date(parseInt(dateStr.replace(/\/Date\((-?\d+)\)\//gi, "$1")));
            }

            return null;
        },

        /// Accepted format is dd MMM yyyy HHmm"
        formatNOTAMDate: function (dateStr) {
            if (typeof dateStr === "string") {

                if (dateStr.indexOf("Date(")) {
                    return kendo.toString(new Date(parseInt(dateStr.replace(/\/Date\((-?\d+)\)\//gi, "$1"))), 'yyMMddHHmm');
                }

                dateStr = dateStr.trim();
                if (dateStr.length === 16) {
                    var date = new Date(dateStr.insertAt(14, ":"));
                    if (isNaN(date) === false) {
                        return kendo.toString(date, 'yyMMddHHmm');
                    }
                }
            }

            return null;
        },

        pad: function (str, max) {
            str = str.toString();
            return str.length < max ? this.pad("0" + str, max) : str;
        },
        /* ui control functions */
        hidePermement: function () {
            $("#cbPermanent").closest("tr").hide();
        },

        /* ui control functions */
        hideEstimated: function () {
            $("#cbEstimated").closest("tr").hide();
        },

        hideInputPanels: function () {
            $(".tabpanel").attr("style", "display:none");
        },

        maximizeReviewPanel: function () {
            var splitter = $("#horizontal").data("kendoSplitter");
            splitter.collapse(".k-pane:first");
        },

        hideActivePeriodTab: function () {
            $($("#tabStrip").data("kendoTabStrip").items()["activePeriodTab"]).attr("style", "display:none");
        },

        hideAdditonalInfoTab: function () {
            $($("#tabStrip").data("kendoTabStrip").items()["additionalInfoTab"]).attr("style", "display:none");
        },

        hideConditionTab: function () {
            $($("#tabStrip").data("kendoTabStrip").items()["conditionTab"]).attr("style", "display:none");
        },

        setConditionValid: function (valid) {
            if (valid) {
                $("#conditionSectionIcon").attr("src", "/Images/yes.png");
            } else {
                $("#conditionSectionIcon").attr("src", "/Images/no.png");
                $("#reviewSectionIcon").attr("src", "/Images/no.png");
            }
        },
        /* ui control functions */
        hideImmediate: function () {
            $("#chkSetBeginDateAtSubmit").closest("td").hide();
        },

        /* ui control functions */
        hideSchedule: function () {

            $("#scheduleText").closest("tr").hide();
        },
        decimalToDMS: function (dec, type) {
            var minus = dec < 0;
            dec = Math.abs(dec);
            var degrees = Math.floor(dec);
            var seconds = (dec - degrees) * 3600;
            var minutes = Math.floor(seconds / 60);
            seconds = Math.floor(seconds - (minutes * 60));


            //var tmp = (dec - degree) * 60;
            //var mintue = Math.floor(tmp);
            //var seconds = Math.min(Math.round((tmp - mintue) * 60), 59);
            switch (type) {
                case 'lat':
                    return this.pad(degrees, 2) + this.pad(minutes, 2) + this.pad(seconds, 2) + (minus ? 'S' : 'N');
                case 'long':
                    return this.pad(degrees, 3) + this.pad(minutes, 2) + this.pad(seconds, 2) + (minus ? 'W' : 'E');
            }

            return '';


        },


        DMSTodecimal: function (str) {



            if (str.length > 8 || str.length < 7) {
                return null;
            }

            var degrees = parseInt(str.length === 7 ? str.substring(0, 2) : str.substring(0, 3));
            var minutes = parseInt(str.length === 7 ? str.substring(2, 4) : str.substring(3, 5));
            var seconds = parseInt(str.length === 7 ? str.substring(4, 6) : str.substring(5, 7));
            var sufx = str.length === 7 ? str.substring(6, 7) : str.substring(7, 8);
            var sing = (sufx === 'W' || sufx === 'S') ? -1 : 1;
            var decimal = sing * (degrees + ((minutes * 60.0 + seconds) / 3600.0));

            return decimal;
        },

        locationToDecimal: function (location) {
            if (this.IsValidDMSLocation(location)) {
                var parts = location.split(" ");
                return this.DMSTodecimal(parts[0]) + " " + this.DMSTodecimal(parts[1]);
            } else if (this.IsValidDecimalLocation(location)) {
                return location;
            }

            return null;
        },

        IsValidLocation: function (location) {
            if (!location) return false;
            return this.IsValidDMSLocation(location) || this.IsValidDecimalLocation(location);
        },

        IsValidDecimalLocation: function (location) {
            if (!location) return false;
            var parts = location.split(" ");
            if (parts.length != 2) return false;

            if (!(/^-?\d+\.\d*$/.test(parts[0])) || !(/^-?\d+\.\d*$/.test(parts[1]))) return false;

            var dmsLat = this.decimalToDMS(parts[0], 'lat');
            var dmsLon = this.decimalToDMS(parts[1], 'long');

            return this.IsValidDMSLatitude(dmsLat) && this.IsValidDMSLongitude(dmsLon);

        },


        IsValidDMSLocation: function (location) {
            if (!location) return false;
            var parts = location.split(" ");
            if (parts.length != 2) return false;

            return this.IsValidDMSLatitude(parts[0]) && this.IsValidDMSLongitude(parts[1]);

        },

        // format is DDMMSSd => d is N or S
        IsValidDMSLatitude: function (lat) {

            if (lat.length != 7) return false;

            var degrees = parseInt(lat.substring(0, 2));
            var minutes = parseInt(lat.substring(2, 4));
            var seconds = parseInt(lat.substring(4, 6));

            if (!(/^\d+$/.test(degrees) && /^\d+$/.test(degrees) && /^\d+$/.test(seconds))) return false;

            var sufx = lat.substring(6, 7);

            if (degrees < 0 || degrees > 89 || minutes > 59 || seconds > 59 || (sufx != 'S' && sufx != 'N')) return false;

            return true;
        },

        // format is DDDMMSSd => d is W or E
        IsValidDMSLongitude: function (lon) {
            if (lon.length != 8) return false;

            var degrees = parseInt(lon.substring(0, 3));
            var minutes = parseInt(lon.substring(3, 5));
            var seconds = parseInt(lon.substring(5, 7));
            if (!(/^\d+$/.test(degrees) && /^\d+$/.test(degrees) && /^\d+$/.test(seconds))) return false;

            var sufx = lon.substring(7, 8);

            if (degrees < 0 || degrees > 179 || minutes > 59 || seconds > 59 || (sufx != 'W' && sufx != 'E')) return false;

            return true;

        },

        getCurrentUTCTime: function(){
            var now = new Date(); 
            return new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
        },

        getUTCDiff: function () {
            var mill = new Date().getTime();
            var utcDiff = null;

            $.ajax({
                url: 'api/UTCTime/',
                type: "GET",
                dataType: 'json',
                async: false,
            })
            .success(function (data) {

                var utc = new Date(data.year, data.month - 1, data.day, data.hour, data.minute, data.second, data.millsecond);
                var local = new Date().getTime();
                utcDiff = local - utc.getTime() + (local - mill);

            })

            return utcDiff;
        },


        getUTCTime: function (utcDiff) {

            if (utcDiff === null) { return null; }
            return new Date(new Date().getTime() - utcDiff);

        },

        buildErrorMessage: function (e) {
            return e.line !== undefined && e.column !== undefined
                ? "Line " + e.line + ", column " + e.column + ": " + e.message
                : e.message;
        },

        getObjects: function (obj, key, val) {
            //debugger;
            var objects = [];
            for (var i in obj) {
                if (!obj.hasOwnProperty(i)) continue;

                if (typeof obj[i] == 'object') {
                    if (i == key && val === undefined) {
                        objects.push(obj[i]);
                    } else {
                        objects = objects.concat(this.getObjects(obj[i], key, val));
                    }
                } else if (i == key && (val === undefined || obj[key] == val)) {
                    objects.push(obj);
                }
            }
            return objects;
        },

        validateFreeTextInput: function (freeInputText, setting) {
            if (freeInputText && freeInputText.length > 0 && setting && Object.prototype.toString.call(setting) === '[object Array]') {
                var allowedChars = $.grep(setting, function (e) { return e.Name == 'NOTAMAllowedSymbols'; })[0];
                var forbiddenWords = $.grep(setting, function (e) { return e.Name == 'NOTAMForbiddenWords'; })[0];

                if (allowedChars && allowedChars.Value) {
                    var re = new RegExp(allowedChars.Value, "g");
                    if (!re.test(freeInputText)) return false;
                }

                if (forbiddenWords && forbiddenWords.Value) {
                    var listOfforbiddenWords = forbiddenWords.Value.split('|');
                    for (var i = 0; i < listOfforbiddenWords.length; i++) {
                        if (freeInputText.indexOf(listOfforbiddenWords[i]) > -1) {
                            return false;
                        }
                    }
                }
            }

            return true;

        },


        resizeGrid: function (grid) {
            //Define Elements Needed
            var header = $("#header-content");
            var content = $("#main-content");
            var grid = $(grid);

            //Other variables
            var minimumAcceptableGridHeight = 250; //This is roughly 5 rows 
            var otherElementsHeight = 0;

            //Get Window Height 
            var windowHeight = $(window).innerHeight();

            //Get Header Height if its existing
            var hasHeader = header.length;
            var headerHeight = hasHeader ? header.outerHeight(true) : 0;

            //Get the Grid Element and Areas Inside It
            var contentArea = grid.find(".k-grid-content");  //This is the content Where Grid is located
            var otherGridElements = grid.children().not(".k-grid-content"); //This is anything ather than the Grid iteslf like header, commands, etc
            console.debug(otherGridElements);

            //Calcualte all Grid elements height
            otherGridElements.each(function () {
                otherElementsHeight += $(this).outerHeight(true);
            });

            //Get other elements same level as Grid
            var parentDiv = grid.parent("div");

            var hasMainContent = parentDiv.length;
            if (hasMainContent) {
                var otherSiblingElements = content.children()
                        .not("#grid")
                        .not("script");

                //Calculate all Sibling element height
                otherSiblingElements.each(function () {
                    otherElementsHeight += $(this).outerHeight(true);
                });
            }

            //Padding you want to apply below your page
            var bottomPadding = 10;

            //Check if Calculated height is below threshold
            var calculatedHeight = windowHeight - headerHeight - otherElementsHeight - bottomPadding;
            var finalHeight = calculatedHeight < minimumAcceptableGridHeight ? minimumAcceptableGridHeight : calculatedHeight;

            //Apply the height for the content area
            contentArea.height(finalHeight);
        },

        showErrorMessage: function (operation, errorStatus) {

            if (errorStatus && errorStatus.Code && errorStatus.Error) {
                kendo.ui.ExtAlertDialog.show({
                title: RES.dbRes("Error: ") + errorStatus.Code,
                message: operation+"\n"+errorStatus.Error,
                icon: "k-Ext-error"
                });
            } else {
                kendo.ui.ExtAlertDialog.show({
                title: RES.dbRes("Error") ,
                message: operation+"\n"+errorStatus,
                icon: "k-Ext-error"
                });
            }

            
        },

        showSuccessMessage: function (operation) {
            if (typeof operation === 'undefined' || operation === null) {
                operation = RES.dbRes("Operation");
            }
            kendo.ui.ExtAlertDialog.show({
                title: RES.dbRes("Information"),
                message: operation + " successful.",
                icon: "k-ext-information"
            });
        },
        
        //TODO these functions are not tested but might be useful in the future.
        ArePolygonsOverlapped: function (poly1, poly2) {
        
            if(poly1.length >= 3 && poly2.length >= 3)
            {
                //close polygons
                poly1.push(poly1[0]);
                poly2.push(poly2[0]);
        
                for(var i = 0; i < poly1.length-1;i++)
                {
                    for(var k = 0; k < poly2.length-1; k++)
                    {
                        if(SimplePolylineIntersection(poly1[i],poly1[i+1],poly2[k],poly2[k+1])!=null)
                            return true;
                    }
                }
        
                return false;
            }
    
            return null;
    },

    SimplePolylineIntersection: function (latlong1,latlong2,latlong3,latlong4) {
        
        //Line segment 1 (p1, p2)
        var A1 = latlong2.Latitude - latlong1.Latitude;
        var B1 = latlong1.Longitude - latlong2.Longitude;
        var C1 = A1*latlong1.Longitude + B1*latlong1.Latitude;
    
        //Line segment 2 (p3,  p4)
        var A2 = latlong4.Latitude - latlong3.Latitude;
        var B2 = latlong3.Longitude - latlong4.Longitude;
        var C2 = A2*latlong3.Longitude + B2*latlong3.Latitude;

        var determinate = A1*B2 - A2*B1;

        var intersection;
        if(determinate != 0)
        {
            var x = (B2*C1 - B1*C2)/determinate;
            var y = (A1*C2 - A2*C1)/determinate;
        
            var intersect = new VELatLong(y,x);
        
            if(inBoundedBox(latlong1, latlong2, intersect) && 
    inBoundedBox(latlong3, latlong4, intersect))
                intersection = intersect;
            else
                intersection = null;
        }
        else //lines are parrallel
            intersection = null; 
        
        return intersection;
    },

    //latlong1 and latlong2 represent two coordinates that make up the bounded box
    //latlong3 is a point that we are checking to see is inside the box
    inBoundedBox: function (latlong1, latlong2, latlong3) {

        var betweenLats;
        var betweenLons;
    
        if(latlong1.Latitude < latlong2.Latitude)
            betweenLats = (latlong1.Latitude <= latlong3.Latitude && 
    latlong2.Latitude >= latlong3.Latitude);
        else
            betweenLats = (latlong1.Latitude >= latlong3.Latitude && 
    latlong2.Latitude <= latlong3.Latitude);
        
        if(latlong1.Longitude < latlong2.Longitude)
            betweenLons = (latlong1.Longitude <= latlong3.Longitude && 
    latlong2.Longitude >= latlong3.Longitude);
        else
            betweenLons = (latlong1.Longitude >= latlong3.Longitude && 
    latlong2.Longitude <= latlong3.Longitude);
    
        return (betweenLats && betweenLons);
    },


    }
})
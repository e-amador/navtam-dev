﻿namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    using Domain.Model.Entitities;

    public class IcaoSubjectConfig : EntityTypeConfiguration<IcaoSubject>
    {
        public IcaoSubjectConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(e => e.Name).IsRequired();
            Property(e => e.NameFrench).IsRequired();
            Property(e => e.EntityCode).IsRequired();
            Property(e => e.Code).IsRequired().HasMaxLength(2);
            Property(e => e.Scope).IsRequired().HasMaxLength(2);
        }
    }
}

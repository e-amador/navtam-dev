﻿using NavCanada.Core.Domain.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NAVTAM.NsdEngine
{
    public class ObstacleType
    {
        public ObstacleType(string name, string eFieldName)
        {
            Name = name;
            EFieldName = eFieldName;
        }

        public ObstacleType(string name, string eFieldName, string lighted, string notLighted, string painted, string notPainted, string ballmarked, string notBallmarked, string removed)
        {
            Name = name;
            EFieldName = eFieldName;
            _lightedLabel = lighted;
            _notLightedLabel = notLighted;
            _paintedLabel = painted;
            _notPaintedLabel = notPainted;
            _ballmarked = ballmarked;
            _notBallmarked = notBallmarked;
            _removedLabel = removed;
        }

        public string Id { get; }
        public string Name { get; }
        public string EFieldName { get; }

        public string LightedText(bool value) => value ? _lightedLabel : _notLightedLabel;
        public string PaintedText(bool value) => value ? _paintedLabel : _notPaintedLabel;
        public string Ballmarked(bool value) => value ? _ballmarked : _notBallmarked;
        public string RemovedText => _removedLabel;

        public string GetObstacleStatusesText(SwitchState painted, SwitchState lighted, SwitchState ballmarkers)
        {
            var status = new List<string>();
            if (painted != SwitchState.Unknown)
                status.Add(PaintedText(painted == SwitchState.On));

            if (lighted != SwitchState.Unknown)
                status.Add(LightedText(lighted == SwitchState.On));

            if (ballmarkers != SwitchState.Unknown)
                status.Add(Ballmarked(ballmarkers == SwitchState.On));

            var text = string.Join(", ", status);

            return text.Length > 0 ? $"{text}." : string.Empty;
        }

        readonly string _lightedLabel;
        readonly string _notLightedLabel;
        readonly string _paintedLabel;
        readonly string _notPaintedLabel;
        readonly string _removedLabel;
        readonly string _ballmarked;
        readonly string _notBallmarked;
    }

    public abstract class ObstacleTypes
    {
        public Dictionary<string, ObstacleType> this[string language] => GetLanguageDictionary(language);

        public bool HasType(string type) => this["en"].ContainsKey(type);

        protected abstract Dictionary<string, ObstacleType> GetLanguageDictionary(string language);

        protected static ObstacleType BuildObstacleTypeEnglish(string name, string eFieldName)
        {
            return new ObstacleType(name, eFieldName, "LGTD", "NOT LGTD", "PAINTED", "NOT PAINTED", "MARKER BALL IN PLACE", "NON-STANDARD MARKING (MARKER BALLS MISSING)", "REMOVED");
        }

        protected static ObstacleType BuildObstacleTypeFrench(string name, string eFieldName, string painted, string notPainted, string removed)
        {
            return new ObstacleType(name, eFieldName, "LGTD", "NON LGTD", painted, notPainted, "BALISAGE SPHERIQUE EN PLACE", "BALISAGE NON-STANDARD (BALISES SPHERIQUES MANQUANTES)", removed);
        }
    }

    public enum ObstacleAreaType
    {
        Circle,
        Line
    }

    public enum ObstacleRadiusUnit
    {
        NM,
        FT
    }
}
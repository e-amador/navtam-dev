﻿namespace RSC.Model.Enums
{
    /// <summary>
    /// Contaminant type restrictions for contaminants with NO depth.
    /// </summary>
    public enum ContaminantWithNoDepthEnum : int
    {
        Dry = 20,
        Unused21 = 21,
        Wet = 22,
        Frost = 23,
        Ice = 24,
        CompactedSnow = 25,
        Unused26 = 26,
        WetIce = 27,
        Unused28 = 28,
        CompactedSnowGravelMix = 29,
        Unused30 = 30,
        Unused31 = 31,
        SlipperyWhenWet = 32
    }
}
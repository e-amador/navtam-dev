﻿using System;
using System.Runtime.Serialization;

namespace NavCanada.Core.TaskEngine.Mto
{
    [Serializable]
    [DataContract]
    public class PropertyData
    {
        public PropertyData(object val, DataType dataType, bool isArray = false)
        {
            Value = val;
            Type = dataType;
            IsArray = isArray;
        }

        [DataMember]
        public object Value
        {
            get;
            private set;
        }

        [DataMember]
        public DataType Type
        {
            get;
            private set;
        }

        [DataMember]
        public bool IsArray
        {
            get;
            private set;
        }

        public override string ToString()
        {
            return Value == null ? "" : Value is byte[] ? ByteArrayHelper.ToByteString((byte[])Value) : Value.ToString();
        }
    }
}
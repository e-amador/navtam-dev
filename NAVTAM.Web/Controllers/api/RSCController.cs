﻿using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NavCanada.Core.Proxies.AeroRdsProxy.Model;
using NAVTAM.Helpers;
using NAVTAM.NsdEngine.RSC.V10;
using NAVTAM.ViewModels;
using RSC.Business.Helpers;
using RSC.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Runway = NavCanada.Core.Proxies.AeroRdsProxy.Model.Runway;

namespace NAVTAM.Controllers.api
{
    [Authorize]
    [RoutePrefix("api/rsc")]
    public class RSCController : ApiBaseController
    {
        public RSCController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger) : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {

        }

        [HttpGet]
        [Route("FilterWithRwy")]
        public async Task<IHttpActionResult> FilterWithRwy([FromUri] ResourceQuery res)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                var doaGeoArea = await GeoLocationCache.GetUserDoa(Uow, ApplicationContext.GetUserId());
                var subjects = await Uow.SdoCacheRepo.FilterAerodromesWithRunwaysAsync(res.Query, res.Size, doaGeoArea);

                return Ok(subjects);

            }, "api/rsc/FilterWithRwy");
        }

        [HttpGet]
        [Route("GetRwys")]
        public async Task<IHttpActionResult> GetRwys(string sdoId)
        {
            return await LogAction(async () => 
            {
                var sdo = await Uow.SdoCacheRepo.GetByIdAsync(sdoId);
                var aerodrome = await SdoCacheBroker.GetAerodromeCache(Uow, sdo, true, false);

                return Ok(MapToRunwayViewModel(aerodrome.Runways));
            }, "api/rsc/GetRwys");
        }

        [Route("CalculateRwyCC", Name = "CalculateRwyCC")]
        [HttpPost]
        public IHttpActionResult CalculateRwyCC([FromBody]ClearedSurfaceThird clearedSurfaceThird)
        {
            return Ok(RCAMCalculation.GetCalculationFromThird(clearedSurfaceThird));
        }

        private List<RunwayViewModel> MapToRunwayViewModel(List<Runway>runways)
        {
            var runwayViewModel = new List<RunwayViewModel>();
            foreach (var rw in runways)
            {
                var rvm = new RunwayViewModel {
                    Designator = rw.Designator,
                    RunwayId = rw.RunwayId,
                    RunwayLength = rw.RunwayLength,
                    RunwayLengthInFeet = rw.RunwayLengthInFeet,
                    RunwayWidth = rw.RunwayWidth,
                    RunwayWidthInFeet = rw.RunwayWidthInFeet,
                    Status = rw.Status,
                    Selected = false,
                    RunwayDirections = new List<RunwayDirectionViewModel>(),
                };
                foreach (var rwd in rw.RunwayDirections)
                {
                    var rvmDirection = new RunwayDirectionViewModel {
                        Designator = rwd.Designator,
                        RunwayDirectionId = rwd.RunwayDirectionId,
                        Status = rwd.Status,
                        Selected = false,
                    };
                    rvm.RunwayDirections.Add(rvmDirection);
                }
                runwayViewModel.Add(rvm);
            }

            return runwayViewModel; 
        }
    }
}
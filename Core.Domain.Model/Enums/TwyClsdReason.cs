﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum TwyClsdReason
    {
        PAINTING = 1,
        LINE_PAINTING,
        CONST,
        MAINT,
        CRACK_FILLING,
        RESURFACING,
        GRADING_AND_PACKING,
        DISABLE_ACFT,
        OTHER
    }
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NofMsgTemplateListComponent = void 0;
var core_1 = require("@angular/core");
var toastr_service_1 = require("../../common/toastr.service");
var router_1 = require("@angular/router");
var angular_l10n_1 = require("angular-l10n");
var data_service_1 = require("../shared/data.service");
var NofMsgTemplateListComponent = /** @class */ (function () {
    function NofMsgTemplateListComponent(toastr, dataService, router, route, translation) {
        this.toastr = toastr;
        this.dataService = dataService;
        this.router = router;
        this.route = route;
        this.translation = translation;
        this.loadingData = false;
        this.tableHeaders = {
            sorting: [
                { columnName: 'Name', direction: 0 },
                { columnName: 'Addresses', direction: 0 },
                { columnName: 'Created', direction: 0 }
            ],
            itemsPerPage: [
                { id: '15', text: '15' },
                { id: '30', text: '30' },
                { id: '75', text: '75' },
                { id: '100', text: '100' },
            ]
        };
    }
    NofMsgTemplateListComponent.prototype.onKeyup = function ($event) {
        switch ($event.code) {
            case "ArrowUp": //NAVIGATE TABLE ROW UP
                if (this.selectedTemplate.index > 0) {
                    this.onRowSelected(this.selectedTemplate.index - 1);
                }
                break;
            case "ArrowDown": //NAVIGATE TABLE ROW DOWN
                if (this.selectedTemplate.index < this.templates.length - 1) {
                    this.onRowSelected(this.selectedTemplate.index + 1);
                }
                break;
        }
    };
    NofMsgTemplateListComponent.prototype.ngOnInit = function () {
        this.getMessageTemplates();
    };
    NofMsgTemplateListComponent.prototype.confirmDelete = function () {
        $('#modal-delete').modal({});
    };
    NofMsgTemplateListComponent.prototype.backToMsgList = function () {
        this.router.navigate(['/dashboard/']);
    };
    NofMsgTemplateListComponent.prototype.deleteTemplate = function () {
        var _this = this;
        if (this.selectedTemplate) {
            this.dataService.deleteMessageTemplate(this.selectedTemplate.id)
                .subscribe(function (result) {
                _this.getMessageTemplates();
                //let msg = this.translation.translate('NdsClient.SuccessClientDeleted');
                var msg = "Template successfully deleted!";
                _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            }, function (error) {
                //let msg = this.translation.translate('NdsClient.FailureClientDeleted');
                var msg = "An error has ocurred deleting the template!";
                _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
        }
    };
    NofMsgTemplateListComponent.prototype.onRowSelected = function (index) {
        this.selectMessageTemplate(this.templates[index]);
    };
    NofMsgTemplateListComponent.prototype.onPageChange = function (page) {
        this.getMessageTemplates();
    };
    NofMsgTemplateListComponent.prototype.selectMessageTemplate = function (template) {
        if (this.selectedTemplate) {
            this.selectedTemplate.isSelected = false;
        }
        this.selectedTemplate = template;
        this.selectedTemplate.isSelected = true;
    };
    NofMsgTemplateListComponent.prototype.getMessageTemplates = function () {
        var _this = this;
        try {
            this.loadingData = true;
            this.dataService.getMessageTemplates()
                .subscribe(function (templates) {
                _this.processMessageTemplates(templates);
            });
        }
        catch (e) {
            this.loadingData = false;
        }
    };
    NofMsgTemplateListComponent.prototype.processMessageTemplates = function (templates) {
        for (var iter = 0; iter < templates.length; iter++) {
            templates[iter].index = iter;
        }
        if (templates.length > 0) {
            this.templates = templates;
            var index = 0;
            this.templates[index].isSelected = true;
            this.selectedTemplate = this.templates[index];
            this.loadingData = false;
            this.onRowSelected(index);
        }
        else {
            this.templates = [];
            this.selectedTemplate = null;
            this.loadingData = false;
        }
    };
    NofMsgTemplateListComponent.prototype.confirmSubmit = function () {
        $('#modal-sendnotams').modal({});
    };
    __decorate([
        core_1.HostListener('window:keyup', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], NofMsgTemplateListComponent.prototype, "onKeyup", null);
    NofMsgTemplateListComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/dashboard/nof/nof-msg-template-list.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, data_service_1.DataService,
            router_1.Router,
            router_1.ActivatedRoute,
            angular_l10n_1.TranslationService])
    ], NofMsgTemplateListComponent);
    return NofMsgTemplateListComponent;
}());
exports.NofMsgTemplateListComponent = NofMsgTemplateListComponent;
//# sourceMappingURL=nof-msg-template-list.component.js.map
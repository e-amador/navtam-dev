﻿namespace NavCanada.Core.Proxies.SignalrHubProxy
{
    using System.Net;

    public interface IAuthenticate
    {
        void AuthenticateUser(out bool status, out Cookie authCookie);
    }
}

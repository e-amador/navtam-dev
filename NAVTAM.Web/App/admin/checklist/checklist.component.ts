﻿import { Component, Inject, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormGroup, FormBuilder, AbstractControl } from '@angular/forms'
import { LocaleService, TranslationService } from 'angular-l10n';
import { MemoryStorageService } from '../shared/mem-storage.service'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';
import { WindowRef } from '../../common/windowRef.service';
import { DataService } from '../shared/data.service'

import {
    IQueryOptions,
    IPaging,
    ITableHeaders,
    IConfigValuePartial,
    ISeriesChecklist } from '../shared/data.model';

declare var $: any;


@Component({
    templateUrl: '/app/admin/checklist/checklist.component.html'
})
export class ChecklistComponent implements OnInit {
    queryOptions: IQueryOptions;
    paging: IPaging = {
        currentPage: 1,
        pageSize: 0,
        totalCount: 0,
        totalPages: 0,
    };
    totalPages: number = 0;
    itemsPerPageData: Array<any>;
    itemsPerPageStartValue: string;
    loadingData: boolean = false;
    tableHeaders: ITableHeaders;

    seriesRecs: ISeriesChecklist[];
    selectedSeries: ISeriesChecklist = null;

    model: any = null;
    checklistForm: FormGroup;
    latestPubConfig: IConfigValuePartial = null;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private winRef: WindowRef,
        public locale: LocaleService,
        public translation: TranslationService) {

        if (this.winRef.appConfig.ccs)
            this.router.navigate(['/administration']);


        this.tableHeaders = {
            sorting:
                [
                    { columnName: 'Series', direction: 0 },
                    { columnName: 'FIRS', direction: 0 },
                    { columnName: 'Coordinates', direction: 0 },
                    { columnName: 'Radius', direction: 0 }
                ],
            itemsPerPage:
                [
                    { id: '10', text: '10' },
                    { id: '20', text: '20' },
                    { id: '50', text: '50' },
                    { id: '100', text: '100' },
                    { id: '200', text: '200' },
                ]
        };

        this.paging.pageSize = +this.tableHeaders.itemsPerPage[0].id;
        this.queryOptions = {
            entity: "seriesRec",
            page: 1,
            pageSize: this.paging.pageSize,
            sort: this.tableHeaders.sorting[0].columnName, 
            filterValue: ""
        }

    }

    ngOnInit() {
        this.model = this.activatedRoute.snapshot.data['model'];
        this.latestPubConfig = this.model;

        const queryOptions = this.memStorageService.get(this.memStorageService.ITEM_QUERY_OPTIONS_KEY);
        if (queryOptions && queryOptions.entity === "seriesRec") {
            this.queryOptions = queryOptions;
        }
        this.configureReactiveForm();
        this.getSeriesValuesInPage(this.queryOptions.page);
    }

    get langCulture() {
        return (window["app"].cultureInfo || "en").toUpperCase();
    }

    selectSeriesRecord(serie: ISeriesChecklist) {
        if (this.selectedSeries) {
            this.selectedSeries.isSelected = false;
        }

        this.selectedSeries = serie;
        this.selectedSeries.isSelected = true;
        this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, this.selectedSeries.series);
    }

    itemsPerPageChanged(e: any): void {
        this.queryOptions.pageSize = e.value;
        this.getSeriesValuesInPage(1);
    }

    onPageChange(page: number) {
        this.getSeriesValuesInPage(page);
    }

    sort(index) {
        const dir = this.tableHeaders.sorting[index].direction;

        for (let i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }

        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
        }

        this.getSeriesValuesInPage(this.queryOptions.page);
    }

    sortingClass(index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';

        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';

        return 'sorting';
    }

    private getSeriesValuesInPage(page) {
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, this.queryOptions);

            this.dataService.getRegisteredSeriesChecklistValues(this.queryOptions)
                .subscribe((configData: any) => {
                    this.processSeriesValues(configData);
                });
        }
        catch (e) {
            this.loadingData = false;
        }
    }

    private processSeriesValues(configData: any) {
        this.paging = configData.paging;
        this.totalPages = configData.paging.totalPages;

        for (var iter = 0; iter < configData.data.length; iter++) configData.data[iter].index = iter;

        const stored = this.memStorageService.get(this.memStorageService.ITEM_ID_KEY);
        if (configData.data.length > 0) {
            this.seriesRecs = configData.data;
            let index = 0;
            if (stored) {
                index = this.seriesRecs.findIndex(x => x.series === stored);
                if (index < 0) index = 0; // when not found first should be selected;
            }
            this.seriesRecs[index].isSelected = true;
            this.selectedSeries = this.seriesRecs[index];
            this.loadingData = false;
        } else {
            this.seriesRecs = [];
            this.selectedSeries = null;
            this.loadingData = false;
        }
    }

    onSubmit() {
        $('#modal-publish').modal({});
    }

    public publishChecklist(): void { 
        this.dataService.pushChecklist()
            .subscribe(data => {
                let msg = this.translation.translate('ChecklistSession.SuccessPush');
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            }, error => {
                let msg = this.translation.translate('ChecklistSession.FailurePush');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    private configureReactiveForm() {
        this.checklistForm = this.fb.group({
            latestPublication: [{ value: this.latestPubConfig.value, disabled: false }, []],
        });
    }

    public saveData(): void {
        this.latestPubConfig.value = this.$ctrl('latestPublication').value;
        this.latestPubConfig.value = this.latestPubConfig.value.toUpperCase();
        this.dataService.saveChecklistConfig(this.latestPubConfig)
            .subscribe(data => {
                let msg = this.translation.translate('ConfigurationSession.SuccessConfigCreated');
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            }, error => {
                let msg = this.translation.translate('ConfigurationSession.FailureConfigCreated');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    public disableSave(): boolean {
        var val: string = this.$ctrl('latestPublication').value;
        if (val.trim().length === 0) return true;
        else if (this.latestPubConfig.value.localeCompare(val.toUpperCase()) === 0) return true;
        return false;
    }

    public disableReset(): boolean {
        var val: string = this.$ctrl('latestPublication').value;
        if (this.latestPubConfig.value.localeCompare(val.toUpperCase()) === 0) return true;
        else return false;
    }

    public reloadData(): void {
        this.dataService.getChecklistValue()
            .finally(() => {
                this.$ctrl('latestPublication').setValue(this.latestPubConfig.value);
            })
            .subscribe((resp: IConfigValuePartial) => {
                this.latestPubConfig = resp;
            }, error => {
                this.latestPubConfig = {
                    name: "",
                    description: "",
                    value: "",
                    category: "NOTAM",
                    action: ""
                };
            });
    }

    $ctrl(name: string): AbstractControl {
        return this.checklistForm.get(name);
    }

    confirmDeleteSeries() {
        $('#modal-delete').modal({});
    }

    deleteSeries() {
        if (this.selectedSeries) {
            this.dataService.deleteSeriesChecklist(this.selectedSeries.series)
                .subscribe(result => {
                    this.getSeriesValuesInPage(this.paging.currentPage);
                    let msg = this.translation.translate('ChecklistSession.SuccessSerieChecklistDeleted');
                    this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                }, error => {
                    let msg = this.translation.translate('ChecklistSession.FailureSerieChecklistDeleted');
                    this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        }
    }

}

function isANumber(str: string): boolean {
    return !/\D/.test(str);
}

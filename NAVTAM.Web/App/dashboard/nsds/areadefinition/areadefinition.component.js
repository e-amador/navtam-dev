"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AreaDefinitionNsdComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var data_service_1 = require("../../shared/data.service");
var areadefinition_service_1 = require("./areadefinition.service");
var AreaDefinitionNsdComponent = /** @class */ (function () {
    function AreaDefinitionNsdComponent(dataService, activatedRoute, service, router) {
        this.dataService = dataService;
        this.activatedRoute = activatedRoute;
        this.service = service;
        this.router = router;
        this.model = null;
        this.points = [];
        this.polygonEntry = false;
        this.pointAndRadiusEntry = false;
        this.pointAndRadiusRadius = "";
        this.bufferedLineEntry = false;
        this.bufferedLineBuffer = "";
        this.lowerLimit = "";
        this.upperLimit = "";
    }
    AreaDefinitionNsdComponent.prototype.ngOnInit = function () {
        window['loadLeafletMapScript'].call(this, null);
        this.model = this.activatedRoute.snapshot.data['model'];
    };
    AreaDefinitionNsdComponent.prototype.ngOnDestroy = function () {
        if (window["app"].leafletmap) {
            window["app"].leafletmap.dispose();
        }
    };
    AreaDefinitionNsdComponent.prototype.polygonEntryChange = function ($e) {
        if (this.pointAndRadiusEntry)
            this.pointAndRadiusEntry = false;
        if (this.bufferedLineEntry)
            this.bufferedLineEntry = false;
        if ($e) {
            this.polygonEntry = true;
        }
        else {
            this.polygonEntry = false;
        }
        ;
    };
    AreaDefinitionNsdComponent.prototype.pointAndRadiusEntryChange = function ($e) {
        if (this.bufferedLineEntry)
            this.bufferedLineEntry = false;
        if (this.polygonEntry)
            this.polygonEntry = false;
        if ($e) {
            this.pointAndRadiusEntry = true;
        }
        else {
            this.pointAndRadiusEntry = false;
        }
        ;
    };
    AreaDefinitionNsdComponent.prototype.bufferedLineEntryChange = function ($e) {
        if (this.polygonEntry)
            this.polygonEntry = false;
        if (this.pointAndRadiusEntry)
            this.pointAndRadiusEntry = false;
        if ($e) {
            this.bufferedLineEntry = true;
        }
        else {
            this.bufferedLineEntry = false;
        }
    };
    AreaDefinitionNsdComponent.prototype.isGeoPointInputEmpty = function () {
        if (this.points.length == 0)
            return true;
        return false;
    };
    AreaDefinitionNsdComponent.prototype.calculateGeos = function () {
        this.service.calculate(this.AreaVm);
        // do stuff with result
    };
    Object.defineProperty(AreaDefinitionNsdComponent.prototype, "langCulture", {
        get: function () {
            return (window["app"].cultureInfo || "en").toUpperCase();
        },
        enumerable: false,
        configurable: true
    });
    AreaDefinitionNsdComponent.prototype.validateKeyPressNumber = function (evt) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!this.isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
        }
    };
    AreaDefinitionNsdComponent.prototype.isANumber = function (str) {
        return !/\D/.test(str);
    };
    AreaDefinitionNsdComponent = __decorate([
        core_1.Component({
            templateUrl: './app/dashboard/nsds/areadefinition/areadefinition.component.html'
        }),
        __metadata("design:paramtypes", [data_service_1.DataService,
            router_1.ActivatedRoute,
            areadefinition_service_1.AreaDefinitionNsdService,
            router_1.Router])
    ], AreaDefinitionNsdComponent);
    return AreaDefinitionNsdComponent;
}());
exports.AreaDefinitionNsdComponent = AreaDefinitionNsdComponent;
//# sourceMappingURL=areadefinition.component.js.map
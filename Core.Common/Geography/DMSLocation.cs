﻿using System;
using System.Device.Location;
using System.Globalization;
using System.Linq;

namespace Core.Common.Geography
{
    public class DMSLocation
    {
        private DMSLocation(string value)
        {
            var parts = SplitLocation(value);
            Latitude = ParseDMSLatitude(parts[0]);
            Longitude = ParseDMSLongitude(parts[1]);
        }

        public DMSValue Latitude { get; private set; }
        public DMSValue Longitude { get; private set; }

        public DMSLocation(DMSValue latitude, DMSValue longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        public double LatitudeDecimal => Latitude.AsDecimal;
        public double LongitudeDecimal => Longitude.AsDecimal;

        public static bool IsValidDMS(string value)
        {
            var parts = SplitLocation(value);
            return parts.Length == 2 && IsValidDMSLatitude(parts[0]) && IsValidDMSLongitude(parts[1]);
        }

        public static bool IsValidDM(string value) => IsValidDMS(FromDMToDMS(value));

        public static string FromDMToDMS(string value)
        {
            var parts = SplitLocation(value).Select(p => InjectSeconds(p));
            return string.Join(" ", parts);
        }

        public static DMSLocation FromDMS(string value)
        {
            return IsValidDMS(value) ? new DMSLocation(value) : null;
        }

        public static DMSLocation FromDM(string value) => FromDMS(FromDMToDMS(value));

        public static bool IsValidDecimal(string value)
        {
            var parts = SplitLocation(value);
            return parts.Length == 2 && IsValidDecimalLatitude(parts[0]) && IsValidDecimalLongitude(parts[1]);
        }

        public static DMSLocation FromDecimal(string value)
        {
            if (IsValidDecimal(value))
            {
                var parts = SplitLocation(value);
                return new DMSLocation(DMSValue.FromDecimal(parts[0]), DMSValue.FromDecimal(parts[1]));
            }
            return null;
        }

        public static string CleanValidLocationSeparators(string value) => string.Join(" ", SplitLocation(value));

        public static bool IsValidSubfixedDecimal(string value)
        {
            var parts = SplitLocation(value);
            return parts.Length == 2 && IsValidSubfixedDecimalLatitude(parts[0]) && IsValidSubfixedDecimalLongitude(parts[1]);
        }

        public static DMSLocation FromSubfixedDecimal(string value)
        {
            if (IsValidSubfixedDecimal(value))
            {
                var parts = SplitLocation(value);
                var latitude = ConvertSubfixedToValidSigned(parts[0], 'N', 'S');
                var longitude = ConvertSubfixedToValidSigned(parts[1], 'E', 'W');
                return new DMSLocation(DMSValue.FromDecimal(latitude), DMSValue.FromDecimal(longitude));
            }
            return null;
        }

        public static bool IsKnownFormat(string value)
        {
            return IsValidDMS(value) || IsValidDecimal(value) || IsValidSubfixedDecimal(value) || IsValidDM(value);
        }

        public static DMSLocation FromKnownFormat(string value)
        {
            return FromDMS(value) ?? FromDecimal(value) ?? FromSubfixedDecimal(value) ?? FromDM(value);
        }

        public GeoCoordinate ToGeoCoordinate()
        {
            return new GeoCoordinate(LatitudeDecimal, LongitudeDecimal);
        }

        public string GeoPointText => $"POINT({LongitudeDecimal.ToString(CultureInfo.InvariantCulture)} {LatitudeDecimal.ToString(CultureInfo.InvariantCulture)})";

        public bool IsValidDMS()
        {
            return Latitude.IsValidLatitude() && Longitude.IsValidLongitude();
        }

        public string ToDMS(string separator = " ")
        {
            return $"{Latitude.AsLatitude()}{separator}{Longitude.AsLongitude()}";
        }

        public string ToDM(string separator = " ")
        {
            return $"{Latitude.AsLatitudeDM()}{separator}{Longitude.AsLongitudeDM()}";
        }

        public override string ToString()
        {
            return ToDMS();
        }

        public DMSLocation Round()
        {
            return new DMSLocation(Latitude.Round(), Longitude.Round());
        }

        public static string ToDecimalLatitudeLongitude(string location)
        {
            if (IsValidDecimal(location))
                return location;

            if (IsValidSubfixedDecimal(location))
            {
                var parts = SplitLocation(location);
                var latitude = ConvertSubfixedToValidSigned(parts[0], 'N', 'S');
                var longitude = ConvertSubfixedToValidSigned(parts[1], 'E', 'W');
                return $"{latitude} {longitude}";
            }

            var dmsLocation = FromDMS(location);

            return dmsLocation != null ? $"{dmsLocation.LatitudeDecimal} {dmsLocation.LongitudeDecimal}" : null;
        }

        public static string FlipCoordinates(string location)
        {
            var parts = SplitLocation(location);
            return $"{parts[1]} {parts[0]}";
        }

        private DMSValue ParseDMSLatitude(string value)
        {
            return ParseDMSValue(value.PadLeft(8), "N");
        }

        private DMSValue ParseDMSLongitude(string value)
        {
            return ParseDMSValue(value.PadLeft(8), "E");
        }

        private DMSValue ParseDMSValue(string value, string subfix)
        {
            var sign = value.EndsWith(subfix) ? 1 : -1;
            return new DMSValue(int.Parse(value.Substring(0, 3)) * sign, int.Parse(value.Substring(3, 2)), int.Parse(value.Substring(5, 2)));
        }

        private static bool IsValidDMSLatitude(string value)
        {
            var len = value.Length;
            return (len == 7 || len == 8) && ValidateDMSString(value, len == 8 ? 3 : 2, "N", "S", 90);
        }

        private static bool IsValidDMSLongitude(string value)
        {
            var len = value.Length;
            return (len == 7 || len == 8) && ValidateDMSString(value, len == 8 ? 3 : 2, "E", "W", 180);
        }

        private static bool ValidateDMSString(string dmsValue, int degLen, string dir1, string dir2, int maxDegrees)
        {
            if (!dmsValue.EndsWith(dir1) && !dmsValue.EndsWith(dir2))
                return false;

            int degrees, minutes, seconds;
            if (!int.TryParse(dmsValue.Substring(0, degLen), out degrees) || 
                !int.TryParse(dmsValue.Substring(degLen + 0, 2), out minutes) || 
                !int.TryParse(dmsValue.Substring(degLen + 2, 2), out seconds))
                return false;

            return DMSValue.IsValid(maxDegrees, degrees, minutes, seconds);
        }

        private static bool IsValidDecimalLatitude(string value)
        {
            return IsValidDecimalValue(value, 90) || IsValidDecimalMinutesValue(value, 90);
        }

        private static bool IsValidDecimalLongitude(string value)
        {
            return IsValidDecimalValue(value, 180) || IsValidDecimalMinutesValue(value, 180);
        }

        private static bool IsValidDecimalValue(string value, double max)
        {
            double d;
            return double.TryParse(value, out d) && (d > -max) && (d < max);
        }

        private static bool IsValidDecimalMinutesValue(string value, double max)
        {
            double d;
            if (!double.TryParse(value, out d)) 
                return false;

            // int part must have at least 4 digits
            if (Math.Abs(d) < 1000) 
                return false;

            var degMin = (int)Math.Truncate(d);
            var deg = degMin / 100;
            var min = Math.Abs(degMin % 100);

            return deg > -max && deg < max && min <= 59;
        }

        private static bool IsValidSubfixedDecimalLatitude(string value)
        {
            return IsValidDecimalLatitude(ConvertSubfixedToValidSigned(value, 'N', 'S'));
        }

        private static bool IsValidSubfixedDecimalLongitude(string value)
        {
            return IsValidDecimalLongitude(ConvertSubfixedToValidSigned(value, 'E', 'W'));
        }

        private static string ConvertSubfixedToValidSigned(string value, char plusChar, char minusChar)
        {
            var lastChar = value.LastOrDefault();

            if (value.Any(c => !char.IsDigit(c) && c != '.' && c != plusChar && c != minusChar))
                return string.Empty;

            if (lastChar == plusChar)
                return value.Substring(0, value.Length - 1);

            if (lastChar == minusChar)
                return $"-{value.Substring(0, value.Length - 1)}";

            return string.Empty;
        }

        static char[] SplitChars = new char[] { ' ', ',' };

        static string[] SplitLocation(string value)
        {
            return (value ?? "").Split(SplitChars, StringSplitOptions.RemoveEmptyEntries);
        }

        static string InjectSeconds(string dm)
        {
            var len = dm.Length;
            return len > 1 ? $"{dm.Substring(0, len - 1)}00{dm[len - 1]}" : dm;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Device.Location;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using log4net;
using NavCanada.Applications.NotamWiz.Web.Code.aeroRDS;
using NavCanada.Applications.NotamWiz.Web.Code.Extentions;
using NavCanada.Applications.NotamWiz.Web.Code.GeoTools;
using NavCanada.Applications.NotamWiz.Web.Code.Helpers;
using NavCanada.Applications.NotamWiz.Web.Models;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Proxies.AeroRdsProxy.SDOData.Models.SDOEntities;
using System.Web.Script.Serialization;

namespace NavCanada.Applications.NotamWiz.Web.NSDs.Obst.V0R1
{
    public class ObstController : NsdBaseController
    {
        private const int BeamLineThreshold = 50;
        private const int RoundingDistance = 10;

        private readonly ILog log = LogManager.GetLogger(typeof(ObstController));

        private readonly ObstacleTypes obstacleTypes = new ObstacleTypes();

        public ObstController(IUow uow, IClusterQueues clusterQueues) : base(uow, clusterQueues)
        {
        }

        public override ActionResult Index(string id)
        {
            TempData["ObstacleTypes"] = (from o in obstacleTypes[Thread.CurrentThread.CurrentUICulture.Name] select new ValueText
            {
                Value = o.Key,
                Text = o.Value.Name
            }).ToList();
            ViewBag.ObstacleTypes = Json(TempData["ObstacleTypes"]);
            return View(new ViewModel());
        }

        protected override async Task<Tuple<ProcessResult, NotamProposalViewModel>> GenerateAsync(string proposalType, NotamProposalViewModel notamProposal, NotamProposalViewModelBase notamProposalBase)
        {
            log.Object(new
            {
                Method = "GenerateAsync",
                Parameters = new
                {
                    proposalType,
                    notamProposal,
                    notamProposalBase
                }
            });
            var tokens = new JavaScriptSerializer().Deserialize<ViewModel>(notamProposal.Tokens);
            var proposal = notamProposalBase as ViewModel;
            if (proposal != null)
            {
                notamProposal.IsCatchAll = false;
                notamProposal.ContainFreeText = false;
                var itemEObstacleType = obstacleTypes["en"][proposal.ObstacleType].EFieldName;
                var itemEObstacleTypeFrench = obstacleTypes["fr"][proposal.ObstacleType].EFieldName;
                if (proposalType == "C")
                {
                    notamProposal.ReferredNumber = notamProposal.SubmittedNotam.Number;
                    notamProposal.ReferredSeries = notamProposal.SubmittedNotam.Series;
                    notamProposal.ReferredYear = notamProposal.SubmittedNotam.Year;

                    var itemELatLong = GeoDataService.IsValidDecimalLocation(proposal.Location) ? new GeoCoordinate().FromLocation(proposal.Location).ToDMSLocation() : proposal.Location;
                    notamProposal.Type = proposalType;
                    notamProposal.Code45 = "CN";
                    notamProposal.ItemE = string.Format("{0} {1} {2}", itemEObstacleType, itemELatLong, obstacleTypes["en"][proposal.ObstacleType].Removed);
                    notamProposal.ItemEFrench = proposal.Bilingual ? string.Format("{0} {1} {2}", itemEObstacleTypeFrench, itemELatLong, obstacleTypes["fr"][proposal.ObstacleType].Removed) : string.Empty;
                    return new Tuple<ProcessResult, NotamProposalViewModel>(new ProcessResult(), notamProposal);
                }
                else
                {
                    var itemELatLong = proposal.Location;
                    double itemEDistanceFrom;
                    var itemEObstacleHeight = proposal.Height;
                    var itemEObstacleElevation = proposal.Elevation;
                    var itemEReferenceFeature = "AD";
                    var itemEReferenceFeatureFrench = "AD";
                    string itemEAprxLine;
                    string itemEAprxLineFrench;
                    string alternateAd;
                    var obstacleLocation = new GeoCoordinate().FromLocation(itemELatLong);
                    itemELatLong = GeoDataService.IsValidDecimalLocation(itemELatLong) ? obstacleLocation.ToDMSLocation() : itemELatLong;
                    var parts = itemELatLong.Split(' ');
                    var obstacleGeoPoint = DbGeography.PointFromText(string.Format("POINT({0} {1})", GeoDataService.DMSToDecimal(parts[1]), GeoDataService.DMSToDecimal(parts[0])), GeoDataService.SRID);
                    var nearestAd = await GetNearestAd(obstacleGeoPoint);
                    var subjectLocation = new GeoCoordinate(nearestAd.Item2.SubjectGeoRefPoint.Latitude??0, nearestAd.Item2.SubjectGeoRefPoint.Longitude??0);

                    var ahp = AeroRdsUtility.GetAhpByMid(nearestAd.Item2.Mid);

                    var itemECardinalPoint = GeoDataService.GetDirection(subjectLocation.BearingTo(obstacleLocation) - NSDUtilities.GetAhpMagVar(Uow, ahp));
                    
                    if (ahp == null)
                    {
                        throw new Exception("AHP not found.");
                    }
                    var nearestAdRunways = AeroRdsUtility.GetExpandedRwyForAhp(nearestAd.Item2.Mid);
                    var nearestAdHasWaterRunways = nearestAdRunways.Any() && nearestAdRunways.Any(r => r.TxtDesig.StartsWith("RWY-W"));
                    var nearestAdIsHeliport = ahp.CodeType.Equals("HP");
                    if (nearestAd.Item1 > 5) // FIR 
                    {
                        alternateAd = nearestAd.Item2.Designator.Any(char.IsDigit) ? string.Format("{0} {1}", nearestAd.Item2.Designator, nearestAd.Item2.Name) : string.Empty;
                        itemEDistanceFrom = Math.Round(GeoDataService.MetersToNM(obstacleLocation.GetDistanceTo(subjectLocation)));
                        notamProposal.Radius = 2;
                        notamProposal.Scope = "E";
                        notamProposal.Traffic = "V";
                        notamProposal.Fir = Subject.Fir;
                        foreach (var fir in GeoDataService.GetFIRsGeoInfo().Where(a => a.Key != Subject.Fir && a.Value.Distance(obstacleGeoPoint) <= GeoDataService.NMToMeters(2)))
                        {
                            notamProposal.ItemA += string.Format(" {0}", fir.Key);
                        }
                        if (nearestAdHasWaterRunways)
                        {
                            itemEReferenceFeature = string.Format("(WATER) {0}", itemEReferenceFeature);
                            itemEReferenceFeatureFrench = string.Format("(HYDRO) {0}", itemEReferenceFeatureFrench);
                        }
                        if (nearestAdIsHeliport)
                        {
                            itemEReferenceFeature = string.Format("(HELI) {0}", itemEReferenceFeature);
                            itemEReferenceFeatureFrench = itemEReferenceFeature;
                        }
                        itemEReferenceFeature = string.Format("{0} {1}", nearestAd.Item2.Name, itemEReferenceFeature);
                        itemEReferenceFeatureFrench = string.Format("{0} {1}", nearestAd.Item2.Name, itemEReferenceFeatureFrench);
                        notamProposal.Series = GetNotamSeriesByGeoRegion(obstacleGeoPoint, "OB");
                        itemEAprxLine = string.Format("(APRX {0}NM {1} {2})", itemEDistanceFrom, itemECardinalPoint, itemEReferenceFeature);
                        itemEAprxLineFrench = string.Format("(APRX {0}NM {1} {2})", itemEDistanceFrom, itemECardinalPoint, itemEReferenceFeatureFrench);
                    }
                    else
                    {
                        alternateAd = nearestAd.Item2.Designator.Any(char.IsDigit) ? string.Format("{0} {1}", nearestAd.Item2.Designator, nearestAd.Item2.Name) : string.Empty;
                        if (nearestAd.Item2.SubjectGeoRefPoint.Latitude != null)
                        {
                            if (nearestAd.Item2.SubjectGeoRefPoint.Longitude != null)
                            {
                                subjectLocation = new GeoCoordinate(nearestAd.Item2.SubjectGeoRefPoint.Latitude??0, nearestAd.Item2.SubjectGeoRefPoint.Longitude??0);
                            }
                        }
                        itemEDistanceFrom = Math.Round(GeoDataService.MetersToNM(obstacleLocation.GetDistanceTo(subjectLocation)));
                        notamProposal.ItemA = GetUpdatedItemA(nearestAd.Item2.Designator);
                        notamProposal.Radius = 5;
                        notamProposal.Scope = "AE";
                        notamProposal.Traffic = "IV";
                        notamProposal.Series = await GetNotamProposalSeriesBySubjectCode(nearestAd.Item2.Designator, "OB");
                        var hasRunwaysInfo = nearestAdRunways.Any() && !nearestAdRunways.Any(r => r.Rdn.Any(rdn => string.IsNullOrEmpty(rdn.GeoLat)));
                        if (itemEDistanceFrom > 3 || !hasRunwaysInfo || nearestAdHasWaterRunways || nearestAdIsHeliport)
                        {
                            itemEAprxLine = string.Format("(APRX {0}NM {1} {2})", itemEDistanceFrom, itemECardinalPoint, itemEReferenceFeature);
                            itemEAprxLineFrench = string.Format("(APRX {0}NM {1} {2})", itemEDistanceFrom, itemECardinalPoint, itemEReferenceFeature);
                        }
                        else
                        {
                            // create geo for each runway directions and find nearest point
                            var minDistance = double.MaxValue;
                            Rdn nearestRdn = null;
                            GeoCoordinate nearestRdnGeo = null;
                            GeoCoordinate otherRdnGeo = null;
                            try
                            {
                                foreach (var runway in nearestAdRunways)
                                {
                                    foreach (var runwayRdn in runway.Rdn)
                                    {
                                        var runwayDirectionGeo = new GeoCoordinate().FromDMS(runwayRdn.GeoLat, runwayRdn.GeoLong);
                                        var distance = runwayDirectionGeo.GetDistanceTo(obstacleLocation);
                                        if (distance < minDistance)
                                        {
                                            minDistance = distance;
                                            nearestRdnGeo = runwayDirectionGeo;
                                            nearestRdn = runwayRdn;
                                            var otherRdn = runway.Rdn.First(rdn => rdn.Mid != runwayRdn.Mid);
                                            otherRdnGeo = new GeoCoordinate().FromDMS(otherRdn.GeoLat, otherRdn.GeoLong);
                                        }
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                var s = e.Message;
                                s += string.Empty;
                            }

                            //var sdoBearing = NSDUtilities.GetRdnBearing(Uow, ahp, nearestRdn);
                            var runwayBearing = nearestRdnGeo.BearingTo(otherRdnGeo);

                            var rcl = new GeoLineDto(nearestRdnGeo, otherRdnGeo);
                            var distanceToRcl = GeoDataService.MeterToFeet(obstacleLocation.DistanceToLine(rcl));
                            var beam = new GeoLineDto(nearestRdnGeo.DestinationPoint(runwayBearing + 90, minDistance * 2), nearestRdnGeo.DestinationPoint(runwayBearing - 90, minDistance * 2));
                            var distanceToBeam = GeoDataService.MeterToFeet(obstacleLocation.DistanceToLine(beam));
                            //var extendedRcl = new GeoLineDto(nearestRdnGeo.DestinationPoint(runwayBearing - 180, minDistance * 2), otherRdnGeo.DestinationPoint(runwayBearing, minDistance * 2));
                            //var distanceToExtendedRcl = GeoDataService.MeterToFeet(obstacleLocation.DistanceToLine(extendedRcl));
                            var itemEDisplacedThreshold = AeroRdsUtility.IsRdnDisplaced(nearestRdn.Mid) ? "DTHR" : "THR";
                            var itemERwyDirectionDesignator = nearestRdn.TxtDesig;

                            // find location of obstacle
                            var ax = rcl.X.Latitude;
                            var ay = rcl.X.Longitude;
                            var bx = rcl.Y.Latitude;
                            var by = rcl.Y.Longitude;
                            var x = obstacleGeoPoint.Latitude;
                            var y = obstacleGeoPoint.Longitude;
                            var position = Math.Sign((double)((bx - ax) * (y - ay) - (@by - ay) * (x - ax)));                            
                            //var runwayDirection = int.Parse(Regex.Match(nearestRdn.TxtDesig, @"\d+").Value);

                            var nearestRdnBearing = int.Parse(new String(nearestRdn.TxtDesig.TakeWhile(Char.IsDigit).ToArray()));

                            itemECardinalPoint = GeoDataService.RunwayDirectionToCardinal(nearestRdnBearing, position == 1, false);
                            var itemECardinalPointFrench = GeoDataService.RunwayDirectionToCardinal(nearestRdnBearing, position == 1, true);
                            var angleOfObstacle = nearestRdnGeo.Angle(obstacleLocation, otherRdnGeo);
                            double itemEDistanceInFeetXY;
                            double itemEDistanceInFeetZ;
                            if (distanceToBeam <= BeamLineThreshold) // at beam
                            {
                                itemEDistanceInFeetZ = Math.Round(distanceToRcl / RoundingDistance) * RoundingDistance;
                                itemEAprxLine = string.Format("ABEAM {0} {1} AND {2}FT {3} RCL", itemEDisplacedThreshold, itemERwyDirectionDesignator, itemEDistanceInFeetZ, itemECardinalPoint);
                                itemEAprxLineFrench = string.Format("PAR LE TRAVERS {0} {1} ET {2}FT {3} DE RCL", itemEDisplacedThreshold, itemERwyDirectionDesignator, itemEDistanceInFeetZ, itemECardinalPointFrench);
                            }
                            else if (angleOfObstacle > 90) // before runway
                            {
                                itemEDistanceInFeetXY = Math.Round(distanceToBeam / RoundingDistance) * RoundingDistance;
                                itemEDistanceInFeetZ = Math.Round(distanceToRcl / RoundingDistance) * RoundingDistance;
                                itemEAprxLine = string.Format("APRX {0}FT BFR {1} {2} AND {3}FT {4} EXTENDED RCL", itemEDistanceInFeetXY, itemEDisplacedThreshold, itemERwyDirectionDesignator, itemEDistanceInFeetZ, itemECardinalPoint);
                                itemEAprxLineFrench = string.Format("APRX {0}FT BFR {1} {2} ET {3}FT {4} DU PROLONGEMENT DE RCL", itemEDistanceInFeetXY, itemEDisplacedThreshold, itemERwyDirectionDesignator, itemEDistanceInFeetZ, itemECardinalPointFrench);
                            }
                            else // next to runway
                            {
                                itemEDistanceInFeetXY = Math.Round(distanceToBeam / RoundingDistance) * RoundingDistance;
                                itemEDistanceInFeetZ = Math.Round(distanceToRcl / RoundingDistance) * RoundingDistance;
                                itemEAprxLine = string.Format("APRX {0}FT BEYOND {1} {2} AND {3}FT {4} RCL", itemEDistanceInFeetXY, itemEDisplacedThreshold, itemERwyDirectionDesignator, itemEDistanceInFeetZ, itemECardinalPoint);
                                itemEAprxLineFrench = string.Format("APRX {0}FT APRES {1} {2} ET {3}FT {4} DE RCL", itemEDistanceInFeetXY, itemEDisplacedThreshold, itemERwyDirectionDesignator, itemEDistanceInFeetZ, itemECardinalPointFrench);
                            }
                        }
                    }
                    var itemE = string.Format("{0} {1} {2}. {3}FT AGL {4}FT AMSL. {5}{6}.", itemEObstacleType, itemELatLong, itemEAprxLine, itemEObstacleHeight, itemEObstacleElevation, GetItemELighted("en", proposal.ObstacleType, proposal.Lighted), GetItemEPainted("en", proposal.ObstacleType, proposal.Painted));
                    var itemEFrench = string.Format("{0} {1} {2}. {3}FT AGL {4}FT AMSL. {5}{6}.", itemEObstacleTypeFrench, itemELatLong, itemEAprxLineFrench, itemEObstacleHeight, itemEObstacleElevation, GetItemELighted("fr", proposal.ObstacleType, proposal.Lighted), GetItemEPainted("fr", proposal.ObstacleType, proposal.Painted));
                    notamProposal.Type = proposalType;
                    notamProposal.Code23 = "OB";
                    notamProposal.Code45 = "CE";
                    notamProposal.Purpose = "M";
                    notamProposal.Lower = 0;
                    notamProposal.Upper = (proposal.Elevation + 100) / 100;
                    if (nearestAd.Item1 > 5 || string.IsNullOrEmpty(alternateAd))
                    {
                        notamProposal.ItemE = itemE;
                        notamProposal.ItemEFrench = proposal.Bilingual ? itemEFrench : string.Empty;
                    }
                    else
                    {
                        var prefixE = nearestAdHasWaterRunways?" (WATER)":nearestAdIsHeliport?" (HELI)":"";
                        var prefixF = nearestAdHasWaterRunways?" (HYDRO)":nearestAdIsHeliport?" (HELI)":"";
                        notamProposal.ItemE = string.Format("{0}{1}\n{2}", alternateAd,prefixE, itemE);
                        notamProposal.ItemEFrench = proposal.Bilingual ? string.Format("{0}{1}\n{2}", alternateAd,prefixF, itemEFrench) : string.Empty;
                    }
                    notamProposal.Fir = Subject.Fir;
                    notamProposal.Coordinates = GeoDataService.DMSToRoundedDM(proposal.Location);
                }
            }
            return new Tuple<ProcessResult, NotamProposalViewModel>(new ProcessResult(), notamProposal);
        }

        protected override async Task<NotamProposalViewModel> ProcessNewProposalAsync(NotamProposalViewModel notamProposal)
        {
            return notamProposal;
        }

        protected override async Task<ProcessResult> ValidateAsync(NotamProposalViewModelBase notamProposalBase)
        {
            log.Object(new
            {
                Method = "ValidateAsync",
                Parameters = new
                {
                    notamProposalBase
                }
            });
            if (AeroRdsUtility.GetAhpByMid(Subject.SubjectMid) == null)
            {
                return new ProcessResult
                {
                    Success = false,
                    ErrorCode = "AOAC-003",
                    Errors = new List<string>
                    {
                        "Invalid Aerodrome."
                    }
                };
            }
            var validator = new ObstValidator(this, DbRes);
            var validationResult = await validator.ValidateAsync(notamProposalBase as ViewModel);
            if (!validationResult.IsValid)
            {
                var result = new ProcessResult
                {
                    Success = false,
                    ErrorCode = "AOAC-001"
                };
                foreach (var failure in validationResult.Errors)
                {
                    result.Errors.Add(failure.ErrorMessage);
                }
                return result;
            }
            return new ProcessResult();
        }

        private string GetItemELighted(string language, string obstacleType, string lighted)
        {
            return lighted == "Y" ? obstacleTypes[language][obstacleType].Lighted : obstacleTypes[language][obstacleType].NotLighted;
        }

        private string GetItemEPainted(string language, string obstacleType, string painted)
        {
            switch (painted)
            {
                case "Y":
                    return string.Format(", {0}", obstacleTypes[language][obstacleType].Painted);
                case "N":
                    return string.Format(", {0}", obstacleTypes[language][obstacleType].NotPainted);
                default:
                    return string.Empty;
            }
        }

        private class ObstacleType
        {
            public ObstacleType(string name, string eFieldName, string lighted, string notLighted, string painted, string notPainted, string removed)
            {
                Name = name;
                EFieldName = eFieldName;
                Lighted = lighted;
                NotLighted = notLighted;
                Painted = painted;
                NotPainted = notPainted;
                Removed = removed;
            }

            public string EFieldName
            {
                get;
                private set;
            }

            public string Lighted
            {
                get;
                private set;
            }

            public string Name
            {
                get;
                private set;
            }

            public string NotLighted
            {
                get;
                private set;
            }

            public string NotPainted
            {
                get;
                private set;
            }

            public string Painted
            {
                get;
                private set;
            }

            public string Removed
            {
                get;
                private set;
            }
        }

        private class ObstacleTypes
        {
            private readonly Dictionary<string, Dictionary<string, ObstacleType>> dictionary = new Dictionary<string, Dictionary<string, ObstacleType>>
            {
                { "en", new Dictionary<string, ObstacleType>
                {
                    { "Obs01", BuildObstacleTypeEN("Tower", "TOWER") },
                    { "Obs02", BuildObstacleTypeEN("NDB Tower", "NDB TOWER") },
                    { "Obs03", BuildObstacleTypeEN("Smoke Stack", "SMOKE STACK") },
                    { "Obs04", BuildObstacleTypeEN("Wind Turbine", "WIND TURBINE") },
                    { "Obs05", BuildObstacleTypeEN("Drill Rig", "DRILL RIG") },
                    { "Obs06", BuildObstacleTypeEN("Crane", "CRANE") },
                    { "Obs07", BuildObstacleTypeEN("Bridge", "BRIDGE") },
                    { "Obs08", BuildObstacleTypeEN("Cable Crossing", "CABLE CROSSING") },
                    { "Obs09", BuildObstacleTypeEN("Building", "BUILDING") },
                    { "Obs10", BuildObstacleTypeEN("Rig On Land", "RIG") },
                    { "Obs11", BuildObstacleTypeEN("Rig On Water", "RIG") },
                    { "Obs12", BuildObstacleTypeEN("Mobile Crane", "MOBILE CRANE") },
                    { "Obs13", BuildObstacleTypeEN("Obst", "OBST") },
                    { "Obs14", BuildObstacleTypeEN("Antenna", "ANTENNA") },
                    { "Obs15", BuildObstacleTypeEN("Multiple Cranes", "MULTIPLE CRANES") }
                } },
                { "fr", new Dictionary<string, ObstacleType>
                {
                    { "Obs01", BuildObstacleTypeFR("Tour", "TOUR", "PEINTE", "NON PEINTE", "ENLEVEE") },
                    { "Obs02", BuildObstacleTypeFR("Tour NDB", "TOUR NDB", "PEINTE", "NON PEINTE", "ENLEVEE") },
                    { "Obs03", BuildObstacleTypeFR("Cheminée", "CHEMINEE", "PEINTE", "NON PEINTE", "ENLEVEE") },
                    { "Obs04", BuildObstacleTypeFR("Éolienne", "EOLIENNE", "PEINTE", "NON PEINTE", "ENLEVEE") },
                    { "Obs05", BuildObstacleTypeFR("Équipement de forage", "EQUIPEMENT DE FORAGE", "PEINT", "NON PEINT", "ENLEVE") },
                    { "Obs06", BuildObstacleTypeFR("Grue", "GRUE", "PEINTE", "NON PEINTE", "ENLEVEE") },
                    { "Obs07", BuildObstacleTypeFR("Pont", "PONT", "PEINT", "NON PEINT", "ENLEVE") },
                    { "Obs08", BuildObstacleTypeFR("Traverse de câbles", "TRAVERSE DE CABLE", "PEINTE", "NON PEINTE", "ENLEVEE") },
                    { "Obs09", BuildObstacleTypeFR("Bâtiment", "BATIMENT", "PEINT", "NON PEINT", "ENLEVE") },
                    { "Obs10", BuildObstacleTypeFR("Derrick", "DERRICK", "PEINT", "NON PEINT", "ENLEVE") },
                    { "Obs11", BuildObstacleTypeFR("Plate-forme", "PLATE-FORME", "PEINTE", "NON PEINTE", "ENLEVEE") },
                    { "Obs12", BuildObstacleTypeFR("Grue mobile", "GRUE MOBILE", "PEINTE", "NON PEINTE", "ENLEVEE") },
                    { "Obs13", BuildObstacleTypeFR("Obst", "OBST", "PEINT", "NON PEINT", "ENLEVE") },
                    { "Obs14", BuildObstacleTypeFR("Antenne", "ANTENNE", "PEINTE", "NON PEINTE", "ENLEVEE") },
                    { "Obs15", BuildObstacleTypeFR("Grues multiples", "GRUES MULTIPLES", "PEINTES", "NON PEINTES", "ENLEVEES") }
                } }
            };

            public Dictionary<string, ObstacleType> this[string language]
            {
                get
                {
                    return dictionary[language];
                }
            }

            private static ObstacleType BuildObstacleTypeEN(string name, string eFieldName)
            {
                return new ObstacleType(name, eFieldName, "LGTD", "NOT LGTD", "PAINTED", "NOT PAINTED", "REMOVED");
            }

            private static ObstacleType BuildObstacleTypeFR(string name, string eFieldName, string painted, string notPainted, string removed)
            {
                return new ObstacleType(name, eFieldName, "LGTD", "NON LGTD", painted, notPainted, removed);
            }
        }
    }
}
﻿namespace NavCanada.Services.Api.Diagnostics.Data.Contracts
{
    using System;
    using System.Data.Entity;
    using System.Threading.Tasks;

    public interface IDiagnosticUow : IDisposable
    {
        // Save pending changes to the data store.
        int SaveChanges();
        Task<int> SaveChangesAsync();

        // Reject all pending changes.
        void Detach();

        //Transactional region
        void BeginTransaction();
        void Commit();
        void Rollback();

        DbContext DbContext
        {
            get;
        }

        //repos
        ILogRepo LogRepo { get; }
        IKeepAliveRepo KeepAliveRepo { get; }
    }
}

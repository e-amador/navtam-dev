﻿import { Component, OnInit, OnChanges, OnDestroy, Inject, Input } from '@angular/core';
import { TranslationService } from 'angular-l10n';
import { FormGroup, FormBuilder, FormArray, AbstractControl, Validators } from '@angular/forms';
import { IClearedWidthModel, IClearedPortionOffset, CardinalSideEnum } from '../rsc.model';
import { RscComponent } from '../rsc.component';
import { RscService } from '../rsc.service';

import {
} from '../../../shared/data.model';

import * as moment from 'moment';
import { TOASTR_TOKEN, Toastr } from '../../../../common/toastr.service';

@Component({
    selector: 'rsc-cleared-width',
    templateUrl: './app/dashboard/nsds/rsc/components/rsc-clearedwidth.component.html'
})
export class ClearedWidthComponent implements OnInit, OnChanges, OnDestroy {
    @Input('form') mainForm: FormGroup = null;
    @Input('model') model: IClearedWidthModel = null;
    @Input('isReadOnly') isReadOnly: boolean = false;

    frmGroup: FormGroup;
    isFormLoadingData: boolean;

    clearedType: number = 0;

    cardinalOptions = {
        width: "100%",
        placeholder: {
            id: '-1',
            text: this.translation.translate('ClearedWidthComponent.Select')
        }
    }

    cardinalValues: any;
    cardinalPointsByDirections: any;

    firstOffset: IClearedPortionOffset = null;
    secondOffset: IClearedPortionOffset = null;
    rwyWidth: number = 0;

    constructor(
        private fb: FormBuilder,
        public translation: TranslationService,
        private rscService: RscService,
        @Inject(TOASTR_TOKEN) private toastr: Toastr) {
    }

    ngOnInit() {
        this.isFormLoadingData = false;
        this.cardinalValues = this.model.parent.cardinalSideEnum;
        this.cardinalPointsByDirections = this.model.parent.cardinalPointsByDirections;
        this.firstOffset = this.model.clearedPortionOffsets[0];
        this.secondOffset = this.model.clearedPortionOffsets[1];
        this.rwyWidth = +this.model.runway.runwayWidthInFeet;

        this.configureReactiveForm();


    }

    ngOnChanges() {

    }

    ngOnDestroy() {
    }

    $ctrl(name: string): AbstractControl {
        return this.frmGroup.get(name);
    }

    clearedTypeChanged(value) {
        this.clearedType = value;
        if (this.clearedType === 0) {

            this.$ctrl('centredClearedWidth').patchValue(0);
            this.firstOffset.offsetFT = 0;
            this.firstOffset.offsetSide = CardinalSideEnum.E;
            this.secondOffset.offsetFT = 0;
            this.secondOffset.offsetSide = CardinalSideEnum.E;

            this.model.clearedFull = true;
            this.model.centredClearedWidth = 0;
            this.model.clearedPortionOffsets = [];
            this.model.clearedPortionOffsets.push(this.firstOffset);
            this.model.clearedPortionOffsets.push(this.secondOffset);

        } else if (this.clearedType === 1) {

            this.firstOffset.offsetFT = 0;
            this.firstOffset.offsetSide = CardinalSideEnum.E;
            this.secondOffset.offsetFT = 0;
            this.secondOffset.offsetSide = CardinalSideEnum.E;

            this.model.clearedFull = false;
            this.model.clearedPortionOffsets = [];
            this.model.clearedPortionOffsets.push(this.firstOffset);
            this.model.clearedPortionOffsets.push(this.secondOffset);

        } else {
            this.$ctrl('centredClearedWidth').patchValue(0);

            this.model.clearedFull = false;
            this.model.centredClearedWidth = 0;
            this.model.clearedPortionOffsets = [];
            this.model.clearedPortionOffsets.push(this.firstOffset);
            this.model.clearedPortionOffsets.push(this.secondOffset);
        }
    }

    private configureReactiveForm() {
        const frmArrControls = <FormArray>this.mainForm.controls['frmClearedWithArr'];

        this.frmGroup = this.fb.group({
            clearedType: [{ value: this.clearedType, disabled: this.isReadOnly }, []],
            centredClearedWidth: [{ value: this.model.centredClearedWidth, disabled: this.isReadOnly }, []],
            grpFirstOffset: this.fb.group({
                offsetFT: this.firstOffset.offsetFT,
                offsetSide: this.firstOffset.offsetSide,
            }),
            grpSecondOffset: this.fb.group({
                offsetFT: this.secondOffset.offsetFT,
                offsetSide: this.secondOffset.offsetSide,
            }),
        });

        frmArrControls.push(this.frmGroup);

    }

    onOffsetFTChange(groupName: string): void {
        if (groupName === 'first') {
            this.firstOffset.offsetFT = this.$ctrl('grpFirstOffset.offsetFT').value;
        } else {
            this.secondOffset.offsetFT = this.$ctrl('grpSecondOffset.offsetFT').value;
        }
    }

    onOffsetSideChanged(groupName: string, e: any): void {
        if (e.value) {
            let cardinalId = e.value;
            if (groupName === 'first') {
                const control = this.frmGroup.get('grpFirstOffset.offsetSide')
                if (this.isFormLoadingData) {
                    cardinalId = control.value;
                } else {
                    if (cardinalId !== control.value) {
                        control.setValue(cardinalId);
                        control.markAsDirty();
                        this.firstOffset.offsetSide = cardinalId;
                    }
                }
            } else {
                const control = this.frmGroup.get('grpSecondOffset.offsetSide');
                if (this.isFormLoadingData) {
                    cardinalId = control.value;
                } else {
                    if (cardinalId !== control.value) {
                        control.setValue(cardinalId);
                        control.markAsDirty();
                        this.secondOffset.offsetSide = cardinalId;
                    }
                }
            }
        }
    }

    public validaClearedWidth(): boolean {
        if (this.clearedType === 0) return true;
        else if (this.clearedType === 1 && this.model.centredClearedWidth < this.rwyWidth) return true;
        else if (this.clearedType === 2) {
            let totalCleared: number = this.firstOffset.offsetFT + this.secondOffset.offsetFT;
            if (totalCleared < this.rwyWidth) return true;
        }
        return false;
    }

    //Export functions
    public isFormValid(): boolean {
        if (!this.validaClearedWidth()) return false;
        //Other validations

        return true;
    }

    public exportComponentInternalData(): IClearedWidthModel {
        let data: IClearedWidthModel = {
            parent: this.model.parent,
            runway: this.model.runway,
            clearedFull: false,
            centredClearedWidth: 0,
            clearedPortionOffsets: [],
        };
        if (this.clearedType === 0) data.clearedFull = true;
        else if (this.clearedType === 1) {
            data.centredClearedWidth = +this.$ctrl('centredClearedWidth').value;
        } else {
            data.clearedPortionOffsets.push(this.firstOffset);
            data.clearedPortionOffsets.push(this.secondOffset);
        }

        return data;
    }

    public clearValues(): void {
        this.clearedTypeChanged(0);
    }


}
﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum UserFilterType
    {
        ProposalHistory = 0,
        Notam = 1,
        Report = 2
    }
}

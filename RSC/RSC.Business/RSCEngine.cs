﻿using RSC.Business.Helpers;
using RSC.Business.SDO;
using RSC.Model;
using RSC.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Runway = RSC.Model.Runway;

namespace RSC.Business
{
    public class RSCEngine
    {
        readonly ISDORepository _sdoRepository;
        readonly HashSet<string> _forbiddenWords;
        readonly bool _dummyMode;

        public RSCEngine(ISDORepository sdoRepository, bool dummymode)
        {
            _sdoRepository = sdoRepository;
            _forbiddenWords = new HashSet<string>(sdoRepository.GetForbiddenAftnWords());
            _dummyMode = dummymode;
        }

        public RSCResponse Process(RSCProposal proposal, bool retrieveRSC)
        {
            var response = new RSCResponse();

            // create the proposal context
            var context = GetProposalContext(proposal);
            
            // execute validations
            var rscValidator = new RSCValidator(context);
            rscValidator.ExecuteValidations(message =>
            {
                // pick the greatest meaning the most serious (Ok, Warning, Error)
                if (message.Status > response.Status) response.Status = message.Status;

                response.Messages.Add(message);
            });

            // RSC report
            if (response.Status != ResponseStatus.Error && retrieveRSC)
            {
                response.RSC = new RSCText()
                {
                    EnglishRSC = GetRSC(context, Language.English),
                    FrenchRSC = GetRSC(context, Language.French)
                };
            }

            return response;
        }

        public RSCProposalContext GetProposalContext(RSCProposal proposal)
        {
            var aerodrome = _sdoRepository.GetAerodrome(proposal.Aerodrome);
            var runways = (aerodrome?.Runways ?? Enumerable.Empty<IRunway>()).ToArray();

            return new RSCProposalContext()
            {
                Proposal = proposal,
                Aerodrome = aerodrome,
                Runways = runways,
                ForbiddenWords = _forbiddenWords,
                Dummymode = _dummyMode
            };
        }

        static string GetRSC(RSCProposalContext context, Language lang)
        {
            var proposal = context.Proposal;

            // cancel?
            if (proposal.CancelCurrentRSC) return CancelCurrentRSC(context);

            var rsc = new StringBuilder();

            var runwayDirections = GetRunwayDirections(proposal.Runways);

            // render runways directions
            foreach (var runwayDirection in runwayDirections)
            {
                rsc.Append(RenderRunwayDirection(runwayDirection, lang)).Append("\n\n");
            }

            // render runway NON-TALPA INFO:
            rsc.Append("ADDN NON-GRF/TALPA INFO:\n");
            foreach (var runwayDirection in runwayDirections)
            {
                rsc.Append(RenderRunwayDirectionNonTALPA(runwayDirection, lang)).Append("\n");
            }
            rsc.Append("\n");

            // taxiways
            rsc.Append(GetRemarkText(proposal.Taxiways, lang));

            // aprons
            rsc.Append(GetRemarkText(proposal.Aprons, lang));

            // general remarks
            rsc.Append(GetRemarkText(proposal.GeneralRemarks, lang));

            // next schedule observation remark
            rsc.Append(GetNextObservationRemark(proposal.NextPlannedObs, lang));

            return rsc.ToString();
        }

        static string CancelCurrentRSC(RSCProposalContext context)
        {
            // >> todo
            return string.Empty;
        }

        static RunwayDirection[] GetRunwayDirections(IEnumerable<Runway> runways)
        {
            return EnumerateRunwayDirections(runways).OrderBy(rd => rd.SortKey).ToArray();
        }

        static StringBuilder RenderRunwayDirection(RunwayDirection runwayDirection, Language lang)
        {
            var runway = runwayDirection.Runway;
            var reverse = runwayDirection.Reversed;
            var designator = runwayDirection.Designator;

            var sb = new StringBuilder();

            sb.Append("RSC").Append(' ');

            // runway identification and contaminants
            if (runway.IsGRF())
            {
                // RSC 07 1/2/3 ... 
                sb.Append(designator).Append(' ');
                sb.Append(runway.GetRunwayCCs(reverse)).Append(' ');
                sb.Append(RenderRunwayThirds(runway, reverse, lang));
            }
            else
            {
                // RSC 04/22 ... 
                sb.Append(designator).Append(' ');
                sb.Append(RenderContaminants(runway.ClearedPortion.Average.Contaminants, lang));

                // gravel treatments (Graded, Packed or Scarified)
                var gravelTreatments = GetRunwayTreatments(runway.ClearedPortion?.Treatments, IsGravelTreatment, lang);
                if (!string.IsNullOrEmpty(gravelTreatments))
                {
                    sb.Append('.').Append(' ').Append(gravelTreatments);
                }
            }
            sb.Append('.');

            // clear portion
            if (!runway.ClearedFull)
            {
                sb.Append(' ').Append(RenderRunwayWidth(runway, lang));
                if (runway.IsClearedByPortions())
                {
                    sb.Append(' ').Append(RenderRunwayClearPortionOffsets(runway, lang));
                }
                sb.Append('.');

                // remaining width
                sb.Append(' ').Append(RenderRemainingWidth(runway.RemainingWidth, lang)).Append('.');
            }

            // snowbanks
            if (runway.Snowbanks != null)
            {
                sb.Append(' ').Append(RenderRunwaySnowbanks(runway.Snowbanks, lang)).Append('.');
            }

            // other conditions (TBD...)
            var otherConditions = runway.ClearedPortion?.OtherConditions;
            if (otherConditions != null)
            {
                sb.Append(' ').Append(RenderRunwayOtherConditions(otherConditions, lang));
            }

            // treatments (Chemically Treated or Loose Sand)
            var treatments = GetRunwayTreatments(runway.ClearedPortion?.Treatments, IsSurfaceAddonTreatment, lang);
            if (!string.IsNullOrEmpty(treatments))
            {
                sb.Append(' ').Append(treatments).Append('.');
            }

            // observed at YYMMDDHHMM
            if (runway.ObsTimeUTC.HasValue)
            {
                sb.Append(' ').Append(FormatObservationDateTime(runway.ObsTimeUTC.Value, lang)).Append('.');
            }

            return sb;
        }

        static StringBuilder RenderRunwayDirectionNonTALPA(RunwayDirection runwayDirection, Language lang)
        {
            var runway = runwayDirection.Runway;
            var designator = runwayDirection.Designator;
            var reverse = runwayDirection.Reversed;
            var sb = new StringBuilder();

            sb.Append("CRFI").Append(' ').Append("RWY").Append(' ');

            // runway identifier
            sb.Append(designator).Append(' ');

            // check runway contains average friction
            var avgRwyFriction = runway?.ClearedPortion?.AverageRwyFriction;
            if (avgRwyFriction == null)
            {
                sb.Append("NR.");
                return sb;
            }

            // temperature
            sb.Append(FormatTemperatureCelcius(avgRwyFriction.Temperature)).Append(' ');

            // CRFI
            sb.Append(FormatIntValue(avgRwyFriction.FrictionCoefficients, 2)).Append(' ');

            // observation time
            sb.Append(FormatObservationDateTime(avgRwyFriction.RFObsTime, lang)).Append('.');

            return sb;
        }

        static string GetRemarkText(FreeformText text, Language lang)
        {
            var rmk = lang == Language.English ? text?.EnglishText : text?.FrenchText;
            return !string.IsNullOrEmpty(rmk) ? $"RMK: {rmk}\n" : string.Empty;
        }

        static string GetNextObservationRemark(DateTime? nextPlannedObs, Language lang)
        {
            if (nextPlannedObs.HasValue)
            {
                var nextSchedObs = lang == Language.English ? "NEXT OBS FOR" : "PROCHAINE OBS PREVUE A";
                return $"RMK: {nextSchedObs} {FormatDateTime(nextPlannedObs.Value)}";
            }
            return string.Empty;
        }

        static IEnumerable<RunwayDirection> EnumerateRunwayDirections(IEnumerable<Runway> runways)
        {
            foreach (var runway in runways.Where(r => !r.NoWinterMaintenance))
            {
                var dirs = runway.GetRunwayDirectionDesignators();
                var sortKey = $"{dirs[0]}/{dirs[1]}";
                if (runway.IsGRF())
                {
                    yield return new RunwayDirection(runway, $"{dirs[0]}", sortKey, false);
                    yield return new RunwayDirection(runway, $"{dirs[1]}", sortKey, true);
                }
                else
                {
                    yield return new RunwayDirection(runway, sortKey, sortKey, false);
                }
            };
        }

        static string GetRunwayTreatments(TreatmentEnum[] treatments, Func<TreatmentEnum, bool> filter, Language lang)
        {
            return treatments != null ? string.Join(" ", treatments.Where(filter).Select(t => t.Translate(lang))) : string.Empty;
        }

        static bool IsSurfaceAddonTreatment(TreatmentEnum value) => value == TreatmentEnum.ChemicallyTreated || value == TreatmentEnum.LoseSand;
        static bool IsGravelTreatment(TreatmentEnum value) => value == TreatmentEnum.Graded || value == TreatmentEnum.Packed || value == TreatmentEnum.Scarified;

        static string FormatCoeficients(int[] values, bool reverse)
        {
            var coefs = (reverse ? values.Reverse() : values).Select(c => $".{FormatIntValue(c, 2)}");
            return string.Join("/", coefs);
        }

        static string FormatIntValue(int value, int places) => value.ToString().PadLeft(places, '0');

        static string FormatTemperatureCelcius(string value) => $"{value}C";

        static string FormatDateTime(DateTime date) => date.ToString("yyMMddHHmm");

        static string FormatObservationDateTime(DateTime date, Language lang)
        {
            var label = lang == Language.English ? "OBS AT" : "OBS A";
            return $"{label} {FormatDateTime(date)}";
        }

        static StringBuilder RenderRunwaySnowbanks(SnowbanksRunway snowbanks, Language lang)
        {
            var sb = new StringBuilder();

            // height (?)
            var height = snowbanks.BankHeight.ToStringWithUOM();
            if (!string.IsNullOrEmpty(height))
                sb.Append(height).Append(' ');

            // snowbanks label
            sb.Append(lang == Language.English ? "SNOWBANKS" : "BANCS DE NEIGE").Append(' ');

            // bank edge
            sb.Append(snowbanks.BankEdge.ToString(lang)).Append(' ');

            // cardinal sides
            var conj = lang == Language.English ? " AND " : " ET ";
            var sides = string.Join(conj, snowbanks.BankSides.Select(s => s.Translate(lang)));
            sb.Append(sides).Append(' ');

            // end label
            var endLabel = lang == Language.English ? "RWY EDGE" : "DU BORD DE RWY";
            sb.Append(endLabel);

            return sb;
        }

        static StringBuilder RenderRunwayOtherConditions(OtherConditions otherConditions, Language lang)
        {
            var sb = new StringBuilder();
            var comments = lang == Language.English ? otherConditions?.OtherConditionsComment?.EnglishText : otherConditions?.OtherConditionsComment?.FrenchText;
            if (!string.IsNullOrEmpty(comments))
            {
                sb.Append(' ').Append(comments);
                if (!comments.EndsWith("."))
                    sb.Append('.');
            }
            return sb;
        }

        static string RenderRemainingWidth(RemainingWidth rw, Language lang)
        {
            var rwLabel = lang == Language.English ? "REMAINING WIDTH" : "LARGEUR RESTANTE";
            return $"{rwLabel} {rw.ContaminantFI.ToString(lang)}";
        }

        static string RenderRunwayWidth(Runway runway, Language lang)
        {
            var width = runway.IsClearedByPortions() ? runway.ClearedPortionOffsets.Sum(p => p.OffsetFT) : runway.CentredClearedWidth;
            return lang == Language.English ? $"{width}FT WIDTH" : $"{width}FT DE LONGEUR";
        }

        static string RenderRunwayClearPortionOffsets(Runway runway, Language lang)
        {
            var FM = lang == Language.English ? "FM" : "DE";
            var OFCLTO = lang == Language.English ? "OF CL TO" : "DE CL A";
            var OFCL = lang == Language.English ? "OF CL" : "DE CL";
            var offset1 = runway.ClearedPortionOffsets.First();
            var offset2 = runway.ClearedPortionOffsets.Last();
            return $"{FM} {offset1.OffsetFT}FT {offset1.OffsetSide.Translate(lang)} {OFCLTO} {offset2.OffsetFT}FT {offset2.OffsetSide.Translate(lang)} {OFCL}";
        }

        static string RenderRunwayThirds(Runway runway, bool reverse, Language lang)
        {
            var thirds = runway.GetRunwayThirds(reverse).Select(t => RenderContaminants(t.Contaminants, lang));
            return string.Join(", ", thirds);
        }

        static string RenderContaminants(ContaminantIns[] contaminants, Language lang)
        {
            var conj = lang == Language.English ? " AND " : " ET ";
            return string.Join(conj, contaminants.Select(c => RenderContaminantAndCoverage(c, lang)));
        }

        static string RenderContaminantAndCoverage(ContaminantIns contaminant, Language lang)
        {
            return $"{contaminant.ContamCoverage} PCT {RenderContaminantAndDepth(contaminant, lang)}";
        }

        static string RenderContaminantAndDepth(ContaminantIns contaminant, Language lang)
        {
            if (contaminant.HasDepdth)
                return $"{contaminant.ContamDepthI.ToStringWithUOM()} {contaminant.ContamType.Value.Translate(lang)}";
            else
                return $"{contaminant.ContamTypeNoD.Value.Translate(lang)}";
        }
    }
}

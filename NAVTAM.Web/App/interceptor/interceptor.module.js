"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InterceptorModule = exports.HttpsRequestInterceptor = void 0;
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var HttpsRequestInterceptor = /** @class */ (function () {
    function HttpsRequestInterceptor() {
    }
    HttpsRequestInterceptor.prototype.intercept = function (req, next) {
        var antiForgeryTokenField = document.getElementById('__antiForgeryToken');
        if (antiForgeryTokenField) {
            var xsrfReq = req.clone({ headers: req.headers.set('RequestVerificationToken', antiForgeryTokenField.value) });
            return next.handle(xsrfReq);
        }
        return next.handle(req);
    };
    HttpsRequestInterceptor = __decorate([
        core_1.Injectable()
    ], HttpsRequestInterceptor);
    return HttpsRequestInterceptor;
}());
exports.HttpsRequestInterceptor = HttpsRequestInterceptor;
;
var InterceptorModule = /** @class */ (function () {
    function InterceptorModule() {
    }
    InterceptorModule = __decorate([
        core_1.NgModule({
            providers: [
                { provide: http_1.HTTP_INTERCEPTORS, useClass: HttpsRequestInterceptor, multi: true }
            ]
        })
    ], InterceptorModule);
    return InterceptorModule;
}());
exports.InterceptorModule = InterceptorModule;
//# sourceMappingURL=interceptor.module.js.map
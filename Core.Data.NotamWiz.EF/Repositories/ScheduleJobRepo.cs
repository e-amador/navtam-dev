﻿using System.Data.Entity;

using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;

namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    public class ScheduleJobRepo : Repository<ScheduleJob>, IScheduleJobRepo
    {
        public ScheduleJobRepo(DbContext dbContext)
            : base(dbContext)
        {
        }
    }
}

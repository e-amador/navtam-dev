"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataService = void 0;
var core_1 = require("@angular/core");
var Rx_1 = require("rxjs/Rx");
var http_1 = require("@angular/http");
var windowRef_service_1 = require("../../common/windowRef.service");
var xsrf_token_service_1 = require("../../common/xsrf-token.service");
var DataService = /** @class */ (function () {
    function DataService(http, winRef, xsrfTokenService) {
        this.http = http;
        this.winRef = winRef;
        this.xsrfTokenService = xsrfTokenService;
        this.apiUrl = winRef.nativeWindow['app'].apiUrl;
        this.app = winRef.nativeWindow['app'];
        if (this.app.antiForgeryTokenValue) {
            this.xsrfTokenService.addForgeryTokenToHeader(this.app.antiForgeryTokenValue);
        }
    }
    DataService.prototype.getRoles = function () {
        return this.http.get(this.apiUrl + "admin/user/roles").map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    //USERS
    DataService.prototype.getRegisteredUsers = function (queryOptions) {
        return this.http.post(this.apiUrl + "admin/users/page/?includeDisabled=" + queryOptions.includeDisabled + "&includeDeleted=" + queryOptions.includeDeleted, queryOptions)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getAvailableRolesByOrg = function (orgId) {
        return this.http.get(this.apiUrl + "admin/user/availableroles/?id=" + orgId).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getUserModel = function (username) {
        return this.http.get(this.apiUrl + "admin/users/?username=" + username).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getDoasInOrg = function (orgId) {
        return this.http.get(this.apiUrl + "admin/orgs/doas/?id=" + orgId)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.saveUser = function (user) {
        return this.http.post(this.apiUrl + "admin/users", user)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.updateUser = function (user) {
        return this.http.put(this.apiUrl + "admin/users", user)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.resetUserPassword = function (data) {
        return this.http.put(this.apiUrl + "admin/users/resetPassword", data)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.sendVerificationEmail = function (userId) {
        return this.http.post(this.apiUrl + "admin/users/resendVerificationEmail?userId=" + userId, null)
            .map(function (response) {
            return response;
        }).catch(this.handleError);
    };
    DataService.prototype.resetPasswordVerificationCode = function (userId) {
        return this.http.post(this.apiUrl + "admin/users/resetPasswordVerificationCode?userId=" + userId, null)
            .map(function (response) {
            return response;
        }).catch(this.handleError);
    };
    DataService.prototype.deleteUser = function (username) {
        return this.http.delete(this.apiUrl + "admin/users/" + username)
            .map(function (response) {
            return response;
        }).catch(this.handleError);
    };
    DataService.prototype.deleteMultiUsers = function (usernames) {
        return this.http.put(this.apiUrl + "admin/multiusers/", usernames)
            .map(function (response) {
            return response;
        }).catch(this.handleError);
    };
    //ORGANIZATIONS
    DataService.prototype.getRegisteredOrgs = function (queryOptions) {
        return this.http.post(this.apiUrl + "admin/orgs/page", queryOptions)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getOrganizations = function () {
        return this.http.get(this.apiUrl + "admin/orgs/partial").map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getOrgUserCount = function (id) {
        return this.http.get(this.apiUrl + "admin/orgs/users/count?id=" + id).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getOrgModel = function (id) {
        return this.http.get(this.apiUrl + "admin/orgs/?id=" + id).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.saveOrg = function (org) {
        return this.http.post(this.apiUrl + "admin/orgs", org)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.updateOrg = function (org) {
        return this.http.put(this.apiUrl + "admin/orgs", org)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.deleteOrg = function (orgId) {
        return this.http.delete(this.apiUrl + "admin/orgs/" + orgId)
            .map(function (response) {
            return response;
        }).catch(this.handleError);
    };
    DataService.prototype.isOrgNameUnique = function (id, name) {
        return this.http.get(this.apiUrl + "admin/orgs/orgunique/?id=" + id + "&name=" + name)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.isOrgAdminNameUnique = function (name) {
        return this.http.get(this.apiUrl + "admin/orgs/userunique/?name=" + name)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getOrganizationsWithNsdId = function (nsdId) {
        //This is a fix? related with IE that cached the get request always. Everytime that a request is sent,
        //we need to sent something different, like the parameter that is not used on the Controller
        return this.http.get(this.apiUrl + "admin/orgs/getorgsbynsdid/?nsdId=" + nsdId + "&timeRequest=" + new Date().getTime())
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    //ICAO Subjects
    DataService.prototype.getRegisteredIcaoSubjects = function (queryOptions) {
        return this.http.post(this.apiUrl + "icaosubjects/page", queryOptions)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getIcaoSubjectModel = function (id) {
        return this.http.get(this.apiUrl + "icaosubjects/model?id=" + id).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getAllIcaoSubjectCodes = function () {
        return this.http.get(this.apiUrl + "icaosubjects/codes").map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.createIcaoSubject = function (icaoSubject) {
        return this.http.post(this.apiUrl + "icaosubjects/create", icaoSubject)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.updateIcaoSubject = function (icaoSubject) {
        return this.http.put(this.apiUrl + "icaosubjects/update", icaoSubject)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.deleteIcaoSubject = function (icaoSubjectId) {
        return this.http.delete(this.apiUrl + "icaosubjects/delete/" + icaoSubjectId)
            .map(function (response) {
            return response;
        }).catch(this.handleError);
    };
    //ICAO SubjectConditions
    DataService.prototype.getRegisteredIcaoSubjectConditions = function (subjectId, queryOptions) {
        return this.http.post(this.apiUrl + "icaoconditions/page?subId=" + subjectId, queryOptions)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getAllIcaoConditionCodes = function () {
        return this.http.get(this.apiUrl + "icaoconditions/codes").map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getIcaoConditionModel = function (id) {
        return this.http.get(this.apiUrl + "icaoconditions/model?id=" + id).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.createIcaoCondition = function (icaoSubject) {
        return this.http.post(this.apiUrl + "icaoconditions/create", icaoSubject)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.updateIcaoCondition = function (icaoSubject) {
        return this.http.put(this.apiUrl + "icaoconditions/update", icaoSubject)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.deleteIcaoCondition = function (icaoCondition) {
        return this.http.delete(this.apiUrl + "icaoconditions/delete/" + icaoCondition)
            .map(function (response) {
            return response;
        }).catch(this.handleError);
    };
    //DOAS
    DataService.prototype.getDoas = function () {
        //This is a fix? related with IE that cached the get request always. Everytime that a request is sent, 
        //we need to sent something different, like the parameter that is not used on the Controller
        return this.http.get(this.apiUrl + "doas/admin/partials/?timeRequest=" + new Date().getTime())
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    //SERIES
    DataService.prototype.getRegisteredSeries = function (queryOptions) {
        return this.http.post(this.apiUrl + "series/page", queryOptions)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.deleteSerie = function (serieId) {
        return this.http.delete(this.apiUrl + "series/delete/" + serieId)
            .map(function (response) {
            return response;
        }).catch(this.handleError);
    };
    DataService.prototype.serieCanBeDeleted = function (serieId) {
        return this.http.get(this.apiUrl + "series/candelete/?serieId=" + serieId)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getSerieModel = function (id) {
        return this.http.get(this.apiUrl + "series/getserie/?id=" + id).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getAllGeoRegions = function () {
        return this.http.get(this.apiUrl + "geo/getGeoRegions").map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.saveSeries = function (serie) {
        return this.http.post(this.apiUrl + "series/create", serie)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.updateSeries = function (serie) {
        return this.http.put(this.apiUrl + "series/update", serie)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    //Aerodrome Dissemination Category
    DataService.prototype.getRegisteredADC = function (queryOptions) {
        return this.http.post(this.apiUrl + "adc/page", queryOptions)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getRegisteredAhpNoDC = function (queryOptions) {
        return this.http.post(this.apiUrl + "adc/sdo/page", queryOptions)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getAhpNoDCTotalPending = function () {
        return this.http.get(this.apiUrl + "adc/pending/count").map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.saveADC = function (adc) {
        return this.http.post(this.apiUrl + "adc/create", adc)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getAhpModel = function (mid) {
        return this.http.get(this.apiUrl + "adc/getsdo/?id=" + mid).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getADCModel = function (id) {
        return this.http.get(this.apiUrl + "adc/getadc/?id=" + id).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.updateADC = function (adc) {
        return this.http.post(this.apiUrl + "adc/update", adc)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    //DOA Admin
    DataService.prototype.getRegisteredDoas = function (queryOptions) {
        return this.http.post(this.apiUrl + "doas/admin/page", queryOptions)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getDoaAdmin = function (id) {
        return this.http.get(this.apiUrl + "doas/admin/find/?doaId=" + id)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.copyDoaAdmin = function (id) {
        return this.http.get(this.apiUrl + "doas/admin/copy/?doaId=" + id)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.saveDoaFile = function (data) {
        return this.http.post(this.apiUrl + "doas/admin/uploadfile", data)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.saveDoa = function (data) {
        return this.http.post(this.apiUrl + "doas/admin/save", data)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.validateDoaInCanada = function (data) {
        return this.http.post(this.apiUrl + "doas/admin/validatedoaincanada", data)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.existsDoaName = function (id, name) {
        return this.http.get(this.apiUrl + "doas/admin/exists/?doaId=" + id + "&name=" + name)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.deleteDoa = function (doaId) {
        return this.http.delete(this.apiUrl + "doas/admin/delete/?doaId=" + doaId)
            .map(function (response) {
            return response;
        }).catch(this.handleError);
    };
    DataService.prototype.getAllFirs = function () {
        return this.http.get(this.apiUrl + "doas/admin/firs").map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.validateDoaLocation = function (loc) {
        return this.http.get(this.apiUrl + "doas/admin/validatepoint/?value=" + loc)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.validateDoaGeoLocation = function (id, loc) {
        return this.http.get(this.apiUrl + "doas/admin/PointInDoa/?doaId=" + id + "&value=" + loc)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.validateLocInRegion = function (locRegion) {
        return this.http.post(this.apiUrl + "doas/admin/PointInRegion/", locRegion)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.validateDoaPoints = function (points) {
        return this.http.get(this.apiUrl + "doas/admin/validatepoints/?value=" + points)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getDoaBilingualRegion = function () {
        return this.http.get(this.apiUrl + "doas/admin/bilingual/")
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getDoaNorthernRegion = function () {
        return this.http.get(this.apiUrl + "doas/admin/northern/")
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getGeoFeatures = function (region) {
        return this.http.post(this.apiUrl + "doas/admin/GetGeoFeatures/", region)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    //Geographic Regions
    DataService.prototype.getRegisteredGeoRegions = function (queryOptions) {
        return this.http.post(this.apiUrl + "geo/admin/page", queryOptions)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.deleteGeoRegion = function (geoId) {
        return this.http.delete(this.apiUrl + "geo/admin/delete/?geoId=" + geoId)
            .map(function (response) {
            return response;
        }).catch(this.handleError);
    };
    DataService.prototype.getGeoRegionModel = function (id) {
        return this.http.get(this.apiUrl + "geo/admin/find/?geoId=" + id).map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.saveGeoRegion = function (data) {
        return this.http.post(this.apiUrl + "geo/admin/save", data)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.existsGeoRegionName = function (id, name) {
        return this.http.get(this.apiUrl + "geo/admin/exists/?geoId=" + id + "&name=" + name)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    // NSD Admin
    DataService.prototype.getNsdLeaves = function () {
        return this.http.get(this.apiUrl + "nsdcategories/nsdleaves")
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getNsdsByOrgId = function (id) {
        return this.http.get(this.apiUrl + "organizationnsdcategory/get/?ordId=" + id)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getNsds = function () {
        //This is a fix? related with IE that cached the get request always. Everytime that a request is sent, 
        //we need to sent something different, like the parameter that is not used on the Controller
        return this.http.get(this.apiUrl + "nsdcategories/GetNsds/?timeRequest=" + new Date().getTime())
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getActiveNsds = function () {
        return this.http.get(this.apiUrl + "nsdcategories/activensds")
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.activateNsd = function (nsd) {
        return this.http.post(this.apiUrl + "nsdcategories/activatensd/", nsd)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getDisabledNsds = function () {
        return this.http.get(this.apiUrl + "nsdcategories/disablednsds")
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.removeFromAllOrganizations = function (nsd) {
        return this.http.post(this.apiUrl + "nsdcategories/removefromallorganizations/", nsd)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.disableNsd = function (nsd) {
        return this.http.post(this.apiUrl + "nsdcategories/disablensd/", nsd)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getActiveNsdsByOrganization = function (id) {
        return this.http.get(this.apiUrl + "nsdcategories/?orgId=" + id)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getDisabledNsdsByOrganizationId = function (id) {
        return this.http.get(this.apiUrl + "nsdcategories/disablednsds/?orgId=" + id)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.removeNsdFromOrganization = function (nsd) {
        return this.http.post(this.apiUrl + "organizationnsdcategory/removensdsinorganizationbynsdid/", nsd)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.addNsdToOrganization = function (nsd) {
        return this.http.post(this.apiUrl + "organizationnsdcategory/addnsdtoorganization/", nsd)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    //Config Values
    DataService.prototype.getRegisteredConfigValues = function (queryOptions) {
        return this.http.post(this.apiUrl + "configvalues/admin/page", queryOptions)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.deleteConfig = function (cat, name) {
        return this.http.delete(this.apiUrl + "configvalues/admin/delete/?cat=" + cat + "&name=" + name)
            .map(function (response) {
            return response;
        }).catch(this.handleError);
    };
    DataService.prototype.saveConfig = function (configRec) {
        return this.http.post(this.apiUrl + "configvalues/admin/save", configRec)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.existsConfigName = function (name) {
        return this.http.get(this.apiUrl + "configvalues/admin/exists/?name=" + name)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getConfigRecAdmin = function (cat, name) {
        return this.http.get(this.apiUrl + "configvalues/admin/find/?cat=" + cat + "&name=" + name)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    //Checklist
    DataService.prototype.getChecklistValue = function () {
        return this.http.get(this.apiUrl + "checklist/admin/findvalue")
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.saveChecklistConfig = function (configRec) {
        return this.http.post(this.apiUrl + "checklist/admin/save", configRec)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.pushChecklist = function () {
        return this.http.post(this.apiUrl + "checklist/push", null)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getRegisteredSeriesChecklistValues = function (queryOptions) {
        return this.http.post(this.apiUrl + "checklist/admin/page", queryOptions)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.deleteSeriesChecklist = function (serie) {
        return this.http.delete(this.apiUrl + "checklist/admin/delete/?serie=" + serie)
            .map(function (response) {
            return response;
        }).catch(this.handleError);
    };
    DataService.prototype.getSeriesChecklist = function (series) {
        return this.http.get(this.apiUrl + "checklist/admin/findseries/?series=" + series)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.verifySeriesChecklist = function (series) {
        return this.http.get(this.apiUrl + "checklist/admin/verifyseries/?series=" + series)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.validateFirsNames = function (names) {
        return this.http.get(this.apiUrl + "checklist/admin/validatefirs/?names=" + names)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.saveSeriesChecklist = function (seriesRec) {
        return this.http.post(this.apiUrl + "checklist/admin/saveseries", seriesRec)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    //GeoRef functions
    DataService.prototype.saveGeoFile = function (data) {
        return this.http.post(this.apiUrl + "nof/ndsclients/uploadfile", data)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    //getGeoFeatures(region: IRegionMapRender): Observable<string> {
    //    return this.http.post(`${this.apiUrl}nof/ndsclients/GetGeoFeatures/`, region)
    //        .map((response: Response) => {
    //            return response.json();
    //        }).catch(this.handleError);
    //}
    DataService.prototype.validateGeoLocation = function (loc) {
        return this.http.get(this.apiUrl + "nof/ndsclients/validatepoint/?value=" + loc)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.validateGeoPoints = function (points) {
        return this.http.get(this.apiUrl + "nof/ndsclients/validatepoints/?value=" + points)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    //End GeoRef functions
    //NDS Clients subscriptions
    DataService.prototype.getAerodromes = function () {
        return this.http.get(this.app.apiUrl + "sdosubjects/aerodromes").map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getNdsClient = function (id) {
        return this.http.get(this.apiUrl + "nof/ndsclients/" + id)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getNdsClients = function (queryOptions) {
        return this.http.post(this.apiUrl + "nof/ndsclients/page", queryOptions)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.getNdsClientAddresses = function () {
        return this.http.get(this.apiUrl + "nof/ndsclients")
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.validateRegionInCanada = function (data) {
        return this.http.post(this.apiUrl + "nof/validateregionincanada", data)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.saveNdsClient = function (client) {
        debugger;
        return this.http.post(this.apiUrl + "nof/ndsclients/", client)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.updateNdsClient = function (client) {
        return this.http.put(this.apiUrl + "nof/ndsclients/", client)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.deleteNdsClient = function (clientId) {
        return this.http.delete(this.apiUrl + "nof/ndsclients/" + clientId)
            .map(function (response) {
            return response;
        }).catch(this.handleError);
    };
    DataService.prototype.sendNotamsToSubscriptions = function (clientIds) {
        var viewmodel = { subsClientIds: clientIds };
        return this.http.post(this.apiUrl + "transition/queuejob/", viewmodel)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.existsClientName = function (id, name) {
        return this.http.get(this.apiUrl + "nof/ndsclients/exists/?Id=" + id + "&name=" + name)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    // AeroRDS
    DataService.prototype.getAeroRDSCacheStatus = function () {
        return this.http.get(this.apiUrl + "sdo/status")
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.pushAeroRDSGlobalSync = function () {
        return this.http.post(this.apiUrl + "sdo/globalsync", null)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.pushAeroRDSEffectiveCacheSync = function () {
        return this.http.post(this.apiUrl + "sdo/effectivecachesync", null)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    DataService.prototype.pushAeroRDSEffectiveCacheLoad = function () {
        return this.http.post(this.apiUrl + "sdo/effectivecacheload", null)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    //Map API
    DataService.prototype.getMapHealth = function () {
        return this.http.get(this.apiUrl + "map/getmaphealth").map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    //PRIVATE SESSION
    DataService.prototype.handleError = function (error) {
        if (error.status === 401) {
            window.location.reload();
        }
        return Rx_1.Observable.throw(error.json());
    };
    DataService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http, windowRef_service_1.WindowRef, xsrf_token_service_1.XsrfTokenService])
    ], DataService);
    return DataService;
}());
exports.DataService = DataService;
//# sourceMappingURL=data.service.js.map
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProposalModelResolver = void 0;
var core_1 = require("@angular/core");
var data_service_1 = require("./../shared/data.service");
var ProposalModelResolver = /** @class */ (function () {
    function ProposalModelResolver(dataService) {
        this.dataService = dataService;
    }
    ProposalModelResolver.prototype.resolve = function (route) {
        var id = +route.params['id'];
        return this.dataService.getProposalModel(id).map(function (model) { return model; });
    };
    ProposalModelResolver = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [data_service_1.DataService])
    ], ProposalModelResolver);
    return ProposalModelResolver;
}());
exports.ProposalModelResolver = ProposalModelResolver;
//# sourceMappingURL=proposal-model.resolver.js.map
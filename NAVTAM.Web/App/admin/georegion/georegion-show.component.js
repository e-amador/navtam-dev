"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GeoRegionShowComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var windowRef_service_1 = require("../../common/windowRef.service");
var toastr_service_1 = require("./../../common/toastr.service");
var angular_l10n_1 = require("angular-l10n");
var data_service_1 = require("../shared/data.service");
var GeoRegionShowComponent = /** @class */ (function () {
    function GeoRegionShowComponent(toastr, fb, dataService, router, winRef, activatedRoute, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.router = router;
        this.winRef = winRef;
        this.activatedRoute = activatedRoute;
        this.locale = locale;
        this.translation = translation;
        this.model = null;
        this.mapHealthy = false;
        if (this.winRef.appConfig.ccs || this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);
    }
    GeoRegionShowComponent.prototype.ngOnInit = function () {
        this.action = this.activatedRoute.snapshot.data[0].action;
        this.model = this.activatedRoute.snapshot.data['model'];
        this.geoRegion = this.model;
        this.configureReactiveForm();
    };
    GeoRegionShowComponent.prototype.ngAfterViewInit = function () {
        this.mapHealthCheck();
    };
    GeoRegionShowComponent.prototype.ngOnDestroy = function () {
        this.disposeMap();
    };
    GeoRegionShowComponent.prototype.configureReactiveForm = function () {
        this.geoShowForm = this.fb.group({
            geoName: [{ value: this.model.name, disabled: true }, []],
            geoRegionGeo: [{ value: this.model.region.geography, disabled: true }, []],
        });
    };
    GeoRegionShowComponent.prototype.backToGeoRegionList = function () {
        this.router.navigate(['/administration/georegion']);
    };
    GeoRegionShowComponent.prototype.$ctrl = function (name) {
        return this.geoShowForm.get(name);
    };
    //Map functions
    GeoRegionShowComponent.prototype.geoRegionRenderMap = function () {
        var reg = {
            region: this.geoRegion.region,
            changes: []
        };
        this.renderMap(reg);
    };
    GeoRegionShowComponent.prototype.initMap = function () {
        var winObj = this.winRef.nativeWindow;
        this.leafletmap = winObj['app'].leafletmap;
        if (this.leafletmap) {
            this.leafletmap.initMap();
        }
    };
    GeoRegionShowComponent.prototype.mapHealthCheck = function () {
        var _this = this;
        this.dataService.getMapHealth().subscribe(function (response) {
            if (response) {
                _this.initMap();
                _this.mapHealthy = true;
            }
        }, function (error) {
            _this.mapHealthy = false;
            return false;
        }, function () { return _this.renderRegionInMap(_this.model.region.geoFeatures, "Doa"); });
    };
    GeoRegionShowComponent.prototype.mapAvailable = function () {
        if (this.leafletmap) {
            return this.leafletmap.isReady();
        }
        return false;
    };
    GeoRegionShowComponent.prototype.renderMap = function (reg) {
        var _this = this;
        this.dataService.getGeoFeatures(reg)
            .subscribe(function (resp) {
            var geoFeatures = resp;
            _this.renderRegionInMap(geoFeatures, "DoaModified");
        }, function (error) {
            _this.toastr.error(_this.translation.translate('DoasAdminSession.RenderMapErrorMsg'), _this.translation.translate('DoasAdminSession.RenderMapErrorTitle'), { 'positionClass': 'toast-bottom-right' });
        });
    };
    GeoRegionShowComponent.prototype.renderRegionInMap = function (geoFeatures, name) {
        if (geoFeatures !== null) {
            this.leafletmap.addDoa(geoFeatures, name, function () {
                console.log("OK");
            });
        }
        else {
            this.toastr.error(this.translation.translate('DoasAdminSession.RenderMapErrorMsg'), this.translation.translate('DoasAdminSession.RenderMapErrorTitle'), { 'positionClass': 'toast-bottom-right' });
        }
    };
    GeoRegionShowComponent.prototype.disposeMap = function () {
        if (this.leafletmap) {
            this.leafletmap.dispose();
        }
    };
    GeoRegionShowComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/georegion/georegion-show.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], GeoRegionShowComponent);
    return GeoRegionShowComponent;
}());
exports.GeoRegionShowComponent = GeoRegionShowComponent;
//# sourceMappingURL=georegion-show.component.js.map
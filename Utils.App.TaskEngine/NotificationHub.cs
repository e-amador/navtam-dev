﻿namespace NavCanada.Utils.Apps.TaskEngine
{
    using Microsoft.AspNet.SignalR;
    using Microsoft.AspNet.SignalR.Hubs;

    [HubName("SignalrNotificationHub")]
    public class SignalrNotificationHub : Hub
    {        
        public void DataChanged()
        {
            Clients.All.refreshData();
        }
    }
}
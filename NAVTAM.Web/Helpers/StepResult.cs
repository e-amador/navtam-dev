﻿using System.Web.Http;

namespace NAVTAM.Helpers
{
    public class StepResult<T>
    {
        public static StepResult<T> Succeed(T result) => new StepResult<T>() { Result = result };
        public static StepResult<T> Fail(string error = "") => new StepResult<T>() { ErrorMessage = error };
        public T Result { get; private set; }
        public string ErrorMessage { get; private set; }
        public bool Succeeded => ErrorMessage == null;
        public bool Failed => ErrorMessage != null;
    }

    public class ApiStepResult
    {
        public static ApiStepResult Succeed(IHttpActionResult result) => new ApiStepResult() { Result = result };
        public static ApiStepResult Fail(IHttpActionResult error) => new ApiStepResult() { Error = error };
        public IHttpActionResult Result { get; private set; }
        public IHttpActionResult Error { get; private set; }
        public bool Succeeded => Error == null;
        public bool Failed => Error != null;
    }

    public class StepResult 
    {
        public static StepResult Succeed() => new StepResult() { };
        public static StepResult Fail(string errorMessage) => new StepResult() { ErrorMessage = errorMessage };
        public bool Succeeded => ErrorMessage == null;
        public bool Failed => ErrorMessage != null;
        public string ErrorMessage { get; private set; }
    }
}
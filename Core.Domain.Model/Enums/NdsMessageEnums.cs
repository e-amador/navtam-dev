﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum NdsMessageFormat : byte
    {
        Xml, RawIcao, Aftn
    }

    public enum NdsMessageStatus : byte
    {
        Pending, Sent, Confirmed, Failed
    }

    public enum NdsClientType : int
    {
        Aftn, Swim, Rest
    }
}

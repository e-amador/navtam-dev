﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    public class OrganizationDOAsViewModel
    {
        public long Id { get; set; }

        public long DOAId { get; set; }

        public long OrganizationId { get; set; }
        public string OrganizationName { get; set; }

        public bool Assigned { get; set; }
    }
}
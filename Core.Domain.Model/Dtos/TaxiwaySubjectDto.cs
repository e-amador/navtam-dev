﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NavCanada.Core.Domain.Model.Dtos
{
    public class TaxiwaySubjectDto
    {
        public string Id { get; set; }
        public string Mid { get; set; }
        public string ParentId { get; set; }
        public string SdoEntityName { get; set; }
        public string Designator { get; set; }
        public string Name { get; set; }
    }
}

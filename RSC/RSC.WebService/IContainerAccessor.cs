﻿using Microsoft.Practices.Unity;

namespace RSC.WebService
{
    public interface IContainerAccessor
    {
        IUnityContainer Container { get; }
    }
}

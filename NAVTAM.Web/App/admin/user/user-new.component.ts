﻿import { Component, Inject, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms'

import { LocaleService, TranslationService } from 'angular-l10n';

import { Select2OptionData } from 'ng2-select2';

import { MemoryStorageService } from '../shared/mem-storage.service'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';

//import { PasswordValidator } from '../shared/password.validator'
import { PhoneValidator } from '../shared/phone.validator'

import { DataService } from '../shared/data.service'
import { ProposalStatus } from '../shared/data.model';

import { IRole, IOrganization, IUser, IDoaPartial } from '../shared/data.model';

import { WindowRef } from '../../common/windowRef.service';

@Component({ 
    templateUrl: '/app/admin/user/user-new.component.html'
})
export class UserNewComponent implements OnInit  {
    
    model: any = null;
    action: string;

    userForm: FormGroup;

    isReadOnly: boolean;

    public roleOptions: Select2Options;
    public orgOptions: Select2Options;
    public doaOptions: Select2Options;

    public selectedRoleId?: string;
    public roles: Array<Select2OptionData>;

    public selectedOrgId?: number;
    public orgs: Array<Select2OptionData>;

    public selectedDoasIds: string[];
    public doas: Array<Select2OptionData>;

    public isSubmitting: boolean = false;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private winRef: WindowRef,
        public locale: LocaleService,
        public translation: TranslationService ) {
    }

    ngOnInit() {

        this.roleOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('UserSession.SelectRole')
            },
            width: "100%"
        }
        this.orgOptions = {
            width: "100%",  
            placeholder: {
                id: '-1',
                text: this.translation.translate('UserSession.SelectUserOrg')
            },
        }
        this.doaOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('UserSession.SelectMultiDoas')
            },
            multiple: true,
            width: "100%"
        }

        this.model = this.prepareEmptyUser();

        this.configureReactiveForm();

        this.loadRoles('-1');
        this.loadDoas('-1');
        this.loadOrganizations();
    }

    dissableOrganizationDropdown() {
        const appConfig = this.winRef.appConfig;
        return this.isReadOnly || (appConfig && !appConfig.sysAdmin);
    }

    phoneNumberFocus(ctrName: string) {
        const phoneCtrl = this.$ctrl(ctrName);
        const value = PhoneValidator.stripFormat(phoneCtrl.value);
        phoneCtrl.setValue(value);
    }

    phoneNumberBlur(ctrName: string) {
        const phoneCtrl = this.$ctrl(ctrName);
        const value = PhoneValidator.formatTenDigits(phoneCtrl.value);
        phoneCtrl.setValue(value);
    }

    onOrgChanged(data: { value: string }) {
        if (data.value) {
            this.userForm.get('org').setValue(data.value);
            this.loadRoles(data.value);
            this.loadDoas(data.value);
        } else {
            this.userForm.get('org').setValue(-1);
            this.loadRoles('-1');
            this.loadDoas('-1');
        }
        this.userForm.get('doas').setValue("");
        this.userForm.get('roles').setValue("-1");
    }

    onRoleChanged(data: { value: string[] }) {
        if (data.value && data.value.length > 0) {
            this.userForm.get('roles').setValue(data.value);
        } else {
            this.userForm.get('roles').setValue("-1");
        }
    }

    onDoaChanged(data: { value: string[] }) {
        if (data.value && data.value.length > 0) {
            this.userForm.get('doas').setValue(data.value.join(','));
        } else {
            this.userForm.get('doas').setValue("");
        }
    }

    onApproveChanged(checked: boolean) {
        const control = this.userForm.get('approved');
        control.setValue(checked);
        control.markAsDirty();
    }

    onDisableChanged(e: any) {
        const control = this.$ctrl('disabled');
        const val = control.value;
        control.setValue(!val);
        control.markAsDirty();
    }

    backToUserList() {
        this.router.navigate(['/administration']);
    }

    onSubmit() {
        if (this.isSubmitting) return;
        this.isSubmitting = true;
        const user = this.prepareSaveUser();
        this.dataService.saveUser(user)
            .subscribe(data => {
                let msg = this.translation.translate('UserSession.SuccessUserSaved')
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                this.router.navigate(["/administration"]);
            }, error => {
                this.isSubmitting = false;
                let msg = this.translation.translate('UserSession.FailureUserSaved')
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    validateUsername() {
        const usernameCtrl = this.userForm.get("username");
        return usernameCtrl.valid || this.userForm.pristine;
    }

    validateFirstName() {
        const firstNameCtrl = this.userForm.get("firstName");
        return firstNameCtrl.valid || this.userForm.pristine;
    }

    validateLastName() {
        const lastNameCtrl = this.userForm.get("lastName");
        return lastNameCtrl.valid || this.userForm.pristine;
    }

    //validatePassword() {
    //    const passwordCtrl = this.userForm.get('grpPassword.password');
    //    return passwordCtrl.valid || this.userForm.pristine;
    //}

    //validateConfirmPassword() {
    //    const confirmPasswordCtrl = this.userForm.get('grpPassword.confirmPassword');
    //    return confirmPasswordCtrl.valid || this.userForm.pristine;
    //}

    validateEmailAddress() {
        const emailCtrl = this.userForm.get("email");
        return emailCtrl.valid || this.userForm.pristine;
    }

    validateAddress() {
        const emailCtrl = this.userForm.get("address");
        return emailCtrl.valid || this.userForm.pristine;
    }

    validatePhoneNumber() {
        const emailCtrl = this.userForm.get("phoneNumber");
        return emailCtrl.valid || this.userForm.pristine;
    }

    validateFax() {
        const emailCtrl = this.userForm.get("fax");
        return emailCtrl.valid || this.userForm.pristine;
    }

    validateRoles() {
        const fc = this.userForm.get('roles');
        return (fc.value !== "" && fc.value !== "-1") || this.userForm.pristine;
    }

    validateOrganization() {
        const fc = this.userForm.get('org');
        return fc.value !== -1 || this.userForm.pristine;
    }

    validateDoas() {
        const fc = this.userForm.get('doas');
        return fc.value !== "" || this.userForm.pristine;
    }

    private loadOrganizations(): void {
        this.dataService.getOrganizations()
            .subscribe((org: IOrganization[]) => {
                this.orgs = org.map(
                    function (o: IOrganization) {
                        return <Select2OptionData>{
                            id: o.id,
                            text: o.name
                        };
                    });
                this.selectedOrgId = this.model.organizationId || -1;
                this.orgs.unshift({ id: "-1", text: "" });
            });
    }

    private loadRoles(orgId: string): void {
        if (orgId !== '-1') {
            this.dataService.getAvailableRolesByOrg(orgId)
                .subscribe((roles: IRole[]) => {
                    this.roles = roles.map(
                        function (r: IRole) {
                            return <Select2OptionData>{
                                id: r.roleId,
                                text: r.roleName
                            };
                        });
                    this.roles.unshift({ id: "-1", text: "" });
                });
        }
        this.selectedRoleId = '-1';
    }

    private loadDoas(orgId: string): void {
        if (orgId !== '-1') {
            this.dataService.getDoasInOrg(orgId)
                .subscribe((doas: IDoaPartial[]) => {
                    this.doas = doas.map(
                        function (d: IDoaPartial) {
                            return <Select2OptionData>{
                                id: d.id.toString(),
                                text: d.name
                            };
                        });
                    this.selectedDoasIds = this.getDefaultDoas(doas);
                    this.doas.unshift({ id: "-1", text: "" });
                });
        } else {
            this.selectedDoasIds = [];
        }
    }

    private configureReactiveForm() {
        this.userForm = this.fb.group({
            username: [{ value: this.model.username, disabled: false }, [Validators.required, Validators.minLength(3), Validators.maxLength(16), Validators.pattern('[a-zA-Z0-9\-_]+$')]],
            firstName: [{ value: this.model.firstName, disabled: false }, [Validators.required, Validators.pattern('[a-zA-Z0-9\-_. ]+$')]],
            lastName: [{ value: this.model.lastName, disabled: false }, [Validators.required, Validators.pattern('[a-zA-Z0-9\-_. ]+$')]],
            //grpPassword: this.fb.group({
            //    password: [{ value: "", disabled: false }, [Validators.required, Validators.pattern("((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%]).{8,20})")]],
            //    confirmPassword: [{ value: "", disabled: false }, [Validators.required]],
            //}, {validator: PasswordValidator.MatchPassword }),
            position: { value: this.model.position, disabled: false },
            email: [{ value: this.model.email, disabled: false }, [Validators.required, Validators.pattern("[a-zA-Z0-9._+-]{1,}@[a-zA-Z0-9._-]{2,}[.]{1}[a-zA-Z]{2,3}")]],
            roles: [{ value: this.model.roleIds, disabled: false }, [roleValidator]],
            org: [{ value: this.model.organizationId || -1, disabled: false }, [orgValidator]],
            address: [{ value: this.model.address, disabled: false }, [Validators.required]],
            doas: [{ value: this.model.doaIds, disabled: false }, [doaValidator]],
            //approved: { value: this.model.approved, disabled: false },
            disabled: { value: this.model.deleted, disabled: false },
            phoneNumber: [{ value: this.model.phoneNumber, disabled: false }, [Validators.required, phoneNumberValidator]],
            fax: [{ value: this.model.fax, disabled: false }, [phoneNumberValidator]]
        });
    }

    private $ctrl(name: string): AbstractControl {
        return this.userForm.get(name);
    }

    private prepareSaveUser(): IUser {
        const user : IUser = {
            username: this.userForm.get('username').value,
            firstName: this.userForm.get('firstName').value,
            lastName: this.userForm.get('lastName').value,
            //password: this.userForm.get('grpPassword.password').value,
            //confirmPassword: this.userForm.get('grpPassword.confirmPassword').value,
            position: this.userForm.get('position').value,
            email: this.userForm.get('email').value,
            roleId: this.userForm.get('roles').value,
            organizationId: this.userForm.get('org').value,
            address: this.userForm.get('address').value,
            approved: true,
            disabled: this.userForm.get('disabled').value,
            phoneNumber: this.userForm.get('phoneNumber').value,
            fax: this.userForm.get('fax').value,
            doaIds: this.userForm.get('doas').value,
            deleted: false,
        }

        return user;
    }


    private prepareEmptyUser(): IUser {
        const user: IUser = {
            username: "",
            firstName: "",
            lastName: "",
            position: "",
            email: "",
            roleId: "",
            organizationId: this.getOrganizationId(),
            //password: "",
            //confirmPassword:"",
            address: "",
            approved: true,
            disabled: false,
            phoneNumber: "",
            fax: "",
            doaIds: "",
            deleted: false,
        }

        return user;
    }

    private getOrganizationId() {
        const appConfig = this.winRef.appConfig;
        return appConfig && !appConfig.sysAdmin ? appConfig.orgId : null;
    }

    private getDefaultDoas(doas: IDoaPartial[]): string[] {
        const appConfig = this.winRef.appConfig;
        if ((appConfig && !appConfig.sysAdmin) || (doas.length === 1)) {
            return this.doas.map(d => d.id.toString());
        } 
        return [];
    }
}

function roleValidator(c: AbstractControl): { [key: string]: boolean } | null {
    if (!c.value) {
        return { 'required': true }
    } else if (c.value === '-1') {
        return { 'required': true }
    }
    return null;
}

function doaValidator(c: AbstractControl): { [key: string]: boolean } | null {
    if (!c.value) {
        return { 'required': true }
    }
    return null;
}

function orgValidator(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value === -1) {
        return { 'required': true }
    }
    return null;
}

function phoneNumberValidator(c: AbstractControl): { [key: string]: boolean } | null {
    const value = c.value;
    if (value && !PhoneValidator.isValidPhoneNumber(c.value, 10)) {
        return { 'pattern': true }
    }
    return null;
}
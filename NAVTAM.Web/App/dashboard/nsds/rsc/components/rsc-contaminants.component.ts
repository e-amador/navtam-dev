﻿import { Component, Input, OnChanges, Inject, OnDestroy } from '@angular/core';
import { TranslationService } from 'angular-l10n';
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms';
import { Select2OptionData, Select2Component } from 'ng2-select2';
import { IClearedSurfaceThird, IClearedSurface, IContaminantComponentModel, IContaminantIns, IContamDepth, IRwyFriction } from '../rsc.model';
import { IRwyCCInput, SelectContaminantOptionData } from '../rsc.model';
import { RscComponent } from '../rsc.component';
import { RscService } from '../rsc.service';

import {
} from '../../../shared/data.model';

import * as moment from 'moment';
import { TOASTR_TOKEN, Toastr } from '../../../../common/toastr.service';

//declare var $: any;

@Component({
    selector: 'rsc-contaminants',
    templateUrl: '/app/dashboard/nsds/rsc/components/rsc-contaminants.component.html'
})

export class RscContaminantsComponent implements OnChanges, OnDestroy {
    @Input('form') mainForm: FormGroup = null;
    @Input('isReadOnly') isReadOnly: boolean = false;
    @Input('isAverage') isAverage: boolean = false;
    @Input('model') model: IContaminantComponentModel = null;

    //parent: RscComponent = null;

    frmGroup: FormGroup;

    isFormLoadingData: boolean;
    isCalculatingCC: boolean = false;

    isFirstContaminantDepthEnabled: boolean = false;
    isSecondContaminantDepthEnabled: boolean = false;
    isFirstContaminantCoverageEnabled: boolean = false;
    isSecondContaminantCoverageEnabled: boolean = false;
    isFinalRwyCCDisable: boolean = false;

    rwyContaminants: SelectContaminantOptionData[];
    depths: Select2OptionData[];
    coefficients: Select2OptionData[]; 
    coverages: Select2OptionData[];
    rwyCC: Select2OptionData[];
    rwyCCAction: Select2OptionData[];

    contaminantOptions = {
        width: "100%",
        placeholder: {
            id: '-1',
            text: this.translation.translate('ContaminantsComponent.SelectContaminant'),
            contaminatId: '-1',
            needDepth: false,
            temperature: 0,
        }
    }

    depthOptions = {
        width: "100%",
        placeholder: {
            id: '-1',
            text: this.translation.translate('ContaminantsComponent.SelectDepth')
        }
    }

    coefficientsOptions = {
        width: "100%",
        placeholder: {
            id: '-1',
            text: this.translation.translate('ContaminantsComponent.SelectCoefficient')
        }
    }

    coverageOptions = {
        width: "100%",
        placeholder: {
            id: '-1',
            text: this.translation.translate('ContaminantsComponent.SelectCoverage')
        }
    }

    rwyCCOptions = {
        width: "100%",
        placeholder: {
            id: '-1',
            text: this.translation.translate('ContaminantsComponent.SelectRwyCC')
        }
    }

    rwyCCActionOptions = {
        width: "100%",
        placeholder: {
            id: '-1',
            text: this.translation.translate('ContaminantsComponent.SelectRwyCCAction')
        }
    }

    intervalHandler = null;

    constructor(
        private fb: FormBuilder,
        public translation: TranslationService,
        private rscService: RscService,
        @Inject(TOASTR_TOKEN) private toastr: Toastr) {
    }

    ngOnInit() {
        this.isFormLoadingData = false;

        this.rwyContaminants = this.model.parent.contaminants;
        this.depths = this.model.parent.depths;
        this.coefficients = this.model.parent.coefficients;
        this.coverages = this.model.parent.coverages;
        this.rwyCC = this.model.parent.conditionCodes;
        this.rwyCCAction = this.model.parent.conditionCodeAction;

        if (!this.isAverage) {
            this.rwyContaminants = this.rwyContaminants.filter(c => c.id !== '29' );
        }
        this.configureReactiveForm();

        this.setIntervalCalculateCC();
    }

    ngOnChanges() {
    }

    ngOnDestroy() {
        if (this.intervalHandler) {
            window.clearInterval(this.intervalHandler);
        }
    }

    private setIntervalCalculateCC() {
        const self = this;
        this.intervalHandler = window.setInterval(function () {
            if (self.isFormLoadingData) return;
            if (self.isCalculateCCDisabled() === false) {
                self.calculateRwyCC();
            } else {
                self.clearCalculatedCC();
            }
        }, 3000);
    }

    private clearCalculatedCC() {
        if (this.model.data.rwyCCCalculated !== '') {
            this.model.data.rwyCCCalculated = '';
            let ctrl = this.frmGroup.get('rwyCCCalculated');
            if (ctrl.value !== this.model.data.rwyCCCalculated) {
                ctrl.setValue(this.model.data.rwyCCCalculated);
                ctrl.markAsDirty();
            }
        }
    }

    isCalculateCCDisabled(): boolean {
        var cont = this.rwyContaminants.find(x => x.id === this.model.data.firstContaminant.id);
        if (!cont) {
            this.clearCalculatedCC();
            return true;
        }
        if (+this.model.data.firstContaminant.coverage < 10) {
            this.clearCalculatedCC();
            return true;
        }
        if (cont.needDepth) {
            if (+this.model.data.firstContaminant.depth < 0) {
                this.clearCalculatedCC();
                return true;
            }
        }
        cont = this.rwyContaminants.find(x => x.id === this.model.data.secondContaminant.id);
        if (cont) {
            if (+this.model.data.secondContaminant.coverage < 10) {
                this.clearCalculatedCC();
                return true;
            }
            if (cont.needDepth) {
                if (+this.model.data.secondContaminant.depth < 0) {
                    this.clearCalculatedCC();
                    return true;
                }
            }
        }
        return false;
    }

    public calculateRwyCC() {
        if (this.isCalculatingCC) return;
        this.isCalculatingCC = true;

        let data = this.exportClearedSurfaceThirdData();
        this.rscService.calculateRwyCC(data).subscribe((res: IClearedSurfaceThird) => {
                this.model.data.rwyCCCalculated = res.nesCalculatedRwyCC;
                this.isCalculatingCC = false;
                let ctrl = this.frmGroup.get('rwyCCCalculated');
                if (ctrl.value !== this.model.data.rwyCCCalculated) {
                    ctrl.setValue(this.model.data.rwyCCCalculated);
                    ctrl.markAsDirty();
                }
            }, error => {
                this.isCalculatingCC = false;
                let msg = this.translation.translate('ContaminantsComponent.ErrorCalculatingRWYCC');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });

    }

    public calculateRwyCondCode() {
        if (this.isCalculatingCC) return;
        this.isCalculatingCC = true;

        let data = this.exportRwyCCInputData();
        this.rscService.calculateRwyCondCode(data).subscribe((res: string) => {
            this.model.data.rwyCCCalculated = res;
            this.isCalculatingCC = false;
            let ctrl = this.frmGroup.get('rwyCCCalculated');
            if (ctrl.value !== this.model.data.rwyCCCalculated) {
                ctrl.setValue(this.model.data.rwyCCCalculated);
                ctrl.markAsDirty();
            }
        }, error => {
            this.isCalculatingCC = false;
            let msg = this.translation.translate('ContaminantsComponent.ErrorCalculatingRWYCC');
            this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });

    }

    isCalendarActive() {
        return true;
    }

    onContaminantChanged(groupName: string, e: any): void {
        if (e.value) {
            let contaminantId = e.value;

            const control = groupName === 'first'
                ? this.frmGroup.get('grpFirstContaminant.contaminantId')
                : this.frmGroup.get('grpSecondContaminant.contaminantId');

            if (this.isFormLoadingData) {
                contaminantId = control.value;
            } else {
                if (contaminantId !== control.value) {
                    if (contaminantId === '00') {
                        contaminantId = '-1';
                        if (groupName === 'first') {
                            this.resetFirstContaminant();
                            this.isFirstContaminantCoverageEnabled = false;
                            this.isFirstContaminantDepthEnabled = false;
                        }
                        else {
                            this.resetSecondContaminant();
                            this.isSecondContaminantCoverageEnabled = false;
                            this.isSecondContaminantDepthEnabled = false;
                        }
                    } else {
                        control.patchValue(contaminantId);
                        control.markAsDirty();

                        let contaminant = this.rwyContaminants.find(x => x.id === contaminantId);

                        this.model.data.temperature = contaminant.temperature;

                        if (groupName === 'first') {
                            this.model.data.firstContaminant.contaminantId = contaminant.contaminatId;
                            this.model.data.firstContaminant.id = contaminant.id;
                            this.isFirstContaminantCoverageEnabled = true;
                            if (contaminant.needDepth) {
                                this.isFirstContaminantDepthEnabled = true;
                            }
                            else {
                                this.isFirstContaminantDepthEnabled = false;
                                const depthCtrl = this.frmGroup.get('grpFirstContaminant.depth')
                                depthCtrl.setValue('-1');
                            }
                        }
                        else {
                            this.model.data.secondContaminant.contaminantId = contaminant.contaminatId;;
                            this.model.data.secondContaminant.id = contaminant.id;
                            this.isSecondContaminantCoverageEnabled = true;
                            if (contaminant.needDepth) {
                                this.isSecondContaminantDepthEnabled = true;
                            } else {
                                this.isSecondContaminantDepthEnabled = false;
                                const depthCtrl = this.frmGroup.get('grpSecondContaminant.depth');
                                depthCtrl.setValue('-1');
                            }

                        }
                    }
                }
            }
        }
    }

    onDepthChanged(groupName: string, e: any): void {
        if (e.value) {
            let depthId = e.value;
            if (groupName === 'first') {
                const control = this.frmGroup.get('grpFirstContaminant.depth')
                if (this.isFormLoadingData) {
                    depthId = control.value;
                } else {
                    if (depthId !== control.value) {
                        control.setValue(depthId);
                        control.markAsDirty();
                        this.model.data.firstContaminant.depth = depthId;
                    }
                }
            } else {
                const control = this.frmGroup.get('grpSecondContaminant.depth');
                if (this.isFormLoadingData) {
                    depthId = control.value;
                } else {
                    if (depthId !== control.value) {
                        control.setValue(depthId);
                        control.markAsDirty();
                        this.model.data.secondContaminant.depth = depthId;
                    }
                }
            }
        }
    }

    onCoverageChanged(groupName: string, e: any): void {
        if (e.value) {
            let coverageId = e.value;

            const control = groupName === 'first'
                ? this.frmGroup.get('grpFirstContaminant.coverage')
                : this.frmGroup.get('grpSecondContaminant.coverage');

            if (this.isFormLoadingData) {
                coverageId = control.value;
            } else {
                if (coverageId !== control.value) {
                    control.setValue(coverageId);
                    control.markAsDirty();
                    if (groupName === 'first') this.model.data.firstContaminant.coverage = coverageId;
                    else this.model.data.secondContaminant.coverage = coverageId;
                }
            }
        }
    }

    onCoefficientChanged(e: any): void {
        if (e.value) {
            let coefficientId = e.value;
            const control = this.frmGroup.get('grpCrfi.coefficient')
            if (this.isFormLoadingData) {
                coefficientId = control.value;
            } else {
                if (coefficientId !== control.value) {
                    control.setValue(coefficientId);
                    control.markAsDirty();
                    this.model.data.coefficient = coefficientId;
                }
            }
        }
    }

    onRwyCCChange(e: any) {
        if (e.value) {
            let rwyCCId = e.value;

            const control = this.frmGroup.get('rwyCC');
            if (this.isFormLoadingData) {
                rwyCCId = control.value;
            } else {
                if (rwyCCId !== control.value) {
                    control.setValue(rwyCCId);
                    control.markAsDirty();
                    this.model.data.rwyCC = rwyCCId;
                }
            }
        }
    }

    onRwyCCActionChange(e: any) {
        if (e.value) {
            let rwyCCActionId = e.value;

            const control = this.frmGroup.get('rwyCCAction');
            if (this.isFormLoadingData) {
                rwyCCActionId = control.value;
            } else {
                if (rwyCCActionId !== control.value) {
                    control.setValue(rwyCCActionId);
                    control.markAsDirty();
                    this.model.data.rwyCCActionSelected = rwyCCActionId;
                    this.isFinalRwyCCDisable = (this.model.data.rwyCCActionSelected === '0');
                    this.resetFinalRwyCCComboValues(rwyCCActionId);
                }
            }
        }
    }

    private resetFinalRwyCCComboValues(value: string) {
        this.rwyCC = [
            { id: '0', text: '0' },
            { id: '1', text: '1' },
            { id: '2', text: '2' },
            { id: '3', text: '3' },
            { id: '4', text: '4' },
            { id: '5', text: '5' },
            { id: '6', text: '6' }
        ];
        if (value === '0') {
            this.rwyCC = this.rwyCC.filter(c => +c.id === +this.model.data.rwyCCCalculated);
            this.model.data.rwyCC = this.model.data.rwyCCCalculated;
            this.onRwyCCChange({ value: this.model.data.rwyCC });
        } else if (value === '1') {
            this.rwyCC = this.rwyCC.filter(c => +c.id < +this.model.data.rwyCCCalculated);
            this.model.data.rwyCC = '-1';
            this.onRwyCCChange({ value: this.model.data.rwyCC });
        } else if (value === '2') {
            this.rwyCC = this.rwyCC.filter(c => +c.id > +this.model.data.rwyCCCalculated);
            this.model.data.rwyCC = '-1';
            this.onRwyCCChange({ value: this.model.data.rwyCC });
        }

    }

    validateContaminantId(groupName: string) {
        const fc = groupName === 'first'
            ? this.frmGroup.get('grpFirstContaminant.contaminantId')
            : this.frmGroup.get('grpSecondContaminant.contaminantId');

        return fc.value !== '-1' || this.frmGroup.pristine;
    }

    validateDepth(groupName: string) {
        const fc = groupName === 'first'
            ? this.frmGroup.get('grpFirstContaminant.depth')
            : this.frmGroup.get('grpSecondContaminant.depth');

        const contaminant = groupName === 'first'
            ? this.frmGroup.get('grpFirstContaminant.contaminantId')
            : this.frmGroup.get('grpSecondContaminant.contaminantId');

        if (contaminant.value !== '-1') {
            let needDepth = this.rwyContaminants.find(x => x.id === contaminant.value);
            if (needDepth && needDepth.needDepth) {
                return fc.value !== '-1' || this.frmGroup.pristine;
            }
        }
        return true;
    }

    validateCoverage(groupName: string) {
        return true;
    }

    validateCoefficient() {
        const fc = this.frmGroup.get('grpCrfi.coefficient');
        return !fc.errors || this.frmGroup.pristine;
    }

    public validateKeyPressNumber(evt: any) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!this.isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
        }
    }

    private resetFirstContaminant() {
        this.model.data.firstContaminant.contaminantId = '-1';
        this.model.data.firstContaminant.id = '-1';
        this.model.data.firstContaminant.depth = '-1';
        this.model.data.firstContaminant.coverage = '-1';

        var ctrl = this.$ctrl('grpFirstContaminant.contaminantId');
        ctrl.patchValue(this.model.data.firstContaminant.contaminantId);
        ctrl = this.$ctrl('grpFirstContaminant.depth');
        ctrl.patchValue(this.model.data.firstContaminant.depth);
        ctrl = this.$ctrl('grpFirstContaminant.coverage');
        ctrl.patchValue(this.model.data.firstContaminant.coverage);

        this.isFirstContaminantCoverageEnabled = false;
        this.isFirstContaminantDepthEnabled = false;

    }

    private resetSecondContaminant() {
        this.model.data.secondContaminant.contaminantId = '-1';
        this.model.data.secondContaminant.id = '-1';
        this.model.data.secondContaminant.depth = '-1';
        this.model.data.secondContaminant.coverage = '-1';

        var ctrl = this.$ctrl('grpSecondContaminant.contaminantId');
        ctrl.patchValue(this.model.data.secondContaminant.contaminantId);
        ctrl = this.$ctrl('grpSecondContaminant.depth');
        ctrl.patchValue(this.model.data.secondContaminant.depth);
        ctrl = this.$ctrl('grpSecondContaminant.coverage');
        ctrl.patchValue(this.model.data.secondContaminant.coverage);

        this.isSecondContaminantCoverageEnabled = false;
        this.isSecondContaminantDepthEnabled = false;

    }

    $ctrl(name: string): AbstractControl {
        return this.frmGroup.get(name);
    }

    private configureReactiveForm() {
        const frmArrControls = <FormArray>this.mainForm.controls['frmContaminantArr'];

        this.frmGroup = this.fb.group({
            grpFirstContaminant: this.fb.group({
                contaminantId: [this.model.data.firstContaminant.contaminantId || '-1', [validContaminantValidator]],
                depth: this.model.data.firstContaminant.depth,
                coverage: this.model.data.firstContaminant.coverage
            }),
            grpSecondContaminant: this.fb.group({
                contaminantId: this.model.data.secondContaminant.contaminantId || '-1',
                depth: this.model.data.secondContaminant.depth,
                coverage: this.model.data.secondContaminant.coverage
            }),
            rwyCC: { value: this.model.data.rwyCC, disabled: this.isReadOnly },
            rwyCCCalculated: { value: this.model.data.rwyCCCalculated, disabled: true },
            rwyCCAction: { value: this.model.data.rwyCCActionSelected, disabled: true },
            grpCrfi: this.fb.group({
                coefficient: this.model.data.coefficient,
                temperature: this.model.data.temperature,
            }),
        });

        frmArrControls.push(this.frmGroup);
    }


    private isANumber(str: string): boolean {
        return !/\D/.test(str);
    }

    //Export Functions
    public isFormValid(): boolean {

        return true;
    }

    public refreshValues(newValues: any): void {
        if (!newValues) return;

        this.model.data.rwyCCCalculated = newValues.data.rwyCCCalculated;

        this.onContaminantChanged('first', { value: newValues.data.firstContaminant.contaminantId });
        this.onDepthChanged('first', { value: newValues.data.firstContaminant.depth });
        this.onCoverageChanged('first', { value: newValues.data.firstContaminant.coverage });
        this.onContaminantChanged('second', { value: newValues.data.secondContaminant.contaminantId });
        this.onDepthChanged('second', { value: newValues.data.secondContaminant.depth });
        this.onCoverageChanged('second', { value: newValues.data.secondContaminant.coverage });
        this.onCoefficientChanged({ value: newValues.data.coefficient });

        this.onRwyCCActionChange({ value: newValues.data.rwyCCActionSelected });

        this.onRwyCCChange({ value: newValues.data.rwyCC});
    }

    public processClean(): void {
        var oldId = this.model.id;
        this.model = this.model.parent.createContaminantModel(oldId);
        this.model.data.firstContaminant.contaminantId = "00";
        this.model.data.secondContaminant.contaminantId = "00";
        this.model.data.firstContaminant.id = "00";
        this.model.data.secondContaminant.id = "00";
        this.model.data.rwyCCCalculated = '';

        this.onContaminantChanged('first', { value: this.model.data.firstContaminant.id });
        this.onDepthChanged('first', { value: this.model.data.firstContaminant.depth });
        this.onCoverageChanged('first', { value: this.model.data.firstContaminant.coverage });
        this.onContaminantChanged('second', { value: this.model.data.secondContaminant.id });
        this.onDepthChanged('second', { value: this.model.data.secondContaminant.depth });
        this.onCoverageChanged('second', { value: this.model.data.secondContaminant.coverage });
        this.onCoefficientChanged({ value: this.model.data.coefficient });

        this.onRwyCCActionChange({ value: this.model.data.rwyCCActionSelected });

        this.onRwyCCChange({ value: this.model.data.rwyCC });

        this.clearCalculatedCC();
    }

    public exportComponentData(): any {
        let data = {
            id: this.model.id,
            clearedSurfaceThird: this.exportClearedSurfaceThirdData(),
            rwyCCCalculated: this.model.data.rwyCCCalculated,
            rwyCCActionSelected: this.model.data.rwyCCActionSelected,
            rwyCC: this.model.data.rwyCC,
        };

        return data;
    }

    public exportComponentInternalData(): IContaminantComponentModel {
        let data: IContaminantComponentModel = {
            parent: this.model.parent,
            id: this.model.id,
            data: {
                firstContaminant: {
                    contaminantId: this.model.data.firstContaminant.contaminantId,
                    id: this.model.data.firstContaminant.id,
                    depth: this.model.data.firstContaminant.depth,
                    coverage: this.model.data.firstContaminant.coverage,
                },
                secondContaminant: {
                    contaminantId: this.model.data.secondContaminant.contaminantId,
                    id: this.model.data.firstContaminant.id,
                    depth: this.model.data.secondContaminant.depth,
                    coverage: this.model.data.secondContaminant.coverage,
                },
                rwyCC: this.model.data.rwyCC,
                rwyCCCalculated: this.model.data.rwyCCCalculated,
                rwyCCActionSelected: this.model.data.rwyCCActionSelected,
                coefficient: this.model.data.coefficient,
                temperature: this.model.data.temperature,
            }
        };
        return data;
    }

    public exportClearedSurfaceData(): IClearedSurface {
        if (!this.isAverage) return null;

        let contams: IContaminantIns[] = [];
        var cont = this.rwyContaminants.find(x => x.id === this.model.data.firstContaminant.id);
        var depth: IContamDepth = {
            contamInches: (cont.needDepth) ? +this.model.data.firstContaminant.depth : 0,
            contamTrace: false,
        };
        let fCont: IContaminantIns = {
            hasDepdth: cont.needDepth,
            contamCoverage: +this.model.data.firstContaminant.coverage,
            contamDepthI: depth,
            contamType: (cont.needDepth) ? +cont.contaminatId : null,
            contamTypeNoD: (!cont.needDepth) ? +cont.contaminatId : null,
        };
        contams.push(fCont);

        cont = this.rwyContaminants.find(x => x.id === this.model.data.secondContaminant.id);
        if (cont) {
            depth = {
                contamInches: (cont.needDepth) ? +this.model.data.secondContaminant.depth : 0,
                contamTrace: false,
            };
            let sCont: IContaminantIns = {
                hasDepdth: cont.needDepth,
                contamCoverage: +this.model.data.secondContaminant.coverage,
                contamDepthI: depth,
                contamType: (cont.needDepth) ? +cont.contaminatId : null,
                contamTypeNoD: (!cont.needDepth) ? +cont.contaminatId : null,
            };
            contams.push(sCont);
        }

        let data: IClearedSurface = {
            contaminants: contams,
            rwyCC: this.model.data.rwyCC,
        };
        return data;
    }

    public exportClearedSurfaceThirdData(): IClearedSurfaceThird {
        if (this.isAverage) return null;

        let contams: IContaminantIns[] = [];
        var cont = this.rwyContaminants.find(x => x.id === this.model.data.firstContaminant.id);
        var depth: IContamDepth = {
            contamInches: (cont.needDepth) ? +this.model.data.firstContaminant.depth : 0,
            contamTrace: false,
        };
        let fCont: IContaminantIns = {
            hasDepdth: cont.needDepth,
            contamCoverage: +this.model.data.firstContaminant.coverage,
            contamDepthI: depth,
            contamType: (cont.needDepth) ? +cont.contaminatId : null,
            contamTypeNoD: (!cont.needDepth) ? +cont.contaminatId : null,
        };
        contams.push(fCont);

        cont = this.rwyContaminants.find(x => x.id === this.model.data.secondContaminant.id);
        if (cont) {
            depth = {
                contamInches: (cont.needDepth) ? +this.model.data.secondContaminant.depth : 0,
                contamTrace: false,
            };
            let sCont: IContaminantIns = {
                hasDepdth: cont.needDepth,
                contamCoverage: +this.model.data.secondContaminant.coverage,
                contamDepthI: depth,
                contamType: (cont.needDepth) ? +cont.contaminatId : null,
                contamTypeNoD: (!cont.needDepth) ? +cont.contaminatId : null,
            };
            contams.push(sCont);
        }

        //Prepare RwyFriction data
        let rwyFriction: IRwyFriction = {
            rfObsTime: null,
            device: 130, //Default
            temperature: this.model.data.temperature.toString(),
            unreliable: false,
            frictionCoefficients: +this.model.data.coefficient
        };

        let portion: number = 0;
        if (this.model.id.trim().toUpperCase() === 'TOUCHDOWN') {
            //Touchdown
            portion = 1;
        } else if (this.model.id.trim().toUpperCase() === 'MIDPOINT') {
            //Midpoint
            portion = 2;
        } else if (this.model.id.trim().toUpperCase() === 'ROLLOUT') {
            //Rollout
            portion = 3;
        }

        //Prepare ClearedSurfaceThird data
        let data: IClearedSurfaceThird = {
            portion: portion,
            rwyCC: this.model.data.rwyCC,
            nesCalculatedRwyCC: this.model.data.rwyCCCalculated,
            frictionCoefficient: +this.model.data.coefficient,
            rwyFrictionThird: rwyFriction,
            contaminants: contams
        };

        return data;
    }

    public exportRwyCCInputData(): IRwyCCInput {
        let data: IRwyCCInput = {
            firstContaminant: {
                contaminantId: this.model.data.firstContaminant.contaminantId,
                id: this.model.data.firstContaminant.id,
                depth: this.model.data.firstContaminant.depth,
                coverage: this.model.data.firstContaminant.coverage,
            },
            secondContaminant: {
                contaminantId: this.model.data.secondContaminant.contaminantId,
                id: this.model.data.secondContaminant.id,
                depth: this.model.data.secondContaminant.depth,
                coverage: this.model.data.secondContaminant.coverage,
            },
            temperature: 0,
        };

        return data;
    }

}

function validContaminantValidator(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value === '-1') {
        return { 'required': true }
    }
    return null;
}


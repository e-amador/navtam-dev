﻿namespace RSC.Model
{
    /// <summary>
    /// Accommodates freeform text in both official languages.
    /// </summary>
    public class FreeformText
    {
        /// <summary>
        /// English text should always be present if the parent data element is specified.
        /// </summary>
        public string EnglishText { get; set; }
        /// <summary>
        /// French text is optional except for certain aerodromes.
        /// </summary>
        public string FrenchText { get; set; }
    }
}

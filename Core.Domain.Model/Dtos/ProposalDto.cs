﻿using NavCanada.Core.Domain.Model.Enums;
using System;

namespace NavCanada.Core.Domain.Model.Dtos
{
    public class ProposalDto
    {
        public int Id { get; set; }
        public string NotamId { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string ItemA { get; set; }
        public string ItemE { get; set; }
        public string SiteId { get; set; }

        public string IcaoSubjectDesc { get; set; }

        public string IcaoConditionDesc { get; set; }
        public string Version { get; set; }
        public string StartActivity { get; set; }
        public string EndValidity { get; set; }
        public bool Estimated { get; set; }
        public DateTime Received { get; set; }
        public string ModifiedByUsr { get; set; }
        public string Originator { get; set; }
        public string Operator { get; set; }
        public NotamProposalStatusCode Status { get; set; }
        public NotamType? ProposalType { get; set; }
        public string GroupId { get; set; }
        public bool SeriesChecklist { get; set; }
        public bool IsObsolete { get; set; }
        public bool SoonToExpire { get; set; }

    }
}
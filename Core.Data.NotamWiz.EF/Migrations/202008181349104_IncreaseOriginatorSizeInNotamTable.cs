namespace NavCanada.Core.Data.NotamWiz.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IncreaseOriginatorSizeInNotamTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Notam", "Originator", c => c.String(maxLength: 250));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Notam", "Originator", c => c.String(maxLength: 50));
        }
    }
}

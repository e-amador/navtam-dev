﻿using System.Threading.Tasks;

namespace NAVTAM.Helpers
{
    public interface IApplicationContext
    {
        string GetCookie(string name, string defaultValue = "");
        void SetCookie(string name, string value, int ttl = 30);
        Task<bool> IsUserInRole(string userId, string role);
        string GetUserId();
        string GetUserName();
        string GetConfigValue(string name, string defaultValue);
        void SetResponseStatusCode(int code);
        bool IsValidUserRequest(string userIdCookie);
    }

    public static class IApplicationContextExt
    {
        public static Task<bool> IsUserInRole(this IApplicationContext ctx, string role) => ctx.IsUserInRole(ctx.GetUserId(), role);
    }
}

﻿using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.NSDs
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Spatial;
    using System.Device.Location;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.Script.Serialization;

    using AutoMapper;

    using Microsoft.AspNet.Identity;

    using Code.Extentions;
    using Code.GeoTools;
    using Code.Helpers;
    using Controllers;
    using Models;
    using Core.Common.Contracts;
    using Core.Data.NotamWiz.Contracts;
    using Core.Domain.Model.Entitities;
    using Core.Domain.Model.Enums;
    using Core.SqlServiceBroker;

    [Authorize(Roles = "User")]
    public abstract class NsdBaseController : BaseController
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(NsdBaseController));

        protected NsdBaseController(IUow uow, IClusterQueues clusterQueues) : base(uow, clusterQueues)
        {
        }

        public NotamSubjectViewModel Subject { get; set; }

        [HttpPost]
        public async Task<ActionResult> CancelProposalDirect(long packageId)
        {
            _log.Object(new
            {
                Method = "CancelProposalDirect",
                Parameters = new
                {
                    packageId
                }
            });
            var package = await Uow.NotamPackageRepo.GetFullPackageByIdAsync(packageId);
            var subject = Uow.SdoSubjectRepo.GetById(package.SdoSubjectId);
            Subject = Mapper.Map<NotamSubjectViewModel>(Mapper.Map<NotamSubjectViewModel>(subject, opt =>
            {
                opt.Items["db"] = Uow;
            }));

            if (!package.Proposals.Any())
            {
                return ProcessResult.GetErrorResult("NSDBC-0009", DbRes.T("Invalid NOTAM Package", "NSD-Common"));
            }
            if (package.Proposals.Last().Status != NotamProposalStatusCode.Submitted &&
                package.Proposals.Last().Status != NotamProposalStatusCode.ModifiedAndSubmitted)
            {
                return ProcessResult.GetErrorResult("NSDBC-0010", DbRes.T("Invalid NOTAM Status", "NSD-Common"));
            }
            var proposalId =
                package.Proposals.Last(
                    a =>
                        !a.EntityDelete &&
                        (a.Status == NotamProposalStatusCode.Submitted ||
                         a.Status == NotamProposalStatusCode.ModifiedAndSubmitted ||
                         a.Status == NotamProposalStatusCode.Enforced ||
                         a.Status == NotamProposalStatusCode.ModifiedEnforced)).Id;
            var cancelProposal = Mapper.Map<NotamProposalViewModel>(Uow.NotamProposalRepo.GetById(proposalId));
            var org = await GetUserOrganizationAsync();
            var userProfile = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            cancelProposal.Status = NotamProposalStatusCode.QueuePending;
            cancelProposal.Id = 0;
            cancelProposal.Type = NotamType.C.ToString();
            cancelProposal.StartValidity = DateTime.UtcNow;
            cancelProposal.EndValidity = null;
            cancelProposal.OriginatorName = string.Format("{0} {1}", userProfile.FirstName, userProfile.LastName);
            cancelProposal.Operator = User.Identity.GetUserName();
            cancelProposal.OnBehalfOfOrganizationId = org.Id;
            var v = GetViewModelFromTokens(cancelProposal.Tokens);
            cancelProposal.StatusTime = DateTime.UtcNow;
            var result = await GenerateAsync("C", cancelProposal, v);
            if (result.Item1.Success)
            {
                var proposal = Mapper.Map<NotamProposalViewModel, NotamProposal>(result.Item2);
                var mto = CreateMtoForNotamChangeStatus(MessageTransferObjectId.SubmitNotamProposal, proposal.Id);

                Uow.BeginTransaction();
                try
                {
                    package.Proposals.Add(proposal);
                    await Uow.SaveChangesAsync();

                    //Publish Mto to target Queeue
                    PublishMtoToTargetQueue(mto, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings);
                    Uow.Commit();
                }
                catch (Exception)
                {
                    Uow.Rollback();
                }
                return ProcessResult.GetSuccessResult();
            }
            return result.Item1.Result;
        }

        public virtual async Task<NotamProposalViewModel> CreateProposalFromBaseAsync(long proposalId,
            NotamType propsalType)
        {
            var proposal = await Uow.NotamProposalRepo.GetFullProposalAsync(proposalId);
            var newProposal = Mapper.Map<NotamProposal, NotamProposalViewModel>(proposal);

            //var vm = GetViewModelFromTokens(newProposal.Tokens);
            //if (propsalType == NotamType.R)
            //{
            //    vm.StartValidity = DateTime.UtcNow;
            //}

            if (propsalType == NotamType.R)
                newProposal.Type = "R";

            return await ProcessNewProposalAsync(newProposal);
        }

        [HttpPost]
        [Obsolete("use GenerateNotamModel()")]
        public async Task<ActionResult> GenerateNotam(string proposalType, long packageId, string subjectId,
            string tokens)
        {
            _log.Object(new
            {
                Method = "GenerateNotam",
                Parameters = new
                {
                    proposalType,
                    packageId,
                    subjectId,
                    tokens
                }
            });
            Subject = Mapper.Map<NotamSubjectViewModel>(Uow.SdoSubjectRepo.GetById(subjectId), opt =>
            {
                opt.Items["db"] = Uow;
            });
            var doGenerateResult = await DoGenerate(proposalType, packageId, tokens);
            var result = doGenerateResult.Item1;
            var proposal = doGenerateResult.Item2;
            if (!result.Success)
            {
                return Json(result.Result);
            }
            return Json(proposal);
        }

        [HttpPost]
        public async Task<ActionResult> GenerateNotamModel(string proposalType, long packageId, string subjectId,
            string tokens)
        {
            _log.Object(new
            {
                Method = "GenerateNotamModel",
                Parameters = new
                {
                    proposalType,
                    packageId,
                    subjectId,
                    tokens
                }
            });
            Subject = Mapper.Map<NotamSubjectViewModel>(Uow.SdoSubjectRepo.GetById(subjectId), opt =>
            {
                opt.Items["db"] = Uow;
            });

            if (Subject == null)
            {
                return ProcessResult.GetErrorResult("NSDBC-0011", DbRes.T("Subject not provided or Invalid Subject.", "NSD-Common"));
            }

            var doGenerateResult = await DoGenerate(proposalType, packageId, tokens);
            var result = doGenerateResult.Item1;
            var proposal = doGenerateResult.Item2;
            if (!result.Success)
            {
                return Json(result.Result);
            }

            var model = Mapper.Map<NotamProposalViewModel, NotamViewModel>(proposal);
            return Json(model);
        }

        public async Task<string> GetNotamProposalSeries(string subjectCode, string qCode, double latitude, double longitude)
        {
            _log.Object(new
            {
                Method = "GetNotamProposalSeries",
                Parameters = new
                {
                    subjectCode,
                    vqCode = qCode,
                    latitude,
                    longitude
                }
            });
            var series = await GetNotamProposalSeriesBySubjectCode(subjectCode, qCode);
            if (series == "X")
            {
                series = await GetNotamProposalSeriesByLocation(latitude, longitude, qCode);
            }
            return series;
        }

        public async Task<string> GetNotamProposalSeriesByLocation(double latitude, double longitude, string qCode)
        {
            _log.Object(new
            {
                Method = "GetNotamProposalSeriesByLocation",
                Parameters = new
                {
                    latitude,
                    longitude,
                    vqCode = qCode
                }
            });

            //var region = Uow.GeoRegionRepo.GetAll().ToList().FirstOrDefault(r => r.Geo.MakeValid().Intersects(location));
            var location = DbGeography.PointFromText("POINT(" + longitude + " " + latitude + ")", GeoDataService.SRID);
            var region =
                await Uow.GeoRegionRepo.GetByGeoLocationAsync(location);

            if (region == null)
            {
                _log.Warn(new
                {
                    Method = "GetNotamProposalSeriesByLocation",
                    Parameters = new
                    {
                        latitude,
                        longitude,
                        vqCode = qCode
                    },
                    Warning = "Region Not found for location"
                });
                return "X";
            }
            var sa = await
                 Uow.SeriesAllocationRepo.GetByRegionIdAndCodeAndCategoryIdAsync(region.Id, qCode, DisseminationCategory.International);

            if (!sa.Any())
            {
                _log.Warn(new
                {
                    Method = "GetNotamProposalSeriesByLocation",
                    Parameters = new
                    {
                        latitude,
                        longitude,
                        vqCode = qCode
                    },
                    Warning = "Series Allocation not found",
                    region
                });
                return "X";
            }
            if (sa.Count() > 1)
            {
                _log.Warn(new
                {
                    Method = "GetNotamProposalSeriesByLocation",
                    Parameters = new
                    {
                        latitude,
                        longitude,
                        vqCode = qCode
                    },
                    Warning = "Multiple Series Allocation records found",
                    region,
                    sa
                });
                return "X";
            }
            return sa.First().Series;
        }

        public async Task<string> GetNotamProposalSeriesBySubjectCode(string adCode, string qCode)
        {
            _log.Object(new
            {
                Method = "GetNotamProposalSeries",
                Parameters = new
                {
                    adCode,
                    vqCode = qCode
                }
            });
            var adc = await Uow.AerodromeDisseminationCategoryRepo.GetByAhpCodeIdAsync(adCode);
            if (adc == null)
            {
                _log.Warn(new
                {
                    Method = "GetNotamProposalSeries",
                    Parameters = new
                    {
                        adCode,
                        vqCode = qCode
                    },
                    Warning = "AD not found in AerodromeDisseminationCategory table"
                });
                return "X";
            }
            if (adc.Location == null)
            {
                _log.Warn(new
                {
                    Method = "GetNotamProposalSeries",
                    Parameters = new
                    {
                        adCode,
                        vqCode = qCode
                    },
                    Warning = "AD Location is NULL"
                });
                return "X";
            }
            //var region =
            //    Uow.GeoRegionRepo.GetAll().ToList().FirstOrDefault(r => r.Geo.MakeValid().Intersects(adc.Location));
            var region =
                await Uow.GeoRegionRepo.GetByGeoLocationAsync(adc.Location);

            if (region == null)
            {
                _log.Warn(new
                {
                    Method = "GetNotamProposalSeries",
                    Parameters = new
                    {
                        adCode,
                        vqCode = qCode
                    },
                    Warning = "Region Not found for AD"
                });
                return "X";
            }
            var sa = await 
                Uow.SeriesAllocationRepo.GetByRegionIdAndCodeAndCategoryIdAsync(region.Id, qCode,
                    adc.DisseminationCategory);

            if (!sa.Any())
            {
                _log.Warn(new
                {
                    Method = "GetNotamProposalSeries",
                    Parameters = new
                    {
                        adCode,
                        vqCode = qCode
                    },
                    Warning = "Series Allocation not found",
                    adc,
                    region
                });
                return "X";
            }
            if (sa.Count() > 1)
            {
                _log.Warn(new
                {
                    Method = "GetNotamProposalSeries",
                    Parameters = new
                    {
                        adCode,
                        vqCode = qCode
                    },
                    Warning = "Multiple Series Allocation records found",
                    adc,
                    region,
                    sa
                });
                return "X";
            }
            return sa.First().Series;
        }

        [HttpPost]
        public async Task<ActionResult> GetNotamSeries(string subjectCode, string qCode)
        {
            var series = await GetNotamProposalSeriesBySubjectCode(subjectCode, qCode);
            return ProcessResult.GetSuccessResult(new
            {
                series
            });
        }

        [HttpGet]
        public virtual ActionResult GetScript()
        {
            _log.Object(new
            {
                Method = "GetScript",
                Parameters = new
                {
                }
            });
            var file = Server.MapPath(GetScriptLocation());
            return Content(System.IO.File.ReadAllText(file), "text/html");
        }

        public abstract ActionResult Index(string id);

        public ActionResult SubjectSelector()
        {
            return View();
        }

        public bool IsInBilingualRegion()
        {
            return Subject != null && GeoDataService.IsInBilingualRegion(Subject.SubjectGeoRefLat, Subject.SubjectGeoRefLong, Uow);
        }

        [HttpPost]
        public async Task<ActionResult> IsWithinDoa(string location)
        {
            var result = GeoDataService.IsLocationWithinDOA(new GeoCoordinate().FromLocation(location),
                await GetUserDoaAsync());
            return ProcessResult.GetSuccessResult(new
            {
                WithinDOA = result
            });
        }

        [HttpPost]
        public async Task<ActionResult> SubmitNotamProposal(string proposalType, long packageId, string subjectId,
            string tokens)
        {
            _log.Object(new
            {
                Method = "SubmitNotamProposal",
                Parameters = new
                {
                    proposalType,
                    packageId,
                    tokens,
                    subjectId
                }
            });

            Subject = Mapper.Map<NotamSubjectViewModel>(Uow.SdoSubjectRepo.GetById(subjectId), opt =>
            {
                opt.Items["db"] = Uow;
            });


            if(Subject == null)
            {
                return ProcessResult.GetErrorResult("NSDBC-0011" , DbRes.T("Subject not provided or Invalid Subject.", "NSD-Common"));
            }


            var doGenerateResult = await DoGenerate(proposalType, packageId, tokens);
            var result = doGenerateResult.Item1;
            var proposal = doGenerateResult.Item2;
            
            if (!result.Success)
                return Json(result.Result);
            
            long proposalId = 0;

            Uow.BeginTransaction();
            try
            {
                var submitResult = await DoSubmit(packageId, proposal, subjectId, proposalId);
                result = submitResult.Item1;
                packageId = submitResult.Item2;
                proposalId = submitResult.Item3;
                if (result.Success)
                {
                    //Publish Mto to target Queeue
                    var mto = CreateMtoForNotamChangeStatus(MessageTransferObjectId.SubmitNotamProposal, proposalId);
                    PublishMtoToTargetQueue(mto, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings);
                    Uow.Commit();

                    return
                        ProcessResult.GetSuccessResult(
                            DbRes.T("Submitting NOTAM Proposal operation completed successfully.", "NSD-Common"), new
                            {
                                PackageId = packageId,
                                ProposalId = proposalId
                            });
                }
            }
            catch (Exception)
            {
                Uow.Rollback();
            }
            
            return Json(result.Result);
        }

        public async Task<ProcessResult> ValidateSubject(NotamSubjectViewModel subject)
        {
            this._log.Object(new
            {
                Method = "ValidateSubject",
                Parameters = new
                {
                    subject
                }
            });
            var doa = await GetUserDoaAsync();
            if (
                !GeoDataService.IsLocationWithinDOA(
                    new GeoCoordinate(subject.SubjectGeoRefLat, subject.SubjectGeoRefLong), doa))
            {
                return new ProcessResult("NSDBC-0004",
                    DbRes.T("Subject location is outside your Domain of Authority", "NSD-Common"));
            }
            return new ProcessResult();
        }

        protected async Task<Tuple<ProcessResult, NotamProposalViewModel>> DoGenerate(string proposalType,
            long packageId, string tokens)
        {
            _log.Object(new
            {
                Method = "DoGenerate",
                Parameters = new
                {
                    proposalType,
                    packageId,
                    tokens
                }
            });
            var v = GetViewModelFromTokens(tokens);
            NotamProposalViewModel notamProposal = null;
            if (v == null)
            {
                _log.Warn(new
                {
                    Method = "SubmitNotamProposal",
                    Parameters = new
                    {
                        proposalType,
                        packageId,
                        tokens
                    },
                    Warning = "Could not convert NOTAM tokens to View Model"
                });
                return
                    new Tuple<ProcessResult, NotamProposalViewModel>(
                        new ProcessResult("NSDBC-0005", DbRes.T("Invalid NOTAM Proposal tokens.", "NSD-Common")), null);
            }

            var baseModelValidationResult = await ValidateBaseModel(v);
            if (!baseModelValidationResult.Success)
            {
                return new Tuple<ProcessResult, NotamProposalViewModel>(baseModelValidationResult, null);
            }

            var result = await ValidateAsync(v);
            if (!result.Success)
            {
                return new Tuple<ProcessResult, NotamProposalViewModel>(result, null);
            }
            PrepareNOTAMProposal(ref v);
           
            var package = await Uow.NotamPackageRepo.GetFullPackageByIdAsync(packageId);
            if (package == null && (proposalType == "R" || proposalType == "C"))
            {
                return new Tuple<ProcessResult, NotamProposalViewModel>(new ProcessResult("NSDBC-0006", DbRes.T("Invalid NOTAM Package.", "NSD-Common")), null);
            }
            long proposalId = 0;
            if (package != null)
            {
                var lastProposal = package.Proposals.OrderBy(p => p.Id).LastOrDefault(p => !p.EntityDelete);
                if (lastProposal == null)
                {
                    return new Tuple<ProcessResult, NotamProposalViewModel>(new ProcessResult("NSDBC-0007", DbRes.T("Invalid NOTAM Package.", "NSD-Common")), null);
                }
                switch (lastProposal.Status)
                {
                    case NotamProposalStatusCode.Draft:
                    case NotamProposalStatusCode.Withdrawn:
                    case NotamProposalStatusCode.Rejected:
                        proposalId = lastProposal.Id; // proposal to edit
                        break;
                    default:
                        proposalId = 0; // new proposal to create
                        break;
                }
                notamProposal = Mapper.Map<NotamProposalViewModel>(lastProposal);
            }
            else
            {
                notamProposal = new NotamProposalViewModel();
            }
            notamProposal.Id = proposalId;
            notamProposal.StartValidity = v.StartValidity;
            notamProposal.EndValidity = v.EndValidity;
            notamProposal.Estimated = v.Estimated;
            notamProposal.Permanent = v.Permanent;
            notamProposal.SetBeginDateAtSubmit = v.SetBeginDateAtSubmit;
            notamProposal.ItemD = proposalType != "C" ? v.ItemD : "";
            notamProposal.Tokens = tokens;
            notamProposal.NoteToNof = v.NoteToNof;
            notamProposal.OriginatorName = v.OriginatorName;
            // TODO ASK WHY WE NEED A DEFAULT NUMBER FOR NEW PROPOSAL
            notamProposal.Nof = "CYHQ";
            if (proposalType == "R" || proposalType == "C")
            {
                notamProposal.ReferredNumber = notamProposal.SubmittedNotam.Number;
                notamProposal.ReferredSeries = notamProposal.SubmittedNotam.Series;
                notamProposal.ReferredYear = notamProposal.SubmittedNotam.Year;
            }
            else
            {
                // TODO ASK WHY WE NEED A DEFAULT REFERRED NUMBER FOR NEW PROPOSAL
                notamProposal.ReferredNumber = 1212;
            }
            
            var generateResult = await GenerateAsync(proposalType, notamProposal, v);

            // req 373694
            var updatedItemE_ItemD = GetUpdatedItemE_ItemD(generateResult.Item2.ItemE, generateResult.Item2.ItemD);
            generateResult.Item2.ItemE = updatedItemE_ItemD.Item1;
            generateResult.Item2.ItemD = updatedItemE_ItemD.Item2;

            return new Tuple<ProcessResult, NotamProposalViewModel>(generateResult.Item1, generateResult.Item2);
        }

        private async Task<ProcessResult> ValidateBaseModel(NotamProposalViewModelBase v)
        {
            // validate base model
            var validator = new NotamProposalBaseValidator(this, DbRes);
            var validationResult = await validator.ValidateAsync(v);
            if (!validationResult.IsValid)
            {
                var result = new ProcessResult
                {
                    Success = false,
                    ErrorCode = "AOAC-001"
                };
                foreach (var failure in validationResult.Errors)
                {
                    result.Errors.Add(failure.ErrorMessage);
                }

                return result;
            }

            return new ProcessResult();
        }


        private void PrepareNOTAMProposal(ref NotamProposalViewModelBase v)
        {
            // Requirments #373452   https://jazznc.deveng.local:9443/rm/web#action=com.ibm.rdm.web.pages.showArtifact&artifactURI=https%3A%2F%2Fjazznc.deveng.local%3A9443%2Frm%2Fresources%2F_YmEPoMyyEeO6FYe9fps2Gw
            if (v.Estimated && !v.Permanent && v.EndValidity != null && v.StartValidity != null && v.EndValidity.Value > v.StartValidity.Value.AddHours(2208))
            {
                v.EndValidity = v.StartValidity.Value.AddHours(2208);
            }
            
        }

        protected async Task<Tuple<ProcessResult, long, long>> DoSubmit(long packageId, NotamProposalViewModel notamProposalViewModel, string subjectId, long proposalID)
        {
            _log.Object(new
            {
                Method = "DoSubmit",
                Parameters = new
                {
                    packageId,
                    NotamProposal = notamProposalViewModel,
                    subjectId
                }
            });
            proposalID = notamProposalViewModel.Id;
            try
            {
                var notamProposal = await Uow.NotamProposalRepo.GetByIdAsync(notamProposalViewModel.Id) ?? new NotamProposal();
                var currentUser = await Uow.UserProfileRepo.GetByIdAsync(User.Identity.GetUserId(), false);
                var nsdLocation = GetType().Name.Replace("Controller", "");
                var nsd = await Uow.NsdRepo.GetWithLastVersionByFilesLocationAsync(nsdLocation);
                var nsdInfo = Mapper.Map<NsdInfoViewModel>(nsd);
                var subject = Uow.SdoSubjectRepo.GetById(subjectId);
                var org = await GetUserOrganizationAsync();
                notamProposalViewModel.Status = NotamProposalStatusCode.QueuePending;
                notamProposalViewModel.StatusTime = DateTime.UtcNow;                
                notamProposalViewModel.Operator = currentUser.UserName;
                notamProposalViewModel.OnBehalfOfOrganizationId = org.Id;
                if (notamProposalViewModel.SetBeginDateAtSubmit)
                {
                    notamProposalViewModel.StartValidity = DateTime.UtcNow;
                }
                Mapper.Map(notamProposalViewModel, notamProposal);
                notamProposal.ModifiedByNof = false; // reset if copied form previous proposal

                notamProposal.NsdVersion = nsdInfo.Version;

                //Workaroud because Aimsl will fail submition if ItemE french is provided and the subject is not in a 'Bilingual Region'
                if (!IsInBilingualRegion())
                    notamProposal.ItemEFrench = null;

                if (packageId == 0)
                {
                    var package = new NotamPackage
                    {
                        Name = nsd.Name,
                        Description = nsd.Description,
                        NsdId = nsdInfo.Id,
                        SdoSubjectId = subjectId,                        
                        SubjectCategory = DashboardItemType.SdoSubject,
                        SubjectType = subject.SdoEntityName,
                        Proposals = new List<NotamProposal>
                        {
                            notamProposal
                        }
                    };
                    
                    Uow.NotamPackageRepo.Add(package);
                    await Uow.SaveChangesAsync();
                    
                    packageId = package.Id;
                    proposalID = package.Proposals.OrderByDescending(a => a.Id).First().Id;
                }
                else
                {
                    if (notamProposalViewModel.Id > 0)
                    {
                        Uow.NotamProposalRepo.Update(notamProposal);
                        await Uow.SaveChangesAsync();
                    }
                    else
                    {
                        var pkId = packageId;
                        var package = await Uow.NotamPackageRepo.GetFullPackageByIdAsync(pkId);
                        package.Proposals.Add(notamProposal);
                        await Uow.SaveChangesAsync();

                        proposalID = package.Proposals.OrderByDescending(a => a.Id).First().Id;
                    }
                }
                return new Tuple<ProcessResult, long, long>(new ProcessResult(), packageId, proposalID);
            }
            catch (Exception se)
            {
                var errorUid = _log.ErrorWithUID(se, "Error submitting NOTAM Proposal.");
                return new Tuple<ProcessResult, long, long>(new ProcessResult("NSDBC-0008", string.Format(DbRes.T("Error submitting NOTAM Proposal.\n Unique Error Code: {0}", "NSD-Common"), errorUid)), packageId, proposalID);
            }
        }

        protected abstract Task<Tuple<ProcessResult, NotamProposalViewModel>> GenerateAsync(string proposalType, NotamProposalViewModel notamProposal, NotamProposalViewModelBase notamProposalBase);

        protected string GetItemA(string adCode)
        {
            if (adCode.Any(char.IsDigit))
            {
                return "CXXX";
            }
            return adCode;
        }

        protected Tuple<string, string> GetUpdatedItemE_ItemD(string itemE, string itemD)
        {
            if (!string.IsNullOrEmpty(itemD) && itemD.Length >= 200)
            {
                itemE = itemE + "\n" + itemD;
                itemD = null;
            }
            return Tuple.Create(itemE, itemD);
        }  

        protected string GetItemEPrefix(NotamSubjectViewModel subject)
        {
            if (subject.Ad.Any(char.IsDigit))
            {
                return subject.Ad + " " + subject.AdName + "\n";
            }
            return "";
        }

        protected string GetItemEPrefix(string ident)
        {
            if (ident.Any(char.IsDigit))
            {
                var subject = Uow.SdoSubjectRepo.GetAll().Single(a => a.Designator == ident);
                return subject.Designator + " " + subject.Name + "\n";
            }
            return "";
        }

        protected async Task<Tuple<double, SdoSubject>> GetNearestAd(DbGeography obstacleGeo)
        {
            double distance = 5;
            while (true)
            {
                var d = GeoDataService.NMToMeters(distance);
                var ads = await Uow.SdoSubjectRepo.GetNearestByDistanceAsync("Ahp", obstacleGeo, d);
                var highestCategoryAd = "";
                var maxAdc = 0;
                var minDistance = double.MaxValue;
                var nearestAd = ads.FirstOrDefault();
                foreach (var ad in ads)
                {
                    if (distance > 5)
                    {
                        if (ad.Distance < minDistance)
                        {
                            minDistance = ad.Distance;
                            nearestAd = ad;
                        }
                    }
                    else
                    {
                        if ((ad.AerodromeDisseminationCategory != null && 
                            (int)ad.AerodromeDisseminationCategory.DisseminationCategory > maxAdc) || 
                            (ad.AerodromeDisseminationCategory != null && (int)ad.AerodromeDisseminationCategory.DisseminationCategory == maxAdc && ad.Distance < minDistance))
                        {
                            minDistance = ad.Distance;
                            highestCategoryAd = ad.CodeId;
                            maxAdc = (int)ad.AerodromeDisseminationCategory.DisseminationCategory;
                            nearestAd = ad;
                        }
                        else if (ad.AerodromeDisseminationCategory == null && highestCategoryAd == "" && ad.Distance < minDistance)
                        {
                            minDistance = ad.Distance;
                            nearestAd = ad;
                        }
                    }
                }
                if (nearestAd != null)
                {
                    return new Tuple<double, SdoSubject>(GeoDataService.MetersToNM(minDistance), nearestAd.SdoSubject);
                }
                distance += 5;
            }
        }

        protected string GetNotamSeriesByGeoRegion(DbGeography geo, string qCode)
        {
            // calculate Series
            try
            {
                //TODO: (Esteban- Move this query to a repo and improve performance)
                var tmp = (from s in Uow.SeriesAllocationRepo.GetAll()
                                        join r in Uow.GeoRegionRepo.GetAll() on s.RegionId equals r.Id
                                        where s.QCode == qCode && s.DisseminationCategory == DisseminationCategory.International
                                        select new { series = s.Series, geo = r.Geo }).ToList();

                var seriesAllocation = tmp.FirstOrDefault(a => a.geo.MakeValid().Intersects(geo.MakeValid()));

                return seriesAllocation == null ? "X" : seriesAllocation.series;
            }catch(Exception e)
            {
                _log.Error(e);
                throw;
            }
        }

        protected string GetUpdatedItemA(string itemA)
        {
            if (itemA.Any(char.IsDigit))
            {
                return "CXXX";
            }
            return itemA;
        }

        protected abstract Task<NotamProposalViewModel> ProcessNewProposalAsync(NotamProposalViewModel notamProposal);

        protected abstract Task<ProcessResult> ValidateAsync(NotamProposalViewModelBase notamProposalBase);

        private IEnumerable<Type> FindDerivedTypes(Assembly assembly, Type baseType)
        {
            return assembly.GetTypes().Where(t => t != baseType && baseType.IsAssignableFrom(t));
        }

        private string GetScriptLocation()
        {
            _log.Object(new
            {
                Method = "GetScriptLocation",
                Parameters = new
                {
                }
            });
            var parts = GetType().Namespace.Split('.');
            var count = parts.Count();
            return "/Nsds/" + parts[count - 2] + "/" + parts[count - 1] + "/script.js";
        }

        private NotamProposalViewModelBase GetViewModelFromTokens(string tokens)
        {
            var viewModelType = FindDerivedTypes(Assembly.GetExecutingAssembly(), typeof(NotamProposalViewModelBase)).Where(t => t.Namespace == GetType().Namespace).First();
            object v;
            try
            {
                var jss = new JavaScriptSerializer();
                v = jss.Deserialize(tokens, viewModelType);
            }
            catch (Exception)
            {
                return null;
            }
            return v as NotamProposalViewModelBase;
        }
    }
}
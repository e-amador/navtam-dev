﻿import { Injectable } from '@angular/core';

function _window(): any {
    // return the global native browser window object
    return window;
}

@Injectable()
export class WindowRef {
    get nativeWindow(): any {
        return _window();
    }
    get appConfig(): any {
        return window['app'];
    }

    get authData(): any {
        return window['app'].authData;
    }

    saveAuthData(authData: any): void {
        window['app'].authData = authData;
    }

    zeroPad(num, places) {
        let zero = places - num.toString().length + 1;
        return Array(+(zero > 0 && zero)).join("0") + num;
    }
}
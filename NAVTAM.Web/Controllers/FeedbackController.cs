﻿using System.Web.Mvc;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NAVTAM.ViewModels;
using NavCanada.Core.Domain.Model.Entitities;
using System;
using System.Threading.Tasks;
using NavCanada.Core.Common.Contracts;

namespace NAVTAM.Controllers
{
    [Authorize]
    public class FeedbackController : BaseController
    {
        public FeedbackController(IUow uow, ILogger logger) : base(uow, logger)
        {
        }

        // GET: Feedback
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Post(FeedbackViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("Index", model);
            }

            try
            {
                model.InternalError = false;
                Uow.FeedbackRepo.Add(new Feedback
                {
                    Firstname = model.Firstname,
                    Lastname = model.Lastname,
                    EmailAddress = model.EmailAddress,
                    PhoneNumber = model.PhoneNumber,
                    Department = model.Department,
                    Organization = model.Organization,
                    Comment = model.Comment
                });
                
                await Uow.SaveChangesAsync();
            }
            catch(Exception)
            {
                model.InternalError = true;
                return View("Index", model);
            }

            return RedirectToAction("Index", "Home");
        }
    }
}
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IcaoSubjectsListComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var toastr_service_1 = require("./../../common/toastr.service");
var router_1 = require("@angular/router");
var data_service_1 = require("../shared/data.service");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var windowRef_service_1 = require("../../common/windowRef.service");
var angular_l10n_1 = require("angular-l10n");
var IcaoSubjectsListComponent = /** @class */ (function () {
    function IcaoSubjectsListComponent(toastr, fb, dataService, memStorageService, router, winRef, route, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.route = route;
        this.locale = locale;
        this.translation = translation;
        this.selectedIcaoSubject = null;
        this.selectedIcaoCondition = null;
        this.paging = {
            currentPage: 1,
            pageSize: 0,
            totalCount: 0,
            totalPages: 0,
        };
        this.totalPages = 0;
        this.loadingData = false;
        this.loadingConditions = false;
        this.leftMenuHeight = null;
        if (this.winRef.appConfig.ccs)
            this.router.navigate(['/administration']);
        this.tableSubjectHeaders = {
            sorting: [
                { columnName: 'EntiTyCode', direction: 0 },
                { columnName: 'Name', direction: 0 },
                { columnName: 'NameFrench', direction: 0 },
                { columnName: 'Code', direction: 0 },
                { columnName: 'Scope', direction: 0 },
                { columnName: 'AllowItemAChange', direction: 0 },
                { columnName: 'AllowScopeChange', direction: 0 },
                { columnName: 'AllowSeriesChange', direction: 0 }
            ],
            itemsPerPage: [
                { id: '8', text: '8' },
                { id: '20', text: '20' },
                { id: '50', text: '50' },
                { id: '100', text: '100' },
                { id: '200', text: '200' },
            ]
        };
        this.tableConditionHeaders = {
            sorting: [
                { columnName: 'Description', direction: 0 },
                { columnName: 'DescriptionFrench', direction: 0 },
                { columnName: 'Code', direction: 0 },
                { columnName: 'Lower', direction: 0 },
                { columnName: 'Upper', direction: 0 },
                { columnName: 'Radius', direction: 0 },
                { columnName: 'Code', direction: 0 },
                { columnName: 'Traffic', direction: 0 },
                { columnName: 'Purpose', direction: 0 },
                { columnName: 'AllowFgEntry', direction: 0 },
                { columnName: 'AllowPurposeChange', direction: 0 },
                { columnName: 'CancelNotamOnly', direction: 0 },
                { columnName: 'Active', direction: 0 },
            ],
            itemsPerPage: []
        };
        this.paging.pageSize = +this.tableSubjectHeaders.itemsPerPage[0].id;
        this.queryOptions = {
            entity: "icaosubjects",
            page: 1,
            pageSize: this.paging.pageSize,
            sort: "_" + this.tableSubjectHeaders.sorting[0].columnName,
            filterValue: ""
        };
        this.conditionQueryOptions = {
            entity: "icaoconditions",
            page: 1,
            pageSize: 20000,
            sort: "_Id",
            filterValue: ""
        };
    }
    ;
    IcaoSubjectsListComponent.prototype.ngOnInit = function () {
        //Set latest query options
        this.updateLeftMenuHeight();
        var queryOptions = this.memStorageService.get(this.memStorageService.ITEM_QUERY_OPTIONS_KEY);
        if (queryOptions && queryOptions.entity === "icaosubjects") {
            this.queryOptions = queryOptions;
        }
        this.configureReactiveForm();
        this.getIcaoSubjectsInPage(this.queryOptions.page);
    };
    IcaoSubjectsListComponent.prototype.ngOnDestroy = function () {
        this.updateLeftMenuHeight();
    };
    Object.defineProperty(IcaoSubjectsListComponent.prototype, "langCulture", {
        get: function () {
            return (window["app"].cultureInfo || "en").toUpperCase();
        },
        enumerable: false,
        configurable: true
    });
    IcaoSubjectsListComponent.prototype.configureReactiveForm = function () {
        var _this = this;
        this.icaoSubjectListForm = this.fb.group({
            searchSubject: [{ value: this.queryOptions.filterValue, disabled: false }, []],
        });
        this.$ctrl("searchSubject").valueChanges.debounceTime(1000).subscribe(function () { return _this.updateSubjectList(); });
    };
    IcaoSubjectsListComponent.prototype.updateSubjectList = function () {
        if (this.loadingData)
            return;
        var subject = this.$ctrl('searchSubject');
        this.queryOptions.filterValue = subject.value;
        this.getIcaoSubjectsInPage(this.queryOptions.page);
    };
    IcaoSubjectsListComponent.prototype.$ctrl = function (name) {
        return this.icaoSubjectListForm.get(name);
    };
    IcaoSubjectsListComponent.prototype.selectIcaoSubject = function (icaoSubject) {
        if (this.selectedIcaoSubject) {
            this.selectedIcaoSubject.isSelected = false;
        }
        this.selectedIcaoSubject = icaoSubject;
        this.selectedIcaoSubject.isSelected = true;
        this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, this.selectedIcaoSubject.id);
        this.getIcaoConditions(this.selectedIcaoSubject.id);
    };
    IcaoSubjectsListComponent.prototype.selectIcaoCondition = function (icaoCondition) {
        if (this.selectedIcaoCondition) {
            this.selectedIcaoCondition.isSelected = false;
        }
        this.selectedIcaoCondition = icaoCondition;
        this.selectedIcaoCondition.isSelected = true;
    };
    IcaoSubjectsListComponent.prototype.itemsPerPageChanged = function (e) {
        this.queryOptions.pageSize = e.value;
        this.getIcaoSubjectsInPage(1);
    };
    IcaoSubjectsListComponent.prototype.onPageChange = function (page) {
        this.getIcaoSubjectsInPage(page);
    };
    IcaoSubjectsListComponent.prototype.sort = function (index) {
        var dir = this.tableSubjectHeaders.sorting[index].direction;
        for (var i = 0; i < this.tableSubjectHeaders.sorting.length; i++) {
            this.tableSubjectHeaders.sorting[i].direction = 0;
        }
        if (dir > 0) {
            this.tableSubjectHeaders.sorting[index].direction = -1;
            this.queryOptions.sort = '_' + this.tableSubjectHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableSubjectHeaders.sorting[index].direction = 1;
            this.queryOptions.sort = this.tableSubjectHeaders.sorting[index].columnName;
        }
        this.getIcaoSubjectsInPage(this.queryOptions.page);
    };
    IcaoSubjectsListComponent.prototype.sortCondition = function (index) {
        var dir = this.tableConditionHeaders.sorting[index].direction;
        for (var i = 0; i < this.tableConditionHeaders.sorting.length; i++) {
            this.tableConditionHeaders.sorting[i].direction = 0;
        }
        if (dir > 0) {
            this.tableConditionHeaders.sorting[index].direction = -1;
            this.conditionQueryOptions.sort = '_' + this.tableConditionHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableConditionHeaders.sorting[index].direction = 1;
            this.conditionQueryOptions.sort = this.tableConditionHeaders.sorting[index].columnName;
        }
        this.getIcaoConditions(this.selectedIcaoSubject.id);
    };
    IcaoSubjectsListComponent.prototype.sortingClass = function (index, isSubject) {
        var tableHeaders = isSubject ? this.tableSubjectHeaders : this.tableConditionHeaders;
        if (tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';
        if (tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';
        return 'sorting';
    };
    IcaoSubjectsListComponent.prototype.confirmDeleteIcaoSubject = function () {
        $('#modal-delete-subject').modal({});
    };
    IcaoSubjectsListComponent.prototype.confirmDeleteIcaoCondition = function () {
        $('#modal-delete-condition').modal({});
    };
    IcaoSubjectsListComponent.prototype.deleteIcaoSubject = function () {
        var _this = this;
        if (this.selectedIcaoSubject) {
            this.dataService.deleteIcaoSubject(this.selectedIcaoSubject.id)
                .subscribe(function (result) {
                _this.getIcaoSubjectsInPage(_this.paging.currentPage);
                var msg = _this.translation.translate('IcaoSubjectSession.SuccessDeleted');
                _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            }, function (error) {
                var msg = _this.translation.translate('IcaoSubjectSession.FailureDeleted');
                _this.toastr.error(msg + " " + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
        }
    };
    IcaoSubjectsListComponent.prototype.deleteIcaoCondition = function () {
        var _this = this;
        if (this.selectedIcaoSubject) {
            this.dataService.deleteIcaoCondition(this.selectedIcaoCondition.id)
                .subscribe(function (result) {
                _this.getIcaoConditions(_this.selectedIcaoSubject.id);
                var msg = _this.translation.translate('IcaoSubjectSession.SuccessConditionDeleted');
                _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            }, function (error) {
                var msg = _this.translation.translate('IcaoSubjectSession.FailureConditionDeleted');
                _this.toastr.error(msg + " " + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
        }
    };
    IcaoSubjectsListComponent.prototype.getIcaoSubjectsInPage = function (page) {
        var _this = this;
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, this.queryOptions);
            this.dataService.getRegisteredIcaoSubjects(this.queryOptions)
                .subscribe(function (icaoSubjects) {
                _this.processIcaoSubjects(icaoSubjects);
            });
        }
        catch (e) {
            this.loadingData = false;
        }
    };
    IcaoSubjectsListComponent.prototype.getIcaoConditions = function (subjectId) {
        var _this = this;
        try {
            this.loadingConditions = true;
            this.dataService.getRegisteredIcaoSubjectConditions(subjectId, this.conditionQueryOptions)
                .subscribe(function (icaoConditions) {
                _this.processIcaoConditions(icaoConditions);
            });
        }
        catch (e) {
            this.loadingConditions = false;
        }
    };
    IcaoSubjectsListComponent.prototype.processIcaoSubjects = function (icaoSubjects) {
        this.paging = icaoSubjects.paging;
        this.totalPages = icaoSubjects.paging.totalPages;
        for (var iter = 0; iter < icaoSubjects.data.length; iter++)
            icaoSubjects.data[iter].index = iter;
        var lastICaoSubjectIdSelected = this.memStorageService.get(this.memStorageService.ITEM_ID_KEY);
        if (icaoSubjects.data.length > 0) {
            this.icaoSubjects = icaoSubjects.data;
            var index = 0;
            if (lastICaoSubjectIdSelected) {
                index = this.icaoSubjects.findIndex(function (x) { return x.id === lastICaoSubjectIdSelected; });
                if (index < 0)
                    index = 0; // when not found first should be selected;
            }
            this.icaoSubjects[index].isSelected = true;
            this.selectedIcaoSubject = this.icaoSubjects[index];
            this.getIcaoConditions(this.selectedIcaoSubject.id);
            this.loadingData = false;
        }
        else {
            this.icaoSubjects = [];
            this.icaoConditions = [];
            this.selectedIcaoSubject = null;
            this.selectedIcaoCondition = null;
            this.loadingData = false;
        }
    };
    IcaoSubjectsListComponent.prototype.processIcaoConditions = function (icaoConditions) {
        if (icaoConditions.data.length > 0) {
            this.icaoConditions = icaoConditions.data;
            this.icaoConditions[0].isSelected = true;
            this.selectedIcaoCondition = this.icaoConditions[0];
            this.loadingConditions = false;
        }
        else {
            this.icaoConditions = [];
            this.selectedIcaoCondition = null;
            this.loadingConditions = false;
        }
    };
    IcaoSubjectsListComponent.prototype.updateLeftMenuHeight = function () {
        var elem = $('.sidebar-inner');
        if (this.leftMenuHeight) {
            elem.height(this.leftMenuHeight);
        }
        else {
            this.leftMenuHeight = elem.height();
            elem.height("2000px");
        }
    };
    IcaoSubjectsListComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/icaosubjects/icaosubjects-list.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], IcaoSubjectsListComponent);
    return IcaoSubjectsListComponent;
}());
exports.IcaoSubjectsListComponent = IcaoSubjectsListComponent;
//# sourceMappingURL=icaosubjects-list.component.js.map
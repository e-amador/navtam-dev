"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CatchAllDraftComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var data_model_1 = require("../../shared/data.model");
var data_service_1 = require("../../shared/data.service");
var location_service_1 = require("../../shared/location.service");
var catchall_service_1 = require("./catchall.service");
var toastr_service_1 = require("./../../../common/toastr.service");
var angular_l10n_1 = require("angular-l10n");
var CatchAllDraftComponent = /** @class */ (function () {
    function CatchAllDraftComponent(toastr, injector, dataService, locationService, catchAllService, fb, changeDetectionRef, translation) {
        this.toastr = toastr;
        this.injector = injector;
        this.dataService = dataService;
        this.locationService = locationService;
        this.catchAllService = catchAllService;
        this.fb = fb;
        this.changeDetectionRef = changeDetectionRef;
        this.translation = translation;
        this.action = "";
        this.token = null;
        this.isReadOnly = false;
        this.isBilingualRegion = false;
        this.overrideBilingual = false;
        this.locationInDoa = true;
        this.disableIcaoSubjectSelect = true;
        this.disableIcaoConditionSelect = true;
        this.subjectSelected = null;
        this.icaoSubjectSelected = null;
        this.icaoConditionSelected = null;
        this.icaoSubjectSelectOptions = {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.ICAOSubjectPlaceholder')
            }
        };
        this.icaoConditionSelectOptions = {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.ICAOConditionPlaceholder')
            }
        };
        this.nonIcaoAerodrome = false;
        this.isTrafficDisabled = true;
        this.itemFErrorMessage = '';
        this.itemGErrorMessage = '';
        this.token = this.injector.get('token');
        this.proposalType = this.injector.get('proposalType');
        this.nsdForm = this.injector.get('form');
        this.isReadOnly = this.injector.get('isReadOnly');
        this.action = this.injector.get('type');
        this.overrideBilingual = this.token.overrideBilingual;
    }
    CatchAllDraftComponent.prototype.ngOnInit = function () {
        var _this = this;
        var app = this.catchAllService.app;
        this.leafletmap = app.leafletmap;
        this.isFormLoadingData = !(!this.token.subjectId);
        this.dataService.getFirs()
            .subscribe(function (firs) {
            _this.firs = firs;
        });
        this.createUnitsListBox();
        this.createSeriesListBox();
        var frmArr = this.nsdForm.controls['frmArr'];
        if (frmArr.length === 0) {
            this.configureReactiveForm();
        }
        else {
            /*
            this is required because of the grouping funtionallity. When the NsdFormGroup
            was configured and any of items in the group has changed (new set of tokens values)
            the NsdForm Group needs to be updated with the new tokens values */
            this.updateReactiveForm(frmArr);
        }
        this.nsdFrmGroup.get('grpItemFG.itemFSurface')
            .valueChanges.subscribe(function (value) { return _this.itemFSurfaceChanged(value); });
        this.nsdFrmGroup.get('grpItemFG.itemGUnlimited')
            .valueChanges.subscribe(function (value) { return _this.itemGUnlimitedChanged(value); });
        this.nsdFrmGroup.get('subjectLocation')
            .valueChanges.subscribe(function (value) { return _this.subjectLocationChanged(value); });
        this.nsdFrmGroup.get('latitude')
            .valueChanges.subscribe(function (value) { return _this.latitudeLongitudeChanged(value); });
        this.nsdFrmGroup.get('longitude')
            .valueChanges.subscribe(function (value) { return _this.latitudeLongitudeChanged(value); });
        if (this.isFormLoadingData) {
            this.loadSubject(this.token.subjectId);
        }
        this.loadMapLocations(app.doaId, this.isFormLoadingData);
    };
    CatchAllDraftComponent.prototype.ngOnDestroy = function () {
        /* There is an issue when a grouped proposal change item A. Map was null because of following code  */
        //this.leafletmap.dispose();
        //this.leafletmap = null;
    };
    CatchAllDraftComponent.prototype.ngAfterViewInit = function () {
        this.DisableItemFG(this.nsdFrmGroup);
    };
    CatchAllDraftComponent.prototype.requireLowerUpperLimit = function () {
        return this.nsdFrmGroup.get('icaoConditionId').value != -1 &&
            (!this.nsdFrmGroup.get('requiresItemFG').value ||
                this.nsdFrmGroup.get('grpItemFG.itemFUnits').value === 2 ||
                this.nsdFrmGroup.get('grpItemFG.itemGUnits').value === 2);
    };
    CatchAllDraftComponent.prototype.requireItemFG = function () {
        return this.nsdFrmGroup.get('requiresItemFG').value;
    };
    CatchAllDraftComponent.prototype.DisableItemFG = function (grp) {
        if (grp.controls.grpItemFG.controls.itemGUnlimited.value) {
            this.nsdFrmGroup.get('grpItemFG.itemGValue').disable();
        }
        if (grp.controls.grpItemFG.controls.itemFSurface.value) {
            this.nsdFrmGroup.get('grpItemFG.itemFValue').disable();
        }
    };
    CatchAllDraftComponent.prototype.icaoSubjectChanged = function (e) {
        var _this = this;
        if (e.value) {
            var subjectId_1 = this.isFormLoadingData ? this.nsdFrmGroup.get('icaoSubjectId').value : +e.value;
            if (subjectId_1 !== -1) {
                if (this.icaoSubjectSelected && this.icaoSubjectSelected.id === subjectId_1) {
                    return; //when multiples change events occurring react only to the first one
                }
                this.changeDetectionRef.detectChanges();
                if (!this.isFormLoadingData) {
                    this.nsdFrmGroup.patchValue({
                        icaoSubjectId: subjectId_1,
                        icaoConditionId: -1
                    });
                    this.nsdFrmGroup.get("icaoSubjectId").markAsDirty();
                    this.resetIcaoSubjectChildrenControls();
                }
                this.loadIcaoConditions(subjectId_1);
                this.disableIcaoConditionSelect = false;
                this.icaoSubjectSelected = this.icaoSubjects.filter(function (item) { return item.id === subjectId_1; })[0];
                if (this.icaoSubjectSelected) {
                    if (this.icaoSubjectSelected.requiresItemA) {
                        this.nsdFrmGroup.get('itemA').setValidators(itemAValidator(this.subjectSelected.designator, this));
                    }
                    else {
                        this.nsdFrmGroup.get('itemA').clearValidators();
                    }
                    if (!this.isFormLoadingData) {
                        this.nsdFrmGroup.patchValue({
                            requiresSeries: this.icaoSubjectSelected.requiresSeries,
                            requiresItemA: this.icaoSubjectSelected.requiresItemA,
                            requiresScope: this.icaoSubjectSelected.requiresScope,
                        });
                        this.nsdFrmGroup.patchValue({ itemA: this.formatDesignator(this.subjectSelected.designator) });
                        var fc = this.nsdFrmGroup.get('scope');
                        if (this.icaoSubjectSelected.requiresScope) {
                            this.nsdFrmGroup.patchValue({ scope: this.icaoSubjectSelected.scope });
                            if (this.scopes) {
                                var scope = this.scopes.filter(function (item) { return item.id === _this.icaoSubjectSelected.scope; });
                                if (scope.length === 1) {
                                    fc.setValue(scope[0].id);
                                    fc.markAsDirty();
                                }
                            }
                        }
                        else {
                            this.token.scope = null;
                            fc.setValue(null);
                            fc.markAsDirty();
                        }
                    }
                    var seriesCtrl = this.nsdFrmGroup.get('series');
                    if (this.icaoSubjectSelected.requiresSeries) {
                        this.loadSeries(this.icaoSubjectSelected.code);
                        if (!this.isFormLoadingData) {
                            this.calculateSeries(this.icaoSubjectSelected.code, seriesCtrl);
                        }
                    }
                    else {
                        this.token.series = null;
                        seriesCtrl.setValue(null);
                        seriesCtrl.markAsDirty;
                    }
                }
            }
            else {
                this.disableIcaoConditionSelect = true;
                if (!this.isFormLoadingData) {
                    this.resetIcaoSubjectChildrenControls();
                    this.nsdFrmGroup.markAsDirty();
                }
            }
        }
    };
    CatchAllDraftComponent.prototype.icaoConditionChanged = function (e) {
        if (e.value) {
            var conditionId_1 = +e.value;
            var icaoConditionIdCtrl = this.nsdFrmGroup.get('icaoConditionId');
            if (this.isFormLoadingData) {
                conditionId_1 = +icaoConditionIdCtrl.value;
            }
            else {
                if (conditionId_1 !== icaoConditionIdCtrl.value) {
                    icaoConditionIdCtrl.setValue(conditionId_1);
                    icaoConditionIdCtrl.markAsDirty();
                }
            }
            if (conditionId_1 === -1) {
                this.resetIcaoConditionChildrenControls();
            }
            else {
                if (this.icaoConditionSelected && this.icaoConditionSelected.id === conditionId_1) {
                    return; //when multiples change events occurring react only to the first one
                }
                this.icaoConditionSelected = this.icaoConditions.filter(function (item) { return item.id === conditionId_1; })[0];
                if (this.icaoConditionSelected) {
                    this.changeDetectionRef.detectChanges();
                    var grpTrafficCtrl = this.nsdFrmGroup.get('grpTraffic');
                    grpTrafficCtrl.setValidators(trafficValidator);
                    var grpItemFGCtrl = this.nsdFrmGroup.get('grpItemFG');
                    if (this.icaoConditionSelected.requiresItemFG) {
                        grpItemFGCtrl.setValidators([itemFValidator(this), itemGValidator(this)]);
                    }
                    else {
                        grpItemFGCtrl.clearValidators();
                        this.token.itemFValue = 0;
                        this.token.itemFUnits = 0;
                        this.token.itemFSurface = false;
                        this.token.itemGValue = 1;
                        this.token.itemGUnits = 0;
                        this.token.itemGUnlimited = false;
                    }
                    var grpPurposesCtrl = this.nsdFrmGroup.get('grpPurposes');
                    if (this.icaoConditionSelected.requiresPurpose) {
                        grpPurposesCtrl.setValidators(purposeAllowValidator);
                    }
                    else {
                        grpPurposesCtrl.clearValidators();
                    }
                    this.isTrafficDisabled = !(this.icaoConditionSelected.i && this.icaoConditionSelected.v);
                    this.lowerLimit = this.icaoConditionSelected.lower;
                    this.upperLimit = this.icaoConditionSelected.upper;
                    this.lowerFLimit = this.icaoConditionSelected.lower;
                    this.upperFLimit = this.icaoConditionSelected.upper - 1;
                    if (this.upperFLimit > 998)
                        this.upperFLimit = 998;
                    this.lowerGLimit = this.icaoConditionSelected.lower + 1;
                    this.upperGLimit = this.icaoConditionSelected.upper;
                    if (this.upperGLimit > 999)
                        this.upperGLimit = 999;
                    this.minimunRadius = this.icaoConditionSelected.radius;
                    if (this.minimunRadius == 999) {
                        //TODO: Create a new column in the ICAOSubjectConditions to register the defaulkt value and the minimiun value
                        this.minimunRadius = 1;
                    }
                    var radiusCtrl = this.nsdFrmGroup.get('radius');
                    radiusCtrl.clearValidators();
                    radiusCtrl.setValidators(radiusValidator(this.minimunRadius));
                    if (this.requireLowerUpperLimit()) {
                        var ctrl = this.nsdFrmGroup.get('lowerLimit');
                        ctrl.clearValidators();
                        ctrl.setErrors(null);
                        ctrl = this.nsdFrmGroup.get('upperLimit');
                        ctrl.clearValidators();
                        ctrl.setErrors(null);
                    }
                    else {
                        this.nsdFrmGroup.get("lowerLimit").setValidators(lowerLimitValidator(this.nsdFrmGroup.get('upperLimit')));
                        this.nsdFrmGroup.get("upperLimit").setValidators(upperLimitValidator(this.nsdFrmGroup.get('lowerLimit')));
                    }
                    if (!this.isFormLoadingData) {
                        this.nsdFrmGroup.patchValue({
                            requiresItemFG: this.icaoConditionSelected.requiresItemFG,
                            requiresPurpose: this.icaoConditionSelected.requiresPurpose,
                            requiresTraffic: this.icaoConditionSelected.i || this.icaoConditionSelected.v,
                            grpItemFG: {
                                itemFValue: 0,
                                itemFSurface: this.token.itemFSurface,
                                itemFUnits: this.token.itemFUnits,
                                itemGValue: (this.token.itemGUnlimited ? this.upperGLimit : 1),
                                itemGUnlimited: this.token.itemGUnlimited,
                                itemGUnits: this.token.itemGUnits,
                            },
                            grpPurposes: {
                                purposeB: this.icaoConditionSelected.b,
                                purposeM: this.icaoConditionSelected.m,
                                purposeN: this.icaoConditionSelected.n,
                                purposeO: this.icaoConditionSelected.o,
                            },
                            radius: this.icaoConditionSelected.radius,
                            lowerLimit: this.icaoConditionSelected.lower,
                            upperLimit: this.icaoConditionSelected.upper,
                            grpTraffic: {
                                trafficI: this.icaoConditionSelected.i,
                                trafficV: this.icaoConditionSelected.v
                            }
                        });
                    }
                    this.isFormLoadingData = false;
                }
            }
        }
        else {
        }
    };
    CatchAllDraftComponent.prototype.scopeChanged = function (e) {
        if (!this.isFormLoadingData) {
            var fc = this.nsdFrmGroup.get('scope');
            if (fc.value !== e.value) {
                fc.setValue(e.value);
                fc.markAsDirty();
            }
        }
    };
    CatchAllDraftComponent.prototype.seriesChanged = function (e) {
        if (!this.isFormLoadingData) {
            var fc = this.nsdFrmGroup.get('series');
            if (fc.value !== e.value) {
                fc.setValue(e.value);
                fc.markAsDirty();
            }
        }
    };
    CatchAllDraftComponent.prototype.checkboxChanged = function (controlName, checked) {
        var control = this.nsdFrmGroup.get(controlName);
        control.setValue(checked);
        control.markAsDirty();
    };
    CatchAllDraftComponent.prototype.decValue = function (fieldName) {
        if (this.isReadOnly)
            return;
        var value = 0;
        switch (fieldName) {
            case 'lowerLimit':
                if (this.icaoConditionSelected) {
                    if (!this.nsdFrmGroup.get('grpItemFG.itemFSurface').value) {
                        value = +this.nsdFrmGroup.get('lowerLimit').value;
                        if (value > 0) {
                            this.nsdFrmGroup.get('lowerLimit').setValue(value - 1);
                            this.nsdFrmGroup.get('lowerLimit').updateValueAndValidity();
                            this.nsdFrmGroup.get('upperLimit').updateValueAndValidity();
                            this.nsdFrmGroup.get('lowerLimit').markAsDirty();
                        }
                    }
                }
                break;
            case 'upperLimit':
                if (this.icaoConditionSelected) {
                    if (!this.nsdFrmGroup.get('grpItemFG.itemGUnlimited').value) {
                        value = +this.nsdFrmGroup.get('upperLimit').value;
                        if (value > 0) {
                            this.nsdFrmGroup.get('upperLimit').setValue(value - 1);
                            this.nsdFrmGroup.get('lowerLimit').updateValueAndValidity();
                            this.nsdFrmGroup.get('upperLimit').updateValueAndValidity();
                            this.nsdFrmGroup.get('upperLimit').markAsDirty();
                        }
                    }
                }
                break;
            case 'radius':
                if (this.icaoConditionSelected) {
                    value = +this.nsdFrmGroup.get('radius').value;
                    if (value > this.minimunRadius) {
                        this.nsdFrmGroup.get('radius').setValue(value - 1);
                        this.nsdFrmGroup.get('radius').markAsDirty();
                    }
                }
                break;
            case 'itemF':
                if (!this.nsdFrmGroup.get('grpItemFG.itemFSurface').value) {
                    value = +this.nsdFrmGroup.get('grpItemFG.itemFValue').value;
                    var units = +this.nsdFrmGroup.get('grpItemFG.itemFUnits').value;
                    if (value > (units === 0 ? this.lowerFLimit : this.lowerFLimit * 100)) {
                        this.nsdFrmGroup.get('grpItemFG.itemFValue').setValue(value - 1);
                        this.nsdFrmGroup.get('grpItemFG.itemFValue').updateValueAndValidity();
                        this.nsdFrmGroup.get('grpItemFG.itemGValue').updateValueAndValidity();
                        this.nsdFrmGroup.get('grpItemFG.itemFValue').markAsDirty();
                    }
                }
                break;
            case 'itemG':
                if (!this.nsdFrmGroup.get('grpItemFG.itemGUnlimited').value) {
                    value = +this.nsdFrmGroup.get('grpItemFG.itemGValue').value;
                    var units = +this.nsdFrmGroup.get('grpItemFG.itemGUnits').value;
                    if (value > (units === 0 ? this.lowerGLimit : this.lowerGLimit * 100)) {
                        if (value > 2) {
                            this.nsdFrmGroup.get('grpItemFG.itemGValue').setValue(value - 1);
                            this.nsdFrmGroup.get('grpItemFG.itemFValue').updateValueAndValidity();
                            this.nsdFrmGroup.get('grpItemFG.itemGValue').updateValueAndValidity();
                            this.nsdFrmGroup.get('grpItemFG.itemGValue').markAsDirty();
                        }
                    }
                }
                break;
        }
    };
    CatchAllDraftComponent.prototype.incValue = function (fieldName) {
        if (this.isReadOnly)
            return;
        var value = 0;
        switch (fieldName) {
            case 'lowerLimit':
                if (this.icaoConditionSelected) {
                    value = +this.nsdFrmGroup.get('lowerLimit').value;
                    if (!this.nsdFrmGroup.get('grpItemFG.itemFSurface').value) {
                        if (value < this.upperLimit) {
                            this.nsdFrmGroup.get('lowerLimit').setValue(value + 1);
                            this.nsdFrmGroup.get('lowerLimit').updateValueAndValidity();
                            this.nsdFrmGroup.get('upperLimit').updateValueAndValidity();
                            this.nsdFrmGroup.get('lowerLimit').markAsDirty();
                        }
                    }
                }
                break;
            case 'upperLimit':
                if (this.icaoConditionSelected) {
                    value = +this.nsdFrmGroup.get('upperLimit').value;
                    if (!this.nsdFrmGroup.get('grpItemFG.itemGUnlimited').value) {
                        if (value < this.upperLimit) {
                            this.nsdFrmGroup.get('upperLimit').setValue(value + 1);
                            this.nsdFrmGroup.get('lowerLimit').updateValueAndValidity();
                            this.nsdFrmGroup.get('upperLimit').updateValueAndValidity();
                            this.nsdFrmGroup.get('upperLimit').markAsDirty();
                        }
                    }
                }
                break;
            case 'radius':
                if (this.icaoConditionSelected) {
                    value = +this.nsdFrmGroup.get('radius').value;
                    if (value < 999) {
                        this.nsdFrmGroup.get('radius').setValue(value + 1);
                        this.nsdFrmGroup.get('radius').markAsDirty();
                    }
                }
                break;
            case 'itemF':
                if (!this.nsdFrmGroup.get('grpItemFG.itemFSurface').value) {
                    value = +this.nsdFrmGroup.get('grpItemFG.itemFValue').value;
                    var units = +this.nsdFrmGroup.get('grpItemFG.itemFUnits').value;
                    var limit = (units === 0 ? this.upperFLimit : (this.upperFLimit * 100) + 99);
                    if (value < limit) {
                        this.nsdFrmGroup.get('grpItemFG.itemFValue').setValue(value + 1);
                        this.nsdFrmGroup.get('grpItemFG.itemFValue').updateValueAndValidity();
                        this.nsdFrmGroup.get('grpItemFG.itemGValue').updateValueAndValidity();
                        this.nsdFrmGroup.get('grpItemFG.itemFValue').markAsDirty();
                    }
                }
                break;
            case 'itemG':
                if (!this.nsdFrmGroup.get('grpItemFG.itemGUnlimited').value) {
                    value = +this.nsdFrmGroup.get('grpItemFG.itemGValue').value;
                    var units = +this.nsdFrmGroup.get('grpItemFG.itemGUnits').value;
                    var limit = (units === 0 ? this.upperGLimit : (this.upperGLimit * 100) + 99);
                    if (value < limit) {
                        this.nsdFrmGroup.get('grpItemFG.itemGValue').setValue(value + 1);
                        this.nsdFrmGroup.get('grpItemFG.itemFValue').updateValueAndValidity();
                        this.nsdFrmGroup.get('grpItemFG.itemGValue').updateValueAndValidity();
                        this.nsdFrmGroup.get('grpItemFG.itemGValue').markAsDirty();
                    }
                }
                break;
        }
    };
    CatchAllDraftComponent.prototype.validateKeyPressNumber = function (evt) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
        }
    };
    CatchAllDraftComponent.prototype.validateSubjectId = function () {
        var fc = this.nsdFrmGroup.get('subjectId');
        return fc.value || this.nsdFrmGroup.pristine;
    };
    CatchAllDraftComponent.prototype.validateIcaoSubjectId = function () {
        var fc = this.nsdFrmGroup.get('icaoSubjectId');
        return fc.value !== -1 || this.nsdFrmGroup.pristine;
    };
    CatchAllDraftComponent.prototype.validateIcaoConditionId = function () {
        var fc = this.nsdFrmGroup.get('icaoConditionId');
        return fc.value !== -1 || this.nsdFrmGroup.pristine;
    };
    CatchAllDraftComponent.prototype.validateSubjectLocation = function () {
        var fc = this.nsdFrmGroup.get('subjectLocation');
        return !fc.errors || this.nsdFrmGroup.pristine;
    };
    CatchAllDraftComponent.prototype.validateSubjectLatitude = function () {
        var fc = this.nsdFrmGroup.get('latitude');
        return !fc.errors || this.nsdFrmGroup.pristine;
    };
    CatchAllDraftComponent.prototype.validateSubjectLongitude = function () {
        var fc = this.nsdFrmGroup.get('longitude');
        return !fc.errors || this.nsdFrmGroup.pristine;
    };
    CatchAllDraftComponent.prototype.validateAerodromeName = function () {
        var fc = this.nsdFrmGroup.get('aerodromeName');
        return !fc.errors || this.nsdFrmGroup.pristine;
    };
    CatchAllDraftComponent.prototype.validateRadius = function () {
        var fc = this.nsdFrmGroup.get('radius');
        return !fc.errors || this.nsdFrmGroup.pristine;
    };
    CatchAllDraftComponent.prototype.validateLowerLimit = function () {
        var fc = this.nsdFrmGroup.get('lowerLimit');
        return !fc.errors || this.nsdFrmGroup.pristine;
    };
    CatchAllDraftComponent.prototype.validateUpperLimit = function () {
        var fc = this.nsdFrmGroup.get('upperLimit');
        return !fc.errors || this.nsdFrmGroup.pristine;
    };
    CatchAllDraftComponent.prototype.validateItemE = function () {
        var fc = this.nsdFrmGroup.get('itemEValue');
        return fc.value || this.nsdFrmGroup.pristine;
    };
    CatchAllDraftComponent.prototype.validateItemEFrench = function () {
        var fc = this.nsdFrmGroup.get('itemEFrenchValue');
        return fc.value || this.nsdFrmGroup.pristine;
    };
    CatchAllDraftComponent.prototype.select2Options = function () {
        var self = this;
        var app = this.catchAllService.app;
        return {
            minimumInputLength: 2,
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.SdoSubjectPlaceholder')
            },
            ajax: {
                type: 'GET',
                url: app.apiUrl + "sdosubjects/catchallfilter",
                dataType: 'json',
                delay: 500,
                headers: {
                    "RequestVerificationToken": app.antiForgeryTokenValue
                },
                data: function (parms, page) {
                    return {
                        Query: parms.term,
                        Size: 200,
                        doaId: app.doaId //set by the app controller
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            templateResult: function (data) {
                if (data.loading) {
                    return data.text;
                }
                return self.dataService.getSdoExtendedName(data);
            },
            templateSelection: function (selection) {
                if (selection && selection.id === "-1") {
                    return selection.text;
                }
                self.updateSubjectSelected(selection);
                return self.dataService.getSdoExtendedName(selection);
            },
        };
    };
    CatchAllDraftComponent.prototype.loadSubject = function (subjectId) {
        var _this = this;
        this.dataService.getSdoSubject(subjectId)
            .subscribe(function (sdoSubjectResult) {
            _this.subjectSelected = sdoSubjectResult;
            _this.subjects = [];
            _this.subjects.push(sdoSubjectResult);
            _this.nonIcaoAerodrome = _this.designatorHasNumbers(sdoSubjectResult.designator);
            if (_this.nonIcaoAerodrome) {
                var aerodromeCtrl = _this.nsdFrmGroup.get('aerodromeName');
                if (!aerodromeCtrl.value) {
                    aerodromeCtrl.markAsDirty();
                }
                aerodromeCtrl.setValidators(forms_1.Validators.required);
            }
            _this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);
            _this.loadIcaoSubjects(sdoSubjectResult.sdoEntityName);
        });
    };
    CatchAllDraftComponent.prototype.loadSeries = function (qCode) {
        var _this = this;
        this.dataService.getSeries()
            .subscribe(function (series) {
            _this.setFirstSelectItemInCombo(series, _this.token.series, false);
            _this.series = series;
        });
    };
    CatchAllDraftComponent.prototype.calculateSeries = function (qCode, fc) {
        var _this = this;
        if (qCode && this.subjectSelected) {
            this.dataService.getSerie(this.subjectSelected.id, qCode)
                .subscribe(function (serie) {
                _this.nsdFrmGroup.patchValue({ series: serie });
                if (_this.series) {
                    var series = _this.series.filter(function (item) { return item.id === serie; });
                    if (series.length === 1) {
                        if (fc.value !== series[0].id) {
                            fc.setValue(series[0].id);
                            fc.markAsDirty();
                        }
                    }
                }
            });
        }
    };
    CatchAllDraftComponent.prototype.loadMapLocations = function (doaId, loadingFromProposal) {
        var _this = this;
        var self = this;
        this.dataService.getDoaLocation(doaId)
            .subscribe(function (geoJsonStr) {
            var interval = window.setInterval(function () {
                if (self.leafletmap && self.leafletmap.isReady()) {
                    window.clearInterval(interval);
                    self.leafletmap.addDoa(geoJsonStr, 'DOA');
                    if (loadingFromProposal) {
                        self.updateMap(self.subjectSelected, false);
                    }
                }
                ;
            }, 100);
        }, function (error) {
            _this.toastr.error(_this.translation.translate('Nsd.DOAGeoError') + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    CatchAllDraftComponent.prototype.updateSubjectSelected = function (sdoSubject) {
        var _this = this;
        this.createScopesListBox(sdoSubject.sdoEntityName);
        this.subjectSelected = sdoSubject;
        var subjectId = this.nsdFrmGroup.get('subjectId').value;
        if (subjectId === sdoSubject.id) {
            //for some weird reasons select2 templateSelection is called multiples
            //times after selection
            return;
        }
        if (!this.isReadOnly) {
            var itemACtrl = this.nsdFrmGroup.get('itemA');
            if (sdoSubject.sdoEntityName === "Ahp") {
                itemACtrl.disable(); //disable ItemA when Aerodrome
            }
            else {
                itemACtrl.enable();
            }
        }
        if (sdoSubject.id !== subjectId) {
            var subjectCtrl = this.nsdFrmGroup.get('subjectId');
            subjectCtrl.setValue(sdoSubject.id);
            subjectCtrl.markAsDirty();
        }
        this.icaoSubjects = null;
        this.icaoSubjectSelected = null;
        this.icaoConditionSelected = null;
        this.nonIcaoAerodrome = this.designatorHasNumbers(this.subjectSelected.designator);
        var aerodromeCtrl = this.nsdFrmGroup.get('aerodromeName');
        if (this.nonIcaoAerodrome) {
            aerodromeCtrl.setValidators(forms_1.Validators.required);
        }
        else {
            aerodromeCtrl.clearValidators();
        }
        this.nsdFrmGroup.patchValue({
            icaoSubjectId: -1,
            icaoConditionId: -1,
            itemA: this.nonIcaoAerodrome ? "CXXX" : this.subjectSelected.designator,
            aerodromeName: this.nonIcaoAerodrome ? this.subjectSelected.name : null
        });
        this.disableIcaoSubjectSelect = true;
        this.disableIcaoConditionSelect = true;
        if (!sdoSubject.subjectGeo) {
            this.dataService.getSdoSubject(sdoSubject.id)
                .subscribe(function (sdoSubjectResult) {
                _this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);
                _this.updateMap(sdoSubjectResult, true);
            });
        }
        this.loadIcaoSubjects(sdoSubject.sdoEntityName);
    };
    CatchAllDraftComponent.prototype.loadIcaoSubjects = function (sdoEntityName) {
        var _this = this;
        if (sdoEntityName) {
            this.dataService.getIcaoSubjects(sdoEntityName)
                .subscribe(function (icaoSubjects) {
                _this.disableIcaoSubjectSelect = false;
                _this.setFirstSelectItemInCombo(icaoSubjects, _this.token.icaoSubjectId);
                _this.icaoSubjects = icaoSubjects.map(function (item) {
                    if (item.code !== undefined) {
                        if (item.name.length > 90) {
                            item.text = item.code + ' - ' + item.name.substr(0, 80) + '...';
                        }
                        else {
                            item.text = item.code + ' - ' + item.name;
                        }
                    }
                    return item;
                });
                if (!_this.isFormLoadingData) {
                    _this.resetIcaoSubjectChildrenControls();
                }
            });
        }
    };
    CatchAllDraftComponent.prototype.loadIcaoConditions = function (icaoSubjectId) {
        var _this = this;
        if (icaoSubjectId) {
            this.dataService.getIcaoConditions(icaoSubjectId, this.proposalType === data_model_1.ProposalType.Cancel)
                .subscribe(function (icaoConditions) {
                _this.setFirstSelectItemInCombo(icaoConditions, _this.token.icaoConditionId);
                _this.icaoConditions = icaoConditions.map(function (item) {
                    if (item.code !== undefined) {
                        if (item.description.length > 90) {
                            item.text = item.code + ' - ' + item.description.substr(0, 80) + '...';
                        }
                        else {
                            item.text = item.code + ' - ' + item.description;
                        }
                    }
                    return item;
                });
            });
        }
    };
    CatchAllDraftComponent.prototype.updateMap = function (sdoSubject, acceptSubjectLocation) {
        var _this = this;
        if (acceptSubjectLocation && sdoSubject && sdoSubject.subjectGeoRefPoint) {
            var geojson = JSON.parse(sdoSubject.subjectGeoRefPoint);
            var subLocation = this.leafletmap.geojsonToTextCordinates(geojson);
            var coordinates = geojson.features[0].geometry.coordinates;
            this.nsdFrmGroup.patchValue({
                subjectLocation: subLocation,
                latitude: coordinates[1],
                longitude: coordinates[0],
            }, { onlySelf: true, emitEvent: false });
            if (this.token) {
                this.token.subjectLocation = subLocation;
            }
            var location_1 = this.locationService.parse(this.token.subjectLocation);
            this.locationInDoa = true;
            this.updateSubjectLocationInMap(location_1.latitude, location_1.longitude);
            this.setBilingualRegion(sdoSubject.isBilingualRegion);
        }
        else if (this.token && this.token.subjectLocation) {
            var location_2 = this.locationService.parse(this.token.subjectLocation);
            this.dataService.validateLocation(this.token.subjectLocation)
                .subscribe(function (result) {
                _this.locationInDoa = result.inDoa;
                if (_this.locationInDoa) {
                    _this.updateSubjectLocationInMap(location_2.latitude, location_2.longitude);
                    _this.setBilingualRegion(result.inBilingualRegion);
                }
            });
            //this.updateSubjectLocationInMap(this.token.latitude, this.token.longitude);
        }
        else {
            this.leafletmap.clearFeaturesOnLayers(['DOA']);
            if (sdoSubject && sdoSubject.subjectGeoRefPoint) {
                var geojson_1 = JSON.parse(sdoSubject.subjectGeoRefPoint);
                if (geojson_1.features.length === 1) {
                    var subLocation = this.leafletmap.geojsonToTextCordinates(geojson_1);
                    var coordinates = geojson_1.features[0].geometry.coordinates;
                    var markerGeojson = acceptSubjectLocation ? geojson_1 : this.getGeoJsonFromLocation(this.token.subjectLocation);
                    if (acceptSubjectLocation) {
                        this.nsdFrmGroup.patchValue({
                            subjectLocation: subLocation,
                            latitude: coordinates[1],
                            longitude: coordinates[0],
                        }, { onlySelf: true, emitEvent: false });
                    }
                    this.dataService.validateLocation(subLocation)
                        .subscribe(function (result) {
                        _this.locationInDoa = result.inDoa;
                        if (_this.locationInDoa) {
                            _this.leafletmap.addMarker(geojson_1, sdoSubject.name, function () {
                            });
                        }
                    });
                    //this.leafletmap.addMarker(geojson, sdoSubject.name, () => {
                    //});
                }
            }
        }
    };
    CatchAllDraftComponent.prototype.updateSubjectLocationInMap = function (latitude, longitude) {
        if (this.leafletmap) {
            var geojson = this.leafletmap.latitudeLongitudeToGeoJson(latitude, longitude);
            this.leafletmap.setNewMarkerLocation(geojson);
        }
    };
    CatchAllDraftComponent.prototype.getGeoJsonFromLocation = function (value) {
        var location = this.locationService.parse(value);
        return location ? this.leafletmap.latitudeLongitudeToGeoJson(location.latitude, location.longitude) : null;
    };
    CatchAllDraftComponent.prototype.setFirstSelectItemInCombo = function (items, itemId, insertUndefinedAtZero) {
        if (insertUndefinedAtZero === void 0) { insertUndefinedAtZero = true; }
        if (this.isFormLoadingData) {
            var index = items.findIndex(function (x) { return x.id === itemId; });
            if (index > 0) { //move selected item to position 0
                items.splice(0, 0, items.splice(index, 1)[0]);
            }
        }
        else { //insert undefined item at position 0
            if (insertUndefinedAtZero) {
                items.unshift({
                    id: -1,
                    text: '',
                });
            }
        }
    };
    CatchAllDraftComponent.prototype.createUnitsListBox = function () {
        this.units = [
            { id: '0', text: 'FL' },
            { id: '1', text: 'AMSL' },
            { id: '2', text: 'AGL' },
        ];
    };
    CatchAllDraftComponent.prototype.createScopesListBox = function (entityName) {
        this.scopes = [
            { id: 'A', text: 'A' },
            { id: 'E', text: 'E' },
            { id: 'W', text: 'W' },
            { id: 'AE', text: 'AE' },
            { id: 'AW', text: 'AW' },
            { id: 'K', text: 'K' }
        ];
    };
    CatchAllDraftComponent.prototype.createSeriesListBox = function () {
        var _this = this;
        this.dataService.getSeries()
            .subscribe(function (series) {
            _this.series = series;
        });
    };
    CatchAllDraftComponent.prototype.resetIcaoSubjectChildrenControls = function () {
        this.nsdFrmGroup.patchValue({
            series: null,
            scope: null,
            itemA: null,
            lowerLimit: 0,
            upperLimit: 0,
            radius: 0,
            grpTraffic: {
                trafficI: false,
                trafficV: false,
            },
        });
        this.isTrafficDisabled = true;
    };
    CatchAllDraftComponent.prototype.resetIcaoConditionChildrenControls = function () {
        if (!this.isFormLoadingData) {
            this.nsdFrmGroup.patchValue({
                requiresItemFG: false,
                requiresPurpose: false,
                grpPurposes: {
                    purposeB: false,
                    purposeM: false,
                    purposeN: false,
                    purposeO: false,
                },
                grpItemFG: {
                    itemFValue: 0,
                    itemFSurface: false,
                    itemFUnits: 0,
                    itemGValue: 1,
                    itemGUnlimited: false,
                    itemGUnits: 0
                }
            });
        }
    };
    CatchAllDraftComponent.prototype.configureReactiveForm = function () {
        var _this = this;
        var frmArrControls = this.nsdForm.controls['frmArr'];
        this.nsdFrmGroup = this.fb.group({
            subjectId: [this.token.subjectId, [forms_1.Validators.required]],
            icaoSubjectId: [this.token.icaoSubjectId || -1, [forms_1.Validators.required]],
            icaoConditionId: [this.token.icaoConditionId || -1, [icaoConditionValidator]],
            subjectLocation: [{ value: this.token.subjectLocation, disabled: this.isReadOnly }, [forms_1.Validators.required, validateLocation(this.locationService)]],
            latitude: [{ value: this.token.latitude, disabled: this.isReadOnly }, [forms_1.Validators.required, validateLatitude(this.locationService)]],
            longitude: [{ value: this.token.longitude, disabled: this.isReadOnly }, [forms_1.Validators.required, validateLongitude(this.locationService)]],
            scope: this.token.scope,
            series: this.token.series,
            itemA: { value: this.token.itemA, disabled: this.isReadOnly },
            aerodromeName: { value: this.token.aerodromeName, disabled: this.isReadOnly },
            itemEValue: [{ value: this.token.itemEValue, disabled: this.isReadOnly }, [forms_1.Validators.required]],
            itemEFrenchValue: [{ value: this.token.itemEFrenchValue, disabled: this.isReadOnly }],
            grpPurposes: this.fb.group({
                purposeB: this.token.purposeB,
                purposeM: this.token.purposeM,
                purposeN: this.token.purposeN,
                purposeO: this.token.purposeO,
            }),
            lowerLimit: [{ value: this.token.lowerLimit, disabled: this.isReadOnly }, []],
            upperLimit: [{ value: this.token.upperLimit, disabled: this.isReadOnly }, []],
            radius: [{ value: this.token.radius, disabled: this.isReadOnly }, []],
            grpTraffic: this.fb.group({
                trafficI: this.token.trafficI,
                trafficV: this.token.trafficV,
            }),
            grpItemFG: this.fb.group({
                itemFValue: { value: this.token.itemFValue || 0, disabled: this.isReadOnly },
                itemFSurface: { value: this.token.itemFSurface, disabled: this.isReadOnly },
                itemFUnits: this.token.itemFUnits,
                itemGValue: { value: this.token.itemGValue || 1, disabled: this.isReadOnly },
                itemGUnlimited: { value: this.token.itemGUnlimited, disabled: this.isReadOnly },
                itemGUnits: this.token.itemGUnits,
            }),
            requiresItemA: this.token.requiresItemA,
            requiresSeries: this.token.requiresSeries,
            requiresScope: this.token.requiresScope,
            requiresPurpose: this.token.requiresPurpose,
            requiresItemFG: this.token.requiresItemFG
        });
        this.nsdFrmGroup.get("lowerLimit").setValidators(lowerLimitValidator(this.nsdFrmGroup.get('upperLimit')));
        this.nsdFrmGroup.get("upperLimit").setValidators(upperLimitValidator(this.nsdFrmGroup.get('lowerLimit')));
        if (this.isFormLoadingData) {
            if (this.token.requiresScope && this.token.scope) {
                this.createScopesListBox(this.token.subjectId.substring(0, 3));
                var scope = this.scopes.filter(function (item) { return item.id === _this.token.scope; });
                if (this.scopes) {
                    if (scope.length === 1) {
                        var fc = this.nsdFrmGroup.get('scope');
                        fc.setValue(scope[0].id);
                    }
                }
            }
            if (this.token.requiresSeries && this.token.series) {
                if (this.series) {
                    var series = this.series.filter(function (item) { return item.id === _this.token.series; });
                    if (series.length === 1) {
                        var fc = this.nsdFrmGroup.get('series');
                        fc.setValue(series[0].id);
                    }
                }
            }
        }
        //this.$ctrl("aerodromeName").valueChanges.debounceTime(1000).subscribe(() => this.updateIcao());
        frmArrControls.push(this.nsdFrmGroup);
    };
    CatchAllDraftComponent.prototype.updateReactiveForm = function (formArr) {
        this.nsdFrmGroup = formArr.controls[0];
        this.nsdFrmGroup.get("aerodromeName").clearValidators();
        this.nsdFrmGroup.get("itemA").clearValidators();
        this.nsdFrmGroup.get('grpTraffic').clearValidators();
        this.nsdFrmGroup.get("lowerLimit").clearValidators();
        this.nsdFrmGroup.get("upperLimit").clearValidators();
        this.nsdFrmGroup.get('grpPurposes').clearValidators();
        this.nsdFrmGroup.get('grpItemFG').clearValidators();
        //It is better to update only those controls that tokens values have changed.
        this.updateFormControlValue("subjectId", this.token.subjectId);
        this.updateFormControlValue("icaoSubjectId", this.token.icaoSubjectId);
        this.updateFormControlValue("icaoConditionId", this.token.icaoConditionId);
        this.updateFormControlValue("subjectLocation", this.token.subjectLocation);
        this.updateFormControlValue("latitude", this.token.latitude);
        this.updateFormControlValue("longitude", this.token.longitude);
        this.updateFormControlValue("scope", this.token.scope);
        this.updateFormControlValue("series", this.token.series);
        this.updateFormControlValue("itemA", this.token.itemA);
        this.updateFormControlValue("aerodromeName", this.token.aerodromeName);
        this.updateFormControlValue("itemEValue", this.token.itemEValue);
        this.updateFormControlValue("itemEFrenchValue", this.token.itemEFrenchValue);
        this.updateFormControlValue("lowerLimit", this.token.lowerLimit);
        this.updateFormControlValue("upperLimit", this.token.upperLimit);
        this.updateFormControlValue("radius", this.token.radius);
        this.updateFormControlValue("requiresItemA", this.token.requiresItemA);
        this.updateFormControlValue("requiresSeries", this.token.requiresSeries);
        this.updateFormControlValue("requiresPurpose", this.token.requiresPurpose);
        this.updateFormControlValue("requiresItemFG", this.token.requiresItemFG);
        this.updateFormControlValue("grpPurposes.purposeB", this.token.purposeB);
        this.updateFormControlValue("grpPurposes.purposeM", this.token.purposeM);
        this.updateFormControlValue("grpPurposes.purposeN", this.token.purposeN);
        this.updateFormControlValue("grpPurposes.purposeO", this.token.purposeO);
        this.updateFormControlValue("grpTraffic.trafficI", this.token.trafficI);
        this.updateFormControlValue("grpTraffic.trafficV", this.token.trafficV);
        this.updateFormControlValue("grpItemFG.itemFValue", this.token.itemFValue);
        this.updateFormControlValue("grpItemFG.itemFSurface", this.token.itemFSurface);
        this.updateFormControlValue("grpItemFG.itemFUnits", this.token.itemFUnits);
        this.updateFormControlValue("grpItemFG.itemGValue", this.token.itemGValue);
        this.updateFormControlValue("grpItemFG.itemGUnlimited", this.token.itemGUnlimited);
        this.updateFormControlValue("grpItemFG.itemGUnits", this.token.itemGUnits);
    };
    CatchAllDraftComponent.prototype.updateFormControlValue = function (controlName, value) {
        var control = this.nsdFrmGroup.get(controlName);
        if (control && control.value !== value) {
            control.setValue(value);
        }
    };
    CatchAllDraftComponent.prototype.formatDesignator = function (designator) {
        var hasNumbers = false;
        for (var i = 0; i < designator.length; i++) {
            if (parseFloat(designator[i])) {
                hasNumbers = true;
            }
        }
        return hasNumbers ? "CXXX" : designator;
    };
    CatchAllDraftComponent.prototype.designatorHasNumbers = function (designator) {
        for (var i = 0; i < designator.length; i++) {
            if (parseFloat(designator[i])) {
                return true;
            }
        }
        return false;
    };
    CatchAllDraftComponent.prototype.setBilingualRegion = function (value) {
        this.token.isBilingual = value;
        this.isBilingualRegion = value;
        var itemEFCtrl = this.nsdFrmGroup.get('itemEFrenchValue');
        if (this.isBilingualRegion || this.overrideBilingual) {
            itemEFCtrl.setValidators(forms_1.Validators.required);
            if (!this.isReadOnly) {
                itemEFCtrl.enable();
            }
        }
        else {
            itemEFCtrl.clearValidators();
            if (!this.isReadOnly) {
                itemEFCtrl.disable();
            }
        }
    };
    CatchAllDraftComponent.prototype.isAGLUnitsInUse = function () {
        if (+this.nsdFrmGroup.get('grpItemFG.itemFUnits').value === 2)
            return true;
        if (+this.nsdFrmGroup.get('grpItemFG.itemGUnits').value === 2)
            return true;
        return false;
    };
    CatchAllDraftComponent.prototype.itemFUnitChanged = function (e) {
        this.nsdFrmGroup.get('grpItemFG.itemFUnits').setValue(+e.value);
        if (this.isAGLUnitsInUse()) {
            this.nsdFrmGroup.get('lowerLimit').patchValue(this.lowerLimit);
            if (this.nsdFrmGroup.get('grpItemFG.itemFSurface').value === true)
                this.nsdFrmGroup.get('lowerLimit').disable();
            else
                this.nsdFrmGroup.get('lowerLimit').enable();
            if (this.nsdFrmGroup.get('grpItemFG.itemGUnlimited').value === true) {
                this.nsdFrmGroup.get('upperLimit').patchValue(this.upperLimit);
                this.nsdFrmGroup.get('upperLimit').disable();
            }
            else
                this.nsdFrmGroup.get('upperLimit').enable();
        }
    };
    CatchAllDraftComponent.prototype.itemFSurfaceChanged = function (checked) {
        if (checked) {
            this.nsdFrmGroup.patchValue({
                grpItemFG: {
                    itemFUnits: +this.units[0].id,
                    itemFValue: 0
                }
            });
            this.nsdFrmGroup.get('grpItemFG.itemFValue').disable();
            if (this.isAGLUnitsInUse()) {
                this.nsdFrmGroup.get('lowerLimit').patchValue(this.lowerLimit);
                this.nsdFrmGroup.get('lowerLimit').disable();
            }
        }
        else {
            this.nsdFrmGroup.get('grpItemFG.itemFValue').enable();
            if (this.isAGLUnitsInUse()) {
                this.nsdFrmGroup.get('lowerLimit').enable();
            }
        }
    };
    CatchAllDraftComponent.prototype.isLowerLimitDisabled = function () {
        if (this.nsdFrmGroup.get('lowerLimit').disabled)
            return true;
        if (this.isAGLUnitsInUse() && this.nsdFrmGroup.get('grpItemFG.itemFSurface').value === true)
            return true;
        return false;
    };
    CatchAllDraftComponent.prototype.itemGUnitChanged = function (e) {
        this.nsdFrmGroup.get('grpItemFG.itemGUnits').setValue(+e.value);
        if (this.isAGLUnitsInUse()) {
            this.nsdFrmGroup.get('upperLimit').patchValue(this.upperLimit);
            if (this.nsdFrmGroup.get('grpItemFG.itemGUnlimited').value === true)
                this.nsdFrmGroup.get('upperLimit').disable();
            else
                this.nsdFrmGroup.get('upperLimit').enable();
            if (this.nsdFrmGroup.get('grpItemFG.itemFSurface').value === true) {
                this.nsdFrmGroup.get('lowerLimit').patchValue(this.lowerLimit);
                this.nsdFrmGroup.get('lowerLimit').disable();
            }
            else
                this.nsdFrmGroup.get('lowerLimit').enable();
        }
    };
    CatchAllDraftComponent.prototype.isUpperLimitDisabled = function () {
        if (this.nsdFrmGroup.get('upperLimit').disabled)
            return true;
        if (this.isAGLUnitsInUse() && this.nsdFrmGroup.get('grpItemFG.itemGUnlimited').value === true)
            return true;
        return false;
    };
    CatchAllDraftComponent.prototype.itemGUnlimitedChanged = function (checked) {
        if (checked) {
            this.nsdFrmGroup.patchValue({
                grpItemFG: {
                    itemGUnits: +this.units[0].id,
                    itemGValue: this.upperGLimit
                }
            });
            this.nsdFrmGroup.get('grpItemFG.itemGValue').disable();
            if (this.isAGLUnitsInUse()) {
                this.nsdFrmGroup.get('upperLimit').patchValue(this.upperLimit);
                this.nsdFrmGroup.get('upperLimit').disable();
            }
        }
        else {
            this.nsdFrmGroup.get('grpItemFG.itemGValue').enable();
            if (this.isAGLUnitsInUse()) {
                this.nsdFrmGroup.get('upperLimit').enable();
            }
        }
    };
    CatchAllDraftComponent.prototype.$ctrl = function (name) {
        return this.nsdFrmGroup.get(name);
    };
    CatchAllDraftComponent.prototype.subjectLocationChanged = function (value) {
        var _this = this;
        var location = this.locationService.parse(value);
        if (location) {
            this.dataService.validateLocation(value)
                .subscribe(function (result) {
                _this.locationInDoa = result.inDoa;
                if (_this.locationInDoa) {
                    var options = { onlySelf: true, emitEvent: false };
                    _this.nsdFrmGroup.patchValue(location, options);
                    _this.updateSubjectLocationInMap(location.latitude, location.longitude);
                    _this.setBilingualRegion(result.inBilingualRegion);
                }
            });
        }
    };
    CatchAllDraftComponent.prototype.latitudeLongitudeChanged = function (value) {
        var _this = this;
        var latCtrl = this.$ctrl('latitude');
        var lngCtrl = this.$ctrl('longitude');
        if (!latCtrl.errors && !lngCtrl.errors) {
            var latitude_1 = latCtrl.value;
            var longitude_1 = lngCtrl.value;
            this.dataService.validateLocation(latitude_1 + " " + longitude_1)
                .subscribe(function (result) {
                _this.locationInDoa = result.inDoa;
                if (_this.locationInDoa) {
                    var options = { onlySelf: true, emitEvent: false };
                    _this.nsdFrmGroup.patchValue({ subjectLocation: _this.locationService.convertToDMS(latitude_1, longitude_1) }, options);
                    _this.updateSubjectLocationInMap(latitude_1, longitude_1);
                    _this.setBilingualRegion(result.inBilingualRegion);
                }
            });
            /*
            const latitude = latCtrl.value;
            const longitude = lngCtrl.value;
            const options = { onlySelf: true, emitEvent: false };
            this.nsdFrmGroup.patchValue({ subjectLocation: this.locationService.convertToDMS(latitude, longitude) }, options);
            this.updateSubjectLocationInMap(latitude, longitude);
            */
        }
    };
    CatchAllDraftComponent.prototype.itemFErrors = function () {
        if (this.nsdFrmGroup.get('grpItemFG').errors) {
            if (this.nsdFrmGroup.get('grpItemFG').errors.ItemFMinValue) {
                this.itemFErrorMessage = this.translation.translate('ErrorMsgs.ItemFMinInvalid');
                return true;
            }
            if (this.nsdFrmGroup.get('grpItemFG').errors.invalidFValue) {
                this.itemFErrorMessage = this.translation.translate('ErrorMsgs.ItemFInputInvalid');
                return true;
            }
            if (this.nsdFrmGroup.get('grpItemFG').errors.range) {
                this.itemFErrorMessage = this.translation.translate('ErrorMsgs.ItemFNotSmallerItemG');
                return true;
            }
        }
        this.itemFErrorMessage = '';
        return false;
    };
    CatchAllDraftComponent.prototype.itemGErrors = function () {
        if (this.nsdFrmGroup.get('grpItemFG').errors) {
            if (this.nsdFrmGroup.get('grpItemFG').errors.ItemGMinValue) {
                this.itemGErrorMessage = this.translation.translate('ErrorMsgs.ItemGMinInvalid');
                return true;
            }
            if (this.nsdFrmGroup.get('grpItemFG').errors.invalidGValue) {
                this.itemGErrorMessage = this.translation.translate('ErrorMsgs.ItemGInputInvalid');
                return true;
            }
            if (this.nsdFrmGroup.get('grpItemFG').errors.range) {
                this.itemGErrorMessage = this.translation.translate('ErrorMsgs.ItemGNotSmallerItemF');
                return true;
            }
        }
        this.itemGErrorMessage = '';
        return false;
    };
    CatchAllDraftComponent.prototype.toogleOverrideBilingual = function (e) {
        if (!this.isBilingualRegion) {
            this.overrideBilingual = e.target.checked;
            this.dataService.nextOverrideBilingual(this.overrideBilingual);
            if (this.overrideBilingual) {
                this.nsdFrmGroup.get('itemEFrenchValue').clearValidators();
                this.nsdFrmGroup.get('itemEFrenchValue').setValidators(forms_1.Validators.required);
                if (this.nsdFrmGroup.get('itemEFrenchValue').value) {
                    this.nsdFrmGroup.get('itemEFrenchValue').setValue(this.token.itemEFrenchValue);
                }
                else {
                    this.nsdFrmGroup.setErrors({ 'invalid': true });
                }
                this.isReadOnly ? this.nsdFrmGroup.get('itemEFrenchValue').disable() : this.nsdFrmGroup.get('itemEFrenchValue').enable();
            }
            else {
                this.nsdFrmGroup.get('itemEFrenchValue').clearValidators();
                this.nsdFrmGroup.get('itemEFrenchValue').disable();
                this.nsdFrmGroup.markAsDirty();
            }
        }
    };
    CatchAllDraftComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/dashboard/nsds/catchall/catchall-draft.component.html',
            styleUrls: ['app/dashboard/nsds/catchall/catchall-draft.component.css']
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, core_1.Injector,
            data_service_1.DataService,
            location_service_1.LocationService,
            catchall_service_1.CatchAllService,
            forms_1.FormBuilder,
            core_1.ChangeDetectorRef,
            angular_l10n_1.TranslationService])
    ], CatchAllDraftComponent);
    return CatchAllDraftComponent;
}());
exports.CatchAllDraftComponent = CatchAllDraftComponent;
//Custom Validators
function validateLocation(locationService) {
    return function (ctrl) {
        var location = ctrl.value;
        if (!location)
            return { required: true };
        if (locationService.validDMSLocation(location) === null)
            return { format: true };
        return null; //{ validatingLocation: true };
    };
}
function validateLatitude(locationService) {
    return function (ctrl) {
        if (!locationService.validLatitude(ctrl.value))
            return { format: true };
        return null;
    };
}
function validateLongitude(locationService) {
    return function (ctrl) {
        if (!locationService.validLongitude(ctrl.value))
            return { format: true };
        return null;
    };
}
function icaoConditionValidator(c) {
    if (c.value === -1) {
        return { 'required': true };
    }
    return null;
}
function radiusValidator(minRadius) {
    return function (control) {
        if (control.value === "") {
            return { 'required': true };
        }
        var v = +control.value;
        return v < minRadius ? { "invalid": true } : null;
    };
}
function lowerLimitValidator(upperLimitCtrl) {
    return function (control) {
        var v1 = +control.value;
        var v2 = +upperLimitCtrl.value;
        if (v1 === 0 && v2 === 0)
            return null;
        return v1 >= v2 ? { "invalidLowerLimitRange": true } : null;
    };
}
function upperLimitValidator(lowerLimitCtrl) {
    return function (control) {
        var v1 = +control.value;
        var v2 = +lowerLimitCtrl.value;
        return v1 <= v2 ? { "invalidUpperLimitRange": true } : null;
    };
}
function itemFValidator(comp) {
    return function (c) {
        var itemFSurface = comp.$ctrl("grpItemFG.itemFSurface").value;
        if (itemFSurface)
            return null;
        var unitsF = +comp.$ctrl('grpItemFG.itemFUnits').value;
        var unitsG = +comp.$ctrl('grpItemFG.itemGUnits').value;
        var itemFValue = comp.$ctrl("grpItemFG.itemFValue");
        var itemGValue = comp.$ctrl("grpItemFG.itemGValue");
        var itemF = 0;
        var itemG = 1;
        if (itemFValue.value === '') {
            return { 'invalidFValue': true };
        }
        if (isANumber(itemFValue.value)) {
            if (unitsF !== 2) {
                itemF = +itemFValue.value;
                var lowLimitF = (unitsF === 0 ? comp.lowerFLimit : comp.lowerFLimit * 100);
                var upperLimitF = (unitsF === 0 ? comp.upperFLimit : (comp.upperFLimit * 100) + 99);
                if (itemF < lowLimitF || itemF > upperLimitF) {
                    return { 'invalidFValue': true };
                }
            }
        }
        else {
            return { 'invalidFValue': true };
        }
        if (itemF !== 0 && unitsF !== 2) { // FL
            if (itemF < 1) {
                return { 'ItemFMinValue': true };
            }
        }
        if (isANumber(itemGValue.value) && unitsF !== 2 && unitsG !== 2) {
            itemF = (unitsF === 0 ? itemFValue.value * 100 : itemFValue.value);
            itemG = (unitsG === 0 ? itemGValue.value * 100 : itemGValue.value);
            if (+itemF >= +itemG) {
                return { 'range': true };
            }
        }
        return null;
    };
}
function itemGValidator(comp) {
    return function (c) {
        var itemGUnlimited = comp.$ctrl("grpItemFG.itemGUnlimited").value;
        if (itemGUnlimited)
            return null;
        var unitsF = +comp.$ctrl('grpItemFG.itemFUnits').value;
        var unitsG = +comp.$ctrl('grpItemFG.itemGUnits').value;
        var itemFValue = comp.$ctrl("grpItemFG.itemFValue");
        var itemGValue = comp.$ctrl("grpItemFG.itemGValue");
        var itemF = 0;
        var itemG = 1;
        if (itemGValue.value === '') {
            return { 'invalidGValue': true };
        }
        if (isANumber(itemGValue.value)) {
            if (unitsG !== 2) {
                itemG = +itemGValue.value;
                var lowLimitG = (unitsG === 0 ? comp.lowerGLimit : comp.lowerGLimit * 100);
                var upperLimitG = (unitsG === 0 ? comp.upperGLimit : (comp.upperGLimit * 100) + 99);
                if (itemG < lowLimitG || itemG > upperLimitG) {
                    return { 'invalidGValue': true };
                }
            }
        }
        else {
            return { 'invalidGValue': true };
        }
        if (itemG !== 1 && unitsG !== 2) { // FL
            if (itemG < 2) {
                return { 'ItemGMinValue': true };
            }
        }
        if (isANumber(itemFValue.value) && unitsF !== 2 && unitsG !== 2) {
            itemF = (unitsF === 0 ? itemFValue.value * 100 : itemFValue.value);
            itemG = (unitsG === 0 ? itemGValue.value * 100 : itemGValue.value);
            if (+itemF >= +itemG) {
                return { 'range': true };
            }
        }
        return null;
    };
}
function purposeAllowValidator(c) {
    var nCtrl = c.get("purposeN");
    var bCtrl = c.get("purposeB");
    var oCtrl = c.get("purposeO");
    var mCtrl = c.get("purposeM");
    var n = nCtrl.value;
    var b = bCtrl.value;
    var o = oCtrl.value;
    var m = mCtrl.value;
    if (n || b || o || m) {
        if (n && b && !o && !m)
            return null; //NB
        if (b && o && !m && !n)
            return null; //BO
        if (m && !o && !b && !n)
            return null; //M
        if (b && !o && !m && !n)
            return null; //B
        if (n && b && o && !m)
            return null; //NBO
        return { 'purposeInvalid': true };
    }
    else {
        return { 'purposeRequired': true };
    }
}
function trafficValidator(c) {
    var i = c.get("trafficI");
    var v = c.get("trafficV");
    if (i.value || v.value) {
        return null;
    }
    return { 'trafficRequired': true };
}
function itemAValidator(designator, comp) {
    return function (c) {
        if (!c.value) {
            return { 'required': true };
        }
        if (c.value === "CXXX") {
            return null;
        }
        var itemA = c.value.toUpperCase().trim();
        var parts = itemA.replace(/\s+/g, '|').split('|');
        if (parts[0] !== designator) {
            return { 'invalid_fir': true };
        }
        //Valid FIRs and valid adjacent combinations
        if (parts.length > 1) {
            var validFirs = comp.firs;
            //Reset counter
            validFirs.forEach(function (x) { return x.count = 0; });
            var _loop_1 = function (i) {
                var index = validFirs.findIndex(function (x) { return x.designator === parts[i]; });
                if (index < 0) {
                    return { value: { 'invalid_fir': true } };
                }
                else {
                    validFirs[index].count = validFirs[index].count + 1;
                }
            };
            for (var i = 0; i < parts.length; i++) {
                var state_1 = _loop_1(i);
                if (typeof state_1 === "object")
                    return state_1.value;
            }
            for (var j = 0; j < validFirs.length; j++) {
                if (validFirs[j].count > 1)
                    return { 'repeated_fir': true };
            }
        }
        var trailingSpace = itemA.replace(/\s+/g, ' ') + " ";
        if (trailingSpace.match(/^([A-Z]{4} ){1,7} *$/)) {
            return null;
        }
        return { 'invalid': true };
    };
}
function isANumber(str) {
    return !/\D/.test(str);
}
//# sourceMappingURL=catchall-draft.component.js.map
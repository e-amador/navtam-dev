﻿using System;
using System.Web;
using System.IO;
using System.Configuration;

namespace RSC.WebService
{
    public class RSCHttpModule : IHttpModule
    {
        readonly string _logPath;

        public RSCHttpModule()
        {
            _logPath = ConfigurationManager.AppSettings["TrafficLogPath"];
        }

        public void Dispose()
        {
        }

        public bool LoggingTraffic => !string.IsNullOrEmpty(_logPath);

        public void Init(HttpApplication context)
        {
            context.PreRequestHandlerExecute += Context_PreRequestHandlerExecute;
            context.PreSendRequestContent += Context_PreSendRequestContent; 
        }

        private void Context_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            if (LoggingTraffic)
            {
                var application = sender as HttpApplication;
                var context = application.Context;
                var response = context.Response;
                // Add a filter to capture response stream
                response.Filter = new ResponseSniffer(response.Filter);
            }
        }

        private void Context_PreSendRequestContent(object sender, EventArgs e)
        {
            var application = sender as HttpApplication;
            var context = application.Context;
            var response = context.Response;
            if (LoggingTraffic)
            {
                var name = GetUniqueName();

                var requestName = Path.Combine(_logPath, $"{name}-request.txt");
                File.WriteAllText(requestName, ReadStreamContextAsText(context.Request.InputStream));

                var filter = response.Filter as ResponseSniffer;
                if (filter != null)
                {
                    var reader = new StreamReader(new MemoryStream(filter.RecordStream.GetBuffer()));
                    var source = reader.ReadToEnd();

                    var responseName = Path.Combine(_logPath, Path.Combine(_logPath, $"{name}-response.txt"));
                    File.WriteAllText(responseName, source);
                }
            }
            if (response.StatusCode == 403)
            {
                response.StatusCode = 404;
            }                        
        }

        static string ReadStreamContextAsText(Stream stream)
        {
            stream.Seek(0, SeekOrigin.Begin);
            using (var reader = new StreamReader(stream))
            {
                reader.BaseStream.Seek(0, SeekOrigin.Begin);
                return reader.ReadToEnd();
            }
        }

        static string GetUniqueName()
        {
            lock (typeof(RSCHttpModule))
            {
                System.Threading.Thread.Sleep(TimeSpan.FromMilliseconds(10));
                return DateTime.UtcNow.ToString("yyMMddHHmmssfffffff");
            }
        }
    }

    public class ResponseSniffer : Stream
    {
        private readonly Stream ResponseStream;

        public MemoryStream RecordStream { get; set; }

        #region Implements of Stream
        public override bool CanRead
        {
            get { return ResponseStream.CanRead; }
        }

        public override bool CanSeek
        {
            get { return ResponseStream.CanSeek; }
        }

        public override bool CanWrite
        {
            get { return ResponseStream.CanWrite; }
        }

        public override void Flush()
        {
            ResponseStream.Flush();
        }

        public override long Length
        {
            get { return ResponseStream.Length; }
        }

        public override long Position
        {
            get
            {
                return ResponseStream.Position;
            }
            set
            {
                ResponseStream.Position = value;
            }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return ResponseStream.Read(buffer, offset, count);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            ResponseStream.SetLength(value);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            RecordStream.Write(buffer, offset, count);
            ResponseStream.Write(buffer, offset, count);
        }
        #endregion

        public ResponseSniffer(Stream stream)
        {
            RecordStream = new MemoryStream();
            ResponseStream = stream;
        }
    }
}
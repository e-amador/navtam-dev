﻿namespace RSC.Model.Enums
{
    public enum TreatmentEnum : int
    {
        LoseSand = 100,
        Unused101 = 101,
        Graded = 104,
        Packed = 106,
        Unused107 = 107,
        ChemicallyTreated = 108,
        Scarified = 109
    }
}
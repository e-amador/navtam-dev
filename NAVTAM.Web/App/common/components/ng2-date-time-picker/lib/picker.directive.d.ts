import { ElementRef, Compiler, ViewContainerRef, EventEmitter, OnInit } from '@angular/core';
export declare class DateTimePickerDirective implements OnInit {
    private compiler;
    private vcRef;
    private el;
    dateTimePicker: any;
    dateTimePickerChange: EventEmitter<any>;
    locale: string;
    viewFormat: string;
    returnObject: string;
    mode: 'popup' | 'dropdown' | 'inline';
    hourTime: '12' | '24';
    theme: 'default' | 'green' | 'teal' | 'cyan' | 'grape' | 'red' | 'gray';
    positionOffset: string;
    pickerType: 'both' | 'date' | 'time';
    private created;
    private dialog;
    constructor(compiler: Compiler, vcRef: ViewContainerRef, el: ElementRef);
    ngOnInit(): void;
    onClick(): void;
    momentChanged(value: any): void;
    private openDialog();
}

﻿using System.Collections.Generic;

namespace NavCanada.Core.Proxies.AeroRdsProxy
{
    public interface IAeroRdsFeatureProxy
    {
        T GetFeature<T>(string featureQuery);
        string GetFeatureRaw(string featureQuery);
        IAeroRDSUrlBuilder GetUrlBuilder();
    }
}

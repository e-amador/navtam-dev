﻿using RSC.Model;
using RSC.Model.Enums;
using System;
using System.Linq;
using System.Text;

namespace RSC.Business.Helpers
{
    public static class RCAMCalculation
    {
        // < xsd:enumeration value = ".13" /> The value of "depth" below is because of this enumeration value
        // despite the fact that the AC supports 1/8" which translates to 0.125
        private static readonly decimal MINIMUM_DEPTH = 0.13m;

        public static bool GetCalculationFromClearedPortion(ClearedPortion clearedPortion)
        {
            StringBuilder calculated = new StringBuilder();
            StringBuilder provided = new StringBuilder();
            foreach (var third in clearedPortion.RunwayThirds)
            {
                calculated.Append(Calculate(third, ParseTemperature(third.RwyFrictionThird?.Temperature)));
                provided.Append(third.RwyCC);
            }

            return calculated.Equals(provided);
        }

        public static ClearedSurfaceThird GetCalculationFromThird(ClearedSurfaceThird clearedSurfaceThird)
        {
            clearedSurfaceThird.NESCalculatedRwyCC = Calculate(clearedSurfaceThird, ParseTemperature(clearedSurfaceThird.RwyFrictionThird?.Temperature));

            return clearedSurfaceThird;
        }

        #region Calculations
        private static string Calculate(ClearedSurfaceThird clearedSurfaceThird, decimal? temperature)
        {
            #region Contaminants <= 25% and under 25% total coverage
            if (clearedSurfaceThird.Contaminants.Sum(p => p.ContamCoverage) <= 25) return "6";
            #endregion

            #region Contaminants with <= 25% coverage (RWYCC BASED ON HIGHER PERCENTAGE CONTAMINANT) & (RWYCC BASED ON CONTAMINANT WITH LOWER RCAM VALUE)
            if (clearedSurfaceThird.Contaminants.All(x => x.ContamCoverage <= 25m))
            {
                if (clearedSurfaceThird.Contaminants.Length > 1 && clearedSurfaceThird.Contaminants[0].ContamCoverage == clearedSurfaceThird.Contaminants[1].ContamCoverage)
                {
                    return GetLowestRcam(clearedSurfaceThird, temperature);
                }
                else
                {
                    return CalculateRWYCC(clearedSurfaceThird.Contaminants.OrderByDescending(c => c.ContamCoverage).First(), temperature).ToString();
                }
            }
            #endregion

            #region Contaminants both > 25% coverage (RWYCC BASED ON CONTAMINANT WITH LOWER RWYCC IN RCAM)
            else if (clearedSurfaceThird.Contaminants.All(x => x.ContamCoverage > 25m)) 
            {
                return GetLowestRcam(clearedSurfaceThird, temperature);
            }
            #endregion

            #region Contaminants with one > 25% & one <= 25% (RWYCC BASED ON HIGHER PERCENTAGE COVERAGE CONTAMINANT)
            else
            {
                return CalculateRWYCC(clearedSurfaceThird.Contaminants.OrderByDescending(
                    c => c.ContamCoverage).First(), temperature).ToString();
            }
            #endregion        
        }

        private static string GetLowestRcam(ClearedSurfaceThird surface, decimal? temperature)
        {
            int[] values = new int[surface.Contaminants.Length];
            for (int i = 0; i < surface.Contaminants.Length; i++)
            {
                values[i] = CalculateRWYCC(surface.Contaminants[i], temperature);
            }

            return values.Min().ToString();
        }

        private static int CalculateRWYCC(ContaminantIns contaminantIns, decimal? temperature)
        {
            if (contaminantIns.HasDepdth)
            {
                return CalculateRWYCCWithDepth(contaminantIns.ContamType, contaminantIns.ContamDepthI.ContamInches);
            }
            else
            {
                return CalculateRWYCCNoDepth(contaminantIns.ContamTypeNoD, temperature);
            }
        }

        private static int CalculateRWYCCWithDepth(ContaminantWithDepthEnum? contamType, decimal contamDepthI)
        {
            if (contamDepthI <= MINIMUM_DEPTH)
            {
                return CalculateRWYCCDepthUnder3mm(contamType);
            }
            else
            {
                return CalculateRWYCCDepthOver3mm(contamType);
            }
        }
        #endregion

        #region Codes
        private static int CalculateRWYCCDepthUnder3mm(ContaminantWithDepthEnum? contamType)
        {
            switch (contamType)
            {
                case ContaminantWithDepthEnum.DrySnowOnTopOfIce:
                case ContaminantWithDepthEnum.WetSnowOnTopOfIce:
                case ContaminantWithDepthEnum.SlushOnTopOfIce:
                case ContaminantWithDepthEnum.WaterOnTopOfCompactedSnow:
                    return 0;

                case ContaminantWithDepthEnum.StandingWater:
                    return 2;

                case ContaminantWithDepthEnum.WetSnowOnTopOfCompactedSnow:
                case ContaminantWithDepthEnum.DrySnowOnTopOfCompactedSnow:
                    return 3;

                case ContaminantWithDepthEnum.DrySnow:
                case ContaminantWithDepthEnum.WetSnow:
                case ContaminantWithDepthEnum.Slush:
                    return 5;

                case ContaminantWithDepthEnum.Unused64:
                case ContaminantWithDepthEnum.Unused69:
                case ContaminantWithDepthEnum.Unused70:
                default:
                    throw new Exception($"Unused contaminent code: {contamType}");
            }
        }

        private static int CalculateRWYCCDepthOver3mm(ContaminantWithDepthEnum? contamType)
        {
            switch (contamType)
            {
                case ContaminantWithDepthEnum.SlushOnTopOfIce:
                case ContaminantWithDepthEnum.WetSnowOnTopOfIce:
                case ContaminantWithDepthEnum.DrySnowOnTopOfIce:
                case ContaminantWithDepthEnum.WaterOnTopOfCompactedSnow:
                    return 0;

                case ContaminantWithDepthEnum.StandingWater:
                case ContaminantWithDepthEnum.Slush:
                    return 2;

                case ContaminantWithDepthEnum.DrySnow:
                case ContaminantWithDepthEnum.WetSnow:
                case ContaminantWithDepthEnum.DrySnowOnTopOfCompactedSnow:
                case ContaminantWithDepthEnum.WetSnowOnTopOfCompactedSnow:
                    return 3;

                case ContaminantWithDepthEnum.Unused64:
                case ContaminantWithDepthEnum.Unused69:
                case ContaminantWithDepthEnum.Unused70:
                default:
                    throw new Exception($"Contaminent not found: {contamType}");
            }
        }

        private static int CalculateRWYCCNoDepth(ContaminantWithNoDepthEnum? contamTypeNoD, decimal? temperature)
        { 
            switch (contamTypeNoD)
            {
                case ContaminantWithNoDepthEnum.WetIce:
                    return 0;

                case ContaminantWithNoDepthEnum.Ice:
                    return 1;

                case ContaminantWithNoDepthEnum.SlipperyWhenWet:
                    return 3;

                case ContaminantWithNoDepthEnum.CompactedSnow:
                    if (temperature.HasValue)
                    {
                        if (temperature.Value > -15m)
                            return 3;

                        return 4;
                    }
                    else
                    {
                        throw new Exception($"Temperature must be provided for Compacted Snow RWYCC calculation!");
                    }

                case ContaminantWithNoDepthEnum.Frost:
                case ContaminantWithNoDepthEnum.Wet:
                    return 5;

                case ContaminantWithNoDepthEnum.Dry:
                    return 6;

                case ContaminantWithNoDepthEnum.CompactedSnowGravelMix:
                    throw new Exception($"Unused contaminent type: {contamTypeNoD} for RWYCC conditions");

                case ContaminantWithNoDepthEnum.Unused21:
                case ContaminantWithNoDepthEnum.Unused30:
                case ContaminantWithNoDepthEnum.Unused31:
                case ContaminantWithNoDepthEnum.Unused26:
                case ContaminantWithNoDepthEnum.Unused28:
                default:
                    throw new Exception($"Unused contaminent type: {contamTypeNoD}");
            }
        }
        #endregion

        static decimal? ParseTemperature(string value)
        {
            var parsed = 0m;

            if (!string.IsNullOrEmpty(value) && decimal.TryParse(value, out parsed))
                return parsed;

            return null;
        }
    }
}
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserListComponent = void 0;
var core_1 = require("@angular/core");
var toastr_service_1 = require("./../../common/toastr.service");
var router_1 = require("@angular/router");
var data_service_1 = require("../shared/data.service");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var windowRef_service_1 = require("../../common/windowRef.service");
var angular_l10n_1 = require("angular-l10n");
var proposal_filter_component_1 = require("./proposal-filter.component");
var data_model_1 = require("../shared/data.model");
var syncFrecuency = 30; //seconds
var UserListComponent = /** @class */ (function () {
    function UserListComponent(toastr, dataService, memStorageService, router, route, locale, translation, winRef) {
        this.toastr = toastr;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.route = route;
        this.locale = locale;
        this.translation = translation;
        this.winRef = winRef;
        this.lastSyncTime = null;
        this.paging = {
            currentPage: 1,
            pageSize: 0,
            totalCount: 0,
            totalPages: 0,
        };
        this.filterPaging = {
            currentPage: 1,
            pageSize: 0,
            totalCount: 0,
            totalPages: 0,
        };
        this.filterTotalPages = 0;
        this.totalPages = 0;
        this.parentCategories = [];
        this.childCategories = [];
        this.loadingData = false;
        this.isFilterApplied = false;
        this.filterPannelOpen = false;
        this.checkPeriodicalHandler = null;
        this.intervalHandler = null;
        this.expiringCount = 0;
        this.runExpiringBellAnimation = false;
        this.expiringApplied = false;
        this.isOrgAdmin = false;
        this.isLeavingForm = false;
        this.alarms = [
            { id: 0, name: 'Bell Default', ext: 'wav' },
            { id: 1, name: 'Chime Default', ext: 'wav' },
            { id: 2, name: 'Bell Sound', ext: 'mp3' },
            { id: 3, name: 'Police Warning', ext: 'mp3' },
            { id: 4, name: 'Burglary Alarm', ext: 'mp3' },
            { id: 5, name: 'Security Alarm', ext: 'mp3' },
        ];
        this.audioElem = null;
        this.tableHeaders = {
            sorting: [
                { columnName: 'NotamId', direction: 0 },
                { columnName: 'CategoryId', direction: 0 },
                { columnName: 'ItemA', direction: 0 },
                { columnName: 'SiteID', direction: 0 },
                { columnName: 'ItemE', direction: 0 },
                { columnName: 'StartActivity', direction: 0 },
                { columnName: 'EndValidity', direction: 0 },
                { columnName: 'Estimated', direction: 0 },
                { columnName: 'Received', direction: -1 },
                { columnName: 'Operator', direction: 0 },
                { columnName: 'Originator', direction: 0 },
                { columnName: 'ModifiedByUsr', direction: 0 },
                { columnName: 'Status', direction: 0 },
            ],
            itemsPerPage: [
                { id: '10', text: '10' },
                { id: '25', text: '25' },
                { id: '50', text: '50' },
                { id: '100', text: '100' },
                { id: '1000', text: '1000' },
            ]
        };
        this.setWidthrawModifyButtonApparenance(null);
        this.paging.pageSize = +this.tableHeaders.itemsPerPage[2].id;
        this.filterPaging.pageSize = this.paging.pageSize;
        this.queryOptions = {
            page: 1,
            pageSize: this.paging.pageSize,
            sort: "_" + this.tableHeaders.sorting[8].columnName //Default Received Ascending
        };
        this.txtModify = this.translation.translate('ActionButtons.BtnModify');
        this.txtWidthdrawn = this.translation.translate('ActionButtons.BtnWidthdraw');
        this.txtWidthdrawModifyButtom = this.txtModify;
        this.isOrgAdmin = this.winRef.appConfig.isOrgAdmin;
    }
    UserListComponent.prototype.ngOnInit = function () {
        //If we want to change the default, we need to work here
        this.currentEstimatedQueueSound = this.alarms[0];
        this.categoriesData = this.route.snapshot.data['categories'];
        this.addAllToComboFilter(this.parentCategories);
        for (var i = 0; i < this.categoriesData.length; i++) {
            var category = this.categoriesData[i];
            this.addItemToComboFilter(this.parentCategories, category);
        }
        var faKey = this.memStorageService.get(this.memStorageService.FILTER_APPLIED_KEY);
        if (faKey)
            this.isFilterApplied = faKey;
        if (this.isFilterApplied) {
            this.filter.loadFilterState();
        }
        //Set latest combo box status
        var catId = this.memStorageService.get(this.memStorageService.CATEGORY_ID_KEY);
        var subCatId = this.memStorageService.get(this.memStorageService.SUB_CATEGORY_ID_KEY);
        this.categoryStartValue = catId ? catId : this.parentCategories[0].id;
        this.currentCategoryId = +this.categoryStartValue;
        if (subCatId) {
            this.childCategories = this.addChildCategoryToComboFilter(this.childCategories, this.currentCategoryId);
            this.currentSubCategoryId = subCatId;
            this.subCategoryStartValue = subCatId;
        }
        //Set latest query options
        var queryOptions = this.memStorageService.get(this.memStorageService.PROPOSAL_QUERY_OPTIONS_KEY);
        if (queryOptions) {
            this.queryOptions = queryOptions;
        }
        if (this.isFilterApplied) {
            this.filter.runFilter(false);
        }
        else {
            this.getProposalsByCategory(this.queryOptions.page);
        }
        this.checkProposalStatus();
        this.getExpiringQueueStats();
        this.checkExpireCounter();
    };
    UserListComponent.prototype.ngOnDestroy = function () {
        if (this.checkPeriodicalHandler) {
            clearInterval(this.checkPeriodicalHandler);
        }
        if (this.intervalHandler) {
            window.clearInterval(this.intervalHandler);
        }
    };
    UserListComponent.prototype.ngAfterViewInit = function () {
        this.filter.parent = this;
    };
    Object.defineProperty(UserListComponent.prototype, "langCulture", {
        get: function () {
            return (window["app"].cultureInfo || "en").toUpperCase();
        },
        enumerable: false,
        configurable: true
    });
    UserListComponent.prototype.changeRoute = function (routeValue) {
        var _this = this;
        this.isLeavingForm = true;
        this.router.navigate(routeValue).catch(function () {
            _this.isLeavingForm = false;
            _this.toastr.error("This action is not allowed at this time.", "Action failed!", { 'positionClass': 'toast-bottom-right' });
        });
    };
    UserListComponent.prototype.getItemEString = function (proposal, history) {
        var maxsize = history ? 90 : 110;
        var result = proposal.itemE.substring(0, maxsize);
        if (proposal.itemE.length > maxsize)
            result += '...';
        return result;
    };
    UserListComponent.prototype.getOriginatorString = function (proposal, history) {
        var maxsize = history ? 10 : 10;
        var result = proposal.originator.substring(0, maxsize);
        if (proposal.originator.length > maxsize)
            result += '...';
        return result;
    };
    UserListComponent.prototype.getOperatorString = function (proposal) {
        if (proposal.modifiedByUsr === null || proposal.modifiedByUsr.length === 0)
            return proposal.operator;
        else
            return proposal.modifiedByUsr;
    };
    UserListComponent.prototype.getApproverString = function (proposal) {
        if (proposal.status === data_model_1.ProposalStatus.Disseminated ||
            proposal.status === data_model_1.ProposalStatus.DisseminatedModified ||
            proposal.status === data_model_1.ProposalStatus.Rejected)
            return proposal.modifiedByUsr;
        else
            return '';
    };
    UserListComponent.prototype.checkExpireCounter = function () {
        var self = this;
        this.intervalHandler = window.setInterval(function () {
            self.getExpiringQueueStats.call(self);
        }, syncFrecuency * 1000);
    };
    UserListComponent.prototype.getExpiringQueueStats = function () {
        var _this = this;
        try {
            this.dataService.getExpiringTotal()
                .subscribe(function (count) {
                var self = _this;
                var shouldPlaySound = false;
                if (count != _this.expiringCount) {
                    if (count > _this.expiringCount) {
                        shouldPlaySound = true;
                        self.audioElem = document.getElementById('estimated-queue-sound');
                        self.audioElem.load();
                        self.audioElem.play();
                    }
                    _this.expiringCount = count;
                    self.runExpiringBellAnimation = true;
                    setTimeout(function () {
                        self.runExpiringBellAnimation = false;
                        if (shouldPlaySound) {
                            self.audioElem.pause();
                            self.audioElem.currentTime = 0;
                            self.audioElem = null;
                        }
                    }, 10000);
                    if (_this.expiringApplied)
                        _this.getProposalsByCategory(_this.queryOptions.page);
                }
                if ((_this.expiringCount === 0 && _this.expiringApplied)) {
                    _this.executeFilter();
                }
            });
        }
        catch (e) {
        }
    };
    UserListComponent.prototype.executeFilter = function () {
        this.expiringApplied = !this.expiringApplied;
        if (this.expiringApplied) {
            this.getProposalsByCategory(this.queryOptions.page);
        }
        else {
            this.changeCategory(this.parentCategories[0].id);
        }
    };
    UserListComponent.prototype.runCloneAction = function () {
        if (this.selectedProposal) {
            this.isLeavingForm = true;
            this.router.navigate(["/dashboard/clone", this.selectedProposal.id, this.isFilterApplied]);
        }
    };
    UserListComponent.prototype.runClearFilter = function ($event) {
        this.filter.clearFilter($event, false);
        this.changeCategory(this.parentCategories[0].id);
    };
    UserListComponent.prototype.onKeyup = function ($event) {
        switch ($event.code) {
            case "ArrowUp": //NAVIGATE TABLE ROW UP
                if (this.selectedProposal.index > 0) {
                    this.selectProposal(this.proposals[this.selectedProposal.index - 1]);
                }
                break;
            case "ArrowDown": //NAVIGATE TABLE ROW DOWN
                if (this.selectedProposal.index < this.proposals.length - 1) {
                    this.selectProposal(this.proposals[this.selectedProposal.index + 1]);
                }
                break;
            case "KeyV": //ALT-V: REVIEW
                if ($event.altKey && this.selectedProposal) {
                    this.reviewProposal();
                }
                break;
            case "Enter": //ENTER: MODIFY/WIDTHRAW
                if (!this.isModifyButtonDisabled() && this.selectedProposal) {
                    this.router.navigate(["/dashboard/edit", this.selectedProposal.id]);
                }
                break;
            case "KeyM": //ALT-M: MODIFY/WIDTHRAW
                if ($event.altKey) {
                    if (!this.isModifyButtonDisabled() && this.selectedProposal) {
                        this.router.navigate(["/dashboard/edit", this.selectedProposal.id]);
                    }
                }
                break;
            case "KeyH": //ALT-H: HISTORY
                if ($event.altKey && this.selectedProposal && !this.isHistoryButtonDisabled()) {
                    this.showHistory(this.selectedProposal.id);
                }
                break;
            case "KeyZ": //ALT-Z: DISCARD
                debugger;
                if ($event.altKey && this.selectedProposal) {
                    if (!this.isDiscardButtonDisabled(this.selectedProposal.status)) {
                        this.confirmDiscardProposal();
                    }
                }
                break;
            case "KeyC":
                if ($event.altKey && this.selectedProposal) { //ALT-C CLONE PROPOSAL
                    this.router.navigate(["/dashboard/clone", this.selectedProposal.id, this.isFilterApplied]);
                }
                else { //CTRL-C START NOTAM CREATION
                    if ($event.ctrlKey) {
                        this.frsPrevKeyCodeEvent = $event;
                    }
                    else {
                        if (this.frsPrevKeyCodeEvent) { //CTRL-C C: CATCHALL
                            this.currentCategoryId = data_model_1.NsdCategory.CatchAll;
                            this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                        }
                    }
                }
                break;
            case "KeyO": //CTRL-C O: OBSTACLE
                if (this.frsPrevKeyCodeEvent) {
                    this.sndPrevKeyCodeEvent = $event;
                }
                break;
            case "KeyR": //CTRL-C R: RUNWAY
                if (this.frsPrevKeyCodeEvent) {
                    this.sndPrevKeyCodeEvent = $event;
                }
                break;
            case "KeyT": // CTRL-C T: Taxiway
                if (this.frsPrevKeyCodeEvent) {
                    this.sndPrevKeyCodeEvent = $event;
                }
                break;
            case "KeyA": // CTRL-C T:
                if (this.frsPrevKeyCodeEvent) {
                    this.sndPrevKeyCodeEvent = $event;
                }
                break;
            case "Digit1":
                //CTRL-C O 1: OBST
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyO") {
                    this.currentCategoryId = data_model_1.NsdCategory.Obst;
                    this.currentSubCategoryId = 4;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                // CTRL-C R 1: RWY CLOSED
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyR") {
                    this.currentCategoryId = data_model_1.NsdCategory.RwyClsd;
                    this.currentSubCategoryId = 7;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                //CTRL-C T 1: TWY CLOSED
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyT") {
                    this.currentCategoryId = data_model_1.NsdCategory.TwyClsd;
                    this.currentSubCategoryId = 10;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                //CTRL-C A 1: CARS CLOSED
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyA") {
                    this.currentCategoryId = data_model_1.NsdCategory.CarsClsd;
                    this.currentSubCategoryId = 13;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                break;
            case "Digit2":
                //CTRL-C O 2: OBST LGT U/S
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyO") {
                    this.currentCategoryId = data_model_1.NsdCategory.ObstLGT;
                    this.currentSubCategoryId = 5;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                break;
            case "Digit3":
                //CTRL-C O 3: MULTI OBST
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyO") {
                    this.currentCategoryId = data_model_1.NsdCategory.MultiObst;
                    this.currentSubCategoryId = 14;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                break;
            case "Digit4":
                //CTRL-C O 3: MULTI OBST LGT U/S
                if (this.sndPrevKeyCodeEvent && this.sndPrevKeyCodeEvent.code === "KeyO") {
                    this.currentCategoryId = data_model_1.NsdCategory.MultiObstLGT;
                    this.currentSubCategoryId = 15;
                    this.router.navigate(["/dashboard/create", this.resolveCategoryId()]);
                }
                break;
        }
    };
    UserListComponent.prototype.onFilterChanged = function (event) {
        if (event.error) {
            this.toastr.error("Filter error: " + event.error.message, "", { 'positionClass': 'toast-bottom-right' });
        }
        else {
            this.isFilterApplied = event.isFilterApplied;
            this.filterPannelOpen = false;
            if (this.isFilterApplied) {
                this.processProposals(event.data);
            }
            else {
                this.getProposalsByCategory(this.queryOptions.page);
            }
        }
        this.loadingData = false;
    };
    UserListComponent.prototype.onCategoryChanged = function (e) {
        this.changeCategory(e.value);
    };
    UserListComponent.prototype.onChildCategoryChanged = function (e) {
        this.changeChildCategory(e.value);
    };
    UserListComponent.prototype.reviewProposal = function () {
        this.isLeavingForm = true;
        if (this.isFilterApplied) {
            this.router.navigate(["/dashboard/viewhistory", this.selectedProposal.id]);
        }
        else {
            this.router.navigate(["/dashboard/view", this.selectedProposal.id]);
        }
    };
    UserListComponent.prototype.sort = function (index) {
        var dir = this.tableHeaders.sorting[index].direction;
        for (var i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }
        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            if (this.isFilterApplied) {
                this.filter.changeFilterSort('_' + this.tableHeaders.sorting[index].columnName);
            }
            else {
                this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
            }
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            if (this.isFilterApplied) {
                this.filter.changeFilterSort(this.tableHeaders.sorting[index].columnName);
            }
            else {
                this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
            }
        }
        if (this.isFilterApplied) {
            this.loadingData = true;
            this.filter.runFilter(false, 1);
        }
        else {
            this.getProposalsByCategory(this.queryOptions.page);
        }
    };
    UserListComponent.prototype.sortingClass = function (index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';
        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';
        return 'sorting';
    };
    UserListComponent.prototype.confirmDiscardProposal = function () {
        $('#modal-delete').modal({});
    };
    UserListComponent.prototype.discardProposal = function () {
        var _this = this;
        if (this.selectedProposal) {
            this.isLeavingForm = true;
            this.dataService.discardProposal(this.selectedProposal.id)
                .subscribe(function (r) {
                var msg = _this.translation.translate('Dashboard.MessageDiscardSuccess');
                _this.getProposalsByCategory(_this.queryOptions.page);
                _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            }, function (error) {
                var msg = _this.translation.translate('Dashboard.MessageDiscardSuccess');
                _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
                _this.isLeavingForm = false;
            });
        }
    };
    UserListComponent.prototype.resolveCategoryId = function () {
        if (this.childCategories.length > 0) {
            return this.currentSubCategoryId !== data_model_1.NsdCategory.All ? this.currentSubCategoryId : this.currentCategoryId;
        }
        return this.currentCategoryId;
    };
    UserListComponent.prototype.isCreateButtonDisable = function () {
        if (this.isLeavingForm || this.isFilterApplied) {
            return true;
        }
        return false;
    };
    UserListComponent.prototype.isModifyButtonDisabled = function () {
        if (this.isLeavingForm) {
            return true;
        }
        if (!this.selectedProposal) {
            return true;
        }
        if (this.isFilterApplied) {
            return true;
        }
        if (this.selectedProposal.seriesChecklist) {
            return true;
        }
        return this.selectedProposal.status === data_model_1.ProposalStatus.Disseminated ||
            this.selectedProposal.status === data_model_1.ProposalStatus.Terminated ||
            this.selectedProposal.status === data_model_1.ProposalStatus.DisseminatedModified ||
            this.selectedProposal.status === data_model_1.ProposalStatus.Picked ||
            this.selectedProposal.status === data_model_1.ProposalStatus.Parked ||
            this.selectedProposal.status === data_model_1.ProposalStatus.ParkedPicked ||
            this.selectedProposal.status === data_model_1.ProposalStatus.ParkedDraft;
    };
    UserListComponent.prototype.isReviewButtonDisabled = function () {
        if (this.isLeavingForm) {
            return true;
        }
        if (!this.selectedProposal) {
            return true;
        }
        if (this.selectedProposal.seriesChecklist) {
            return true;
        }
        return false;
    };
    UserListComponent.prototype.isReplaceButtonDisabled = function () {
        if (this.isLeavingForm) {
            return true;
        }
        if (!this.selectedProposal) {
            return true;
        }
        if (this.isFilterApplied) {
            return true;
        }
        if (this.selectedProposal.seriesChecklist) {
            return true;
        }
        if (this.selectedProposal.grouped && !this.selectedProposal.groupId) {
            return true;
        }
        if (this.selectedProposal.isObsolete)
            return true;
        if (this.selectedProposal.status === data_model_1.ProposalStatus.Terminated)
            return true;
        if (this.selectedProposal.status !== data_model_1.ProposalStatus.Disseminated && this.selectedProposal.status !== data_model_1.ProposalStatus.DisseminatedModified) {
            return true;
        }
        return false;
    };
    UserListComponent.prototype.isCancelButtonDisabled = function () {
        if (this.isLeavingForm) {
            return true;
        }
        if (!this.selectedProposal) {
            return true;
        }
        if (this.isFilterApplied) {
            return true;
        }
        if (this.selectedProposal.seriesChecklist) {
            return true;
        }
        if (this.selectedProposal.grouped && !this.selectedProposal.groupId) {
            return true;
        }
        if (this.selectedProposal.isObsolete)
            return true;
        if (this.selectedProposal.proposalType === data_model_1.ProposalType.Cancel ||
            this.selectedProposal.proposalType === data_model_1.ProposalStatus.Expired ||
            this.selectedProposal.status === data_model_1.ProposalStatus.Terminated) {
            return true;
        }
        if (this.selectedProposal.status !== data_model_1.ProposalStatus.Disseminated && this.selectedProposal.status !== data_model_1.ProposalStatus.DisseminatedModified) {
            return true;
        }
        return false;
    };
    UserListComponent.prototype.isHistoryButtonDisabled = function () {
        if (!this.selectedProposal) {
            return true;
        }
        if (this.selectedProposal.seriesChecklist) {
            return true;
        }
        return this.isFilterApplied || !this.selectedProposal;
    };
    UserListComponent.prototype.isCloneButtonDisabled = function () {
        if (this.isLeavingForm) {
            return true;
        }
        if (this.isFilterApplied) {
            return false;
        }
        if (!this.selectedProposal) {
            return true;
        }
        if (this.selectedProposal.seriesChecklist) {
            return true;
        }
        if (this.selectedProposal.proposalType === data_model_1.ProposalType.Cancel) {
            return true;
        }
        return false;
    };
    UserListComponent.prototype.checkProposalStatus = function () {
        var self = this;
        this.checkPeriodicalHandler = window.setInterval(function () {
            if (self.proposals !== undefined && !self.isFilterApplied) {
                if (self.proposals.length > 0) {
                    //let proposalsIds = self.proposals.filter(p => p.status !== ProposalStatus.Draft && !p.grouped)
                    //    .map(function (p: IProposal) {
                    //        return p.id;
                    //    });
                    var proposalsIds = self.proposals.filter(function (p) { return !p.grouped; })
                        .map(function (p) {
                        return p.id;
                    });
                    self.dataService.getProposalStatus(proposalsIds, syncFrecuency, self.lastSyncTime)
                        .subscribe(function (response) {
                        var proposalStatus = response.proposalStatus;
                        self.lastSyncTime = response.lastSyncTime;
                        for (var _i = 0, proposalStatus_1 = proposalStatus; _i < proposalStatus_1.length; _i++) {
                            var p = proposalStatus_1[_i];
                            self.updateProposalStatus(p);
                        }
                        self.setWidthrawModifyButtonApparenance(self.selectedProposal);
                    });
                }
            }
        }, syncFrecuency * 1000);
    };
    UserListComponent.prototype.updateProposalStatus = function (data) {
        if (data) {
            var index = this.proposals.findIndex(function (x) { return x.id === data.id; });
            if (index >= 0) {
                if (data.status === data_model_1.ProposalStatus.Deleted || data.status === data_model_1.ProposalStatus.Discarded ||
                    data.status === data_model_1.ProposalStatus.Expired || data.status === data_model_1.ProposalStatus.Terminated) {
                    this.deleteRow(index);
                }
                else {
                    var $rowElem = $("#p_" + data.id);
                    var iniBgColorClass = this.rowColor(this.proposals[index]);
                    this.proposals[index].notamId = data.notamId;
                    if (data.startActivity) {
                        this.proposals[index].startActivity = data.startActivity;
                    }
                    this.proposals[index].endValidity = data.endValidity; //clean endValidity when cancellation...
                    this.proposals[index].status = data.status;
                    this.proposals[index].soonToExpire = data.soonToExpire;
                    this.proposals[index].modifiedByUsr = data.modifiedByUsr;
                    this.proposals[index].originator = data.originator;
                    this.proposals[index].operator = data.operator;
                    this.proposals[index].itemE = data.itemE;
                    this.proposals[index].itemA = data.itemA;
                    this.proposals[index].siteId = data.siteId;
                    this.proposals[index].estimated = data.estimated;
                    var endBgColorClass = this.rowColor(this.proposals[index]);
                    if (iniBgColorClass !== endBgColorClass) {
                        $rowElem.removeClass(iniBgColorClass, { duration: 500 });
                    }
                    ////when cancellation
                    //if (data.endValidity === "") {
                    //    this.proposals[index].endValidity = data.endValidity; //clean endValidity when cancellation...
                    //}
                    //if (data.startActivity) {
                    //    this.proposals[index].startActivity = data.startActivity;
                    //}
                    if (iniBgColorClass !== endBgColorClass) {
                        $rowElem.addClass(endBgColorClass, { duration: 500 });
                    }
                }
            }
        }
    };
    UserListComponent.prototype.deleteRow = function (index) {
        this.proposals.splice(index, 1);
        var selectedIndex = this.proposals.findIndex(function (x) { return x.isSelected; });
        if (this.proposals.length > 0) {
            this.selectedProposal = this.proposals[0];
            if (selectedIndex >= 0 && this.selectedProposal.id !== selectedIndex) {
                this.proposals[selectedIndex].isSelected = false;
            }
            this.selectedProposal.isSelected = true;
        }
    };
    UserListComponent.prototype.cssIcon = function (proposal) {
        switch (proposal.status) {
            case data_model_1.ProposalStatus.Draft: return 'fa-save';
            case data_model_1.ProposalStatus.Submitted: return 'fa-upload';
            case data_model_1.ProposalStatus.Withdrawn: return 'fa-download';
            case data_model_1.ProposalStatus.Picked: return 'fa-hourglass-half';
            case data_model_1.ProposalStatus.Expired: return 'fa-clock-o';
            case data_model_1.ProposalStatus.Replaced: return 'fa-code-fork';
            case data_model_1.ProposalStatus.Parked: return 'fa-lock';
            case data_model_1.ProposalStatus.ParkedDraft: return 'fa-lock';
            case data_model_1.ProposalStatus.ParkedPicked: return 'fa-lock';
            case data_model_1.ProposalStatus.Disseminated: return 'fa-wifi';
            case data_model_1.ProposalStatus.DisseminatedModified: return 'fa-wifi';
            case data_model_1.ProposalStatus.Cancelled: return 'fa-hand-paper-o';
            case data_model_1.ProposalStatus.Discarded: return 'fa-trash';
            case data_model_1.ProposalStatus.Rejected: return 'fa-ban';
            case data_model_1.ProposalStatus.Terminated: return 'fa-hand-paper-o';
            default: return 'fa-save';
        }
    };
    UserListComponent.prototype.rowColor = function (proposal) {
        if (proposal.grouped) {
            return "bg-rejected";
        }
        if (proposal.soonToExpire) {
            return 'bg-soontoexpire';
        }
        switch (proposal.status) {
            case data_model_1.ProposalStatus.Draft: return 'bg-saved';
            case data_model_1.ProposalStatus.Submitted: return 'bg-submitted';
            case data_model_1.ProposalStatus.Withdrawn: return 'bg-widthdrawn';
            case data_model_1.ProposalStatus.Picked: return 'bg-picked';
            case data_model_1.ProposalStatus.Parked: return 'bg-picked';
            case data_model_1.ProposalStatus.ParkedDraft: return 'bg-picked';
            case data_model_1.ProposalStatus.ParkedPicked: return 'bg-picked';
            case data_model_1.ProposalStatus.Replaced: return 'bg-replace';
            case data_model_1.ProposalStatus.Rejected: return 'bg-rejected fg-white';
            case data_model_1.ProposalStatus.Expired: return 'bg-expired';
            case data_model_1.ProposalStatus.Cancelled: return 'bg-cancel';
            case data_model_1.ProposalStatus.Terminated: return 'bg-cancel';
            case data_model_1.ProposalStatus.DisseminatedModified: return 'c-orange';
            default: return '';
        }
    };
    UserListComponent.prototype.isDiscardButtonDisabled = function (proposalStatus) {
        if (!this.selectedProposal) {
            return true;
        }
        return this.isFilterApplied ||
            proposalStatus === data_model_1.ProposalStatus.Picked ||
            proposalStatus === data_model_1.ProposalStatus.Submitted ||
            proposalStatus === data_model_1.ProposalStatus.Withdrawn ||
            proposalStatus === data_model_1.ProposalStatus.Disseminated ||
            proposalStatus === data_model_1.ProposalStatus.ParkedDraft ||
            proposalStatus === data_model_1.ProposalStatus.ParkedPicked ||
            proposalStatus === data_model_1.ProposalStatus.Parked ||
            proposalStatus === data_model_1.ProposalStatus.Terminated ||
            proposalStatus === data_model_1.ProposalStatus.DisseminatedModified;
    };
    UserListComponent.prototype.itemsPerPageChanged = function (e) {
        if (this.isFilterApplied) {
            this.loadingData = true;
            this.filter.runFilter(false, 1, e.value);
        }
        else {
            this.queryOptions.pageSize = e.value;
            this.getProposalsByCategory(data_model_1.NsdCategory.All);
        }
    };
    UserListComponent.prototype.selectProposal = function (proposal) {
        if (this.selectedProposal) {
            this.selectedProposal.isSelected = false;
        }
        this.selectedProposal = proposal;
        this.selectedProposal.isSelected = true;
        this.setWidthrawModifyButtonApparenance(proposal);
        this.memStorageService.save(this.memStorageService.PROPOSAL_ID_KEY, this.selectedProposal.id);
    };
    UserListComponent.prototype.showGroup = function (rowIndex) {
        var _this = this;
        var pos = rowIndex + 1;
        if (this.proposals[rowIndex].groupOpen) {
            if (this.proposals[rowIndex].group.length > 0) {
                this.proposals.splice(pos, this.proposals[rowIndex].group.length);
            }
            this.proposals[rowIndex].groupOpen = false;
        }
        else {
            this.proposals[rowIndex].groupOpen = true;
        }
        this.proposals[rowIndex].group = [];
        if (this.proposals[rowIndex].groupOpen) {
            this.dataService.getGroupedProposals(this.proposals[rowIndex].groupId)
                .subscribe(function (proposals) {
                if (proposals.length > 0) {
                    for (var i = 0; i < proposals.length; i++) {
                        proposals[i].isSelected = false;
                        proposals[i].grouped = true;
                        proposals[i].groupId = null;
                        _this.proposals[rowIndex].group.push(proposals[i]);
                    }
                    var pos_1 = rowIndex + 1;
                    if (_this.proposals[rowIndex].group) {
                        var isLastRow = _this.proposals.length === pos_1;
                        for (var i = 0; i < _this.proposals[rowIndex].group.length; i++) {
                            var pg = _this.proposals[rowIndex].group[i];
                            if (isLastRow) {
                                _this.proposals.push(pg);
                            }
                            else {
                                _this.proposals.splice(pos_1 + i, 0, pg);
                            }
                        }
                    }
                }
            });
        }
    };
    UserListComponent.prototype.modifyOrWithdrawProposal = function (proposal) {
        this.selectProposal(proposal);
        if (proposal.grouped) {
            return;
        }
        if (!this.isModifyButtonDisabled()) {
            this.isLeavingForm = true;
            this.memStorageService.save(this.memStorageService.MODIFY_ACTION_KEY, this.txtWidthdrawModifyButtom === this.txtModify ? 0 : 1);
            if (this.selectedProposal.categoryId === 18) {
                this.changeRoute(["/dashboard/edit/rsc", this.selectedProposal.id]);
            }
            else {
                this.changeRoute(["/dashboard/edit", this.selectedProposal.id]); //this.router.navigate(["/dashboard/edit", this.selectedProposal.id]).catch;
            }
        }
    };
    UserListComponent.prototype.onPageChange = function (page) {
        this.getProposalsByCategory(page);
    };
    UserListComponent.prototype.onFilterPageChange = function (page) {
        this.loadingData = true;
        this.filter.runFilter(false, page);
    };
    UserListComponent.prototype.showHistory = function (proposalId) {
        var _this = this;
        $('#proposal-history').modal({});
        this.dataService.getProposalHistory(proposalId)
            .subscribe(function (proposals) {
            if (proposals.length > 0) {
                _this.proposalHistory = proposals;
                _this.proposalHistory[0].isSelected = true;
            }
            else {
                _this.proposalHistory = [];
            }
        });
    };
    UserListComponent.prototype.isNSDAllowed = function (catName) {
        var id = this.categoriesData.findIndex(function (x) { return x.name === catName; });
        if (id === -1)
            return false;
        return true;
    };
    UserListComponent.prototype.changeCategory = function (catId) {
        this.currentCategoryId = +catId;
        this.childCategories = this.addChildCategoryToComboFilter(this.childCategories, this.currentCategoryId);
        var queryOptions = this.memStorageService.get(this.memStorageService.PROPOSAL_QUERY_OPTIONS_KEY);
        this.memStorageService.save(this.memStorageService.CATEGORY_ID_KEY, this.currentCategoryId);
        this.memStorageService.save(this.memStorageService.SUB_CATEGORY_ID_KEY, data_model_1.NsdCategory.All); //set all by default
        if (!this.expiringApplied)
            this.getProposalsByCategory(data_model_1.NsdCategory.All);
    };
    UserListComponent.prototype.changeChildCategory = function (subCatId) {
        this.currentSubCategoryId = +subCatId;
        this.memStorageService.save(this.memStorageService.SUB_CATEGORY_ID_KEY, this.currentSubCategoryId);
        this.getProposalsByCategory(data_model_1.NsdCategory.All);
    };
    UserListComponent.prototype.getProposalsByCategory = function (page) {
        var _this = this;
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            if (this.isFilterApplied) {
                this.filter.runFilter(false, page, this.queryOptions.pageSize);
            }
            else if (this.expiringApplied) {
                this.dataService.getExpiringProposals(this.queryOptions)
                    .subscribe(function (proposals) {
                    _this.processProposals(proposals);
                });
            }
            else {
                this.memStorageService.save(this.memStorageService.PROPOSAL_QUERY_OPTIONS_KEY, this.queryOptions);
                if (this.childCategories.length > 0) {
                    if (this.currentSubCategoryId !== data_model_1.NsdCategory.All) {
                        this.dataService.getProposalsByCategory(this.currentSubCategoryId, this.queryOptions)
                            .subscribe(function (proposals) {
                            _this.processProposals(proposals);
                        });
                    }
                    else {
                        this.dataService.getProposalsByParentCategory(this.currentCategoryId, this.queryOptions)
                            .subscribe(function (proposals) {
                            _this.processProposals(proposals);
                        });
                    }
                }
                else {
                    this.dataService.getProposalsByCategory(this.currentCategoryId, this.queryOptions)
                        .subscribe(function (proposals) {
                        _this.processProposals(proposals);
                    });
                }
            }
        }
        catch (e) {
            this.loadingData = false;
        }
    };
    UserListComponent.prototype.processProposals = function (proposals) {
        if (this.isFilterApplied) {
            this.filterPaging = proposals.paging;
            this.filterTotalPages = proposals.paging.totalPages;
        }
        this.paging = proposals.paging;
        this.totalPages = proposals.paging.totalPages;
        for (var iter = 0; iter < proposals.data.length; iter++)
            proposals.data[iter].index = iter;
        var lastProposalSelected = this.memStorageService.get(this.memStorageService.PROPOSAL_ID_KEY);
        if (proposals.data.length > 0) {
            this.proposals = proposals.data;
            var index = 0;
            if (lastProposalSelected) {
                index = this.proposals.findIndex(function (x) { return x.id === lastProposalSelected; });
                if (index < 0)
                    index = 0; // when not found first should be selected;
            }
            this.proposals[index].isSelected = true;
            this.selectedProposal = this.proposals[index];
            this.setWidthrawModifyButtonApparenance(this.selectedProposal);
            this.loadingData = false;
        }
        else {
            this.proposals = [];
            this.selectedProposal = null;
            this.loadingData = false;
        }
    };
    UserListComponent.prototype.addItemToComboFilter = function (comboData, category) {
        comboData.push({
            groupCategoryId: category.groupCategoryId,
            id: category.id,
            isSelected: category.isSelected,
            name: category.name,
            text: category.name
        });
    };
    UserListComponent.prototype.setWidthrawModifyButtonApparenance = function (proposal) {
        proposal = proposal || { status: data_model_1.ProposalStatus.Draft };
        var isButtonTextModified = proposal.status === data_model_1.ProposalStatus.Draft ||
            proposal.status === data_model_1.ProposalStatus.Withdrawn ||
            proposal.status === data_model_1.ProposalStatus.Replaced ||
            proposal.status === data_model_1.ProposalStatus.Rejected ||
            proposal.status === data_model_1.ProposalStatus.Cancelled;
        this.txtWidthdrawModifyButtom = isButtonTextModified ? this.txtModify : this.txtWidthdrawn;
        this.iconWidthdrawModifyButtom = isButtonTextModified ? "fa-pencil-square-o" : "fa-download";
    };
    UserListComponent.prototype.addChildCategoryToComboFilter = function (comboData, currentCategoryId) {
        comboData = [];
        var categoryIndex = this.categoriesData.findIndex(function (x) { return x.id === currentCategoryId; });
        if (categoryIndex >= 0) {
            if (this.categoriesData[categoryIndex].children) {
                this.addAllToComboFilter(comboData);
                for (var i = 0; i < this.categoriesData[categoryIndex].children.length; i++) {
                    var subCategory = this.categoriesData[categoryIndex].children[i];
                    this.addItemToComboFilter(comboData, subCategory);
                }
            }
        }
        this.currentSubCategoryId = data_model_1.NsdCategory.All;
        return comboData;
    };
    UserListComponent.prototype.addAllToComboFilter = function (comboData) {
        comboData.push({
            groupCategoryId: 0,
            id: 1,
            isSelected: true,
            name: 'Root',
            text: "ALL"
        });
    };
    __decorate([
        core_1.ViewChild(proposal_filter_component_1.ProposalFilterComponent),
        __metadata("design:type", proposal_filter_component_1.ProposalFilterComponent)
    ], UserListComponent.prototype, "filter", void 0);
    __decorate([
        core_1.HostListener('window:keyup', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], UserListComponent.prototype, "onKeyup", null);
    UserListComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/dashboard/user/user-list.component.html',
            styleUrls: ['app/dashboard/user/user-list.component.css']
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            router_1.ActivatedRoute,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService,
            windowRef_service_1.WindowRef])
    ], UserListComponent);
    return UserListComponent;
}());
exports.UserListComponent = UserListComponent;
//# sourceMappingURL=user-list.component.js.map
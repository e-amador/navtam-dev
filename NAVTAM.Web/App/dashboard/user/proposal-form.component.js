"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var moment = require("moment");
var ProposalFormComponent = (function () {
    function ProposalFormComponent(fb, router) {
        this.fb = fb;
        this.router = router;
        this.tabActive = 0;
        this.componentData = null;
    }
    ProposalFormComponent.prototype.ngOnInit = function () {
        debugger;
        this.configureReactiveForm();
        this.componentData = {
            catId: this.model.categoryId,
            version: this.model.version,
            inputs: {
                isBilingualRegion: this.model.isBilingualRegion,
                form: this.proposalForm,
                token: this.model.token //passing the model}
            }
        };
    };
    ProposalFormComponent.prototype.activateTab = function (tab) {
        switch (tab) {
            case 'map':
                this.tabActive = 0;
                break;
            case 'icao':
                this.tabActive = 1;
                break;
            default:
                this.tabActive = 1;
        }
    };
    ProposalFormComponent.prototype.backToProposals = function () {
        var bmap = window['app'].bmap;
        if (bmap) {
            bmap.dispose();
        }
        this.router.navigate(['/userdashboard']);
    };
    ProposalFormComponent.prototype.setStartActivityDate = function (date) {
        var momentDate = moment(date);
        if (momentDate.isValid()) {
            this.proposalForm.patchValue({ grpDatePeriod: { startActivity: momentDate.format("L, LT") } });
            this.proposalForm.get('grpDatePeriod.startActivity').markAsDirty();
        }
    };
    ProposalFormComponent.prototype.setEndValidityDate = function (date) {
        var momentDate = moment(date);
        if (momentDate.isValid()) {
            this.proposalForm.patchValue({ grpDatePeriod: { endValidity: momentDate.format("L, LT") } });
            this.proposalForm.get('grpDatePeriod.endValidity').markAsDirty();
        }
    };
    ProposalFormComponent.prototype.immediateChanged = function (checked) {
        if (checked) {
            this.proposalForm.patchValue({ grpDatePeriod: { startActivity: null } });
            this.proposalForm.get("grpDatePeriod.startActivity").disable();
        }
        else {
            this.proposalForm.get("grpDatePeriod.startActivity").enable();
        }
    };
    ProposalFormComponent.prototype.permanentChanged = function (checked) {
        if (checked) {
            this.proposalForm.patchValue({ grpDatePeriod: { endValidity: null } });
            this.proposalForm.get("grpDatePeriod.endValidity").disable();
        }
        else {
            this.proposalForm.get("grpDatePeriod.endValidity").enable();
        }
    };
    ProposalFormComponent.prototype.validateOriginator = function () {
        var originatorCtrl = this.proposalForm.get("originator");
        return originatorCtrl.valid || originatorCtrl.untouched;
    };
    ProposalFormComponent.prototype.validateStartActivity = function () {
        if (this.proposalForm.get("grpDatePeriod.immediate").value) {
            return true;
        }
        var starActivityCtrl = this.proposalForm.get("grpDatePeriod.startActivity");
        return starActivityCtrl.valid || starActivityCtrl.untouched;
    };
    ProposalFormComponent.prototype.validateEndValidity = function () {
        if (this.proposalForm.get("grpDatePeriod.permanent").value) {
            return true;
        }
        var endValidityCtrl = this.proposalForm.get("grpDatePeriod.endValidity");
        return endValidityCtrl.valid || endValidityCtrl.untouched;
    };
    ProposalFormComponent.prototype.setPayload = function () {
        debugger;
        var estimated = this.proposalForm.get("estimated");
        if (estimated) {
            this.model.token.estimated = estimated.value;
        }
        var itemD = this.proposalForm.get("itemD");
        if (itemD) {
            this.model.token.itemD = itemD.value;
        }
        var originator = this.proposalForm.get("originator");
        if (originator) {
            this.model.token.originator = originator.value;
        }
        var grpDatePeriod = this.proposalForm.controls["grpDatePeriod"];
        var endValidity = grpDatePeriod.get('endValidity');
        if (endValidity && endValidity.value) {
            this.model.token.endValidity = endValidity.value;
        }
        var startActivity = grpDatePeriod.get('startActivity');
        if (startActivity && startActivity.value) {
            this.model.token.startActivity = startActivity.value;
        }
        var immediate = grpDatePeriod.get('immediate');
        if (immediate) {
            this.model.token.immediate = immediate.value;
        }
        var permanent = grpDatePeriod.get('permanent');
        if (permanent) {
            this.model.token.permanent = permanent.value;
        }
        this.setNsdPayload();
    };
    ProposalFormComponent.prototype.setNsdPayload = function () {
        var frmArrControl = this.proposalForm.controls['frmArr'];
        var frmArrControls = frmArrControl.controls[0];
        for (var key in frmArrControls.controls) {
            if (frmArrControls.controls.hasOwnProperty(key)) {
                var control = frmArrControls.controls[key];
                if (control.value !== null && typeof (control.value) === "object") {
                    var grpObject = control.value;
                    for (var grpKey in grpObject) {
                        if (grpObject.hasOwnProperty(grpKey)) {
                            var grpControl = grpObject[grpKey];
                            if (grpControl) {
                                if (grpKey in this.model.token) {
                                    var value = typeof (grpControl) === "object" ? grpControl.value : grpControl;
                                    this.model.token[grpKey] = value;
                                }
                            }
                        }
                    }
                }
                else {
                    if (control) {
                        if (key in this.model.token) {
                            var value = typeof (control) === "object" ? control.value : control;
                            this.model.token[key] = control.value;
                        }
                    }
                }
            }
        }
    };
    ProposalFormComponent.prototype.configureReactiveForm = function () {
        this.proposalForm = this.fb.group({
            originator: [this.model.token.originator, [forms_1.Validators.required, forms_1.Validators.pattern('[a-zA-Z].*')]],
            grpDatePeriod: this.fb.group({
                immediate: this.model.token.immediate,
                startActivity: [this.model.token.startActivity],
                permanent: this.model.token.permanent,
                endValidity: [this.model.token.endValidity],
            }, { validator: dateRangeValidator }),
            estimated: this.model.token.estimated,
            itemD: this.model.token.itemD,
            frmArr: this.fb.array([])
        });
    };
    return ProposalFormComponent;
}());
__decorate([
    core_1.Input('model'),
    __metadata("design:type", Object)
], ProposalFormComponent.prototype, "model", void 0);
ProposalFormComponent = __decorate([
    core_1.Component({
        templateUrl: '/app/dashboard/proposals/proposal-form.component.html',
        selector: 'proposal-form',
    }),
    __metadata("design:paramtypes", [forms_1.FormBuilder,
        router_1.Router])
], ProposalFormComponent);
exports.ProposalFormComponent = ProposalFormComponent;
//Custom Validators
function dateRangeValidator(c) {
    var startActivityCtrl = c.get("startActivity");
    var endValidityCrl = c.get("endValidity");
    var immediateCtrl = c.get("immediate");
    var permanentCtrl = c.get("permanent");
    if (startActivityCtrl.pristine && immediateCtrl && endValidityCrl && permanentCtrl) {
        return null;
    }
    if (!immediateCtrl.value) {
        var startDate = moment(startActivityCtrl.value, "L, LT");
        if (startDate.isValid()) {
            var now = moment();
            var days = startDate.diff(now, 'days');
            if (days < 1) {
                return {
                    'validateOneDayInAdvance': true
                };
            }
        }
        else {
            return {
                'startActivityRequired': true
            };
        }
    }
    if (!permanentCtrl.value) {
        var startDate = immediateCtrl.value
            ? moment(startActivityCtrl.value, "L, LT")
            : moment();
        var endValidityDate = moment(endValidityCrl.value, "L, LT");
        if (endValidityDate.isValid()) {
            startDate = startDate.add(30, 'minutes');
            var days = endValidityDate.diff(startDate, 'days');
            if (days < 1) {
                return {
                    'validateLaterThanStartingDate': true
                };
            }
        }
        else {
            return {
                'endValidityRequired': true
            };
        }
    }
    return null;
}
//# sourceMappingURL=proposal-form.component.js.map
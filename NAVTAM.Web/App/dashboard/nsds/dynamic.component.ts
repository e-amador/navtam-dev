﻿import {Component, Input, ViewContainerRef, 
        ViewChild, ReflectiveInjector, ComponentFactoryResolver} from '@angular/core';

//Nsds Components
import { CatchAllDraftComponent } from './catchall/catchall-draft.component';  
import { CatchAllCancelComponent } from './catchall/catchall-cancel.component';  
import { ObstComponent } from './obst/obst.component';
import { ObstCancelComponent } from './obst/obst-cancel.component';
import { ObstLgUsComponent } from './obstlgus/obstlgus.component';
import { ObstLgUsCancelComponent } from './obstlgus/obstlgus-cancel.component';
import { MObstComponent } from './mobst/mobst.component';
import { MObstCancelComponent } from './mobst/mobst-cancel.component';
import { MObstLgUsComponent } from './mobstlgus/mobstlgus.component';
import { MObstLgUsCancelComponent } from './mobstlgus/mobstlgus-cancel.component';
import { RwyClsdComponent } from './rwyclsd/rwyclsd.component';
import { RwyClsdCancelComponent } from './rwyclsd/rwyclsd-cancel.component';
import { TwyClsdComponent } from './twyclsd/twyclsd.component';
import { TwyClsdCancelComponent } from './twyclsd/twyclsd-cancel.component';
import { CarsClsdComponent } from './carsclsd/carsclsd.component';
import { CarsClsdCancelComponent } from './carsclsd/carsclsd-cancel.component';
import { AreaDefinitionNsdComponent } from './areadefinition/areadefinition.component';
import { AreaDefinitionCancelNsdComponent } from './areadefinition/areadefinition-cancel.component';

@Component({
  selector: 'dynamic-component',
  entryComponents: [
      CatchAllDraftComponent, 
      CatchAllCancelComponent,
      ObstComponent,
      ObstCancelComponent,
      ObstLgUsComponent,
      ObstLgUsCancelComponent,
      MObstComponent,
      MObstCancelComponent,
      MObstLgUsComponent,
      MObstLgUsCancelComponent,
      RwyClsdComponent,
      RwyClsdCancelComponent,
      TwyClsdComponent,
      TwyClsdCancelComponent,
      CarsClsdComponent,
      CarsClsdCancelComponent,
      AreaDefinitionNsdComponent,
      AreaDefinitionCancelNsdComponent
      ], 
  template: `<div #dynamicComponentContainer></div>`,
})
export class DynamicComponent {  
  currentComponent = null;

  @ViewChild('dynamicComponentContainer', { read: ViewContainerRef }) dynamicComponentContainer: ViewContainerRef;

  constructor(private resolver: ComponentFactoryResolver) {
  }

  // catId: Nsd categoryId used to resolve the component
  // ver: Nsd version (not used at this point but passed if future versioning is needed)
  // inputs: An object with key/value pairs mapped to input name/input value injected to the component
  @Input() set componentData(data: {catId: number, version: string, inputs: any}){

    if (!data) {
      return;
    }

    // Inputs need to be in the following format to be resolved properly
    const inputProviders = Object.keys(data.inputs).map((inputName) => {
            return {provide: inputName, useValue: data.inputs[inputName]};
        });

    const resolvedInputs = ReflectiveInjector.resolve(inputProviders);
    
    // We create an injector out of the data we want to pass down and this components injector
    const injector = ReflectiveInjector.fromResolvedProviders(
        resolvedInputs, this.dynamicComponentContainer.parentInjector);

    const componentClass = this.resolveComponent(data.catId, data.version, data.inputs['type']);
          
    // We create a factory out of the component we want to create
    const factory = this.resolver.resolveComponentFactory<any>(componentClass);
    
    // We create the component using the factory and the injector
    const component = factory.create(injector);
    
    // We insert the component into the dom container
    this.dynamicComponentContainer.insert(component.hostView);
    
    // We can destroy the old component is we like by calling destroy
    if (this.currentComponent) {
      this.currentComponent.destroy();
    }
    
    this.currentComponent = component;
  }

  private resolveComponent(catId: number, version: string, type: string) {
      //At this point there is not versioning. If in the future a new new version of the component is added, 
      //we can use the version param to resolve the correct instance (naming convention should be decided). 

      //Depending on the type of NSD it might be necesary to have different components for Cancel and Replace. 
      //In the case of Catch All all actions are the same so only one component is required.
      switch (catId) {
          case 2: //CATCH ALL
              switch (type) {
                  case "cancel": 
                      return CatchAllCancelComponent;
                  default: return CatchAllDraftComponent;
              }
          case 4:// OBST
              switch (type) {
                  case "cancel":
                      return ObstCancelComponent;
                  default:
                      return ObstComponent;
              }
          case 5:// OBST LG US
              switch (type) {
                  case "cancel":
                      return ObstLgUsCancelComponent;
                  default:
                      return ObstLgUsComponent;
              }
          case 7: //RWY CLSD
              switch (type) {
                  case "cancel":
                      return RwyClsdCancelComponent;
                  default:
                      return RwyClsdComponent;

              }
          case 11: //TWYCLSD
              switch (type) {
                  case "cancel":
                      return TwyClsdCancelComponent;
                  default:
                      return TwyClsdComponent;
              }
          case 13: //CARSCLSD
              switch (type) {
                  case "cancel":
                      return CarsClsdCancelComponent;
                  default:
                      return CarsClsdComponent;
              }
          case 14: //MULT OBST
              switch (type) {
                  case "cancel":
                      return MObstCancelComponent;
                  default:
                      return MObstComponent;
              }
          case 15: //MULT OBST LG US
              switch (type) {
                  case "cancel":
                      return MObstLgUsCancelComponent;
                  default:
                      return MObstLgUsComponent;
              }
          case 18: // AIRSPACE
              switch (type) {
                  case "cancel":
                      return AreaDefinitionCancelNsdComponent;
                  default:
                      return AreaDefinitionNsdComponent;
              }

          default:
              return CatchAllDraftComponent; 
      }
    }
  }
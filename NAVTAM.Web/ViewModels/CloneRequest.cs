﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NAVTAM.ViewModels
{
    public class CloneRequest
    {
        public int Id { get; set; }
        public bool History { get; set; }
    }
}
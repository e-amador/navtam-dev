﻿using NavCanada.Core.Domain.Model.Entitities;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    public interface IOrganizationNsdRepo : IRepository<OrganizationNsdCategory>
    {
        Task<bool> AddNsdToOrganization(int nsdId, int orgId);

        Task<bool> RemoveNsdInOrganizationByNsdId(int id, int orgId);

        Task<bool> OrganizationContainsNsd(int orgId, int nsdId);

        Task UpdateOrganizationNsds(int orgId, List<int> nsdIds);

        Task<bool> RemoveNsdFromAllOrganizationsByNsdId(int nsdId);
    }
}
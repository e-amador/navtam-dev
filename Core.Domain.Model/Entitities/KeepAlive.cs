﻿using System;
using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class KeepAlive
    {
        public Guid Id { get; set; }

        public string Component { get; set; }

        public TickType Tick { get; set; }

        public DateTime Received { get; set; }
    }
}
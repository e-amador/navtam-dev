import { Component, Inject } from '@angular/core';
import { INotam } from './services/service.contracts';
import { QueueService } from './services/queue.service';

import { Observable } from 'rxjs/Observable';
import './rxjs-extensions';

@Component({
  selector: 'my-app',
  templateUrl: `./app.component.html`,
})
export class AppComponent  {
    constructor(@Inject(QueueService) private service: QueueService) {
    }

    title: string = 'NOTAM Monitor';
    userName: string = "nds";
    password: string = "password";
    topicName: string = "NDS/B/>";
    subscriptionName: string = "subscription";
    filterValue: string = "";
    timer: any = null;

    started: boolean = false;
    subscriptionId: string;
    notams: INotam[] = [];

    createSubscription() {
        this.started = true;
        this.service
            .createTopic(this.userName, this.password, this.topicName, this.subscriptionName, this.filterValue)
            .toPromise()
            .then(id => this.mapSubscriptionId(id), error => this.handleError(error));
    }

    unsubscribe() {
        this.service
            .unsubscribe(this.subscriptionId)
            .toPromise()
            .then(() => this.stopTimer(), error => this.handleError(error));
    }

    private mapSubscriptionId(id: string) {
        this.subscriptionId = id;
        var model = this;
        this.timer = setInterval(() => model.drainQueue(), 1500);
    }

    private handleError(error: any) {
        console.error(error);    
        this.stopTimer();
        return Promise.reject(error);    
    }

    private stopTimer() {
        this.started = false;
        if (this.timer) {
            clearInterval(this.timer);
            this.timer = null;
        }
    }

    drainQueue() {
        this.service
            .drainQueue(this.subscriptionId)
            .toPromise()
            .then(notams => this.assignNotams(notams), error => this.stopTimer())
    }

    assignNotams(notams: INotam[]) {
        if (notams) {
            notams.forEach(n => this.notams.push(n));
        }
    }

    cleanQueue() {
        this.notams = [];
    }
}
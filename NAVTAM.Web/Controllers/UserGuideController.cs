﻿using System;
using System.Web.Hosting;
using System.Web.Mvc;

namespace NAVTAM.Controllers
{
    [Authorize]
    public class UserGuideController : Controller
    {
        // GET: UserGuide
        [ValidateAntiForgeryToken]
        public ActionResult Index(string language = "en")
        {
            var location = HostingEnvironment.MapPath(String.Format("~/Content/help/NESUserGuide{0}.pdf", language));
            return File(location, "application/pdf");
        }
    }
}
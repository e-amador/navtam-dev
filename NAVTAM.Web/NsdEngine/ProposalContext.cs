﻿using Core.Common.Geography;
using NavCanada.Core.Common.Common;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Proxies.AeroRdsProxy;
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Threading.Tasks;

namespace NAVTAM.NsdEngine
{
    public class ProposalContext
    {
        public ProposalContext(IUow uow, IAeroRdsProxy aeroRdsProxy, DbGeography userDoa, int mvp)
        {
            Uow = uow;
            UserDoa = userDoa;
            MinValidityPeriod = mvp;
            AeroRdsProxy = aeroRdsProxy;
        }

        public IUow Uow { get; set; }

        public DbGeography UserDoa { get; set; }

        public IAeroRdsProxy AeroRdsProxy { get; set; }

        public int MinValidityPeriod { get; set; }

        public bool IsCancelation { get; set; }
        public bool IsBilingual { get; set; }

        public DMSLocation Location { get; set; }
        public DbGeography GeoLocation => Location != null ? DbGeography.PointFromText(Location.GeoPointText, CommonDefinitions.Srid) : null;

        public IcaoSubject IcaoSubject { get; set; }
        public IcaoSubjectCondition IcaoCondition { get; set; }
        public NearSdoSubject NearestSubject { get; set; }
        
        public void AddContext(string name, object value)
        {
            var key = MakeKey(name);
            if (!_container.ContainsKey(key))
            {
                _container[key] = value;
            }
        }

        public void AddContext(string name, Func<object> lambda)
        {
            var key = MakeKey(name);
            if (!_container.ContainsKey(key))
            {
                _container[key] = lambda.Invoke();
            }
        }

        public async Task AddContextAsync(string name, Func<Task<object>> asyncLambda)
        {
            var key = MakeKey(name);
            if (!_container.ContainsKey(key))
            {
                _container[key] = await asyncLambda.Invoke();
            }
        }

        public T GetContext<T>(string name) where T: class
        {
            return _container[MakeKey(name)] as T;
        }

        static string MakeKey(string name) => (name ?? "").ToLower();

        readonly Dictionary<string, object> _container = new Dictionary<string, object>();
    }
}
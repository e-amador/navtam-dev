﻿using NavCanada.Core.Domain.Model.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;

namespace NavCanada.Core.Domain.Model.Entitities
{
    /// <summary>
    /// Stores the cache for an SDO object (Firs, Aerodromes, Runways, Taxiways, Services, etc)
    /// </summary>
    public class SdoCache
    {
        public string Id { get; set; }
        
        /// <summary>
        /// Parent cached object. E.g.: Aerodrome id for a Runway or a Taxiway.
        /// </summary>
        public string ParentId { get; set; }
        
        /// <summary>
        /// Fir, Aerodrome, Runway, Taxiway, Service, etc.
        /// </summary>
        public SdoEntityType Type { get; set; }

        /// <summary>
        /// Name of the cached object when applies (Aerodromes and Firs)
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Designator of the cached object when applies (Aerodromes, Firs, Runways, etc)
        /// </summary>
        public string Designator { get; set; }

        /// <summary>
        /// Fir the object belongs to, when applies.
        /// </summary>
        public string Fir { get; set; }

        /// <summary>
        /// Reference point in the map. This is used to filter access based on DOAs.
        /// </summary>
        public DbGeography RefPoint { get; set; }

        /// <summary>
        /// Geography of the object, it only applies for now for firs.
        /// </summary>
        public DbGeography Geography { get; set; }

        /// <summary>
        /// SDO object attribues as read from AeroRDS.
        /// </summary>
        public string Attributes { get; set; }

        /// <summary>
        /// Searchable keywords taken from the SDO attributes (e.g.: CityServed)
        /// </summary>
        public string Keywords { get; set; }

        /// <summary>
        /// Flags store custom flags for this cached object (e.g.: Category for an aerodrome)
        /// </summary>
        public int Flags { get; set; }

        /// <summary>
        /// Marked as deleted.
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Effective date for this object. 
        /// </summary>
        public DateTime EffectiveDate { get; set; }

        public SdoCache Parent { get; set; }
        public virtual ICollection<SdoCache> Children { get; set; }
    }
}

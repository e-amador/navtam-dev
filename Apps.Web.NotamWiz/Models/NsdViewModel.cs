﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class NsdViewModel
    {
        public long Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Category { get; set; }
      
        public string Description { get; set; }

        public bool Active { get; set; }

        [Required]
        public string FilesLocation { get; set; }

        public List<OrganizationListViewModel> LimitedToOrganizatations { get; set; }

        public List<NsdVersionViewModel> Versions { get; set; }



    }
}
﻿using NavCanada.Core.Domain.Model.Entitities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    public class OrganizationConfig : EntityTypeConfiguration<Organization>
    {
        public OrganizationConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(e => e.Name).IsRequired();
            Property(e => e.EmailAddress).IsRequired();
            Property(e => e.Telephone).IsRequired();
            Property(e => e.TypeOfOperation).IsRequired();

            //HasOptional(e => e.AssignedDoa)
            //    .WithMany()
            //    .HasForeignKey(e => e.AssignedDoaId)
            //    .WillCascadeOnDelete(false);

            HasMany(o => o.Doas)
                .WithRequired(e => e.Organization)
                .WillCascadeOnDelete(true);

            HasMany(o => o.Nsds)
                .WithRequired(n => n.Organization)
                .WillCascadeOnDelete(true);
        }
    }
}


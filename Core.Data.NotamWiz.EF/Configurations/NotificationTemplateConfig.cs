﻿namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    using Domain.Model.Entitities;

    public class NotificationTemplateConfig : EntityTypeConfiguration<NotificationTemplate>
    {
        public NotificationTemplateConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}


﻿define(["jquery", "Resources", "kendoExt"], function ($)
{
    return {

        init: function (labels) {
            var detailsTemplate = kendo.template($("#reviewDetailTemplate").html());

            var windowAdmin = $("#userDetail-Popup").kendoWindow({
                actions: ["Cancel"],
                width: 520,
                visible: false,
                title: labels.newReg_userRegInfo,
                modal: true,
                close: function ()
                {
                    $("#userGrid").data('kendoGrid').dataSource.read();
                    $("#userGrid").data('kendoGrid').refresh();
                }
            }).data("kendoWindow");

            windowAdmin.bind("refresh", function ()
            {
                $("#userDetail-Popup").data("kendoWindow").center();
                $("#userDetail-Popup").data("kendoWindow").open();
            });

            $("#toolbar").kendoToolBar({
                items: [
                    { type: "button", id: "tbrDetails", text: labels.newReg_userDetails, enable: false, click: toolBarEventHandler },
                    { type: "button", id: "tbrResendEmail", text: labels.newReg_resendEmail, enable: false, click: toolBarEventHandler },
                    { type: "button", id: "tbrDelete", text: labels.newReg_delete, enable: false, click: toolBarEventHandler }
                ]
            });

            $("#userGrid").kendoGrid({
                sortable: true,
                scrollable: true,
                resizable: { columns: true },
                selectable: true,
                filterable: true,
                reorderable: true,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [
                    { field: "userVM.UserName", title: labels.newReg_userName, width: "~100" },
                    { field: "userVM.FirstName", title: labels.newReg_firstName, width: "~100" },
                    { field: "userVM.LastName", title: labels.newReg_lastName, width: "~100" },
                    { field: "userVM.EmailAddress", title: labels.newReg_emailAddress, width: "~100" },
                    { field: "userVM.RegistrationOrgName", title: labels.newReg_orgName, width: "~100" }
                ],
                change: function (e) {

                    function enableToolBarButtons() {
                        var toolbar = $("#toolbar").data("kendoToolBar");
                        toolbar.enable("#tbrDetails");
                        toolbar.enable("#tbrResendEmail");
                        toolbar.enable("#tbrDelete");
                    }

                    enableToolBarButtons();
                    $("#userGrid").find(".k-grid-Review").show();
                },
                dataSource: {
                    transport: {
                        read: { url: "/WaitingApproval/ReadWaitingApprovalUsers", dataType: "json", type: "POST" },
                    },
                    pageSize: 10,
                    schema: {
                        data: "Data",
                        model: {
                            id: "UserId"
                        }
                    },
                    sort: [
                        { field: "IsDisabled", dir: "desc" }
                    ]
                },
                dataBound: function (/*e*/)
                {
                    $("#userGrid").find(".k-grid-Review").hide();
                }
            });
            function toolBarEventHandler(e) {

                function showUserDetails(dataItem) {
                    windowAdmin.content(detailsTemplate);
                    kendo.bind($('#reviewDetailContent'), dataItem);
                    $("#userDetail-Popup").data("kendoWindow").refresh();
                    $("#btnClose").on("click", function (e) {
                        $(this).closest("[data-role=window]").kendoWindow("close");
                    });
                }

                function deleteUser(dataItem, grid) {
                    kendo.ui.ExtOkCancelDialog.show({
                        title: labels.newReg_deleteUserDlgTitle,
                        message: labels.newReg_deleteUserDlgContent + " [" + dataItem.userVM.UserName + "]?",
                        width: '400px',
                        height: '90px',
                        icon: "k-ext-warning"
                    }).done(function (response) {
                        if (response.button === 'OK') {
                            $.ajax({
                                type: "POST",
                                dataType: "json",
                                url: "/WaitingApproval/DeleteUser",
                                data: { userId: dataItem.userVM.UserId },
                                async: false,
                                success: function (result) {
                                    if (result.Success) {
                                        grid.dataSource.read();
                                    } else {
                                        kendo.ui.ExtAlertDialog.show({
                                            title: labels.newReg_deleteUserErrorTitle,
                                            message: labels.newReg_deleteUserErrorContent,
                                            icon: "k-ext-error"
                                        });
                                    }

                                },
                                error: function (/*xhr, ajaxOptions, thrownError*/) {
                                }
                            });
                        }
                    });
                }

                function resendEmail(dataItem) {
                    kendo.ui.ExtOkCancelDialog.show({
                        title: labels.newReg_resendEmailTitle,
                        message: labels.newReg_resendEmailContent + " [" + dataItem.userVM.UserName + "]?",
                        width: '400px',
                        height: '90px',
                        icon: "k-ext-warning"
                    }).done(function (response) {
                        if (response.button === 'OK') {
                            $.ajax({
                                type: "POST",
                                dataType: "json",
                                url: "/WaitingApproval/ResendEmail",
                                data: { userId: dataItem.userVM.UserId },
                                async: false,
                                success: function (result) {
                                    if (result.Success) {
                                        //don't show any confirmation as other action in the application
                                    } else {
                                        kendo.ui.ExtAlertDialog.show({
                                            title: labels.newReg_resendEmailErrorTitle,
                                            message: labels.newReg_resendEmailErrorContent,
                                            icon: "k-ext-error"
                                        });
                                    }

                                },
                                error: function (/*xhr, ajaxOptions, thrownError*/) {
                                }
                            });
                        }
                    });
                }

                var grid = $("#userGrid").data("kendoGrid");
                var dataItem = grid.dataItem(grid.select());

                switch( e.id ) {
                    case "tbrDetails": showUserDetails(dataItem); break;
                    case "tbrResendEmail": resendEmail(dataItem); break;
                    case "tbrDelete": deleteUser(dataItem, grid); break;
                }
            };
        }
    };
});

namespace NavCanada.Services.Api.Diagnostics
{
    using System;
    using System.Data.Entity;

    using Microsoft.Practices.Unity;

    using Data;
    using Data.Contracts;
    using Data.Helpers;

    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            //Register as a singleton
            container.RegisterInstance(new RepositoryFactories(), new ContainerControlledLifetimeManager());

            //Register as always new instance
            container.RegisterInstance(new RepositoryProvider(container.Resolve<RepositoryFactories>()));
            container.RegisterType<IRepositoryProvider, RepositoryProvider>(new TransientLifetimeManager());

            //container.RegisterInstance(new AppDbContext("DefaultConnection"));
            container.RegisterType<DbContext, DiagnosticDbContext>(new InjectionConstructor("DefaultDiagnosticConnStr"));
            var uow = new Uow(container.Resolve<DiagnosticDbContext>(), container.Resolve<RepositoryProvider>());

            //Register as always a new instance
            container.RegisterInstance(uow);
            container.RegisterType<IDiagnosticUow, Uow>(new TransientLifetimeManager());

        }
    }
}

﻿using NavCanada.Core.Domain.Model.Enums;
using System.Collections.Generic;
using System.Data.Entity.Spatial;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class Doa
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DbGeography DoaGeoArea { get; set; }
        public RegionType DoaType { get; set; }
        public virtual ICollection<UserDoa> Users { get; set; }
        public virtual ICollection<OrganizationDoa> Organizations { get; set; }
    }
}
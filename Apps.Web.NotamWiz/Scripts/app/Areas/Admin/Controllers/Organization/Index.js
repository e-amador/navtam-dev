﻿define(["jquery", "Resources", "kendo/kendo.grid.min", "kendoExt", "kendo/kendo.all.min"], function ($, res) {

    var orgObject, doaList;
    var kendoValidatorOrganization = null;
    var labels = {};

    return {
        cssRole: false,
        orgAdminRole: false,
        adminRole: false,

        init: function (lbs) {
            labels = lbs;
            orgObject = this;
            orgObject.initUI();
        },

        isTrue: function (v) {
            if (typeof v === 'undefined' || v == null)
                return false;

            return v === true;
        },

        initUI: function () {
            var update_OrganizationTemplate = kendo.template($("#Update_OrganizationTemplate").html());
            doaList = [{ value: null, text: ' ' }].concat($("#doaList").data("model"));

            orgObject.cssRole = orgObject.isTrue($("#roles").data("ccsrole"));
            orgObject.adminRole = orgObject.isTrue($("#roles").data("adminrole"));
            orgObject.orgAdminRole = orgObject.isTrue($("#roles").data("orgaminrole"));

            function setKendoValidatorOrganization() {
                kendoValidatorOrganization = $("#detailContent").kendoValidator({
                    messages: {
                        validmask: function (input) {
                            if (input.is("[name=OrganizationTelephone]")) {
                                return labels.phoneNumberIncomplete;
                            }
                            return "";
                        },
                        required: function (input) {
                            if (input.is("[name=Name]")) {
                                return labels.orgNameRequired;
                            }
                            if (input.is("[name=Address]")) {
                                return labels.addressIsRequired;
                            }
                            if (input.is("[name=EmailAddress]")) {
                                return labels.emailIsRequired;
                            }
                            if (input.is("[name=OrganizationTelephone]")) {
                                return labels.telephoneIsRequired;
                            }
                            if (input.is("[name=TypeOfOperation]")) {
                                return labels.typeOfOpIsRequired;
                            }
                            return "";
                        },
                        validFic: function(input)
                        {
                            return labels.ficIsRequired;
                        }
                    },
                    rules: {

                        validFic: function (input) {
                            if (input.is("[name=ficSelector]") && $('#IsFic:checkbox:checked').length > 0 && (input.val() === null || input.val() === "") ) {
                                return false;
                            }

                            return true;

                        },
                        validmask: function (input) {
                            if (input.is("[name=OrganizationTelephone]")) {
                                if (input.val() !== '') {
                                    var data = input.data("kendoMaskedTextBox");
                                    return data.value().indexOf(data.options.promptChar) === -1;
                                }
                            }
                            return true;
                        }
                    }
                }).data("kendoValidator");

            }

            function wireControllersOrganization() {
                $("#OrganizationTelephone").kendoMaskedTextBox({
                    mask: "(000) 000-0000"
                });
            }

            $("#orgGrid").kendoGrid({

                change: onChange,
                selectable: true,
                sortable: true,
                scrollable: true,
                pageable: {
                    refresh: true,
                    buttonCount: 5
                },
                filterable: true,

                columns: [
                    { field: "Name", title: labels.nameLabel, width: "20%" },
                    { field: "Address", title: labels.addressLabel, width: "30%" },
                    { field: "EmailAddress", title: labels.emailAddress, width: "10%" },
                    { field: "Telephone", title: labels.telephoneNumber, width: "10%" },
                    { field: "TypeOfOperation", title: labels.typeOfOperation, width: "10%" },
                    { field: "HeadOffice", title: labels.headOfficeInfo, width: "10%" },
                    { field: "EmailDistribution", title: labels.emailDistList, width: "10%" }
                ],

                toolbar: [
                    { name: "create", text: labels.addNewOrg },
                    { name: "showDetails", text: labels.showOrgDetails },
                    { name: "update", text: labels.updateOrg },
                    { name: "destroy", text: labels.deleteOrg }
                ],
                editable: {
                    mode: "popup",
                    template: kendo.template(update_OrganizationTemplate),
                    window: { width: 620, title: labels.addUpdateOrg }
                },
                edit: function (e) {
                    initiateTemplate();
                    $(".k-edit-form-container").width(620);
                    setKendoValidatorOrganization();
                    wireControllersOrganization();
                },
                save: function (e) {
                    if (kendoValidatorOrganization.validate() === false) {
                        e.preventDefault();
                    }
                },
                dataSource: {
                    sync: function () {
                        //$("#orgGrid").data("kendoGrid").dataSource.sync();
                    },
                    batch: false,
                    schema: {
                        model: { id: "Id" },
                        fields: {
                            "IsFic": {
                                type: "boolean",
                            }
                            //"FicId": {
                            //    validation: {
                            //        checkRequired: function (input) {
                            //            return false;
                            //        }

                            //    }

                            //}
                        },
                        data: function (data) { return data.Data; },
                        errors: function (response) {
                            if (response.Success === false) {
                                return response.Error;
                            }
                        }
                    },
                    transport: {
                        read: { url: "Organization/HierarchyBinding_Organizations", dataType: "json", type: "POST" },
                        create: { url: "Organization/Create", dataType: "json", type: "POST" },
                        update: { url: "Organization/Update", dataType: "json", type: "POST" },
                        destroy: { url: "Organization/Delete", dataType: "json", type: "POST" }
                    },
                    requestEnd: function (e) {
                        if (e.type === "create" || e.type === "update") {
                            $("#orgGrid").data("kendoGrid").dataSource.read();
                        }
                    },
                    error: function (response) {
                        if (response.xhr) {
                            $('#error-panel li').remove();
                            for (var iter = 0; iter < response.xhr.responseJSON.length; iter++) {
                                $('#error-panel ul').append('<li>' + response.xhr.responseJSON[iter] + '</li>');
                            }
                            $("#error-panel").show();
                        }
                    }
                },
                dataBound: function (e) {
                    $("#orgGrid").find(".k-grid-delete").hide();
                    $("#orgGrid").find(".k-grid-update").hide();
                    $("#orgGrid").find(".k-grid-showDetails").hide();

                    if (!orgObject.adminRole && !orgObject.cssRole) {
                        $("#orgGrid").find(".k-grid-add").hide();
                    }
                }
            });

            $("#orgGrid .k-grid-toolbar .k-grid-update").on("click", function (e) {
                var grid = $("#orgGrid").data("kendoGrid");
                grid.editRow(grid.select());
            });

            $("#orgGrid .k-grid-toolbar .k-grid-showDetails").on("click", function (e) {
                showDetails(e);
            });

            function showDetails(e) {

                var grid = $("#orgGrid").data("kendoGrid");
                grid.editRow(grid.select());
                $("#detailContent input").attr('readonly', true);
                $("#detailContent textarea").attr('readonly', true);
                $("#ficSelector").data("kendoDropDownList").readonly();
                $("#AssignedDoaId").data("kendoComboBox").readonly();                
                $(".k-window .k-grid-cancel").html(labels.closeLbl);
                $(".k-window .k-grid-update").hide();
            };

            function onChange(arg) {
                var grid = $("#orgGrid").data("kendoGrid");
                var dataItem = grid.dataItem(grid.select());
                $("#orgGrid").find(".k-grid-showDetails").show();
                $("#orgGrid").find(".k-grid-delete").hide();
                $("#orgGrid").find(".k-grid-update").hide();
                if (!dataItem.IsReadOnlyOrg && (orgObject.adminRole || orgObject.cssRole)) {
                    $("#orgGrid").find(".k-grid-delete").show();
                    $("#orgGrid").find(".k-grid-update").show();
                }
            };

            $("#orgGrid .k-grid-toolbar .k-grid-delete").on("click", function (e) {
                e.preventDefault();
                var grid = $("#orgGrid").data("kendoGrid");
                var dataItem = grid.dataItem(grid.select());

                kendo.ui.ExtOkCancelDialog.show({
                    title: labels.deleteSelOrg,
                    message: labels.deleteSelOrgQuest,
                    width: '400px',
                    height: '100px',
                    icon: "k-Ext-warning"
                }).done(function (response) {
                    if (response.button === 'OK') {
                        if (!dataItem.IsReadOnlyOrg) {
                            grid.dataSource.remove(dataItem);
                            grid.saveChanges();
                        }
                    }
                });
            });

            function initiateTemplate() {
                $("#AssignedDoaId")
                            .kendoComboBox({
                                dataTextField: "text",
                                dataValueField: "value",
                                dataSource: doaList,
                                valuePrimitive: true
                            });


                // Setup Kendo widgets
                $("#ficSelector").kendoDropDownList({
                    optionLabel: labels.selectFir,
                    filter: "contains",
                    dataTextField: "DisplayName",
                    valuePrimitive: true,
                    //template: '<span class="k-state-default">#: EntityCode # </span> <span class="k-state-default"> #: DisplayName # </span>',
                    dataValueField: "Id",
                    dataSource: {
                        type: "odata-v4",
                        pageSize: 20,
                        serverFiltering: true,
                        filter: {
                            logic: "or",
                            filters: [
                                { field: "Designator", operator: "contains", value: $("#firSelector").text() },
                                { field: "Name", operator: "contains", value: $("#firSelector").text() }
                            ]
                        },
                        transport: {
                            read: {
                                url: "/odata/FIR",
                                dataType: "json"
                            },
                            parameterMap: function (data, type) {
                                if (type === "read") {
                                    var filter = $.grep(data.filter.filters, function (n, i) {
                                        return n.field == "DisplayName";
                                    });
                                    if (filter.length == 0) {
                                        return null;
                                    }
                                    return { $filter: "contains(tolower(Name),'" + filter[0].value + "')" };
                                }
                            }
                        },
                        schema: {
                            data: function (data) {
                                return data["value"];
                            },
                            total: function (data) {
                                return data["odata.count"];
                            },
                            model: {
                                id: "Id"
                            },
                            parse: function (response) {
                                $.each(response.value, function (idx, elem) {
                                    elem.DisplayName = elem.Designator + " - " + elem.Name;
                                });
                                return response;
                            }
                        },
                        change: function (e) {

                        }
                    },
                    change: function (e) {

                    }
                });

         
            
            }
        }
    };
});

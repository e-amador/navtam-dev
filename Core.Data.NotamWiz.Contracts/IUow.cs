﻿namespace NavCanada.Core.Data.NotamWiz.Contracts
{
    using System;
    using System.Data.Entity;
    using System.Threading.Tasks;

    using Repositories;
    using Data.Contracts.Repositories;

    public interface IUow : IDisposable
    {
        // Save pending changes to the data store.
        int SaveChanges(bool clientsWin = false);        
        Task<int> SaveChangesAsync();

        // Reject all pending changes.
        void Detach();

        //Transactional region
        void BeginTransaction(System.Data.IsolationLevel isolationLevel = System.Data.IsolationLevel.RepeatableRead);
        void Commit();
        void Rollback();

        DbContext DbContext { get; }

        // repos
        IAerodromeDisseminationCategoryRepo AerodromeDisseminationCategoryRepo { get; }
        IConfigurationValueRepo ConfigurationValueRepo { get; }
        IDigitalSlaRepo DigitalSlaRepo { get; }
        IDoaRepo DoaRepo { get; }
        IUserDoaRepo UserDoaRepo { get; }
        IOrganizationDoaRepo OrganizationDoaRepo { get; }
        IFeedbackRepo FeedbackRepo { get; }
        IGeoRegionRepo GeoRegionRepo { get; }
        ISeriesAllocationRepo SeriesAllocationRepo { get; }
        IGroupRepo GroupRepo { get; }
        IIcaoSubjectConditionRepo IcaoSubjectConditionRepo { get; }
        IIcaoSubjectRepo IcaoSubjectRepo { get; }
        ISeriesNumberRepo SeriesNumberRepo { get; }
        INotifySeriesRepo NotifySeriesRepo { get; }
        INotamRepo NotamRepo { get; }
        INotificationTemplateRepo NotificationTemplateRepo { get; }
        INotificationTrackingRepo NotificationTrackingRepo { get; }
        IOrganizationRepo OrganizationRepo { get; }
        IOrganizationNsdRepo OrganizationNsdRepo { get; }
        IPermissionRepo PermissionRepo { get; }
        IRegistrationRepo RegistrationRepo { get; }
        IRolePermissionRepo RolePermissionRepo { get; }
        ISdoCacheRepo SdoCacheRepo { get; }
        IUserPreferenceRepo UserPreferenceRepo { get; }
        IUserProfileRepo UserProfileRepo { get; }
        IApplicationRoleRepo ApplicationRoleRepo { get; }
        IPoisonMtoRecordRepo PoisonMtoRecordRepo { get; }
        IPoisonMtoRepo PoisonMtoRepo { get; }
        IScheduleTaskRepo ScheduleTaskRepo { get; }
        IScheduleJobRepo ScheduleJobRepo { get; }
        INsdCategoryRepo NsdCategoryRepo { get; }
        IProposalRepo ProposalRepo { get; }
        IProposalHistoryRepo ProposalHistoryRepo { get; }
        IProposalAttachmentRepo ProposalAttachmentRepo { get; }
        INotamTopicRepo NotamTopicRepo { get; }
        IUserQueryFilterRepo UserQueryFilterRepo { get; }
        INdsClientRepo NdsClientRepo { get; }
        INdsClientSeriesRepo NdsClientSeriesRepo { get; }
        INdsClientItemARepo NdsClientItemARepo { get; }
        INdsNotamMessageRepo NdsNotamMessageRepo { get; }
        INdsOutgoingMessageRepo NdsOutgoingMessageRepo { get; }
        INdsIncomingRequestRepo NdsIncomingRequestRepo { get; }
        IMessageExchangeQueueRepo MessageExchangeQueueRepo { get; }
        IMessageExchangeTemplateRepo MessageExchangeTemplateRepo { get; }
        INofTemplateMessageRecordRepo NofTemplateMessageRecordRepo { get; }
        IItemDRepo ItemDRepo { get; }
        IDisseminationJobRepo DisseminationJobRepo { get; }
        ISeriesChecklistRepo SeriesChecklistRepo { get; }
        IAeroRDSCacheRepo AeroRDSCacheRepo { get; }
        IAeroRDSCacheStatusRepo AeroRDSCacheStatusRepo { get; }
        IUserRegistrationRepo UserRegistrationRepo { get; }
        IOriginatorInfoRepo OriginatorInfoRepo { get; }
        ISystemStatusRepo SystemStatusRepo { get; }
        ILogRepo LogRepo { get; }
        IAerodromeRSCRepo AerodromeRSCRepo { get; }
    }
}

﻿using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    public interface IOriginatorInfoRepo : IRepository<OriginatorInfo>
    {
        Task<OriginatorInfo> GetByIdAsync(int id);
        Task<List<OriginatorInfo>> FilterAsync(string match, int pageSize, int orgId);
        Task<OriginatorInfo> FindAsync(string fullname, int orgId);
    }
}
﻿
define(["jquery", "Resources", 'bingMaps', 'utilities', 'kendoExt'], function ($, res, bingMap, util) {

    var RES, GeoRegionEditor;
    var labels = {};

    return {

        validator: null,
        model: null,

        init: function (lbs) {
            labels = lbs;
            
            // Setup ajax working overlay
            $(document).ajaxStart(function() {
              kendo.ui.progress($("#GeoRegionWindow"), true);
            });

            $(document).ajaxStop(function() {
              kendo.ui.progress($("#GeoRegionWindow"), false);
            });


            RES = res.getResources("GeoRegions");

            GeoRegionEditor = this;

            GeoRegionEditor.model = kendo.observable($.extend({

            }, $("#dataModel").data("model")));

            kendo.bind($('#GeoRegionForm'), GeoRegionEditor.model);

               // Create map.
            var mapOptions = {
                zoom: 5,
                enableClickableLogo: false,
                showTools: true,
            };

            bingMap.init($("#mapContainer"), mapOptions, function () {
                bingMap.setModelGeo(GeoRegionEditor.model.Geo);
                GeoRegionEditor.initUI();
                $(bingMap).on("GeoChanged", GeoRegionEditor.updateGeo);
                
            });

            



        },


        initUI: function () {

            $("#GeoRegionWindow").kendoWindow({
                actions: ["Maximize", "Close"],
                width: 730,
                height: 620,
                resizable: true,
                pinned: true,
                animation: {
                    open: {
                        effects: { fadeIn: {} },
                        duration: 200,
                        show: true
                    },
                    close: {
                        effects: { fadeOut: {} },
                        duration: 600,
                        hide: true
                    }
                },
                close:function(){
                    $("#GeoRegionWindow").data("kendoWindow").destroy();
                },
                visible: true,
                title: labels.doaEditor,
                modal: true,
                //resize: bingMap.resizeMap
            }).data("kendoWindow").open().center();


            GeoRegionEditor.validator = $("#GeoRegionForm").kendoValidator({
                rules: {
                    validateName: function (e) {
                        if (!e.is("[name=txtName]")) return true;

                        if ($("#txtName").val().trim() === "") {
                            $("#txtName").addClass('Invalid')
                            return false;
                        }

                        $("#txtName").removeClass('Invalid')
                        return true;
                    },
                    validateGeo: function (e) {
                        if (!e.is("[name=mapPlace]")) return true;

                        if(GeoRegionEditor.model.Geo === null || GeoRegionEditor.model.Geo.Coordinates === null || GeoRegionEditor.model.Geo.Coordinates.length===0){
                            $("#mapContainer").addClass('Invalid');
                            return false;
                        }

                         $("#mapContainer").removeClass('Invalid')
                         return true;
                    }
                },
                messages: {
                    validateName: labels.regionNameIsReq,
                    validateGeo: labels.pleaseDefineGeoRegion 
                }


            }).data("kendoValidator");

            $("#btnReset").on('click', GeoRegionEditor.Reset);
            $("#btnDiscard").on('click', GeoRegionEditor.Discard);

            $("form").submit(function (event) {
                event.preventDefault();
                if (GeoRegionEditor.validator.validate()) {

                    GeoRegionEditor.Save();

                }
            });


        },

        Discard: function () {
            $.when(kendo.ui.ExtOkCancelDialog.show({
                title: labels.confirmLabel,
                message: labels.areYouSureToDiscard,
                width: '400px',
                height: '150px',
                icon: "k-Ext-warning",
                //buttons: [{ name: 'ssss' }, { name: 'dddd' }]
            })
             ).done(function (response) {
                 if (response.button === 'OK') {
                     $("#GeoRegionWindow").data("kendoWindow").close();
                 }
             });

        },
        Save: function (event) {

            var url = GeoRegionEditor.model.Id == 0 ? "/GeoRegions/Create" : "/GeoRegions/Update";

            $.ajax({
                data: { model: GeoRegionEditor.model.toJSON() },
                cache: false,
                type: "POST",
                dataType: "json",
                url: url,
                success: function (data, status, jqXHR) {

                    $("#GeoRegionWindow").data("kendoWindow").close();
                    if ($('#geoRegionsGrid')) {
                        $('#geoRegionsGrid').data('kendoGrid').dataSource.read();
                        $('#geoRegionsGrid').data('kendoGrid').refresh();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                     kendo.ui.ExtAlertDialog.show({
                                title: labels.errorLabel,
                                message: util.buildErrorMessage(labels.errorSavingGeoRegion, data.errorCode),
                                icon: "k-Ext-error"
                            });
                }
            });

        },

        Reset: function () {

            $.when(kendo.ui.ExtOkCancelDialog.show({
                title: labels.confirmLabel,
                message: labels.areYouSureResetBilReg,
                width: '400px',
                height: '150px',
                icon: "k-Ext-warning",
                //buttons: [{ name: 'ssss' }, { name: 'dddd' }]
            })
                 ).done(function (response) {
                     if (response.button === 'OK') {                         
                         bingMap.clearAll();
                         GeoRegionEditor.model.set("Geo", null);
                     }

                 });
        },

        updateGeo: function (event, geometry) {
            
           GeoRegionEditor.model.set("Geo", geometry);

        },

   
    }
});




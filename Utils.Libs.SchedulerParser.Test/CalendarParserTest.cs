﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utils.Libs.SchedulerParser;

namespace ScheduleParserTest
{
    [TestClass]
    public class CalendarParserTest
    {
        #region Subcase DayRange

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseDayRange_WhenSingleDateAndSingleTime()
        {
            var result = Parser.Agent.Run("JAN 01-10 0100-0200");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.DaysRange);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 1);
            Assert.IsTrue(parserResult.minutes[0].start == 60);
            Assert.IsTrue(parserResult.minutes[0].finish == 120);
            Assert.IsTrue(parserResult.days.Length == 1);
            Assert.IsTrue(parserResult.days[0].startDay == 1);
            Assert.IsTrue(parserResult.days[0].endDay == 10);
            Assert.IsTrue(parserResult.months.Length == 1);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Jan);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseDayRange_WhenMultipeDatesAndSingleTime()
        {
            var result = Parser.Agent.Run("FEB 01-10 13-15 0100-0200");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.DaysRange);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 1);
            Assert.IsTrue(parserResult.minutes[0].start == 60);
            Assert.IsTrue(parserResult.minutes[0].finish == 120);
            Assert.IsTrue(parserResult.days.Length == 2);
            Assert.IsTrue(parserResult.days[0].startDay == 1);
            Assert.IsTrue(parserResult.days[0].endDay == 10);
            Assert.IsTrue(parserResult.days[1].startDay == 13);
            Assert.IsTrue(parserResult.days[1].endDay == 15);
            Assert.IsTrue(parserResult.months.Length == 1);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Feb);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseDayRange_WhenSingleDateAndMultiTimes()
        {
            var result = Parser.Agent.Run("MAR 01-10 0100-0200 0300-0400");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.DaysRange);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 2);
            Assert.IsTrue(parserResult.minutes[0].start == 60);
            Assert.IsTrue(parserResult.minutes[0].finish == 120);
            Assert.IsTrue(parserResult.minutes[1].start == 180);
            Assert.IsTrue(parserResult.minutes[1].finish == 240);
            Assert.IsTrue(parserResult.days.Length == 1);
            Assert.IsTrue(parserResult.days[0].startDay == 1);
            Assert.IsTrue(parserResult.days[0].endDay == 10);
            Assert.IsTrue(parserResult.months.Length == 1);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Mar);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseDayRange_WhenSingleDateAndMultiTimesUsingSunriseAndSunset()
        {
            var result = Parser.Agent.Run("APR 01-10 0100-0200 0300-0400 SR-SS");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.DaysRange);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 3);
            Assert.IsTrue(parserResult.minutes[0].start == 60);
            Assert.IsTrue(parserResult.minutes[0].finish == 120);
            Assert.IsTrue(parserResult.minutes[1].start == 180);
            Assert.IsTrue(parserResult.minutes[1].finish == 240);
            Assert.IsTrue(parserResult.minutes[2].start == Common._SUNRISE_IN_MINUTES_);
            Assert.IsTrue(parserResult.minutes[2].finish == Common._SUNSET_IN_MINUTES_);
            Assert.IsTrue(parserResult.days.Length == 1);
            Assert.IsTrue(parserResult.days[0].startDay == 1);
            Assert.IsTrue(parserResult.days[0].endDay == 10);
            Assert.IsTrue(parserResult.months.Length == 1);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Apr);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseDayRange_WhenSingleDateAndMultiTimesUsingSunriseAndSunsetWithPlusAndMinus()
        {
            var result = Parser.Agent.Run("MAY 01-10 0100-0200 0300-0400 SR MINUS10-SS PLUS20");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.DaysRange);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 3);
            Assert.IsTrue(parserResult.minutes[0].start == 60);
            Assert.IsTrue(parserResult.minutes[0].finish == 120);
            Assert.IsTrue(parserResult.minutes[1].start == 180);
            Assert.IsTrue(parserResult.minutes[1].finish == 240);
            Assert.IsTrue(parserResult.minutes[2].start == Common._SUNRISE_IN_MINUTES_ + 10);
            Assert.IsTrue(parserResult.minutes[2].finish == Common._SUNSET_IN_MINUTES_ + 20);
            Assert.IsTrue(parserResult.days.Length == 1);
            Assert.IsTrue(parserResult.days[0].startDay == 1);
            Assert.IsTrue(parserResult.days[0].endDay == 10);
            Assert.IsTrue(parserResult.months.Length == 1);
            Assert.IsTrue(parserResult.months[0] == Common.Month.May);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseDayRange_WhenMultiDatesAndMultiTimes()
        {
            var result = Parser.Agent.Run("JUN 01-10 13-15 0100-0200 SR-SS PLUS30");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.DaysRange);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 2);
            Assert.IsTrue(parserResult.minutes[0].start == 60);
            Assert.IsTrue(parserResult.minutes[0].finish == 120);
            Assert.IsTrue(parserResult.minutes[1].start == Common._SUNRISE_IN_MINUTES_);
            Assert.IsTrue(parserResult.minutes[1].finish == Common._SUNSET_IN_MINUTES_ + 30);
            Assert.IsTrue(parserResult.days.Length == 2);
            Assert.IsTrue(parserResult.days[0].startDay == 1);
            Assert.IsTrue(parserResult.days[0].endDay == 10);
            Assert.IsTrue(parserResult.days[1].startDay == 13);
            Assert.IsTrue(parserResult.days[1].endDay == 15);
            Assert.IsTrue(parserResult.months.Length == 1);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Jun);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseDayRange_WhenMultiDatesAndMultiTimesWithSpaces()
        {
            var result = Parser.Agent.Run(" JUL  01 -10 13- 15 0100 -0200   0300  - 0400 SR MINUS10 - SS PLUS20");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.DaysRange);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 3);
            Assert.IsTrue(parserResult.minutes[0].start == 60);
            Assert.IsTrue(parserResult.minutes[0].finish == 120);
            Assert.IsTrue(parserResult.minutes[1].start == 180);
            Assert.IsTrue(parserResult.minutes[1].finish == 240);
            Assert.IsTrue(parserResult.minutes[2].start == Common._SUNRISE_IN_MINUTES_ + 10);
            Assert.IsTrue(parserResult.minutes[2].finish == Common._SUNSET_IN_MINUTES_ + 20);
            Assert.IsTrue(parserResult.days.Length == 2);
            Assert.IsTrue(parserResult.days[0].startDay == 1);
            Assert.IsTrue(parserResult.days[0].endDay == 10);
            Assert.IsTrue(parserResult.days[1].startDay == 13);
            Assert.IsTrue(parserResult.days[1].endDay == 15);
            Assert.IsTrue(parserResult.months.Length == 1);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Jul);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseDayRange_WhenMultiDatesAndMultiTimesWithSpacesAndEcluded()
        {
            var result = Parser.Agent.Run(" JUL  01 -10 13- 15 0100 -0200   0300  - 0400 SR MINUS10 - SS PLUS20");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.DaysRange);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 3);
            Assert.IsTrue(parserResult.minutes[0].start == 60);
            Assert.IsTrue(parserResult.minutes[0].finish == 120);
            Assert.IsTrue(parserResult.minutes[1].start == 180);
            Assert.IsTrue(parserResult.minutes[1].finish == 240);
            Assert.IsTrue(parserResult.minutes[2].start == Common._SUNRISE_IN_MINUTES_ + 10);
            Assert.IsTrue(parserResult.minutes[2].finish == Common._SUNSET_IN_MINUTES_ + 20);
            Assert.IsTrue(parserResult.days.Length == 2);
            Assert.IsTrue(parserResult.days[0].startDay == 1);
            Assert.IsTrue(parserResult.days[0].endDay == 10);
            Assert.IsTrue(parserResult.days[1].startDay == 13);
            Assert.IsTrue(parserResult.days[1].endDay == 15);
            Assert.IsTrue(parserResult.months.Length == 1);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Jul);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDayRange_WhenFirstRangeDayGreatherThan31()
        {
            var result = Parser.Agent.Run("JUN 32-01 13-15 0100-0200 SR-SS PLUS30");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Contains("Day between 1 and 31 Parameter name") );
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDayRange_WhenSecondRangeDayGreatherThan31()
        {
            var result = Parser.Agent.Run("JUN 01-32 13-15 0100-0200 SR-SS PLUS30");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Contains("Day between 1 and 31 Parameter name"));
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDayRange_WhenSingleDayGreatherThan31()
        {
            var result = Parser.Agent.Run("JUN 32 0100-0200 SR-SS PLUS30");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Contains("Day between 1 and 31 Parameter name"));
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDayRange_WhenRangeHourGreatherThan23()
        {
            var result = Parser.Agent.Run("JUN 01-23 28 2400-0200 SR-SS PLUS30");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Contains("Hours between 0 and 23 Parameter name"));
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDayRange_WhenRangeMinuteGreatherThan59()
        {
            var result = Parser.Agent.Run("JUN 01-23 28 0160-0200 SR-SS PLUS30");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Contains("Minutes between 0 and 59 Parameter name"));
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDayRange_WhenMonthIsMissing()
        {
            var result = Parser.Agent.Run("01-10 13-15 0100-0200 SR-SS PLUS30");

            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDayRange_WhenMonthIsMisspelled()
        {
            var result = Parser.Agent.Run("FEBR 01-10 13-15 0100-0200 SR-SS PLUS30");

            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDayRange_WhenDaysAreMissing()
        {
            var result = Parser.Agent.Run("DEC 0100-0200 SR-SS PLUS30");

            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDayRange_WhenStartRangeDayIsMissing()
        {
            var result = Parser.Agent.Run("DEC -10 0100-0200 SR-SS PLUS30");

            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDayRange_WhenStartRangeTimeIsMissing()
        {
            var result = Parser.Agent.Run("DEC 01-10 13-15 -0200");

            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDayRange_WhenRangeTimeHasOnePerdiod()
        {
            var result = Parser.Agent.Run("DEC 01-10 13-15 0100");

            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }
        #endregion

        #region Subcase Continues
        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseContinues_WhenRangeDateAndTimeWith4Digits()
        {
            var result = Parser.Agent.Run("AUG 10 0100-SEP 23 0120");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.Continues);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 1);
            Assert.IsTrue(parserResult.minutes[0].start == 60);
            Assert.IsTrue(parserResult.minutes[0].finish == 80);
            Assert.IsTrue(parserResult.days.Length == 1);
            Assert.IsTrue(parserResult.days[0].startDay == 10);
            Assert.IsTrue(parserResult.days[0].endDay == 23);
            Assert.IsTrue(parserResult.months.Length == 2);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Aug);
            Assert.IsTrue(parserResult.months[1] == Common.Month.Sep);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseContinues_WhenRangeDateAndTimeWith4DigitsAndSunset()
        {
            var result = Parser.Agent.Run("OCT 10 0100-NOV 23 SS");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.Continues);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 1);
            Assert.IsTrue(parserResult.minutes[0].start == 60);
            Assert.IsTrue(parserResult.minutes[0].finish == Common._SUNSET_IN_MINUTES_);
            Assert.IsTrue(parserResult.days.Length == 1);
            Assert.IsTrue(parserResult.days[0].startDay == 10);
            Assert.IsTrue(parserResult.days[0].endDay == 23);
            Assert.IsTrue(parserResult.months.Length == 2);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Oct);
            Assert.IsTrue(parserResult.months[1] == Common.Month.Nov);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseContinues_WhenRangeDateAndTimeSunriseAndSunset()
        {
            var result = Parser.Agent.Run("DEC 10 SR-JAN 23 SS");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.Continues);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 1);
            Assert.IsTrue(parserResult.minutes[0].start == Common._SUNRISE_IN_MINUTES_);
            Assert.IsTrue(parserResult.minutes[0].finish == Common._SUNSET_IN_MINUTES_);
            Assert.IsTrue(parserResult.days.Length == 1);
            Assert.IsTrue(parserResult.days[0].startDay == 10);
            Assert.IsTrue(parserResult.days[0].endDay == 23);
            Assert.IsTrue(parserResult.months.Length == 2);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Dec);
            Assert.IsTrue(parserResult.months[1] == Common.Month.Jan);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseContinues_WhenRangeDateAndTimeSunriseMinusAndSunsetPlus()
        {
            var result = Parser.Agent.Run("FEB 10 SR MINUS10-MAR 23 SS PLUS20");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.Continues);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 1);
            Assert.IsTrue(parserResult.minutes[0].start == Common._SUNRISE_IN_MINUTES_ + 10);
            Assert.IsTrue(parserResult.minutes[0].finish == Common._SUNSET_IN_MINUTES_ + 20);
            Assert.IsTrue(parserResult.days.Length == 1);
            Assert.IsTrue(parserResult.days[0].startDay == 10);
            Assert.IsTrue(parserResult.days[0].endDay == 23);
            Assert.IsTrue(parserResult.months.Length == 2);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Feb);
            Assert.IsTrue(parserResult.months[1] == Common.Month.Mar);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseDayRange_WhenExcludedAndSingleRangeDateAnd()
        {
            var result = Parser.Agent.Run("EXC FEB 10-23");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.DaysRange);
            Assert.IsFalse(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 0);
            Assert.IsTrue(parserResult.days.Length == 1);
            Assert.IsTrue(parserResult.days[0].startDay == 10);
            Assert.IsTrue(parserResult.days[0].endDay == 23);
            Assert.IsTrue(parserResult.months.Length == 1);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Feb);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseContinues_WhenRangeDate()
        {
            var result = Parser.Agent.Run("EXC FEB 10-MAR 23");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.Continues);
            Assert.IsFalse(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 0);
            Assert.IsTrue(parserResult.days.Length == 1);
            Assert.IsTrue(parserResult.days[0].startDay == 10);
            Assert.IsTrue(parserResult.days[0].endDay == 23);
            Assert.IsTrue(parserResult.months.Length == 2);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Feb);
            Assert.IsTrue(parserResult.months[1] == Common.Month.Mar);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingContinues_WhenFirstDayGreatherThan31()
        {
            var result = Parser.Agent.Run("DEC 32 SR-JAN 23 SS");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Contains("Day between 1 and 31 Parameter name"));
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingContinues_WhenSecondDayGreatherThan31()
        {
            var result = Parser.Agent.Run("DEC 10 SR-JAN 32 SS");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingContinues_WhenRangeHourGreatherThan23()
        {
            var result = Parser.Agent.Run("DEC 10 2500 23 SS");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Contains("Hours between 0 and 23 Parameter name"));
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingContinues_WhenRangeMinuteGreatherThan59()
        {
            var result = Parser.Agent.Run("DEC 10 2260-JAN 23 SS");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Contains("Minutes between 0 and 59 Parameter name"));
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingContinues_WhenMonthIsMissing()
        {
            var result = Parser.Agent.Run("10 SR-JAN 23 SS");

            Assert.IsTrue(result.IsFailure);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingContinues_WhenMonthIsMisspelled()
        {
            var result = Parser.Agent.Run("DIC 10 SR-JAN 23 SS");

            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingContinues_WhenStartDayIsMissing()
        {
            var result = Parser.Agent.Run("DEC SR-JAN 23 SS");

            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingContinues_WhenEndDayIsMissing()
        {
            var result = Parser.Agent.Run("DEC 10 SR - JAN SS ");

            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingContinues_WhenStartTimeIsMissing()
        {
            var result = Parser.Agent.Run("DEC 10 23 SS");

            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingContinues_WhenEndTimeIsMissing()
        {
            var result = Parser.Agent.Run("DEC 10 SR-JAN 23");

            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        #endregion

        #region Subcase Distinct

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseDistinct_WhenSingleDateAndSingleTime()
        {
            var result = Parser.Agent.Run("DEC 10 0100-0200");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.Distinct);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 1);
            Assert.IsTrue(parserResult.minutes[0].start == 60);
            Assert.IsTrue(parserResult.minutes[0].finish == 120);
            Assert.IsTrue(parserResult.days.Length == 1);
            Assert.IsTrue(parserResult.days[0].startDay == 10);
            Assert.IsTrue(parserResult.days[0].endDay == 10);
            Assert.IsTrue(parserResult.months.Length == 1);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Dec);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseDistinct_WhenMultipeDatesAndSingleTime()
        {
            var result = Parser.Agent.Run("AUG 10 11 0100-0200");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.Distinct);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 1);
            Assert.IsTrue(parserResult.minutes[0].start == 60);
            Assert.IsTrue(parserResult.minutes[0].finish == 120);
            Assert.IsTrue(parserResult.days.Length == 2);
            Assert.IsTrue(parserResult.days[0].startDay == 10);
            Assert.IsTrue(parserResult.days[0].endDay == 10);
            Assert.IsTrue(parserResult.days[1].startDay == 11);
            Assert.IsTrue(parserResult.days[1].endDay == 11);
            Assert.IsTrue(parserResult.months.Length == 1);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Aug);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseDistinct_WhenSingleDateAndMultiTimes()
        {
            var result = Parser.Agent.Run("AUG 10 0100-0200 0300-0400");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.Distinct);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 2);
            Assert.IsTrue(parserResult.minutes[0].start == 60);
            Assert.IsTrue(parserResult.minutes[0].finish == 120);
            Assert.IsTrue(parserResult.minutes[1].start == 180);
            Assert.IsTrue(parserResult.minutes[1].finish == 240);
            Assert.IsTrue(parserResult.days.Length == 1);
            Assert.IsTrue(parserResult.days[0].startDay == 10);
            Assert.IsTrue(parserResult.days[0].endDay == 10);
            Assert.IsTrue(parserResult.months.Length == 1);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Aug);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseDistinct_WhenSingleDateAndMultiTimesUsingSunriseAndSunset()
        {
            var result = Parser.Agent.Run("APR 10 0100-0200 0300-0400 SR-SS");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.Distinct);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 3);
            Assert.IsTrue(parserResult.minutes[0].start == 60);
            Assert.IsTrue(parserResult.minutes[0].finish == 120);
            Assert.IsTrue(parserResult.minutes[1].start == 180);
            Assert.IsTrue(parserResult.minutes[1].finish == 240);
            Assert.IsTrue(parserResult.minutes[2].start == Common._SUNRISE_IN_MINUTES_);
            Assert.IsTrue(parserResult.minutes[2].finish == Common._SUNSET_IN_MINUTES_);
            Assert.IsTrue(parserResult.days.Length == 1);
            Assert.IsTrue(parserResult.days[0].startDay == 10);
            Assert.IsTrue(parserResult.days[0].endDay == 10);
            Assert.IsTrue(parserResult.months.Length == 1);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Apr);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseDistinct_WhenSingleDateAndMultiTimesUsingSunriseAndSunsetWithPlusAndMinus()
        {
            var result = Parser.Agent.Run("APR 10 0100-0200 0300-0400 SR MINUS10-SS PLUS20");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.Distinct);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 3);
            Assert.IsTrue(parserResult.minutes[0].start == 60);
            Assert.IsTrue(parserResult.minutes[0].finish == 120);
            Assert.IsTrue(parserResult.minutes[1].start == 180);
            Assert.IsTrue(parserResult.minutes[1].finish == 240);
            Assert.IsTrue(parserResult.minutes[2].start == Common._SUNRISE_IN_MINUTES_ + 10);
            Assert.IsTrue(parserResult.minutes[2].finish == Common._SUNSET_IN_MINUTES_ + 20);
            Assert.IsTrue(parserResult.days.Length == 1);
            Assert.IsTrue(parserResult.days[0].startDay == 10);
            Assert.IsTrue(parserResult.days[0].endDay == 10);
            Assert.IsTrue(parserResult.months.Length == 1);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Apr);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseDistinct_WhenMultiDatesAndMultiTimes()
        {
            var result = Parser.Agent.Run("APR 10 12 0100-0200 0300-0400 SR MINUS10-SS PLUS20");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.Distinct);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 3);
            Assert.IsTrue(parserResult.minutes[0].start == 60);
            Assert.IsTrue(parserResult.minutes[0].finish == 120);
            Assert.IsTrue(parserResult.minutes[1].start == 180);
            Assert.IsTrue(parserResult.minutes[1].finish == 240);
            Assert.IsTrue(parserResult.minutes[2].start == Common._SUNRISE_IN_MINUTES_ + 10);
            Assert.IsTrue(parserResult.minutes[2].finish == Common._SUNSET_IN_MINUTES_ + 20);
            Assert.IsTrue(parserResult.days.Length == 2);
            Assert.IsTrue(parserResult.days[0].startDay == 10);
            Assert.IsTrue(parserResult.days[0].endDay == 10);
            Assert.IsTrue(parserResult.days[1].startDay == 12);
            Assert.IsTrue(parserResult.days[1].endDay == 12);
            Assert.IsTrue(parserResult.months.Length == 1);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Apr);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseDistinct_WhenMultiDatesAndMultiTimesWithSpaces()
        {
            var result = Parser.Agent.Run(" APR  10    12  0100 -   0200 0300 -0400 SR MINUS10 - SS PLUS20");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.Distinct);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 3);
            Assert.IsTrue(parserResult.minutes[0].start == 60);
            Assert.IsTrue(parserResult.minutes[0].finish == 120);
            Assert.IsTrue(parserResult.minutes[1].start == 180);
            Assert.IsTrue(parserResult.minutes[1].finish == 240);
            Assert.IsTrue(parserResult.minutes[2].start == Common._SUNRISE_IN_MINUTES_ + 10);
            Assert.IsTrue(parserResult.minutes[2].finish == Common._SUNSET_IN_MINUTES_ + 20);
            Assert.IsTrue(parserResult.days.Length == 2);
            Assert.IsTrue(parserResult.days[0].startDay == 10);
            Assert.IsTrue(parserResult.days[0].endDay == 10);
            Assert.IsTrue(parserResult.days[1].startDay == 12);
            Assert.IsTrue(parserResult.days[1].endDay == 12);
            Assert.IsTrue(parserResult.months.Length == 1);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Apr);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseDistinct_WhenMultiDatesWithSpacesAndEcluded()
        {
            var result = Parser.Agent.Run(" EXC APR  10    12  ");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.Distinct);
            Assert.IsFalse(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 0);
            Assert.IsTrue(parserResult.days.Length == 2);
            Assert.IsTrue(parserResult.days[0].startDay == 10);
            Assert.IsTrue(parserResult.days[0].endDay == 10);
            Assert.IsTrue(parserResult.days[1].startDay == 12);
            Assert.IsTrue(parserResult.days[1].endDay == 12);
            Assert.IsTrue(parserResult.months.Length == 1);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Apr);
        }


        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDistinct_WhenDayGreatherThan31()
        {
            var result = Parser.Agent.Run("APR 32 0100-0200 0300-0400 SR-SS");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Contains("Day between 1 and 31 Parameter name"));
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDistinct_WhenRangeHourGreatherThan23()
        {
            var result = Parser.Agent.Run("APR 31 2400-0200 0300-0400 SR-SS");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Contains("Hours between 0 and 23 Parameter name"));
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParseSubcase3_WhenRangeMinuteGreatherThan59()
        {
            var result = Parser.Agent.Run("APR 31 2260-0200 0300-0400 SR-SS");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Contains("Minutes between 0 and 59 Parameter name"));
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDistinct_WhenMonthIsMissing()
        {
            var result = Parser.Agent.Run("10 12 0100-0200 0300-0400 SR MINUS10-SS PLUS20");

            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDistinct_WhenMonthIsMisspelled()
        {
            var result = Parser.Agent.Run("JIN 0 12 0100-0200 0300-0400 SR MINUS10-SS PLUS20");

            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDistinct_WhenDaysAreMissing()
        {
            var result = Parser.Agent.Run("JAN 0100-0200 0300-0400 SR MINUS10-SS PLUS20");

            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDistinct_WhenStartRangeTimeIsMissing()
        {
            var result = Parser.Agent.Run("JAN 10");

            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDistinct_WhenRangeTimeHasOnePerdiod()
        {
            var result = Parser.Agent.Run("DEC 01-10 13");

            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        #endregion

        #region Subcase DistinctAndDayRange
        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseDistinctAndDayRange_WhenSingleRangeDateAndSingleDayWithSingleTime()
        {
            var result = Parser.Agent.Run("DEC 11-13 15 0100-0200");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.DistinctAndDaysRange);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 1);
            Assert.IsTrue(parserResult.minutes[0].start == 60);
            Assert.IsTrue(parserResult.minutes[0].finish == 120);
            Assert.IsTrue(parserResult.days.Length == 2);
            Assert.IsTrue(parserResult.days[0].startDay == 11);
            Assert.IsTrue(parserResult.days[0].endDay == 13);
            Assert.IsTrue(parserResult.days[1].startDay == 15);
            Assert.IsTrue(parserResult.days[1].endDay == 15);
            Assert.IsTrue(parserResult.months.Length == 1);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Dec);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseDistinct_WhenMultipleRangeDateAndExcluded()
        {
            var result = Parser.Agent.Run("EXC DEC 11 12");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.Distinct);
            Assert.IsFalse(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 0);
            Assert.IsTrue(parserResult.days.Length == 2);
            Assert.IsTrue(parserResult.days[0].startDay == 11);
            Assert.IsTrue(parserResult.days[0].endDay == 11);
            Assert.IsTrue(parserResult.days[1].startDay == 12);
            Assert.IsTrue(parserResult.days[1].endDay == 12);
            Assert.IsTrue(parserResult.months.Length == 1);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Dec);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseDistinctAndDayRange_WhenMultipleRangeDateAndSingleDayWithSingleTime()
        {
            var result = Parser.Agent.Run("DEC 11-13 15-21 22 0100-0200");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.DistinctAndDaysRange);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 1);
            Assert.IsTrue(parserResult.minutes[0].start == 60);
            Assert.IsTrue(parserResult.minutes[0].finish == 120);
            Assert.IsTrue(parserResult.days.Length == 3);
            Assert.IsTrue(parserResult.days[0].startDay == 11);
            Assert.IsTrue(parserResult.days[0].endDay == 13);
            Assert.IsTrue(parserResult.days[1].startDay == 15);
            Assert.IsTrue(parserResult.days[1].endDay == 21);
            Assert.IsTrue(parserResult.days[2].startDay == 22);
            Assert.IsTrue(parserResult.days[2].endDay == 22);
            Assert.IsTrue(parserResult.months.Length == 1);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Dec);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseDistinctAndDayRange_WhenMultipleRangeDateAndSingleDayWithMultipleTimes()
        {
            var result = Parser.Agent.Run("DEC 11-13 15-21 22 0100-0200 0300-0400");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.DistinctAndDaysRange);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 2);
            Assert.IsTrue(parserResult.minutes[0].start == 60);
            Assert.IsTrue(parserResult.minutes[0].finish == 120);
            Assert.IsTrue(parserResult.minutes[1].start == 180);
            Assert.IsTrue(parserResult.minutes[1].finish == 240);
            Assert.IsTrue(parserResult.days.Length == 3);
            Assert.IsTrue(parserResult.days[0].startDay == 11);
            Assert.IsTrue(parserResult.days[0].endDay == 13);
            Assert.IsTrue(parserResult.days[1].startDay == 15);
            Assert.IsTrue(parserResult.days[1].endDay == 21);
            Assert.IsTrue(parserResult.days[2].startDay == 22);
            Assert.IsTrue(parserResult.days[2].endDay == 22);
            Assert.IsTrue(parserResult.months.Length == 1);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Dec);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseDistinctAndDayRange_WhenMultipleRangeDateAndSingleDayWithMultipleTimesWithSunriseSunset()
        {
            var result = Parser.Agent.Run("DEC 11-13 15-21 22 0100-0200 SR-SS");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.DistinctAndDaysRange);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 2);
            Assert.IsTrue(parserResult.minutes[0].start == 60);
            Assert.IsTrue(parserResult.minutes[0].finish == 120);
            Assert.IsTrue(parserResult.minutes[1].start == Common._SUNRISE_IN_MINUTES_);
            Assert.IsTrue(parserResult.minutes[1].finish == Common._SUNSET_IN_MINUTES_);
            Assert.IsTrue(parserResult.days.Length == 3);
            Assert.IsTrue(parserResult.days[0].startDay == 11);
            Assert.IsTrue(parserResult.days[0].endDay == 13);
            Assert.IsTrue(parserResult.days[1].startDay == 15);
            Assert.IsTrue(parserResult.days[1].endDay == 21);
            Assert.IsTrue(parserResult.days[2].startDay == 22);
            Assert.IsTrue(parserResult.days[2].endDay == 22);
            Assert.IsTrue(parserResult.months.Length == 1);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Dec);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseDistinctAndDayRange_WhenMultipleRangeDateAndSingleDayWithMultipleTimesWithSunriseMinusSunsetPlus()
        {
            var result = Parser.Agent.Run("DEC 11-13 15-21 22 0100-0200 SR MINUS10-SS PLUS20");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.DistinctAndDaysRange);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 2);
            Assert.IsTrue(parserResult.minutes[0].start == 60);
            Assert.IsTrue(parserResult.minutes[0].finish == 120);
            Assert.IsTrue(parserResult.minutes[1].start == Common._SUNRISE_IN_MINUTES_ + 10);
            Assert.IsTrue(parserResult.minutes[1].finish == Common._SUNSET_IN_MINUTES_ + 20);
            Assert.IsTrue(parserResult.days.Length == 3);
            Assert.IsTrue(parserResult.days[0].startDay == 11);
            Assert.IsTrue(parserResult.days[0].endDay == 13);
            Assert.IsTrue(parserResult.days[1].startDay == 15);
            Assert.IsTrue(parserResult.days[1].endDay == 21);
            Assert.IsTrue(parserResult.days[2].startDay == 22);
            Assert.IsTrue(parserResult.days[2].endDay == 22);
            Assert.IsTrue(parserResult.months.Length == 1);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Dec);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldParseDistinctAndDayRange_WhenMultipleRangeDateAndMultipleDayWithMultipleTimesWithSunriseMinusSunsetPlus()
        {
            var result = Parser.Agent.Run("DEC 11-13 15-21 22 23 0100-0200 SR MINUS10-SS PLUS20");

            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.Calendar);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.DistinctAndDaysRange);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 2);
            Assert.IsTrue(parserResult.minutes[0].start == 60);
            Assert.IsTrue(parserResult.minutes[0].finish == 120);
            Assert.IsTrue(parserResult.minutes[1].start == Common._SUNRISE_IN_MINUTES_ + 10);
            Assert.IsTrue(parserResult.minutes[1].finish == Common._SUNSET_IN_MINUTES_ + 20);
            Assert.IsTrue(parserResult.days.Length == 4);
            Assert.IsTrue(parserResult.days[0].startDay == 11);
            Assert.IsTrue(parserResult.days[0].endDay == 13);
            Assert.IsTrue(parserResult.days[1].startDay == 15);
            Assert.IsTrue(parserResult.days[1].endDay == 21);
            Assert.IsTrue(parserResult.days[2].startDay == 22);
            Assert.IsTrue(parserResult.days[2].endDay == 22);
            Assert.IsTrue(parserResult.days[3].startDay == 23);
            Assert.IsTrue(parserResult.days[3].endDay == 23);
            Assert.IsTrue(parserResult.months.Length == 1);
            Assert.IsTrue(parserResult.months[0] == Common.Month.Dec);
        }

        public void SchedulerParser_Calendar_ShouldFailParseDistinctAndDayRange_WhenFirstRangeDayGreatherThan31()
        {
            var result = Parser.Agent.Run("DEC 32-13 15-21 22 0100-0200 SR-SS");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Contains("Day between 1 and 31 Parameter name"));
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDistinctAndDayRange_WhenSecondRangeDayGreatherThan31()
        {
            var result = Parser.Agent.Run("DEC 31-32 15-21 22 0100-0200 SR-SS");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Contains("Day between 1 and 31 Parameter name"));
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDistinctAndDayRange_WhenSingleDayGreatherThan31()
        {
            var result = Parser.Agent.Run("DEC 32 0100-0200 SR-SS");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Contains("Day between 1 and 31 Parameter name"));
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDistinctAndDayRange_WhenRangeHourGreatherThan23()
        {
            var result = Parser.Agent.Run("DEC 15-21 2400-0200 SR-SS");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Contains("Hours between 0 and 23 Parameter name"));
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDistinctAndDayRange_WhenRangeMinuteGreatherThan59()
        {
            var result = Parser.Agent.Run("DEC 31-13 0160-0200 SR-SS");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Contains("Minutes between 0 and 59 Parameter name"));
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDistinctAndDayRange_WhenMonthIsMissing()
        {
            var result = Parser.Agent.Run("EXC 11-13 15-21 22 23 0100-0200 SR MINUS10-SS PLUS20");

            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDistinctAndDayRange_WhenMonthIsMisspelled()
        {
            var result = Parser.Agent.Run("FEBR 11-13 15-21 22 23 0100-0200 SR MINUS10-SS PLUS20");

            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDistinctAndDayRange_WhenDaysAreMissing()
        {
            var result = Parser.Agent.Run("FEB 0100-0200 SR MINUS10-SS PLUS20");

            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDistinctAndDayRange_WhenStartRangeDayIsMissing()
        {
            var result = Parser.Agent.Run("FEB -13 0100-0200 SR MINUS10-SS PLUS20");

            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);

        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDistinctAndDayRange_WhenStartRangeTimeIsMissing()
        {
            var result = Parser.Agent.Run("FEB 11-13");

            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);

        }

        [TestMethod]
        public void SchedulerParser_Calendar_ShouldFailParsingDistinctAndDayRange_WhenRangeTimeHasOnePerdiod()
        {
            var result = Parser.Agent.Run("FEB 11-13 0100");

            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        #endregion
    }
}

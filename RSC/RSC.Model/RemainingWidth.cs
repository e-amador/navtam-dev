﻿namespace RSC.Model
{
    /// <summary>
    /// Remaining width of the runway
    /// </summary>
    public class RemainingWidth
    {
        /// <summary>
        /// Remaining width of the runway
        /// </summary>
        public ContaminantFtIns ContaminantFI { get; set; }
    }
}
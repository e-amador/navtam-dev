﻿module Utils.Libs.SchedulerParser.TimeOnly

open FParsec
open FParsec.Pipes
open Common

type Parser ()=
    static member private H24OrDailyOnly : Parser<ParserResult, unit> = 
        %% ws -- %["H24"; "DAILY"] -%> 
            { 
                case = TimeOnly; 
                subcase = None;
                months = [||];
                included = true; 
                days = [||];
                minutes = [|{start = 0; finish = 1440}|] 
            }

    static member private MultiplePeriodInMinutes : Parser<ParserResult, unit> = 
        Filter.SinglePeriodInMinutes * qty.[1..] |>> 
            (fun x -> 
                { 
                    case = TimeOnly; 
                    subcase = None;
                    included = true; 
                    months = [||];
                    days = [||];
                    minutes = x.ToArray() 
                })  

    static member Pipeline : Parser<ParserResult, unit> = 
        %[Parser.MultiplePeriodInMinutes; Parser.H24OrDailyOnly]

    static member Parse( str : string ) : ParserResult<ParserResult, unit> = 
        run Parser.Pipeline str




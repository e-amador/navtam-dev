﻿using System.Threading.Tasks;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NAVTAM.ViewModels;
using System.Collections.Generic;
using System.Data.Entity.Spatial;

namespace NAVTAM.NsdEngine
{
    public interface INsdManager
    {
        string CategoryName { get; }
        string Version { get; }
        string FullyQualifiedModelName { get; }
        ProposalViewModel CreateViewModel();
        ProposalViewModel CreateViewModelFromProposal(Proposal proposal);
        void FillProposalFromViewModel(Proposal proposal, ProposalViewModel ToViewModel);
        Task UpdateContext(ProposalViewModel model, ProposalContext context);
        Task<ValidationResult> Validate(ProposalViewModel model, ProposalContext context);
        Task<bool> GenerateIcao(ProposalViewModel model, ProposalContext context);
        string SerializeObject(object obj);
        TokenViewModel CopyTokenForGroupCancellation(TokenViewModel master, TokenViewModel current);
        DbGeography GetProposalLocation(ProposalViewModel model);
    }
}

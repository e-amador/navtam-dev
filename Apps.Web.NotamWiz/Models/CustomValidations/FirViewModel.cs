﻿namespace NavCanada.Applications.NotamWiz.Web.Models.CustomValidations
{
    public class FirViewModel
    {
        public string Id
        {
            get;
            set;
        }

        public string Designator
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }
    }
}
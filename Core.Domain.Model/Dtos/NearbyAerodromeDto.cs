﻿using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Core.Domain.Model.Dtos
{
    public class NearbyAerodromeDto
    {
        public string CodeId { get; set; }
        public double Distance { get; set; }
        public DisseminationCategory DisseminationCategory { get; set; }
        public SdoCache SdoCache { get; set; }
    }
}

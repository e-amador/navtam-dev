﻿namespace RSC.Model.Enums
{
    /// <summary>
    /// The side of the centreline for the current edge of the cleared area. Either E-W
    /// or N-S the latter enumerations depending upon exact runway orientation: Low end # from 05 to 13
    /// use N/S; anything else use E/W.
    /// </summary>
    public enum OffsetSideEnum : int
    {
        East = 11,
        West = 12,
        North = 13,
        South = 14
    }
}

﻿using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.Controllers
{
    using System.Threading.Tasks;
    using System.Web.Mvc;

    using log4net;

    using Microsoft.AspNet.Identity;

    using NavCanada.Applications.NotamWiz.Web.Code.Helpers;
    using NavCanada.Core.Common.Common;
    using NavCanada.Core.Common.Contracts;
    using NavCanada.Core.Data.NotamWiz.Contracts;
    using NavCanada.Core.Domain.Model.Entitities;

    public class HomeController : BaseController
    {
        readonly ILog _logger = LogManager.GetLogger(typeof(HomeController));

        public HomeController(IUow uow, IClusterQueues clusterQueues) : base(uow, clusterQueues)
        {
        }

        [AllowAnonymous]
        public async Task<ActionResult> Index()
        {
            this._logger.Info("user:" + RequestHelpers.GetClientIpAddress(Request) + " Visited us.");

            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            if (Request.IsAuthenticated && Request.UrlReferrer == null)
            {
                // if the user credentials are still valid, redirect to the role's default page
                return await RedirectToRolePage();
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult KeepAlive()
        {
            return new ContentResult { Content = "OK", ContentType = "text/plain" };
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        private async Task<ActionResult> RedirectToRolePage()
        {
            var userId = User.Identity != null ? User.Identity.GetUserId() : null;
            if (!string.IsNullOrEmpty(userId))
            {
                var user = await Uow.UserProfileRepo.GetByIdAsync(User.Identity.GetUserId(), false);
                if (user != null)
                {
                    return RedirectToRolePage(user);
                }
            }
            return View();
        }

        private ActionResult RedirectToRolePage(UserProfile user)
        {
            //if admin -> redirect to mfwic page                       
            if (UserManager.IsInRole(user.Id, CommonDefinitions.AdminRole) || UserManager.IsInRole(user.Id, CommonDefinitions.CcsRole) || UserManager.IsInRole(user.Id, CommonDefinitions.OrgAdminRole))
            {
                return RedirectToAction("Index", "AdminHome", new { area = "Admin" });
            }
            //if not -> redirect to dashboard
            return RedirectToAction("Index", "Dashboard");
        }
    }
}
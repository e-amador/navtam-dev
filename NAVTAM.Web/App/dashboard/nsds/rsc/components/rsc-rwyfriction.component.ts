﻿import { Component, OnInit, OnChanges, OnDestroy, Inject, Input } from '@angular/core';
import { TranslationService } from 'angular-l10n';
import { FormGroup, FormBuilder, FormArray, AbstractControl, Validators } from '@angular/forms'
import { IRwyFrictionModel, IRwyFriction } from '../rsc.model'
import { RscService } from '../rsc.service'

import {
} from '../../../shared/data.model';

import * as moment from 'moment';
import { TOASTR_TOKEN, Toastr } from '../../../../common/toastr.service';
import { Select2OptionData } from 'ng2-select2';

@Component({
    selector: 'rsc-rwyfriction',
    templateUrl: './app/dashboard/nsds/rsc/components/rsc-rwyfriction.component.html'
})
export class RwyFrictionComponent implements OnInit, OnChanges, OnDestroy {
    @Input('form') mainForm: FormGroup = null;
    @Input('isReadOnly') isReadOnly: boolean = false;
    @Input('model') model: IRwyFrictionModel = null;
    @Input('isAverage') isAverage: boolean = false;

    frmGroup: FormGroup;
    isFormLoadingData: boolean;

    measureDateMoment: any;
    measureDateText: string = "";
    dateFormat: string = "YYMMDDHHmm";

    coefficients: Select2OptionData[];
    coefficientsOptions = {
        width: "100%",
        placeholder: {
            id: '-1',
            text: this.translation.translate('RwyFrictionComponent.SelectCoefficient')
        }
    }

    constructor(
        private fb: FormBuilder,
        public translation: TranslationService,
        private rscService: RscService,
        @Inject(TOASTR_TOKEN) private toastr: Toastr) {
    }

    ngOnInit() {
        this.isFormLoadingData = false;
        this.coefficients = this.model.parent.coefficients;
        this.measureDateMoment = moment.utc();

        this.configureReactiveForm();

        //set Measure Date
        this.measureDateText = this.measureDateMoment.format("YYYY/MM/DD, H:mm") + " UTC";
        const fc = this.frmGroup.get('measureDate');
        const formatedDate = this.measureDateMoment.format(this.dateFormat);
        if (fc.value !== formatedDate) {
            fc.patchValue(formatedDate);
        }
        this.model.measureDate = this.measureDateMoment;

    }

    ngOnChanges() {
    }

    ngOnDestroy() {
    }

    $ctrl(name: string): AbstractControl {
        return this.frmGroup.get(name);
    }

    private configureReactiveForm() {
        const frmArrControls = <FormArray>this.mainForm.controls['frmAverageCRFIArr'];

        this.frmGroup = this.fb.group({
            coefficient: [{ value: this.model.coefficient, disabled: this.isReadOnly }, [Validators.required]],
            temperature: [{ value: this.model.temperature, disabled: this.isReadOnly }, [Validators.required]],
            measureDate: [{ value: this.measureDateMoment, disabled: this.isReadOnly }, [Validators.required]],
        });

        frmArrControls.push(this.frmGroup);

        this.frmGroup.get('temperature')
            .valueChanges.subscribe(value => this.temperatureChanged(value));
    }

    //Coefficient
    onCoefficientChanged(e: any): void {
        if (e.value) {
            let coefficientId = e.value;
            const control = this.frmGroup.get('coefficient')
            if (this.isFormLoadingData) {
                coefficientId = control.value;
            } else {
                if (coefficientId !== control.value) {
                    control.setValue(coefficientId);
                    control.markAsDirty();
                    this.model.coefficient = coefficientId;
                }
            }
        }
    }

    validateCoefficient() {
        const fc = this.frmGroup.get('coefficient');
        return !fc.errors || this.frmGroup.pristine;
    }

    //Temperature
    public validateKeyPressNumberTemperature(evt: any) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if(key !== '-' && !this.isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
        }
    }

    temperatureChanged(value: string) {
        this.model.temperature = value;
    }

    validateTempeture() {
        const fc = this.frmGroup.get('temperature');

        if (!this.isValidNumber(fc.value)) {
            return false;
        }
        return !fc.errors || this.frmGroup.pristine;
    }

    //Measure Date
    isCalendarActive() {
        return true;
    }

    setMeasureDate(date: any, leaveEmpty?: boolean) {
        const fc = this.frmGroup.get('measureDate');
        if (date) {
            let momentDate = moment.utc(date);
            if (momentDate.isValid()) {
                this.measureDateText = momentDate.format("YYYY/MM/DD, H:mm") + " UTC";
                const formatedDate = momentDate.format(this.dateFormat);
                if (fc.value !== formatedDate) {
                    fc.setValue(formatedDate);
                    fc.markAsDirty();
                    this.measureDateMoment = momentDate;
                    this.model.measureDate = this.measureDateMoment;
                }

            }
        } else {
            if (leaveEmpty) {
                fc.setValue(null);
                fc.markAsDirty();
                this.model.measureDate = null;
            } else {
                this.setMeasureDate(moment.utc());
            }
        }
    }

    validateMeasureDate(errorName: string) {
        return true;
    }

    onMeasureDateInputChange(strDate: string) {
        if (strDate && strDate.length === 10) {
            let momentDate = moment.utc(strDate, this.dateFormat);
            if (momentDate.isValid()) {
                this.measureDateText = momentDate.format("YYYY/MM/DD, H:mm") + " UTC";
                const fc = this.frmGroup.get('measureDate');
                const formatedDate = momentDate.format(this.dateFormat);
                if (fc.value !== formatedDate) {
                    fc.setValue(formatedDate);
                    fc.markAsDirty();
                    this.measureDateMoment = momentDate;
                    this.model.measureDate = this.measureDateMoment;
                }
            } else {
                this.measureDateText = "";
                this.model.measureDate = null;
            }
        } else {
            this.measureDateText = "";
            this.model.measureDate = null;
        }
    }


    //General functions
    private isValidNumber(value: string | number): boolean {
        return ((value != null) &&
            (value !== '') &&
            !isNaN(Number(value.toString())));
    }

    private isANumber(str: string): boolean {
        return !/\D/.test(str);
    }

    //Export functions
    public isFormValid(): boolean {

        return true;
    }

    public exportComponentInternalData(): IRwyFrictionModel {
        let data: IRwyFrictionModel = {
            parent: this.model.parent,
            measureDate: this.measureDateMoment,
            coefficient: this.model.coefficient,
            temperature: this.model.temperature,
        };
        return data;
    }

    public exportRwyFriction(): IRwyFriction {
        //Prepare RwyFriction data
        let rwyFriction: IRwyFriction = {
            rfObsTime: this.model.measureDate,
            device: 130, //Default
            temperature: this.model.temperature,
            unreliable: false,
            frictionCoefficients: +this.model.coefficient
        };
        return rwyFriction;
    }

    public cleanValues(): void {
        alert('Clear Component Values: AverageRwyFriction');
    }


}
﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace NavCanada.Core.Common.Security
{
    public static class CipherUtils
    {
        public static string DecryptConnectionString(string value)
        {
            if (!string.IsNullOrEmpty(value) && value.StartsWith(ECS) && value.EndsWith("="))
            {
                using (var sha256Hash = SHA256.Create())
                {
                    var sha256 = sha256Hash.ComputeHash(_seed);
                    var aesAlgorithm = new AesManaged { Key = sha256, IV = _iv };
                    var decrypted = DecryptText(aesAlgorithm, value.Substring(ECS.Length));
                    return decrypted;
                }
            }
            return value;
        }

        public static string EncryptConnectionString(string value)
        {
            if (!string.IsNullOrEmpty(value) && !(value.StartsWith(ECS) && value.EndsWith("=")))
            {
                using (var sha256Hash = SHA256.Create())
                {
                    var sha256 = sha256Hash.ComputeHash(_seed);
                    var aesAlgorithm = new AesManaged { Key = sha256, IV = _iv };
                    var encrypted = EncryptText(aesAlgorithm, value);
                    return $"{ECS}{encrypted}";
                }
            }
            return value;
        }

        public static string DecryptText(string value)
        {
            if (!string.IsNullOrEmpty(value) && value.StartsWith(ECS))
            {
                using (var sha256Hash = SHA256.Create())
                {
                    var sha256 = sha256Hash.ComputeHash(_seed);
                    var aesAlgorithm = new AesManaged { Key = sha256, IV = _iv };
                    var decrypted = DecryptText(aesAlgorithm, value.Substring(ECS.Length));
                    return decrypted;
                }
            }
            return value;
        }

        public static string EncryptText(string value)
        {
            if (!string.IsNullOrEmpty(value) && !(value.StartsWith(ECS)))
            {
                using (var sha256Hash = SHA256.Create())
                {
                    var sha256 = sha256Hash.ComputeHash(_seed);
                    var aesAlgorithm = new AesManaged { Key = sha256, IV = _iv };
                    var encrypted = EncryptText(aesAlgorithm, value);
                    return $"{ECS}{encrypted}";
                }
            }
            return value;
        }


        static string DecryptText(SymmetricAlgorithm aesAlgorithm, string data)
        {
            // Create a decryptor from the aes algorithm   
            var decryptor = aesAlgorithm.CreateDecryptor(aesAlgorithm.Key, aesAlgorithm.IV);

            // Read the encrypted bytes from the file   
            var encryptedDataBuffer = Convert.FromBase64String(data);

            // Create a memorystream to write the decrypted data in it   
            using (MemoryStream ms = new MemoryStream(encryptedDataBuffer))
            {
                using (CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
                {
                    using (StreamReader reader = new StreamReader(cs))
                    {
                        // Reutrn all the data from the streamreader   
                        return reader.ReadToEnd();
                    }
                }
            }
        }

        static string EncryptText(SymmetricAlgorithm aesAlgorithm, string text)
        {
            // Create an encryptor from the AES algorithm instance and pass the aes algorithm key and inialiaztion vector to generate a new random sequence each time for the same text  
            var encryptor = aesAlgorithm.CreateEncryptor(aesAlgorithm.Key, aesAlgorithm.IV);

            // Create a memory stream to save the encrypted data in it  
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter writer = new StreamWriter(cs))
                    {
                        // Write the text in the stream writer   
                        writer.Write(text);
                    }
                }

                // Get the result as a byte array from the memory stream   
                var encryptedDataBuffer = ms.ToArray();

                return Convert.ToBase64String(encryptedDataBuffer);
            }
        }

        static string ECS = "ECS:";
        static byte[] _seed = new byte[] { 58, 81, 88, 40, 75, 52, 75, 100, 87, 34, 114, 122, 113, 88, 84, 65, 115, 35, 126, 38, 55, 77, 104, 39, 125, 72, 115, 38, 118, 75, 51, 40 };
        static byte[] _iv = new byte[16] { 11, 47, 23, 32, 35, 33, 46, 12, 10, 251, 123, 97, 17, 88, 73, 1 };
    }
}

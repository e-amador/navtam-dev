﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by xsd, Version=4.6.1055.0.
// 
namespace RSC.WebService.Schemas {
    using System.Xml.Serialization;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.navcanada.ca/nes-rsc/webservice")]
    [System.Xml.Serialization.XmlRootAttribute("RSCResponse", Namespace="http://www.navcanada.ca/nes-rsc/webservice", IsNullable=false)]
    public partial class RSCResponseType {
        
        private RSCResponseTypeStatus statusField;
        
        private RSCResponseTypeMessage[] messageField;
        
        private string mODEField;
        
        private RSCResponseTypeRSC rSCField;
        
        private RSCResponseTypeVersion versionField;
        
        /// <remarks/>
        public RSCResponseTypeStatus Status {
            get {
                return this.statusField;
            }
            set {
                this.statusField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Message")]
        public RSCResponseTypeMessage[] Message {
            get {
                return this.messageField;
            }
            set {
                this.messageField = value;
            }
        }
        
        /// <remarks/>
        public string MODE {
            get {
                return this.mODEField;
            }
            set {
                this.mODEField = value;
            }
        }
        
        /// <remarks/>
        public RSCResponseTypeRSC RSC {
            get {
                return this.rSCField;
            }
            set {
                this.rSCField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public RSCResponseTypeVersion version {
            get {
                return this.versionField;
            }
            set {
                this.versionField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://www.navcanada.ca/nes-rsc/webservice")]
    public enum RSCResponseTypeStatus {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("0")]
        Item0,
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("1")]
        Item1,
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("2")]
        Item2,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://www.navcanada.ca/nes-rsc/webservice")]
    public partial class RSCResponseTypeMessage {
        
        private string msgNumberField;
        
        private string descriptionField;
        
        private string runwayIdField;
        
        private RSCResponseTypeMessageThird thirdField;
        
        private bool thirdFieldSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="integer")]
        public string MsgNumber {
            get {
                return this.msgNumberField;
            }
            set {
                this.msgNumberField = value;
            }
        }
        
        /// <remarks/>
        public string Description {
            get {
                return this.descriptionField;
            }
            set {
                this.descriptionField = value;
            }
        }
        
        /// <remarks/>
        public string RunwayId {
            get {
                return this.runwayIdField;
            }
            set {
                this.runwayIdField = value;
            }
        }
        
        /// <remarks/>
        public RSCResponseTypeMessageThird Third {
            get {
                return this.thirdField;
            }
            set {
                this.thirdField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ThirdSpecified {
            get {
                return this.thirdFieldSpecified;
            }
            set {
                this.thirdFieldSpecified = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://www.navcanada.ca/nes-rsc/webservice")]
    public enum RSCResponseTypeMessageThird {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("0")]
        Item0,
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("1")]
        Item1,
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("2")]
        Item2,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://www.navcanada.ca/nes-rsc/webservice")]
    public partial class RSCResponseTypeRSC {
        
        private string englishRSCField;
        
        private string frenchRSCField;
        
        /// <remarks/>
        public string EnglishRSC {
            get {
                return this.englishRSCField;
            }
            set {
                this.englishRSCField = value;
            }
        }
        
        /// <remarks/>
        public string FrenchRSC {
            get {
                return this.frenchRSCField;
            }
            set {
                this.frenchRSCField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://www.navcanada.ca/nes-rsc/webservice")]
    public enum RSCResponseTypeVersion {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("0.02")]
        Item002,
    }
}

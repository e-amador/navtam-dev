"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TwyClsdCancelComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var data_service_1 = require("../../shared/data.service");
var location_service_1 = require("../../shared/location.service");
var twyclsd_service_1 = require("./twyclsd.service");
var toastr_service_1 = require("./../../../common/toastr.service");
var angular_l10n_1 = require("angular-l10n");
var TwyClsdCancelComponent = /** @class */ (function () {
    function TwyClsdCancelComponent(toastr, injector, fb, locationService, twyClsdService, dataService, locale, translation) {
        this.toastr = toastr;
        this.injector = injector;
        this.fb = fb;
        this.locationService = locationService;
        this.twyClsdService = twyClsdService;
        this.dataService = dataService;
        this.locale = locale;
        this.translation = translation;
        this.token = null;
        this.isReadOnly = true;
        this.isBilingualRegion = false;
        //isFormLoadingData: boolean;
        this.subjectSelected = null;
        this.subjectId = '';
        this.subjects = [];
        this.taxiwayId = '-1';
        this.taxiwaySelected = null;
        this.taxiways = [];
        this.taxiwaysSelected = [];
        this.taxiwayComboList = [];
        this.closureReasonId = -1;
        this.closureReasonFreeText = '';
        this.closureReasonFreeTextFr = '';
        this.twyClsdReasons = [];
        this.closureStatus = '';
        this.allTaxiwaysSelected = false;
        this.disableTaxiwaySelector = false;
        this.closureReasonOptions = {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.ClosurePlaceholder')
            }
        };
        this.taxiwaysOptions = {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.TaxiwayPlaceholder')
            }
        };
        this.token = this.injector.get('token');
        this.nsdForm = this.injector.get('form');
    }
    TwyClsdCancelComponent.prototype.ngOnInit = function () {
        var _this = this;
        //this.isFormLoadingData = !(!this.token.subjectId);
        var app = this.twyClsdService.app;
        this.leafletmap = app.leafletmap;
        this.subjectId = this.token.subjectId || '';
        this.closureReasonId = this.token.closureReasonId || -1;
        this.closureReasonFreeText = this.token.closureReasonFreeText || '';
        this.closureReasonFreeTextFr = this.token.closureReasonFreeTextFr || '';
        this.allTaxiwaysSelected = this.token.allTaxiwaysSelected || false;
        this.taxiways = this.token.allTaxiways || [];
        this.twyClsdService.getTwyClsdReasons()
            .subscribe(function (reasons) {
            _this.twyClsdReasons = reasons;
            _this.setFirstSelectItemInCombo(_this.twyClsdReasons, _this.token.closureReasonId);
        });
        var frmArr = this.nsdForm.controls['frmArr'];
        if (frmArr.length === 0) {
            this.prepareTaxiwayComboList();
            this.taxiwaysSelected = [];
            var lastId = '';
            if (this.allTaxiwaysSelected) {
                this.disableTaxiwaySelector = true;
                var twyAll = {
                    designator: 'TWYS',
                    end: '',
                    endFr: '',
                    full: true,
                    id: '0',
                    mid: '0',
                    sdoEntityName: 'ALL',
                    selected: true,
                    start: '',
                    startFr: '',
                    index: 0
                };
                this.taxiwaysSelected.push(twyAll);
                lastId = twyAll.id;
            }
            else {
                for (var i = 0; i < this.token.allTaxiways.length; i++) {
                    var modelTwy = this.token.allTaxiways[i];
                    if (modelTwy.selected) {
                        var twy = Object.assign({}, modelTwy);
                        this.taxiwaysSelected.push(twy);
                        lastId = twy.id;
                    }
                }
            }
            this.taxiwaySelected = this.taxiwaysSelected.find(function (x) { return x.id === lastId; });
            this.dataService.getSdoSubject(this.token.subjectId)
                .subscribe(function (sdoSubjectResult) {
                _this.subjectSelected = sdoSubjectResult;
                _this.subjects = [];
                _this.subjects.push(sdoSubjectResult);
                _this.isBilingualRegion = sdoSubjectResult.isBilingualRegion;
                _this.token.isBilingual = sdoSubjectResult.isBilingualRegion;
                _this.$ctrl('subjectId').setValue(sdoSubjectResult.id);
                //this.setFirstSelectItemInCombo(this.subjects, this.subjectId);
                _this.loadMapLocations(app.doaId, true);
                _this.updateMap(sdoSubjectResult, true);
                _this.$ctrl('countSelectedTwys').setValue(_this.taxiways.length);
                _this.pickTaxiway(_this.taxiwaySelected.id);
            });
            this.configureReactiveFormLoading();
            this.$ctrl('countSelectedTwys').setValue(0);
            //if (lastId !== '') this.pickTaxiway(lastId);
        }
        else {
            /*
            this is required because of the grouping funtionallity. When the NsdFormGroup
            was configured and any of items in the group has changed (new set of tokens values)
            the NsdForm Group needs to be updated with the new tokens values */
            this.updateReactiveForm(frmArr);
        }
    };
    TwyClsdCancelComponent.prototype.ngOnDestroy = function () {
        if (this.leafletmap) {
            this.leafletmap.dispose();
            this.leafletmap = null;
        }
    };
    TwyClsdCancelComponent.prototype.getTaxiwayName = function (twy) {
        return twy.sdoEntityName.toUpperCase() + ' ' + twy.designator.toUpperCase();
    };
    TwyClsdCancelComponent.prototype.getClosureText = function (twy) {
        if (twy.full)
            return this.translation.translate('TableHeaders.FullClosure');
        else
            return this.translation.translate('TableHeaders.PartialClosure');
    };
    TwyClsdCancelComponent.prototype.getBetweenText = function (twy) {
        if (twy.full)
            return "";
        else {
            if (this.isBilingualRegion) {
                return twy.start.toUpperCase() + " / " + twy.startFr.toUpperCase();
            }
            else {
                return twy.start.toUpperCase();
            }
        }
    };
    TwyClsdCancelComponent.prototype.getAndText = function (twy) {
        if (twy.full)
            return "";
        else {
            if (this.isBilingualRegion) {
                return twy.end.toUpperCase() + " / " + twy.endFr.toUpperCase();
            }
            else {
                return twy.end.toUpperCase();
            }
        }
    };
    //Enable functions
    TwyClsdCancelComponent.prototype.pickTaxiway = function (id) {
        var twySel = this.$ctrl('taxiwayId');
        if (twySel) {
            if (id === twySel.value)
                return;
            twySel.patchValue(id);
            this.taxiwayId = id;
            if (id === '-1')
                return;
            if (id === '0') {
                //All taxiways
                var field = this.$ctrl('startClosure');
                field.patchValue('');
                field = this.$ctrl('endClosure');
                field.patchValue('');
                field = this.$ctrl('startClosureFr');
                field.patchValue('');
                field = this.$ctrl('endClosureFr');
                field.patchValue('');
                field = this.$ctrl('fullClosure');
                field.patchValue(true);
                this.taxiwaySelected = null;
            }
            else {
                this.taxiwaySelected = this.taxiways.find(function (x) { return x.id === id; });
                //Update the fields
                var field = this.$ctrl('fullClosure');
                field.patchValue(this.taxiwaySelected.full);
                field = this.$ctrl('startClosure');
                if (!this.taxiwaySelected.full)
                    field.patchValue(this.taxiwaySelected.start);
                else
                    field.patchValue('');
                field = this.$ctrl('endClosure');
                if (!this.taxiwaySelected.full)
                    field.patchValue(this.taxiwaySelected.end);
                else
                    field.patchValue('');
                field = this.$ctrl('startClosureFr');
                if (!this.taxiwaySelected.full)
                    field.patchValue(this.taxiwaySelected.startFr);
                else
                    field.patchValue('');
                field = this.$ctrl('endClosureFr');
                if (!this.taxiwaySelected.full)
                    field.patchValue(this.taxiwaySelected.endFr);
                else
                    field.patchValue('');
                //if (this.isBilingualRegion) {
                //    field = this.$ctrl('startClosureFr');
                //    if (!this.taxiwaySelected.full) field.patchValue(this.taxiwaySelected.startFr);
                //    else field.patchValue('');
                //    field = this.$ctrl('endClosureFr');
                //    if (!this.taxiwaySelected.full) field.patchValue(this.taxiwaySelected.endFr);
                //    else field.patchValue('');
                //} else {
                //    field = this.$ctrl('startClosureFr');
                //    field.patchValue('');
                //    field = this.$ctrl('endClosureFr');
                //    field.patchValue('');
                //}
                if (this.taxiwaySelected.full)
                    this.closureStatus = this.translation.translate('TableHeaders.FullClosure');
                else
                    this.closureStatus = this.translation.translate('TableHeaders.PartialClosure');
            }
        }
    };
    TwyClsdCancelComponent.prototype.rowColor = function (id) {
        if (!this.taxiwaySelected) {
            if (this.allTaxiwaysSelected)
                return 'bg-submitted';
            else
                return '';
        }
        if (this.taxiwaySelected.id === id)
            return 'bg-submitted';
        return '';
    };
    TwyClsdCancelComponent.prototype.configureReactiveFormLoading = function () {
        var frmArrControls = this.nsdForm.controls['frmArr'];
        this.nsdFrmGroup = this.fb.group({
            subjectId: [this.token.subjectId || '', [forms_1.Validators.required]],
            subjectLocation: this.token.subjectLocation || '',
            taxiwayId: [{ value: this.taxiwayId, disabled: false }],
            fullClosure: [{ value: this.taxiwaySelected.full, disabled: this.isReadOnly }],
            startClosure: [{ value: this.taxiwaySelected.start, disabled: true }],
            startClosureFr: [{ value: this.taxiwaySelected.startFr, disabled: true }],
            endClosure: [{ value: this.taxiwaySelected.end, disabled: true }],
            endClosureFr: [{ value: this.taxiwaySelected.endFr, disabled: true }],
            grpClosure: this.fb.group({
                closureReasonId: [{ value: this.token.closureReasonId, disabled: true }],
                closureReasonFreeText: [{ value: this.token.closureReasonFreeText, disabled: true }],
                closureReasonFreeTextFr: [{ value: this.token.closureReasonFreeTextFr, disabled: true }],
            }),
            allTaxiways: [{ value: this.taxiways, disabled: false }],
            countSelectedTwys: [{ value: 0 }, [countSelectedTwysValidator]],
            allTaxiwaysSelected: [{ value: this.token.allTaxiwaysSelected, disabled: false }],
        });
        frmArrControls.push(this.nsdFrmGroup);
    };
    TwyClsdCancelComponent.prototype.updateReactiveForm = function (formArr) {
        this.nsdFrmGroup = formArr.controls[0];
        //It is better to update only those controls that tokens values have changed.
        this.updateFormControlValue("subjectId", this.token.subjectId);
        this.updateFormControlValue("subjectLocation", this.token.subjectLocation);
        this.updateFormControlValue("taxiwayId", this.taxiwayId);
        this.updateFormControlValue("fullClosure", false);
        this.updateFormControlValue("startClosure", '');
        this.updateFormControlValue("startClosureFr", '');
        this.updateFormControlValue("endClosure", '');
        this.updateFormControlValue("endClosureFr", '');
        this.updateFormControlValue("grpClosure.closureReasonId", this.token.closureReasonId);
        this.updateFormControlValue("grpClosure.closureReasonFreeText", this.token.closureReasonFreeText);
        this.updateFormControlValue("grpClosure.closureReasonFreeTextFr", this.token.closureReasonFreeTextFr);
        this.updateFormControlValue("allTaxiways", this.taxiways);
        this.updateFormControlValue("countSelectedTwys", 0);
        this.updateFormControlValue("allTaxiwaysSelected", this.token.allTaxiwaysSelected);
    };
    TwyClsdCancelComponent.prototype.updateFormControlValue = function (controlName, value) {
        var control = this.nsdFrmGroup.get(controlName);
        if (control && control.value !== value) {
            control.setValue(value);
        }
    };
    TwyClsdCancelComponent.prototype.prepareTaxiwayComboList = function () {
        this.taxiwayComboList = [];
        this.taxiwayComboList = this.taxiways.map(function (item) {
            return {
                id: item.id,
                text: item.sdoEntityName.toUpperCase() + ' ' + item.designator.toUpperCase(),
            };
        });
        if (this.taxiways.length > 1) {
            //Add ALL TWYS
            this.taxiwayComboList.unshift({
                id: "0",
                text: this.translation.translate('Nsd.AllTaxiways'),
            });
        }
        //Add empty slot
        this.taxiwayComboList.unshift({
            id: "-1",
            text: "",
        });
    };
    TwyClsdCancelComponent.prototype.$ctrl = function (name) {
        return this.nsdFrmGroup.get(name);
    };
    TwyClsdCancelComponent.prototype.sdoSubjectWithTwysOptions = function () {
        var self = this;
        var app = this.twyClsdService.app;
        return {
            minimumInputLength: 2,
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.SdoSubjectPlaceholder')
            },
            ajax: {
                type: 'GET',
                url: app.apiUrl + "taxiways/filterwithtwy",
                dataType: 'json',
                delay: 500,
                headers: {
                    "RequestVerificationToken": app.antiForgeryTokenValue
                },
                data: function (parms, page) {
                    return {
                        Query: parms.term,
                        Size: 200,
                        doaId: app.doaId //set by the app controller
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            templateResult: function (data) {
                if (data.loading) {
                    return data.text;
                }
                return self.dataService.getSdoExtendedName(data);
            },
            templateSelection: function (selection) {
                if (selection && selection.id === "-1") {
                    return selection.text;
                }
                return self.dataService.getSdoExtendedName(selection);
            },
        };
    };
    Object.defineProperty(TwyClsdCancelComponent.prototype, "colBetweenCaption", {
        get: function () {
            if (this.isBilingualRegion)
                return this.translation.translate('TableHeaders.BetweenBiligual');
            else
                return this.translation.translate('TableHeaders.Between');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(TwyClsdCancelComponent.prototype, "colAndCaption", {
        get: function () {
            if (this.isBilingualRegion)
                return this.translation.translate('TableHeaders.AndBiligual');
            else
                return this.translation.translate('TableHeaders.And');
        },
        enumerable: false,
        configurable: true
    });
    //OnChange functions
    //Validation functions
    TwyClsdCancelComponent.prototype.isRequiredReasonFreeText = function () {
        var reasonIdCtrl = this.$ctrl('grpClosure.closureReasonId');
        if (reasonIdCtrl.value === -1)
            return false;
        var elem = this.twyClsdReasons.find(function (x) { return x.id === reasonIdCtrl.value; });
        if (elem !== undefined && elem !== null) {
            var reasonValueText = elem.text.toLowerCase();
            if ((reasonValueText === 'other') || (reasonValueText === 'autre'))
                return true;
        }
        return false;
    };
    TwyClsdCancelComponent.prototype.validateTaxiway = function (twy) {
        if (!twy.selected)
            return null;
        if (twy.full) {
            if (twy.start.length === 0 && twy.startFr.length === 0 && twy.end.length === 0 && twy.endFr.length === 0)
                return null;
            else
                return { fullError: { valid: false } };
        }
        else {
            if (twy.start.length === 0 || twy.end.length === 0)
                return { partialError: { valid: false } };
            else {
                if (this.isBilingualRegion) {
                    if (twy.startFr.length === 0 || twy.endFr.length === 0)
                        return { partialError: { valid: false } };
                    else
                        return null;
                }
                else
                    return null;
            }
        }
    };
    TwyClsdCancelComponent.prototype.validateAllTaxiways = function () {
        if (!this.taxiways || !this.taxiwaysSelected)
            return { selectedTaxiways: { valid: false } };
        var error = null;
        if (this.taxiways.length === 0)
            return { allTaxiways: { valid: false } };
        if (this.taxiwaysSelected.length === 0)
            return { allTaxiways: { valid: false } };
        for (var i = 0; i < this.taxiways.length; i++) {
            error = this.validateTaxiway(this.taxiways[i]);
            if (error !== null) {
                i = this.taxiways.length + 1;
            }
        }
        return error;
    };
    TwyClsdCancelComponent.prototype.validateAndTrigger = function () {
        var error = this.validateAllTaxiways();
        if (error) {
            this.nsdForm.controls['frmArr'].setErrors(null);
            this.nsdForm.controls['frmArr'].setErrors(error);
            var ctrl = this.$ctrl('countSelectedTwys');
            ctrl.setValue(0);
        }
        else {
            this.nsdForm.controls['frmArr'].setErrors(null);
            var ctrl = this.$ctrl('countSelectedTwys');
            ctrl.setValue(this.taxiwaysSelected.length);
            ctrl = this.$ctrl('allTaxiways');
            ctrl.setValue(this.taxiways);
        }
    };
    // Standard general function
    TwyClsdCancelComponent.prototype.updateMap = function (sdoSubject, acceptSubjectLocation) {
        var _this = this;
        this.leafletmap.clearFeaturesOnLayers(['DOA']);
        if (sdoSubject.subjectGeoRefPoint) {
            var geojson = JSON.parse(sdoSubject.subjectGeoRefPoint);
            if (geojson.features.length === 1) {
                var subLocation = this.leafletmap.geojsonToTextCordinates(geojson);
                var coordinates = geojson.features[0].geometry.coordinates;
                var markerGeojson_1 = acceptSubjectLocation ? geojson : this.getGeoJsonFromLocation(this.token.subjectLocation);
                if (acceptSubjectLocation) {
                    this.nsdFrmGroup.patchValue({
                        subjectLocation: subLocation,
                        latitude: coordinates[1],
                        longitude: coordinates[0],
                    }, { onlySelf: true, emitEvent: false });
                }
                this.leafletmap.addMarker(geojson, sdoSubject.name, function () {
                    _this.leafletmap.addFeature(sdoSubject.subjectGeo, sdoSubject.name, function () {
                        if (markerGeojson_1) {
                            _this.leafletmap.setNewMarkerLocation(markerGeojson_1);
                        }
                    });
                });
            }
        }
    };
    TwyClsdCancelComponent.prototype.setFirstSelectItemInCombo = function (items, itemId, insertUndefinedAtZero) {
        if (insertUndefinedAtZero === void 0) { insertUndefinedAtZero = true; }
        var index = items.findIndex(function (x) { return x.id === itemId; });
        if (index > 0) { //move selected item to position 0
            items.splice(0, 0, items.splice(index, 1)[0]);
        }
    };
    TwyClsdCancelComponent.prototype.setFirstSelectItemInTaxiways = function (items, itemId, insertUndefinedAtZero) {
        if (insertUndefinedAtZero === void 0) { insertUndefinedAtZero = true; }
        var index = items.findIndex(function (x) { return x.id === itemId; });
        if (index > 0) { //move selected item to position 0
            items.splice(0, 0, items.splice(index, 1)[0]);
        }
    };
    TwyClsdCancelComponent.prototype.loadMapLocations = function (doaId, loadingFromProposal) {
        var _this = this;
        var self = this;
        this.dataService.getDoaLocation(doaId)
            .subscribe(function (geoJsonStr) {
            var interval = window.setInterval(function () {
                if (self.leafletmap && self.leafletmap.isReady()) {
                    window.clearInterval(interval);
                    self.leafletmap.addDoa(geoJsonStr, 'DOA');
                    if (loadingFromProposal) {
                        self.updateMap(self.subjectSelected, false);
                    }
                }
                ;
            }, 100);
        }, function (error) {
            _this.toastr.error(_this.translation.translate('Nsd.DOAArea') + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    TwyClsdCancelComponent.prototype.getGeoJsonFromLocation = function (value) {
        var location = this.locationService.parse(value);
        return location ? this.leafletmap.latitudeLongitudeToGeoJson(location.latitude, location.longitude) : null;
    };
    TwyClsdCancelComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/dashboard/nsds/twyclsd/twyclsd-cancel.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, core_1.Injector,
            forms_1.FormBuilder,
            location_service_1.LocationService,
            twyclsd_service_1.TwyClsdService,
            data_service_1.DataService,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], TwyClsdCancelComponent);
    return TwyClsdCancelComponent;
}());
exports.TwyClsdCancelComponent = TwyClsdCancelComponent;
function countSelectedTwysValidator(c) {
    if (c.value === 0) {
        return { 'required': true };
    }
    return null;
}
//# sourceMappingURL=twyclsd-cancel.component.js.map
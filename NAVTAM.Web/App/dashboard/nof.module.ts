﻿import { NgModule, APP_INITIALIZER, Injectable } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Select2Module } from 'ng2-select2';
import { DateTimePickerModule } from '../common/components/ng2-date-time-picker';

//import { StartupService } from './startup.service';
import { LocalizationModule, LocaleService, TranslationService } from 'angular-l10n';

import {
    JQUERY_TOKEN, //OR JQUERY_PROVIDER,
    TOASTR_TOKEN,
    Toastr,
    WindowRef,
    XsrfTokenService,
    Ng2ICheckModule,
    SimpleModalComponent,
    ServerPaginationComponent,
    ModalTriggerDirective,
    DateTimeFormatDirective,
    DatexPipe
} from '../common/index';

import { NofComponent } from './nof.component';

import {
    //Shared
    DataService,
    LocationService,
    NotifyService,
    MemoryStorageService,
    ReportFilterComponent,
    ReportContainerComponent,
    ReportNotamComponent,
    ReportStatsComponent,
    ReportActiveComponent,
    //NOF-DASHBOARD
    NofPortalComponent,
    NofQueueComponent,
    NofPickComponent,
    NofCreateComponent,
    NofViewComponent,
    NofNotamComponent,
    //NofMsgComponent,
    NofNotamFilterComponent,
    NofMsgQueueComponent,
    NofMsgComposeComponent,
    NofMsgReadComponent,
    NofMsgTemplateListComponent,
    NofMsgTemplateCreateComponent,
    NofMsgTemplateEditComponent,
    SubscriptionFilterPipe,
    NofViewHistoryComponent,
    //Nsds
    DynamicComponent,
    NsdIcaoComponent,
    NsdIcaoFormatComponent,
    NsdFormComponent,
    NsdButtonBarComponent,
    NsdGeoRefToolComponent,
    NsdSchedulerDailyComponent,
    NsdSchedulerDateComponent,
    NsdSchedulerDaysOfWeekComponent,
    NsdSchedulerFreeTextComponent,
    //CatchAll
    CatchAllDraftComponent,
    CatchAllCancelComponent,
    CatchAllService,
    //Obst
    ObstComponent,
    ObstCancelComponent,
    ObstacleService,
    MObstService,
    MObstComponent,
    MObstCancelComponent,
    //ObstLgUs
    ObstLgUsComponent,
    ObstLgUsCancelComponent,
    ObstacleLgUsService,
    MObstLgUsComponent,
    MObstLgUsCancelComponent,
    //RwyClsd
    RwyClsdComponent,
    RwyClsdCancelComponent,
    RwyClsdService,
    //TwyClsd
    TwyClsdComponent,
    TwyClsdCancelComponent,
    TwyClsdService,
    //CarsClsd
    CarsClsdComponent,
    CarsClsdCancelComponent,
    CarsClsdService,
    //Airspace
    AreaDefinitionNsdComponent,
    AreaDefinitionCancelNsdComponent,
    AreaDefinitionNsdService,
    //RSC
    RscComponent,
    RscService,
    RscContaminantsComponent,
    ClearedWidthComponent,
    ClearingOperationsComponent,
    SnowBanksComponent,
    RscTreatmentsComponent,
    RwyFrictionComponent,
    RemainingWidthRemarksComponent,
    NextObservationComponent,
    WindrowsComponent,
    TaxiwaysRemarksComponent,
    ApronRemarksComponent,
    //Resolvers
    CategoriesResolver,
    ProposalModelResolver,
    ProposalReadOnlyModelResolver,
    NotamProposalReadOnlyModelResolver,
    ProposalCreateModelResolver,
    ProposalCancelModelResolver,
    ProposalReplaceModelResolver,
    ProposalCloneModelResolver,
    NotamCloneModelResolver,
    ProposalHistoryModelResolver,
    NofMessageResolver,
    NofMessageTemplateResolver
} from './nof.index'

import { nofRoutes } from './nof.routes';

declare let toastr: Toastr;
//declare let $: Object;

//export function startupServiceFactory(startupService: StartupService): Function {
//    return () => startupService.load();
//}

//export function createConfig(): SignalRConfiguration {
//    const c = new SignalRConfiguration();
//    c.hubName = 'SignalRHub';
//    //c.qs = { user: 'user.subscribed' };
//    c.url = window.location.origin;
//    c.logging = true;
//    return c;
//}

@Injectable()
export class LocalizationConfig {

    constructor(public locale: LocaleService, public translation: TranslationService) { }

    load(): Promise<any> {
        this.locale.addConfiguration()
            .addLanguage('en', 'ltr')
            .addLanguage('fr', 'ltr')
            //.setCookieExpiration(30)
            .defineLanguage(window['app'].cultureInfo)
            .defineCurrency("CAD")
            .defineDefaultLocale('en', 'CA')
        this.locale.init();

        this.translation.addConfiguration()
            .addProvider('./app/resources/locale-');

        let promise: Promise<any> = new Promise((resolve: any) => {
            this.translation.translationChanged.subscribe(() => {
                resolve(true);
            });
        });

        this.translation.init();

        return promise;
    }
}

// AoT compilation requires a reference to an exported function.
export function initLocalization(localizationConfig: LocalizationConfig): Function {
    return () => localizationConfig.load();
}

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forRoot(nofRoutes),
        Select2Module,
        DateTimePickerModule,
        Ng2ICheckModule,
        //SignalRModule.forRoot(createConfig),
        LocalizationModule.forRoot() // New instance of TranslationService
    ],
    declarations: [
        NofComponent,
        //Shared
        DatexPipe,
        DateTimeFormatDirective,
        ModalTriggerDirective,
        SimpleModalComponent,
        ServerPaginationComponent,
        ReportNotamComponent,
        ReportStatsComponent,
        ReportContainerComponent,
        ReportFilterComponent,
        ReportActiveComponent,
        //NOF,
        NsdFormComponent,
        NsdButtonBarComponent,
        NsdGeoRefToolComponent,
        NsdIcaoComponent,
        NsdIcaoFormatComponent,
        NsdSchedulerDailyComponent,
        NsdSchedulerDateComponent,
        NsdSchedulerDaysOfWeekComponent,
        NsdSchedulerFreeTextComponent,
        //NOF,
        NofPortalComponent,
        NofQueueComponent,
        NofPickComponent,
        NofCreateComponent,
        NofViewComponent,
        NofViewHistoryComponent,
        NofNotamComponent,
        NofNotamFilterComponent,
        //NofMsgComponent,
        NofMsgQueueComponent,
        NofMsgComposeComponent,
        NofMsgReadComponent,
        NofMsgTemplateListComponent,
        NofMsgTemplateCreateComponent,
        NofMsgTemplateEditComponent,
        SubscriptionFilterPipe,
        DynamicComponent,
        // CatchAll
        CatchAllDraftComponent,
        CatchAllCancelComponent,
        // Obstacle
        ObstComponent,
        ObstCancelComponent,
        // Obstacle LG US
        ObstLgUsComponent,
        ObstLgUsCancelComponent,
        //Multiple Obstacle
        MObstComponent,
        MObstCancelComponent,
        //Multiple Obstacle LG US
        MObstLgUsComponent,
        MObstLgUsCancelComponent,
        // Runway closed
        RwyClsdComponent,
        RwyClsdCancelComponent,
        // Taxyway closed
        TwyClsdComponent,
        TwyClsdCancelComponent,
        // CARS closed
        CarsClsdComponent,
        CarsClsdCancelComponent,
        // Area Definition
        AreaDefinitionNsdComponent,
        AreaDefinitionCancelNsdComponent,
        //Rsc
        RscComponent,
        RscContaminantsComponent,
        ClearedWidthComponent,
        ClearingOperationsComponent,
        SnowBanksComponent,
        RscTreatmentsComponent,
        RwyFrictionComponent,
        RemainingWidthRemarksComponent,
        NextObservationComponent,
        WindrowsComponent,
        TaxiwaysRemarksComponent,
        ApronRemarksComponent,
    ],
    providers: [
        DataService,
        NotifyService,
        MemoryStorageService,
        LocationService,
        CatchAllService,
        ObstacleService,
        ObstacleLgUsService,
        MObstService,
        RwyClsdService,
        TwyClsdService,
        CarsClsdService,
        AreaDefinitionNsdService,
        RscService,
        CategoriesResolver,
        ProposalModelResolver,
        ProposalReadOnlyModelResolver,
        NotamProposalReadOnlyModelResolver,
        ProposalCreateModelResolver,
        ProposalCancelModelResolver,
        ProposalReplaceModelResolver,
        ProposalCloneModelResolver,
        NotamCloneModelResolver,
        ProposalHistoryModelResolver,
        NofMessageResolver,
        NofMessageTemplateResolver,
        //StartupService,
        LocalizationConfig,
        {
            provide: APP_INITIALIZER,
            useFactory: initLocalization,
            deps: [LocalizationConfig],
            multi: true
        },
        //common
        //{
        //    // Provider for APP_INITIALIZER
        //    provide: APP_INITIALIZER,
        //    useFactory: startupServiceFactory,
        //    deps: [StartupService],
        //    multi: true
        //},
        { provide: TOASTR_TOKEN, useValue: toastr },
        { provide: 'canDeactivateCreateEvent', useValue: checkDirtyState }, //route-guard
        XsrfTokenService,
        WindowRef
    ],
    bootstrap: [NofComponent]
})
export class NofModule { }

function checkDirtyState(component: NofCreateComponent): Promise<boolean> {
    let el: any = $('#modal-unsave-changes');
    let $modal = el.modal();
    return new Promise<boolean>((resolve, reject) => {
        var isRSC = (component.nsdForm.nsdForm === undefined);
        if (isRSC) {
            var comp = <any>component;
            comp = <RscComponent>comp;
            if (!comp.isSaveButtonDisabled && comp.nsdForm.dirty) {
                let alreadyResolved = false;
                $("#btn-yes").click(function () {
                    resolve(true);
                    alreadyResolved = true;
                });
                $("#btn-no").click(function () {
                    resolve(false);
                    alreadyResolved = true;
                });
                el.on('hide.bs.modal', function (e) {
                    if (!alreadyResolved) { resolve(false); }
                })
                $modal.modal("show");
            }
            else {
                resolve(true);
            }

        } else {
            if (!component.nsdForm.isSaveButtonDisabled && component.nsdForm.nsdForm.dirty) {
                let alreadyResolved = false;
                $("#btn-yes").click(function () {
                    resolve(true);
                    alreadyResolved = true;
                });
                $("#btn-no").click(function () {
                    resolve(false);
                    alreadyResolved = true;
                });
                el.on('hide.bs.modal', function (e) {
                    if (!alreadyResolved) { resolve(false); }
                })
                $modal.modal("show");
            }
            else {
                resolve(true);
            }

        }
        //if (!component.nsdForm.isSaveButtonDisabled && component.nsdForm.nsdForm.dirty) {
        //    let alreadyResolved = false;
        //    $("#btn-yes").click(function () {
        //        resolve(true);
        //        alreadyResolved = true;
        //    });
        //    $("#btn-no").click(function () {
        //        resolve(false);
        //        alreadyResolved = true;
        //    });
        //    el.on('hide.bs.modal', function (e) {
        //        if (!alreadyResolved) { resolve(false); }
        //    })
        //    $modal.modal("show");
        //}
        //else {
        //    resolve(true);
        //}
    });
}


﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.ServiceBus.Messaging;
using NDS.DataBus.Contracts;

namespace NDS.DataBus.ServiceBus
{
    public class BrokeredMessageWrapper : INdsMessage
    {
        internal BrokeredMessageWrapper(BrokeredMessage message)
        {
            _message = message;
        }

        internal BrokeredMessageWrapper(string content, IEnumerable<Property> properties)
        {
            _message = new BrokeredMessage(content);
            if (properties != null)
            {
                foreach (var prop in properties)
                {
                    _message.Properties.Add(prop.Name, prop.Value);
                }
            }
        }

        public object this[string key]
        {
            get { return _message.Properties.ContainsKey(key) ? _message.Properties[key] : string.Empty; }
        }

        public string Body => _message.GetBody<string>();

        public BrokeredMessage InnerMessage => _message;

        public IEnumerable<Property> Properties
        {
            get { return _message.Properties.Select(pair => Property.Create(pair.Key, pair.Value)); }
        }

        private BrokeredMessage _message;

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.
                _message?.Dispose();
                _message = null;

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~BrokeredMessageWrapper() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NavCanada.Core.Proxies.AeroRdsProxy.SDOData.Models.SDOEntities;

namespace NavCanada.Applications.NotamWiz.Web.Code.aeroRDS
{
    public interface IAeroRdsUtility
    {
        Ahp GetAhpByMid(string mid);
        List<Rwy> GetExpandedRwyForAhp(string mid);
        bool IsRdnDisplaced(string rdnMid);
    }

    public class AeroRdsUtility : IAeroRdsUtility
    {
        public virtual Ahp GetAhpByMid(string mid)
        {
            return AeroRds.ODataCtx.Ahp.Where(a => a.Mid == mid).FirstOrDefault();
        }

        public virtual List<Rwy> GetExpandedRwyForAhp(string mid)
        {
            return AeroRds.ODataCtx.Rwy.Expand(r => r.Rdn).Where(r => r.Ahp.Mid == mid).ToList();
        }

        public virtual bool IsRdnDisplaced(string rdnMid)
        {
            return AeroRds.ODataCtx.Rdd.Where(r => r.FK_RdnUid == rdnMid && r.CodeType == "DPLM").Count() > 0;
        }
    }
}
﻿using NavCanada.Core.Scheduler.Contracts;
using Quartz;

namespace NavCanada.Core.Scheduler
{
    public class QuartzScheduler
    {
        public QuartzScheduler(IScheduler scheduler, ISchedulerJobProvider schedulerJobProvider)
        {
            _scheduler = scheduler;
            _schedulerJobProvider = schedulerJobProvider;
        }

        public void Start()
        {
            if (_scheduler.IsStarted)
                return;

            _scheduler.Start();
        }

        public void Shutdown()
        {
            if (_scheduler.IsShutdown)
                return;

            _scheduler.Shutdown();
        }

        public void StandBy()
        {
            if (_scheduler.InStandbyMode)
                return;

            _scheduler.Standby();
        }


        public void StartJobs()
        {
            foreach(var scheduleJob in _schedulerJobProvider.GetAllScheduleJobs())
                _scheduler.ScheduleJob(scheduleJob.Job, scheduleJob.Trigger);
        }

        IScheduler _scheduler;
        ISchedulerJobProvider _schedulerJobProvider;
    }
}

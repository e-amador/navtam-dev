"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserNewComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var angular_l10n_1 = require("angular-l10n");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var toastr_service_1 = require("./../../common/toastr.service");
//import { PasswordValidator } from '../shared/password.validator'
var phone_validator_1 = require("../shared/phone.validator");
var data_service_1 = require("../shared/data.service");
var windowRef_service_1 = require("../../common/windowRef.service");
var UserNewComponent = /** @class */ (function () {
    function UserNewComponent(toastr, fb, dataService, memStorageService, router, activatedRoute, winRef, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.winRef = winRef;
        this.locale = locale;
        this.translation = translation;
        this.model = null;
        this.isSubmitting = false;
    }
    UserNewComponent.prototype.ngOnInit = function () {
        this.roleOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('UserSession.SelectRole')
            },
            width: "100%"
        };
        this.orgOptions = {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('UserSession.SelectUserOrg')
            },
        };
        this.doaOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('UserSession.SelectMultiDoas')
            },
            multiple: true,
            width: "100%"
        };
        this.model = this.prepareEmptyUser();
        this.configureReactiveForm();
        this.loadRoles('-1');
        this.loadDoas('-1');
        this.loadOrganizations();
    };
    UserNewComponent.prototype.dissableOrganizationDropdown = function () {
        var appConfig = this.winRef.appConfig;
        return this.isReadOnly || (appConfig && !appConfig.sysAdmin);
    };
    UserNewComponent.prototype.phoneNumberFocus = function (ctrName) {
        var phoneCtrl = this.$ctrl(ctrName);
        var value = phone_validator_1.PhoneValidator.stripFormat(phoneCtrl.value);
        phoneCtrl.setValue(value);
    };
    UserNewComponent.prototype.phoneNumberBlur = function (ctrName) {
        var phoneCtrl = this.$ctrl(ctrName);
        var value = phone_validator_1.PhoneValidator.formatTenDigits(phoneCtrl.value);
        phoneCtrl.setValue(value);
    };
    UserNewComponent.prototype.onOrgChanged = function (data) {
        if (data.value) {
            this.userForm.get('org').setValue(data.value);
            this.loadRoles(data.value);
            this.loadDoas(data.value);
        }
        else {
            this.userForm.get('org').setValue(-1);
            this.loadRoles('-1');
            this.loadDoas('-1');
        }
        this.userForm.get('doas').setValue("");
        this.userForm.get('roles').setValue("-1");
    };
    UserNewComponent.prototype.onRoleChanged = function (data) {
        if (data.value && data.value.length > 0) {
            this.userForm.get('roles').setValue(data.value);
        }
        else {
            this.userForm.get('roles').setValue("-1");
        }
    };
    UserNewComponent.prototype.onDoaChanged = function (data) {
        if (data.value && data.value.length > 0) {
            this.userForm.get('doas').setValue(data.value.join(','));
        }
        else {
            this.userForm.get('doas').setValue("");
        }
    };
    UserNewComponent.prototype.onApproveChanged = function (checked) {
        var control = this.userForm.get('approved');
        control.setValue(checked);
        control.markAsDirty();
    };
    UserNewComponent.prototype.onDisableChanged = function (e) {
        var control = this.$ctrl('disabled');
        var val = control.value;
        control.setValue(!val);
        control.markAsDirty();
    };
    UserNewComponent.prototype.backToUserList = function () {
        this.router.navigate(['/administration']);
    };
    UserNewComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.isSubmitting)
            return;
        this.isSubmitting = true;
        var user = this.prepareSaveUser();
        this.dataService.saveUser(user)
            .subscribe(function (data) {
            var msg = _this.translation.translate('UserSession.SuccessUserSaved');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            _this.router.navigate(["/administration"]);
        }, function (error) {
            _this.isSubmitting = false;
            var msg = _this.translation.translate('UserSession.FailureUserSaved');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    UserNewComponent.prototype.validateUsername = function () {
        var usernameCtrl = this.userForm.get("username");
        return usernameCtrl.valid || this.userForm.pristine;
    };
    UserNewComponent.prototype.validateFirstName = function () {
        var firstNameCtrl = this.userForm.get("firstName");
        return firstNameCtrl.valid || this.userForm.pristine;
    };
    UserNewComponent.prototype.validateLastName = function () {
        var lastNameCtrl = this.userForm.get("lastName");
        return lastNameCtrl.valid || this.userForm.pristine;
    };
    //validatePassword() {
    //    const passwordCtrl = this.userForm.get('grpPassword.password');
    //    return passwordCtrl.valid || this.userForm.pristine;
    //}
    //validateConfirmPassword() {
    //    const confirmPasswordCtrl = this.userForm.get('grpPassword.confirmPassword');
    //    return confirmPasswordCtrl.valid || this.userForm.pristine;
    //}
    UserNewComponent.prototype.validateEmailAddress = function () {
        var emailCtrl = this.userForm.get("email");
        return emailCtrl.valid || this.userForm.pristine;
    };
    UserNewComponent.prototype.validateAddress = function () {
        var emailCtrl = this.userForm.get("address");
        return emailCtrl.valid || this.userForm.pristine;
    };
    UserNewComponent.prototype.validatePhoneNumber = function () {
        var emailCtrl = this.userForm.get("phoneNumber");
        return emailCtrl.valid || this.userForm.pristine;
    };
    UserNewComponent.prototype.validateFax = function () {
        var emailCtrl = this.userForm.get("fax");
        return emailCtrl.valid || this.userForm.pristine;
    };
    UserNewComponent.prototype.validateRoles = function () {
        var fc = this.userForm.get('roles');
        return (fc.value !== "" && fc.value !== "-1") || this.userForm.pristine;
    };
    UserNewComponent.prototype.validateOrganization = function () {
        var fc = this.userForm.get('org');
        return fc.value !== -1 || this.userForm.pristine;
    };
    UserNewComponent.prototype.validateDoas = function () {
        var fc = this.userForm.get('doas');
        return fc.value !== "" || this.userForm.pristine;
    };
    UserNewComponent.prototype.loadOrganizations = function () {
        var _this = this;
        this.dataService.getOrganizations()
            .subscribe(function (org) {
            _this.orgs = org.map(function (o) {
                return {
                    id: o.id,
                    text: o.name
                };
            });
            _this.selectedOrgId = _this.model.organizationId || -1;
            _this.orgs.unshift({ id: "-1", text: "" });
        });
    };
    UserNewComponent.prototype.loadRoles = function (orgId) {
        var _this = this;
        if (orgId !== '-1') {
            this.dataService.getAvailableRolesByOrg(orgId)
                .subscribe(function (roles) {
                _this.roles = roles.map(function (r) {
                    return {
                        id: r.roleId,
                        text: r.roleName
                    };
                });
                _this.roles.unshift({ id: "-1", text: "" });
            });
        }
        this.selectedRoleId = '-1';
    };
    UserNewComponent.prototype.loadDoas = function (orgId) {
        var _this = this;
        if (orgId !== '-1') {
            this.dataService.getDoasInOrg(orgId)
                .subscribe(function (doas) {
                _this.doas = doas.map(function (d) {
                    return {
                        id: d.id.toString(),
                        text: d.name
                    };
                });
                _this.selectedDoasIds = _this.getDefaultDoas(doas);
                _this.doas.unshift({ id: "-1", text: "" });
            });
        }
        else {
            this.selectedDoasIds = [];
        }
    };
    UserNewComponent.prototype.configureReactiveForm = function () {
        this.userForm = this.fb.group({
            username: [{ value: this.model.username, disabled: false }, [forms_1.Validators.required, forms_1.Validators.minLength(3), forms_1.Validators.maxLength(16), forms_1.Validators.pattern('[a-zA-Z0-9\-_]+$')]],
            firstName: [{ value: this.model.firstName, disabled: false }, [forms_1.Validators.required, forms_1.Validators.pattern('[a-zA-Z0-9\-_. ]+$')]],
            lastName: [{ value: this.model.lastName, disabled: false }, [forms_1.Validators.required, forms_1.Validators.pattern('[a-zA-Z0-9\-_. ]+$')]],
            //grpPassword: this.fb.group({
            //    password: [{ value: "", disabled: false }, [Validators.required, Validators.pattern("((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%]).{8,20})")]],
            //    confirmPassword: [{ value: "", disabled: false }, [Validators.required]],
            //}, {validator: PasswordValidator.MatchPassword }),
            position: { value: this.model.position, disabled: false },
            email: [{ value: this.model.email, disabled: false }, [forms_1.Validators.required, forms_1.Validators.pattern("[a-zA-Z0-9._+-]{1,}@[a-zA-Z0-9._-]{2,}[.]{1}[a-zA-Z]{2,3}")]],
            roles: [{ value: this.model.roleIds, disabled: false }, [roleValidator]],
            org: [{ value: this.model.organizationId || -1, disabled: false }, [orgValidator]],
            address: [{ value: this.model.address, disabled: false }, [forms_1.Validators.required]],
            doas: [{ value: this.model.doaIds, disabled: false }, [doaValidator]],
            //approved: { value: this.model.approved, disabled: false },
            disabled: { value: this.model.deleted, disabled: false },
            phoneNumber: [{ value: this.model.phoneNumber, disabled: false }, [forms_1.Validators.required, phoneNumberValidator]],
            fax: [{ value: this.model.fax, disabled: false }, [phoneNumberValidator]]
        });
    };
    UserNewComponent.prototype.$ctrl = function (name) {
        return this.userForm.get(name);
    };
    UserNewComponent.prototype.prepareSaveUser = function () {
        var user = {
            username: this.userForm.get('username').value,
            firstName: this.userForm.get('firstName').value,
            lastName: this.userForm.get('lastName').value,
            //password: this.userForm.get('grpPassword.password').value,
            //confirmPassword: this.userForm.get('grpPassword.confirmPassword').value,
            position: this.userForm.get('position').value,
            email: this.userForm.get('email').value,
            roleId: this.userForm.get('roles').value,
            organizationId: this.userForm.get('org').value,
            address: this.userForm.get('address').value,
            approved: true,
            disabled: this.userForm.get('disabled').value,
            phoneNumber: this.userForm.get('phoneNumber').value,
            fax: this.userForm.get('fax').value,
            doaIds: this.userForm.get('doas').value,
            deleted: false,
        };
        return user;
    };
    UserNewComponent.prototype.prepareEmptyUser = function () {
        var user = {
            username: "",
            firstName: "",
            lastName: "",
            position: "",
            email: "",
            roleId: "",
            organizationId: this.getOrganizationId(),
            //password: "",
            //confirmPassword:"",
            address: "",
            approved: true,
            disabled: false,
            phoneNumber: "",
            fax: "",
            doaIds: "",
            deleted: false,
        };
        return user;
    };
    UserNewComponent.prototype.getOrganizationId = function () {
        var appConfig = this.winRef.appConfig;
        return appConfig && !appConfig.sysAdmin ? appConfig.orgId : null;
    };
    UserNewComponent.prototype.getDefaultDoas = function (doas) {
        var appConfig = this.winRef.appConfig;
        if ((appConfig && !appConfig.sysAdmin) || (doas.length === 1)) {
            return this.doas.map(function (d) { return d.id.toString(); });
        }
        return [];
    };
    UserNewComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/user/user-new.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            router_1.ActivatedRoute,
            windowRef_service_1.WindowRef,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], UserNewComponent);
    return UserNewComponent;
}());
exports.UserNewComponent = UserNewComponent;
function roleValidator(c) {
    if (!c.value) {
        return { 'required': true };
    }
    else if (c.value === '-1') {
        return { 'required': true };
    }
    return null;
}
function doaValidator(c) {
    if (!c.value) {
        return { 'required': true };
    }
    return null;
}
function orgValidator(c) {
    if (c.value === -1) {
        return { 'required': true };
    }
    return null;
}
function phoneNumberValidator(c) {
    var value = c.value;
    if (value && !phone_validator_1.PhoneValidator.isValidPhoneNumber(c.value, 10)) {
        return { 'pattern': true };
    }
    return null;
}
//# sourceMappingURL=user-new.component.js.map
﻿using System;
using System.Collections.Generic;

namespace NDS.DataBus.Contracts
{
    public interface INdsMessage : IDisposable
    {
        /// <summary>
        /// Get/Set message body
        /// </summary>
        string Body { get; }
        /// <summary>
        /// Get/Set message properties
        /// </summary>
        object this[string name] { get; }
        /// <summary>
        /// Enumerate message properties
        /// </summary>
        IEnumerable<Property> Properties { get; }
    }
}

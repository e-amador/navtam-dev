﻿function CreateCredentials($username, $password) {
	$secstr = New-Object -TypeName System.Security.SecureString
	$password.ToCharArray() | ForEach-Object {$secstr.AppendChar($_)}

    return new-object -typename System.Management.Automation.PSCredential -argumentlist $username, $secstr
}

function StopIIS($session) {
	Try {
		Write-Host " ";
        Write-Host "-----------------------------------------------------";
        Write-Host "-----------------------------------------------------";
        Write-Host "Attempting to stop IIS";
        Write-Host "-----------------------------------------------------";

        Invoke-Command -Session $session -ErrorAction Stop -ScriptBlock {iisreset /stop }

		Write-Host "IIS Sucessfully stoped.";
	}
	Catch {
		Write-Host "There was an error running StopIIS script";
		Write-Host $_.Exception.Message;
		throw 1
	}
	Finally {
        Write-Host "-----------------------------------------------------";
    }		
}

function StartIIS($session) {
	Try {
		Write-Host " ";
        Write-Host "-----------------------------------------------------";
        Write-Host "-----------------------------------------------------";
        Write-Host "Attempting to star IIS";
        Write-Host "-----------------------------------------------------";

        Invoke-Command -Session $session -ErrorAction Stop -ScriptBlock {iisreset /start }

		Write-Host "IIS Sucessfully started.";
	}
	Catch {
		Write-Host "There was an error running StartIIS script";
		Write-Host $_.Exception.Message;
		throw 1	
	}
	Finally {
        Write-Host "-----------------------------------------------------";
    }		
}

function WebDeploymentPackageToServer($deploymentDir, $server, $session) {
	Try {
		Write-Host " ";
        Write-Host "-----------------------------------------------------";
        Write-Host "-----------------------------------------------------";
        Write-Host "Deploy Content to Remote IIS server";
        Write-Host "-----------------------------------------------------";

		$dest = "C:\TEMP"
		CopyFilesToRemoteServer $session $deploymentDir $dest 1

        Invoke-Command -ErrorAction Stop -ScriptBlock {
            Param($dir, $server)

			$cmdpath = 'c:\windows\system32\cmd.exe /c'
			$deployCmd = "NES.deploy.cmd /y /m:$server" 

			$dir = "$dir\NAVTAM"
			Set-Location $dir -ErrorAction Stop
			Invoke-Expression "$cmdpath $deployCmd" -ErrorAction Stop
		} -ArgumentList $dest, $server -Session $session 

		Write-Host "Deployment Completed";
	}
	Catch {
		Write-Host "There was an error deploying the site";
		Write-Host $_.Exception.Message;
		throw 1	
	}
	Finally {
        Write-Host "-----------------------------------------------------";
    }		
}

function RunMigrations($deploymentDir) {
	Try {
		Write-Host " ";
        Write-Host "-----------------------------------------------------";
        Write-Host "-----------------------------------------------------";
        Write-Host "Migrate Database";
        Write-Host "-----------------------------------------------------";

		$command = "$deploymentDir\bin\migrate.exe Core.Data.EF.dll /startUpDirectory=""$deploymentDir\bin"" /startupConfigurationFile=""$deploymentDir\web.config"" /verbose"
        Invoke-Expression $command -ErrorAction Stop

		Write-Host "Migration Completed";
	}
	Catch {
		Write-Host "There was an error running migrations";
		Write-Host $_.Exception.Message;
		throw 1	
	}
	Finally {
        Write-Host "-----------------------------------------------------";
    }		
}

function RunMigrationsRemotly($session, $remoteDeploymentDir) {
	Try {
		Write-Host " ";
        Write-Host "-----------------------------------------------------";
        Write-Host "-----------------------------------------------------";
        Write-Host "Migrate Database";
        Write-Host "-----------------------------------------------------";

        Invoke-Command -ErrorAction Stop -Session $session -ScriptBlock { 
            Param($remoteDeploymentDir)
            
            $command = "$remoteDeploymentDir\bin\migrate.exe Core.Data.EF.dll /startUpDirectory=""$remoteDeploymentDir\bin"" /startupConfigurationFile=""$remoteDeploymentDir\web.config"" /verbose"

            Invoke-Expression $command -ErrorAction Stop
        } -ArgumentList $remoteDeploymentDir 

		Write-Host "Migration Completed";
	}
	Catch {
		Write-Host "There was an error running migrations";
		Write-Host $_.Exception.Message;
		throw 1	
	}
	Finally {
        Write-Host "-----------------------------------------------------";
    }		
}

function InstallNpmPackages($localDeploymentDir) {
	Try {
		Write-Host " ";
        Write-Host "-----------------------------------------------------";
        Write-Host "-----------------------------------------------------";
        Write-Host "Install npm packages for the WebApp";
        Write-Host "-----------------------------------------------------";

		if ( -Not ($env:Path.Contains("C:\Program Files\nodejs\")) ) { 
			$env:Path += ";C:\Program Files\nodejs\"
		}

        Set-Location -Path $localDeploymentDir;

		#Run npm install in the 
		$cmdpath = "c:\windows\system32\cmd.exe /c npm install --loglevel=error" 
		Invoke-Expression -Command $cmdpath

		Write-Host "Npm Install Completed";
	}
	Catch {
		Write-Host "There was an error installing npm packages";
		Write-Host $_.Exception.Message;
		throw 1	
	}
	Finally {
        Write-Host "-----------------------------------------------------";
    }		
}

function DeployWebApp([string]$server, [string]$webSiteName, [string]$localPath, [string]$username, [string]$password) {
    $msdeploy = "C:\Program Files\IIS\Microsoft Web Deploy V3\msdeploy.exe";
    cmd.exe /C $("`"{3}`" -verb:sync -source:iisapp=`"{0}`" -dest:computerName=`"{1}`",UserName=`"{4}`",Password=`"{5}`",iisApp=`"{2}`" "  -f $localPath, $server, $webSiteName , $msdeploy, $username, $password )
    #cmd.exe /C $("`"{3}`" -verb:sync -source:contentPath=`"{0}`" -dest:computerName=`"{1}`",UserName=`"{4}`",Password=`"{5}`",contentPath=`"{2}`" "  -f $localPath, $server, $remotePath , $msdeploy, $username, $password )
}

function DeployWinService([string]$server, [string]$remotePath, [string]$localPath, [string]$username, [string]$password) {
    $msdeploy = "C:\Program Files\IIS\Microsoft Web Deploy V3\msdeploy.exe";
    cmd.exe /C $("`"{3}`" -verb:sync -source:contentPath=`"{0}`" -dest:computerName=`"{1}`",UserName=`"{4}`",Password=`"{5}`",contentPath=`"{2}`" "  -f $localPath, $server, $remotePath , $msdeploy, $username, $password )
}

function CopyFilesToRemoteServer($remoteSession, $source, $dest, $chkRemoteDir) {

	Try	{
		Write-Host " ";
        Write-Host "-----------------------------------------------------";
        Write-Host "-----------------------------------------------------";
        Write-Host "Copy files to remnote server";
        Write-Host "-----------------------------------------------------";

		if ($dest.StartsWith("\\") -or $dest.StartsWith("Z:\")  ) { 
			#Shared folder on the network
			Remove-Item "$dest\*" -recurse
			Copy-Item -Path $source -Destination $dest -Recurse
		} else {
			#local folder in remote server folder
			if($chkRemoteDir -eq 1) {
				#Ensure remote deployment folder exist and is empty
				EnsureRemoteFolderExist $remoteSession $dest
			}

			#Copy files to remote folder
			Copy-Item -Path "$source" -ToSession $remoteSession -Destination $dest -Recurse
		}

		Write-Host "Files were copied to remote server";
	}
	Catch {
		Write-Host "There was an error copying files to remote server";
		Write-Host $_.Exception.Message;
		throw 1	
	}
	Finally {
        Write-Host "-----------------------------------------------------";
    }		
}


function EnsureRemoteFolderExist($remoteSession, $remoteDir) {
	Try {
		Write-Host " ";
        Write-Host "-----------------------------------------------------";
        Write-Host "-----------------------------------------------------";
		Write-Host "Enforce $remoteDir exist in remote server";
        Write-Host "-----------------------------------------------------";

		Invoke-command -ErrorAction Stop -scriptBlock { 
			Param($path)

			if (Test-Path -Path $path) {
				$attempt = 10
				$success = $false
				$errorMessage = ""
				while ($attempt -gt 0 -and -not $success) {
				  try {
					Remove-Item $path -recurse -ErrorAction Stop
					$success = $true
				  } catch {
					$errorMessage = $_.Exception.Message
					Write-Host "attept#: $attempt, errorMessage: $errorMessage" 
					# remember error information
					$attempt--
					Start-sleep -s 15
				  }
				}

				if (-not $success) {
					Write-Host $errorMessage
					throw 1
				} else {
					New-Item -Path $path -ItemType directory -ErrorAction Stop
				}
			}

		} -ArgumentList $remoteDir -Session $remoteSession 

		Write-Host "Remote deployment folter [$remoteDir] is ready and empty.";
	}
	Catch {
		Write-Host $_.Exception.Message;
		throw 1	
	}
	Finally {
        Write-Host "-----------------------------------------------------";
    }		

}

function EnsureLocalDeploymentDirExist($fullPathDir) {
	Try {
		Write-Host " ";
        Write-Host "-----------------------------------------------------";
        Write-Host "-----------------------------------------------------";
        Write-Host "Create or clean Deployment Folder before Deployment";
        Write-Host "-----------------------------------------------------";

        if (Test-Path -Path $fullPathDir)
        {
            Remove-Item $fullPathDir -recurse -ErrorAction Stop
		    Write-Host "The Deployment exited and was deleted.";
        }
        
        New-Item -Path $fullPathDir -ItemType directory -ErrorAction Stop
		Write-Host "Deployment Folder Create";
	}
	Catch {
		Write-Host "There was an error running SetDeploymentFolder script";
		Write-Host $_.Exception.Message;
		throw 1	
	}
	Finally {
        Write-Host "-----------------------------------------------------";
    }		
}

function InstallWindowsService32($remoteSession, $fullPathExeService) {
	Try {
		Write-Host " ";
        Write-Host "-----------------------------------------------------";
        Write-Host "-----------------------------------------------------";
        Write-Host "Attempting to install 32 bits service: $fullPathExeService";
        Write-Host "-----------------------------------------------------";

        Invoke-Command -ErrorAction Stop -ScriptBlock {
			Param($fullPath)
            $command = "C:\Windows\Microsoft.NET\Framework\v4.0.30319\InstallUtil.exe $fullPath"
            Invoke-Expression $command -ErrorAction Stop  
		} -ArgumentList $fullPathExeService -Session $remoteSession 

		Write-Host "service: $fullPathExeService successfully installed.";
	}
	Catch {
		Write-Host "There was an error installing 32 bits service: $fullPathExeService";
		Write-Host $_.Exception.Message;
		throw 1	
	}
	Finally {
        Write-Host "-----------------------------------------------------";
    }		
}

function InstallWindowsService64($remoteSession, $fullPathExeService) {
	Try {
		Write-Host " ";
        Write-Host "-----------------------------------------------------";
        Write-Host "-----------------------------------------------------";
        Write-Host "Attempting to install 64 bits service : $fullPathExeService";
        Write-Host "-----------------------------------------------------";

        Invoke-Command -ErrorAction Stop -ScriptBlock {
			Param($fullPath)
            $command = "C:\Windows\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe $fullPath"
            Invoke-Expression $command -ErrorAction Stop  
		} -ArgumentList $fullPathExeService -Session $remoteSession 

		Write-Host "service: $fullPathExeService successfully installed.";
	}
	Catch {
		Write-Host "There was an error installing 64 bits service: $fullPathExeService";
		Write-Host $_.Exception.Message;
		throw 1	
	}
	Finally {
        Write-Host "-----------------------------------------------------";
    }		
}



function StartWindowsService($remoteSession, $serviceName) {
	Try {
		Write-Host " ";
        Write-Host "-----------------------------------------------------";
        Write-Host "-----------------------------------------------------";
        Write-Host "Attempting to start service: $serviceName";
        Write-Host "-----------------------------------------------------";

        Invoke-Command -ErrorAction Stop -ScriptBlock {
			Param($name)
			Get-Service -Name $name | Set-Service -Status Running
		} -ArgumentList $serviceName -Session $session 

		Write-Host "service: $serviceName successfully started.";
	}
	Catch {
		Write-Host "There was an error starting service: $serviceName";
		Write-Host $_.Exception.Message;
		throw 1	
	}
	Finally {
        Write-Host "-----------------------------------------------------";
    }		
}

function StopWindowsService($remoteSession, $serviceName) {
	Try {
		Write-Host " ";
        Write-Host "-----------------------------------------------------";
        Write-Host "-----------------------------------------------------";
        Write-Host "Attempting to stop service: $serviceName ";
        Write-Host "-----------------------------------------------------";

        Invoke-Command -ErrorAction Stop -ScriptBlock {
			Param($name)

			$service = Get-WmiObject -Class Win32_Service -Filter "Name = '$name'"
			if ($service -ne $null) 
			{
			  $service | stop-service -Force

			  $service.Delete()
			}
		} -ArgumentList $serviceName -Session $remoteSession 

		Start-sleep -s 10

		Write-Host "service: $serviceName successfully stopped.";
	}
	Catch {
		Write-Host "There was an error stoping service: $serviceName";
		Write-Host $_.Exception.Message;
		throw 1	
	}
	Finally {
        Write-Host "-----------------------------------------------------";
    }		
}

function ZipFolderToFile($pathDir, $zipFullPath, $includeBaseDirectory) {
	Try {
		Write-Host " ";
        Write-Host "-----------------------------------------------------";
        Write-Host "-----------------------------------------------------";
        Write-Host "Zip folder: $pathDir to $zipFullPath ";
        Write-Host "-----------------------------------------------------";

		#If Zip file exist, delete it first
		if (Test-Path -Path $zipFullPath)
		{
			Remove-Item $zipFullPath -ErrorAction Stop
		}

		Add-Type -AssemblyName "system.io.compression.filesystem"
		$compressionLevel = [System.IO.Compression.CompressionLevel]::Optimal
		[io.compression.zipfile]::CreateFromDirectory($pathDir, $zipFullPath, $compressionLevel, $includeBaseDirectory)

		Write-Host "$zipFullPath created.";
	}
	Catch {
		Write-Host "There was an error creating zip file : $zipFullPath";
		Write-Host $_.Exception.Message;
		throw 1	
	}
	Finally {
        Write-Host "-----------------------------------------------------";
    }		
} 

function UnZipFileToFolder($zipFullPath, $destPath) {
	Try {
		Write-Host " ";
        Write-Host "-----------------------------------------------------";
        Write-Host "-----------------------------------------------------";
        Write-Host "UnZip zip file: $zipFullPath to $destPath";
        Write-Host "-----------------------------------------------------";

		Remove-Item "$destPath" -recurse

		Add-Type -AssemblyName "system.io.compression.filesystem"
		[io.compression.zipfile]::ExtractToDirectory($zipFullPath, $destPath)

		Write-Host "$zipFullPath Unzipped.";
	}
	Catch {
		Write-Host "There was an error creating Unziping file : $zipFullPath";
		Write-Host $_.Exception.Message;
		throw 1	
	}
	Finally {
        Write-Host "-----------------------------------------------------";
    }		
} 



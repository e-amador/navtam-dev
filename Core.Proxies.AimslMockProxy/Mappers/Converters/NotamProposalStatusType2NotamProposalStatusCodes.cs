﻿namespace NavCanada.Core.Proxies.AimslMockProxy.Mappers.Converters
{
    using AutoMapper;

    using NavCanada.Core.Domain.Model.Enums;
    using NavCanada.Core.Proxies.AimslMockProxy.Proxy;

    class NotamProposalStatusType2NotamProposalStatusCode : ITypeConverter<NotamProposalStatusType, NotamProposalStatusCode>
    {
        public NotamProposalStatusCode Convert(ResolutionContext context)
        {
            NotamProposalStatusType source = (NotamProposalStatusType)context.SourceValue;

            switch (source)
            {
                case NotamProposalStatusType.Pending:
                    return NotamProposalStatusCode.Pending;
                case NotamProposalStatusType.ModifiedAndSubmitted:
                    return NotamProposalStatusCode.ModifiedAndSubmitted;
                case NotamProposalStatusType.Queued:
                    return NotamProposalStatusCode.Queued;
                case NotamProposalStatusType.Rejected:
                    return NotamProposalStatusCode.Rejected;
                case NotamProposalStatusType.Submitted:
                    return NotamProposalStatusCode.Submitted;
                case NotamProposalStatusType.Withdrawn:
                    return NotamProposalStatusCode.Withdrawn;
                case NotamProposalStatusType.Undefined:
                default:
                    return NotamProposalStatusCode.Undefined;
            }

        }

    }
}

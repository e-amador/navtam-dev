﻿using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole()
        {
        }

        public ApplicationRole(string name, string description)
            : base(name)
        {
            Description = description;
        }

        public virtual string Description { get; set; }

        public virtual ICollection<RolePermission> RolePermissions { get; set; }
    }
}
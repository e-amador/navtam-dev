﻿namespace NavCanada.Applications.NotamWiz.Web.NSDs
{
    public class ValueText
    {
        public string Value { get; set; }
        public string Text { get; set; }
        
    }
}
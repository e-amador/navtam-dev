﻿namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    using System.Threading.Tasks;

    using Domain.Model.Entitities;

    public interface IUserPreferenceRepo : IRepository<UserPreference>
    {
        Task<UserPreference> GetExpiredAsync(int userPreferenceId);
    }
}

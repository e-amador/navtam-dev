"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SeriesListComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var toastr_service_1 = require("./../../common/toastr.service");
var router_1 = require("@angular/router");
var data_service_1 = require("../shared/data.service");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var windowRef_service_1 = require("../../common/windowRef.service");
var angular_l10n_1 = require("angular-l10n");
var SeriesListComponent = /** @class */ (function () {
    function SeriesListComponent(toastr, fb, dataService, memStorageService, router, winRef, route, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.route = route;
        this.locale = locale;
        this.translation = translation;
        this.selectedSerie = null;
        this.paging = {
            currentPage: 1,
            pageSize: 0,
            totalCount: 0,
            totalPages: 0,
        };
        this.totalPages = 0;
        this.loadingData = false;
        this.canbeDeleted = true;
        if (this.winRef.appConfig.ccs)
            this.router.navigate(['/administration']);
        this.tableHeaders = {
            sorting: [
                { columnName: 'Subject', direction: 0 },
                { columnName: 'RegionId', direction: 0 },
                { columnName: 'DisseminationCategory', direction: 0 },
                { columnName: 'QCode', direction: 0 },
                { columnName: 'Series', direction: 0 },
            ],
            itemsPerPage: [
                { id: '10', text: '10' },
                { id: '20', text: '20' },
                { id: '50', text: '50' },
                { id: '100', text: '100' },
                { id: '200', text: '200' },
            ]
        };
        this.paging.pageSize = +this.tableHeaders.itemsPerPage[0].id;
        this.queryOptions = {
            entity: "serie",
            page: 1,
            pageSize: this.paging.pageSize,
            sort: "_" + this.tableHeaders.sorting[0].columnName,
            filterValue: ""
        };
    }
    ;
    SeriesListComponent.prototype.ngOnInit = function () {
        if (this.winRef.appConfig.ccs)
            this.router.navigate(['/administration']);
        //Set latest query options
        var queryOptions = this.memStorageService.get(this.memStorageService.ITEM_QUERY_OPTIONS_KEY);
        if (queryOptions && queryOptions.entity === "serie") {
            this.queryOptions = queryOptions;
        }
        else {
            this.queryOptions.sort = this.tableHeaders.sorting[1].columnName; // region
        }
        this.configureReactiveForm();
        this.getSeriesInPage(this.queryOptions.page);
    };
    Object.defineProperty(SeriesListComponent.prototype, "langCulture", {
        get: function () {
            return (window["app"].cultureInfo || "en").toUpperCase();
        },
        enumerable: false,
        configurable: true
    });
    SeriesListComponent.prototype.configureReactiveForm = function () {
        var _this = this;
        this.serieListForm = this.fb.group({
            searchSerie: [{ value: this.queryOptions.filterValue, disabled: false }, []],
        });
        this.$ctrl("searchSerie").valueChanges.debounceTime(1000).subscribe(function () { return _this.updateSerieList(); });
    };
    SeriesListComponent.prototype.updateSerieList = function () {
        if (this.loadingData)
            return;
        var subject = this.$ctrl('searchSerie');
        this.queryOptions.filterValue = subject.value;
        this.getSeriesInPage(this.queryOptions.page);
    };
    SeriesListComponent.prototype.$ctrl = function (name) {
        return this.serieListForm.get(name);
    };
    SeriesListComponent.prototype.selectSerieAllocation = function (serie) {
        if (this.selectedSerie) {
            this.selectedSerie.isSelected = false;
        }
        //this.canbeDeleted = true;
        this.selectedSerie = serie;
        this.setCanBeDeleted(this.selectedSerie.id);
        this.selectedSerie.isSelected = true;
        this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, this.selectedSerie.id);
    };
    SeriesListComponent.prototype.itemsPerPageChanged = function (e) {
        this.queryOptions.pageSize = e.value;
        this.getSeriesInPage(1);
    };
    SeriesListComponent.prototype.onPageChange = function (page) {
        this.getSeriesInPage(page);
    };
    SeriesListComponent.prototype.sort = function (index) {
        var dir = this.tableHeaders.sorting[index].direction;
        for (var i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }
        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
        }
        this.getSeriesInPage(this.queryOptions.page);
    };
    SeriesListComponent.prototype.sortingClass = function (index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';
        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';
        return 'sorting';
    };
    SeriesListComponent.prototype.confirmDeleteSerieAllocation = function () {
        if (this.selectedSerie) {
            if (this.canbeDeleted) {
                this.deleteSeries();
            }
            else {
                $('#modal-delete').modal({});
            }
        }
    };
    SeriesListComponent.prototype.deleteSeries = function () {
        var _this = this;
        if (this.selectedSerie) {
            this.dataService.deleteSerie(this.selectedSerie.id)
                .subscribe(function (result) {
                _this.getSeriesInPage(_this.paging.currentPage);
                var msg = _this.translation.translate('SeriesAllocationSession.SuccessSerieDeleted');
                _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            }, function (error) {
                var msg = _this.translation.translate('SeriesAllocationSession.FailureSerieDeleted');
                _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
        }
    };
    SeriesListComponent.prototype.setCanBeDeleted = function (id) {
        var _this = this;
        this.dataService.serieCanBeDeleted(id)
            .subscribe(function (result) {
            _this.canbeDeleted = result;
        }, function (error) {
            var msg = _this.translation.translate('SeriesAllocationSession.FailureSerieDeleted');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    SeriesListComponent.prototype.getSeriesInPage = function (page) {
        var _this = this;
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, this.queryOptions);
            this.dataService.getRegisteredSeries(this.queryOptions)
                .subscribe(function (series) {
                _this.processSeries(series);
            });
        }
        catch (e) {
            this.loadingData = false;
        }
    };
    SeriesListComponent.prototype.processSeries = function (series) {
        this.paging = series.paging;
        this.totalPages = series.paging.totalPages;
        for (var iter = 0; iter < series.data.length; iter++)
            series.data[iter].index = iter;
        var lastSerieIdSelected = this.memStorageService.get(this.memStorageService.ITEM_ID_KEY);
        if (series.data.length > 0) {
            this.seriesAllocations = series.data;
            var index = 0;
            if (lastSerieIdSelected) {
                index = this.seriesAllocations.findIndex(function (x) { return x.id === lastSerieIdSelected; });
                if (index < 0)
                    index = 0; // when not found first should be selected;
            }
            this.seriesAllocations[index].isSelected = true;
            this.selectedSerie = this.seriesAllocations[index];
            this.loadingData = false;
            //this.canbeDeleted = true;
            this.setCanBeDeleted(this.selectedSerie.id);
        }
        else {
            this.seriesAllocations = [];
            this.selectedSerie = null;
            this.loadingData = false;
            this.canbeDeleted = false;
        }
    };
    SeriesListComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/series/series-list.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], SeriesListComponent);
    return SeriesListComponent;
}());
exports.SeriesListComponent = SeriesListComponent;
//# sourceMappingURL=series-list.component.js.map
﻿namespace NavCanada.Core.Common.Common
{
    public enum CardinalDirectionEnum
    {
        North,  
        East,   
        South,  
        West    
    }
}

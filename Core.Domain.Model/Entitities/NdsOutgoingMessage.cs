﻿using NavCanada.Core.Domain.Model.Enums;
using System;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class NdsOutgoingMessage
    {
        public Guid Id { get; set; }
        public Guid GroupId { get; set; }
        public int GroupOrder { get; set; }
        public Guid? NextId { get; set; }
        public string Destination { get; set; }
        public NdsMessageFormat Format { get; set; }
        public string Content { get; set; }
        public string Metadata { get; set; }
        public NdsMessageStatus Status { get; set; }
        public string ErrorMessage { get; set; }
        public DateTime SubmitDate { get; set; }
    }
}

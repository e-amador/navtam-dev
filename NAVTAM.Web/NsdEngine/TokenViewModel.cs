﻿using System;
using System.Collections.Generic;

namespace NAVTAM.NsdEngine
{
    public class TokenViewModel
    {
        public TokenViewModel()
        {
            Annotations = new List<ProposalAnnotationViewModel>();
        }

        public string SubjectId { get; set; }
        public List<ProposalAnnotationViewModel> Annotations { get; set; }
        public string Originator { get; set; } // a.k.a. OriginatorName 
        public string OriginatorEmail { get; set; } 
        public string OriginatorPhone { get; set; } 
        public string ItemD { get; set; }
        public DateTime? StartActivity { get; set; }
        public DateTime? EndValidity { get; set; }
        public bool Immediate { get; set; }
        public bool Permanent { get; set; }
        public bool Estimated { get; set; }
        public string NoteToNof { get; set; }
        public string RejectionReason { get; set; }
        public bool Urgent { get; set; }
        public bool IsBilingual { get; set; }
        public string AmendPubMessage { get; set; }
        public string AmendPubMessageFrench { get; set; }
        public bool OverrideBilingual { get; set; }
    }
}
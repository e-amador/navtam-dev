﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NavCanada.Core.Proxies.AeroRdsProxy;

namespace NAVTAM.Web.Test.AeroRDS
{
    [TestClass]
    public class AeroRDSUrlBuilderTests
    {
        private AeroRDSUrlBuilder _aeroRDSUrlBuilder;

        [TestInitialize]
        public void InitializeTest()
        {
            _aeroRDSUrlBuilder = new AeroRDSUrlBuilder();
        }

        [TestMethod]
        public void AeroRDS_DashFormatDate_test()
        {
            Assert.AreEqual(AeroRDSUrlBuilder.DashFormatDate(new DateTime(2019, 4, 4)), "2019-04-04");
        }

        [TestMethod]
        public void AeroRDS_GetAirspacesActiveUrl_test()
        {
            var date = AeroRDSUrlBuilder.DashFormatDate(DateTime.UtcNow);

            var expected = $"AirspaceFeature?ActiveDate={date}&$inlinecount=allpages&$filter=(Type eq 'Fir' and (startswith(Identifier, 'C') or Identifier eq 'BGGL' or Identifier eq 'BIRD' or Identifier eq 'EGGX'))";
            var actual = _aeroRDSUrlBuilder.GetAirspacesActiveDateUrl("BGGL BIRD EGGX");
            Assert.AreEqual(expected, actual);

            expected = $"AirspaceFeature?ActiveDate={date}&$inlinecount=allpages&$filter=(Type eq 'Fir' and (startswith(Identifier, 'C')))";
            actual = _aeroRDSUrlBuilder.GetAirspacesActiveDateUrl("");
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AeroRDS_GetAirspacesEffectiveUrl_test()
        {
            var d = DateTime.UtcNow;
            var date = AeroRDSUrlBuilder.DashFormatDate(d);

            var expected = $"AirspaceFeature?$inlinecount=allpages&$filter=(Type eq 'Fir' and EffectiveDate eq datetime'{date}' and (startswith(Identifier, 'C') or Identifier eq 'BGGL' or Identifier eq 'BIRD' or Identifier eq 'EGGX'))";
            var actual = _aeroRDSUrlBuilder.GetAirspacesEffectiveDateUrl(d, "BGGL BIRD EGGX");
            Assert.AreEqual(expected, actual);

            expected = $"AirspaceFeature?$inlinecount=allpages&$filter=(Type eq 'Fir' and EffectiveDate eq datetime'{date}' and (startswith(Identifier, 'C')))";
            actual = _aeroRDSUrlBuilder.GetAirspacesEffectiveDateUrl(d, "");
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AeroRDS_GetAerodromesActiveUrl_test()
        {
            var date = AeroRDSUrlBuilder.DashFormatDate(DateTime.UtcNow);

            var expected = $"AerodromeFeature?ActiveDate={date}&$inlinecount=allpages&$filter=(Country eq 'CAN' or Identifier eq 'BGGL' or Identifier eq 'BIRD' or Identifier eq 'EGGX')";
            var actual = _aeroRDSUrlBuilder.GetAerodromesActiveDateUrl("BGGL BIRD EGGX");
            Assert.AreEqual(expected, actual);

            expected = $"AerodromeFeature?ActiveDate={date}&$inlinecount=allpages&$filter=(Country eq 'CAN')";
            actual = _aeroRDSUrlBuilder.GetAerodromesActiveDateUrl("");
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AeroRDS_GetAerodromesEffectiveUrl_test()
        {
            var d = DateTime.UtcNow;
            var date = AeroRDSUrlBuilder.DashFormatDate(d);

            var expected = $"AerodromeFeature?$inlinecount=allpages&$filter=(EffectiveDate eq datetime'{date}' and (Country eq 'CAN' or Identifier eq 'BGGL' or Identifier eq 'BIRD' or Identifier eq 'EGGX'))";
            var actual = _aeroRDSUrlBuilder.GetAerodromesEffectiveDateUrl(d, "BGGL BIRD EGGX");
            Assert.AreEqual(expected, actual);

            expected = $"AerodromeFeature?$inlinecount=allpages&$filter=(EffectiveDate eq datetime'{date}' and (Country eq 'CAN'))";
            actual = _aeroRDSUrlBuilder.GetAerodromesEffectiveDateUrl(d, "");
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AeroRDS_GetFeatureActiveUrl_test()
        {
            var date = AeroRDSUrlBuilder.DashFormatDate(DateTime.UtcNow);
            var expected = $"RunwayFeature?ActiveDate={date}&$inlinecount=allpages";
            var actual = _aeroRDSUrlBuilder.GetFeatureActiveDateUrl("RunwayFeature");
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AeroRDS_GetFeatureEffectiveUrl_test()
        {
            var d = DateTime.UtcNow;
            var date = AeroRDSUrlBuilder.DashFormatDate(d);

            var expected = $"RunwayFeature?$inlinecount=allpages&$filter=(EffectiveDate eq datetime'{date}')";
            var actual = _aeroRDSUrlBuilder.GetFeatureEffectiveDateUrl("RunwayFeature", d);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AeroRDS_GetFixActiveUrl_test()
        {
            var date = AeroRDSUrlBuilder.DashFormatDate(DateTime.UtcNow);
            var expected = $"FixFeature?ActiveDate={date}&$inlinecount=allpages&$filter=(FIR eq 'CZUL' or FIR eq 'CZVR')";
            var actual = _aeroRDSUrlBuilder.GetFixActiveDateUrl("CZUL CZVR".Split(' '));
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AeroRDS_GetFixEffectiveUrl_test()
        {
            var d = DateTime.UtcNow;
            var date = AeroRDSUrlBuilder.DashFormatDate(d);

            var expected = $"FixFeature?$inlinecount=allpages&$filter=(EffectiveDate eq datetime'{date}' and (FIR eq 'CZUL' or FIR eq 'CZVR' or FIR eq 'CZEG'))";
            var actual = _aeroRDSUrlBuilder.GetFixEffectiveDateUrl(d, "CZUL CZVR CZEG".Split(' '));
            Assert.AreEqual(expected, actual);
        }
    }
}

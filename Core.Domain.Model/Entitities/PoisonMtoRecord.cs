﻿namespace NavCanada.Core.Domain.Model.Entitities
{
    public class PoisonMtoRecord
    {
        /// <summary>
        ///     Unique identifier of the poison Mto
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        ///     Number of times that the Task Engine has failed to process this Mto.
        /// </summary>
        /// <remarks>
        ///     When this number reaches the max number of times the Task Engine is set up to process Mtos, the message is moved to
        ///     the PoisonMtos table for further analysis.
        /// </remarks>
        public int NumTries { get; set; }
    }
}
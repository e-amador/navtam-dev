﻿using NavCanada.Core.Scheduler.Contracts;
using NavCanada.Core.Scheduler.ScheduleJobs;
using System.Collections.Generic;

namespace NavCanada.Core.Scheduler
{
    public class SchedulerJobFactory
    {
        public SchedulerJobFactory()
        {
            this._schedulerJobFactories = GetFactories();
        }

        public IScheduleJob GetSchedulerJob(string identityName)
        {
            IScheduleJob schedulerJob;
            _schedulerJobFactories.TryGetValue(identityName, out schedulerJob);
            return schedulerJob;
        }

        public List<IScheduleJob> GetSchedulerJobs()
        {
            var result = new List<IScheduleJob>();
            foreach (var f in _schedulerJobFactories)
            {
                result.Add(f.Value);
            }

            return result;
        }

        private IDictionary<string, IScheduleJob> GetFactories()
        {
            return new Dictionary<string, IScheduleJob>
            {
                { AeroRdsScheduleJob.JobIdentity, new AeroRdsScheduleJob() }
            };
        }

        private readonly IDictionary<string, IScheduleJob> _schedulerJobFactories;
    }
}

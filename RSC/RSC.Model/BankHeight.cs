﻿namespace RSC.Model
{
    /// <summary>
    /// Height of snowbank in feet and/or inches.
    /// </summary>
    public class BankHeight
    {
        /// <summary>
        /// Height in feet.
        /// </summary>
        public int BankHeightFt { get; set; }
        /// <summary>
        /// Height in inches.
        /// </summary>
        public int BankHeightInch { get; set; }
    }
}
// todo organize...
/*

function parseCordinates(value: string) {
    let parts = (value || "").split(' ');

    if (parts.length !== 2)
        return null;

    return tryParseDMSCordinates(parts) || tryParseDecimalCordinates(parts) || tryParseSubfixedCordinates(parts);
}

function tryParseDMSCordinates(cords: string[]) {
    const latPat = /^([0-9]){6,7}[NS]$/;
    const lngPat = /^([0-9]){6,7}[WE]$/;
    return cords[0].match(latPat) && cords[1].match(lngPat) ? validateAndConvertCordinates(cords, parseDMSValue) : null;
}

function tryParseDecimalCordinates(cords: string[]) {
    const pat = /^[+-]?([0-9]*[.])?[0-9]+$/;
    return cords[0].match(pat) && cords[1].match(pat) ? validateAndConvertCordinates(cords, parseSafeValue) : null;
}

function tryParseSubfixedCordinates(cords: string[]) {
    const latPat = /^([0-9]*[.])?[0-9]+[NS]$/;
    const lngPat = /^([0-9]*[.])?[0-9]+[WE]$/;
    return cords[0].match(latPat) && cords[1].match(lngPat) ? validateAndConvertCordinates(cords, parseSufixedValue) : null;
}

function validateAndConvertCordinates(cords: string[], parseFun: Function) {
    const lat = parseFun(cords[0], 360.0);
    const lng = parseFun(cords[1], 360.0);
    if (lat >= -180.0 && lat <= 180.0 && lng >= -180.0 && lng <= 180.0) {
        return {
            latitude: lat,
            longitude: lng
        }
    }
    return null;
}

function parseSufixedValue(str: string, defValue: number) {
    const len = str.length;
    const sufix = str[len - 1];
    var sing = sufix === 'W' || sufix === 'S' ? -1 : 1;
    return sing * parseSafeValue(str.substring(0, len - 1), defValue);
}

function parseSafeValue(str: string, defValue: number) {
    try {
        return parseFloat(str);
    } catch (e) {
        return defValue;
    }
}

function parseDMSValue(str: string, defValue: number) {
    try {
        const len = str.length;
        const degrees = parseInt(len === 7 ? str.substring(0, 2) : str.substring(0, 3));
        const minutes = parseInt(len === 7 ? str.substring(2, 4) : str.substring(3, 5));
        const seconds = parseInt(len === 7 ? str.substring(4, 6) : str.substring(5, 7));
        const sufx = str.length === 7 ? str.substring(6, 7) : str.substring(7, 8);
        var sing = sufx === 'W' || sufx === 'S' ? -1 : 1;
        return sing * (degrees + (minutes * 60.0 + seconds) / 3600.0);
    } catch (e) {
        return defValue;
    }
}
*/ 
//# sourceMappingURL=cordinates.utils.js.map
﻿using RSC.Business.SDO;
using RSC.Model;
using System.Collections.Generic;

namespace RSC.Business
{
    public class RSCProposalContext
    {
        public RSCProposal Proposal { get; set; }
        public IAerodrome Aerodrome { get; set; }
        public IRunway[] Runways { get; set; }
        public HashSet<string> ForbiddenWords { get; set; }

        public bool Dummymode { get; set; }
    }
}

﻿using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.NSDs
{
    using System.Linq;

    using FluentValidation;

    using NavCanada.Applications.NotamWiz.Web.Models;
    using NavCanada.Core.Data.NotamWiz.Contracts;

    public class NOTAMValidator : AbstractValidator<NotamViewModel>
    {
        private ILog log = LogManager.GetLogger(typeof(NOTAMValidator));
        IUow Uow {get;set;}


        public NOTAMValidator(IUow uow)
        {
            Uow = uow;

            RuleFor(a => a).Must(a => Uow.IcaoSubjectRepo.GetAll().Any(s => s.Code == a.Code23 && s.Conditions.Any(c => c.Code == a.Code45))).WithName("QCodes") .WithMessage("Invalid or not found NOTAM Code23 or Code45");
            RuleFor(a => a.Fir).Must(f => Uow.SdoSubjectRepo.GetAll().Any(s => s.SdoEntityName == "FIR" && s.Designator == f)).WithMessage("Invalid Fir Code");
            RuleFor(a => a.ItemA)
                .Cascade(FluentValidation.CascadeMode.StopOnFirstFailure)
                .NotEmpty().WithMessage("Item A is Required")
                .Matches("^([^0-9]*)$").WithMessage("No digits allowed in Item A")
                .Must(BeValidItemA).WithMessage("Invalid Item A");

           RuleFor(a => a.ItemE)
                    .NotEmpty().WithMessage("Condition text is required.")
                    .Must(t => CommonValidators.IsValidFreeText(t, Uow))
                    .WithMessage("Invalid Condition text.");

            When(a => !string.IsNullOrEmpty(a.ItemEFrench), () =>
            {
                RuleFor(a => a.ItemEFrench)                    
                    .Must(t => CommonValidators.IsValidFreeText(t, Uow)).WithMessage("Invalid French Condition text.");
            });

            RuleFor(a => a.ItemF)
                 .Matches("^(SFC)|(([0-9]*)(FT|AMSL|AGL))$")
                 .WithMessage("Invalid Item F");

            RuleFor(a => a.ItemG)
                 .Matches("^(UNL)|(([0-9]*)(FT|AMSL|AGL))$")
                 .WithMessage("Invalid Item F");

            RuleFor(a => a.Lower).GreaterThanOrEqualTo(0).WithMessage("Invalid NOTAM Lower.");
            RuleFor(a => a.Upper).GreaterThanOrEqualTo(0).WithMessage("Invalid NOTAM Upper.");
            RuleFor(a => a)                
                .Must(a=> a.Lower<= a.Upper)
                .WithName("Lower")
                .WithMessage("Invalid NOTAM Lower and Upper combination.");

            RuleFor(a => a.Number).NotEmpty().WithMessage("Invalid NOTAM Number.");
            RuleFor(a => a.Purpose)
                .Matches("^([NBOM]{1,3})$")
                .WithMessage("Invalid NOTAM Purpose.");

            RuleFor(a => a.Radius)
                .GreaterThanOrEqualTo(1)
                .WithMessage("Invalid NOTAM Radius.");

            RuleFor(a => a.Scope)
                .Matches("^(A|E|W|AE|AW|EW|K)$")
                .WithMessage("Invalid NOTAM Scope");

            RuleFor(a=> a.Series)
                .Cascade(FluentValidation.CascadeMode.StopOnFirstFailure)
                .NotEmpty().WithMessage("Missing NOTAM Series.")
                .Length(1).WithMessage("Invalid NOTAM Series.")
                .Must(BeValidSeries).WithMessage("Invalid NOTAM Series.");

            RuleFor(a=>a.StartValidity).NotEmpty().WithMessage("Invalid NOTAM Start Validity.");

            RuleFor(a=>a.Traffic)
                .NotEmpty().WithMessage("Missing NOTAM Traffic.")
                .Matches("^(I|V|IV)$").WithMessage("Invalid Traffic.");

            RuleFor(a=>a.Year)
                .GreaterThanOrEqualTo(2015)
                .WithMessage("Invalid NOTAM Year.");

        }


        private bool BeValidItemA(string itemA)
        {
            string[] parts = itemA.Split(' ');

            // validate length of 4
            if (parts.Any(p => p.Length != 4)) return false;

            // validate all items are unique 
            if (parts.Distinct().Count() != parts.Length) return false;

            return true;
        }


        private bool BeValidSeries(string series)
        {
            string usedSeries = Uow.ConfigurationValueRepo.GetAll().Single(c => c.Category == "NOTAM" && c.Name == "NOTAMUsedSeries").Value;
            return usedSeries.Any(s => s == series[0]);
        }

    }
}
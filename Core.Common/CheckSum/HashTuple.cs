﻿using System.Collections;
using System.Collections.Generic;

namespace Core.Common.CheckSum
{
    public struct HashTuple : IEnumerable<byte>
    {
        public HashTuple(params ulong[] values)
        {
            _values = values;
        }

        public IEnumerator<byte> GetEnumerator()
        {
            return GetCharEnumerator().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetCharEnumerator().GetEnumerator();
        }

        private IEnumerable<byte> GetCharEnumerator()
        {
            foreach (var v in _values)
            {
                ulong h1 = v;
                for (int i = 0; i < 8; i++)
                {
                    yield return (byte)(h1 & 0xFFFF);
                    h1 >>= 8;
                }
            }
        }

        ulong[] _values;
    }
}

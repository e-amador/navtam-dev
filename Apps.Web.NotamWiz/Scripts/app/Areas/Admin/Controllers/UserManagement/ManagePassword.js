﻿define(["jquery", "Resources", "kendoExt"], function ($, res, kendoExt) {

    var self = null;
    var kendoValidator = null;
    var labels = {};

    return {

        init: function (lbs) {
            var self = this;
            labels = lbs;
            self.registerValidation();
        },

        registerValidation: function () {
            kendoValidator = $("#passwordForm").kendoValidator({
                messages: {
                    matches: function (input) {
                        return labels.passwordsMustMatch;
                    },
                    passwordRule: labels.passwordStrength,
                    required: function (input) {
                        if (input.is("[name=OldPassword]")) {
                            return labels.oldPasswordRequired;
                        }
                        if (input.is("[name=NewPassword]")) {
                            return labels.newPasswordRequired;
                        }
                        if (input.is("[name=ConfirmPassword]")) {
                            return labels.mustConfirmPassword; 
                        }
                        return "";
                    },
                    passwordDifferentRule: labels.passwordDifferent,
                },
                rules: {
                    passwordRule: function (input) {
                        if (input.is("[name=NewPassword]")) {
                            var validated = true;

                            var value = input.val();  // contains at least 8 chars
                            if (value.length < 8)
                                validated = false;

                            if (!/\d/.test(value))  // contains at least one digit
                                validated = false;

                            if (!/[a-z]/.test(value)) // contains at least one lower case
                                validated = false;

                            if (!/[A-Z]/.test(value)) // contains at least one upper case
                                validated = false;

                            if (!/[\]\[?\/<>~#`!@$%^&*()+={}|_:";',{ ]/.test(value)) // contains at least one special character
                                validated = false;

                            return validated;
                        }
                        return true;
                    },
                    passwordDifferentRule: function (input) {
                        if (input.is("[name=ConfirmPassword]"))
                        {
                            var oldPassword = $("#oldPassword").val();
                            if (oldPassword !== "") {
                                var newPassword = input.val();
                                return newPassword !== oldPassword;
                            }
                        }
                        return true;
                    },
                    matches: function (input) {
                        if (input.is("[name=ConfirmPassword]")) {
                            var confirmPassword = input.val();
                            var match = $("#NewPassword").val();
                            return confirmPassword === match;
                        }
                        return true;
                    }
                }
            }).data("kendoValidator");

            $("#changePasswordButton").click(function (event) {
                if (kendoValidator.validate() === false) {
                    event.preventDefault();
                }
            });
        }
    };
});
﻿namespace NavCanada.Applications.NotamWiz.Web.NSDs.NavaIds.V0R1
{
    public class NavaOption
    {
        public string Text
        {
            get;
            set;
        }

        public int Value
        {
            get;
            set;
        }

        public string Group
        {
            get;
            set;
        }
    }
}
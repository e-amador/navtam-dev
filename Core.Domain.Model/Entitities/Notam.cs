﻿using NavCanada.Core.Domain.Model.Enums;
using System;
using System.Data.Entity.Spatial;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class Notam : INotam
    {
        public Guid Id { get; set; }

        /// <summary>
        /// Id of the Parent NOTAM or ID when it is NEW
        /// </summary>
        public Guid RootId { get; set; }

        #region Replacing Notam
        /// <summary>
        /// Replacing Notam Id.
        /// </summary>
        public Guid? ReplaceNotamId { get; set; }
        /// <summary>
        /// Replacing Notam entity.
        /// </summary>
        public Notam ReplaceNotam { get; set; }
        #endregion

        /// <summary>
        /// Notam type can be New, Replace or Cancel.
        /// </summary>
        public NotamType Type { get; set; }

        /// <summary>
        /// Notam proposal this Notam was generated from
        /// </summary>
        public int ProposalId { get; set; }

        /// <summary>
        /// Sequence number for mass dissemination sequencing
        /// </summary>
        public int SequenceNumber { get; set; }

        /// <summary>
        /// Message for Nof
        /// </summary>
        public string Nof { get; set; }

        #region Notam series number
        public string Series { get; set; }
        public int Number { get; set; }
        public int Year { get; set; }
        public string NotamId { get; set; }
        /// <summary>
        /// Whether this notam number has been rolled.
        /// </summary>
        public bool NumberRolled { get; set; }
        #endregion

        #region Referenced Notam series number
        public string ReferredSeries { get; set; }
        public int ReferredNumber { get; set; }
        public int ReferredYear { get; set; }
        public string ReferredNotamId { get; set; }
        #endregion

        public string Fir { get; set; }
        public string Code23 { get; set; }
        public string Code45 { get; set; }
        public string Traffic { get; set; }
        public string Purpose { get; set; }
        public string Scope { get; set; }
        public int LowerLimit { get; set; }
        public int UpperLimit { get; set; }
        public string Coordinates { get; set; }
        public DbGeography EffectArea { get; set; }
        public int Radius { get; set; }
        public string ItemA { get; set; }

        #region Validity range
        /// <summary>
        /// When it started to be active.
        /// </summary>
        public DateTime? StartActivity { get; set; }
        /// <summary>
        /// When it is scheduled to become inactive.
        /// </summary>
        public DateTime? EndValidity { get; set; }
        /// <summary>
        /// This is an estimated end validity date.
        /// </summary>
        public bool Estimated { get; set; }
        /// <summary>
        /// When it became replaced or canceled.
        /// </summary>
        public DateTime? EffectiveEndValidity { get; set; }
        #endregion

        public string ItemD { get; set; }
        public string ItemE { get; set; }
        public string ItemEFrench { get; set; }
        public string ItemF { get; set; }
        public string ItemG { get; set; }
        public string ItemX { get; set; }
        public string Operator { get; set; }
        public string Originator { get; set; }
        public string OriginatorEmail { get; set; }
        public string OriginatorPhone { get; set; }
        public bool Permanent { get; set; }
        public DateTime Published { get; set; }
        public DateTime Received { get; set; }

        public string NoteToNof { get; set; }
        public string IcaoText { get; set; }

        /// <summary>
        /// Used to produce a high priority aftn message with priority DD instead of GG
        /// </summary>
        public bool Urgent { get; set; }

        #region INotam extras
        public bool Immediate => false;
        public NotamType? ReferenceType => Type;
        #endregion

        public string ModifiedByOrg { get; set; }
        public string ModifiedByUsr { get; set; }

        public bool SeriesChecklist { get; set; }

        public static bool IsObsoleteNotam(Notam ntm, DateTime date)
        {
            if (ntm.Type == NotamType.C) return true; 
            if (ntm.EffectiveEndValidity.HasValue) return true;
            if (!ntm.Estimated && ntm.EndValidity.HasValue && ntm.EndValidity < date) return true;
            return false;
        }

        public static bool IsExpiredNotam(Notam ntm)
        {
            if (!ntm.Estimated && !ntm.EffectiveEndValidity.HasValue && ntm.EndValidity.HasValue && ntm.EndValidity < DateTime.UtcNow)
                return true;

            return false;
        }
    }
}

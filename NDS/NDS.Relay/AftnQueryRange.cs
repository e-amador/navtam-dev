﻿namespace NDS.Relay
{
    public class AftnQueryRange
    {
        public AftnQueryRange(string series, int fromNumber, int toNumber, int year)
        {
            Series = series;
            FromNumber = fromNumber;
            ToNumber = toNumber;
            Year = year;
        }

        public string Series { get; private set; }
        public int FromNumber { get; private set; }
        public int ToNumber { get; private set; }
        public int Year { get; private set; }

        public override string ToString()
        {
            return $"{Series}{FromNumber}/{Year} - {Series}{ToNumber}/{Year}";
        }
    }
}

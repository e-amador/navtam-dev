﻿using System;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class NdsNotamMessage
    {
        public Guid MessageId { get; set; }
        public NdsOutgoingMessage Message { get; set; }
        public Guid NotamId { get; set; }
        public Notam Notam { get; set; }
    }
}

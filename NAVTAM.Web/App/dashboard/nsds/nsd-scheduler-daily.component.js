"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NsdSchedulerDailyComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var windowRef_service_1 = require("../../common/windowRef.service");
var moment = require("moment");
var NsdSchedulerDailyComponent = /** @class */ (function () {
    function NsdSchedulerDailyComponent(windowRef, cdRef) {
        this.windowRef = windowRef;
        this.cdRef = cdRef;
        this.parent = null;
        this.lastEventId = -1;
        this.currentEvent = null;
        this.displayStartDate = null;
        this.slootDuration = 15;
        this.scrollTime = '06:00:00';
        this.initialized = false;
        this.SR = "06:00";
        this.SS = "18:00";
        this.data = null;
        this.editingExtendedHours = false;
        this.eventDetails = {
            id: '',
            starttime: '',
            endtime: '',
            s_sunrise: false,
            s_sunset: false,
            e_sunrise: false,
            e_sunset: false,
            extendedHours: false,
            exStarttime: '',
            exEndtime: '',
            calendarType: ''
        };
        this.defaultColor = '#3a87ad';
        this.selectedColor = '#a1c5f761';
        this.startEventUpdate = null;
        this.start = null;
        this.end = null;
        this.starttime = null;
        this.endtime = null;
        this.hasExtendedHours = false;
        this.sheduleCleared = false;
        this.repeatToggle = false;
        this.dpkStart = null;
        this.dpkEnd = null;
        this.dateEvents = [];
        this.dailyRow = {
            starttime: "",
            endtime: ""
        };
    }
    NsdSchedulerDailyComponent.prototype.ngOnInit = function () {
    };
    NsdSchedulerDailyComponent.prototype.ngOnDestroy = function () {
    };
    NsdSchedulerDailyComponent.prototype.ngAfterViewInit = function () {
        var self = this;
        this.calendar = $('#calendar');
        $('.d-datepicker').each(function () {
            $(this).bootstrapDatepicker({
                autoclose: false,
                todayHighlight: true,
                orientation: "top",
                startView: 0,
                language: "en",
                forceParse: false,
                daysOfWeekDisabled: "",
                calendarWeeks: false,
                toggleActive: true,
                multidate: false,
            }).on('changeDate', function (e) {
                $(this).bootstrapDatepicker('hide');
                self.dateChanged(e);
            }).on('dp.show', function () {
            });
        });
    };
    NsdSchedulerDailyComponent.prototype.initialize = function (data, start, end, slotDuration, events) {
        this.data = data;
        var currentDate = moment();
        this.initCalendar(this.calendar, currentDate);
        if (this.data) {
            if (this.data.length > 0) {
                start = moment(start);
                end = moment(end);
                this.start = new Date(start.format('YYYY'), start.format('MM') - 1, start.format('DD'));
                this.end = new Date(end.format('YYYY'), end.format('MM') - 1, end.format('DD'));
            }
            for (var i = 0; i < this.data.length; i++) {
                var obj = JSON.parse(this.data[i].event);
                var event_1 = this.setEvent(obj);
                if (i === 0) {
                    this.starttime = start.format("HH:mm");
                    this.endtime = end.format("HH:mm");
                    currentDate = event_1.start;
                    this.displayStartDate = start;
                    this.scrollTime = this.starttime + ":00";
                }
                this.calendar.fullCalendar('renderEvent', event_1, true);
            }
            this.calendar.fullCalendar('gotoDate', currentDate);
        }
        else if (events) {
            for (var i = 0; i < events.length; i++) {
                var evt = this.setEvent(events[i]);
                if (i === 0) {
                    currentDate = evt.start;
                }
                this.calendar.fullCalendar('renderEvent', evt, true);
            }
            this.calendar.fullCalendar('gotoDate', currentDate);
        }
        else {
            //trick to enforce refreshing the calendar when modifications were made with calendar hidden 
            this.calendar.fullCalendar('prev');
            this.calendar.fullCalendar('next');
        }
    };
    NsdSchedulerDailyComponent.prototype.toggleShedule = function (event, clearItemD) {
        if (clearItemD === void 0) { clearItemD = false; }
        var isPannelOpenNow = this.isPanelOpen();
        if (event) {
            event.preventDefault();
            this.repeatToggle = true;
        }
        else {
            this.repeatToggle = false;
        }
        if (!this.initialized) {
            this.slideToggle();
        }
        this.showSchedule(this.parent.type === 'clone' ? false : clearItemD);
        var isPannelOpenThen = this.isPanelOpen();
        if (isPannelOpenNow === isPannelOpenThen) {
            if (!isPannelOpenNow || !this.repeatToggle) {
                this.slideToggle();
            }
        }
    };
    NsdSchedulerDailyComponent.prototype.deleteEvents = function (event, clearItemD) {
        if (clearItemD === void 0) { clearItemD = true; }
        if (event && clearItemD) {
            if (this.data && this.data.length > 0) {
                this.parent.resetSchedulersStatus();
            }
            event.preventDefault();
        }
        this.dateEvents = [];
        this.parent.hasScheduler = false;
        this.data = new Object();
        this.start = null;
        this.end = null;
        this.starttime = null;
        this.endtime = null;
        this.clearSchedule(clearItemD);
        if (clearItemD) {
            var enforceToggle = this.parent.type === 'clone' || this.parent.model.proposalId;
            this.parent.schedulerDayWeeks.toggleScheduleAfterDeleteAction(enforceToggle, true);
            this.parent.schedulerDate.toggleScheduleAfterDeleteAction(enforceToggle, true);
        }
        this.parent.enableActivePeridDates();
        this.data = null;
    };
    NsdSchedulerDailyComponent.prototype.resetClearScheduleFlag = function () {
        this.sheduleCleared = true;
        this.repeatToggle = false;
    };
    NsdSchedulerDailyComponent.prototype.slideToggle = function () {
        $('#schedule-daily-content').slideToggle();
    };
    NsdSchedulerDailyComponent.prototype.isPanelOpen = function () {
        return $('#schedule-daily-content').css('display') !== 'none';
    };
    NsdSchedulerDailyComponent.prototype.closePanelIfOpen = function () {
        if (this.isPanelOpen()) {
            this.slideToggle();
        }
    };
    NsdSchedulerDailyComponent.prototype.removeEvents = function () {
        if (this.hasEvents()) {
            this.calendar.fullCalendar('removeEvents');
            this.dateEvents = [];
            this.data = null;
            this.start = null;
            this.end = null;
            $('#d-start').bootstrapDatepicker('update', '');
            $('#d-end').bootstrapDatepicker('update', '');
            this.toggleScheduleAfterDeleteAction(true, true);
        }
    };
    NsdSchedulerDailyComponent.prototype.clearSchedule = function (clearItemD) {
        if (this.data) {
            this.toggleScheduleAfterDeleteAction(true, this.initialized);
            $('#d-start').bootstrapDatepicker('update', '');
            $('#d-end').bootstrapDatepicker('update', '');
            this.calendar.fullCalendar('destroy');
            var start = null;
            var end = null;
            if (this.start) {
                var starttime = moment(this.starttime, 'HH:mm');
                var endtime = moment(this.endtime, 'HH:mm');
                start = moment(this.start).set({
                    hour: starttime.get('hour'),
                    minute: starttime.get('minute'),
                });
                end = moment(this.end).set({
                    hour: endtime.get('hour'),
                    minute: endtime.get('minute'),
                });
            }
            this.initialize((this.parent.type === 'clone' || this.parent.model.proposalId) ? this.data : null, start, end);
            if (start && start.isValid()) {
                this.calendar.fullCalendar('gotoDate', start);
            }
            else {
                this.calendar.fullCalendar('gotoDate', moment());
            }
        }
        else {
            if (clearItemD) {
                if (this.parent.type === 'clone') {
                    this.toggleScheduleAfterDeleteAction(this.parent.model.proposalId ? true : false, this.initialized);
                }
                else {
                    this.toggleScheduleAfterDeleteAction(this.sheduleCleared, this.hasEvents());
                    this.sheduleCleared = false;
                }
            }
            this.initialize(null, this.start, this.end);
        }
        $('#daily-midnight').mask('00:00');
    };
    NsdSchedulerDailyComponent.prototype.toggleScheduleAfterDeleteAction = function (enforce, initialized) {
        if (enforce) {
            this.slideToggle();
            this.initialized = initialized;
        }
    };
    NsdSchedulerDailyComponent.prototype.showSchedule = function (clearItemD) {
        this.clearSchedule(clearItemD);
        if (!this.displayStartDate) {
            this.displayStartDate = moment();
        }
        if (this.start) {
            this.start = moment.isMoment(this.start) ? this.start : moment(this.start);
            this.end = moment.isMoment(this.end) ? this.end : moment(this.end);
            $('#d-start').bootstrapDatepicker('update', this.start.toDate());
        }
        if (this.end) {
            $('#d-end').bootstrapDatepicker('update', this.end.toDate());
        }
    };
    NsdSchedulerDailyComponent.prototype.zoomIn = function () {
        if (this.slootDuration > 0) {
            this.slootDuration -= 1;
            var events = null;
            if (!this.data) {
                events = this.calendar.fullCalendar('clientEvents');
            }
            this.calendar.fullCalendar('destroy');
            this.initialize(this.data, this.starttime, this.endtime, this.slootDuration, events);
        }
    };
    NsdSchedulerDailyComponent.prototype.zoomOut = function () {
        if (this.slootDuration < 15) {
            this.slootDuration += 1;
            var events = null;
            if (!this.data) {
                events = this.calendar.fullCalendar('clientEvents');
            }
            this.calendar.fullCalendar('destroy');
            this.initialize(this.data, this.starttime, this.endtime, this.slootDuration, events);
        }
    };
    NsdSchedulerDailyComponent.prototype.hasEvents = function () {
        return this.dateEvents.length > 0;
    };
    NsdSchedulerDailyComponent.prototype.isClearButtonDisabled = function () {
        if (this.parent && this.parent.isReadOnly) {
            return true;
        }
        if (this.hasEvents()) {
            return false;
        }
        if (this.calendar) {
            var events = this.calendar.fullCalendar('clientEvents');
            return events.length <= 0;
        }
        return true;
    };
    NsdSchedulerDailyComponent.prototype.checkboxChanged = function (control, e) {
        switch (control) {
            case 's_sunrise':
                this.eventDetails.s_sunrise = !this.eventDetails.s_sunrise;
                this.eventDetails.s_sunset = false;
                if (this.eventDetails.s_sunrise) {
                    this.eventDetails.e_sunrise = false;
                    this.eventDetails.starttime = this.SR;
                    if (!this.eventDetails.e_sunrise && !this.eventDetails.e_sunset) {
                        if (this.eventDetails.endtime) {
                            var hours = this.eventDetails.endtime.substring(0, 2);
                            if (parseInt(hours) <= 6) {
                                this.eventDetails.endtime = "07:00";
                            }
                        }
                        else {
                            this.eventDetails.endtime = "07:00";
                        }
                    }
                }
                break;
            case 's_sunset':
                this.eventDetails.s_sunset = !this.eventDetails.s_sunset;
                this.eventDetails.s_sunrise = false;
                if (this.eventDetails.s_sunset) {
                    this.eventDetails.e_sunrise = false;
                    this.eventDetails.e_sunset = false;
                    this.eventDetails.starttime = this.SS;
                    if (!this.eventDetails.e_sunrise && !this.eventDetails.e_sunset) {
                        if (this.eventDetails.endtime) {
                            var hours = this.eventDetails.endtime.substring(0, 2);
                            if (parseInt(hours) <= 18) {
                                this.eventDetails.endtime = "22:00";
                            }
                        }
                        else {
                            this.eventDetails.endtime = "22:00";
                        }
                    }
                }
                break;
            case 'e_sunrise':
                this.eventDetails.e_sunrise = !this.eventDetails.e_sunrise;
                this.eventDetails.e_sunset = false;
                if (this.eventDetails.e_sunrise) {
                    this.eventDetails.s_sunrise = false;
                    this.eventDetails.s_sunset = false;
                    this.eventDetails.extendedHours = false;
                    this.eventDetails.exStarttime = "";
                    this.eventDetails.exEndtime = "";
                    this.eventDetails.endtime = this.SR;
                    if (!this.eventDetails.s_sunrise && !this.eventDetails.s_sunset) {
                        if (this.eventDetails.starttime) {
                            var hours = this.eventDetails.starttime.substring(0, 2);
                            if (parseInt(hours) >= 6) {
                                this.eventDetails.starttime = "04:00";
                            }
                        }
                        else {
                            this.eventDetails.starttime = "04:00";
                        }
                    }
                }
                break;
            case 'e_sunset':
                this.eventDetails.e_sunset = !this.eventDetails.e_sunset;
                this.eventDetails.e_sunrise = false;
                if (this.eventDetails.e_sunset) {
                    this.eventDetails.extendedHours = false;
                    this.eventDetails.exStarttime = "";
                    this.eventDetails.exEndtime = "";
                    this.eventDetails.s_sunset = false;
                    this.eventDetails.endtime = this.SS;
                    if (!this.eventDetails.s_sunrise && !this.eventDetails.s_sunset) {
                        if (this.eventDetails.starttime) {
                            var hours = this.eventDetails.starttime.substring(0, 2);
                            if (parseInt(hours) >= 18) {
                                this.eventDetails.starttime = "13:00";
                            }
                        }
                        else {
                            this.eventDetails.starttime = "13:00";
                        }
                    }
                }
                break;
        }
    };
    NsdSchedulerDailyComponent.prototype.isValid = function () {
        if (this.parent && this.parent.isReadOnly) {
        }
        var hasStart = $('#d-start').bootstrapDatepicker('getDate') != null;
        var hasEnd = $('#d-end').bootstrapDatepicker('getDate') != null;
        var hasEvents = false;
        if (this.calendar) {
            var events = this.getCalendarEvents();
            hasEvents = events.length > 0;
        }
        return hasStart && hasEnd && hasEvents;
    };
    NsdSchedulerDailyComponent.prototype.isStartHasValue = function () {
        return $('#d-start').bootstrapDatepicker('getDate') != null;
    };
    NsdSchedulerDailyComponent.prototype.isStartValidDate = function () {
        if (this.isStartHasValue()) {
            var start = moment($('#d-start').bootstrapDatepicker('getDate')).format("YYYY/MM/DD");
            var now = moment().format("YYYY/MM/DD");
            return moment(start).isSameOrAfter(now);
        }
        return true;
    };
    NsdSchedulerDailyComponent.prototype.isEndHaveValue = function () {
        return $('#d-end').bootstrapDatepicker('getDate') != null;
    };
    NsdSchedulerDailyComponent.prototype.isEndValidDate = function () {
        var start = moment($('#d-start').bootstrapDatepicker('getDate'));
        var end = moment($('#d-end').bootstrapDatepicker('getDate'));
        if (start.isValid() && end.isValid()) {
            return (start <= end);
        }
        return true;
    };
    NsdSchedulerDailyComponent.prototype.parseEvents = function (toggle) {
        if (toggle === void 0) { toggle = false; }
        this.dateEvents = [];
        var events = this.calendar.fullCalendar('clientEvents');
        events = events.sort(this.sortEvents);
        if (this.data && this.data.length > 0) {
            var proposalId = this.data[0].proposalId;
            this.resetDataEvents(proposalId, events);
        }
        var details = this.getDailyEvent(events);
        var itemD = "";
        for (var k = 0; k < details.length; k++) {
            this.updateDataEvent(details[k]);
            if (details[k].extendedHours && details[k].startTime === "00:00") {
                continue;
            }
            var row = {
                start: details[k].startTime,
                end: details[k].extendedHours ? details[k].extEndTime : details[k].endTime,
            };
            itemD = itemD + (row.start.replace(':', '') + "-" + row.end.replace(':', '') + " ");
            if (details[k].extendedHours) {
                row.end = row.end + ' [next day]';
            }
            this.dateEvents.push(row);
            //let rowStart = (details[k].startTime === "SR" ? this.SR : (details[k].startTime === "SS" ? this.SS : details[k].startTime))
            //let rowEnd = (details[k].endTime === "SR" ? this.SR : (details[k].endTime === "SS" ? this.SS : (details[k].extendedHours ? details[k].extEndTime : details[k].endTime)));
            //let _start = moment(this.start);
            //let _end = moment(this.end);
            //let momentRowStart = moment.utc(`${_start.year()}/${_start.month()}/${_start.day()} ${rowStart}`, 'yyyy/mm/dd HH:mm');
            //let momentRowEnd = moment.utc(`${_end.year()}/${_end.month()}/${_end.day()} ${rowEnd}`, 'yyyy/mm/dd HH:mm');
            //let row = {
            //    start: momentRowStart.format('HH:mm'),
            //    end: momentRowEnd.format('HH:mm')
            //}
            //itemD = itemD + `${row.start.replace(':', '')}-${row.end.replace(':', '')} `;
            //if (details[k].extendedHours) {
            //    row.end = row.end + ' [next day]'
            //}
            //this.dateEvents.push(row);
        }
        itemD = 'DAILY ' + itemD.trim();
        this.parent.hasScheduler = true;
        if (this.start && this.end) {
            var startDate = moment(this.start);
            var endDate = moment(this.end);
            if (details[details.length - 1].extendedHours) {
                //endDate = endDate.add("days", 1);
                this.end = moment(endDate);
            }
            var sDay = startDate.format('DD');
            var sMonth = startDate.format('MM');
            var sYear = startDate.format('YYYY');
            this.starttime = details[0].startTime;
            var sTime = (this.starttime === "SR"
                ? this.getSunriseTime(details[0].start)
                : (this.starttime === "SS" ? this.getSunsetTime(details[0].start) : this.starttime));
            var eDay = endDate.format('DD');
            var eMonth = endDate.format('MM');
            var eYear = endDate.format('YYYY');
            this.endtime = details[details.length - 1].extendedHours ? details[details.length - 1].exEndtime : details[details.length - 1].endTime;
            var eTime = details[details.length - 1].extendedHours
                ? details[details.length - 1].exEndtime
                : (this.endtime === "SR" ? this.getSunriseTime(details[details.length - 1].end) : (this.endtime === "SS" ? this.getSunsetTime(details[details.length - 1].end) : this.endtime));
            ;
            var start = moment.utc(sDay + "-" + sMonth + "-" + sYear + " " + sTime, 'DD-MM-YYYY HH:mm');
            var end = moment.utc(eDay + "-" + eMonth + "-" + eYear + " " + eTime, 'DD-MM-YYYY HH:mm');
            this.parent.disableActivePeridDates(start, end, itemD);
        }
        if (toggle) {
            this.slideToggle();
        }
    };
    NsdSchedulerDailyComponent.prototype.slideScheduleClose = function () {
        this.slideToggle();
        this.initialized = true;
    };
    NsdSchedulerDailyComponent.prototype.getCalendarEvents = function () {
        return this.calendar.fullCalendar('clientEvents');
    };
    NsdSchedulerDailyComponent.prototype.isUpdateButtonDisabled = function () {
        if (this.parent && this.parent.isReadOnly) {
            return true;
        }
        var _start = moment(this.eventDetails.starttime, "HH:mm");
        var _end = moment(this.eventDetails.endtime, "HH:mm");
        if (!_start.isValid() || !_end.isValid())
            return true;
        return false;
    };
    NsdSchedulerDailyComponent.prototype.getSunriseTime = function (date) {
        var time = moment(date).format('HH:mm');
        return time !== this.SR ? time : this.SR;
    };
    NsdSchedulerDailyComponent.prototype.getSunsetTime = function (date) {
        var time = moment(date).format('HH:mm');
        return time !== this.SS ? time : this.SS;
    };
    NsdSchedulerDailyComponent.prototype.sortEvents = function (event1, event2) {
        var start1 = new Date(event1.start).getTime();
        var start2 = new Date(event2.start).getTime();
        return start1 > start2 ? 1 : -1;
    };
    ;
    NsdSchedulerDailyComponent.prototype.dateChanged = function (ev) {
        switch (ev.currentTarget.id) {
            case "d-start":
                this.start = moment(new Date(ev.currentTarget.value));
                this.calendar.fullCalendar('removeEvents');
                this.calendar.fullCalendar('gotoDate', this.start);
                if (this.end && this.end.isValid()) {
                    var el = $('#d-start');
                    if (this.start.isAfter(this.end)) {
                        this.start = null;
                        var x = el.bootstrapDatepicker('getDate');
                        el.bootstrapDatepicker('update', '');
                        el.css({
                            "border-color": "red",
                            "border-width": "1px",
                            "border-style": "solid"
                        });
                    }
                    else {
                        el.css({
                            "border-color": "white",
                            "border-width": "0px",
                        });
                    }
                }
                break;
            case "d-end":
                this.end = moment(new Date(ev.currentTarget.value));
                if (this.start && this.start.isValid()) {
                    var el = $('#d-end');
                    if (this.start.isAfter(this.end)) {
                        this.end = null;
                        el.bootstrapDatepicker('update', '');
                        el.css({
                            "border-color": "red",
                            "border-width": "1px",
                            "border-style": "solid"
                        });
                    }
                    else {
                        el.css({
                            "border-color": "white",
                            "border-width": "0px",
                        });
                    }
                }
                break;
        }
    };
    NsdSchedulerDailyComponent.prototype.getDailyEvent = function (dailyEvents) {
        var events = [];
        for (var i = 0; i < dailyEvents.length; i++) {
            var startEvent = dailyEvents[i].start;
            var endEvent = dailyEvents[i].end;
            var startHour = startEvent.format('HH');
            var startMinute = startEvent.format('mm');
            var endHour = endEvent.format('HH');
            var endMinute = endEvent.format('mm');
            var start = null;
            var end = null;
            if (dailyEvents[i].s_sunrise) {
                start = "SR";
            }
            else if (dailyEvents[i].s_sunset) {
                start = "SS";
            }
            if (dailyEvents[i].e_sunrise) {
                end = "SR";
            }
            else if (dailyEvents[i].e_sunset) {
                end = "SS";
            }
            start = start ? start : moment(startHour + ":" + startMinute, 'HH:mm').format('HH:mm');
            end = end ? end : moment(endHour + ":" + endMinute, 'HH:mm').format('HH:mm');
            events.push({
                id: dailyEvents[i]._id || dailyEvents[i].id,
                startTime: start,
                endTime: end,
                start: dailyEvents[i].start,
                end: dailyEvents[i].end,
                s_sunrise: dailyEvents[i].s_sunrise,
                e_sunrise: dailyEvents[i].e_sunrise,
                s_sunset: dailyEvents[i].s_sunset,
                e_sunset: dailyEvents[i].e_sunset,
                extendedHours: dailyEvents[i].extendedHours,
                extEndTime: dailyEvents[i].exEndtime,
                exStarttime: dailyEvents[i].exStarttime,
                exEndtime: dailyEvents[i].exEndtime,
            });
        }
        return events.length > 0 ? this.removeDuplicates(events) : events;
    };
    NsdSchedulerDailyComponent.prototype.removeDuplicates = function (myArr) {
        var props = Object.keys(myArr[0]);
        return myArr.filter(function (item, index, self) {
            return index === self.findIndex(function (t) { return (props.every(function (prop) {
                return t[prop] === item[prop];
            })); });
        });
    };
    NsdSchedulerDailyComponent.prototype.initCalendar = function (calendar, currentDate) {
        var self = this;
        var duration = "00:" + this.windowRef.zeroPad(this.slootDuration, 2) + ":00";
        calendar.fullCalendar({
            defaultDate: moment(currentDate),
            defaultView: 'agendaDay',
            contentHeight: 820,
            slotDuration: duration,
            allDaySlot: false,
            selectable: true,
            editable: true,
            displayEventTime: false,
            droppable: true,
            eventLimit: true,
            fixedWeekCount: false,
            eventOverlap: false,
            //scrollTime: firstEventTime,
            columnFormat: {
                day: false,
            },
            header: false,
            axisFormat: 'HH:mm',
            timeFormat: 'HH:mm{ - HH:mm}',
            //maxTime: "23:59:00",
            nextDayThreshold: '00:00',
            minTime: 0,
            scrollTime: this.scrollTime,
            eventDragStart: function (event, jsEvent, ui, view) {
                self.startEventUpdate = {
                    start: event.start,
                    end: event.end
                };
            },
            eventDragStop: function (event, jsEvent, ui, view) {
            },
            viewRender: function (view, element) {
                element.find('.fc-day-header').html('');
            },
            eventDrop: function (event, delta, revertFunc, jsEvent, ui, view) {
                self.startEventUpdate = null;
                var isSameDay = event.start.isSame(event.end, 'day');
                if (!isSameDay) { // crossed midnight
                    revertFunc();
                }
            },
            eventResizeStart: function (event, jsEvent, ui, view) {
                self.startEventUpdate = {
                    start: event.start,
                    end: event.end
                };
            },
            select: function (start, end, event, view) {
                var eventData = {
                    start: start,
                    end: end,
                    s_sunrise: false,
                    s_sunset: false,
                    e_sunrise: false,
                    e_sunset: false,
                    allDay: false,
                    extendedHours: false,
                    exStarttime: '',
                    exEndtime: ''
                };
                if (!self.parent.isReadOnly && !self.isOverlapping({ start: start, end: end }) && self.isEventTimeAllowed(eventData)) {
                    self.calendar.fullCalendar('renderEvent', eventData, true);
                    self.parent.hasScheduler = true;
                }
                self.calendar.fullCalendar('unselect');
            },
            eventResizeStop: function (event, jsEvent, ui, view) {
            },
            eventResize: function (event, delta, revertFunc, jsEvent, ui, view) {
                self.startEventUpdate = null; // ??
                if (!self.isEventTimeAllowed(event)) {
                    revertFunc();
                }
            },
            eventRender: function (event, element, view) {
                if (self.parent && self.parent.isReadOnly) {
                    event.editable = false;
                }
                var time = event.sunrise ? "SUNRISE - " : '{ ' + event.start.format('HH:mm') + ' - ';
                if (event.sunset) {
                    time = time + 'SUNSET';
                }
                else {
                    time = time + event.end.format('HH:mm') + ' }';
                }
                element.find('.fc-content').prepend('<span><i class="fa fa-clock-o m-r-5"></i>' + time + '</span>');
                element.css('background-color', event.eventColor);
                element.css('border-color', event.eventColor);
            },
            eventAfterRender: function (event, element, view) {
                if (event.extendedHours) {
                    var title = element.text() + " - { " + event.exStarttime + " - " + event.exEndtime + " }";
                    element.html(title);
                }
            },
            eventClick: function (event, jsEvent, view) {
                var el = $('#calendar-daily-modal');
                self.eventDetails.starttime = event.start.format('HH:mm');
                self.eventDetails.endtime = event.end.format('HH:mm');
                $('.time').mask('00:00');
                el.modal();
                self.eventDetails.id = event.id;
                self.eventDetails.s_sunrise = event.s_sunrise;
                self.eventDetails.s_sunset = event.s_sunset;
                self.eventDetails.e_sunrise = event.e_sunrise;
                self.eventDetails.e_sunset = event.e_sunset;
                self.eventDetails.extendedHours = event.extendedHours;
                self.eventDetails.exStarttime = event.exStarttime;
                self.eventDetails.exEndtime = event.exEndtime;
                self.hasExtendedHours = event.extendedHours;
                self.editingExtendedHours = event.extendedHours;
                el.on('shown.bs.modal', function (e) {
                    self.cdRef.detectChanges();
                });
                el.on('hidden.bs.modal', function () {
                    $(this).data('bs.modal', null);
                });
                el.on('hide.bs.modal', function (e) {
                    var pressedButton = $(document.activeElement).attr('id');
                    if (pressedButton === "daily_btn-modal-yes") {
                        var _start = moment(self.eventDetails.starttime, "HH:mm").format("HH:mm");
                        var _end = moment(self.eventDetails.endtime, "HH:mm").format("HH:mm");
                        if (_start.length === 5 && _end.length === 5) {
                            var eventStartTime = event.start.format('HH:mm');
                            var eventEndTime = event.end.format('HH:mm');
                            var events = calendar.fullCalendar('clientEvents');
                            var count = 0;
                            var lastParentId = events[0].parentId;
                            for (var i = 0; i < events.length; i++) {
                                var startTime = events[i].start.format('HH:mm');
                                var endTime = events[i].end.format('HH:mm');
                                if ((events[i].parentId === event.parentId || events[i].groupId === event.groupId) && eventStartTime === startTime && eventEndTime == endTime) {
                                    if (events[i].parentId !== lastParentId) {
                                        lastParentId = events[i].parentId;
                                        count = 0;
                                    }
                                    count += 1;
                                    var start = events[i].start.clone();
                                    var end = events[i].end.clone();
                                    events[i].start = events[i].start.set({
                                        hour: parseInt(_start.substring(0, 2)),
                                        minute: parseInt(_start.substring(3))
                                    });
                                    events[i].end = events[i].end.set({
                                        hour: parseInt(_end.substring(0, 2)),
                                        minute: parseInt(_end.substring(3))
                                    });
                                    if (self.isOverlapping(events[i])) {
                                        events[i].start = start;
                                        events[i].end = end;
                                    }
                                    else {
                                        events[i].s_sunrise = self.eventDetails.s_sunrise;
                                        events[i].s_sunset = self.eventDetails.s_sunset;
                                        events[i].e_sunrise = self.eventDetails.e_sunrise;
                                        events[i].e_sunset = self.eventDetails.e_sunset;
                                        events[i].extendedHours = self.eventDetails.extendedHours;
                                        events[i].exStarttime = self.eventDetails.exStarttime;
                                        events[i].exEndtime = self.eventDetails.exEndtime;
                                        //let eventColor = events[i].eventColor;
                                        if (self.eventDetails.s_sunrise || self.eventDetails.s_sunset || self.eventDetails.e_sunrise || self.eventDetails.e_sunset) {
                                            events[i].previousColor = events[i].eventColor;
                                            events[i].eventColor = '#2b3c4e';
                                        }
                                        else {
                                            if (event.eventColor === '#2b3c4e') {
                                                events[i].eventColor = events[i].previousColor;
                                                events[i].previousColor = null;
                                            }
                                        }
                                        calendar.fullCalendar('updateEvent', events[i]);
                                    }
                                }
                            }
                        }
                    }
                    else {
                        if (pressedButton === "daily_btn-modal-delete") {
                            calendar.fullCalendar('removeEvents', event._id);
                            calendar.fullCalendar('renderEvents');
                        }
                    }
                    self.eventDetails.s_sunrise = false;
                    self.eventDetails.s_sunset = false;
                    $('#calendar-daily-modal').off('hide.bs.modal');
                });
            },
        });
    };
    NsdSchedulerDailyComponent.prototype.updateDataEvent = function (event) {
        if (!this.data) {
            return;
        }
        ;
        for (var i = 0; i < this.data.length; i++) {
            var obj = JSON.parse(this.data[i].event);
            var id = event._id || event.id;
            if (obj.id !== id) {
                continue;
            }
            ;
            obj.start = event.start;
            obj.end = event.end;
            obj.s_sunset = event.s_sunset;
            obj.s_sunrise = event.s_sunrise;
            obj.e_sunset = event.e_sunset;
            obj.e_sunrise = event.e_sunrise;
            obj.extendedHours = event.extendedHours;
            obj.exStarttime = event.exStarttime;
            obj.exEndtime = event.exEndtime;
            this.data[i].event = JSON.stringify(obj);
        }
    };
    NsdSchedulerDailyComponent.prototype.resetDataEvents = function (proposalId, cEvents) {
        this.data = [];
        for (var i = 0; i < cEvents.length; i++) {
            var cEvent = cEvents[i];
            var event_2 = {
                id: cEvent._id || cEvent.id,
                start: cEvent.start,
                end: cEvent.end,
                linked: cEvent.linked,
                groupId: cEvent.groupId,
                eventColor: cEvent.eventColor,
                previousColor: cEvent.previousColor,
                parentId: cEvent.parentId,
                s_sunset: cEvent.s_sunset,
                s_sunrise: cEvent.s_sunrise,
                e_sunset: cEvent.e_sunset,
                e_sunrise: cEvent.e_sunrise,
                exStarttime: cEvent.exStarttime,
                exEndtime: cEvent.exEndtime,
                extendedHours: cEvent.extendedHours
            };
            this.data.push({
                calendarType: 1,
                proposalId: proposalId,
                calendarId: -1,
                eventId: event_2.id,
                itemD: this.model.itemD,
                event: JSON.stringify(event_2)
            });
        }
    };
    NsdSchedulerDailyComponent.prototype.isOverlapping = function (event) {
        var array = this.calendar.fullCalendar('clientEvents');
        for (var i in array) {
            if (array[i]._id != event._id) {
                if (!(array[i].start >= event.end || array[i].end <= event.start)) {
                    return true;
                }
            }
        }
        return false;
    };
    NsdSchedulerDailyComponent.prototype.onExtendedHours = function () {
        this.eventDetails.extendedHours = !this.eventDetails.extendedHours;
        if (!this.eventDetails.extendedHours) {
            this.eventDetails.exStarttime = "";
            this.eventDetails.exEndtime = "";
        }
        else {
            this.eventDetails.endtime = "23:59";
            this.eventDetails.e_sunrise = false;
            this.eventDetails.e_sunset = false;
            this.eventDetails.exStarttime = "00:00";
        }
    };
    NsdSchedulerDailyComponent.prototype.IsValidDate = function (selectedDate, calendarDate) {
        var dayOfMonth = parseInt(selectedDate.format('D'));
        return selectedDate.format('M') === calendarDate.format('M') && dayOfMonth >= parseInt(moment().format('D'));
    };
    NsdSchedulerDailyComponent.prototype.findEventById = function (events, eventId) {
        for (var i = 0; i < events.length; i++) {
            if (events[i].id == eventId) {
                return events[i].id;
            }
        }
        return null;
    };
    NsdSchedulerDailyComponent.prototype.isEventTimeAllowed = function (event) {
        //let result = event.start._d.getDay() === event.end._d.getDay();
        var minTime = moment('00:00', 'HH: mm');
        var maxTime = moment('00:05', 'HH: mm');
        var invalidTime = moment(event.start.format('HH:mm'), 'HH:mm').isBefore(minTime) || moment(event.end.format('HH:mm'), 'HH:mm').isSameOrBefore(maxTime);
        return !invalidTime;
    };
    NsdSchedulerDailyComponent.prototype.setEvent = function (obj) {
        var event = {
            id: obj.id,
            start: moment.utc(obj.start),
            end: moment.utc(obj.end),
            previousColor: obj.previousColor,
            s_sunset: obj.s_sunset,
            s_sunrise: obj.s_sunrise,
            e_sunset: obj.e_sunset,
            e_sunrise: obj.e_sunrise,
            extendedHours: obj.extendedHours,
            exStarttime: obj.exStarttime,
            exEndtime: obj.exEndtime,
            allDay: false,
            stick: true
        };
        return event;
    };
    __decorate([
        core_1.Input('form'),
        __metadata("design:type", forms_1.FormGroup)
    ], NsdSchedulerDailyComponent.prototype, "form", void 0);
    __decorate([
        core_1.Input('model'),
        __metadata("design:type", Object)
    ], NsdSchedulerDailyComponent.prototype, "model", void 0);
    NsdSchedulerDailyComponent = __decorate([
        core_1.Component({
            selector: 'scheduler-daily',
            templateUrl: '/app/dashboard/nsds/nsd-scheduler-daily.component.html'
        }),
        __metadata("design:paramtypes", [windowRef_service_1.WindowRef, core_1.ChangeDetectorRef])
    ], NsdSchedulerDailyComponent);
    return NsdSchedulerDailyComponent;
}());
exports.NsdSchedulerDailyComponent = NsdSchedulerDailyComponent;
//# sourceMappingURL=nsd-scheduler-daily.component.js.map
﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Subject, Observable } from 'rxjs/Rx';

import { WindowRef } from '../../../common/windowRef.service'


export interface AreaDefinitionModel {
    pointAndRadiusEntry: boolean,
    bufferedLineEntry: boolean,
    polygonEntry: boolean,
    buffer: string,
    points: string[],
    radius: string,
}

@Injectable()
export class AreaDefinitionNsdService {
    public app: any;

    constructor(private http: Http, winRef: WindowRef) {
        //this.app = winRef.nativeWindow['app'];
    }

    calculate(areaDefinitionModel: AreaDefinitionModel): any {

        throw new Error("Method not implemented.");
    }

    private handleError(error: Response) {
        return Observable.throw(error.statusText);
    }
}


﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace RSC.WebService.Helpers
{
    public static class SchemaExtensions
    {
        /// <summary>
        /// Converts a RSCProposalType into a RSCProposal (from the RSC.Model module).
        /// </summary>
        public static Model.RSCProposal ToModel(this Schemas.RSCProposalType proposal)
        {
            var p = new Model.RSCProposal
            {
                Version = proposal.version.XmlEnumToString(),
                Origin = proposal.origin,
                Created = proposal.created,
                Aerodrome = proposal.Aerodrome,
                Runways = MapMany<Schemas.Runway, Model.Runway>(proposal.Items, r => r.ToModel())
            };

            SchemaFieldAssigner<Schemas.ItemsChoiceType2>.For(proposal.ItemsElementName, proposal.Items)
                .On(Schemas.ItemsChoiceType2.Taxiway, v => p.Taxiways = (v as Schemas.FreeformText).ToModel())
                .On(Schemas.ItemsChoiceType2.Apron, v => p.Aprons = (v as Schemas.FreeformText).ToModel())
                .On(Schemas.ItemsChoiceType2.CancelCurrentRSC, v => p.CancelCurrentRSC = (bool)v)
                .On(Schemas.ItemsChoiceType2.ConditionsChangingRapidly, v => p.ConditionsChangingRapidly = (v as Schemas.Telephone).ToModel())
                .On(Schemas.ItemsChoiceType2.GeneralRemarks, v => p.GeneralRemarks = (v as Schemas.FreeformText).ToModel())
                .On(Schemas.ItemsChoiceType2.NextPlannedObs, v => p.NextPlannedObs = (v as Schemas.DateTimeUTC).ToModel(10101, "Invalid Next Planned Observation Time"))
                .Apply();

            return p;
        }

        public static Model.Runway ToModel(this Schemas.Runway runway)
        {
            var rwy = new Model.Runway(runway.RunwayId);

            SchemaFieldAssigner<Schemas.ItemsChoiceType1>.For(runway.ItemsElementName, runway.Items)
                .On(Schemas.ItemsChoiceType1.CentredClearedWidth, v => rwy.CentredClearedWidth = int.Parse(v.ToString()))
                .On(Schemas.ItemsChoiceType1.ClearedFull, v => rwy.ClearedFull = (bool)v)
                .On(Schemas.ItemsChoiceType1.ClearedPortion, v => rwy.ClearedPortion = (v as Schemas.RunwayClearedPortion).ToModel())
                .On(Schemas.ItemsChoiceType1.NoWinterMaintenance, v => rwy.NoWinterMaintenance = (bool)v)
                .On(Schemas.ItemsChoiceType1.ObsTime, v => rwy.ObsTimeUTC = (v as Schemas.DateTimeUTC).ToModel(10301, "Invalid Runway Observation Time"))
                .On(Schemas.ItemsChoiceType1.RemainingWidth, v => rwy.RemainingWidth = (v as Schemas.RemainingWidth).ToModel())
                .On(Schemas.ItemsChoiceType1.Snowbanks, v => rwy.Snowbanks = (v as Schemas.SnowbanksRunway).ToModel())
                .Apply();

            rwy.ClearedPortionOffsets = MapMany<Schemas.ClearedPortionOffset, Model.ClearedPortionOffset>(runway.Items, p => p.ToModel());

            return rwy;
        }

        public static Model.Telephone ToModel(this Schemas.Telephone tel)
        {
            return new Model.Telephone()
            {
                AreaCode = tel.TelArea,
                Number = $"{tel.Telddd}{tel.Teldddd}",
                Extension = tel.TelExt
            };
        }

        public static Model.FreeformText ToModel(this Schemas.FreeformText text)
        {
            return new Model.FreeformText()
            {
                EnglishText = text.EnglishText,
                FrenchText = text.FrenchText
            };
        }

        public static DateTime ToModel(this Schemas.DateTimeUTC utc, int errorCode, string errorMessage)
        {
            var strDate = $"{utc.UTC_Year}/{utc.UTC_Month}/{utc.UTC_Day} {utc.UTC_Time}";
            try
            {
                return DateTime.ParseExact(strDate, "yyyy/MM/dd HHmm", CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                throw new RSCException(errorCode, $"{errorMessage}. Reason: {ex.Message}. {strDate}.");
            }
        }

        public static Model.RunwayClearedPortion ToModel(this Schemas.RunwayClearedPortion portion)
        {
            return new Model.RunwayClearedPortion()
            {
                Average = portion.Items.MapOne<Schemas.ClearedSurface, Model.ClearedSurface>(v => v.ToModel()),
                RunwayThirds = portion.Items.MapMany<Schemas.ClearedSurfaceThird, Model.ClearedSurfaceThird>(v => v.ToModel()),
                Treatments = portion.Treatment?.Map(),
                ContaminantComment = portion.ContaminantComment?.ToModel(),
                OtherConditions = portion.OtherConditions?.ToModel(),
                AverageRwyFriction = portion.AverageRwyFriction?.ToModel()
            };
        }

        public static Model.ClearedSurface ToModel(this Schemas.ClearedSurface cs)
        {
            return new Model.ClearedSurface()
            {
                Contaminants = cs.Contaminant.MapMany<Schemas.ContaminantIns, Model.ContaminantIns>(c => c.ToModel()),
                RwyCC = cs.RwyCC
            };
        }

        public static Model.ClearedSurfaceThird ToModel(this Schemas.ClearedSurfaceThird cst)
        {
            return new Model.ClearedSurfaceThird()
            {
                Portion = int.Parse(cst.Portion),
                Contaminants = cst.Contaminant.MapMany<Schemas.ContaminantIns, Model.ContaminantIns>(c => c.ToModel()),
                RwyFrictionThird = cst.RwyFrictionThird?.ToModel(),
                RwyCC = cst.RwyCC
            };
        }

        public static Model.RwyFriction ToModel(this Schemas.RwyFriction schemaRF)
        {
            var modelRF = new Model.RwyFriction();
            var coeficients = new List<string>();

            SchemaFieldAssigner<Schemas.ItemsChoiceType>.For(schemaRF.ItemsElementName, schemaRF.Items)
                .On(Schemas.ItemsChoiceType.Device, v => modelRF.Device = ParseEnum<Model.Enums.MeasurementDeviceEnum>(v.ToString()))
                .On(Schemas.ItemsChoiceType.FrictionCoefficient, v => coeficients.Add((string)v))
                .On(Schemas.ItemsChoiceType.RFObsTime, v => modelRF.RFObsTime = (v as Schemas.DateTimeUTC).ToModel(10408, "Invalid Runway Friction Observation Time"))
                .On(Schemas.ItemsChoiceType.Temperature, v => modelRF.Temperature = (string)v)
                .On(Schemas.ItemsChoiceType.Unreliable, v => modelRF.Unreliable = (bool)v)
                .Apply();

            modelRF.FrictionCoefficients = int.Parse(coeficients.First()); //coeficients.Select(c => int.Parse(c)).ToArray();

            return modelRF;
        }

        public static Model.OtherConditions ToModel(this Schemas.OtherConditions oc)
        {
            return new Model.OtherConditions()
            {
                OtherConditArea = oc.OtherConditArea?.ToModel(),
                OtherConditEdge = oc.OtherConditEdge?.ToModel(),
                OtherConditionsComment = oc.OtherConditionsComment?.ToModel()
            };
        }

        public static Model.OtherConditArea ToModel(this Schemas.OtherConditionsOtherConditArea oca)
        {
            return new Model.OtherConditArea()
            {
                ConditTypeL = ParseEnum<Model.Enums.OtherConditionsAreaEnum>(oca.ConditTypeL),
                DistFromThreshold = int.Parse(oca.DistFromThreshold),
                ThresholdNum = int.Parse(oca.ThresholdNum)
            };
        }

        public static Model.OtherConditEdge ToModel(this Schemas.OtherConditionsOtherConditEdge schemaOCE)
        {
            return new Model.OtherConditEdge()
            {
                ConditTypeR = schemaOCE.ConditTypeR.MapEnum(),
                OCHeight = schemaOCE.OCHeight?.ToModel(),
                OCLocation = schemaOCE.OCLocation.MapEnum(),
                OCDistance = int.Parse(schemaOCE.OCDistance),
                OCDistanceUOM = schemaOCE.OCDistanceUOM.MapEnum(),
                OCSide = schemaOCE.OCSide?.Select(v => v.MapEnum()).ToArray(),
                AcrossRwy = schemaOCE.AcrossRwy
            };
        }

        public static Model.OCHeightType ToModel(this Schemas.OCHeightType schemaOCH)
        {
            if (schemaOCH.Item.GetType() == typeof(decimal))
            {
                // inches
                return new Model.OCHeightType((decimal)schemaOCH.Item, true);
            }
            else
            {
                // feet
                return new Model.OCHeightType(decimal.Parse(schemaOCH.Item.ToString()), false);
            }
        }

        public static Model.ContaminantIns ToModel(this Schemas.ContaminantIns c)
        {
            var contIns = new Model.ContaminantIns() { ContamCoverage = c.ContamCoverage };

            foreach (var v in c.Items)
            {
                if (v.GetType() == typeof(Schemas.ContamTypeType))
                    contIns.ContamType = ((Schemas.ContamTypeType)v).MapEnum();

                if (v.GetType() == typeof(Schemas.ContamNoDType))
                    contIns.ContamTypeNoD = ((Schemas.ContamNoDType)v).MapEnum();

                if (v is Schemas.ContamDepth)
                    contIns.ContamDepthI = (v as Schemas.ContamDepth).ToModel();
            }

            return contIns;
        }

        public static Model.ContamDepth ToModel(this Schemas.ContamDepth cd)
        {
            var contDep = new Model.ContamDepth();

            if (cd.Item.GetType() == typeof(decimal))
                contDep.ContamInches = (decimal)cd.Item;
            else
                contDep.ContamTrace = (bool)cd.Item;

            return contDep;
        }

        public static Model.ClearedPortionOffset ToModel(this Schemas.ClearedPortionOffset portion)
        {
            return new Model.ClearedPortionOffset()
            {
                OffsetFT = int.Parse(portion.OffsetFT),
                OffsetSide = ParseEnum<Model.Enums.OffsetSideEnum>(portion.OffsetSide)
            };
        }

        public static Model.RemainingWidth ToModel(this Schemas.RemainingWidth rw)
        {
            return new Model.RemainingWidth()
            {
                ContaminantFI = rw.ContaminantFI?.ToModel()
            };
        }

        public static Model.ContaminantFtIns ToModel(this Schemas.ContaminantFtIns c)
        {
            var contFiIns = new Model.ContaminantFtIns();
            foreach (var v in c.Items)
            {
                if (v.GetType() == typeof(Schemas.ContamDepthFtIn))
                    contFiIns.ContamDepthFI = (v as Schemas.ContamDepthFtIn).ToModel();

                if (v.GetType() == typeof(Schemas.ContamTypeType))
                    contFiIns.ContamType = ((Schemas.ContamTypeType)v).MapEnum();

                if (v.GetType() == typeof(Schemas.ContamNoDType))
                    contFiIns.ContamTypeNoDepth = ((Schemas.ContamNoDType)v).MapEnum();
            }

            return contFiIns;
        }

        public static Model.ContamDepthFtIn ToModel(this Schemas.ContamDepthFtIn cd)
        {
            var contFtIn = new Model.ContamDepthFtIn();

            if (cd.Item.GetType() == typeof(string))
                contFtIn.ContamFeet = int.Parse((string)cd.Item);

            if (cd.Item.GetType() == typeof(decimal))
                contFtIn.ContamInches = (decimal)cd.Item;

            if (cd.Item.GetType() == typeof(bool))
                contFtIn.ContamTrace = (bool)cd.Item;

            return contFtIn;
        }

        public static Model.SnowbanksRunway ToModel(this Schemas.SnowbanksRunway sb)
        {
            return new Model.SnowbanksRunway()
            {
                BankHeight = sb.BankHeight?.ToModel(),
                BankEdge = sb.BankEdge?.ToModel(),
                BankSides = sb.BankSide?.Select(s => (Model.Enums.CardinalSideEnum)Enum.Parse(typeof(Model.Enums.CardinalSideEnum), s)).ToArray()
            };
        }

        public static Model.BankEdge ToModel(this Schemas.BankEdge be)
        {
            var edge = new Model.BankEdge();

            switch (be.ItemElementName)
            {
                case Schemas.ItemChoiceType.BankEdgeIns:
                    edge.BankEdgeIns = int.Parse((string)be.Item);
                    break;

                case Schemas.ItemChoiceType.BankEdgeFt:
                    edge.BankEdgeFt = int.Parse((string)be.Item);
                    break;

                case Schemas.ItemChoiceType.OnRwyEdge:
                    edge.OnRwyEdge = (bool)be.Item;
                    break;
            }

            return edge;
        }

        public static Model.BankHeight ToModel(this Schemas.BankHeight bh)
        {
            return new Model.BankHeight()
            {
                BankHeightInch = !string.IsNullOrEmpty(bh.BankHeightInch) ? int.Parse(bh.BankHeightInch) : 0,
                BankHeightFt = !string.IsNullOrEmpty(bh.BankHeightFt) ? int.Parse(bh.BankHeightFt) : 0
            };
        }

        public static Model.Enums.TreatmentEnum[] Map(this Schemas.RunwayClearedPortionTreatment[] values) => values.Select(e => e.MapEnum()).ToArray();

        public static Model.Enums.TreatmentEnum MapEnum(this Schemas.RunwayClearedPortionTreatment value)
        {
            switch (value)
            {
                case Schemas.RunwayClearedPortionTreatment.Item100:
                    return Model.Enums.TreatmentEnum.LoseSand;

                case Schemas.RunwayClearedPortionTreatment.Item104:
                    return Model.Enums.TreatmentEnum.Graded;

                case Schemas.RunwayClearedPortionTreatment.Item106:
                    return Model.Enums.TreatmentEnum.Packed;

                case Schemas.RunwayClearedPortionTreatment.Item108:
                    return Model.Enums.TreatmentEnum.ChemicallyTreated;

                case Schemas.RunwayClearedPortionTreatment.Item109:
                    return Model.Enums.TreatmentEnum.Scarified;

                default:
                    throw new Exception($"Trying to map unknown RunwayClearedPortionTreatment value {value}.");
            }
        }

        public static Model.Enums.OtherConditionsEdgeEnum MapEnum(this Schemas.OtherConditionsOtherConditEdgeConditTypeR value)
        {
            switch (value)
            {
                case Schemas.OtherConditionsOtherConditEdgeConditTypeR.Item120:
                    return Model.Enums.OtherConditionsEdgeEnum.SnowDrifts;

                case Schemas.OtherConditionsOtherConditEdgeConditTypeR.Item121:
                    return Model.Enums.OtherConditionsEdgeEnum.Windrows;

                case Schemas.OtherConditionsOtherConditEdgeConditTypeR.Item125:
                    return Model.Enums.OtherConditionsEdgeEnum.SnowBanks;

                default:
                    throw new Exception($"Trying to map unknown OtherConditionsOtherConditEdgeConditTypeR value {value}.");
            }
        }

        public static Model.Enums.OCLocationEnum MapEnum(this Schemas.OtherConditionsOtherConditEdgeOCLocation value)
        {
            switch (value)
            {
                case Schemas.OtherConditionsOtherConditEdgeOCLocation.Item127:
                    return Model.Enums.OCLocationEnum.AlongInsideRedl;

                case Schemas.OtherConditionsOtherConditEdgeOCLocation.Item128:
                    return Model.Enums.OCLocationEnum.AlongInsideRwyEdge;

                case Schemas.OtherConditionsOtherConditEdgeOCLocation.Item129:
                    return Model.Enums.OCLocationEnum.AlongClearedWidth;

                case Schemas.OtherConditionsOtherConditEdgeOCLocation.Item134:
                    return Model.Enums.OCLocationEnum.FMCL;

                default:
                    throw new Exception($"Trying to map unknown OtherConditionsOtherConditEdgeOCLocation value {value}.");
            }
        }

        public static Model.Enums.UnitOfMeasurement MapEnum(this Schemas.OtherConditionsOtherConditEdgeOCDistanceUOM value)
        {
            switch (value)
            {
                case Schemas.OtherConditionsOtherConditEdgeOCDistanceUOM.Item9:
                    return Model.Enums.UnitOfMeasurement.Inches;

                case Schemas.OtherConditionsOtherConditEdgeOCDistanceUOM.Item10:
                    return Model.Enums.UnitOfMeasurement.Feet;

                default:
                    throw new Exception($"Trying to map unknown OtherConditionsOtherConditEdgeOCDistanceUOM value {value}.");
            }
        }

        public static Model.Enums.CardinalSideEnum MapEnum(this Schemas.OtherConditionsOtherConditEdgeOCSide value)
        {
            switch (value)
            {
                case Schemas.OtherConditionsOtherConditEdgeOCSide.Item250:
                    return Model.Enums.CardinalSideEnum.E;

                case Schemas.OtherConditionsOtherConditEdgeOCSide.Item251:
                    return Model.Enums.CardinalSideEnum.SE;

                case Schemas.OtherConditionsOtherConditEdgeOCSide.Item252:
                    return Model.Enums.CardinalSideEnum.S;

                case Schemas.OtherConditionsOtherConditEdgeOCSide.Item253:
                    return Model.Enums.CardinalSideEnum.SW;

                case Schemas.OtherConditionsOtherConditEdgeOCSide.Item254:
                    return Model.Enums.CardinalSideEnum.W;

                case Schemas.OtherConditionsOtherConditEdgeOCSide.Item255:
                    return Model.Enums.CardinalSideEnum.NW;

                case Schemas.OtherConditionsOtherConditEdgeOCSide.Item256:
                    return Model.Enums.CardinalSideEnum.N;

                case Schemas.OtherConditionsOtherConditEdgeOCSide.Item257:
                    return Model.Enums.CardinalSideEnum.NE;

                default:
                    throw new Exception($"Trying to map unknown OtherConditionsOtherConditEdgeOCSide value {value}.");
            }
        }

        public static Model.Enums.ContaminantWithDepthEnum MapEnum(this Schemas.ContamTypeType value)
        {
            switch (value)
            {
                case Schemas.ContamTypeType.Item60:
                    return Model.Enums.ContaminantWithDepthEnum.StandingWater;

                case Schemas.ContamTypeType.Item61:
                    return Model.Enums.ContaminantWithDepthEnum.DrySnow;

                case Schemas.ContamTypeType.Item62:
                    return Model.Enums.ContaminantWithDepthEnum.WetSnow;

                case Schemas.ContamTypeType.Item63:
                    return Model.Enums.ContaminantWithDepthEnum.Slush;

                case Schemas.ContamTypeType.Item65:
                    return Model.Enums.ContaminantWithDepthEnum.DrySnowOnTopOfIce;

                case Schemas.ContamTypeType.Item66:
                    return Model.Enums.ContaminantWithDepthEnum.WetSnowOnTopOfIce;

                case Schemas.ContamTypeType.Item67:
                    return Model.Enums.ContaminantWithDepthEnum.SlushOnTopOfIce;

                case Schemas.ContamTypeType.Item68:
                    return Model.Enums.ContaminantWithDepthEnum.DrySnowOnTopOfCompactedSnow;

                case Schemas.ContamTypeType.Item71:
                    return Model.Enums.ContaminantWithDepthEnum.WaterOnTopOfCompactedSnow;

                case Schemas.ContamTypeType.Item72:
                    return Model.Enums.ContaminantWithDepthEnum.WetSnowOnTopOfCompactedSnow;

                default:
                    throw new Exception($"Trying to map unknown ContamTypeType value {value}.");
            }
        }

        public static Model.Enums.ContaminantWithNoDepthEnum MapEnum(this Schemas.ContamNoDType value)
        {
            switch (value)
            {
                case Schemas.ContamNoDType.Item20:
                    return Model.Enums.ContaminantWithNoDepthEnum.Dry;

                case Schemas.ContamNoDType.Item22:
                    return Model.Enums.ContaminantWithNoDepthEnum.Wet;

                case Schemas.ContamNoDType.Item23:
                    return Model.Enums.ContaminantWithNoDepthEnum.Frost;

                case Schemas.ContamNoDType.Item24:
                    return Model.Enums.ContaminantWithNoDepthEnum.Ice;

                case Schemas.ContamNoDType.Item25:
                    return Model.Enums.ContaminantWithNoDepthEnum.CompactedSnow;

                case Schemas.ContamNoDType.Item27:
                    return Model.Enums.ContaminantWithNoDepthEnum.WetIce;

                case Schemas.ContamNoDType.Item29:
                    return Model.Enums.ContaminantWithNoDepthEnum.CompactedSnowGravelMix;

                case Schemas.ContamNoDType.Item32:
                    return Model.Enums.ContaminantWithNoDepthEnum.SlipperyWhenWet;

                default:
                    throw new Exception($"Trying to map unknown ContamNoDType value {value}.");
            }
        }

        public static M MapOne<T, M>(this object[] items, Func<T, M> map) where T : class where M : class
        {
            var one = items.FirstOrDefault(item => item is T) as T;
            return one != null ? map(one) : null;
        }

        public static M[] MapMany<T, M>(this object[] items, Func<T, M> map) where T : class where M : class
        {
            return items.Where(item => item is T).Select(item => map(item as T)).ToArray();
        }

        public static T ParseEnum<T>(string value) where T : struct, IConvertible
        {
            return (T)(object)int.Parse(value);
        }
    }
}
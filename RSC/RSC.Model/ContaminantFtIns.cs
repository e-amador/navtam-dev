﻿using RSC.Model.Enums;

namespace RSC.Model
{
    /// <summary>
    /// Contaminant identification, coverage and depth (if appropriate) in feet or inches
    /// </summary>
    public class ContaminantFtIns
    {
        /// <summary>
        /// Type of contamination to be found on this portion of the runway - that
        /// must be accompanied by a depth modifier.This element provides the ability to specify
        ///  differing layers of contamination.
        /// </summary>
        public ContaminantWithDepthEnum? ContamType { get; set; }
        /// <summary>
        /// Depth of contamination.
        /// </summary>
        public ContamDepthFtIn ContamDepthFI { get; set; }
        /// <summary>
        /// Type of contamination that does not have a Depth modifier to be found on this
        /// portion of the runway.This element provides the ability to specify differing layers of
        /// contamination.
        /// </summary>
        public ContaminantWithNoDepthEnum? ContamTypeNoDepth { get; set; }

        public bool HasDepth => ContamType.HasValue;
    }
}
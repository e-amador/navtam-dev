﻿namespace RSC.Model
{
    /// <summary>
    /// Height INS vs FT
    /// </summary>
    public class OCHeightType
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public OCHeightType(decimal value, bool inInches)
        {
            InInches = inInches;
            Value = value;
        }
        /// <summary>
        /// Height messured in inches
        /// </summary>
        public bool InInches { get; set; }
        /// <summary>
        /// Height messured in feet
        /// </summary>
        public bool InFeet => !InInches;
        /// <summary>
        /// Depth/height of Contamination in inches/feet.
        /// </summary>
        public decimal Value { get; set; }
    }
}
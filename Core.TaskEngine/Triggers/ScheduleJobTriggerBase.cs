﻿using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Data.NotamWiz.EF.Helpers;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.SqlServiceBroker;
using NavCanada.Core.TaskEngine.Contracts;
using NavCanada.Core.TaskEngine.Helpers;
using NavCanada.Core.TaskEngine.Implementations;
using Quartz;
using System;
using System.Diagnostics;

namespace NavCanada.Core.TaskEngine.Triggers
{
    public abstract class ScheduleJobTriggerBase : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                using (var clusterStorage = ClusterStorage())
                {
                    var uow = clusterStorage.Uow;
                    var scheduleTaskId = GetScheduleTaskId();

                    uow.BeginTransaction();
                    try
                    {
                        var scheduleTask = clusterStorage.Uow.ScheduleTaskRepo.GetById(scheduleTaskId);

                        // check if last time it executed was longer than the minimun execution span of this job
                        if (GetLastExecutedSpan(scheduleTask) >= MinExecuteSpan)
                        {
                            // add or update last executed date
                            if (scheduleTask == null)
                            {
                                CreateScheduleTaskRecord(uow, scheduleTaskId);
                            }
                            else
                            {
                                UpdateScheduleTaskRecord(uow, scheduleTask);
                            }

                            // add schedule task message to queue
                            var mto = MtoHelper.CreateMtoMessage(GetSheduleTaskMessageId(), m => { });
                            clusterStorage.Queues.TaskEngineQueue.Publish(mto);
                        }
                        uow.Commit();
                    }
                    catch (Exception)
                    {
                        uow.Rollback();
                        throw;
                    }
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine(e); 
            }
        }

        protected abstract string GetScheduleTaskId();
        protected abstract MessageTransferObjectId GetSheduleTaskMessageId();

        protected virtual TimeSpan MinExecuteSpan => TimeSpan.FromMinutes(30);

        protected IClusterStorage ClusterStorage()
        {
            var provider = new RepositoryProvider(new RepositoryFactories());
            var uow = new Uow(provider);

            var clusterQueues = new ClusterQueues(
                new SqlServiceBrokerQueue(uow.DbContext, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings),
                new SqlServiceBrokerQueue(uow.DbContext, SqlServiceBrokerQueueSettings.TaskEngineToInitiatorSsbSettings));

            return new ClusterStorage(uow, clusterQueues);
        }

        private void CreateScheduleTaskRecord(IUow uow, string scheduleId)
        {
            uow.ScheduleTaskRepo.Add(new ScheduleTask
            {
                Id = scheduleId,
                IsProcessed = false,
                ProcessedDate = DateTime.UtcNow,
                RowVersion = Guid.NewGuid()
            });
            uow.SaveChanges();
        }

        private void UpdateScheduleTaskRecord(IUow uow, ScheduleTask scheduleTask)
        {
            scheduleTask.ProcessedDate = DateTime.UtcNow;
            uow.ScheduleTaskRepo.SafeUpdateScheduleTask(scheduleTask);
        }

        static TimeSpan GetLastExecutedSpan(ScheduleTask scheduleTask)
        {
            if (scheduleTask == null || !scheduleTask.ProcessedDate.HasValue)
                return TimeSpan.FromDays(100 * 365);

            return DateTime.UtcNow - scheduleTask.ProcessedDate.Value;
        }
    }
}

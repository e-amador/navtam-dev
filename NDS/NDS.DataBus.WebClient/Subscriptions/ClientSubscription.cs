﻿using NDS.Common.Contracts;
using NDS.DataBus.WebClient.Models;
using NDS.SolaceVMR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace NDS.DataBus.WebClient.Subscriptions
{
    public class ClientSubscription
    {
        public ClientSubscription(string topic, string subsName, string filters)
        {
            Id = Guid.NewGuid().ToString();
            _topic = topic;
            _subscriptionName = subsName;
            _filters = filters;
            _messages = new List<NotamViewModel>();
        }

        public string Id { get; private set; }
        public string Topic => _topic;
        public string SubscriptionName => _subscriptionName;

        public void StartListening(string connectionstring)
        {
            if (_completedResetEvent != null)
                throw new Exception("Already listening!");

            _completedResetEvent = new ManualResetEvent(false);
            var listener = Task.Factory.StartNew(() =>
            {
                using (var dataBus = new SolaceVMRWrapper(connectionstring))
                {
                    var subscription = dataBus.ReadFromTopic(_topic, _subscriptionName, _filters, OnReadMessage);

                    // wait until StopListening called
                    _completedResetEvent.WaitOne();

                    subscription.Unsubscribe();
                }
            });
        }

        public List<NotamViewModel> Drain()
        {
            lock (this)
            {
                var results = new List<NotamViewModel>(_messages);
                _messages.Clear();
                return results;
            }
        }

        public void StopListening()
        {
            _completedResetEvent?.Set();
            _completedResetEvent = null;
        }

        private void OnReadMessage(INdsMessage message)
        {
            lock (this)
            {
                try
                {
                    var props = message.Properties.ToDictionary(p => p.Name, p => p.Value.ToString());
                    var metadata = string.Join("\n", props.Select(kp => $"{kp.Key}: {kp.Value}"));
                    var notam = new NotamViewModel()
                    {
                        Id = props["MSG_ID"], //$"{message["series"]}{message["number"].ToString().PadLeft(4, '0')}-{message["year"].ToString().Substring(2)}",
                        //Operator = message["operator"].ToString(),
                        ItemA = message["itema"].ToString(),
                        //Fir = message["fir"].ToString(),
                        //Coordinates = message["coordinates"].ToString(),
                        Body = (message.Body ?? "").Trim(),
                        Metadata = metadata
                    };
                    _messages.Add(notam);
                }
                catch (Exception)
                {
                    _messages.Add(new NotamViewModel() { Id = "?", Body = "Error parsing message's metadata." });
                }
            }
        }

        private string _topic;
        private string _subscriptionName;
        private string _filters;
        private List<NotamViewModel> _messages;
        private ManualResetEvent _completedResetEvent;
    }
}
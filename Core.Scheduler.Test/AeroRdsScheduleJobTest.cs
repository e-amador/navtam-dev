﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Quartz;
using Moq;
using NavCanada.Core.Scheduler.Contracts;
using NavCanada.Core.Scheduler.ScheduleJobs;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.TaskEngine.Mto;
using NavCanada.Core.Domain.Model.Enums;
using System.Threading.Tasks;

namespace Core.Scheduler.Test
{
    [TestClass]
    public class AeroRdsScheduleJobTest
    {
        [TestInitialize]
        public void Initialize()
        {
            _clusterStorageMock.Setup(x => x.Uow).Returns(_uowMock.Object);
            _clusterStorageMock.Setup(x => x.Queues.TaskEngineQueue).Returns(_teQueueMock.Object);

            _uowMock.Setup(x => x.ScheduleJobRepo).Returns(_scheduleJobRepoMock.Object);
            _schedulerJob = new AeroRdsScheduleJob(_jobDetailsMock.Object, _triggerMock.Object, _clusterStorageMock.Object);
        }

        [TestMethod]
        public void AeroRdsScheduleJobExecute_ShouldPublishMessageAndAddScheduleRecord_WhenRunFirstTimeAndNotRecordExist()
        {
            var now = DateTime.UtcNow;

            var context = new Mock<IJobExecutionContext>();

            NavCanada.Core.Domain.Model.Entitities.ScheduleJob scheduleJob = null;

            var mto = CreateAeroRdsScheduleMto(now);

            _teQueueMock.Setup(x =>
                x.Publish(It.Is<MessageTransferObject>(m =>
                    (byte)m[MessageDefs.MessageIdKey].Value == (byte)mto[MessageDefs.MessageIdKey].Value)))
                    .Verifiable();

            _scheduleJobRepoMock
                .Setup(x => x.GetById(It.IsAny<object>()))
                .Returns(scheduleJob);

            context.Setup(x => x.FireTimeUtc).Returns(new DateTimeOffset(now));
            context.Setup(x => x.NextFireTimeUtc).Returns(new DateTimeOffset(now.AddMinutes(1)));

            _schedulerJob.Execute(context.Object);

            // make sure the save methods was called once
            _uowMock.Verify(x => x.BeginTransaction(It.Is<System.Data.IsolationLevel>(r=> r == System.Data.IsolationLevel.RepeatableRead)), Times.Exactly(1));
            _scheduleJobRepoMock.Verify(x => x.Add(It.IsAny<NavCanada.Core.Domain.Model.Entitities.ScheduleJob>()), Times.Exactly(1));
            _uowMock.Verify(x => x.SaveChanges(false), Times.Exactly(1));
            _uowMock.Verify(x => x.Commit(), Times.Exactly(1));
        }

        [TestMethod]
        public void AeroRdsScheduleJobExecute_ShouldPublishMessageAndUpdateScheduleRecord_WhenRunningOnSchedule()
        {
            var now = DateTime.UtcNow;

            var context = new Mock<IJobExecutionContext>();

            var scheduleJob = new NavCanada.Core.Domain.Model.Entitities.ScheduleJob {
                Id = "XX",
                LastTrigger = now.AddMinutes(-2)
            };

            var mto = CreateAeroRdsScheduleMto(now);
            _teQueueMock.Setup(x =>
                x.Publish(It.Is<MessageTransferObject>(m =>
                    (byte)m[MessageDefs.MessageIdKey].Value == (byte)mto[MessageDefs.MessageIdKey].Value)))
                    .Verifiable();

            _scheduleJobRepoMock
                .Setup(x => x.GetById(It.IsAny<object>()))
                .Returns(scheduleJob);

            context.Setup(x => x.FireTimeUtc).Returns(new DateTimeOffset(now));
            context.Setup(x => x.NextFireTimeUtc).Returns(new DateTimeOffset(now.AddMinutes(1)));

            _schedulerJob.Execute(context.Object);

            // make sure the save methods was never called
            _uowMock.Verify(x => x.BeginTransaction(It.Is<System.Data.IsolationLevel>(r => r == System.Data.IsolationLevel.RepeatableRead)), Times.Exactly(1));
            _scheduleJobRepoMock.Verify(x => x.Update(It.IsAny<NavCanada.Core.Domain.Model.Entitities.ScheduleJob>()), Times.Exactly(1));
            _uowMock.Verify(x => x.SaveChanges(false), Times.Exactly(1));
            _uowMock.Verify(x => x.Commit(), Times.Exactly(1));
        }

        private MessageTransferObject CreateAeroRdsScheduleMto(DateTime current)
        {
            var mto = new MessageTransferObject();

            mto.Add(MessageDefs.UniqueIdentifierKey, Guid.NewGuid().ToString(), DataType.String);
            mto.Add(MessageDefs.MessageTypeKey, (byte)MessageType.ExternalServicesTask, DataType.Byte);
            mto.Add(MessageDefs.MessageIdKey, (byte)MessageTransferObjectId.AeroRdsSync, DataType.Byte);
            mto.Add(MessageDefs.MessageTimeKey, current, DataType.DateTime);

            return mto;
        }


        private AeroRdsScheduleJob _schedulerJob;

        Mock<IJobDetail> _jobDetailsMock = new Mock<IJobDetail>();
        Mock<ITrigger> _triggerMock = new Mock<ITrigger>();
        Mock<IClusterStorage> _clusterStorageMock = new Mock<IClusterStorage>();
        Mock<IQueue> _teQueueMock = new Mock<IQueue>();
        Mock<IUow> _uowMock = new Mock<IUow>();

        Mock<IScheduleJobRepo> _scheduleJobRepoMock = new Mock<IScheduleJobRepo>();
    }
}

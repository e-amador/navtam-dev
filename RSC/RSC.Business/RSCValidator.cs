﻿using RSC.Business.Helpers;
using RSC.Business.SDO;
using RSC.Model;
using RSC.Model.Enums;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using Runway = RSC.Model.Runway;

namespace RSC.Business
{
    public class RSCValidator
    {
        readonly RSCProposalContext _context;

        public RSCValidator(RSCProposalContext context)
        {
            _context = context;
        }

        public void ExecuteValidations(Action<RSCResponseMessage> addMessage)
        {
            foreach (var fn in GetValidationFunctions())
            {
                var resultMessage = fn.Invoke(_context);
                if (resultMessage != null)
                {
                    addMessage(resultMessage);
                    if (resultMessage.Status == ResponseStatus.Error)
                        break;
                }
            }
        }

        delegate RSCResponseMessage ValidationFunction(RSCProposalContext ctx);

        private IEnumerable<ValidationFunction> GetValidationFunctions()
        {
            return new List<ValidationFunction>()
            {
                // 10001 - Unknown aerodrome 
                UnknownAerodrome,
                // 10002 - All runways are set to "No Winter Maintenance".
                AllRunwaysSetToNoWinterMaintenance,
                // 10003 - Too many runways are specified for aerodrome.
                AtLeastOneRunwayNotSpecified,
                // 10004 - Too many runways are specified for aerodrome.
                TooManyRunwaysForAerodrome,
                // 10005 - This runway is not defined for aerodrome. 
                RunwayNotDefinedForAerodrome,
                // 10006 - This runway is specified more than once in RSC proposal.
                RunwaySpecifiedMoreThanOnce,
                // 10102 - Next Planned Observation Time not in the future.
                NextPlannedObservationNotInTheFuture,
                // 10302 - All Runway Observation Times and Friction Observation Times more than 24 hours in the past.
                AllRunwayObservationTimesAndFrictionObservationTimesOld,
                // 10303 - Runway Observation Time in the future.
                RunwayObservationTimeInTheFuture,
                // 10401 - Duplicate Cleared Surface contaminant types.
                DuplicateClearedSurfaceContaminantTypes,
                // Sum of all contaminants coverage percentage in Cleared Portion is greater than 100%.
                SumContaminantsCoverageInClearedPortionGreaterThan100Perc,
                // 10403 - Both Cleared Surface treatments are equal.
                BothClearedSurfaceTreatmentsAreEqual,
                // 10404 - Across Runways - this runway is not defined for aerodrome.
                AcrossRunwaysNotDefinedForAerodrome,
                // 10405 - Across Runways - too many runways are specified.
                AcrossRunwaysTooMany,
                // 10406 - Other Conditions comment contains illegal characters.
                OtherConditionsCommentContainsIllegalCharacters,
                // 10407 - Other Conditions comment in French/English is missing.
                OtherConditionsCommentInLanguageMissing,
                // 10409 - Runway Friction Observation Time more than 24 hours in the past.
                RunwayFrictionObservationTimeIsOld,
                // 10410 - Runway Friction Observation Time in the future. {RunwayFrictionObservationTime}
                RunwayFrictionObservationTimeInTheFuture,
                // 10411 - Remaining Width is greater than the width of the runway. {Remaining Width}, {Runway width}
                RemainingWidthIsGreaterThanTheWidthOfTheRunway,
                // 10413 - Distance from threshold exceeds runway length. {Distance}, {RunwayLength}
                DistanceFromThresholdExceedsRunwayLength,
                // 10415 - This runway is specified more than once in Across Runways. {RunwayDesignator}
                RunwaySpecifiedMoreThanOnceAcrossRunways,
                // 10417 - Cleared Surface Contaminant Comment contains illegal characters. {Language}.
                ClearedSurfaceContaminantCommentContainsIllegalCharacters,
                // 10418 - Cleared Surface Contaminant Comment in French is missing
                ClearedSurfaceContaminantCommentMissing,
                // 10419 - Other Conditions comment contains illegal words. {Language}.
                OtherConditionsCommentContainsIllegalWords,
                // 10420 - Cleared Surface Contaminant Comment contains illegal words. {Language}.
                ClearedSurfaceContaminantCommentContainsIllegalWords,
                // 14421 - Other Conditions distance is greater that half of runway width. {Distance}, {RunwayWidth}.
                OtherConditionsDistanceGreaterThanHalfRunwayWidth,
                // 10501 - Both Cleared Portion Offset sides are equal. {Side}.
                BothClearedPortionOffsetSidesAreEqual,
                // 10502 - Cleared Portion width exceeds runway width. {ClearedWidth}, {RunwayWidth}
                ClearedPortionWidthExceedsRunwayWidth,
                // 10505 - Cleared Portion Offset from the centreline is greater that half of runway width. {Offset}, {Side}, {RunwayWidth}
                ClearedPortionOffsetFromCentrelineGreaterThanHalfOfRunwayWidth,
                // 10601 - Coverage for Contaminants ICE PATCHES or COMPACTED SNOW PATCHES limited to 95%.
                CoverageForContaminantsIcePatchesOrCompactedSnowLimitedto95Perc,
                // 10604 - Average Runway Friction is not specified for a runway with a length of less than 6000ft.
                AverageRunwayFrictionMissingOnShortRunway,
                // 10703 - Portion element of Runway Third is repeated in the message
                PortionOfRunwayThirdRepeated,
                // 10706 - Runway friction is provided in thirds for a runway that is less than 6000ft. Provide runway friction as Average Runway Friction instead.
                RunwayFrictionProvidedInThirdsForAShortRunway,
                // 10707 - Rcam value provided does not match calculated result
                RunwayConditionCodeDoesNotMatch,
                // 10711 - General Remarks contains illegal characters or words
                GeneralRemarksContainsIllegalCharacter,
                GeneralRemarksContainsIllegalWords,
                // 10712 - General Remarks in French is missing.
                GeneralRemarksInFrenchIsMissing,
                // 11001 - Taxiway freeform text contains illegal characters.
                TaxiwayTextContainsIllegalCharacters,
                // 11002 - Taxiway freeform text in French is missing.
                TaxiwayTextInFrenchIsMissing,
                // 11003 - Taxiway freeform text contains illegal words. {Language}.
                TaxiwayTextContainsIllegalWords,
                // 11101 - Apron freeform text contains illegal characters. {Language}
                ApronTextContainsIllegalCharacters,
                // 11102 - Apron freeform text in French is missing
                ApronTextInFrenchIsMissing,
                // 11103 - Apron freeform text contains illegal words. {Language}.
                ApronTextContainsIllegalWords
            };
        }

        const RSCResponseMessage NoMessage = null;

        static RSCResponseMessage UnknownAerodrome(RSCProposalContext ctx)
        {
            return ctx.Aerodrome == null ? CreateError(10001, $"Unknown aerodrome '{ctx.Proposal.Aerodrome}'.") : NoMessage;
        }

        static RSCResponseMessage AllRunwaysSetToNoWinterMaintenance(RSCProposalContext ctx)
        {
            var applies = ctx.Proposal.Runways?.All(r => r.NoWinterMaintenance) == true;
            return applies ? CreateError(10002, "All runways are set to \"No Winter Maintenance\".") : NoMessage;
        }

        static RSCResponseMessage AtLeastOneRunwayNotSpecified(RSCProposalContext ctx)
        {
            // sdo runways
            var sdoRunways = ctx.Runways.Select(rwy => rwy.Designator.ToUpper());

            // proposal runways
            var propRunways = new HashSet<string>(ctx.Proposal.Runways.Select(rwy => rwy.RunwayId.ToUpper()));

            // all SDO runwys must exist in proposal runways
            var missingRunways = !sdoRunways.All(rwy => propRunways.Contains(rwy));

            return missingRunways ? CreateError(10003, "At least one runway is not specified for aerodrome.") : NoMessage;
        }

        static RSCResponseMessage TooManyRunwaysForAerodrome(RSCProposalContext ctx)
        {
            // sdo runways
            var sdoRunwayCount = ctx.Runways.Count();

            // proposal runways
            var propRunwayCount = ctx.Proposal.Runways.Count();

            return propRunwayCount > sdoRunwayCount ? CreateError(10004, "Too many runways are specified for aerodrome.") : NoMessage;
        }

        static RSCResponseMessage RunwayNotDefinedForAerodrome(RSCProposalContext ctx)
        {
            // sdo runways
            var sdoRunways = ctx.Runways.Select(rwy => rwy.Designator.ToUpper());

            // proposal runways
            var propRunways = new HashSet<string>(ctx.Proposal.Runways.Select(rwy => rwy.RunwayId.ToUpper()));

            // find the first undefined
            var notDefined = sdoRunways.FirstOrDefault(d => !propRunways.Contains(d));

            return !string.IsNullOrEmpty(notDefined)
                ? CreateError(10005, $"This runway is not defined for aerodrome. {notDefined}")
                : NoMessage;
        }

        static RSCResponseMessage RunwaySpecifiedMoreThanOnce(RSCProposalContext ctx)
        {
            var designator = FindFirstDuplicated(ctx.Proposal.Runways.Select(rwy => rwy.RunwayId.ToUpper()));
            return !string.IsNullOrEmpty(designator)
                ? CreateError(10006, $"This runway is specified more than once in RSC proposal. {designator}")
                : NoMessage;
        }

        static RSCResponseMessage NextPlannedObservationNotInTheFuture(RSCProposalContext ctx)
        {
            if (ctx.Dummymode)
            {
                return NoMessage;
            }

            return ctx.Proposal.NextPlannedObs.HasValue && ctx.Proposal.NextPlannedObs.Value < DateTime.UtcNow
                ? CreateError(10102, $"Next Planned Observation Time not in the future. {ctx.Proposal.NextPlannedObs.Value}.")
                : null;
        }

        static RSCResponseMessage AllRunwayObservationTimesAndFrictionObservationTimesOld(RSCProposalContext ctx)
        {
            if (ctx.Dummymode)
            {
                return NoMessage;
            }

            return ctx.Proposal.Runways.All(RunwayObservationTimesAndFrictionObservationTimesOld)
                ? CreateError(10302, "All Runway Observation Times and Friction Observation Times more than 24 hours in the past.")
                : NoMessage;
        }

        static RSCResponseMessage RunwayObservationTimeInTheFuture(RSCProposalContext ctx)
        {
            if (ctx.Dummymode)
            {
                return NoMessage;
            }
            
            var runway = ctx.Proposal.Runways.FirstOrDefault(RunwayObservationTimeInTheFuture);
            return runway != null
                ? CreateError(10303, $"Runway Observation Time in the future. {runway.ObsTimeUTC.Value}", runway.RunwayId)
                : NoMessage;
        }

        static RSCResponseMessage DuplicateClearedSurfaceContaminantTypes(RSCProposalContext ctx)
        {
            var runway = ctx.Proposal.Runways.FirstOrDefault(RunwayContainDuplicatedContaminants);
            return runway != null
                ? CreateError(10401, $"Duplicate Cleared Surface contaminant types.", runway.RunwayId) // todo find which contaminant
                : NoMessage;
        }

        static  RSCResponseMessage SumContaminantsCoverageInClearedPortionGreaterThan100Perc(RSCProposalContext ctx)
        {
            var runway = ctx.Proposal.Runways.FirstOrDefault(RunwayContaminantsAddMoreThan100Perc);
            return runway != null
                ? CreateError(10402, $"Sum of all contaminants coverage percentage in Cleared Portion is greater than 100%.", runway.RunwayId) // todo find which contaminant
                : NoMessage;
        }

        static  RSCResponseMessage BothClearedSurfaceTreatmentsAreEqual(RSCProposalContext ctx)
        {
            var runway = ctx.Proposal.Runways.FirstOrDefault(BothClearedSurfaceTreatmentsAreEqual);
            return runway != null
                ? CreateError(10403, $"Both Cleared Surface treatments are equal.", runway.RunwayId) // todo find which treatment
                : NoMessage;
        }

        static  RSCResponseMessage AcrossRunwaysNotDefinedForAerodrome(RSCProposalContext ctx)
        {
            var runway = ctx.Proposal.Runways.FirstOrDefault(rwy => AcrossRunwaysNotDefinedForAerodrome(ctx, rwy));
            return runway != null
                ? CreateError(10404, $"Across Runways - this runway is not defined for aerodrome. {runway.RunwayId}", runway.RunwayId)
                : NoMessage;
        }

        static  RSCResponseMessage AcrossRunwaysTooMany(RSCProposalContext ctx)
        {
            var runway = ctx.Proposal.Runways.FirstOrDefault(rwy => AcrossRunwaysTooMany(ctx.Runways.Count(), rwy));
            return runway != null
                ? CreateError(10405, $"Across Runways - too many runways are specified.", runway.RunwayId)
                : NoMessage;
        }

        static  RSCResponseMessage OtherConditionsCommentContainsIllegalCharacters(RSCProposalContext ctx)
        {
            var runway = ctx.Proposal.Runways.FirstOrDefault(OtherConditionsCommentContainsIllegalCharacters);
            var comments = runway?.ClearedPortion?.OtherConditions?.OtherConditionsComment;
            var language = !AftnHelpers.IsValidAftnText(comments?.EnglishText) ? "English" : "French";
            return runway != null
                ? CreateError(10406, $"Other Conditions comment contains illegal characters. {language}", runway.RunwayId)
                : NoMessage;
        }

        static  RSCResponseMessage OtherConditionsCommentInLanguageMissing(RSCProposalContext ctx)
        {
            var runway = ctx.Proposal.Runways.FirstOrDefault(OtherConditionsCommentInLanguageMissing);
            var comments = runway?.ClearedPortion?.OtherConditions?.OtherConditionsComment;
            var language = string.IsNullOrEmpty(comments?.EnglishText) ? "English" : "French";
            return runway != null
                ? CreateError(10407, $"Other Conditions comment in {language} is missing.", runway.RunwayId)
                : NoMessage;
        }

        static  RSCResponseMessage RunwayFrictionObservationTimeIsOld(RSCProposalContext ctx)
        {
            if (ctx.Dummymode)
            {
                return NoMessage;
            }

            var runway = ctx.Proposal.Runways.FirstOrDefault(RunwayFrictionObservationTimeIsOld);
            return runway != null
                ? CreateError(10409, $"Runway Friction Observation Time more than 24 hours in the past.", runway.RunwayId)
                : NoMessage;
        }

        static  RSCResponseMessage RunwayFrictionObservationTimeInTheFuture(RSCProposalContext ctx)
        {
            if (ctx.Dummymode)
            {
                return NoMessage;
            }

            var runway = ctx.Proposal.Runways.FirstOrDefault(RunwayFrictionObservationTimeInTheFuture);
            return runway != null
                ? CreateError(10410, $"Runway Friction Observation Time in the future.", runway.RunwayId)
                : NoMessage;
        }

        static  RSCResponseMessage RemainingWidthIsGreaterThanTheWidthOfTheRunway(RSCProposalContext ctx)
        {
            var runway = ctx.Proposal.Runways.FirstOrDefault(rwy => RemainingWidthIsGreaterThanTheWidthOfTheRunway(rwy, ctx.Aerodrome.GetRunway(rwy.RunwayId)));
            return runway != null
                ? CreateError(10411, $"Remaining Width is greater than the width of the runway.", runway.RunwayId)
                : NoMessage;
        }

        static  RSCResponseMessage DistanceFromThresholdExceedsRunwayLength(RSCProposalContext ctx)
        {
            var runway = ctx.Proposal.Runways.FirstOrDefault(rwy => DistanceFromThresholdExceedsRunwayLength(rwy, ctx.Aerodrome.GetRunway(rwy.RunwayId)));
            return runway != null
                ? CreateError(10413, $"Distance from threshold exceeds runway length.", runway.RunwayId)
                : NoMessage;
        }

        static  RSCResponseMessage RunwaySpecifiedMoreThanOnceAcrossRunways(RSCProposalContext ctx)
        {
            var runway = ctx.Proposal.Runways.FirstOrDefault(RunwaySpecifiedMoreThanOnceAcrossRunways);
            return runway != null
                ? CreateError(10415, $"This runway is specified more than once in Across Runways. {runway.RunwayId}", runway.RunwayId)
                : NoMessage;
        }

        static  RSCResponseMessage ClearedSurfaceContaminantCommentContainsIllegalCharacters(RSCProposalContext ctx)
        {
            var runway = ctx.Proposal.Runways.FirstOrDefault(ClearedSurfaceContaminantCommentContainsIllegalCharacters);
            var comments = runway?.ClearedPortion?.ContaminantComment;
            var language = !AftnHelpers.IsValidAftnText(comments?.EnglishText) ? "English" : "French";
            return runway != null
                ? CreateError(10417, $"Cleared Surface Contaminant Comment contains illegal characters.", runway.RunwayId)
                : NoMessage;
        }

        static  RSCResponseMessage ClearedSurfaceContaminantCommentMissing(RSCProposalContext ctx)
        {
            var runway = ctx.Proposal.Runways.FirstOrDefault(ClearedSurfaceContaminantCommentMissing);
            var comments = runway?.ClearedPortion?.ContaminantComment;
            var language = string.IsNullOrEmpty(comments?.EnglishText) ? "English" : "French";
            return runway != null
                ? CreateError(10418, $"Cleared Surface Contaminant Comment in {language} is missing.", runway.RunwayId)
                : NoMessage;
        }

        private RSCResponseMessage OtherConditionsCommentContainsIllegalWords(RSCProposalContext ctx)
        {
            var runway = ctx.Proposal.Runways.FirstOrDefault(OtherConditionsCommentContainsIllegalWords);
            return runway != null
                ? CreateError(10419, $"Other Conditions comment contains illegal words.", runway.RunwayId)
                : NoMessage;
        }

        private RSCResponseMessage ClearedSurfaceContaminantCommentContainsIllegalWords(RSCProposalContext ctx)
        {
            var runway = ctx.Proposal.Runways.FirstOrDefault(ClearedSurfaceContaminantCommentContainsIllegalWords);
            return runway != null
                ? CreateError(10420, $"Cleared Surface Contaminant Comment contains illegal words.", runway.RunwayId)
                : NoMessage;
        }

        static  RSCResponseMessage OtherConditionsDistanceGreaterThanHalfRunwayWidth(RSCProposalContext ctx)
        {
            var runway = ctx.Proposal.Runways.FirstOrDefault(rwy => OtherConditionsDistanceGreaterThanHalfRunwayWidth(rwy, ctx.Aerodrome.GetRunway(rwy.RunwayId)));
            return runway != null
                ? CreateError(10421, $"Other Conditions distance is greater that half of runway width.", runway.RunwayId)
                : NoMessage;
        }

        static  RSCResponseMessage BothClearedPortionOffsetSidesAreEqual(RSCProposalContext ctx)
        {
            var runway = ctx.Proposal.Runways.FirstOrDefault(BothClearedPortionOffsetSidesAreEqual);
            return runway != null
                ? CreateError(10501, $"Both Cleared Portion Offset sides are equal.", runway.RunwayId)
                : NoMessage;
        }

        static  RSCResponseMessage ClearedPortionWidthExceedsRunwayWidth(RSCProposalContext ctx)
        {
            var runway = ctx.Proposal.Runways.FirstOrDefault(rwy => ClearedPortionWidthExceedsRunwayWidth(ctx, rwy));
            return runway != null
                ? CreateError(10502, $"Cleared Portion width exceeds runway width.", runway.RunwayId)
                : NoMessage;
        }

        static  RSCResponseMessage ClearedPortionOffsetFromCentrelineGreaterThanHalfOfRunwayWidth(RSCProposalContext ctx)
        {
            var runway = ctx.Proposal.Runways.FirstOrDefault(rwy => ClearedPortionOffsetFromCentrelineGreaterThanHalfOfRunwayWidth(ctx, rwy));
            return runway != null
                ? CreateError(10505, $"Cleared Portion Offset from the centreline is greater that half of runway width.", runway.RunwayId)
                : NoMessage;
        }

        static  RSCResponseMessage CoverageForContaminantsIcePatchesOrCompactedSnowLimitedto95Perc(RSCProposalContext ctx)
        {
            var runway = ctx.Proposal.Runways.FirstOrDefault(CoverageForContaminantsIcePatchesOrCompactedSnowMoreThan95Perc);
            return runway != null
                ? CreateError(10601, $"Coverage for Contaminants ICE PATCHES or COMPACTED SNOW PATCHES limited to 95%", runway.RunwayId)
                : NoMessage;
        }

        static  RSCResponseMessage AverageRunwayFrictionMissingOnShortRunway(RSCProposalContext ctx)
        {
            var runway = ctx.Proposal.Runways.FirstOrDefault(rwy => AverageRunwayFrictionMissingOnShortRunway(ctx, rwy));
            return runway != null
                ? CreateWarning(10604, $"Average Runway Friction is not specified for a runway with a length of less than 6000ft.", runway.RunwayId)
                : NoMessage;
        }

        static  RSCResponseMessage PortionOfRunwayThirdRepeated(RSCProposalContext ctx)
        {
            var runway = ctx.Proposal.Runways.FirstOrDefault(PortionOfRunwayThirdRepeated);
            return runway != null
                ? CreateError(10703, $"Portion element of Runway Third is repeated in the message.", runway.RunwayId)
                : NoMessage;
        }

        static  RSCResponseMessage RunwayFrictionProvidedInThirdsForAShortRunway(RSCProposalContext ctx)
        {
            var runway = ctx.Proposal.Runways.FirstOrDefault(rwy => RunwayFrictionProvidedInThirdsForAShortRunway(ctx, rwy));
            return runway != null
                ? CreateWarning(10706, $"Runway friction is provided in thirds for a runway that is less than 6000ft. Provide runway friction as Average Runway Friction instead.", runway.RunwayId)
                : NoMessage;
        }

        static RSCResponseMessage RunwayConditionCodeDoesNotMatch(RSCProposalContext ctx)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var rwy in ctx.Proposal.Runways.ToList())
            {
                if (!rwy.NoWinterMaintenance || !IsConditionsChangingRapidly(ctx.Proposal))
                {
                    foreach (var clearedPortionThird in rwy.ClearedPortion.RunwayThirds)
                    { 
                        var result = RCAMCalculation.GetCalculationFromThird(clearedPortionThird);
                        if (!result.NESCalculatedRwyCC.Equals(result.RwyCC))
                            sb.Append($"Runway: {rwy.RunwayId} provided RwyCC for third {clearedPortionThird.Portion} does not match calculated result from NAV CANADA. ");
                    }
                }
            }

            return sb.Length > 0 
                ? CreateWarning(10706, sb.ToString())
                : NoMessage;
        }

        private static bool IsConditionsChangingRapidly(RSCProposal proposal)
        {
            return proposal.ConditionsChangingRapidly != null;
        }

        static  RSCResponseMessage GeneralRemarksContainsIllegalCharacter(RSCProposalContext ctx)
        {
            var language = ValidateBilingualTextAftnChars(ctx.Proposal?.GeneralRemarks);
            return !string.IsNullOrEmpty(language)
                ? CreateError(10711, $"General Remarks contains illegal characters. {language}")
                : NoMessage;
        }

        private RSCResponseMessage GeneralRemarksContainsIllegalWords(RSCProposalContext ctx)
        {
            var language = ValidateBilingualTextAftnWords(ctx.Proposal?.GeneralRemarks);
            return !string.IsNullOrEmpty(language)
                ? CreateError(10711, $"General Remarks contains illegal words. {language}")
                : NoMessage;
        }

        static  RSCResponseMessage GeneralRemarksInFrenchIsMissing(RSCProposalContext ctx)
        {
            var rmks = ctx.Proposal.GeneralRemarks;
            return rmks != null && string.IsNullOrEmpty(rmks.FrenchText)
                ? CreateError(10712, $"General Remarks in French is missing.")
                : NoMessage;
        }

        static  RSCResponseMessage TaxiwayTextContainsIllegalCharacters(RSCProposalContext ctx)
        {
            var language = ValidateBilingualTextAftnChars(ctx.Proposal.Taxiways);
            return !string.IsNullOrEmpty(language)
                ? CreateError(11001, $"Taxiway freeform text contains illegal characters. {language}")
                : NoMessage;
        }

        static  RSCResponseMessage TaxiwayTextInFrenchIsMissing(RSCProposalContext ctx)
        {
            var twyText = ctx.Proposal.Taxiways;
            return twyText != null && string.IsNullOrEmpty(twyText.FrenchText)
                ? CreateError(11002, $"Taxiway freeform text in French is missing.")
                : NoMessage;
        }

        private RSCResponseMessage TaxiwayTextContainsIllegalWords(RSCProposalContext ctx)
        {
            var language = ValidateBilingualTextAftnWords(ctx.Proposal.Taxiways);
            return !string.IsNullOrEmpty(language)
                ? CreateError(11003, $"Taxiway freeform text contains illegal words. {language}")
                : NoMessage;
        }

        static  RSCResponseMessage ApronTextContainsIllegalCharacters(RSCProposalContext ctx)
        {
            var language = ValidateBilingualTextAftnChars(ctx.Proposal.Aprons);
            return !string.IsNullOrEmpty(language)
                ? CreateError(11101, $"Apron freeform text contains illegal characters. {language}")
                : NoMessage;
        }

        static  RSCResponseMessage ApronTextInFrenchIsMissing(RSCProposalContext ctx)
        {
            var apronText = ctx.Proposal.Aprons;
            return apronText != null && string.IsNullOrEmpty(apronText.FrenchText)
                ? CreateError(11102, $"Apron freeform text in French is missing.")
                : NoMessage;
        }

        private RSCResponseMessage ApronTextContainsIllegalWords(RSCProposalContext ctx)
        {
            var language = ValidateBilingualTextAftnWords(ctx.Proposal.Aprons);
            return !string.IsNullOrEmpty(language)
                ? CreateError(11103, $"Apron freeform text contains illegal words. {language}")
                : NoMessage;
        }

        bool OtherConditionsCommentContainsIllegalWords(Runway rwy)
        {
            var comments = rwy.ClearedPortion?.OtherConditions?.OtherConditionsComment;
            return ContainsAnyWord(comments?.EnglishText, _context.ForbiddenWords.Contains) || ContainsAnyWord(comments?.FrenchText, _context.ForbiddenWords.Contains);
        }

        bool ClearedSurfaceContaminantCommentContainsIllegalWords(Runway rwy)
        {
            var comments = rwy.ClearedPortion?.ContaminantComment;
            return ContainsAnyWord(comments?.EnglishText, _context.ForbiddenWords.Contains) || ContainsAnyWord(comments?.FrenchText, _context.ForbiddenWords.Contains);
        }

        static string FindFirstDuplicated(IEnumerable<string> list)
        {
            return list.GroupBy(s => s).Where(g => g.Count() > 1).Select(g => g.Key).FirstOrDefault();
        }

        static bool ContainsDuplicated<T>(IEnumerable<T> list)
        {
            return list.GroupBy(s => s).Count(g => g.Count() > 1) > 0;
        }

        static bool RunwayObservationTimesAndFrictionObservationTimesOld(Runway rwy)
        {
            return RunwayObservationTimeOld(rwy) && RunwayFrictionObservationTimeOld(rwy);
        }

        static bool RunwayObservationTimeOld(Runway rwy)
        {
            return rwy.ObsTimeUTC.HasValue && rwy.ObsTimeUTC.Value < DateTime.UtcNow.AddHours(-24);
        }

        static bool RunwayFrictionObservationTimeOld(Runway rwy)
        {
            var obsTimeUTC = rwy.ClearedPortion?.AverageRwyFriction?.RFObsTime;
            return obsTimeUTC.HasValue && obsTimeUTC.Value < DateTime.UtcNow.AddHours(-24);
        }

        static bool RunwayObservationTimeInTheFuture(Runway rwy) => rwy.ObsTimeUTC.HasValue && rwy.ObsTimeUTC.Value > DateTime.UtcNow;

        static bool RunwayContainDuplicatedContaminants(Runway rwy)
        {
            if (rwy.ClearedPortion != null)
            {
                if (rwy.ClearedPortion.Average != null)
                {
                    return DuplicatedContaminant(rwy.ClearedPortion.Average.Contaminants);
                }
                else
                {
                    return rwy.ClearedPortion.RunwayThirds.Count(t => DuplicatedContaminant(t.Contaminants)) != 0;
                }
            }
            return false;
        }
        static bool DuplicatedContaminant(ContaminantIns[] list) => list.Length > 1 && SameContaminant(list[0], list[1]);
        static bool SameContaminant(ContaminantIns a, ContaminantIns b) => a.ContamType == b.ContamType && a.ContamTypeNoD == b.ContamTypeNoD;

        static bool RunwayContaminantsAddMoreThan100Perc(Runway rwy)
        {
            if (rwy.ClearedPortion != null)
            {
                if (rwy.ClearedPortion.Average != null)
                {
                    return ContaminantsAddMoreThan100Perc(rwy.ClearedPortion.Average.Contaminants);
                }
                else
                {
                    return rwy.ClearedPortion.RunwayThirds.Count(t => ContaminantsAddMoreThan100Perc(t.Contaminants)) > 0;
                }
            }
            return false;
        }

        static bool ContaminantsAddMoreThan100Perc(ContaminantIns[] contaminants) => contaminants.Sum(c => c.ContamCoverage) > 100;

        static bool ContaminantsNoDepthCoverageMoreThan95Perc(ContaminantIns[] contaminants, params ContaminantWithNoDepthEnum[] list)
        {
            return contaminants.Any(c => c.ContamCoverage > 95 && c.ContamTypeNoD.HasValue && list.Contains(c.ContamTypeNoD.Value));
        }

        static bool BothClearedSurfaceTreatmentsAreEqual(Runway rwy)
        {
            var treatments = rwy.ClearedPortion?.Treatments;
            return treatments != null && treatments.Length > 1 && treatments[0] == treatments[1];
        }

        static bool AcrossRunwaysNotDefinedForAerodrome(RSCProposalContext ctx, Runway rwy)
        {
            var acrossRwys = rwy.ClearedPortion?.OtherConditions?.OtherConditEdge?.AcrossRwy;
            if (acrossRwys != null)
            {
                var sdoRunways = new HashSet<string>(ctx.Runways.Select(r => r.Designator.ToUpper()));
                return !acrossRwys.All(sdoRunways.Contains);
            }
            return false;
        }

        static bool AcrossRunwaysTooMany(int sdoRwyCount, Runway rwy)
        {
            var acrossRwys = rwy.ClearedPortion?.OtherConditions?.OtherConditEdge?.AcrossRwy;
            var acrossRwysCount = acrossRwys != null ? acrossRwys.Length : 0;
            return sdoRwyCount < acrossRwysCount;
        }

        static bool OtherConditionsCommentContainsIllegalCharacters(Runway rwy)
        {
            var comments = rwy.ClearedPortion?.OtherConditions?.OtherConditionsComment;
            return !AftnHelpers.IsValidAftnText(comments?.EnglishText) || !AftnHelpers.IsValidAftnText(comments?.FrenchText);
        }

        static bool OtherConditionsCommentInLanguageMissing(Runway rwy)
        {
            var comments = rwy.ClearedPortion?.OtherConditions?.OtherConditionsComment;
            return comments != null && (string.IsNullOrEmpty(comments.EnglishText) || string.IsNullOrEmpty(comments.FrenchText));
        }

        static bool RunwayFrictionObservationTimeIsOld(Runway rwy)
        {
            if (rwy.ClearedPortion != null)
            {
                if (rwy.ClearedPortion.Average != null)
                    return RunwayFrictionObservationTimeIsOld(rwy.ClearedPortion.AverageRwyFriction);
                else
                    return rwy.ClearedPortion.RunwayThirds.Any(thrd => RunwayFrictionObservationTimeIsOld(thrd.RwyFrictionThird));
            }
            return false;
        }

        static bool RunwayFrictionObservationTimeInTheFuture(Runway rwy)
        {
            if (rwy.ClearedPortion != null)
            {
                if (rwy.ClearedPortion.Average != null)
                    return RunwayFrictionObservationTimeInTheFuture(rwy.ClearedPortion.AverageRwyFriction);
                else
                    return rwy.ClearedPortion.RunwayThirds.Any(thrd => RunwayFrictionObservationTimeInTheFuture(thrd.RwyFrictionThird));
            }
            return false;
        }

        static bool RemainingWidthIsGreaterThanTheWidthOfTheRunway(Runway rwy, IRunway sdoRwy)
        {
            var fi = rwy?.RemainingWidth?.ContaminantFI?.ContamDepthFI;
            var rwyWidth = (int)(fi != null ? fi.ContamFeet + fi.ContamInches / 12 : 0);
            return sdoRwy != null && sdoRwy.Width != 0 && sdoRwy.Width < rwyWidth;
        }

        static bool DistanceFromThresholdExceedsRunwayLength(Runway rwy, IRunway sdoRwy)
        {
            var oca = rwy.ClearedPortion?.OtherConditions?.OtherConditArea;
            var dist = oca != null ? oca.DistFromThreshold : 0;
            return sdoRwy != null && sdoRwy.Length != 0 && sdoRwy.Length < dist;
        }

        static bool RunwaySpecifiedMoreThanOnceAcrossRunways(Runway rwy)
        {
            var acrossRwys = rwy.ClearedPortion?.OtherConditions?.OtherConditEdge?.AcrossRwy;
            return acrossRwys != null && !string.IsNullOrEmpty(FindFirstDuplicated(acrossRwys));
        }

        static bool ClearedSurfaceContaminantCommentContainsIllegalCharacters(Runway rwy)
        {
            var comments = rwy.ClearedPortion?.ContaminantComment;
            return !AftnHelpers.IsValidAftnText(comments?.EnglishText) || !AftnHelpers.IsValidAftnText(comments?.FrenchText);
        }

        static bool ClearedSurfaceContaminantCommentMissing(Runway rwy)
        {
            var comments = rwy.ClearedPortion?.ContaminantComment;
            return comments != null && (string.IsNullOrEmpty(comments.EnglishText) || string.IsNullOrEmpty(comments.FrenchText));
        }

        static bool OtherConditionsDistanceGreaterThanHalfRunwayWidth(Runway rwy, IRunway sdoRwy)
        {
            var ocDist = rwy?.ClearedPortion?.OtherConditions?.OtherConditEdge?.OCDistance;
            var rwyWidth = sdoRwy?.Width;
            return ocDist.HasValue && rwyWidth.HasValue && ocDist.Value > rwyWidth.Value / 2;
        }

        static bool BothClearedPortionOffsetSidesAreEqual(Runway rwy)
        {
            var sides = rwy?.ClearedPortion?.OtherConditions?.OtherConditEdge?.OCSide;
            return sides != null && sides.Length == 2 && sides[0] == sides[1];
        }

        static bool ClearedPortionWidthExceedsRunwayWidth(RSCProposalContext ctx, Runway rwy)
        {
            var sdoRwy = ctx.Aerodrome.GetRunway(rwy.RunwayId);
            return sdoRwy != null && sdoRwy.Width != 0 && sdoRwy.Width < rwy.CentredClearedWidth;
        }

        static bool ClearedPortionOffsetFromCentrelineGreaterThanHalfOfRunwayWidth(RSCProposalContext ctx, Runway rwy)
        {
            var sdoRwy = ctx.Aerodrome.GetRunway(rwy.RunwayId);
            var halfWidth = sdoRwy.Width / 2;
            if (!rwy.ClearedFull && halfWidth != 0)
            {
                if (rwy.ClearedPortionOffsets?.Length == 2)
                {
                    return rwy.ClearedPortionOffsets.Any(p => p.OffsetFT > halfWidth);
                }
            }
            return false;
        }

        static bool CoverageForContaminantsIcePatchesOrCompactedSnowMoreThan95Perc(Runway rwy)
        {
            if (rwy.ClearedPortion != null)
            {
                if (rwy.ClearedPortion.Average != null)
                {
                    return ContaminantsNoDepthCoverageMoreThan95Perc(rwy.ClearedPortion.Average.Contaminants, ContaminantWithNoDepthEnum.Ice, ContaminantWithNoDepthEnum.CompactedSnow);
                }
                else
                {
                    return rwy.ClearedPortion.RunwayThirds.Count(t => ContaminantsNoDepthCoverageMoreThan95Perc(t.Contaminants, ContaminantWithNoDepthEnum.Ice, ContaminantWithNoDepthEnum.CompactedSnow)) > 0;
                }
            }
            return false;
        }

        static bool AverageRunwayFrictionMissingOnShortRunway(RSCProposalContext ctx, Runway rwy)
        {
            var sdoRwy = ctx.Aerodrome.GetRunway(rwy.RunwayId);
            return sdoRwy.Length < 6000 && rwy.ClearedPortion != null && rwy.ClearedPortion.AverageRwyFriction == null;
        }

        static bool PortionOfRunwayThirdRepeated(Runway rwy)
        {
            var thirds = rwy.ClearedPortion?.RunwayThirds ?? new ClearedSurfaceThird[] {};
            return ContainsDuplicated(thirds.Select(t => t.Portion));
        }

        static bool RunwayFrictionProvidedInThirdsForAShortRunway(RSCProposalContext ctx, Runway rwy)
        {
            var sdoRwy = ctx.Aerodrome.GetRunway(rwy.RunwayId);
            var thirds = rwy.ClearedPortion?.RunwayThirds ?? new ClearedSurfaceThird[] {};
            return sdoRwy.Length < 6000 && thirds.Length > 0;
        }

        static bool RunwayFrictionObservationTimeIsOld(RwyFriction rwyFriction) => rwyFriction != null && rwyFriction.RFObsTime < DateTime.UtcNow.AddHours(-24);
        static bool RunwayFrictionObservationTimeInTheFuture(RwyFriction rwyFriction) => rwyFriction != null && rwyFriction.RFObsTime > DateTime.UtcNow;

        static  string ValidateBilingualTextAftnChars(FreeformText text)
        {
            if (!AftnHelpers.IsValidAftnText(text?.EnglishText))
                return "English";
            if (!AftnHelpers.IsValidAftnText(text?.FrenchText))
                return "French";
            return string.Empty;
        }

        private string ValidateBilingualTextAftnWords(FreeformText text)
        {
            if (ContainsAnyWord(text?.EnglishText, _context.ForbiddenWords.Contains))
                return "English";
            if (ContainsAnyWord(text?.FrenchText, _context.ForbiddenWords.Contains))
                return "French";
            return string.Empty;
        }

        static bool ContainsAnyWord(string text, Func<string, bool> containFunction)
        {
            var textWords = (text ?? "").Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            return textWords.Any(w => containFunction.Invoke(w));
        }

        static RSCResponseMessage CreateError(int code, string message, string runwayId = null, int? third = null) => CreateMessage(ResponseStatus.Error, code, message, runwayId, third);
        static RSCResponseMessage CreateWarning(int code, string message, string runwayId = null, int? third = null) => CreateMessage(ResponseStatus.Warning, code, message, runwayId, third);
        static RSCResponseMessage CreateMessage(ResponseStatus status, int code, string message, string runwayId = null, int? third = null)
        {
            return new RSCResponseMessage()
            {
                Status = status,
                Number = code,
                Description = message,
                RunwayId = runwayId,
                Third = third
            };
        }
    }
}
﻿using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.TaskEngine.Contracts;
using NavCanada.Core.TaskEngine.Helpers;
using NDS.Relay;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NavCanada.Core.TaskEngine.NdsTasks
{
    public abstract class AftnQueryTask : INdsTask
    {
        ///TODO: Is logging really nessecary here?
        public void Execute(NdsIncomingRequest request, IClusterStorage clusterStorage, ILogger logger)
        {
            var aftnRequest = AftnMessageParser.ParseRequestMessage(request.Body);
            if (aftnRequest != null)
            {
                if (UserCanPerformQueries(aftnRequest.ReplyAddress, clusterStorage.Uow))
                {
                    // verify it is addressed to the proper Notam office
                    if (aftnRequest.NotamOffice == AftnConsts.CYHQ)
                    {
                        ExecuteRequest(aftnRequest, clusterStorage);
                    }
                    else
                    {
                        ReplyWithMessage(aftnRequest.ReplyAddress, AftnConsts.SeriesNotManaged, clusterStorage);
                    }
                }
                else
                {
                    // the user doesn't have rights to perform queries
                    ReplyWithMessage(aftnRequest.ReplyAddress, AftnConsts.UserCannotPerformQueries, clusterStorage);
                }
            }
        }

        protected abstract void ExecuteRequest(AftnRequestMessage request, IClusterStorage clusterStorage);

        protected static bool UserCanPerformQueries(string address, IUow uow)
        {
            // NOF MUST be able to perform queries
            return AftnConsts.NOFAddress.Equals(address) || uow.NdsClientRepo.CanAddressPerformQueries(address);
        }

        protected static IEnumerable<NdsOutgoingMessage> SaveMessageGroupToDatabase(IEnumerable<NdsOutgoingMessage> messages, IUow uow)
        {
            // ensure the messages have the proper ordering before saving to the DB
            var group = RelayUtils.CreateMessageGroupOrdering(messages);

            foreach (var message in group)
            {
                uow.NdsOutgoingMessageRepo.Add(message);
            }
            uow.SaveChanges();

            return group;
        }

        protected static void PublishMessageGroup(IEnumerable<NdsOutgoingMessage> group, IClusterStorage clusterStorage)
        {
            var firstMessage = group.FirstOrDefault();
            if (firstMessage != null)
            {
                var mto = MtoHelper.CreateMtoMessageWithKeyValue(MessageTransferObjectId.PublishMessageGroup, firstMessage.Id);
                clusterStorage.Queues.TaskEngineQueue.Publish(mto);
            }
        }

        private static void ReplyWithMessage(string address, string message, IClusterStorage clusterStorage)
        {
            var ndsMessage = RelayUtils.GetSingleRqrResponseMessage(Guid.NewGuid(), string.Empty, address, message);
            var group = SaveMessageGroupToDatabase(ndsMessage.Yield(), clusterStorage.Uow);
            PublishMessageGroup(group, clusterStorage);
        }
    }

    public class AftnRqnTask : AftnQueryTask
    {
        protected override void ExecuteRequest(AftnRequestMessage request, IClusterStorage clusterStorage)
        {
            var groupId = Guid.NewGuid();
            var resultMessages = Enumerable.Empty<NdsOutgoingMessage>();

            var queryNumbers = TrimLastCRLF(request.RequestBody);
            var queries = AftnQueryUtils.ParseRqnQuery(queryNumbers).ToList();

            if (queries.AreValid())
            {
                var uow = clusterStorage.Uow;

                // execute queries
                var queryResults = AftnQueryUtils.ExecuteRqnQueries(queries, range => FindNotamsInRange(range, uow));

                // create messages
                resultMessages = RelayUtils.GetAftnRqnResponseMessages(queryResults, groupId, request.ReplyAddress, request.Language == "C");
            }
            else
            {
                // query has errrors
                resultMessages = RelayUtils.GetSingleRqrResponseMessage(groupId, queryNumbers, request.ReplyAddress, AftnConsts.IncorrectRequestFormat).Yield();
            }

            // save messages to the DB
            var group = SaveMessageGroupToDatabase(resultMessages, clusterStorage.Uow);

            // publish the message group
            PublishMessageGroup(group, clusterStorage);
        }

        private static IEnumerable<RqnNotamResultDto> FindNotamsInRange(AftnQueryRange range, IUow uow)
        {
            return uow.NotamRepo.GetNotamsByRange(range.Series, range.FromNumber, range.ToNumber, range.Year);
        }

        private static string TrimLastCRLF(string text)
        {
            return (text ?? "").EndsWith(AftnConsts.CRLF) ? text.Substring(0, text.Length - AftnConsts.CRLF.Length) : text;
        }
    }

    public class AftnRqlTask : AftnQueryTask
    {
        protected override void ExecuteRequest(AftnRequestMessage request, IClusterStorage clusterStorage)
        {
            var groupId = Guid.NewGuid();
            var resultMessages = Enumerable.Empty<NdsOutgoingMessage>();

            var series = GetRequestSeries(request.RequestBody);
            if (ValidSeries(series))
            {
                // retrieve the list of supported series
                var managedSeries = GetSupportedNotamSeries(clusterStorage.Uow);

                // get the rql response messages
                resultMessages = RelayUtils.GetAftnRqlResponseMessages(series, groupId, request.ReplyAddress, s => GetNotamNumbersFromSerie(s, managedSeries, clusterStorage.Uow));
            }
            else
            {
                // query has errors
                resultMessages = RelayUtils.GetSingleRqrResponseMessage(groupId, string.Join(" ", series), request.ReplyAddress, AftnConsts.IncorrectRequestFormat).Yield();
            }

            // save messages to the DB
            var group = SaveMessageGroupToDatabase(resultMessages, clusterStorage.Uow);

            // publish the message group
            PublishMessageGroup(group, clusterStorage);
        }

        private static List<string> GetRequestSeries(string request)
        {
            return (request ?? "").Split(new char[] { ' ', '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        private static AftnRqlResult GetNotamNumbersFromSerie(string series, HashSet<string> managedSeries, IUow uow)
        {
            return new AftnRqlResult(managedSeries.Contains(series) ? uow.NotamRepo.GetActiveNotamNumbersBySeries(series, true) : null);
        }

        private static HashSet<string> GetSupportedNotamSeries(IUow uow)
        {
            return new HashSet<string>(uow.SeriesChecklistRepo.GetAvailableSeries());
        }

        private static bool ValidSeries(IEnumerable<string> series) => series.All(s => s.Length == 1 && s[0] >= 'A' && s[0] <= 'Z');
    }
}
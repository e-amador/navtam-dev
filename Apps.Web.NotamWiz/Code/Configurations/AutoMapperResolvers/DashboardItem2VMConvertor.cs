﻿namespace NavCanada.Applications.NotamWiz.Web.Code.Configurations.AutoMapperResolvers
{
    using AutoMapper;

    using NavCanada.Applications.NotamWiz.Web.Models;
    using NavCanada.Core.Domain.Model.Entitities;
    using NavCanada.Core.Domain.Model.Enums;

    using Westwind.Globalization;

    public class DashboardItem2VMConvertor : ITypeConverter<DashboardItem, DashboardItemViewModel>
    {

        public DashboardItemViewModel Convert(ResolutionContext context)
        {
            Doa doa = (Doa)context.Options.Items["doa"];

            DashboardItem d =  context.SourceValue as DashboardItem;

            DashboardItemViewModel dvm = new DashboardItemViewModel()
            {
                Id = d.Id,
                Name = d.Type == DashboardItemType.All? DbRes.T("All", "Dashboard"):DbRes.T("Subject", "Dashboard"),
                HasChildren = HasChildren(d.Type, doa),
                Type = d.Type,
                SubjectType = d.SdoSubjectType,
                SubjectId = d.SubjectId

            };


            return dvm;
        }

        private bool HasChildren(DashboardItemType itemType, Doa doa)
        {
            switch(itemType)
            {
                case DashboardItemType.SubjectList:
                    return true;
 //               case enDashboardItemTypes.SDO:
 //                   return doa.SDOSubjects.Any();               
//                case enDashboardItemTypes.InBox:
 //               case enDashboardItemTypes.OutBox:
                default:
                    return false;
                
                
            }
            
        }
    }
    
}
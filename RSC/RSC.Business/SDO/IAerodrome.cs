﻿using System.Collections.Generic;

namespace RSC.Business.SDO
{
    public interface IAerodrome
    {
        string Id { get; }
        string Designator { get; }
        IEnumerable<IRunway> Runways { get; }
        IRunway GetRunway(string designator);
        bool ContainsRunway(string designator);
    }
}

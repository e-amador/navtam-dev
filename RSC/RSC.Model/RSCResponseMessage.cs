﻿using RSC.Model.Enums;

namespace RSC.Model
{
    public class RSCResponseMessage
    {
        public int Number { get; set; }
        public string Description { get; set; }
        public string RunwayId { get; set; }
        public int? Third { get; set; }
        public ResponseStatus Status { get; set; }
    }
}

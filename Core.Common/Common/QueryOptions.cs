﻿using System;

namespace NavCanada.Core.Common.Common
{
    public class QueryOptions
    {
        public QueryOptions()
        {
            Page = 1;
            PageSize = 10;
            Sort = "id";
        }

        public int Page { get; set; }
        public int PageSize { get; set; }

        /// <summary>
        /// field name separated by columns. For descending sorting append _ in from of the field name
        /// example: "id,_firtname" will sort by id asc and then firstname descending 
        /// </summary>
        public string Sort { get; set; }

        /// <summary>
        /// field name you want to filter on. Adding * will filter in all posible columns
        /// </summary>
        public string FilterField { get; set; }

        /// <summary>
        /// value that will be applied to the filter
        /// </summary>
        public string FilterValue { get; set; }
    }
}

﻿using Business.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NDS.Relay;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NAVTAM.Web.Test.AFTN
{
    [TestClass]
    public class AftnQueryTests
    {
        [TestMethod]
        public void Relay_AftnQueryRange_test()
        {
            // same year from 1 to 100
            var range1 = new AftnQueryRange("A", 1, 100, 2017);
            Assert.IsTrue(range1.IsValid());

            // incorrect number order
            var range4 = new AftnQueryRange("A", 100, 1, 217);
            Assert.IsFalse(range4.IsValid());
        }

        [TestMethod]
        public void Relay_AftnQuery_ParseRqnQuery_test()
        {
            // empty query produces no ranges
            Assert.AreEqual(0, AftnQueryUtils.ParseRqnQuery("").Count());

            // invalid single range
            Assert.IsFalse(AftnQueryUtils.ParseRqnQuery("A0001/18-A0012/17").All(q => q.IsValid()));

            // valid multiple ranges
            var queryRanges1 = AftnQueryUtils.ParseRqnQuery("A0001/17-A0012/17 A0100/17 A0101/17-A0112/17").ToList();
            Assert.AreEqual(queryRanges1.Count, 3);
            Assert.IsTrue(queryRanges1.AreValid());

            // multiple queries with invalid range
            var queryRanges2 = AftnQueryUtils.ParseRqnQuery("A0001/17-A0012/17 GARBAGE A0101/17-A0112/17").ToList();
            Assert.IsFalse(queryRanges2.AreValid());
            Assert.AreEqual(queryRanges2.Count, 3);
            Assert.AreEqual(2, queryRanges2.Count(q => q.IsValid()));
            Assert.AreEqual(null, queryRanges2[1]);

            var queryRanges4 = AftnQueryUtils.ParseRqnQuery("A0001/17-A0012/17      A0101/17-A0112/17").ToList();
            Assert.AreEqual(queryRanges4.Count, 2);
            Assert.AreEqual(2, queryRanges4.Count(q => q.IsValid()));

            var queryRanges5 = AftnQueryUtils.ParseRqnQuery("A0001/17-A0012/17 A0101/17-A0112/17\r\n").ToList();
            Assert.AreEqual(queryRanges5.Count, 2);
            Assert.AreEqual(2, queryRanges5.Count(q => q.IsValid()));
        }

        [TestMethod]
        public void Relay_AftnQueryRange_CalculateTotalNotams_test()
        {
            var singleRange = new List<AftnQueryRange> { new AftnQueryRange("A", 10, 10, 2017) };
            Assert.AreEqual(singleRange.CalculateTotalNotams(), 1);

            var sameYearRange = new List<AftnQueryRange> { new AftnQueryRange("A", 1, 100, 2017) };
            Assert.AreEqual(sameYearRange.CalculateTotalNotams(), 100);

            var manyYearRanges = new List<AftnQueryRange>
            {
                new AftnQueryRange("A", 1, 100, 2017),
                new AftnQueryRange("A", 1, 100, 2018),
                new AftnQueryRange("A", 1, 100, 2019)
            };
            Assert.AreEqual(manyYearRanges.CalculateTotalNotams(), 300);
        }

        [TestMethod]
        public void Relay_AftnQueryUtils_ExecuteRqnQueries_too_many_notams_test()
        {
            var ranges = new List<AftnQueryRange> { new AftnQueryRange("A", 1, 101, 2017) };
            var results = AftnQueryUtils.ExecuteRqnQueries(ranges, r => GetSampleResults()).ToList();

            Assert.AreEqual(results.Count, 1);
            Assert.AreEqual(results[0].ResultType, AftnRqnResultType.LimitReached);
        }

        [TestMethod]
        public void Relay_AftnQueryUtils_ExecuteRqnQueries_one_found_test()
        {
            var ranges = new List<AftnQueryRange> { new AftnQueryRange("A", 1, 10, 2017) };
            var results = AftnQueryUtils.ExecuteRqnQueries(ranges, r => GetSampleResults()).ToList();

            Assert.AreEqual(results.Count, 2);
            Assert.AreEqual(results[0].ResultType, AftnRqnResultType.Active);
            Assert.AreEqual(results[1].ResultType, AftnRqnResultType.Missing);
        }

        [TestMethod]
        public void Relay_AftnQueryUtils_ExecuteRqnQueries_multiple_ranges_test()
        {
            var ranges = new List<AftnQueryRange>
            {
                new AftnQueryRange("A", 1, 10, 2017),
                new AftnQueryRange("B", 1, 10, 2018),
            };
            var results = AftnQueryUtils.ExecuteRqnQueries(ranges, r => GetSampleResults(r.Series, r.Year, r.FromNumber)).ToList();

            Assert.AreEqual(results.Count, 3);
            Assert.AreEqual(results[0].ResultType, AftnRqnResultType.Active);
            Assert.AreEqual(results[1].ResultType, AftnRqnResultType.Active);
            Assert.AreEqual(results[2].ResultType, AftnRqnResultType.Missing);
        }

        [TestMethod]
        public void Relay_AftnQueryUtils_ExecuteRqnQueries_no_longer_in_db_test()
        {
            var ranges = new List<AftnQueryRange>
            {
                new AftnQueryRange("E", 1, 1, 2018),
            };

            var notam = new Notam()
            {
                Series = "E",
                Number = 1,
                Year = 2018,
                EffectiveEndValidity = DateTime.UtcNow.AddDays(-95)
            };
            var rqnResult = new RqnNotamResultDto() { Notam = notam };
            var results = AftnQueryUtils.ExecuteRqnQueries(ranges, r => rqnResult.Yield()).ToList();

            Assert.AreEqual(results.Count, 1);
            Assert.AreEqual(results[0].ResultType, AftnRqnResultType.Obsolete);

            notam = new Notam()
            {
                Series = "E",
                Number = 1,
                Year = 2018,
                StartActivity = DateTime.UtcNow.AddDays(-100),
                EndValidity = DateTime.UtcNow.AddDays(-95),
            };
            rqnResult = new RqnNotamResultDto() { Notam = notam };
            results = AftnQueryUtils.ExecuteRqnQueries(ranges, r => rqnResult.Yield()).ToList();

            Assert.AreEqual(results.Count, 1);
            Assert.AreEqual(results[0].ResultType, AftnRqnResultType.Obsolete);
        }

        static IEnumerable<RqnNotamResultDto> GetSampleResults(string series = "A", int year = 2017, int number = 1)
        {
            yield return new RqnNotamResultDto()
            {
                Notam = new Notam() { Series = series, Number = number, Year = year, NotamId = ICAOTextUtils.FormatNotamId(series, number, year) },
                ReplaceNotamType = NotamType.N,
                ReplaceNotamId = null
            };
        }
    }
}

namespace NavCanada.Core.Data.NotamWiz.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRowVersionToProposalHistory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProposalHistory", "RowVersion", c => c.Binary());
            AddColumn("dbo.ProposalHistory", "PendingReview", c => c.Boolean(nullable: false));
            AlterColumn("dbo.ProposalHistory", "Originator", c => c.String(maxLength: 250));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ProposalHistory", "Originator", c => c.String(maxLength: 50));
            DropColumn("dbo.ProposalHistory", "PendingReview");
            DropColumn("dbo.ProposalHistory", "RowVersion");
        }
    }
}

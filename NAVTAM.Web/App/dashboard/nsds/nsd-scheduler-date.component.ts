﻿import { Component, OnInit, OnDestroy, AfterViewInit, HostListener, Input, ChangeDetectorRef } from '@angular/core'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators } from '@angular/forms'
import { NsdFormComponent } from '../nsds/nsd-form.component';
import { WindowRef } from '../../common/windowRef.service';

import { ProposalStatus, ProposalType } from '../shared/data.model';

import * as moment from 'moment';

declare var $: any;

@Component({
    selector: 'scheduler-date',
    templateUrl: '/app/dashboard/nsds/nsd-scheduler-date.component.html'
})
export class NsdSchedulerDateComponent implements OnInit, AfterViewInit, OnDestroy {

    public parent: NsdFormComponent = null;
    @Input('form') form: FormGroup;
    @Input('model') model: any;

    lastEventId: number = -1;
    currentEvent: any = null;
    data: any = null;

    calendar1: any;
    calendar2: any;
    calendar3: any;
    calendar4: any;

    SR = "06:00";
    SS = "18:00";

    start: any = null;
    end: any = null;
    startDate: any = null;
    starttime: string = null;
    endtime: string = null;
    initialized: boolean = false;

    slootDuration = 15;
    scrollTime = '06:00:00';
    loading: boolean = false; 

    eventsLinked = [];
    startLinking = false;

    editingExtendedHours : boolean = false;
    hasExtendedHours: boolean = false;
    isDeleteDetailEventDisabled: boolean = true;
    sheduleCleared: boolean = false;
    repeatToggle: boolean = false;   

    eventDetails = {
        starttime: '',
        endtime: '',
        s_sunrise: false,
        s_sunset: false,
        e_sunrise: false,
        e_sunset: false,
        extendedHours: false,
        exStarttime: '',
        exEndtime: ''
    }

    defaultColor = '#3a87ad';
    selectedColor = '#a1c5f761';

    eventColors = [
        { color: '#c9625f', used: false },
        { color: '#18a689', used: false },
        { color: '#6e62b5', used: false },
        { color: '#b66d39', used: false },
        { color: '#4a89dc', used: false }];

    currentEventColor: any = null;
    startEventUpdate: any = null;
    dateEventsDic = {};
    arrayOfKeys;
     
    public dailyRow: any = {
        starttime: "",
        endtime: ""
    }

    constructor(private windowRef: WindowRef, private cdRef: ChangeDetectorRef) {
        this.arrayOfKeys = Object.keys(this.dateEventsDic);
    }


    ngOnInit() {
    }

    ngOnDestroy() {
    }


    ngAfterViewInit() {
        let self = this;

        this.calendar1 = $('#calendar-1');
        this.calendar2 = $('#calendar-2');
        this.calendar3 = $('#calendar-3');
        this.calendar4 = $('#calendar-4');


        $('#d-modal-startime').mask('00:00', { onComplete: function (cep) { self.eventDetails.starttime = cep; } });
        $('#d-modal-endtime').mask('00:00', { onComplete: function (cep) { self.eventDetails.endtime = cep; } });
        $('#d-modal-midnight').mask('00:00', { onComplete: function (cep) { self.eventDetails.exEndtime = cep; } });
    }

    initialize(data, start, end, slotDuration?, events? ) {
        this.data = data;
        this.loading = data !== null;

        start = moment(start);
        end = moment(end);

        this.startDate = moment(start);

        this.initMasterCalendar(this.calendar1, this.calendar4, this.startDate, 1);
        this.initMasterCalendar(this.calendar2, this.calendar4, moment(this.startDate).add(1, 'M'), 2);
        this.initMasterCalendar(this.calendar3, this.calendar4, moment(this.startDate).add(2, 'M'), 3);
        this.initDetailCalendar(this.calendar4, this.startDate);

        if (this.data) {
            for (let i = 0; i < this.data.length; i++) {
                let obj = JSON.parse(this.data[i].event);

                let event = this.data[i].calendarId === 4 ? this.setDetailEvent(obj) : this.setPrimaryEvent(obj);
                switch (this.data[i].calendarId) {
                    case 1: this.calendar1.fullCalendar('renderEvent', event, true); break;
                    case 2: this.calendar2.fullCalendar('renderEvent', event, true); break;
                    case 3: this.calendar3.fullCalendar('renderEvent', event, true); break;
                    case 4: this.calendar4.fullCalendar('renderEvent', event, true); break;
                }
            }
            this.calendar1.fullCalendar('today');
        } else if (events) {
            for (let i = 0; i < events.length; i++) {
                let evt = this.setDetailEvent(events[i]);
                if (i === 0) {
                    start = evt.start;
                }
                this.calendar4.fullCalendar('renderEvent', evt, true);
            }
            this.calendar4.fullCalendar('gotoDate', start);
        } else {
            //trick to enforce refreshing the calendar when modifications were made with calendar hidden 
            this.calendar1.fullCalendar('prev');
            this.calendar1.fullCalendar('next');
            this.calendar2.fullCalendar('prev');
            this.calendar2.fullCalendar('next');
            this.calendar3.fullCalendar('prev');
            this.calendar3.fullCalendar('next');
            this.calendar4.fullCalendar('prev');
            this.calendar4.fullCalendar('next');
        }
   }

    toggleShedule(event, clearItemD: boolean = false) {
        let isPannelOpenNow = this.isPanelOpen();

        if (event) {
            this.repeatToggle = true;
            event.preventDefault();
        } else {
            this.repeatToggle = false;
        }

        if (!this.initialized) {
            this.slideToggle();
        }

        this.showSchedule(this.parent.type === 'clone' ? false : clearItemD);

        let isPannelOpenThen = this.isPanelOpen();

        if (isPannelOpenNow === isPannelOpenThen) {
            if (!isPannelOpenNow || !this.repeatToggle) {
                this.slideToggle();
            }
        }
    }

    deleteEvents(event, clearItemD: boolean = false) {
        if (event) {
            if (this.data && this.data.length > 0) {
                this.parent.resetSchedulersStatus();
            }
            event.preventDefault();
        }

        this.data = null;
        this.dateEventsDic = {};

        this.data = new Object();

        this.clearSchedules(clearItemD);

        if (clearItemD) {
            let enforceToggle = this.parent.type === 'clone' || this.parent.model.proposalId;
            this.parent.schedulerDaily.toggleScheduleAfterDeleteAction(enforceToggle, true);
            this.parent.schedulerDayWeeks.toggleScheduleAfterDeleteAction(enforceToggle, true);
        }

        this.parent.enableActivePeridDates();
        this.data = null;
    }

    hasEvents() {
        return !$.isEmptyObject(this.dateEventsDic);; 
    }

    isClearButtonDisabled() {
        if (this.parent && this.parent.isReadOnly) {
            return true;
        }

        if (this.hasEvents()) {
            return false;
        }

        if (this.calendar4) {
            var events = this.calendar4.fullCalendar('clientEvents');
            return events.length <= 0;
        }

        return true;
    }

    zoomIn() {
        if (this.slootDuration > 0) {
            this.slootDuration -= 1;

            let events = this.calendar4.fullCalendar('clientEvents');

            let start = this.calendar4.fullCalendar('getDate').clone();
            this.calendar4.fullCalendar('destroy');

            this.initDetailCalendar(this.calendar4, start);
            for (let i = 0; i < events.length; i++) {
                let evt = this.setDetailEvent(events[i]);
                this.calendar4.fullCalendar('renderEvent', evt, true);
            }
            this.calendar4.fullCalendar('gotoDate', start);
        }
    } 

    zoomOut() {
        if (this.slootDuration < 15) {
            this.slootDuration += 1;

            let events = this.calendar4.fullCalendar('clientEvents');

            let start = this.calendar4.fullCalendar('getDate').clone();
            this.calendar4.fullCalendar('destroy');

            this.initDetailCalendar(this.calendar4, start);
            for (let i = 0; i < events.length; i++) {
                let evt = this.setDetailEvent(events[i]);
                this.calendar4.fullCalendar('renderEvent', evt, true);
            }
            this.calendar4.fullCalendar('gotoDate', start);
        }
    }

    removeEvents() {
        if (this.hasEvents()) {
            this.calendar1.fullCalendar('removeEvents');
            this.calendar2.fullCalendar('removeEvents');
            this.calendar3.fullCalendar('removeEvents');
            this.calendar4.fullCalendar('removeEvents');

            this.data = null;
            this.dateEventsDic = {};
            this.toggleScheduleAfterDeleteAction(true, true);
        }
    } 

    clearSchedules(clearItemD: boolean) {
        if (this.data) {
            this.calendar1.fullCalendar('destroy');
            this.calendar2.fullCalendar('destroy');
            this.calendar3.fullCalendar('destroy');
            this.calendar4.fullCalendar('destroy');

            this.toggleScheduleAfterDeleteAction(true, this.initialized);

            let starttime = moment(this.starttime, 'HH:mm');
            let endtime = moment(this.endtime, 'HH:mm');

            if (this.start === null && this.end === null) {
                if (this.data && this.data.length > 0) {
                    let detailtEvent = this.data.find(x => x.calendarId === 4);
                    if (detailtEvent) {
                        let event = JSON.parse(detailtEvent.event);
                        if (event.start) {
                            this.start = moment(event.start);
                        }
                        if (event.end) {
                            this.end = moment(event.end);
                        }
                    }
                }
            }

            let start = this.start ? moment(this.start).set({ hour: starttime.get('hour'), minute: starttime.get('minute'), }) : moment();
            let end = this.end ? moment(this.end).set({hour: endtime.get('hour'),minute: endtime.get('minute'),}) : moment();

            this.initialize((this.parent.type === 'clone' || this.parent.model.proposalId) ? this.data : null, start, end);

            if (start.isValid()) {
                this.calendar4.fullCalendar('gotoDate', start);
            } else {
                this.calendar4.fullCalendar('gotoDate', moment());
            }
        } else {
            if (clearItemD) {
                if (this.parent.type === 'clone') {
                    this.toggleScheduleAfterDeleteAction(this.parent.model.proposalId ? true : false, this.initialized);
                } else {
                    this.toggleScheduleAfterDeleteAction(this.sheduleCleared, this.hasEvents());
                    this.sheduleCleared = false;
                }
            }
            this.initialize(null, moment(), moment());
        }
    }


    slideToggle() {
        $('#schedule-date-content').slideToggle();
    }

    isPanelOpen() {
        return $('#schedule-date-content').css('display') !== 'none';
    }

    toggleScheduleAfterDeleteAction(enforce: boolean, initialiazed: boolean) {
        if (enforce) {
            this.slideToggle();
            this.initialized = initialiazed;
        }
    }

    resetClearScheduleFlag() {
        this.sheduleCleared = true;
        this.repeatToggle = false;
    }

    closePanelIfOpen() {
        if ($('#schedule-date-content').css('display') !== 'none') {
            this.slideToggle();
        }
    }

    showSchedule(clearItemD: boolean) {
        this.clearSchedules(clearItemD);

        let elem = $('#calendar4 .fc-slats > td').width(10);
    }

    @HostListener('window:keydown', ['$event'])
    onKeydown($event: any) {
        if ($event.ctrlKey) {
            this.startLinking = true;
        }
    }
    @HostListener('window:keyup', ['$event'])
    onKeyup($event: any) {
        if ($event.charCode === 17) {
            this.startLinking = false;
        }
    }

    link() {
        if (this.startLinking == true) {
            return;
        }

        if (this.currentEvent) {
            let calendars = [this.calendar1, this.calendar2, this.calendar3];
            this.unselectEvents(calendars, -1);
            for (let i = 0; i < calendars.length; i++) {
                calendars[i].fullCalendar('rerenderEvents');
            }
        }

        this.eventsLinked = [];
        this.currentEventColor = this.getNextLinkColor();
        this.startLinking = true;
    }

    unlink() {
        if (this.currentEvent && this.currentEvent.event.linked && this.eventsLinked.length < 1) {
            const calendars = [this.calendar1, this.calendar2, this.calendar3];
            for (let i = 0; i < calendars.length; i++) {
                let events = calendars[i].fullCalendar('clientEvents');
                for (let j = 0; j < events.length; j++) {
                    if (events[j].linked && events[j].groupId === this.currentEvent.event.groupId) {
                        //this.deleteLinkedDetailsEvents(events[j]);
                        this.unLinkedDetailsEvents();
                        this.unlikEvent(calendars[i], events[j]);
                    }
                }
                calendars[i].fullCalendar('rerenderEvents');
            }
        } else {
            if (this.eventsLinked.length === 1) {
                let calendar = this.eventsLinked[0].calendar;
                let event = this.eventsLinked[0].event;
                //this.deleteLinkedDetailsEvents(event);
                this.unLinkedDetailsEvents();
                this.unlikEvent(calendar, event);

                calendar.fullCalendar('rerenderEvents');
            }
        }

        this.calendar4.fullCalendar('rerenderEvents');

        this.startLinking = false;
        this.currentEvent = null;
        this.eventsLinked = [];

    }

    deleteEvent() {
        if (!this.currentEvent) {
            return;
        }

        const event = this.currentEvent.event;
        const calendar = this.currentEvent.calendar;

        let eventId = event.id || event._id;
        if (event.linked) {
            if (this.eventsLinked.length === 1) {
                calendar.fullCalendar('removeEvents', eventId);
                this.deleteLinkedDetailsEvents(event);
                calendar.fullCalendar('rerenderEvents');
            } else {
                if (this.eventsLinked.length <= 0 && event.linked) {
                    const calendars = [this.calendar1, this.calendar2, this.calendar3];
                    for (let i = 0; i < calendars.length; i++) {
                        let events = calendars[i].fullCalendar('clientEvents');
                        for (let j = 0; j < events.length; j++) {
                            if (events[j].linked && events[j].groupId === event.groupId) {
                                let id = events[j].id || events[j]._id;
                                calendars[i].fullCalendar('removeEvents', id);
                                this.deleteLinkedDetailsEvents(events[j]);
                            }
                        }
                        calendars[i].fullCalendar('rerenderEvents');
                    }
                }
            }
            this.startLinking = false;
            this.eventsLinked = [];
        } else {
            calendar.fullCalendar('removeEvents', eventId);
            this.deleteLinkedDetailsEvents(event);
            calendar.fullCalendar('rerenderEvents');
        }

        this.currentEvent = null;
    }

    isLinkButtonDisabled() {
        if (this.currentEvent && this.currentEvent.event.linked || this.model.isReadOnly) {
            return true;
        }
        return false;
    }

    isUnlinkButtonDisabled() {
        if (!this.currentEvent && !this.startLinking || this.model.isReadOnly) {
            return true;
        }

        if (this.currentEvent && !this.currentEvent.event.linked || this.model.isReadOnly) {
            return true;
        }
        return false;
    }

    isDeleteButtonDisabled() {
        if (this.model.isReadOnly)
            return true;

        return !this.currentEvent;        
    }

    checkboxChanged(control: string, e: any) {
        switch (control) {
            case 's_sunrise':
                this.eventDetails.s_sunrise = !this.eventDetails.s_sunrise;
                this.eventDetails.s_sunset = false;
                if (this.eventDetails.s_sunrise) {
                    this.eventDetails.e_sunrise = false;
                    this.eventDetails.starttime = this.SR;
                    if (!this.eventDetails.e_sunrise && !this.eventDetails.e_sunset) {
                        if (this.eventDetails.endtime) {
                            let hours = this.eventDetails.endtime.substring(0, 2);
                            if (parseInt(hours) <= 6) {
                                this.eventDetails.endtime = "07:00";
                            }
                        } else {
                            this.eventDetails.endtime = "07:00";
                        }
                    }
                }
                break;
            case 's_sunset':
                this.eventDetails.s_sunset = !this.eventDetails.s_sunset;
                this.eventDetails.s_sunrise = false;
                if (this.eventDetails.s_sunset) {
                    this.eventDetails.e_sunrise = false;
                    this.eventDetails.e_sunset = false;
                    this.eventDetails.starttime = this.SS;
                    if (!this.eventDetails.e_sunrise && !this.eventDetails.e_sunset) {
                        if (this.eventDetails.endtime) {
                            let hours = this.eventDetails.endtime.substring(0, 2);
                            if (parseInt(hours) <= 18) {
                                this.eventDetails.endtime = "22:00";
                            }
                        } else {
                            this.eventDetails.endtime = "22:00";
                        }
                    }
                }
                break;
            case 'e_sunrise':
                this.eventDetails.e_sunrise = !this.eventDetails.e_sunrise;
                this.eventDetails.e_sunset = false;
                if (this.eventDetails.e_sunrise) {
                    this.eventDetails.s_sunrise = false;
                    this.eventDetails.s_sunset = false;
                    this.eventDetails.extendedHours = false;
                    this.eventDetails.exStarttime = "";
                    this.eventDetails.exEndtime = "";
                    this.eventDetails.endtime = this.SR;
                    if (!this.eventDetails.s_sunrise && !this.eventDetails.s_sunset) {
                        if (this.eventDetails.starttime) {
                            let hours = this.eventDetails.starttime.substring(0, 2);
                            if (parseInt(hours) >= 6) {
                                this.eventDetails.starttime = "04:00";
                            }
                        } else {
                            this.eventDetails.starttime = "04:00";
                        }
                    }
                }
                break;
            case 'e_sunset':
                this.eventDetails.e_sunset = !this.eventDetails.e_sunset;
                this.eventDetails.e_sunrise = false;
                if (this.eventDetails.e_sunset) {
                    this.eventDetails.extendedHours = false;
                    this.eventDetails.exStarttime = "";
                    this.eventDetails.exEndtime = "";
                    this.eventDetails.s_sunset = false;
                    this.eventDetails.endtime = this.SS;
                    if (!this.eventDetails.s_sunrise && !this.eventDetails.s_sunset) {
                        if (this.eventDetails.starttime) {
                            let hours = this.eventDetails.starttime.substring(0, 2);
                            if (parseInt(hours) >= 18) {
                                this.eventDetails.starttime = "13:00";
                            }
                        } else {
                            this.eventDetails.starttime = "13:00";
                        }
                    }
                }
                break;
        }
    }

    mergeRanges(r1, r2) {
        if (r1 && r2) {
            return {
                month: r1.month,
                from: r1.from,
                to: r2.to
            };
        }
        return null;
    }

    static printDay(d) {
        return d < 10 ? `0${d}` : `${d}`;
    }

    static serializeFromToRange(r) {
        const from = NsdSchedulerDateComponent.printDay(r.from);
        const to = NsdSchedulerDateComponent.printDay(r.to);
        return from !== to ? `${from}-${to}` : `${from}`;
    }

    serializeDayRange(r) {
        if (!r) return "";

        let range = NsdSchedulerDateComponent.serializeFromToRange(r);

        let other = r.other ? r.other.map(NsdSchedulerDateComponent.serializeFromToRange).join(' ') : null; 
        if (other) range += ` ${other}`

        return `${r.month} ${range}`;
    }

    parseDayRange(range: string) {
        let cols = range.split(' ');
        if (cols.length !== 2)
            return null;

        let month = cols[0];
        let days = cols[1].split('-');

        if (days.length !== 2)
            return null;

        return {
            month: cols[0],
            from: parseInt(days[0]),
            to: parseInt(days[1])
        };
    }

    areConsecutiveRanges(r1, r2) {
        return r1 && r2 && r1.month === r2.month && r1.to + 1 === r2.from;
    }

    timeRangesMatch(list1, list2) {
        if (list1 && list2 && list1.length && list1.length === list2.length) {
            let len = list1.length;
            for (var i = 0; i < len; i++) {
                if (list1[i].start !== list2[i].start || list1[i].end !== list2[i].end)
                    return false;
            }
            return true;
        }
        return list1 === list2;
    }

    mapKeyToRange(dict, key) {
        return {
            range: this.parseDayRange(key),
            values: dict[key]
        };
    }

    compactKeys(dict) {
        let self = this;
        let mapped = Object.keys(dict).map((k) => self.mapKeyToRange(dict, k));

        let count = mapped.length;
        if (count === 0)
            return dict;

        let last = mapped[0];
        let zipped = [ last ];

        for (var i = 1; i < count; i++) {
            if (this.areConsecutiveRanges(last.range, mapped[i].range) && this.timeRangesMatch(last.values, mapped[i].values)) {
                last.range = this.mergeRanges(last.range, mapped[i].range);
            } else {
                last = mapped[i];
                zipped.push(last);
            }
        }

        let merged = [];
        for (let i = 0; i < zipped.length; i++) {
            const zi: any = zipped[i];
            if (zi.merged === undefined) {
                zi.merged = true;
                merged.push(zi);
                zi.range.other = [];
                for (let j = i + 1; j < zipped.length; j++) {
                    const zj: any = zipped[j];
                    if (zi.range.month === zj.range.month && this.timeRangesMatch(zi.values, zj.values)) {
                        zi.range.other.push(zj.range);
                        zj.merged = true;
                    }
                }
            }
        }

        zipped = merged;

        let result = {};
        for (var k = 0; k < zipped.length; k++) {
            let key = this.serializeDayRange(zipped[k].range);
            result[key] = zipped[k].values;
        }

        return result;
    }

    tryAddRange(ranges: any[], range) {
        if (!ranges.find(r => r.start === range.start && r.end === range.end)) {
            ranges.push(range);
        }
    }

    parseEvents(toggle: boolean = false) {
        let proposalId = -1;
        if (this.data && this.data.length > 0) {
            proposalId = this.data[0].proposalId;
            this.data = [];
        }

        let parentEvents = [];
        let calendars = [this.calendar1, this.calendar2, this.calendar3];
        for (let i = 0; i < calendars.length; i++) {
            let events = calendars[i].fullCalendar('clientEvents');

            if (events.length > 0) {
                if (proposalId >= 0) {
                    this.resetMasterDataEvents(proposalId, i + 1, events);
                }
                for (let j = 0; j < events.length; j++) {
                    events[j].end = events[j].end ? events[j].end : events[j].start.clone();
                    parentEvents.push(events[j]);
                }
            }
        }

        let detailsEvents = this.calendar4.fullCalendar('clientEvents');

        parentEvents = parentEvents.sort(this.sortEvents);
        detailsEvents = detailsEvents.sort(this.sortEvents);

        if (proposalId >= 0) {
            this.resetDetailsDataEvents(proposalId, 4, detailsEvents);
        }

        this.dateEventsDic = {};
        for (let i = 0; i < parentEvents.length; i++) {
            let parentEvent = parentEvents[i];

            let startEvent = parentEvent.start.clone();
            let endEvent = parentEvent.extendedHours ? parentEvent.end.clone().add("days", -2) : parentEvent.end.clone().add("days", -1);

            let start = this.setMomentDate(startEvent, 'MMM DD').toUpperCase();
            let end = this.setMomentDate(endEvent, 'MMM DD').toUpperCase();

            let dateRange = `${start}-${end}`;

            let details = this.getDetailsEvent(parentEvent, detailsEvents);

            for (let k = 0; k < details.length; k++) {
                this.updateDataEvent(details[k]);

                if (details[k].extendedHours && details[k].startTime === "00:00") {
                    continue;
                }

                let value = {
                    startEvent: details[k].start,
                    endEvent: details[k].end,
                    start: details[k].startTime,
                    end: details[k].endTime === "SS" ? details[k].endTime : (details[k].extendedHours ? details[k].exEndtime : details[k].endTime),
                }


                let range = dateRange.split('-');
                let key = range[0].substring(0, 3) + " " + range[0].substring(3).trim() + "-" + range[1].substring(3).trim();

                let ranges = this.dateEventsDic[key];
                if (!ranges) {
                    this.dateEventsDic[key] = ranges = [];
                }
                this.tryAddRange(ranges, value);
            }
            this.arrayOfKeys = Object.keys(this.dateEventsDic);
        }

        // compact events
        this.dateEventsDic = this.compactKeys(this.dateEventsDic); //let test = this.compactKeys(this.dateEventsDic);
        this.arrayOfKeys = Object.keys(this.dateEventsDic);

        let itemD = "";
        let keys = Object.keys(this.dateEventsDic);
        for (let i = 0; i < keys.length; i++) {
            itemD = itemD + keys[i];
            for (let j = 0; j < this.dateEventsDic[keys[i]].length; j++) {
                let starttime = this.dateEventsDic[keys[i]][j].start;
                let endtime = this.dateEventsDic[keys[i]][j].end;

                itemD = itemD + ` ${starttime.replace(':', '')}-${endtime.replace(':', '')}`;
            }
            let comma = i < keys.length - 1 ? ', ' : ' ';
            itemD = itemD + comma;
        }

        itemD = itemD.trim(); 

        if (parentEvents.length > 0 && detailsEvents.length > 0) {
            let startDate = parentEvents[0].start.clone();
            let endDate = parentEvents[parentEvents.length - 1].end.clone().add("days", -1);

            let len = Object.keys(this.dateEventsDic).length - 1;

            let start = startDate.format('YYMMDD');
            let end = endDate.format('YYMMDD')

            this.starttime = this.dateEventsDic[Object.keys(this.dateEventsDic)[0]][0].start;
            this.endtime = this.dateEventsDic[Object.keys(this.dateEventsDic)[len]][this.dateEventsDic[Object.keys(this.dateEventsDic)[len]].length - 1].end;
            
            let startEvent = this.dateEventsDic[Object.keys(this.dateEventsDic)[0]][0].startEvent;
            let endEvent = this.dateEventsDic[Object.keys(this.dateEventsDic)[len]][this.dateEventsDic[Object.keys(this.dateEventsDic)[len]].length - 1].endEvent;

            let sTime = (this.starttime === "SR" ? this.getSunriseTime(startEvent) : (this.starttime === "SS" ? this.getSunsetTime(startEvent) : this.starttime));
            let eTime = (this.endtime === "SR" ? this.getSunriseTime(endEvent) : (this.endtime === "SS" ? this.getSunsetTime(endEvent) : this.endtime));

            start = this.setMomentDateTime(startDate, sTime);
            end = this.setMomentDateTime(endDate, eTime);

            this.parent.disableActivePeridDates(start, end, itemD);
        }

        if (toggle) {
            this.slideToggle();
            //this.toggleShedule(null, true);
        }
    }

    slideScheduleClose() {
        this.slideToggle();
        this.initialized = true;
    }

    isModalOkButtonDisabled() {
        if (this.parent && this.parent.isReadOnly) {
            return true;
        }

        if (this.editingExtendedHours) {
            return true;
        }

        let _start = moment(this.eventDetails.starttime, "HH:mm");
        let _end = moment(this.eventDetails.endtime, "HH:mm");

        if (!_start.isValid() || !_end.isValid())
            return true;

        return false;
    } 

    isModalDeleteButtonDisabled() {
        if (this.parent && this.parent.isReadOnly) {
            return true;
        }

        if (this.isDeleteDetailEventDisabled) {
            return true;
        }

        if (this.editingExtendedHours) {
            return true;
        }

        return false;
    } 

    getCalendarEvents(calendarId) {
        switch (calendarId){
            case 1: return this.calendar1.fullCalendar('clientEvents');
            case 2: return this.calendar2.fullCalendar('clientEvents');
            case 3: return this.calendar3.fullCalendar('clientEvents');
            case 4: return this.calendar4.fullCalendar('clientEvents');
        }

        return null;
    }

    private getSunriseTime(date: string) {
        let time = moment(date).format('HH:mm')

        return time !== this.SR ? time : this.SR;
    }

    private getSunsetTime(date: string) {
        let time = moment(date).format('HH:mm')

        return time !== this.SS ? time : this.SS;
    }

    private destroyAllCalendars() {
        this.calendar1.fullCalendar('destroy');
        this.calendar2.fullCalendar('destroy');
        this.calendar3.fullCalendar('destroy');
        this.calendar4.fullCalendar('destroy');
    }

    private setMomentDate(date, format) {
        let day = date.format('DD');
        let month = date.format('MM');
        let year = date.format('YYYY');

        return moment(`${day}-${month}-${year}`, 'DD-MM-YYYY').format(format);
    }

    private setMomentDateTime(date, time) {
        let day = date.format('DD');
        let month = date.format('MM');
        let year = date.format('YYYY');

        return moment.utc(`${day}-${month}-${year} ${time}`, 'DD-MM-YYYY HH:mm');
    }

    private sortEvents(event1, event2) {
        var start1 = new Date(event1.start).getTime();
        var start2 = new Date(event2.start).getTime();
        return start1 > start2 ? 1 : -1;
    };

    private getDetailsEvent(event, detailEvents) {
        let events = []
        for (let i = 0; i < detailEvents.length; i++) {
            if (event.id === detailEvents[i].parentId || event.groupId === detailEvents[i].groupId) {
                let startEvent = detailEvents[i].start.clone();
                let endEvent = detailEvents[i].end.clone();

                let startHour = startEvent.format('HH');
                let startMinute = startEvent.format('mm');

                let endHour = endEvent.format('HH');
                let endMinute = endEvent.format('mm');

                let start = null;
                let end = null;
                if (detailEvents[i].s_sunrise) {
                    start = "SR";
                }
                else if (detailEvents[i].s_sunset) {
                    start = "SS";
                }
                if (detailEvents[i].e_sunrise) {
                    end = "SR";
                }
                else if (detailEvents[i].e_sunset) {
                    end = "SS";
                }

                start = start ? start : moment(`${startHour}:${startMinute}`, 'HH:mm').format('HH:mm');
                end = end ? end : moment(`${endHour}:${endMinute}`, 'HH:mm').format('HH:mm');

                events.push({
                    id: detailEvents[i]._id || detailEvents[i].id,
                    startTime: start,
                    endTime: end,
                    start: detailEvents[i].start,
                    end: detailEvents[i].end,
                    s_sunrise: detailEvents[i].s_sunrise,
                    e_sunrise: detailEvents[i].e_sunrise,
                    s_sunset: detailEvents[i].s_sunset,
                    e_sunset: detailEvents[i].e_sunset,
                    extendedHours: detailEvents[i].extendedHours,
                    exStarttime: detailEvents[i].exStarttime,
                    exEndtime: detailEvents[i].exEndtime
                });
            }
        }

        return events.length > 0 ? this.removeDuplicates(events) : events;
    }

    private removeDuplicates(myArr) {
        var props = Object.keys(myArr[0])
        return myArr.filter((item, index, self) =>
            index === self.findIndex((t) => (
                props.every(prop => {
                    return t[prop] === item[prop]
                })
            ))
        )
    }

    private onExtendedHours() {
        this.eventDetails.extendedHours = !this.eventDetails.extendedHours;
        if (!this.eventDetails.extendedHours) {
            this.eventDetails.exStarttime = "";
            this.eventDetails.exEndtime = "";
        } else {
            this.eventDetails.endtime = "23:59";
            this.eventDetails.e_sunrise = false;
            this.eventDetails.e_sunset = false;
            this.eventDetails.exStarttime = "00:00";
        }
    }

    private initMasterCalendar(masterCalendar: any, detailCalendar: any, currentDate: any, calendarId: number) {
        const self = this;

        let header = {
            left: 'title',
            center: '',
            right: ''
        }
        if (masterCalendar === self.calendar1) {
            header.center = 'today';
            header.right = 'prev,next';
        }

        masterCalendar.fullCalendar({
            defaultDate: currentDate,
            firstDay: 1,
            firstHour: 1,
            allDayDefault: true,
            defaultView: 'month',
            height: 'auto',
            eventColor: this.defaultColor,
            selectable: true,
            editable: false,
            selectHelper: true,
            titleFormat: "MMM YYYY",
            fixedWeekCount: false,
            singleEventDateSelected: null,
            header: header,
            eventOverlap: false,
            events: [],
            calendarId: calendarId,
            displayEventTime: false,
            dayRender: function (event, element) {
            },
            select: function (start, end, event, view) {
                //if (self.currentEvent) {
                //    self.currentEvent = null;
                //    self.calendar4.fullCalendar('gotoDate', moment(start));
                //    return;
                //}

                if (masterCalendar === self.calendar1 && !self.IsValidDate(start, start)) {
                    return false;
                }

                if (!self.parent.isReadOnly && !self.isOverlapping(masterCalendar, { start: start, end: end })) {


                    if (masterCalendar.singleEventDateSelected) { //single event
                        masterCalendar.singleEventDateSelected = null;
                    } else { //multiple events
                        masterCalendar.singleEventDateSelected = null;
                    }

                    self.calendar4.fullCalendar('gotoDate', start);

                    self.lastEventId = ((masterCalendar.fullCalendar('option', 'calendarId') - 1) * 100) + view.calendar.getEventCache().length + 1;
                    let groupId = self.eventsLinked.length > 0 ? self.eventsLinked[0].event.groupId : self.lastEventId;

                    let eventData = {
                        id: self.lastEventId,
                        start: start,
                        end: end,
                        allDay: true,
                        stick: true,
                        groupId: groupId,
                        linked: self.startLinking,
                        selected: false,
                        eventColor: self.startLinking ? self.currentEventColor.color : self.defaultColor
                    };
                    if (self.startLinking) {
                        self.eventsLinked.push({ calendar: masterCalendar, event: eventData });
                    }
                    masterCalendar.fullCalendar('renderEvent', eventData, true); // stick? = true

                    self.parent.hasScheduler = true;
                }

                masterCalendar.fullCalendar('unselect');
            },
            eventClick: function (event, jsEvent, view) {
                let eventClicked = {
                    calendar: masterCalendar, event: {
                        id: event.id,
                        start: event.start,
                        end: event.end,
                        groupId: event.groupId,
                        linked: event.linked,
                        selected: true,
                        eventColor: event.eventColor,
                        previousColor: null
                    }
                };

                if (self.startLinking && !event.linked) {

                    let details = self.calendar4.fullCalendar('clientEvents');
                    for (let i = 0; i < details.length; i++) {
                        if (details[i].parentId === event.id) {
                            return;
                        }
                    }

                    event.linked = true;
                    eventClicked.event.linked = true;
                    if (self.eventsLinked.length > 0) {
                        event.eventColor = self.eventsLinked[0].event.eventColor;
                    } else {
                        event.eventColor = self.getNextLinkColor().color;
                    }
                    if (self.eventsLinked.length > 0) {
                        event.groupId = self.eventsLinked[0].event.groupId;
                    }
                    self.eventsLinked.push({ calendar: masterCalendar, event: event });
                    masterCalendar.fullCalendar('rerenderEvents');
                } else {
                    event.selected = true;
                    self.unselectEvents([self.calendar1, self.calendar2, self.calendar3], event.id);
                    if (self.currentEvent && !self.currentEvent.event.linked) {
                        //self.unselectEvents([self.calendar1, self.calendar2, self.calendar3], event.id);
                        event = self.selectCurrentEvent(event);
                    } else {
                        if (event.linked) {
                            let linkEvents = self.getLinkEvents(event, [self.calendar1, self.calendar2, self.calendar3], []);
                            for (let i = 0; i < linkEvents.length; i++) {
                                linkEvents[i].eventColor = event.eventColor;
                                linkEvents[i].previousColor = self.selectedColor;
                            }
                        }
                        event = self.selectCurrentEvent(event);

                        self.eventsLinked = [];
                        self.startLinking = false;
                    }

                    eventClicked.event.eventColor = event.eventColor;
                    eventClicked.event.previousColor = event.previousColor;
                    eventClicked.event.selected = true;

                    const calendars = [self.calendar1, self.calendar2, self.calendar3];
                    for (let i = 0; i < calendars.length; i++) {
                        calendars[i].fullCalendar('rerenderEvents');
                    }
                }

                detailCalendar.fullCalendar('gotoDate', event.start);

                self.currentEvent = eventClicked;
                self.lastEventId = event.id;
            },
            dayClick: function (date, event, view) {

                if (self.currentEvent) {
                    let event = self.currentEvent.calendar.fullCalendar('clientEvents', self.currentEvent.event.id)[0];
                    if (event) {
                        let color = event.eventColor;
                        event.eventColor = event.previousColor;
                        event.previousColor = color;

                        self.currentEvent.calendar.fullCalendar('rerenderEvents');
                    }
                }

                let events = masterCalendar.fullCalendar('clientEvents');
                for (let i = 0; i < events.length; i++) {
                    let startDay = parseInt(events[i].start.format("DD"));
                    let endDay = new Date(events[i].end.toDate()).getDate();
                    if (endDay < startDay) {
                        endDay = 31;
                    }
                    let clickDay = parseInt(date.format("DD"));

                    if (clickDay >= startDay && clickDay <= endDay) {
                        detailCalendar.fullCalendar('gotoDate', date);
                        return false;
                    }
                };

                if (masterCalendar === self.calendar1 && !self.IsValidDate(date, moment())) {
                    $(event.target).removeClass("fc-highlight");
                    return false;
                }

                masterCalendar.singleEventDateSelected = parseInt(date.format('D'));
                detailCalendar.fullCalendar('gotoDate', date);
            },
            eventRender: function (event, element, view) {
                if (self.parent && self.parent.isReadOnly) {
                    event.editable = false;
                }

                element.css('background-color', event.eventColor);
                element.css('border-color', event.eventColor);
            },
            eventAfterRender: function (event, element, view) {
                detailCalendar.fullCalendar('gotoDate', event.start);
            },
            viewRender: function (view, element) {
                let currentdate = view.intervalStart;
                if (masterCalendar === self.calendar1) {
                    self.calendar2.fullCalendar('gotoDate', view.end);
                    self.calendar3.fullCalendar('gotoDate', moment(view.end).add(1, 'M'));
                }
            },
        });
    }

    private initDetailCalendar(detailCalendar: any, currentDate: any) {
        const self = this;

        let duration = `00:${this.windowRef.zeroPad(this.slootDuration, 2)}:00`

        detailCalendar.fullCalendar({
            defaultDate: moment(currentDate),
            defaultView: 'agendaDay',
            contentHeight: 872,
            slotDuration: duration,
            allDaySlot: false,
            selectable: true,
            editable: true,
            displayEventTime: false,
            droppable: true, // this allows things to be dropped onto the calendar !!!
            eventLimit: true, // allow "more" link when too many events, 
            fixedWeekCount: false,
            eventOverlap: false,
            header: {
                left: 'title',
                center: '',
                right: 'prev,next'
            },
            axisFormat: 'HH:mm',
            timeFormat: 'HH:mm{ - HH:mm}',
            //maxTime: "23:59:00",
            minTime: 0,
            eventDragStart: function (event, jsEvent, ui, view) {
                self.startEventUpdate = {
                    start: event.start.clone(),
                    end: event.end.clone()
                };
            },
            eventDragStop: function (event, jsEvent, ui, view) {
            },
            eventDrop: function (event, delta, revertFunc, jsEvent, ui, view) {
                var isSameDay = event.start.isSame(event.end, 'day');
                if (!isSameDay) { // crossed midnight
                    revertFunc();
                }

                let prevStartTime = self.startEventUpdate.start.format('hh:mm');
                let prevEndTime = self.startEventUpdate.end.format('hh:mm');

                let events = detailCalendar.fullCalendar('clientEvents');
                for (let i = 0; i < events.length; i++) {
                    let startTime = events[i].start.format('hh:mm');
                    let endTime = events[i].end.format('hh:mm');
                    if (event._id !== events[i]._id) {
                        if ((events[i].parentId === event.parentId || events[i].groupId === event.groupId) && prevStartTime === startTime && prevEndTime === endTime) {
                            events[i].start = events[i].start.clone().add(delta._milliseconds, 'milliseconds');
                            events[i].end = events[i].end.clone().add(delta._milliseconds, 'milliseconds');
                            detailCalendar.fullCalendar('updateEvent', events[i]);
                        }
                    }
                }

                self.startEventUpdate = null;
            },
            eventResizeStart: function (event, jsEvent, ui, view) {
                self.startEventUpdate = {
                    start: event.start.clone(),
                    end: event.end.clone()
                };
            },
            eventResizeStop: function (event, jsEvent, ui, view) {
            },
            eventResize: function (event, delta, revertFunc, jsEvent, ui, view) {
                if (self.isValidStartEnd(event.start, event.end)) {
                    let prevStartTime = self.startEventUpdate.start.format('hh:mm');
                    let events = detailCalendar.fullCalendar('clientEvents');
                    for (let i = 0; i < events.length; i++) {
                        let startTime = events[i].start.format('hh:mm');
                        //if (event._id === events[i]._id) {
                        //    events[i].end = events[i].end.clone().add(delta._milliseconds, 'milliseconds');
                        //    detailCalendar.fullCalendar('updateEvent', events[i]);
                        if (event._id !== events[i]._id) {
                            if ((events[i].parentId === event.parentId || events[i].groupId === event.groupId) && prevStartTime === startTime) {
                                events[i].end = events[i].end.clone().add(delta._milliseconds, 'milliseconds');
                                detailCalendar.fullCalendar('updateEvent', events[i]);
                            }
                        }
                    }
                } else {
                    revertFunc();
                }

                self.startEventUpdate = null;
            },
            select: function (start, end, event, view) {
                if (self.startLinking) {
                    self.unlink();
                }

                if (self.lastEventId < 0) {
                    $(event.target).removeClass("fc-highlight");
                    return false;
                };

                if (!self.parent.isReadOnly && !self.isOverlapping(self.calendar4, { start: start, end: end }) && self.isValidStartEnd(start, end)) {
                    const calendars = [self.calendar1, self.calendar2, self.calendar3];
                    let eventsLinked = [];
                    for (let c = 0; c < calendars.length; c++) {
                        let events = calendars[c].fullCalendar('clientEvents');
                        for (let i = 0; i < events.length; i++) {
                            let id = events[i].id || events[i]._id
                            if (id === self.lastEventId) {
                                if (events[i].linked) {
                                    eventsLinked = self.getLinkEvents(events[i], calendars, eventsLinked);
                                    for (let t = 0; t < eventsLinked.length; t++) {
                                        self.replicateEvent(eventsLinked[t], start, end);
                                    }
                                } else {
                                    self.replicateEvent(events[i], start, end);
                                }
                            }
                        }
                    }
                }
                self.calendar4.fullCalendar('unselect');
                self.parent.hasScheduler = true;
            },
            eventRender: function (event, element, view) {
                if (self.parent && self.parent.isReadOnly) {
                    event.editable = false;
                }

                let time = event.sunrise ? "SUNRISE - " : '{ ' + event.start.format('HH:mm') + ' - ';
                if (event.sunset) {
                    time = time + 'SUNSET'
                } else {
                    time = time + event.end.format('HH:mm') + ' }';
                }
                element.find('.fc-content').prepend('<span><i class="fa fa-clock-o m-r-5"></i>' + time + '</span>');

                element.css('background-color', event.eventColor);
                element.css('border-color', event.eventColor);
            },
            eventClick: function (event, jsEvent, view) {
                let el = $('#calendar-date-modal');

                self.eventDetails.starttime = event.start.format('HH:mm');
                self.eventDetails.endtime = event.end.format('HH:mm');

                self.isDeleteDetailEventDisabled = event.linked;

                $('.time').mask('00:00');
                el.modal();

                self.eventDetails.s_sunrise = event.s_sunrise;
                self.eventDetails.s_sunset = event.s_sunset;
                self.eventDetails.e_sunrise = event.e_sunrise;
                self.eventDetails.e_sunset = event.e_sunset;
                self.eventDetails.extendedHours = event.extendedHours;
                self.eventDetails.exStarttime = event.exStarttime;
                self.eventDetails.exEndtime = event.exEndtime;

                self.hasExtendedHours = event.extendedHours;

                self.editingExtendedHours = event.extendedHours;

                el.on('shown.bs.modal', function (e) {
                    self.cdRef.detectChanges();
                });

                //el.on('hidden.bs.modal', function () {
                //    $(this).data('bs.modal', null);
                //});

                if (self.lastEventId < 1) {
                    let eventId = parseInt(event.parentId);
                    if (eventId) {
                        self.lastEventId = eventId;
                    }
                }

                el.on('hide.bs.modal', function (e) {
                    let pressedButton = $(document.activeElement).attr('id');
                    if (pressedButton === "date_btn-modal-yes") {
                        let _start = moment(self.eventDetails.starttime, "HH:mm").format("HH:mm");
                        let _end = moment(self.eventDetails.endtime, "HH:mm").format("HH:mm");
                        if (_start.length === 5) {
                            let eventStartTime = event.start.format('HH:mm');
                            let eventEndTime = event.end.format('HH:mm');

                            let events = detailCalendar.fullCalendar('clientEvents');
                            let count = 0;
                            let lastParentId = events[0].parentId;
                            for (let i = 0; i < events.length; i++) {
                                let startTime = events[i].start.format('HH:mm');
                                let endTime = events[i].end.format('HH:mm');
                                if ((events[i].parentId === event.parentId || events[i].groupId === event.groupId) && eventStartTime === startTime && eventEndTime == endTime) {
                                    if (events[i].parentId !== lastParentId) {
                                        lastParentId = events[i].parentId;
                                        count = 0;
                                    }
                                    count += 1;
                                    let start = events[i].start.clone();
                                    let end = events[i].end.clone();

                                    events[i].start = events[i].start.clone().set({
                                        hour: parseInt(_start.substring(0, 2)),
                                        minute: parseInt(_start.substring(3))
                                    });
                                    events[i].end = events[i].end.clone().set({
                                        hour: parseInt(_end.substring(0, 2)),
                                        minute: parseInt(_end.substring(3))
                                    });

                                    if (self.isOverlapping(detailCalendar, events[i])) {
                                        events[i].start = start;
                                        events[i].end = end;
                                    } else {
                                        events[i].s_sunrise = self.eventDetails.s_sunrise;
                                        events[i].s_sunset = self.eventDetails.s_sunset;
                                        events[i].e_sunrise = self.eventDetails.e_sunrise;
                                        events[i].e_sunset = self.eventDetails.e_sunset;
                                        events[i].extendedHours = self.eventDetails.extendedHours;
                                        events[i].exStarttime = self.eventDetails.exStarttime;
                                        events[i].exEndtime = self.eventDetails.exEndtime;


                                        let eventColor = events[i].eventColor;
                                        if (self.eventDetails.s_sunrise || self.eventDetails.s_sunset || self.eventDetails.e_sunrise || self.eventDetails.e_sunset) {
                                            events[i].previousColor = events[i].eventColor;
                                            events[i].eventColor = '#2b3c4e';
                                        } else {
                                            if (event.eventColor === '#2b3c4e') {
                                                events[i].eventColor = events[i].previousColor;
                                                events[i].previousColor = null;
                                            }
                                        }

                                        if (count > 1 && self.eventDetails.extendedHours) { //extended hours for details calendar staring the next day
                                            if (self.eventDetails.exEndtime.length === 5) {
                                                let hour = parseInt(self.eventDetails.exEndtime.substring(0, 2));
                                                let minute = parseInt(self.eventDetails.exEndtime.substring(3));

                                                let day = events[i].start.format('D');
                                                let month = events[i].start.format('MM');
                                                let year = events[i].start.format('YYYY');

                                                let start = moment(`${day}-${month}-${year} 00:01`, 'DD-MM-YYYY HH:mm a');
                                                self.addExtendedHoursEvent(start, events[i], hour, minute, eventColor)
                                            }
                                        }

                                        detailCalendar.fullCalendar('updateEvent', events[i]);
                                    }
                                }
                            }

                            //extended hours for master && detail calendar only for added day
                            if (self.eventDetails.extendedHours) {
                                if (self.lastEventId > 0 && self.eventDetails.exEndtime.length === 5) {
                                    let calendar = self.lastEventId < 100 ? self.calendar1 : (self.lastEventId > 200 ? self.calendar3 : self.calendar2);
                                    let parentEvent = calendar.fullCalendar('clientEvents', self.lastEventId)[0];
                                    if (parentEvent) {
                                        let linkedEvents = [];
                                        if (parentEvent.linked) {
                                            linkedEvents = self.getLinkEvents(parentEvent, [self.calendar1, self.calendar2, self.calendar3], linkedEvents);
                                        }

                                        self.addExtendedDay(parentEvent, calendar, events)

                                        if (linkedEvents.length > 1) {
                                            for (let i = 0; i < linkedEvents.length; i++) {
                                                let linkedEventId = linkedEvents[i].id || linkedEvents[i]._id;
                                                let parentEventId = parentEvent.id || parentEvent._id;
                                                if (linkedEventId !== parentEventId) {
                                                    calendar = linkedEventId < 100 ? self.calendar1 : (linkedEventId > 200 ? self.calendar3 : self.calendar2);
                                                    self.addExtendedDay(linkedEvents[i], calendar, events)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (pressedButton === "date_btn-modal-delete") {
                            let eventId = event.id || event._id;
                            if (event.extendedHours) {
                                let calendar = self.lastEventId < 100 ? self.calendar1 : (self.lastEventId > 200 ? self.calendar3 : self.calendar2);
                                let detailsEvents = detailCalendar.fullCalendar('clientEvents');
                                let allEventCount = 0;
                                let extendEventCount = 0;
                                for (let i = 0; i < detailsEvents.length; i++) {
                                    if (detailsEvents[i].parentId === event.parentId || detailsEvents[i].groupId === event.groupId) {
                                        allEventCount += 1;
                                    }
                                    if (detailsEvents[i].extendedHours) {
                                        extendEventCount += 1;
                                    }
                                }
                                if (event.extendedHours) {
                                    let masterEvent = calendar.fullCalendar('clientEvents', event.parentId)[0];
                                    let masterEvents = [];
                                    if (masterEvent.linked) {
                                        masterEvents = self.getLinkEvents(masterEvent, [self.calendar1, self.calendar2, self.calendar3], masterEvents);
                                    } else {
                                        masterEvents.push(masterEvent);
                                    }

                                    if (allEventCount > extendEventCount) {
                                        for (let i = 0; i < masterEvents.length; i++) {
                                            let materEventId = masterEvents[i].id || masterEvents[i]._id;
                                            let masterCalendar = materEventId < 100 ? self.calendar1 : (materEventId > 200 ? self.calendar3 : self.calendar2);
                                            masterEvents[i].end = moment(masterEvents[i].end).set({ hour: 0, minute: 0 }).add(-1, 'days');
                                            masterCalendar.fullCalendar('updateEvent', masterEvents[i]);
                                        }
                                    } else {
                                        for (let i = 0; i < masterEvents.length; i++) {
                                            let materEventId = masterEvents[i].id || masterEvents[i]._id;
                                            let masterCalendar = materEventId < 100 ? self.calendar1 : (materEventId > 200 ? self.calendar3 : self.calendar2);
                                            masterCalendar.fullCalendar('removeEvents', materEventId);
                                        }
                                    }
                                }

                                let events = detailCalendar.fullCalendar('clientEvents');
                                for (let i = 0; i < events.length; i++) {
                                    if ((events[i].groupId == event.groupId) && events[i].extendedHours) {
                                        let id = events[i].id || events[i]._id;
                                        detailCalendar.fullCalendar('removeEvents', id);
                                        detailCalendar.fullCalendar('renderEvents');
                                    }
                                }
                            } else {
                                detailCalendar.fullCalendar('removeEvents', eventId);
                                detailCalendar.fullCalendar('renderEvents');
                            }
                        }
                    }
                    self.eventDetails.s_sunrise = false;
                    self.eventDetails.s_sunset = false;

                    $('#calendar-date-modal').off('hide.bs.modal');
                });
            },
        });
    }

    private replicateEvent(event, start, end) {

        function createEvent(eventStart, eventEnd) {
            let _start = start.clone();
            let _end = end.clone();

            _start.set({ date: eventStart.format("DD"), month: eventStart.format("MM") - 1 });
            _end.set({ date: eventEnd.format("DD"), month: eventEnd.format("MM") - 1 });

            let eventData = {
                start: _start,
                end: _end,
                parentId: event.id,
                groupId: event.groupId,
                linked: event.linked,
                eventColor: event.eventColor,
                s_sunrise: event.s_sunrise,
                s_sunset: event.s_sunset,
                e_sunrise: event.e_sunrise,
                e_sunset: event.e_sunset,
                extendedHours: event.extendedHours,
                exStarttime: event.exStarttime,
                exEndtime: event.exEndtime,
                allDay: false
            };
            this.calendar4.fullCalendar('renderEvent', eventData, true);
            this.calendar4.fullCalendar('unselect');
        }

        let from = event.start.clone().add(1, "days");
        let to = event.end.clone();

        if (from.isSame(to, 'day')) {
            let eventEnd = event.end.clone().add(-1, "days")
            createEvent.bind(this)(event.start, eventEnd);
        } else {
            let eventStartDay = from.toDate().getDate();
            let eventEndDay = to.toDate().getDate();
            let totalDays = eventEndDay - eventStartDay + 1;

            if (totalDays > 1) {
                for (let t = 0; t < totalDays; t++) {
                    let _eventStart = event.start.clone();
                    let eventDate = _eventStart.add(t, "days");
                    createEvent.bind(this)(eventDate, eventDate);
                }
            }
        }
    }

    private addExtendedDay(parentEvent, calendar, events) {

        let hour = parseInt(this.eventDetails.exEndtime.substring(0, 2));
        let minute = parseInt(this.eventDetails.exEndtime.substring(3));
        let lastEvent = events[events.length - 1];

        if (!parentEvent.extendedHours) {
            if (!parentEvent.start.isSame(parentEvent.end, 'day')) {
                parentEvent.end.add(-1, "days");
            }

            let lastDay = moment(parentEvent.end).add(2, 'days');

            parentEvent.extendedHours = true;
            parentEvent.end = lastDay;

            calendar.fullCalendar('updateEvent', parentEvent);

            this.addExtendedHoursEvent(lastDay, lastEvent, hour, minute, lastEvent.eventColor)
        } else {
            //let date = parentEvent.end.clone().add(-1, "days")
            //this.addExtendedHoursEvent(date, lastEvent, hour, minute, lastEvent.eventColor)
        }
    }

    private updateDataEvent(event) {
        if (!this.data) {
            return
        };

        for (let i = 0; i < this.data.length; i++) {
            let obj = JSON.parse(this.data[i].event);

            let id = event._id || event.id;
            if (obj.id !== id) {
                continue
            };

            obj.start = event.start;
            obj.end = event.end;
            obj.s_sunset = event.s_sunset;
            obj.s_sunrise = event.s_sunrise;
            obj.e_sunset = event.e_sunset;
            obj.e_sunrise = event.e_sunrise;
            obj.extendedHours = event.extendedHours;
            obj.exStarttime = event.exStarttime;
            obj.exEndtime = event.exEndtime;

            this.data[i].event = JSON.stringify(obj);
        }
    }

    private resetMasterDataEvents(proposalId, calendarId, cEvents) {
        for (let i = 0; i < cEvents.length; i++) {
            let cEvent = cEvents[i];

            let event = {
                id: cEvent.id,
                start: cEvent.start,
                end: cEvent.end ? cEvent.end : null,
                linked: cEvent.linked,
                groupId: cEvent.groupId,
                eventColor: cEvent.eventColor,
                previousColor: cEvent.previousColor
            };

            this.data.push({
                calendarType: 3,
                proposalId: proposalId,
                calendarId: calendarId,
                event: JSON.stringify(event)
            });
        }
    }

    private resetDetailsDataEvents(proposalId, calendarId, cEvents) {
        for (let i = 0; i < cEvents.length; i++) {
            let cEvent = cEvents[i];

            let event = {
                id: cEvent._id || cEvent.id,
                start: cEvent.start,
                end: cEvent.end,
                linked: cEvent.linked,
                groupId: cEvent.groupId,
                eventColor: cEvent.eventColor,
                previousColor: cEvent.previousColor,
                parentId: cEvent.parentId,
                s_sunrise: cEvent.s_sunrise,
                s_sunset: cEvent.s_sunset,
                e_sunrise: cEvent.e_sunrise,
                e_sunset: cEvent.e_sunset,
                exStarttime: cEvent.exStarttime,
                exEndtime: cEvent.exEndtime,
                extendedHours: cEvent.extendedHours
            };

            this.data.push({
                calendarType: 3,
                proposalId: proposalId,
                calendarId: calendarId,
                event: JSON.stringify(event)
            });
        }
    }


    private isValidStartEnd(start:any, end:any) : boolean {
        return start.isSame(end, 'day');
    }

    private isOverlapping(calendar, event) {
        var array = calendar.fullCalendar('clientEvents');
        for (let i in array) {
            if (array[i]._id != event._id) {
                if (!(array[i].start >= event.end || array[i].end <= event.start)) {
                    return true;
                }
            }
        }
        return false;
    }

    private getNextLinkColor() {
        this.currentEventColor = null;
        for (let i = 0; i < this.eventColors.length; i++) {
            if (!this.eventColors[i].used) {
                this.eventColors[i].used = true;
                this.currentEventColor = this.eventColors[i];
                break;
            }
        }

        if (!this.currentEventColor) {
            for (let i = 0; i < this.eventColors.length; i++) {
                this.eventColors[i].used = false;
            }
            this.eventColors[0].used = true;
            this.currentEventColor = this.eventColors[0];
        }

        return this.currentEventColor;
    }

    private resetLinkColor(color: string) {
        for (let i = 0; i < this.eventColors.length; i++) {
            if (this.eventColors[i].color === color) {
                this.eventColors[i].used = false;
                break;
            }
        }
    }

    private findEventById(events, eventId) {
        for (let i = 0; i < events.length; i++) {
            let id = events[i].id || events[i]._id;
            if (id == eventId) {
                return id;
            }
        }

        return null;
    }


    private unlikEvent(calendar, currentEvent) {
        let event = calendar.fullCalendar('clientEvents', currentEvent.id)[0];
        if (event) {
            event.linked = false;
            event.groupId = null;
            event.eventColor = this.defaultColor;
            event.previousColor = null;
        }
    }

    private deleteLinkedDetailsEvents(event) {
        let events = this.calendar4.fullCalendar('clientEvents');
        for (let i = 0; i < events.length; i++) {
            if (events[i].groupId === event.groupId) {
                if (events[i].extendedHours) {

                }

                let id = events[i].id || events[i]._id;
                this.calendar4.fullCalendar('removeEvents', id);
            }
        }
    }

    private unLinkedDetailsEvents() {
        let events = this.calendar4.fullCalendar('clientEvents');
        for (let i = 0; i < events.length; i++) {
            events[i].linked = false;
            events[i].groupId = null;
            events[i].eventColor = this.defaultColor;
            events[i].previousColor = null;
        }
        this.calendar4.fullCalendar('rerenderEvents');
    }


    private unselectEvents(calendars: any[], eventId) {
        for (let i = 0; i < calendars.length; i++) {
            let events = calendars[i].fullCalendar('clientEvents');
            for (let j = 0; j < events.length; j++) {
                let id = events[j].id || events[j]._id;
                if (id !== eventId) {
                    if (events[j].eventColor === this.selectedColor) {
                        events[j].eventColor = events[j].previousColor;
                        events[j].previousColor = null;
                    }
                }
            }
        }
    }

    private selectCurrentEvent(event) {
        if (event) {
            let previousColor = event.previousColor ? event.previousColor : this.selectedColor;
            event.previousColor = event.eventColor;
            event.eventColor = previousColor;
        }
        return event;
    }

    private getLinkEvents(event: any, calendars: any[], eventsLinked: any[]) {
        function exist(events, eventId) {
            for (let i = 0; i < events.length; ++i) {
                let id = events[i].id || events[i]._id;
                if (id === eventId) {
                    return true;
                }
            }
            return false;
        }

        if (!event.linked) return eventsLinked;

        if (!exist(eventsLinked, event.id)) {
            eventsLinked.push(event);
        }

        for (let i = 0; i < calendars.length; i++) {
            let events = calendars[i].fullCalendar('clientEvents');
            for (let j = 0; j < events.length; j++) {
                if (events[j].groupId === event.groupId) {
                    let id = events[j].id || events[j]._id;
                    if (!exist(eventsLinked, id)) {
                        eventsLinked = eventsLinked.concat(events[j]);
                    }
                }
            }
        }

        return eventsLinked;
    }

    private addExtendedHoursEvent(start, event: any, hour, minute, eventColor) {
        if (event.extendedHours) {
            let events = this.calendar4.fullCalendar('clientEvents');
            for (let idx = 0; idx < events.length; idx++) {
                if (events[idx].parentId = event.parentId && events[idx].groupId == event.groupId ) {
                    if (events[idx].start.format("DD") === start.format("DD") && events[idx].start.format("MM") === start.format("MM")) {
                        if (events[idx].start.format('HH:mm') === "00:00") {
                            let id = events[idx]._id || events[idx].id;
                            this.calendar4.fullCalendar('removeEvents', id);
                            this.calendar4.fullCalendar('prev');
                            this.calendar4.fullCalendar('next');
                        }
                    }
                }

            }
        }

        let date = moment(start).local();
        let day = date.format('DD');
        let month = date.format('MM');
        let year = date.format('YYYY');

        let eventStart = moment.utc(`${day}-${month}-${year} 00:00`, 'DD-MM-YYYY HH:mm a');
        let eventEnd = moment.utc(`${day}-${month}-${year} ${hour}:${minute}`, 'DD-MM-YYYY HH:mm a');

        let eventData = {
            start: eventStart,
            end: eventEnd,
            parentId: event.parentId,
            groupId: event.groupId,
            linked: event.linked,
            eventColor: eventColor,
            s_sunrise: false,
            s_sunset: false,
            e_sunrise: false,
            e_sunset: false,
            extendedHours: true,
            exStarttime: '00:00',
            exEndtime: `${this.windowRef.zeroPad(hour, 2)}:${this.windowRef.zeroPad(minute, 2)}`,
            allDay: false
        };
        this.calendar4.fullCalendar('renderEvent', eventData, true);
    }

    private IsValidDate(selectedDate, calendarDate) {
        let dayOfMonth = parseInt(selectedDate.format('D'));
        return selectedDate.format('M') === calendarDate.format('M') && dayOfMonth >= parseInt(moment().format('D'));
    }

    private setPrimaryEvent(obj) {
        let event = {
            id: obj.id,
            eventColor: obj.eventColor,
            previousColor: obj.previousColor,
            groupId: obj.groupId,
            linked: obj.linked,
            start: moment.utc(obj.start),
            end: moment.utc(obj.end).endOf('day'),
            extendedHours: obj.extendedHours,
            allDay: true,
            stick: true
        }

        return event;
    }

    private setDetailEvent(obj) {
        let event = {
            id: obj.id,
            eventColor: obj.eventColor,
            previousColor: obj.previousColor,
            groupId: obj.groupId,
            linked: obj.linked,
            start: moment.utc(obj.start),
            end: moment.utc(obj.end),
            allDay: false,
            stick: true,
            parentId: obj.parentId,
            s_sunrise: obj.s_sunrise,
            s_sunset: obj.s_sunset,
            e_sunrise: obj.e_sunrise,
            e_sunset: obj.e_sunset,
            exStarttime: obj.exStarttime,
            exEndtime: obj.exEndtime,
            extendedHours: obj.extendedHours
        }

        return event;
    }
}
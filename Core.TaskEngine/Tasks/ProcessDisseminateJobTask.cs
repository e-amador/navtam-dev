﻿using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.SqlServiceBroker;
using NavCanada.Core.TaskEngine.Contracts;
using NavCanada.Core.TaskEngine.Helpers;
using NavCanada.Core.TaskEngine.Mto;
using NDS.Relay;
using System;
using System.Collections.Generic;
using System.Threading;

namespace NavCanada.Core.TaskEngine.Tasks
{
    internal class ProcessDisseminateJobTask : IMessageTask
    {
        public void Execute(IMessageTaskContext context)
        {
            var loggerType = GetType();
            var logger = context.ExternalServices.Logger;
            try
            {
                logger.LogDebug(loggerType, "ProcessDisseminateJobTask task started...");

                var keyObj = context.Mto[MessageDefs.EntityIdentifierKey];
                if (keyObj == null)
                {
                    logger.LogWarn(loggerType, "Mto message missing entity identifier...");
                    return;
                }

                var jobIdRaw = keyObj.Value.ToString();

                int jobId;
                if (!int.TryParse(jobIdRaw, out jobId))
                {
                    logger.LogWarn(loggerType, $"Failed to convert Job Id '{jobIdRaw}'", new { jobId = jobIdRaw });
                    return;
                }

                var uow = context.ClusterStorage.Uow;
                var job = uow.DisseminationJobRepo.GetById(jobId);

                if (job == null)
                {
                    logger.LogWarn(loggerType, $"Task completed but Job {jobId} was NOT FOUND in DB.", new { jobId });
                    return;
                }

                try
                {
                    ProcessNextNotam(context, job);
                }
                catch (Exception e)
                {
                    logger.LogError(e, loggerType, $"Disseminate job with id '{jobId}' has failed!", job);
                    job.Status = DisseminationJobStatus.Failed;
                }

                // update the job
                uow.DisseminationJobRepo.Update(job);
                uow.SaveChanges();

                logger.LogDebug(loggerType, $"ProcessDisseminateJobTask finished dissemination of notam# {job.LastNotam}.");
            }
            catch (Exception ex)
            {
                logger.LogError(ex, loggerType, "ProcessDisseminateJobTask has FAILED!");
                throw;
            }
        }

        static void ProcessNextNotam(IMessageTaskContext context, DisseminationJob job)
        {
            // if not active just exit
            if (!IsJobActive(job.Status))
                return;

            var uow = context.ClusterStorage.Uow;

            // set job in process
            job.Status = DisseminationJobStatus.InProcess;

            while (true)
            {
                // try to seek on the next notam to disseminate
                var nextNotam = GetNextNotamToDisseminate(uow, job);
                if (nextNotam != null)
                {
                    // update the last notam in the job
                    job.LastNotam = nextNotam.SequenceNumber;

                    if (DisseminateNotamToAddresses(context, nextNotam, job.ClientIds, job.DisseminateXml) > 0)
                    {
                        // if at least one client matched, queue the next job and leave
                        QueueNextDisseminationJob(uow, job.Id);
                        break;
                    }
                }
                else
                {
                    // none found
                    job.Status = DisseminationJobStatus.Completed;
                    break;
                }
            }

            // update job
            job.LastUpdate = DateTime.UtcNow;
        }

        static Notam GetNextNotamToDisseminate(IUow uow, DisseminationJob job)
        {
            var refDate = job.BreakDate.HasValue ? job.BreakDate.Value : new DateTime(DateTime.UtcNow.Year, 1, 1);
            return uow.NotamRepo.GetNextBySequenceNumberAndReferenceDate(job.LastNotam, refDate);
        }

        static readonly char[] IdSeparators = new char[] { ' ' };

        static HashSet<string> GetIdSet(string ids)
        {
            return new HashSet<string>((ids ?? "").Split(IdSeparators, StringSplitOptions.RemoveEmptyEntries));
        }

        static int DisseminateNotamToAddresses(IMessageTaskContext context, Notam notam, string ids, bool disseminateXml)
        {
            var uow = context.ClusterStorage.Uow;
            var idSet = GetIdSet(ids);

            var messagePublisher = new MessagePublisher(context.ExternalServices.SolaceEndpoint, uow);
            var distributionRelay = new DistributionRelay(uow, messagePublisher);

            var totalDisseminated = 0;

            if (disseminateXml)
            {
                // disseminate to the hub, but do not distribute it
                distributionRelay.Disseminate(notam, false);
                totalDisseminated++;
            }

            if (idSet.Count > 0)
            {
                totalDisseminated += distributionRelay.DistributeToAftn(notam, a => idSet.Contains(a.Id.ToString()), false);
            }

            return totalDisseminated;
        }

        static void QueueNextDisseminationJob(IUow uow, int jobId)
        {
            var mto = MtoHelper.CreateMtoMessage(MessageTransferObjectId.QueueNextDisseminateJobMsg, m => m.Add(MessageDefs.EntityIdentifierKey, jobId, DataType.Int32));
            SubmitToServiceBrokerQueue(uow, mto);
        }

        static void SubmitToServiceBrokerQueue(IUow uow, MessageTransferObject mto)
        {
            var taskEngineQueue = new SqlServiceBrokerQueue(uow.DbContext, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings);
            taskEngineQueue.Publish(mto);
        }

        static bool IsJobActive(DisseminationJobStatus status)
        {
            return status == DisseminationJobStatus.Pending || status == DisseminationJobStatus.InProcess;
        }
    }
}
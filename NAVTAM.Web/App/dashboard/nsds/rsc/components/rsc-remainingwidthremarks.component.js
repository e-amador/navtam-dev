"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RemainingWidthRemarksComponent = void 0;
var core_1 = require("@angular/core");
var angular_l10n_1 = require("angular-l10n");
var forms_1 = require("@angular/forms");
var rsc_service_1 = require("../rsc.service");
var toastr_service_1 = require("../../../../common/toastr.service");
var RemainingWidthRemarksComponent = /** @class */ (function () {
    function RemainingWidthRemarksComponent(fb, translation, rscService, toastr) {
        this.fb = fb;
        this.translation = translation;
        this.rscService = rscService;
        this.toastr = toastr;
        this.mainForm = null;
        this.model = null;
        this.isReadOnly = false;
    }
    RemainingWidthRemarksComponent.prototype.ngOnInit = function () {
        this.isFormLoadingData = false;
        this.configureReactiveForm();
    };
    RemainingWidthRemarksComponent.prototype.ngOnChanges = function () {
    };
    RemainingWidthRemarksComponent.prototype.ngOnDestroy = function () {
    };
    RemainingWidthRemarksComponent.prototype.$ctrl = function (name) {
        return this.frmGroup.get(name);
    };
    RemainingWidthRemarksComponent.prototype.configureReactiveForm = function () {
        var frmArrControls = this.mainForm.controls['frmRemainingWidthRemarksArr'];
        this.frmGroup = this.fb.group({
            remainingWidthRemarks: [{ value: this.model.remainingWidthRemarks, disabled: this.isReadOnly }, [forms_1.Validators.required]],
        });
        frmArrControls.push(this.frmGroup);
    };
    //Export functions
    RemainingWidthRemarksComponent.prototype.isFormValid = function () {
        return true;
    };
    RemainingWidthRemarksComponent.prototype.exportComponentInternalData = function () {
        var data = {
            parent: this.model.parent,
            remainingWidthRemarks: this.model.remainingWidthRemarks,
        };
        return data;
    };
    RemainingWidthRemarksComponent.prototype.clearValues = function () {
        alert('Clear Component values: RemainingWidthRemarks');
    };
    __decorate([
        core_1.Input('form'),
        __metadata("design:type", forms_1.FormGroup)
    ], RemainingWidthRemarksComponent.prototype, "mainForm", void 0);
    __decorate([
        core_1.Input('model'),
        __metadata("design:type", Object)
    ], RemainingWidthRemarksComponent.prototype, "model", void 0);
    __decorate([
        core_1.Input('isReadOnly'),
        __metadata("design:type", Boolean)
    ], RemainingWidthRemarksComponent.prototype, "isReadOnly", void 0);
    RemainingWidthRemarksComponent = __decorate([
        core_1.Component({
            selector: 'rsc-remaining-width-remarks',
            templateUrl: './app/dashboard/nsds/rsc/components/rsc-remainingwidthremarks.component.html'
        }),
        __param(3, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [forms_1.FormBuilder,
            angular_l10n_1.TranslationService,
            rsc_service_1.RscService, Object])
    ], RemainingWidthRemarksComponent);
    return RemainingWidthRemarksComponent;
}());
exports.RemainingWidthRemarksComponent = RemainingWidthRemarksComponent;
//# sourceMappingURL=rsc-remainingwidthremarks.component.js.map
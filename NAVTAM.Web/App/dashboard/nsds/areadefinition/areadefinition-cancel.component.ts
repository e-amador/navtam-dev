﻿import { Component, OnInit, OnDestroy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'

import { DataService } from '../../shared/data.service';

@Component({
    templateUrl: './app/dashboard/nsds/areadefinition/areadefinition-cancel.component.html'
})
export class AreaDefinitionCancelNsdComponent implements OnInit, OnDestroy {

    model: any = null;

    constructor(
        private dataService: DataService,
        private activatedRoute: ActivatedRoute,
        private router: Router) {
    }

    ngOnInit() {
        window['loadLeafletMapScript'].call(this, null);
        this.model = this.activatedRoute.snapshot.data['model'];
    }

    ngOnDestroy() {
        if (window["app"].leafletmap) {
            window["app"].leafletmap.dispose();
        }
    }

    get langCulture() {
        return (window["app"].cultureInfo || "en").toUpperCase();
    }
}
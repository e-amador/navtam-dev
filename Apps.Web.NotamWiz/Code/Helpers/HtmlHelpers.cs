﻿namespace NavCanada.Applications.NotamWiz.Web.Code.Helpers
{
    using System;
    using System.Configuration;
    using System.Text;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Security.AntiXss;
    using System.Web.Util;

    public static class HtmlHelpers
    {
        // Extension method
        public static MvcHtmlString ActionImage(this HtmlHelper html, string action, string controllerName, object routeValues, string imagePath, string alt)
        {
            var url = new UrlHelper(html.ViewContext.RequestContext);

            // build the <img> tag
            var imgBuilder = new TagBuilder("img");
            imgBuilder.MergeAttribute("src", url.Content(imagePath));
            imgBuilder.MergeAttribute("alt", alt);
            string imgHtml = imgBuilder.ToString(TagRenderMode.SelfClosing);

            // build the <a> tag
            var anchorBuilder = new TagBuilder("a");
            anchorBuilder.MergeAttribute("href", url.Action(action, controllerName, routeValues));
            anchorBuilder.InnerHtml = imgHtml; // include the <img> tag inside
            string anchorHtml = anchorBuilder.ToString(TagRenderMode.Normal);

            return MvcHtmlString.Create(anchorHtml);
        }

        public static IHtmlString ToMvcClientTemplate(this MvcHtmlString mvcString)
        {
            if (HttpEncoder.Current.GetType() == typeof(AntiXssEncoder))
            {
                var initial = mvcString.ToHtmlString();
                var corrected = initial.Replace("\\u0026", "&").Replace("%23", "#").Replace("%3D", "=").Replace("&#32;", " ");
                return new HtmlString(corrected);
            }

            return mvcString;
        }


        /// <summary>
        /// An Html helper for Require.js
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="common">Location of the common js file.</param>
        /// <param name="module">Location of the main.js file.</param>
        /// <returns></returns>
        public static MvcHtmlString RequireJs(this HtmlHelper helper, string common, string module)
        {
            var require = new StringBuilder();

            string jsLocation = ConfigurationManager.AppSettings["JsLocation"];

            require.AppendLine("require( [ \"" + jsLocation + common + "\" ], function() {");
            require.AppendLine("    require( [ \"" + module + "\"] );");
            require.AppendLine("});");

            return new MvcHtmlString(require.ToString());
        }


        /// <summary>
        /// An Html helper for Require.js
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="basePath">Prefix for the location of the Scripts folder.  In dev it will be "\" in production it might be "\NotamWiz\" </param>
        /// <returns></returns>
        public static MvcHtmlString RequireJsCommonStart(this HtmlHelper helper, string basePath)
        {
            var require = new StringBuilder();
            //#if (DEBUG)
            string jsLocation = basePath + "Scripts/";
            //#else
            //          string jsLocation = "Scripts-build/";
            //#endif

            require.AppendLine(string.Format(@"require( [ ""{0}app/common.js"" ], function() {{ setupRequireJS('" +
                (System.Web.HttpContext.Current.IsDebuggingEnabled ? DateTime.Now.Ticks.ToString() : System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString()) +
                "'); ", jsLocation));


            return new MvcHtmlString(require.ToString());
        }
        /// <summary>
        /// An Html helper for Require.js
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="common">Location of the common js file.</param>
        /// <param name="module">Location of the main.js file.</param>
        /// <returns></returns>
        public static MvcHtmlString RequireJsCommonEnd(this HtmlHelper helper)
        {
            var require = new StringBuilder();
            require.AppendLine("});");

            return new MvcHtmlString(require.ToString());
        }
    }
}
﻿using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    public class NsdCategoryRepo : Repository<NsdCategory>, INsdCategoryRepo
    {
        public NsdCategoryRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public async Task<List<NsdCategory>> GetChildCategoriesAsync(int parentCategoryId)
        {
            return await DbSet
                .Where(e => e.ParentCategoryId == parentCategoryId)
                .ToListAsync();
        }

        public async Task<List<NsdCategory>> GetOperationalNsdsByOrganizationId(int orgId)
        {
            var ctx = DbContext as AppDbContext;
            if (ctx == null) throw new NullReferenceException("AppDbContext");

            if (orgId > 0)
            {
                var query = new List<NsdCategory>();

                query = await (from orgnsds in ctx.OrganizationNsds
                               join nsds in ctx.NsdCategories
                               on orgnsds.NsdCategoryId equals nsds.Id
                               where orgnsds.OrganizationId == orgId && nsds.Operational == true
                               select nsds).Distinct().ToListAsync();

                return query;
            }

            throw new Exception("Invalid Organization Id");
        }


        public async Task<List<NsdCategory>> GetActiveNsdsByOrganizationId(int orgId)
        {
            var ctx = DbContext as AppDbContext;
            if (ctx == null) throw new NullReferenceException("AppDbContext");

            if (orgId > 0)
            {
                var query = new List<NsdCategory>();

                query = await (from orgnsds in ctx.OrganizationNsds
                               join nsds in ctx.NsdCategories
                               on orgnsds.NsdCategoryId equals nsds.Id
                               where orgnsds.OrganizationId == orgId
                               select nsds).Distinct().ToListAsync();

                return query;
            }

            throw new Exception("Invalid Organization Id");
        }

        public async Task<List<NsdCategory>> GetRootChildrenAsync()
        {
            return await DbSet
                .Where(e => e.ParentCategoryId != null && e.Operational)
                .OrderBy(e => e.Id)
                .ThenBy(e => e.ParentCategoryId)
                .ToListAsync();
        }

        public async Task<List<NsdCategory>> GetActiveNsds()
        {
            return await DbSet.Where(nsd => (nsd.ParentCategoryId > 1 || nsd.Name == "CATCHALL") && nsd.Operational).ToListAsync();
        }

        public async Task<List<NsdCategory>> GetAllNsds()
        {
            return await DbSet.Where(nsd => (nsd.ParentCategoryId > 1 || nsd.Name == "CATCHALL")).ToListAsync();
        }

        public async Task<NsdCategory> GetByNsdId(int nsdId)
        {
            return await DbSet.Where(x => x.Id == nsdId).FirstOrDefaultAsync();
        }

        public NsdCategory GetByName(string name)
        {
            return DbSet.FirstOrDefault(e => e.Name == name);
        }
    }
}
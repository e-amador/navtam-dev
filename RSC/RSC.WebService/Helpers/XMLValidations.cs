﻿using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace RSC.WebService.Helpers
{
    public class XMLValidations
    {
        static XMLValidations()
        {
            _proposalSchema = LoadSchemaFromResources("RSC.WebService.Schemas.RSCProposal.xsd");
        }

        public static XmlSchemaSet ProposalSchemaSet => _proposalSchema;

        public static string ValidateProposal(XmlDocument doc)
        {
            var xDoc = XDocument.Load(new XmlNodeReader(doc));
            string message = null;
            xDoc.Validate(_proposalSchema, (o, e) => 
            {
                message = e.Message;
            });
            return message;
        }

        public static string ValidateProposal(string xml)
        {
            using (var xmlReader = XmlReader.Create(new StringReader(xml ?? "")))
            {
                var xDoc = XDocument.Load(xmlReader);

                string message = null;
                xDoc.Validate(_proposalSchema, (o, e) => { message = e.Message; });

                return message;
            }
        }

        static XmlSchemaSet LoadSchemaFromResources(string resourcePath)
        {
            var assembly = Assembly.GetExecutingAssembly();
            using (var stream = assembly.GetManifestResourceStream(resourcePath))
            {
                var schema = new XmlSchemaSet();
                schema.Add(null, XmlReader.Create(stream));
                return schema;
            }
        }

        static XmlSchemaSet _proposalSchema;
    }
}
﻿import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { UserModule } from './usr.module';

const platform = platformBrowserDynamic();
enableProdMode();
platform.bootstrapModule(UserModule);
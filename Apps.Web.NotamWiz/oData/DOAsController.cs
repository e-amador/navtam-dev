﻿namespace NavCanada.Applications.NotamWiz.Web.oData
{
    using System.Linq;
    using System.Web.Http;
    using System.Web.OData;

    using NavCanada.Core.Data.NotamWiz.Contracts;
    using NavCanada.Core.Domain.Model.Entitities;

    [Authorize]
    public class DOAsController : BaseODataController
    {
        public DOAsController(IUow uow)
            : base(uow)
        {
        }

        // GET: odata/DOA
        [EnableQuery]
        public IQueryable<Doa> GetDOAs()
        {
            return Uow.DoaRepo.GetAll();
        }

        // GET: odata/DOA(5)
        [EnableQuery]
        public SingleResult<Doa> GetDOA([FromODataUri] long key)
        {
            return SingleResult.Create(Uow.DoaRepo.GetAll().Where(dOA => dOA.Id == key));
        }

     
        [EnableQuery]
        public IQueryable<Doa> GetChildDOA([FromODataUri] long key)
        {
            return Uow.DoaRepo.GetAll().Where(m => m.Id == key).SelectMany(m => m.ChildDoas);
        }

        // GET: odata/DOA(5)/DOAFilters
        [EnableQuery]
        public IQueryable<DoaFilter> GetDOAFilters([FromODataUri] long key)
        {
            return Uow.DoaRepo.GetAll().Where(m => m.Id == key).SelectMany(m => m.DoaFilters);
        }

        // GET: odata/DOA(5)/ParentDOA
        [EnableQuery]
        public SingleResult<Doa> GetParentDOA([FromODataUri] long key)
        {
            return SingleResult.Create(Uow.DoaRepo.GetAll().Where(m => m.Id == key).Select(m => m.ParentDoa));
        }

        // GET: odata/DOA(5)/SharedDOADefinition
        [EnableQuery]
        public SingleResult<DoaSharedDefinition> GetSharedDOADefinition([FromODataUri] long key)
        {
            return SingleResult.Create(Uow.DoaRepo.GetAll().Where(m => m.Id == key).Select(m => m.SharedDoaDefinition));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Uow.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DOAExists(long key)
        {
            return Uow.DoaRepo.GetAll().Any(e => e.Id == key);
        }
    }
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReportActiveComponent = void 0;
var core_1 = require("@angular/core");
var toastr_service_1 = require("../../common/toastr.service");
var router_1 = require("@angular/router");
var angular_l10n_1 = require("angular-l10n");
var data_service_1 = require("../shared/data.service");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var windowRef_service_1 = require("../../common/windowRef.service");
var data_model_1 = require("../shared/data.model");
var moment = require("moment");
var report_filter_component_1 = require("./report-filter.component");
var ReportActiveComponent = /** @class */ (function () {
    function ReportActiveComponent(toastr, dataService, memStorageService, router, winRef, route, translation) {
        this.toastr = toastr;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.route = route;
        this.translation = translation;
        this.paging = {
            currentPage: 1,
            pageSize: 0,
            totalCount: 0,
            totalPages: 0,
        };
        this.totalPages = 0;
        this.loadingData = true;
        this.isFilterApplied = false;
        this.filterPannelOpen = false;
        this.filterByActive = 'All';
        this.start = null;
        this.starttime = null;
        this.endtime = null;
        this.tableHeaders = {
            sorting: [
                { columnName: 'notamId', direction: 0 },
                { columnName: 'type', direction: 0 },
                { columnName: 'referredNotamId', direction: 0 },
                { columnName: 'series', direction: 0 },
                { columnName: 'itemA', direction: 0 },
                { columnName: 'siteId', direction: 0 },
                { columnName: 'permanent', direction: 0 },
                { columnName: 'estimated', direction: 0 },
                { columnName: 'itemE', direction: 0 },
                { columnName: 'originator', direction: 0 },
                { columnName: 'modifiedByUsr', direction: 0 },
                { columnName: 'startActivity', direction: 0 },
                { columnName: 'endValidity', direction: 0 },
                { columnName: 'received', direction: 0 },
                { columnName: 'published', direction: 1 }
            ],
            itemsPerPage: [
                { id: '10', text: '10' },
                { id: '25', text: '25' },
                { id: '50', text: '50' },
                { id: '100', text: '100' },
                { id: '1000', text: '1000' },
            ]
        };
        this.paging.pageSize = +this.tableHeaders.itemsPerPage[3].id;
        this.queryOptions = {
            page: 1,
            pageSize: this.paging.pageSize,
            sort: "_Published"
        };
    }
    ReportActiveComponent.prototype.ngOnInit = function () {
        var self = this;
        this.starttime = "00:00";
        this.endtime = "59:59";
        $('.rpt-notam-datepicker').each(function () {
            $(this).bootstrapDatepicker({
                autoclose: false,
                todayHighlight: true,
                orientation: "top",
                startView: 0,
                language: "en",
                forceParse: false,
                daysOfWeekDisabled: "",
                calendarWeeks: false,
                toggleActive: true,
                multidate: false,
            }).on('changeDate', function (e) {
                self.dateChanged(e);
                $(this).bootstrapDatepicker('hide');
            });
        });
        $('#w-starttime').mask('00:00', { onComplete: function (cep) { self.starttime = cep; } });
        $('#w-endtime').mask('00:00', { onComplete: function (cep) { self.endtime = cep; } });
        this.isFilterApplied = this.memStorageService.get(this.memStorageService.REPORT_FILTER_QUERY_OPTIONS_KEY);
        if (this.isFilterApplied) {
            this.filter.loadFilterState();
        }
        else {
            var queryOptions = this.memStorageService.get(this.memStorageService.REPORT_NOTAM_ACTIVE_QUERY_OPTIONS_KEY);
            if (queryOptions !== null) {
                this.queryOptions = queryOptions;
            }
        }
        if (this.isFilterApplied) {
            this.filter.runReportActiveFilter(false, this.start.toDate(), this.starttime, this.endtime, this.queryOptions.page, this.queryOptions.pageSize);
        }
        else {
            this.getNotams(this.queryOptions.page);
        }
    };
    ReportActiveComponent.prototype.ngAfterViewInit = function () {
        this.filter.parent = this;
    };
    ReportActiveComponent.prototype.itemsPerPageChanged = function (e) {
        this.queryOptions.pageSize = e.value;
        if (this.isFilterApplied) {
            this.loadingData = true;
            this.filter.runFilter(false, 1, this.queryOptions.pageSize);
        }
        else {
            this.getNotams(1);
        }
    };
    ReportActiveComponent.prototype.onFilterChanged = function (event) {
        if (event.error) {
            this.toastr.error("Filter error: " + event.error.message, "", { 'positionClass': 'toast-bottom-right' });
        }
        else {
            this.isFilterApplied = event.isFilterApplied;
            this.filterPannelOpen = false;
            if (this.isFilterApplied) {
                this.processNotams(event.data);
            }
            else {
                this.queryOptions = this.memStorageService.get(this.memStorageService.REPORT_NOTAM_QUERY_OPTIONS_KEY);
                this.getNotams(this.queryOptions.page);
            }
        }
        this.loadingData = false;
    };
    ReportActiveComponent.prototype.onPageChange = function (page) {
        this.getNotams(page);
    };
    ReportActiveComponent.prototype.dateChanged = function (ev) {
        this.start = moment(new Date(ev.currentTarget.value));
        $(this).datepicker('hide');
    };
    ReportActiveComponent.prototype.sort = function (index) {
        var dir = this.tableHeaders.sorting[index].direction;
        for (var i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }
        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            if (this.isFilterApplied) {
                this.filter.changeFilterSort('_' + this.tableHeaders.sorting[index].columnName);
            }
            else {
                this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
            }
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            if (this.isFilterApplied) {
                this.filter.changeFilterSort(this.tableHeaders.sorting[index].columnName);
            }
            else {
                this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
            }
        }
        if (this.isFilterApplied) {
            this.loadingData = true;
            this.filter.runFilter(false, 1);
        }
        else {
            this.getNotams(1);
        }
    };
    ReportActiveComponent.prototype.sortingClass = function (index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';
        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';
        return 'sorting';
    };
    ReportActiveComponent.prototype.onRowSelected = function (index) {
        var notam = this.notams[index];
        if (this.selectedNotam) {
            this.selectedNotam.isSelected = false;
        }
        this.selectedNotam = notam;
        this.selectedNotam.isSelected = true;
    };
    ReportActiveComponent.prototype.isStartHasValue = function () {
        return $('#rpt-notam-startdate').bootstrapDatepicker('getDate') != null;
    };
    ReportActiveComponent.prototype.isRunReportButtonEnabled = function () {
        return this.isStartHasValue();
    };
    ReportActiveComponent.prototype.runReport = function () {
        this.getNotams(1);
    };
    ReportActiveComponent.prototype.exportReport = function () {
        var _this = this;
        var filename = window.prompt(this.translation.translate('Reports.PromptFilename'));
        if (filename) {
            filename = filename.replace('.csv', '') + '.csv';
            this.loadingData = true;
            var payload = {
                Conditions: this.isFilterApplied ? this.filter.filters : null,
                queryOptions: this.queryOptions,
                start: this.start.toDate(),
                startTime: this.starttime,
                endTime: this.endtime
            };
            this.dataService.exportNofReportActiveNotams(payload)
                .subscribe(function (response) {
                var csvData = new Blob([response], { type: 'text/csv;charset=utf-8;' });
                var csvURL = window.URL.createObjectURL(csvData);
                var tempLink = document.createElement('a');
                tempLink.href = csvURL;
                tempLink.setAttribute('download', filename);
                tempLink.click();
                _this.loadingData = false;
            });
        }
    };
    ReportActiveComponent.prototype.filterByActives = function (status) {
        this.filterByActive = status;
    };
    ReportActiveComponent.prototype.notamList = function () {
        if (this.notams) {
            return this.notams;
        }
    };
    ReportActiveComponent.prototype.getNotams = function (page) {
        var _this = this;
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            if (this.isFilterApplied) {
                this.filter.runReportActiveFilter(false, this.start.toDate(), this.starttime, this.endtime, page, this.queryOptions.pageSize);
            }
            else {
                this.memStorageService.save(this.memStorageService.REPORT_NOTAM_ACTIVE_QUERY_OPTIONS_KEY, this.queryOptions);
                this.dataService.getNofReportActiveNotams(this.start.toDate(), this.starttime, this.endtime, this.queryOptions)
                    .subscribe(function (response) {
                    _this.processNotams(response);
                });
            }
        }
        catch (e) {
            this.loadingData = false;
        }
    };
    ReportActiveComponent.prototype.processNotams = function (notams) {
        this.paging = notams.paging;
        this.totalPages = notams.paging.totalPages;
        for (var iter = 0; iter < notams.data.length; iter++) {
            notams.data[iter].index = iter;
            notams.data[iter].typeStr = this.toNotamTypeStr(notams.data[iter].notamType);
            //notams[iter].itemEFull = notams[iter].itemE;
            //notams[iter].itemE = notams[iter].itemE.length > 3 ? notams[iter].itemE.substr(0, 3) + ' ...' : notams[iter].itemE;
        }
        var lastNotamSelected = this.memStorageService.get(this.memStorageService.NOTAM_ID_KEY);
        if (notams.data.length > 0) {
            this.notams = notams.data;
            var index = 0;
            if (lastNotamSelected) {
                index = this.notams.findIndex(function (x) { return x.id === lastNotamSelected; });
                if (index < 0)
                    index = 0; // when not found first should be selected;
            }
            this.notams[index].isSelected = true;
            this.selectedNotam = this.notams[index];
            this.loadingData = false;
            //this.onRowSelected(0);
        }
        else {
            this.notams = [];
            this.selectedNotam = null;
            this.loadingData = false;
        }
    };
    ReportActiveComponent.prototype.toNotamTypeStr = function (notamType) {
        switch (notamType) {
            case data_model_1.ProposalType.New: return 'N';
            case data_model_1.ProposalType.Replace: return 'R';
            case data_model_1.ProposalType.Cancel: return 'C';
            default: return '';
        }
    };
    __decorate([
        core_1.ViewChild(report_filter_component_1.ReportFilterComponent),
        __metadata("design:type", report_filter_component_1.ReportFilterComponent)
    ], ReportActiveComponent.prototype, "filter", void 0);
    __decorate([
        core_1.Input('exportFunc'),
        __metadata("design:type", Object)
    ], ReportActiveComponent.prototype, "exportFunc", void 0);
    ReportActiveComponent = __decorate([
        core_1.Component({
            selector: 'report-active',
            templateUrl: '/app/dashboard/shared/report-active.component.html',
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.TranslationService])
    ], ReportActiveComponent);
    return ReportActiveComponent;
}());
exports.ReportActiveComponent = ReportActiveComponent;
//# sourceMappingURL=report-active.component.js.map
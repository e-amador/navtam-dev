﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { XsrfTokenService } from '../../../common/xsrf-token.service'
import { WindowRef } from '../../../common/windowRef.service'

@Injectable()
export class CarsClsdService {
    constructor(private http: Http, winRef: WindowRef, private xsrfTokenService: XsrfTokenService) {
        this.app = winRef.nativeWindow['app'];
        if (this.app.antiForgeryTokenValue) {
            this.xsrfTokenService.addForgeryTokenToHeader(this.app.antiForgeryTokenValue);
        }
    }

    public app: any;

    getCarsClsdStatus(lang: string): Observable<any> {
        return this.http.get(`${this.app.apiUrl}carsclsd/GetCarsClsdStatus/${lang}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    private handleError(error: Response) {
        return Observable.throw(error.statusText);
    }

}
﻿using NavCanada.Core.Domain.Model.Enums;
using System;
using System.Data.Entity.Spatial;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class ProposalHistory : INotam
    {
        public int Id { get; set; }
        public int ProposalId { get; set; }
        public Proposal Proposal { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public NsdCategory Category { get; set; }
        public string Version { get; set; }
        public int OrganizationId { get; set; } // a.k.a. OnBehalfOfOrganizationId
        public string SubjectId { get; set; }
        public string Nof { get; set; }
        public string Series { get; set; }
        public int Number { get; set; }
        public int Year { get; set; }
        public NotamType ProposalType { get; set; } // a.k.a. type
        public string ReferredSeries { get; set; }
        public int ReferredNumber { get; set; }
        public int ReferredYear { get; set; }
        public string Fir { get; set; }
        public string Code23 { get; set; }
        public IcaoSubject IcaoSubject { get; set; }
        public int IcaoSubjectId { get; set; }
        public string Code45 { get; set; }
        public IcaoSubjectCondition IcaoSubjectCondition { get; set; }
        public int IcaoConditionId { get; set; }
        public string Traffic { get; set; }
        public string Purpose { get; set; }
        public string Scope { get; set; }
        public int LowerLimit { get; set; }
        public int UpperLimit { get; set; }
        public string Coordinates { get; set; }
        public DbGeography Location { get; set; }
        public int Radius { get; set; }
        public string ItemA { get; set; }
        public DateTime? StartActivity { get; set; }
        public DateTime? EndValidity { get; set; }
        public bool Estimated { get; set; }
        public string ItemD { get; set; }
        public string ItemE { get; set; }
        public string ItemEFrench { get; set; }
        public string ItemF { get; set; }
        public string ItemG { get; set; }
        public string ItemX { get; set; }
        public string Operator { get; set; }
        public NotamProposalStatusCode Status { get; set; }
        public DateTime? StatusTime { get; set; }
        public string RejectionReason { get; set; }
        public bool Delete { get; set; } // a.k.a. EntityDelete
        public string NoteToNof { get; set; }
        public bool ContainFreeText { get; set; }
        public bool ModifiedByNof { get; set; }
        public bool IsCatchAll { get; set; }
        public bool Permanent { get; set; }
        public string Originator { get; set; } // a.k.a. OriginatorName 
        public string OriginatorEmail { get; set; } // a.k.a. OriginatorName 
        public string OriginatorPhone { get; set; } // a.k.a. OriginatorName 
        public string Tokens { get; set; }
        public DateTime Received { get; set; }
        public string NotamId { get; set; }
        public string UserId { get; set; }
        public UserProfile User { get; set; }
        public bool Grouped { get; set; }
        public bool IsGroupMaster { get; set; }
        public Guid? GroupId { get; set; }
        public bool Urgent { get; set; }

        #region INotam extras
        public NotamType? ReferenceType => ProposalType;
        public bool Immediate => !StartActivity.HasValue;
        #endregion
        public string ModifiedByOrg { get; set; }
        public string ModifiedByUsr { get; set; }
        public OrganizationType OrganizationType { get; set; }
        public byte[] RowVersion { get; set; }
        public bool PendingReview { get; set; }
    }
}

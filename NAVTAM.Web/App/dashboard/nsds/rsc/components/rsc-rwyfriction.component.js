"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RwyFrictionComponent = void 0;
var core_1 = require("@angular/core");
var angular_l10n_1 = require("angular-l10n");
var forms_1 = require("@angular/forms");
var rsc_service_1 = require("../rsc.service");
var moment = require("moment");
var toastr_service_1 = require("../../../../common/toastr.service");
var RwyFrictionComponent = /** @class */ (function () {
    function RwyFrictionComponent(fb, translation, rscService, toastr) {
        this.fb = fb;
        this.translation = translation;
        this.rscService = rscService;
        this.toastr = toastr;
        this.mainForm = null;
        this.isReadOnly = false;
        this.model = null;
        this.isAverage = false;
        this.measureDateText = "";
        this.dateFormat = "YYMMDDHHmm";
        this.coefficientsOptions = {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('RwyFrictionComponent.SelectCoefficient')
            }
        };
    }
    RwyFrictionComponent.prototype.ngOnInit = function () {
        this.isFormLoadingData = false;
        this.coefficients = this.model.parent.coefficients;
        this.measureDateMoment = moment.utc();
        this.configureReactiveForm();
        //set Measure Date
        this.measureDateText = this.measureDateMoment.format("YYYY/MM/DD, H:mm") + " UTC";
        var fc = this.frmGroup.get('measureDate');
        var formatedDate = this.measureDateMoment.format(this.dateFormat);
        if (fc.value !== formatedDate) {
            fc.patchValue(formatedDate);
        }
        this.model.measureDate = this.measureDateMoment;
    };
    RwyFrictionComponent.prototype.ngOnChanges = function () {
    };
    RwyFrictionComponent.prototype.ngOnDestroy = function () {
    };
    RwyFrictionComponent.prototype.$ctrl = function (name) {
        return this.frmGroup.get(name);
    };
    RwyFrictionComponent.prototype.configureReactiveForm = function () {
        var _this = this;
        var frmArrControls = this.mainForm.controls['frmAverageCRFIArr'];
        this.frmGroup = this.fb.group({
            coefficient: [{ value: this.model.coefficient, disabled: this.isReadOnly }, [forms_1.Validators.required]],
            temperature: [{ value: this.model.temperature, disabled: this.isReadOnly }, [forms_1.Validators.required]],
            measureDate: [{ value: this.measureDateMoment, disabled: this.isReadOnly }, [forms_1.Validators.required]],
        });
        frmArrControls.push(this.frmGroup);
        this.frmGroup.get('temperature')
            .valueChanges.subscribe(function (value) { return _this.temperatureChanged(value); });
    };
    //Coefficient
    RwyFrictionComponent.prototype.onCoefficientChanged = function (e) {
        if (e.value) {
            var coefficientId = e.value;
            var control = this.frmGroup.get('coefficient');
            if (this.isFormLoadingData) {
                coefficientId = control.value;
            }
            else {
                if (coefficientId !== control.value) {
                    control.setValue(coefficientId);
                    control.markAsDirty();
                    this.model.coefficient = coefficientId;
                }
            }
        }
    };
    RwyFrictionComponent.prototype.validateCoefficient = function () {
        var fc = this.frmGroup.get('coefficient');
        return !fc.errors || this.frmGroup.pristine;
    };
    //Temperature
    RwyFrictionComponent.prototype.validateKeyPressNumberTemperature = function (evt) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (key !== '-' && !this.isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
        }
    };
    RwyFrictionComponent.prototype.temperatureChanged = function (value) {
        this.model.temperature = value;
    };
    RwyFrictionComponent.prototype.validateTempeture = function () {
        var fc = this.frmGroup.get('temperature');
        if (!this.isValidNumber(fc.value)) {
            return false;
        }
        return !fc.errors || this.frmGroup.pristine;
    };
    //Measure Date
    RwyFrictionComponent.prototype.isCalendarActive = function () {
        return true;
    };
    RwyFrictionComponent.prototype.setMeasureDate = function (date, leaveEmpty) {
        var fc = this.frmGroup.get('measureDate');
        if (date) {
            var momentDate = moment.utc(date);
            if (momentDate.isValid()) {
                this.measureDateText = momentDate.format("YYYY/MM/DD, H:mm") + " UTC";
                var formatedDate = momentDate.format(this.dateFormat);
                if (fc.value !== formatedDate) {
                    fc.setValue(formatedDate);
                    fc.markAsDirty();
                    this.measureDateMoment = momentDate;
                    this.model.measureDate = this.measureDateMoment;
                }
            }
        }
        else {
            if (leaveEmpty) {
                fc.setValue(null);
                fc.markAsDirty();
                this.model.measureDate = null;
            }
            else {
                this.setMeasureDate(moment.utc());
            }
        }
    };
    RwyFrictionComponent.prototype.validateMeasureDate = function (errorName) {
        return true;
    };
    RwyFrictionComponent.prototype.onMeasureDateInputChange = function (strDate) {
        if (strDate && strDate.length === 10) {
            var momentDate = moment.utc(strDate, this.dateFormat);
            if (momentDate.isValid()) {
                this.measureDateText = momentDate.format("YYYY/MM/DD, H:mm") + " UTC";
                var fc = this.frmGroup.get('measureDate');
                var formatedDate = momentDate.format(this.dateFormat);
                if (fc.value !== formatedDate) {
                    fc.setValue(formatedDate);
                    fc.markAsDirty();
                    this.measureDateMoment = momentDate;
                    this.model.measureDate = this.measureDateMoment;
                }
            }
            else {
                this.measureDateText = "";
                this.model.measureDate = null;
            }
        }
        else {
            this.measureDateText = "";
            this.model.measureDate = null;
        }
    };
    //General functions
    RwyFrictionComponent.prototype.isValidNumber = function (value) {
        return ((value != null) &&
            (value !== '') &&
            !isNaN(Number(value.toString())));
    };
    RwyFrictionComponent.prototype.isANumber = function (str) {
        return !/\D/.test(str);
    };
    //Export functions
    RwyFrictionComponent.prototype.isFormValid = function () {
        return true;
    };
    RwyFrictionComponent.prototype.exportComponentInternalData = function () {
        var data = {
            parent: this.model.parent,
            measureDate: this.measureDateMoment,
            coefficient: this.model.coefficient,
            temperature: this.model.temperature,
        };
        return data;
    };
    RwyFrictionComponent.prototype.exportRwyFriction = function () {
        //Prepare RwyFriction data
        var rwyFriction = {
            rfObsTime: this.model.measureDate,
            device: 130,
            temperature: this.model.temperature,
            unreliable: false,
            frictionCoefficients: +this.model.coefficient
        };
        return rwyFriction;
    };
    RwyFrictionComponent.prototype.cleanValues = function () {
        alert('Clear Component Values: AverageRwyFriction');
    };
    __decorate([
        core_1.Input('form'),
        __metadata("design:type", forms_1.FormGroup)
    ], RwyFrictionComponent.prototype, "mainForm", void 0);
    __decorate([
        core_1.Input('isReadOnly'),
        __metadata("design:type", Boolean)
    ], RwyFrictionComponent.prototype, "isReadOnly", void 0);
    __decorate([
        core_1.Input('model'),
        __metadata("design:type", Object)
    ], RwyFrictionComponent.prototype, "model", void 0);
    __decorate([
        core_1.Input('isAverage'),
        __metadata("design:type", Boolean)
    ], RwyFrictionComponent.prototype, "isAverage", void 0);
    RwyFrictionComponent = __decorate([
        core_1.Component({
            selector: 'rsc-rwyfriction',
            templateUrl: './app/dashboard/nsds/rsc/components/rsc-rwyfriction.component.html'
        }),
        __param(3, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [forms_1.FormBuilder,
            angular_l10n_1.TranslationService,
            rsc_service_1.RscService, Object])
    ], RwyFrictionComponent);
    return RwyFrictionComponent;
}());
exports.RwyFrictionComponent = RwyFrictionComponent;
//# sourceMappingURL=rsc-rwyfriction.component.js.map
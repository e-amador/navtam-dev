﻿define(["jquery", "Resources", "utilities", "kendoExt"], function ($, res, Util) {

    var thisIndex;

    return {
        
        init: function () {
            thisIndex = this;
            thisIndex.initUI();
        },


        initUI: function () {
            $("#btnChangePassword").click(function () {
                $("#ManageUserArea").show();
                $.ajax({
                    cache: false,
                    type: "GET",
                    dataType: "html",
                    url: "../Account/ManagePassword",
                    success: function(data) {
                        $("#ManageUserArea").html(data);
                    }
                });
            });
            $("#btnChangeDefaultLanguage").click(function () {
                $("#ManageUserArea").show();
                $.ajax({
                    cache: false,
                    type: "GET",
                    dataType: "html",
                    url: "../Account/ChangeDefaultLanguage",
                    success: function(data) {
                        $("#ManageUserArea").html(data);
                        var currentLanguage = document.getElementById('currentDefaultLanguage').value;
                        document.getElementById(currentLanguage.charAt(0).toUpperCase() + currentLanguage.slice(1)).checked = true;
                    }
                });
            });

            $("#btnChangeExpiredRecordsFilter").click(function () {
                $("#ManageUserArea").show();
                $.ajax({
                    cache: false,
                    type: "GET",
                    dataType: "html",
                    url: "../Account/ChangeExpiredRecordsFilter",
                    success: function(data) {
                        $("#ManageUserArea").html(data);
                    }
                });
            });

            $("#ButtonChangeExpiredRecordsFilterCancel").click(function () {
                $("#ManageUserArea").hide();
                window.history.go(-1);
                return true;
            });

            $("#btnChangeExpiredRecordsFilterCancel").click(function () {
                $("#ManageUserArea").hide();
                window.history.go(-1);
                return true;
            });

            $("#ButtonAccountManageCancel").click(function () {
                $("#ManageUserArea").hide();
            });


        }
    };
});
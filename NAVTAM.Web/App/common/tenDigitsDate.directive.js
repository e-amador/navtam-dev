"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var TenDigitsDateDirective = (function () {
    function TenDigitsDateDirective(elementRef) {
        this.elementRef = elementRef;
        this.el = this.elementRef.nativeElement;
    }
    TenDigitsDateDirective.prototype.ngOnInit = function () {
        this.el.value = this.el.value; //this.currencyPipe.transform(this.el.value);
    };
    TenDigitsDateDirective.prototype.onFocus = function (value) {
        this.el.value = "33"; //this.currencyPipe.parse(value); // opossite of transform
    };
    TenDigitsDateDirective.prototype.onBlur = function (value) {
        this.el.value = "44"; //this.currencyPipe.transform(value);
    };
    return TenDigitsDateDirective;
}());
__decorate([
    core_1.HostListener("focus", ["$event.target.value"]),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], TenDigitsDateDirective.prototype, "onFocus", null);
__decorate([
    core_1.HostListener("blur", ["$event.target.value"]),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], TenDigitsDateDirective.prototype, "onBlur", null);
TenDigitsDateDirective = __decorate([
    core_1.Directive({
        selector: '[tendigitsdate]'
    }),
    __metadata("design:paramtypes", [core_1.ElementRef])
], TenDigitsDateDirective);
exports.TenDigitsDateDirective = TenDigitsDateDirective;
//# sourceMappingURL=tenDigitsDate.directive.js.map
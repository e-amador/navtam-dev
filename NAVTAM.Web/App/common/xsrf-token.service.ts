import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';

@Injectable()
export class XsrfTokenService extends RequestOptions {

    constructor(private requestOptionArgs: RequestOptions) {
        super();
    }

    addForgeryTokenToHeader(xsrfToken: string) {
        //this.addHeader("Content-Type", "application/json");
        this.addHeader("RequestVerificationToken", xsrfToken);
    }

    private addHeader(headerName: string, headerValue: string) {
        (this.requestOptionArgs.headers as Headers).set(headerName, headerValue);
    }
}



﻿namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    using System.Data.Entity;
    using System.Threading.Tasks;

    using Contracts.Repositories;
    using Domain.Model.Entitities;

    public class UserPreferenceRepo : Repository<UserPreference>, IUserPreferenceRepo
    {
        public UserPreferenceRepo(DbContext dbContext)
            : base(dbContext)
        {
        }

        public async Task<UserPreference> GetExpiredAsync(int userPreferenceId)
        {
            return await DbSet.FirstOrDefaultAsync(e => e.Id == userPreferenceId);
        }
    }
}

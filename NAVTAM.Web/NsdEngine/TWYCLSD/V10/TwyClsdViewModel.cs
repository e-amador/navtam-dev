﻿using System.Collections.Generic;

namespace NAVTAM.NsdEngine.TWYCLSD.V10
{
    public class TwyClsdViewModel : TokenViewModel
    {
        public TwyClsdViewModel()
        {
            AllTaxiways = new List<TaxiwayViewModel>();
        }
        public List<TaxiwayViewModel> AllTaxiways { get; set; }
        public bool AllTaxiwaysSelected { get; set; }
        public int ClosureReasonId { get; set; }
        public string ClosureReasonFreeText { get; set; }
        public string ClosureReasonFreeTextFr { get; set; }

        public string SubjectLocation { get; set; }
    }


}
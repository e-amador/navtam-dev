﻿using System.Linq;

namespace RSC.Business.Helpers
{
    public static class AftnHelpers
    {
        const string ValidAftnChars = " ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-?:().,'=/+\"\r\n";

        public static bool IsValidAftnText(string text)
        {
            return string.IsNullOrEmpty(text) || text.All(c => ValidAftnChars.Contains(c));
        }
    }
}
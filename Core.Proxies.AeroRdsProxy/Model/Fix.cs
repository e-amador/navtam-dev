﻿using System;
using System.Collections.Generic;

namespace NavCanada.Core.Proxies.AeroRdsProxy.Model
{
    public class Fix
    {
        public string FixId { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string Status { get; set; }
        public string FixType { get; set; }
        public string Identifier { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public double LatitudeInDecimal { get; set; }
        public double LongitudeInDecimal { get; set; }
        public double? Elevation { get; set; }
        public double? ElevationInFeet { get; set; }
        public string UOMVerticalDistance { get; set; }
        public object ElevationAccuracy { get; set; }
        public string Type { get; set; }
        public string Class { get; set; }
        public string FIR { get; set; }
        public string Name { get; set; }
        public string Remark { get; set; }
        public string Datum { get; set; }
        public string FK_Organization { get; set; }
        public object FK_Aerodrome { get; set; }
        public double? GeoidUndulation { get; set; }
        public object FK_Service { get; set; }
        public GeographicalLocation GeographicalLocation { get; set; }
        public string CyclicRedundancyCheck { get; set; }
        public object ReferenceToVorId { get; set; }
        public object Channel { get; set; }
        public object UOMGeographicalAccuracy { get; set; }
        public object GeographicalAccuracy { get; set; }
        public double? MagneticVariation { get; set; }
        public object IlsPosition { get; set; }
        public object Emission { get; set; }
        public string MagneticVariationDate { get; set; }
        public string FK_Timetable { get; set; }
        public object Display { get; set; }
        public string NorthReference { get; set; }
        public string StationDeclination { get; set; }
        public object ReferenceToNdbId { get; set; }
        public object ReferenceToIlsId { get; set; }
        public object MinorAxisTrueBearing { get; set; }
        public string Frequency { get; set; }
        public object UOMDisplacement { get; set; }
        public string UOMFrequency { get; set; }
        public object VerticalDatum { get; set; }
        public int SEQ { get; set; }
    }

    public class Fixes
    {
        public IList<Fix> Value { get; set; }
    }
}
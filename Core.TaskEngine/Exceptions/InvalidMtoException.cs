﻿namespace NavCanada.Core.TaskEngine.Exceptions
{
    using System;

    using Mto;

    public class InvalidMtoException : Exception
    {
        public InvalidMtoException(string message, Exception innerException, MessageTransferObject mto)
            : base(message, innerException)
        {
            Mto = mto;
        }

        public InvalidMtoException(string message, MessageTransferObject mto)
            : base(message)
        {
            Mto = mto;
        }

        public MessageTransferObject Mto 
        { 
            get;
            set;
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace NavCanada.Core.Proxies.AeroRdsProxy.Model
{
    public class RunwayDirection
    {
        public string RunwayDirectionId { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string Status { get; set; }
        public string Designator { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string LatitudeInDecimal { get; set; }
        public string LongitudeInDecimal { get; set; }
        public double? TrueBearing { get; set; }
        public double? MagneticBearing { get; set; }
        public string TDZElevation { get; set; }
        public string TDZElevationAccuracy { get; set; }
        public string UOMElevationTDZ { get; set; }
        public string TaxiTimeDuration { get; set; }
        public string VasisType { get; set; }
        public string VasisPosition { get; set; }
        public string VasisNumberOfBoxes { get; set; }
        public string PortableVasis { get; set; }
        public string VasisSlopeAngle { get; set; }
        public string MinimunEyeHeight { get; set; }
        public string UOMMinimunEyeHeight { get; set; }
        public string ArrestingDeviceDescription { get; set; }
        public string RVRDescription { get; set; }
        public string VFRPatternDirection { get; set; }
        public string Remark { get; set; }
        public string FK_Runway { get; set; }
        public GeographicalLocation GeographicalLocation { get; set; }
        public int SEQ { get; set; }

        public List<RunwayDirectionDeclaredDistance> RunwayDirectionDeclaredDistances { get; set; }
    }

    public class RunwayDirections
    {
        public IList<RunwayDirection> Value { get; set; }
    }
}

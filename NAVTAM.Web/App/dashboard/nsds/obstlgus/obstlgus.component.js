"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ObstLgUsComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var data_service_1 = require("../../shared/data.service");
var location_service_1 = require("../../shared/location.service");
var obstlgus_service_1 = require("./obstlgus.service");
var angular_l10n_1 = require("angular-l10n");
var ObstLgUsComponent = /** @class */ (function () {
    function ObstLgUsComponent(injector, fb, locationService, obstacleService, dataService, translation) {
        this.injector = injector;
        this.fb = fb;
        this.locationService = locationService;
        this.obstacleService = obstacleService;
        this.dataService = dataService;
        this.translation = translation;
        this.action = "";
        this.token = null;
        this.isReadOnly = false;
        this.obstacleTypes = [];
        //Validations limit
        this.upperLimit = 999;
        this.upperLimitFT = 99999;
        this.radiusLimit = 999;
        this.maxElevation = 99999;
        this.maxHeight = 9999;
        this.token = this.injector.get('token');
        this.isReadOnly = this.injector.get('isReadOnly');
        this.nsdForm = this.injector.get('form');
        this.action = this.injector.get('type');
    }
    ObstLgUsComponent.prototype.ngOnInit = function () {
        this.isFormLoadingData = !(!this.token.subjectId);
        var app = this.obstacleService.app;
        this.leafletmap = app.leafletmap;
        //this.configureReactiveForm();
        var frmArr = this.nsdForm.controls['frmArr'];
        if (frmArr.length === 0) {
            this.configureReactiveForm();
        }
        else {
            /*
            this is required because of the grouping funtionallity. When the NsdFormGroup
            was configured and any of items in the group has changed (new set of tokens values)
            the NsdForm Group needs to be updated with the new tokens values */
            this.updateReactiveForm(frmArr);
        }
        this.loadObstacleTypes();
        this.showDoaMapRegion(app.doaId);
    };
    ObstLgUsComponent.prototype.ngOnDestroy = function () {
        //if (this.leafletmap) {
        //    this.leafletmap.dispose();
        //    this.leafletmap = null;
        //}
    };
    ObstLgUsComponent.prototype.$ctrl = function (name) {
        return this.nsdFrmGroup.get(name);
    };
    ObstLgUsComponent.prototype.objectTypeSelectOptions = function () {
        return {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.ObjectTypePlaceholder')
            }
        };
    };
    ObstLgUsComponent.prototype.validateLocation = function () {
        var fc = this.nsdFrmGroup.get('location');
        return !fc.errors || this.nsdForm.pristine;
    };
    ObstLgUsComponent.prototype.validateRequiredLocation = function () {
        var fc = this.$ctrl('location');
        if (this.nsdForm.pristine)
            return false;
        return fc.errors && fc.errors.required;
    };
    ObstLgUsComponent.prototype.isLocationFormatInvalid = function () {
        var fc = this.$ctrl('location');
        if (this.nsdForm.pristine)
            return false;
        return fc.errors && fc.errors.format;
    };
    ObstLgUsComponent.prototype.isInvalidLocation = function () {
        var fc = this.$ctrl('location');
        if (this.nsdForm.pristine)
            return false;
        return fc.errors && fc.errors.invalidLocation;
    };
    ObstLgUsComponent.prototype.isValidatingLocation = function () {
        var fc = this.$ctrl('location');
        if (this.nsdForm.pristine)
            return false;
        return fc.errors && fc.errors.validatingLocation;
    };
    ObstLgUsComponent.prototype.invalidObstacleType = function () {
        var ctrl = this.$ctrl('obstacleType');
        return !this.isReadOnly && this.$ctrl('obstacleType').invalid;
    };
    ObstLgUsComponent.prototype.validateObstacleTypeId = function () {
        var fc = this.nsdFrmGroup.get('obstacleType');
        return fc.value !== -1 || this.nsdForm.pristine;
    };
    ObstLgUsComponent.prototype.validateHeight = function () {
        if (this.nsdForm.pristine)
            return true;
        var fc = this.$ctrl('height');
        if (fc.errors)
            return false;
        if (fc.value !== null) {
            if (+fc.value > this.maxHeight)
                return false;
            if (+fc.value < 0)
                return false;
        }
        else
            return false;
        return true;
    };
    ObstLgUsComponent.prototype.validateElevation = function () {
        if (this.nsdForm.pristine)
            return true;
        var fc = this.$ctrl('elevation');
        if (fc.errors)
            return false;
        if (fc.value !== null) {
            if (+fc.value > this.maxElevation)
                return false;
            if (+fc.value < 0)
                return false;
        }
        else
            return false;
        return true;
    };
    ObstLgUsComponent.prototype.decHeightValue = function () {
        var radCtrl = this.$ctrl('height');
        if (+radCtrl.value > 0) {
            radCtrl.setValue(+radCtrl.value - 1);
            radCtrl.markAsDirty();
        }
    };
    ObstLgUsComponent.prototype.incHeightValue = function () {
        var radCtrl = this.$ctrl('height');
        if (+radCtrl.value < this.maxHeight) {
            radCtrl.setValue(+radCtrl.value + 1);
            radCtrl.markAsDirty();
        }
    };
    ObstLgUsComponent.prototype.decElevationValue = function () {
        var radCtrl = this.$ctrl('elevation');
        if (+radCtrl.value > 0) {
            radCtrl.setValue(+radCtrl.value - 1);
            radCtrl.markAsDirty();
        }
    };
    ObstLgUsComponent.prototype.incElevationValue = function () {
        var radCtrl = this.$ctrl('elevation');
        if (+radCtrl.value < this.maxElevation) {
            radCtrl.setValue(+radCtrl.value + 1);
            radCtrl.markAsDirty();
        }
    };
    ObstLgUsComponent.prototype.validateKeyPressNumber = function (evt) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
        }
    };
    ObstLgUsComponent.prototype.setCheckBoxValue = function (name) {
        this.$ctrl(name).value === true;
    };
    ObstLgUsComponent.prototype.setObjectType = function ($event) {
        var obstTypeCtrl = this.$ctrl('obstacleType');
        var newValue = $event.value;
        if (newValue !== "-1") {
            if (newValue !== obstTypeCtrl.value) {
                obstTypeCtrl.setValue(newValue);
                obstTypeCtrl.markAsDirty();
            }
            this.refreshMap();
        }
        else {
            //??
        }
    };
    ObstLgUsComponent.prototype.loadObstacleTypes = function () {
        var _this = this;
        this.obstacleService
            .getObstacleTypes()
            .subscribe(function (obstacleTypes) {
            var obstacles = obstacleTypes.map(function (item) {
                return {
                    id: item.id,
                    text: item.name
                };
            });
            // make the 'token.obstacleType' value the first on the list for "optionless convenience" or angular will throw an exception!
            _this.makeFirstElement(obstacles, function (obst) { return obst.id === _this.token.obstacleType; }, true);
            _this.obstacleTypes = obstacles;
        });
    };
    ObstLgUsComponent.prototype.makeFirstElement = function (arr, makeFirst, insertUndefinedAtZero) {
        if (insertUndefinedAtZero === void 0) { insertUndefinedAtZero = true; }
        var len = arr.length;
        for (var i = 0; i < len; i++) {
            if (makeFirst(arr[i])) {
                this.swapArrayPositions(arr, 0, i);
                return;
            }
        }
        if (insertUndefinedAtZero) {
            arr.unshift({
                id: -1,
                text: '',
            });
        }
        //this.swapArrayPositions(arr, 0, len);
    };
    ObstLgUsComponent.prototype.swapArrayPositions = function (arr, p1, p2) {
        var temp = arr[p1];
        arr[p1] = arr[p2];
        arr[p2] = temp;
    };
    ObstLgUsComponent.prototype.radioButtonChanged = function (name) {
        this.$ctrl(name).markAsDirty();
    };
    ObstLgUsComponent.prototype.hasLowerCase = function (str) {
        return (/[a-z]/.test(str));
    };
    ObstLgUsComponent.prototype.locationChanged = function (value) {
        var _this = this;
        var tmp = this.$ctrl('location').value;
        if (tmp !== null && this.hasLowerCase(tmp))
            this.$ctrl('location').patchValue(tmp.toUpperCase());
        if (value !== null && this.hasLowerCase(value))
            value = value.toUpperCase();
        if (this.leafletmap === null)
            this.leafletmap = this.obstacleService.app.leafletmap;
        var pair = this.locationService.parse(value);
        var geojson = pair ? this.leafletmap.latitudeLongitudeToGeoJson(pair.latitude, pair.longitude) : null;
        if (geojson) {
            this.obstacleService
                .validateLocation(value)
                .subscribe(function (result) {
                var locCtrl = _this.$ctrl('location');
                var currentLocation = locCtrl.value;
                if (currentLocation === value) {
                    if (result.inDoa) {
                        _this.leafletmap.clearFeaturesOnLayers();
                        _this.leafletmap.addMarker(geojson, _this.getSelectedObstacleName());
                        _this.leafletmap.setNewMarkerLocation(geojson);
                        locCtrl.setErrors(null);
                        _this.triggerFormChangeEvent(value);
                    }
                    else {
                        locCtrl.setErrors({ invalidLocation: true });
                    }
                }
            }, function (error) {
                var locCtrl = _this.$ctrl('location');
                locCtrl.setErrors({ invalidLocation: true });
            });
        }
    };
    ObstLgUsComponent.prototype.triggerFormChangeEvent = function (value) {
        this.$ctrl('refreshDummy').setValue(value);
    };
    ObstLgUsComponent.prototype.refreshMap = function () {
        var _this = this;
        window.setTimeout(function () { return _this.locationChanged(_this.$ctrl('location').value); }, 1000);
    };
    ObstLgUsComponent.prototype.getSelectedObstacleName = function () {
        var obstId = this.$ctrl('obstacleType').value;
        if (!obstId || !this.obstacleTypes)
            return "OBST";
        var obstacle = this.obstacleTypes.find(function (obst) { return obst.id === obstId; });
        return obstacle ? obstacle.text : "OBST";
    };
    ObstLgUsComponent.prototype.configureReactiveForm = function () {
        var _this = this;
        var frmArrControls = this.nsdForm.controls['frmArr'];
        this.nsdFrmGroup = this.fb.group({
            obstacleType: [{ value: this.token.obstacleType || -1, disabled: this.isReadOnly }, [obstacleTypeValidator]],
            location: [{ value: this.token.location, disabled: this.isReadOnly }, [forms_1.Validators.required, validateLocation(this.locationService)]],
            height: [{ value: this.token.height, disabled: this.isReadOnly }, []],
            elevation: [{ value: this.token.elevation, disabled: this.isReadOnly }, []],
            refreshDummy: ""
        });
        this.$ctrl("elevation").setValidators(elevationValidatorFn(this.maxElevation));
        this.$ctrl("height").setValidators(elevationValidatorFn(this.maxHeight));
        this.$ctrl("location").valueChanges.debounceTime(1000).subscribe(function (value) { return _this.locationChanged(value); });
        frmArrControls.push(this.nsdFrmGroup);
    };
    ObstLgUsComponent.prototype.updateReactiveForm = function (formArr) {
        this.nsdFrmGroup = formArr.controls[0];
        //It is better to update only those controls that tokens values have changed.
        this.updateFormControlValue("obstacleType", this.token.obstacleType);
        this.updateFormControlValue("location", this.token.location);
        this.updateFormControlValue("height", this.token.height);
        this.updateFormControlValue("elevation", this.token.elevation);
    };
    ObstLgUsComponent.prototype.updateFormControlValue = function (controlName, value) {
        var control = this.nsdFrmGroup.get(controlName);
        if (control && control.value !== value) {
            control.setValue(value);
        }
    };
    ObstLgUsComponent.prototype.showDoaMapRegion = function (doaId) {
        var _this = this;
        this.dataService
            .getDoaLocation(doaId)
            .subscribe(function (geoJsonStr) { return _this.renderMap(geoJsonStr); });
    };
    ObstLgUsComponent.prototype.renderMap = function (geoJsonStr) {
        var self = this;
        var interval = window.setInterval(function () {
            if (self.leafletmap && self.leafletmap.isReady()) {
                window.clearInterval(interval);
                self.leafletmap.addDoa(geoJsonStr, 'DOA');
            }
        }, 100);
    };
    ObstLgUsComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/dashboard/nsds/obstlgus/obstlgus.component.html'
        }),
        __metadata("design:paramtypes", [core_1.Injector,
            forms_1.FormBuilder,
            location_service_1.LocationService,
            obstlgus_service_1.ObstacleLgUsService,
            data_service_1.DataService,
            angular_l10n_1.TranslationService])
    ], ObstLgUsComponent);
    return ObstLgUsComponent;
}());
exports.ObstLgUsComponent = ObstLgUsComponent;
function obstacleTypeValidator(c) {
    if (c.value === -1) {
        return { 'required': true };
    }
    return null;
}
function validateLocation(locationService) {
    return function (ctrl) {
        var location = ctrl.value;
        if (!location)
            return { required: true };
        if (locationService.parse(location) === null)
            return { format: true };
        return null;
    };
}
function elevationValidatorFn(maxElevation) {
    return function (c) {
        if (typeof (c.value) === "string") {
            if (!c.value) {
                return { 'required': true };
            }
            if (c.value === "") {
                return { 'required': true };
            }
        }
        if (isANumber(c.value)) {
            if (+c.value < 0 || +c.value > maxElevation) {
                return { 'invalid': true };
            }
            var v = +c.value;
            if (v > maxElevation) {
                return { 'invalid': true };
            }
            return null;
        }
        return { 'invalid': true };
    };
}
function heightValidatorFn(maxHeight) {
    return function (c) {
        if (typeof (c.value) === "string") {
            if (!c.value) {
                return { 'required': true };
            }
            if (c.value === "") {
                return { 'required': true };
            }
        }
        if (isANumber(c.value)) {
            if (+c.value < 0 || +c.value > maxHeight) {
                return { 'invalid': true };
            }
            var v = +c.value;
            if (v > maxHeight) {
                return { 'invalid': true };
            }
            return null;
        }
        return { 'invalid': true };
    };
}
function isANumber(str) {
    return !/\D/.test(str);
}
//# sourceMappingURL=obstlgus.component.js.map
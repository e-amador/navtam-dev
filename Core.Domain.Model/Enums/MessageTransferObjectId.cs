﻿namespace NavCanada.Core.Domain.Model.Enums
{
    /// <summary>
    /// Unique message id for MTOs (message transfer objects). 
    /// </summary>
    /// <remarks>Note that these do NOT correspond to the OBU protocol type values</remarks>
    public enum MessageTransferObjectId
    {
        /// <summary>
        /// Default -- this is not handled, use to indicate unknown or unhandled type of inbound message
        /// </summary>
        Unhandled = 0,

        /// <summary>
        /// Disseminate Notam to external queue
        /// </summary>
        DisseminateNotam = 2,
        
        /// <summary>
        /// Distribute message to AFTN queue
        /// </summary>
        DistributeAftnBroadcastMessage = 3,
        
        /// <summary>
        /// Distribute message to clients queue
        /// </summary>
        DistributeClientSubscriptionMessages = 4,

        /// <summary>
        /// Disseminate Notam to external queue
        /// </summary>
        PublishMessageGroup = 5,

        /// <summary>
        /// Send email message
        /// </summary>
        SendEmailNotitication = 6,

        /// <summary>
        /// Expire Notam Proposal
        /// </summary>
        ExpireNotamProposal = 70,

        /// <summary>
        /// End Notam Proposal
        /// </summary>
        EndNotamProposal = 75,

        /// <summary>
        /// Notam Proposal Signalr Notification  
        /// </summary>
        NotamProposalSignalr = 80,

        /// <summary>
        /// Delete Completed Records Scheduler Msessage
        /// </summary>
        DeleteCompletedRecordsSchedulerMsg = 100,

        /// <summary>
        /// NDSRequest Message waiting to be processed
        /// </summary>
        ProcessNdsRequestMsg = 110,

        /// <summary>
        /// Process Message Exchange Queue task
        /// </summary>
        ProccessExchangeMsg = 111,

        /// <summary>
        /// Execute global sync of AeroRDS data
        /// </summary>
        AeroRDSSyncActiveDataMsg = 120,

        /// <summary>
        /// Execute AIRAC sync of AeroRDS data
        /// </summary>
        AeroRDSCacheEffectiveDataMsg = 121,

        /// <summary>
        /// Execute AIRAC sync of AeroRDS data
        /// </summary>
        AeroRDSLoadCachedDataMsg = 122,

        /// <summary>
        /// Process a disseminate job (transition)
        /// </summary>
        ProcessDisseminateJobMsg = 130,

        /// <summary>
        /// Process a disseminate job (transition)
        /// </summary>
        QueueNextDisseminateJobMsg = 131,

        /// <summary>
        /// Disseminate the checklist messages
        /// </summary>
        DisseminateChecklistsMsg = 132,

        /// <summary>
        /// Check series numbers to alert about possible roll ups
        /// </summary>
        CheckSeriesNumbersMsg = 133,

        /// <summary>
        /// KeepAlive Scheduler Msessage
        /// </summary>
        KeepAliveSchedulerMsg = 240,

        /// <summary>
        /// Simulate Disseminate NOTAM (For Testing Purposes)
        /// </summary>
        SimulatePublishNotamMsg = 245,

        /// <summary>
        /// Dignostic Mto
        /// </summary>
        Diagnostic = 250,

        /// <summary>
        /// Tests that Hub connection is still up
        /// </summary>
        HubConnectionTestMsg = 251,
    }
}

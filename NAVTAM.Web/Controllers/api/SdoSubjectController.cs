﻿using alatas.GeoJSON4EntityFramework;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using NAVTAM.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [Authorize]
    [RoutePrefix("api/sdosubjects")]
    public class SdoSubjectController : ApiBaseController
    {
        public SdoSubjectController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger) 
            : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
        }

        [HttpGet]
        [Route("CatchAllFilter")]
        public async Task<IHttpActionResult> CatchAllFilter([FromUri] ResourceQuery res)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                var doaGeoArea = await GeoLocationCache.GetUserDoa(Uow, ApplicationContext.GetUserId());
                var subjects = await Uow.SdoCacheRepo.FilterCatchAllSubjectsAsync(res.Query, res.Size, doaGeoArea);

                return Ok(subjects);

            }, "api/sdosubjects/CatchAllFilter");
        }

        [HttpGet]
        [Route("RwyClosureFilter")]
        public async Task<IHttpActionResult> GetAllAhpsWithRwysAsync([FromUri] ResourceQuery res)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                var doaGeoArea = await GeoLocationCache.GetUserDoa(Uow, ApplicationContext.GetUserId());
                var subjects = await Uow.SdoCacheRepo.FilterAerodromesWithRunwaysAsync(res.Query, res.Size, doaGeoArea);

                return Ok(subjects);

            }, "api/sdosubjects/RwyClosureFilter");
        }

        [HttpGet]
        [Route("GetAllFIRAsync")]
        public async Task<IHttpActionResult> GetAllFIRAsync()
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                var subjects = await Uow.SdoCacheRepo.GetFirsAsync();
                if (subjects.Count == 0)
                    return NotFound();

                var firs = subjects.Select(s => new { s.Id, s.Designator, s.Name });

                return Ok(firs);

            }, "api/sdosubjects/GetAllFIRAsync");
        }

        [HttpGet]
        [Route("Aerodromes")]
        public async Task<IHttpActionResult> GetAllAerodromesAsync()
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                var aerodromes = await Uow.SdoCacheRepo.GetAerodromesAndFirsAsync();

                return Ok(aerodromes);

            }, "api/sdosubjects/Aerodromes");
        }

        [HttpGet]
        [Route("GetAllRwyByAhpIdAsync")]
        public async Task<IHttpActionResult> GetAllRwyByAhpIdAsync(string ahpId)
        {
            return await LogAction(async () =>
            {
                if (!ValidUserRequest())
                    return Unauthorized();

                var subjects = await Uow.SdoCacheRepo.GetAerodromeRunwaysAsync(ahpId);
                if (subjects.Count == 0)
                    return NotFound();

                var responses = new List<object>(subjects.Count);
                foreach (var subject in subjects)
                {
                    // >> var featureGeo = new FeatureCollection(subject.SubjectGeo);
                    var featureMark = new FeatureCollection(subject.SubjectGeoRefPoint);
                    var isBilingualRegion = GeoDataService.IsInBilingualRegion(subject.SubjectGeoRefPoint, Uow);

                    var response = new
                    {
                        Id = subject.Id,
                        Name = subject.Name,
                        Designator = subject.Designator,
                        SdoEntityName = subject.SdoEntityName,
                        // >> SubjectGeo = featureGeo.Serialize(prettyPrint: false),
                        SubjectGeoRefPoint = featureMark.Serialize(prettyPrint: false),
                        IsBilingualRegion = isBilingualRegion
                    };

                    responses.Add(response);
                }

                return Ok(responses);

            }, "api/sdosubjects/GetAllRwyByAhpIdAsync");
        }

        [HttpGet]
        [Route("Find")]
        public async Task<IHttpActionResult> Find(string subId)
        {
            return await LogAction(async () =>
            {
                var subject = await Uow.SdoCacheRepo.GetByIdAsync(subId);
                if (subject == null)
                    return NotFound();

                var ftGeo = new FeatureCollection(subject.Geography ?? subject.RefPoint); // Todo: request geography for aerodromes
                var ftMark = new FeatureCollection(subject.RefPoint);
                var isBilingualRegion = GeoDataService.IsInBilingualRegion(subject.RefPoint, Uow);
                var servedCity = Uow.SdoCacheRepo.GetServedCity(subject);

                var response = new
                {
                    Id = subject.Id,
                    Name = subject.Name,
                    Designator = subject.Designator,
                    SdoEntityName = subject.Type == SdoEntityType.Fir ? "Fir" : "Ahp",
                    SubjectGeo = ftGeo.Serialize(prettyPrint: false),
                    SubjectGeoRefPoint = ftMark.Serialize(prettyPrint: false),
                    IsBilingualRegion = isBilingualRegion,
                    ServedCity = servedCity,
                };

                return Ok(response);

            }, $"api/sdosubjects/Find/{subId}");
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        [Route("updatekeywords")]
        public async Task<IHttpActionResult> UpdateAerodromeNames()
        {
            Uow.BeginTransaction();
            try
            {
                await Uow.SdoCacheRepo.UpdateKeywords();
                await Uow.SaveChangesAsync();
                Uow.Commit();
            }
            catch (Exception)
            {
                Uow.Rollback();
            }
            return Ok("");
        }
    }
}
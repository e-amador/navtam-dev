﻿namespace NavCanada.Core.Data.NotamWiz.EF
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;

    using Microsoft.AspNet.Identity.EntityFramework;

    using Contracts;
    using Configurations;
    using Migrations;
    using Domain.Model.Entitities;
    using Data.EF.Configurations;
    using System;
    using NavCanada.Core.Common.Security;
    using System.Configuration;

    public class AppDbContextInitializer : DropCreateDatabaseIfModelChanges<AppDbContext>
    {
        protected override void Seed(AppDbContext dbContext)
        {
            base.Seed(dbContext);
        }
    }

    public class AppDbContext : IdentityDbContext<UserProfile>, IAppDbContext 
    {
/*
        static AppDbContext()
        {
            //Database.SetInitializer(new AppDbContextInitializer());
        }
*/

        public AppDbContext(string connectionStringName) : base(nameOrConnectionString: GetConnectionString(connectionStringName ?? "DefaultConnection"))
        {
            // Uncomment to trace the EF queries to the de Ouput \ Debug window
            // >> Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
        }

        public AppDbContext() : this(GetConnectionString("DefaultConnection"))
        {
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<AppDbContext, Configuration>("DefaultConnection"));
        }

        //Use oo configure Owin Auth
        public static AppDbContext Create()
        {
            return new AppDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Use singular table names
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            ConfigureEfIdentityProvider(modelBuilder);

            //modelBuilder.Configurations.Add(new BilingualRegionConfig());

            modelBuilder.Configurations.Add(new ConfigurationValueConfig());
            modelBuilder.Configurations.Add(new AerodromeDisseminationCategoryConfig());
            modelBuilder.Configurations.Add(new DigitalSlaConfig());
            modelBuilder.Configurations.Add(new DoaConfig());
            modelBuilder.Configurations.Add(new UserDoaConfig());
            modelBuilder.Configurations.Add(new OrganizationDoaConfig());
            modelBuilder.Configurations.Add(new FeedbackConfig());
            modelBuilder.Configurations.Add(new SeriesAllocationConfig());
            modelBuilder.Configurations.Add(new GeoRegionConfig());
            modelBuilder.Configurations.Add(new GroupConfig());
            modelBuilder.Configurations.Add(new IcaoSubjectConfig());
            modelBuilder.Configurations.Add(new IcaoSubjectConditionConfig());
            modelBuilder.Configurations.Add(new SeriesNumberConfig());
            modelBuilder.Configurations.Add(new NotificationTemplateConfig());
            modelBuilder.Configurations.Add(new NotificationTrackingConfig());
            modelBuilder.Configurations.Add(new OrganizationConfig());
            modelBuilder.Configurations.Add(new OrganizationNsdCategoryConfig());
            modelBuilder.Configurations.Add(new PermissionConfig());
            modelBuilder.Configurations.Add(new RegistrationConfig());
            modelBuilder.Configurations.Add(new RolePermissionConfig());
            modelBuilder.Configurations.Add(new SdoCacheConfig());
            modelBuilder.Configurations.Add(new UserPreferenceConfig());
            modelBuilder.Configurations.Add(new PoisonMtoConfig());
            modelBuilder.Configurations.Add(new PoisonMtoRecordConfig());

            modelBuilder.Configurations.Add(new ScheduleJobConfig());
            modelBuilder.Configurations.Add(new NsdCategoryConfig());
            modelBuilder.Configurations.Add(new ScheduleTaskConfig());
            modelBuilder.Configurations.Add(new ProposalConfig());
            modelBuilder.Configurations.Add(new ProposalHistoryConfig());
            modelBuilder.Configurations.Add(new ProposalAttachmentConfig());
            modelBuilder.Configurations.Add(new NotamConfig());
            modelBuilder.Configurations.Add(new NotamTopicConfig());
            modelBuilder.Configurations.Add(new UserQueryFilterConfig());
            modelBuilder.Configurations.Add(new UserQueryFilterNameConfig());

            modelBuilder.Configurations.Add(new NdsOutgoingMessageConfig());
            modelBuilder.Configurations.Add(new NdsNotamMessageConfig());
            modelBuilder.Configurations.Add(new NdsIncomingRequestConfig());

            modelBuilder.Configurations.Add(new NdsClientConfig());
            modelBuilder.Configurations.Add(new NdsClientSeriesConfig());
            modelBuilder.Configurations.Add(new NdsClientItemAConfig());

            modelBuilder.Configurations.Add(new NofTemplateMessageRecordConfig());
            modelBuilder.Configurations.Add(new MessageExchangeTemplateConfig());
            modelBuilder.Configurations.Add(new ItemDConfig());

            modelBuilder.Configurations.Add(new DisseminationJobConfig());
            modelBuilder.Configurations.Add(new SeriesChecklistConfig());

            modelBuilder.Configurations.Add(new AeroRDSCacheConfig());
            modelBuilder.Configurations.Add(new AeroRDSCacheStatusConfig());
            modelBuilder.Configurations.Add(new UserRegistrationConfig());
            modelBuilder.Configurations.Add(new NotifySeriesConfig());
            modelBuilder.Configurations.Add(new OriginatorInfoConfig());
            modelBuilder.Configurations.Add(new SystemStatusConfig());
            
            modelBuilder.Configurations.Add(new LogConfig());
            
            modelBuilder.Configurations.Add(new AerodromeRSCConfig());

            modelBuilder.Configurations.Add(new ProposalAcknowledgementConfig());
        }

        private void ConfigureEfIdentityProvider(DbModelBuilder modelBuilder)
        {
            //NOTE: Apparently the way EF IdentityProvider initialize and configure the provider is different 
            //that when using the Confguration classes. To avoid the issue of Proxy creation we need to do the 
            //configuration of the Identity Provider in here instead.

            modelBuilder.Entity<IdentityUserRole>().ToTable("UserRole");
            modelBuilder.Entity<IdentityRole>().ToTable("Role");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("UserLogin");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("UserClaim");

            //UserProfile Configuration
            modelBuilder.Entity<ApplicationRole>()
                .Property(e=> e.Name).IsRequired();

            //UserProfile Configuration
            var userProfileEntity = modelBuilder.Entity<UserProfile>()
                .ToTable("User");

            userProfileEntity.Property(e => e.DefaultLanguage).IsRequired();
            userProfileEntity.Property(e => e.FirstName).IsRequired();
            userProfileEntity.Property(e => e.LastName).IsRequired();
            userProfileEntity.Property(e => e.Position).IsRequired();
            userProfileEntity.Property(e => e.Position).IsRequired();
            userProfileEntity.Property(e => e.UserName).HasMaxLength(20);

            userProfileEntity.HasOptional(e => e.Organization)
                .WithMany(s => s.Users)
                .HasForeignKey(e => e.OrganizationId)
                .WillCascadeOnDelete(false);

            userProfileEntity.HasOptional(e => e.UserPreference)
                .WithMany()
                .HasForeignKey(e => e.UserPreferenceId)
                .WillCascadeOnDelete(false);

            userProfileEntity.HasOptional(e => e.DigitalSla)
                .WithMany()
                .HasForeignKey(e => e.DigitalSlaId)
                .WillCascadeOnDelete(false);
        }

        static string GetConnectionString(string nameOrConnectionString)
        {
            var value = ConfigurationManager.ConnectionStrings[nameOrConnectionString]?.ConnectionString;

            if (string.IsNullOrEmpty(value))
                return nameOrConnectionString;

            return CipherUtils.DecryptConnectionString(value);
        }

        #region IAppDbContext

        public IDbSet<AerodromeDisseminationCategory> AerodromeDisseminationCategories { get; set; }
        public IDbSet<ConfigurationValue> ConfigurationValues { get; set; }
        public IDbSet<DigitalSla> DigitalSlas { get; set; }
        public IDbSet<Doa> Doas { get; set; }
        public IDbSet<UserDoa> UserDoas { get; set; }
        public IDbSet<GeoRegion> GeoRegions { get; set; }
        public IDbSet<SeriesAllocation> SeriesAllocations { get; set; }
        public IDbSet<Feedback> Feedbacks { get; set; }
        public IDbSet<Group> Groups { get; set; }
        public IDbSet<IcaoSubject> IcaoSubjects { get; set; }
        public IDbSet<IcaoSubjectCondition> IcaoSubjectConditions { get; set; }
        public IDbSet<SeriesNumber> SeriesNumbers { get; set; }
        public IDbSet<NotificationTemplate> NotificationTemplates { get; set; }
        public IDbSet<NotificationTracking> NotificationTrackings { get; set; }
        public IDbSet<Organization> Organizations { get; set; }
        public IDbSet<OrganizationDoa> OrganizationDoas { get; set; }
        public IDbSet<OrganizationNsdCategory> OrganizationNsds { get; set; }
        public IDbSet<Permission> Permissions { get; set; }
        public IDbSet<Registration> Registrations { get; set; }
        public IDbSet<RolePermission> RolePermissions { get; set; }
        public IDbSet<SdoCache> SdoCaches { get; set; }
        public IDbSet<UserPreference> UserPreferences { get; set; }
        public IDbSet<PoisonMtoRecord> PoisonMtoRecords { get; set; }
        public IDbSet<PoisonMto> PoisonMtos { get; set; }
        public IDbSet<ScheduleTask> ScheduleTasks { get; set; }
        public IDbSet<NsdCategory> NsdCategories { get; set; }
        public IDbSet<Proposal> Proposals { get; set; }
        public IDbSet<ProposalHistory> ProposalHistories { get; set; }
        public IDbSet<ProposalAttachment> ProposalAttachments { get; set; }
        public IDbSet<ScheduleJob> ScheduleJobs { get; set; }
        public IDbSet<Notam> Notams { get; set; }
        public IDbSet<NotamTopic> NotamTopics { get; set; }
        public IDbSet<UserQueryFilter> UserQueryFilters { get; set; }
        public IDbSet<NdsClient> NdsClients { get; set; }
        public IDbSet<NdsClientSeries> NdsClientSeries { get; set; }
        public IDbSet<NdsClientItemA> NdsClientItemsA { get; set; }
        public IDbSet<NdsNotamMessage> NdsNotamMessages { get; set; }
        public IDbSet<NdsOutgoingMessage> NdsOutgoingMessages { get; set; }
        public IDbSet<NdsIncomingRequest> NdsIncomingRequests { get; set; }
        public IDbSet<MessageExchangeQueue> MessageExchangeQueues { get; set; }
        public IDbSet<MessageExchangeTemplate> MessageExchangeTemplates { get; set; }
        public IDbSet<NofTemplateMessageRecord> NofTemplateMessageRecords { get; set; }
        public IDbSet<ItemD> ItemDs { get; set; }
        public IDbSet<AeroRDSCache> AeroRDSCaches { get; set; }
        public IDbSet<AeroRDSCacheStatus> AeroRDSCacheStatus  { get; set; }
        public IDbSet<UserRegistration> UserRegistrations { get; set; }
        public IDbSet<NotifySeries> NotifySeries { get; set; }
        public IDbSet<OriginatorInfo> OriginatorInfos { get; set; }
        public IDbSet<SystemStatus> SystemStatus { get; set; }
        public IDbSet<Log> Logs { get; set; }
        public IDbSet<AerodromeRSC> AerodromeRSCs { get; set; }

        #endregion
    }
}

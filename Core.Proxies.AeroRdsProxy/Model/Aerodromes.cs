﻿using System;
using System.Collections.Generic;

namespace NavCanada.Core.Proxies.AeroRdsProxy.Model
{
    public class Aerodrome
    {
        public string AerodromeId { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string Status { get; set; }
        public string FK_Organization { get; set; }
        public string Identifier { get; set; }
        public string IATACode { get; set; }
        public string ICAOCode { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string ServedCity { get; set; }
        public string Elevation { get; set; }
        public string ElevationInFeet { get; set; }
        public string ElevationAccuracy { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string LatitudeInDecimal { get; set; }
        public string LongitudeInDecimal { get; set; }
        public string Datum { get; set; }
        public string UOMVerticalDistance { get; set; }
        public string MagneticVariation { get; set; }
        public string MagneticVariationYear { get; set; }
        public string MagneticVariationChange { get; set; }
        public string GeoidUndulation { get; set; }
        public string Remark { get; set; }
        public string VerticalDatum { get; set; }
        public string SiteDescription { get; set; }
        public string ReferenceTemperature { get; set; }
        public string UOMReferenceTemperature { get; set; }
        public string AdminName { get; set; }
        public string AltimeterCheckLocationDescription { get; set; }
        public string SecondaryPowerSupplyDescription { get; set; }
        public string WindDirectionIndicatorDescription { get; set; }
        public string LandingDirectionIndicatorDescription { get; set; }
        public string TransitionAltitude { get; set; }
        public string UOMTransitionAltitude { get; set; }
        public string GeographicalAccuracy { get; set; }
        public string UOMGeographicalAccuracy { get; set; }
        public string ReferencePointDescription { get; set; }
        public GeographicalLocation GeographicalLocation { get; set; }
        public string CodeTypeMilitaryOperations { get; set; }
        public string FIR { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
        public string AerodromeStatus { get; set; }
        public string FK_Timetable { get; set; }
        public int SEQ { get; set; }

        public List<Runway> Runways { get; set; }
        public List<Taxiway> Taxiways { get; set; }
    }

    public class Aerodromes
    {
        public IList<Aerodrome> Value { get; set; }
    }
}

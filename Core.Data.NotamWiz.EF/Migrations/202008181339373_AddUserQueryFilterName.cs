namespace NavCanada.Core.Data.NotamWiz.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserQueryFilterName : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserQueryFilterName",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(nullable: false),
                        SlotNumber = c.Int(nullable: false),
                        FilterName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserQueryFilterName");
        }
    }
}

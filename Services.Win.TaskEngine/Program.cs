﻿using Core.Diagnoistics.SeriLog;

namespace NavCanada.Services.Win.TaskEngine
{
    using System;
    using System.Configuration;
    using System.ServiceProcess;
    
    using Installation;
    using NavCanada.Core.Common.Contracts;
    using SqlServerTypes;

    internal static class Program
    {
        
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private static void Main(string[] args)
        {

            InitDependencies();

            if (args.Length == 0)
            {
                var servicesToRun = new ServiceBase[]
                {
                        new TaskEngineHostSvc(new LoggerSeri())
                };
                ServiceBase.Run(servicesToRun);
            }
            else if (args.Length == 1)
            {
                switch (args[0])
                {
                    case "help":
                    case "?":
                        System.Windows.Forms.MessageBox.Show("To install service:       Services.Win.TaskEngine.exe -install\n" +
                                                             "To uninstall service:     Services.Win.TaskEngine.exe -uninstall\n" +
                                                             "To reinstall service:     Services.Win.TaskEngine.exe -reinstall");
                        break;

                    case "-install":
                        InstallationProcess.InstallService();
                        InstallationProcess.StartService();
                        break;

                    case "-uninstall":
                        InstallationProcess.StopService();
                        InstallationProcess.UninstallService();
                        break;

                    case "-reinstall":
                        InstallationProcess.StopService();
                        InstallationProcess.UninstallService();
                        InstallationProcess.InstallService();
                        InstallationProcess.StartService();
                        break;

                    default:
                        throw new NotImplementedException();
                }
            }
        }

        private static void InitDependencies()
        {
            Utilities.LoadNativeAssemblies(AppDomain.CurrentDomain.BaseDirectory);
        }
    }
}
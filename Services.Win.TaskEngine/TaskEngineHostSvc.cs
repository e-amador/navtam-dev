﻿namespace NavCanada.Services.Win.TaskEngine
{
    using System;
    using System.Runtime.InteropServices;
    using System.ServiceProcess;
    using System.Threading.Tasks;

    using Core.TaskEngine.Implementations;
    using Enum;
    using NavCanada.Core.Common.Contracts;

    public partial class TaskEngineHostSvc : ServiceBase
    {
        public TaskEngineHostSvc(ILogger logger)
        {
            InitializeComponent();

            _logger = logger;

            this.isStarted = false;
            //this.taskEngineHost = new TaskEngineHost();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                if (this.isStarted)
                    return;

                ServiceStatus serviceStatus = new ServiceStatus();
                serviceStatus.CurrentState = ServiceState.ServiceStartPending;
                serviceStatus.WaitHint = 100000;
                SetServiceStatus(ServiceHandle, ref serviceStatus);

                _logger.LogDebug(typeof(TaskEngineHostSvc), "TaskEngineHostService started at: " + DateTime.UtcNow);

                Task.Run(() =>
                {
                    this.taskEngineHost = new TaskEngineShell(_logger);
                    this.taskEngineHost.Start();

                    this.isStarted = true;

                    serviceStatus.CurrentState = ServiceState.ServiceRunning;
                    SetServiceStatus(ServiceHandle, ref serviceStatus);
                });
            }
            catch (AggregateException ae)
            {
                foreach (var e in ae.InnerExceptions)
                    _logger.LogError(ae, typeof(TaskEngineHostSvc), string.Format("{0}: {1}", e.GetType().Name, e.Message));

                _logger.LogError(ae, typeof(TaskEngineHostSvc), "TaskEngineHostService [Start] exit with error at: " + DateTime.UtcNow.ToLongTimeString());
            }
            catch (Exception e)
            {
                e.Data.Add("ERROR_MSG", $"TaskEngineHostService [Start] exit with error at: {DateTime.UtcNow.ToLongTimeString()}, {e.GetType().Name}: {e.Message}");
                _logger.LogError(e, typeof(TaskEngineHostSvc), "TaskEngineHostService [Start] exit with error at: " + DateTime.UtcNow.ToLongTimeString());
            }
        }

        protected override void OnStop()
        {
            try
            {
                if (!this.isStarted)
                    return;

                ServiceStatus serviceStatus = new ServiceStatus();
                serviceStatus.CurrentState = ServiceState.ServiceStopPending;
                serviceStatus.WaitHint = 100000;
                SetServiceStatus(ServiceHandle, ref serviceStatus);

                this.taskEngineHost.Stop();

                serviceStatus.CurrentState = ServiceState.ServiceStopped;
                SetServiceStatus(ServiceHandle, ref serviceStatus);

                _logger.LogDebug(typeof(TaskEngineHostSvc), "TaskEngineHostService stopped at " + DateTime.UtcNow);
            }
            catch (Exception e)
            {
                _logger.LogError(e, typeof(TaskEngineHostSvc), $"TaskEngineHostService[Stop] exit with error at: {DateTime.UtcNow} Message: {e.Message}");
            }
        }

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(IntPtr handle, ref ServiceStatus serviceStatus);

        private readonly ILogger _logger;

        private TaskEngineShell taskEngineHost;
        private bool isStarted;
    }
}
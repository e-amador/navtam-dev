﻿using NavCanada.Core.Common.Contracts;
using NavCanada.Core.TaskEngine.Contracts;
using NDS.Common.Contracts;
using NDS.SolaceVMR;
using System;

namespace NavCanada.Core.TaskEngine.QueueListener
{
    public class NdsQueueListenerContext : INdsQueueListenerContext
    {
        public NdsQueueListenerContext(IClusterStorageFactory storageFactory, ILogger logger, string hubConnectionString, string topic)
        {
            StorageFactory = storageFactory;
            Logger = logger;
            _hubConnectionString = hubConnectionString;
            _queueName = topic;
        }

        public IClusterStorageFactory StorageFactory { get; }
        public ILogger Logger { get; }

        public ISubscription SubscribeToRequestTopic(Action<INdsMessage> subscriptionCallback)
        {
            var dataBus = new SolaceVMRWrapper(_hubConnectionString);
            return dataBus.ReadFromQueue(_queueName, subscriptionCallback);
        }

        private readonly string _hubConnectionString;
        private readonly string _queueName;
    }
}
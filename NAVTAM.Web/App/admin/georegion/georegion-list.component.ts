﻿import { Component, OnInit, Inject } from '@angular/core'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';

import { ActivatedRoute, Router } from '@angular/router'

import { Select2OptionData } from 'ng2-select2';

import { DataService } from '../shared/data.service'
import { MemoryStorageService } from '../shared/mem-storage.service'
import { WindowRef } from '../../common/windowRef.service'

import { LocaleService, TranslationService } from 'angular-l10n';

import {
    IQueryOptions,
    IPaging,
    IGeoRegionPartial,
    ITableHeaders
} from '../shared/data.model'

declare var $: any;

@Component({
    templateUrl: '/app/admin/georegion/georegion-list.component.html'
})
export class GeoRegionListComponent implements OnInit {
    queryOptions: IQueryOptions;
    paging: IPaging = {
        currentPage: 1,
        pageSize: 0,
        totalCount: 0,
        totalPages: 0,
    };
    totalPages: number = 0;

    itemsPerPageData: Array<any>;
    itemsPerPageStartValue: string;
    loadingData: boolean = false;
    tableHeaders: ITableHeaders;
    canbeDeleted: boolean = false;

    geoRegions: IGeoRegionPartial[];
    selectedGeoRegion: IGeoRegionPartial = null;


    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private route: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService) {

        if (this.winRef.appConfig.ccs || this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);

        this.tableHeaders = {
            sorting:
                [
                    { columnName: 'Name', direction: 0 },
                ],
            itemsPerPage:
                [
                    { id: '10', text: '10' },
                    { id: '20', text: '20' },
                    { id: '50', text: '50' },
                    { id: '100', text: '100' },
                    { id: '200', text: '200' },
                ]
        };

        this.paging.pageSize = +this.tableHeaders.itemsPerPage[0].id;
        this.queryOptions = {
            entity: "georegions",
            page: 1,
            pageSize: this.paging.pageSize,
            sort: this.tableHeaders.sorting[0].columnName //Default Received Descending
        }
    };

    ngOnInit() {
        //Set latest query options
        const queryOptions = this.memStorageService.get(this.memStorageService.ITEM_QUERY_OPTIONS_KEY);
        if (queryOptions && queryOptions.entity === "georegions") {
            this.queryOptions = queryOptions;
        }
        this.getGeoRegionsInPage(this.queryOptions.page);
    }

    get langCulture() {
        return (window["app"].cultureInfo || "en").toUpperCase();
    }

    selectRegionAllocation(region: IGeoRegionPartial) {
        if (this.selectedGeoRegion) {
            this.selectedGeoRegion.isSelected = false;
        }

        this.canbeDeleted = true;
        this.selectedGeoRegion = region;
        this.selectedGeoRegion.isSelected = true;
        this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, this.selectedGeoRegion.id);
    }

    itemsPerPageChanged(e: any): void {
        this.queryOptions.pageSize = e.value;
        this.getGeoRegionsInPage(1);
    }

    onPageChange(page: number) {
        this.getGeoRegionsInPage(page);
    }

    sort(index) {
        const dir = this.tableHeaders.sorting[index].direction;

        for (let i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }

        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
        }

        this.getGeoRegionsInPage(this.queryOptions.page);
    }

    sortingClass(index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';

        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';

        return 'sorting';
    }

    private getGeoRegionsInPage(page) {
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, this.queryOptions);

            this.dataService.getRegisteredGeoRegions(this.queryOptions)
                .subscribe((geosData: any) => {
                    this.processGeoRegions(geosData);
                });
        }
        catch (e) {
            this.loadingData = false;
        }
    }

    private processGeoRegions(geosData: any) {
        this.paging = geosData.paging;
        this.totalPages = geosData.paging.totalPages;

        for (var iter = 0; iter < geosData.data.length; iter++) geosData.data[iter].index = iter;

        const lastGeoRegionIdSelected = this.memStorageService.get(this.memStorageService.ITEM_ID_KEY);
        if (geosData.data.length > 0) {
            this.geoRegions = geosData.data;
            let index = 0;
            if (lastGeoRegionIdSelected) {
                index = this.geoRegions.findIndex(x => x.id === lastGeoRegionIdSelected);
                if (index < 0) index = 0; // when not found first should be selected;
            }
            this.geoRegions[index].isSelected = true;
            this.selectedGeoRegion = this.geoRegions[index];
            this.canbeDeleted = true;
            this.loadingData = false;
        } else {
            this.geoRegions = [];
            this.selectedGeoRegion = null;
            this.loadingData = false;
        }
    }

    confirmDeleteGeo() {
        $('#modal-delete').modal({});
    }

    deleteGeoRegion() {
        if (this.selectedGeoRegion) {
            this.dataService.deleteGeoRegion(this.selectedGeoRegion.id)
                .subscribe(result => {
                    this.getGeoRegionsInPage(this.paging.currentPage);
                    let msg = this.translation.translate('GeoRegionSession.SuccessGeoDeleted');
                    this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                }, error => {
                    let msg = this.translation.translate('GeoRegionSession.FailureGeoDeleted');
                    this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        }
    }


}
﻿namespace NavCanada.Applications.NotamWiz.Web
{
    using System.Web.Optimization;

    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jqueryBundle").Include(
                            "~/Scripts/jquery-*",
                            "~/Scripts/jquery-ui-*",
                            "~/Scripts/jquery.inputmask/jquery.inputmask*",
                            "~/Scripts/jquery.feedBackBox.js",
                            "~/Scripts/jquery.idletimer.js",
                            "~/Scripts/jquery.idletimeout.js"
                            ));

            bundles.Add(new Bundle("~/bundles/jqueryLayoutBundle").Include("~/Scripts/jquery.layout.js"));

            // The signalr bundle
            bundles.Add(new ScriptBundle("~/bundles/signalR").Include(
                            "~/signalr/hubs", "~/Scripts/jquery.signalR-2.2.0.min.js"));


            // The NavCanada bundle
            bundles.Add(new ScriptBundle("~/bundles/notamwizcommon").Include(
                            "~/Scripts/navcanada.notamwiz.common.js"));


            // The Kendo JavaScript bundle
            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                    "~/Scripts/kendo/kendo.web.min.js", // or kendo.all.min.js if you want to use Kendo UI Web and Kendo UI DataViz
                    "~/Scripts/kendo/kendo.aspnetmvc.min.js",
                    "~/Scripts/kendo.web.Ext.js"));


            // The Kendo CSS bundle
            var kendoStyleBundle = new StyleBundle("~/Content/kendo/kendoBundle");
            kendoStyleBundle.Include("~/Content/kendo/kendo.common.min.css");
            kendoStyleBundle.Include("~/Content/kendo/kendo.blueopal.min.css");
            kendoStyleBundle.Include("~/Content/kendoext/kendo.Ext.css");
            //kendoStyleBundle.Include("~/Content/NsdEditor/bootstrap.css");
            bundles.Add(kendoStyleBundle);


            var siteStyleBundle = new StyleBundle("~/Content/site");
            siteStyleBundle.Include("~/Content/site.css");
            siteStyleBundle.Include("~/Content/jquery.feedBackBox.css");

            bundles.Add(siteStyleBundle);


            // Clear all items from the ignore list to allow minified CSS and JavaScript files in debug mode
            bundles.IgnoreList.Clear();


            // Add back the default ignore list rules sans the ones which affect minified files and debug mode
            bundles.IgnoreList.Ignore("*.intellisense.js");
            bundles.IgnoreList.Ignore("*-vsdoc.js");
            bundles.IgnoreList.Ignore("*.debug.js", OptimizationMode.WhenEnabled);

            //    BundleTable.EnableOptimizations = false;



/*
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
*/
        }
    }
}

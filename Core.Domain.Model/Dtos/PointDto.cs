﻿
namespace NavCanada.Core.Domain.Model.Dtos
{
    public class PointDto : GeometryDto
    {
        public double[] Coordinates { get; set; }
    }
}

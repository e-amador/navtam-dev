"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfigNewComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var windowRef_service_1 = require("../../common/windowRef.service");
var angular_l10n_1 = require("angular-l10n");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var toastr_service_1 = require("./../../common/toastr.service");
var data_service_1 = require("../shared/data.service");
var ConfigNewComponent = /** @class */ (function () {
    function ConfigNewComponent(toastr, fb, dataService, memStorageService, router, winRef, activatedRoute, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.activatedRoute = activatedRoute;
        this.locale = locale;
        this.translation = translation;
        this.model = null;
        this.isReadOnly = false;
        this.isConfigUniqueName = true;
        this.isValidating = false;
        this.isValidLocation = true;
        this.isSubmitting = false;
        if (this.winRef.appConfig.ccs || this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);
    }
    ConfigNewComponent.prototype.ngOnInit = function () {
        this.action = this.activatedRoute.snapshot.data[0].action;
        if (this.action === 'create') {
            this.configRec = {
                name: "",
                description: "",
                value: "",
                category: "NOTAM",
                action: ""
            };
        }
        else { //if (this.action === 'copy') {
            this.isReadOnly = true;
            this.configRec = this.activatedRoute.snapshot.data['model'];
        }
        this.configRec.action = this.action;
        this.model = this.configRec;
        this.configureReactiveForm();
    };
    ConfigNewComponent.prototype.ngOnDestroy = function () {
    };
    ConfigNewComponent.prototype.configureReactiveForm = function () {
        var _this = this;
        this.configListForm = this.fb.group({
            configName: [{ value: this.model.name, disabled: this.isReadOnly }, []],
            configDescription: [{ value: this.model.description, disabled: false }, []],
            configValue: [{ value: this.model.value, disabled: false }, [forms_1.Validators.required]],
            configCategory: [{ value: this.model.category, disabled: false }, []],
        });
        if (!this.isReadOnly) {
            this.$ctrl("configName").valueChanges.debounceTime(1000).subscribe(function () { return _this.validateConfigNameUnique(); });
        }
    };
    ConfigNewComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.isSubmitting)
            return;
        this.isSubmitting = true;
        this.prepareData();
        this.dataService.saveConfig(this.configRec)
            .subscribe(function (data) {
            var msg = _this.translation.translate('ConfigurationSession.SuccessConfigCreated');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            _this.router.navigate(["/administration/config"]);
        }, function (error) {
            _this.isSubmitting = false;
            var msg = _this.translation.translate('ConfigurationSession.FailureConfigCreated');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    ConfigNewComponent.prototype.prepareData = function () {
        this.configRec.name = this.$ctrl('configName').value;
        this.configRec.description = this.$ctrl('configDescription').value;
        this.configRec.category = this.$ctrl('configCategory').value;
        this.configRec.value = this.$ctrl('configValue').value;
    };
    ConfigNewComponent.prototype.disableSubmit = function () {
        if (this.configListForm.pristine)
            return true;
        if (this.isValidating)
            return true;
        if (this.isSubmitting)
            return true;
        return !this.validateForm();
    };
    ConfigNewComponent.prototype.backToConfigList = function () {
        this.router.navigate(['/administration/config']);
    };
    ConfigNewComponent.prototype.validConfigData = function () {
        if (this.configRec.name.length > 3 &&
            this.configRec.description.length > 3 &&
            this.configRec.value.length > 0 &&
            this.configRec.category.length > 3) {
            return true;
        }
        return false;
    };
    ConfigNewComponent.prototype.validConfigName = function () {
        if (this.configListForm.pristine)
            return true;
        var dName = this.$ctrl('configName');
        if (dName.value) {
            if (dName.value.length > 0)
                return true;
        }
        return false;
    };
    ConfigNewComponent.prototype.validConfigDescription = function () {
        if (this.configListForm.pristine)
            return true;
        var dName = this.$ctrl('configDescription');
        if (dName.value) {
            if (dName.value.length > 0)
                return true;
        }
        return false;
    };
    ConfigNewComponent.prototype.validConfigValue = function () {
        if (this.configListForm.pristine)
            return true;
        var dName = this.$ctrl('configValue');
        if (dName.value) {
            if (dName.value.length > 0)
                return true;
        }
        return false;
    };
    ConfigNewComponent.prototype.validConfigCategory = function () {
        if (this.configListForm.pristine)
            return true;
        var dName = this.$ctrl('configCategory');
        if (dName.value) {
            if (dName.value.length > 0)
                return true;
        }
        return false;
    };
    ConfigNewComponent.prototype.validConfigNameSize = function () {
        if (this.configListForm.pristine)
            return true;
        if (!this.validConfigName())
            return true;
        var dName = this.$ctrl('configName');
        if (dName.value) {
            if (dName.value.length > 3)
                return true;
        }
        return false;
    };
    ConfigNewComponent.prototype.validConfigDescritionSize = function () {
        if (this.configListForm.pristine)
            return true;
        if (!this.validConfigDescription())
            return true;
        var dName = this.$ctrl('configDescription');
        if (dName.value) {
            if (dName.value.length > 3)
                return true;
        }
        return false;
    };
    ConfigNewComponent.prototype.validConfigCategorySize = function () {
        if (this.configListForm.pristine)
            return true;
        if (!this.validConfigCategory())
            return true;
        var dName = this.$ctrl('configCategory');
        if (dName.value) {
            if (dName.value.length > 3)
                return true;
        }
        return false;
    };
    ConfigNewComponent.prototype.validateConfigNameUnique = function () {
        var _this = this;
        if (this.configListForm.pristine)
            return true;
        if (this.isValidating && this.action !== 'create')
            return;
        if (this.validConfigName() && this.validConfigNameSize()) {
            var dName = this.$ctrl('configName');
            if (dName.value) {
                if (dName.value.length > 3) {
                    this.isValidating = true;
                    this.dataService.existsConfigName(dName.value)
                        .finally(function () {
                        _this.isValidating = false;
                    })
                        .subscribe(function (resp) {
                        _this.isConfigUniqueName = !resp;
                    });
                }
            }
        }
    };
    ConfigNewComponent.prototype.validateForm = function () {
        if (this.isValidating)
            return false;
        if (!this.configListForm.pristine) {
            this.prepareData();
            if (this.validConfigData()) {
                return this.isConfigUniqueName;
            }
        }
        return false;
    };
    ConfigNewComponent.prototype.$ctrl = function (name) {
        return this.configListForm.get(name);
    };
    ConfigNewComponent.prototype.validateKeyPressNumber = function (evt) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
        }
    };
    ConfigNewComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/config/config-new.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], ConfigNewComponent);
    return ConfigNewComponent;
}());
exports.ConfigNewComponent = ConfigNewComponent;
function isANumber(str) {
    return !/\D/.test(str);
}
//# sourceMappingURL=config-new.component.js.map
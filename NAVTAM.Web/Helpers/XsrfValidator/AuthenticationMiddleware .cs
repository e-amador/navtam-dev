﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using System.Threading.Tasks;

namespace NAVTAM.Helpers.XsrfValidator
{
    public class AuthenticationMiddleware : OwinMiddleware
    {
        const string _authenticationCookie = CookieAuthenticationDefaults.CookiePrefix + DefaultAuthenticationTypes.ApplicationCookie;

        public AuthenticationMiddleware(OwinMiddleware next) :
            base(next)
        { }

        public override async Task Invoke(IOwinContext context)
        {
            var response = context.Response;
            response.OnSendingHeaders(state =>
            {
                var resp = (OwinResponse)state;

                if (resp.Environment.ContainsKey("StripAspCookie"))
                {
                    resp.Cookies.Delete(_authenticationCookie);
                    resp.Cookies.Append(_authenticationCookie, "");
                }
            }, response);

            await Next.Invoke(context);
        }
    }
}
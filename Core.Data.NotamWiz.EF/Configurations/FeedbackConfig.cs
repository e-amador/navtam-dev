﻿namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    using System.Data.Entity.ModelConfiguration;

    using Domain.Model.Entitities;

    public class FeedbackConfig : EntityTypeConfiguration<Feedback>
    {
        public FeedbackConfig()
        {
            HasKey(e => e.Id);

            Property(e => e.Firstname).IsRequired();
            Property(e => e.Lastname).IsRequired();
            Property(e => e.EmailAddress).IsRequired();
            Property(e => e.PhoneNumber).IsRequired();
            Property(e => e.Organization).IsRequired();
            Property(e => e.Department).IsRequired();
            Property(e => e.Comment).IsRequired();
        }
    }
}

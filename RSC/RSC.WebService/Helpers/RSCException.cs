﻿using System;

namespace RSC.WebService.Helpers
{
    public class RSCException : Exception
    {
        public RSCException(int errorCode, string message) : base(message)
        {
            ErrorCode = errorCode;
        }
        public int ErrorCode { get; set; }
    }
}
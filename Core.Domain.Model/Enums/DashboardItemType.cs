﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum DashboardItemType
    {
        Undefined = -1,
        SubjectList,
        //InBox,
        //OutBox,
        Sdo,
        SdoSubject,
        All
    }
}

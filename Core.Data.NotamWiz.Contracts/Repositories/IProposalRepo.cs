﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    public interface IProposalRepo : IRepository<Proposal>
    {
        Task<Proposal> GetByIdAsync(int id);
        Task<List<Proposal>> GetGroupedProposalsByGroupIdAsync(Guid groupId);
        Task<List<ProposalDto>> GetByCategoryAsync(int categoryId, int orgId, QueryOptions queryOptions);
        Task<List<ProposalDto>> GetAllAsync(int organizationId, QueryOptions queryOptions);
        Task<int> GetCountByCategoryAsync(int categoryId, int orgId, QueryOptions queryOptions);
        Task<int> GetAllCountAsync(int organizationId, QueryOptions queryOptions);
        Task<List<ProposalDto>> GetProposalsGroupedAsync(int organizationId, Guid groupId);
        Task<List<ProposalDto>> GetSubmittedByCategoryAsync(int categoryId, QueryOptions queryOptions);
        Task<int> GetSubmittedCountByCategoryAsync(int categoryId, QueryOptions queryOptions);
        Task<List<ProposalStatusDto>> GetStatusAsync(int[] ids, DateTime since);

        // User Queue
        Task<int> GetUserProposalsInRegionCountAsync(DbGeography region, int orgId, OrganizationType orgType, int catId, QueryOptions queryOptions);
        Task<List<ProposalDto>> GetUserProposalsInRegionAsync(DbGeography region, int orgId, OrganizationType orgType, int catId, QueryOptions queryOptions);
        Task<int> GetCountByChildrenCategoryIdsAsync(DbGeography region, int[] categoryIds, int? orgId, QueryOptions queryOptions);
        Task<List<ProposalDto>> GetByChildrenCategoryIdsAsync(DbGeography region, int[] categoryIds, int? orgId, QueryOptions queryOptions);

        //Pending Queue
        Task<int> GetNofPendingQueueCountAsync();
        Task<int> GetNofPendingQueueCountByCategoryIdAsync(int categoryId);
        Task<int> GetNofPendingByChildrenQueueCountByCategoryIdAsync(int[] categoryIds);
        Task<List<ProposalDto>> GetNofPendingQueueByCategoryIdAsync(int categoryId, QueryOptions queryOptions);
        Task<List<ProposalDto>> GetNofPendingQueueByChildrenCategoryIdsAsync(int[] categoryIds, QueryOptions queryOptions);
        Task<List<ProposalDto>> GetAllNofPendingQueueAsync(QueryOptions queryOptions);
        Task<List<ProposalDto>> GetNofPendingProposalsGroupedAsync(Guid groupId);

        //Park Queue
        Task<int> GetNofParkedQueueCountAsync();
        Task<int> GetNofParkedQueueCountByCategoryIdAsync(int categoryId);
        Task<int> GetNofParkByChildrenQueueCountByCategoryIdAsync(int[] categoryIds);
        Task<List<ProposalDto>> GetNofParkedQueueByCategoryIdAsync(int categoryId, QueryOptions queryOptions);
        Task<List<ProposalDto>> GetNofParkedByChildrenCategoryIdsAsync(int[] categoryIds, QueryOptions queryOptions);
        Task<List<ProposalDto>> GetAllNofParkedQueueByAsync(QueryOptions queryOptions);
        Task<List<ProposalDto>> GetNofParkedProposalsGroupedAsync(Guid groupId);

        //Review Queue
        Task<int> GetNofReviewQueueCountAsync(int currentUserOrgId);
        Task<int> GetNofReviewQueueCountByCategoryIdAsync(int currentUserOrgId, int categoryId);
        Task<int> GetNofReviewByChildrenQueueCountByCategoryIdAsync(int currentUserOrgId, int[] categoryIds);
        Task<List<ProposalDto>> GetNofReviewQueueByCategoryIdAsync(int currentUserOrgId, int categoryId, QueryOptions queryOptions);
        Task<List<ProposalDto>> GetNofReviewByChildrenCategoryIdsAsync(int currentUserOrgId, int[] categoryIds, QueryOptions queryOptions);
        Task<List<ProposalDto>> GetAllNofReviewQueueByAsync(int currentUserOrgId, QueryOptions queryOptions);
        Task<List<ProposalDto>> GetNofReviewProposalsGroupedAsync(int currentUserOrgId, Guid groupId);

        //Schedule Queries
        List<Proposal> GetExpiredProposals();

        //Proposal almost to expire
        Task<int> GetUserProposalsExpiringSoonCountAsync(DbGeography region, int orgId, OrganizationType orgType, int MinutesToExpire);
        Task<List<ProposalDto>> GetUserProposalsExpiringSoonAsync(DbGeography region, int orgId, OrganizationType orgType, QueryOptions queryOptions, int MinutesToExpire);
    }
}

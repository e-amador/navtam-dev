﻿import { Component, Inject, OnInit, AfterViewInit, OnDestroy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormGroup, FormBuilder, FormArray, AbstractControl, ValidatorFn } from '@angular/forms'

import { Select2OptionData } from 'ng2-select2';
import { WindowRef } from '../../common/windowRef.service';

import { LocaleService, TranslationService } from 'angular-l10n';

import { MemoryStorageService } from '../shared/mem-storage.service'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';

import { DataService } from '../shared/data.service'

import { DoaRegionSource, IDoaAdmin, IFIR, IDoaPointModel, ISelectOptions, IRegionLocationModel, IRegionMapRender, RegionType } from '../shared/data.model';

declare var $: any;

@Component({
    templateUrl: '/app/admin/nregion/northernregion-edit.component.html'
})
export class NorthernRegionEditComponent implements OnInit, AfterViewInit, OnDestroy {
    model: any = null;
    action: string;

    public nRegionForm: FormGroup;

    nRegion: IDoaAdmin;
    isValidating: boolean = false;
    isValidDoaLocation: boolean = true;
    isValidModifyLocation: boolean = true;
    isSubmitting: boolean = false;

    doaChangesList: IDoaPointModel[] = [];
    selectedDoaLocation: IDoaPointModel = null;

    doaSources: ISelectOptions[] = [];
    selectedSourceId?: number = 1;
    doaSourceOptions: Select2Options;

    firSources: ISelectOptions[] = [];
    selectedFirId?: string = '-1';
    firSourceOptions: Select2Options;

    activeLocationPoint: IDoaPointModel = null;

    radiusLimit: number = 999;

    doaSource: string = this.translation.translate('DoasAdminSession.DoaSourceGeography');

    leafletmap: any;
    mapHealthy: boolean = false;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private activatedRoute: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService) {

        if (this.winRef.appConfig.ccs || this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);
    }

    ngOnInit() {
        this.activeLocationPoint = { source: '', location: '', inDoa: false, radius: 0 };
        this.selectedDoaLocation = { source: '', location: '', inDoa: false, radius: 5 };
        this.action = this.activatedRoute.snapshot.data[0].action;
        this.model = this.activatedRoute.snapshot.data['model'];
        this.nRegion = this.model;

        //Sources
        this.doaSourceOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('DoasAdminSession.SelectSource')
            },
            width: "100%"
        }
        this.loadDoaSources();
        this.selectedSourceId = +this.doaSources.find(x => x.id === String(this.nRegion.region.source)).id;

        //FIRS
        this.firSourceOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('DoasAdminSession.SelectFir')
            },
            width: "100%"
        }
        this.loadFirSources();

        this.configureReactiveForm();
    }

    ngAfterViewInit() {
        this.mapHealthCheck();
    }

    ngOnDestroy() {
        this.disposeMap();
    }

    get langCulture() {
        return (window["app"].cultureInfo || "en").toUpperCase();
    }

    onSubmit() {
        if (this.isSubmitting) return;
        this.isSubmitting = true;
        var doaUpdate: IDoaAdmin = this.prepareData();
        if (doaUpdate) {
            this.dataService.saveDoa(doaUpdate)
                .subscribe(data => {
                    let msg = this.translation.translate('DoasAdminSession.SuccessDoaCreated');
                    this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                    this.router.navigate(["/administration"]);
                }, error => {
                    this.isSubmitting = false;
                    let msg = this.translation.translate('DoasAdminSession.FailureDoaCreated');
                    this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        } else {
            this.isSubmitting = false;
            let msg = this.translation.translate('DoasAdminSession.ErrorDoaData');
            this.toastr.error(msg, "", { 'positionClass': 'toast-bottom-right' });
        }
    }

    onCancel() {
        this.dataService.getDoaNorthernRegion()
            .subscribe((model: any) => {
                this.isValidDoaLocation = true;
                this.model = model;
                this.nRegion = this.model;
                this.selectedSourceId = +this.doaSources.find(x => x.id === String(this.nRegion.region.source)).id;
                this.selectedFirId = '-1';
                this.doaChangesList = [];
                this.activeLocationPoint = { source: '', location: '', inDoa: false, radius: 0 };
                this.selectedDoaLocation = { source: '', location: '', inDoa: false, radius: 5 };
                this.configureReactiveForm();
                this.northernRegionRenderMap();

            }, error => {
                this.isValidDoaLocation = false;
                this.nRegion.region.location = '';
                this.isValidating = false;
            });
    }

    private configureReactiveForm() {
        this.nRegionForm = this.fb.group({
            doaName: [{ value: this.model.name, disabled: true }, []],
            doaDescription: [{ value: this.model.description, disabled: true }, []],
            doaSource: [{ value: this.doaSource, disabled: true }, []],
            doaGeo: [{ value: this.model.region.geography, disabled: true }, []],

            doaLocation: [{ value: this.model.region.location, disabled: false }, []],
            doaRadius: [{ value: this.model.region.radius, disabled: false }, []],

            doaPoints: [{ value: this.model.region.points, disabled: false }, []],

            modifyLocation: [{ value: this.selectedDoaLocation.location, disabled: false }, []],
            modifyRadius: [{ value: this.selectedDoaLocation.radius, disabled: false }, []],
        });

        this.$ctrl("doaRadius").setValidators(radiusValidatorFn(this.radiusLimit));
        this.$ctrl("modifyRadius").setValidators(radiusValidatorFn(this.radiusLimit));

        //doaLocation is the Point to define a Region
        this.$ctrl("doaLocation").valueChanges.debounceTime(1000).subscribe(() => this.validateDoaLocation());
        //doaPoints is the set of point to define a Region
        this.$ctrl("doaPoints").valueChanges.debounceTime(1000).subscribe(() => this.validateDoaPointsLocation());
        //modifyLocation is the Point and Actions to modify an already defined Region
        this.$ctrl("modifyLocation").valueChanges.debounceTime(1000).subscribe(() => this.validateModifyLocation());

        this.$ctrl("doaRadius").valueChanges.debounceTime(1000).subscribe(() => this.validateDoaLocation());
    }

    public validateForm(): boolean {
        if (this.isValidating) return false;
        if (!this.nRegionForm.pristine) {
            return this.validateData();
        }
        return false;
    }

    public prepareData(): IDoaAdmin {
        if (this.nRegion.region.source === DoaRegionSource.PointAndRadius) {
            const radCtrl = this.$ctrl("doaRadius");
            this.nRegion.region.radius = +radCtrl.value;
            this.nRegion.region.location = this.nRegion.region.location.toUpperCase();
        }
        var doaUpdate: IDoaAdmin = {
            id: this.nRegion.id,
            name: this.nRegion.name,
            description: this.nRegion.description,
            region: this.nRegion.region,
            doaType: RegionType.Northern,
            changes: this.doaChangesList
        };
        return doaUpdate;
    }

    public validateData(): boolean {
        var data = this.prepareData();
        if (data) {
            if (data.region.source === DoaRegionSource.None) return false;
            if (data.region.source === DoaRegionSource.FIR && data.region.fir.length === 0) return false;
            if (data.region.source === DoaRegionSource.Geography && data.region.geography.length === 0) return false;
            if (data.region.source === DoaRegionSource.PointAndRadius) {
                if (data.region.location.length === 0) return false;
                if (data.region.radius < 1 || data.region.radius > this.radiusLimit) return false;
            }
            if (data.region.source === DoaRegionSource.Points && data.region.points.length === 0) return false;
            return true;
        }
        return false;
    }

    public validateDoaLocation() {
        if (this.isValidating) return;
        const dLocation = this.$ctrl('doaLocation');
        if (dLocation.value) {
            if (dLocation.value.length > 3) {
                this.isValidating = true;
                this.dataService.validateDoaLocation(dLocation.value)
                    .subscribe((resp: string) => {
                        this.isValidDoaLocation = true;
                        this.nRegion.region.location = resp.toUpperCase();
                        this.isValidating = false;
                        this.validateForm();
                        this.northernRegionRenderMap();
                    }, error => {
                        this.isValidDoaLocation = false;
                        this.nRegion.region.location = '';
                        this.isValidating = false;
                    });
            } else this.isValidDoaLocation = false;
        } else this.isValidDoaLocation = false;
    }

    public validateDoaPointsLocation() {
        if (this.isValidating) return;
        const dLocation = this.$ctrl('doaPoints');
        if (dLocation.value) {
            if (dLocation.value.length > 3) {
                this.isValidating = true;
                this.dataService.validateDoaPoints(dLocation.value)
                    .subscribe((resp: string) => {
                        this.isValidDoaLocation = true;
                        this.nRegion.region.points = resp.toUpperCase();
                        this.validateForm();
                        this.northernRegionRenderMap();
                        this.isValidating = false;
                    }, error => {
                        this.isValidDoaLocation = false;
                        this.nRegion.region.points = '';
                        this.isValidating = false;
                    });
            } else this.isValidDoaLocation = false;
        } else this.isValidDoaLocation = false;
    }

    public validateModifyLocation() {
        if (this.isValidating) return;
        const dLocation = this.$ctrl('modifyLocation');
        if (dLocation.value) {
            if (dLocation.value.length > 3) {
                this.isValidating = true;
                this.isValidModifyLocation = false;
                var regLoc: IRegionLocationModel = {
                    region: this.nRegion.region,
                    location: dLocation.value
                };
                this.dataService.validateLocInRegion(regLoc)
                    .subscribe((resp: IDoaPointModel) => {
                        this.isValidModifyLocation = true;
                        this.activeLocationPoint = resp;
                        this.isValidating = false;
                        this.validateForm();
                    }, error => {
                        this.activeLocationPoint.location = '';
                        this.isValidating = false;
                    });
            }
        }
    }

    public prepareModifyLocationData() {
        if (this.isValidModifyLocation) {
            const radiusCtrl = this.$ctrl('modifyRadius');
            var loc: IDoaPointModel = { source: '', location: '', inDoa: false, radius: 5 };
            loc.source = this.activeLocationPoint.source.toUpperCase();
            loc.location = this.activeLocationPoint.location.toUpperCase();
            loc.radius = +radiusCtrl.value;
            loc.inDoa = this.activeLocationPoint.inDoa;

            var point = this.doaChangesList.find(x => x.source === loc.source && x.location === loc.location);
            if (point) {
                point.radius = loc.radius;
                this.selectedDoaLocation = point;
            } else {
                var index = this.doaChangesList.push(loc);
                this.selectedDoaLocation = this.doaChangesList[index - 1];
            }
            this.northernRegionRenderMap();
        }
    }

    public includeLocationEnabled(): boolean {
        if (this.isValidModifyLocation && !this.isValidating) {
            const radiusCtrl = this.$ctrl('modifyRadius');
            if (this.activeLocationPoint && +radiusCtrl.value > 0 && +radiusCtrl.value <= this.radiusLimit && this.activeLocationPoint.source !== '' && this.activeLocationPoint.location !== '') {
                return !this.activeLocationPoint.inDoa;
            }
        }
        return false;
    }

    public excludeLocationEnabled(): boolean {
        if (this.isValidModifyLocation && !this.isValidating) {
            const radiusCtrl = this.$ctrl('modifyRadius');
            if (this.activeLocationPoint && +radiusCtrl.value > 0 && +radiusCtrl.value <= this.radiusLimit && this.activeLocationPoint.source !== '' && this.activeLocationPoint.location !== '') return this.activeLocationPoint.inDoa;
        }
        return false;
    }

    disableSubmit(): boolean {
        if (this.nRegionForm.pristine) return true;
        if (this.isValidating) return true;
        if (this.isSubmitting) return true;
        return !this.validateForm();
    }

    public pickLocation(loc: IDoaPointModel) {
        if (this.doaChangesList) {
            var point = this.doaChangesList.find(x => (x.source === loc.source && x.location === loc.location && x.inDoa === loc.inDoa));
            if (point) {
                this.selectedDoaLocation = point;
                var ctrlLocation = this.$ctrl('doaLocation');
                var ctrlRadius = this.$ctrl('doaRadius');
                ctrlLocation.setValue(this.selectedDoaLocation.source);
                ctrlRadius.setValue(this.selectedDoaLocation.radius);
            }
        }
    }

    $ctrl(name: string): AbstractControl {
        return this.nRegionForm.get(name);
    }

    public rowColor(loc: IDoaPointModel) {
        if (!this.selectedDoaLocation) return '';
        if (this.selectedDoaLocation.source === loc.source &&
            this.selectedDoaLocation.location === loc.location &&
            this.selectedDoaLocation.inDoa === loc.inDoa) return 'bg-submitted';
        return '';
    }

    public deletePoint(loc: IDoaPointModel) {
        if (this.doaChangesList) {
            var index = this.doaChangesList.findIndex(x => (x.source === loc.source && x.location === loc.location && x.inDoa === loc.inDoa));
            if (index > -1) {
                this.doaChangesList.splice(index, 1);
                if (this.doaChangesList.length > 0) {
                    this.selectedDoaLocation = this.doaChangesList[0];
                    var ctrlLocation = this.$ctrl('modifyLocation');
                    var ctrlRadius = this.$ctrl('modifyRadius');
                    ctrlLocation.setValue(this.selectedDoaLocation.source);
                    ctrlRadius.setValue(this.selectedDoaLocation.radius);
                }
                else {
                    var ctrlLocation = this.$ctrl('modifyLocation');
                    var ctrlRadius = this.$ctrl('modifyRadius');
                    ctrlLocation.setValue('');
                    ctrlRadius.setValue(5);
                    this.activeLocationPoint = { source: '', location: '', inDoa: false, radius: 0 };
                    this.selectedDoaLocation = { source: '', location: '', inDoa: false, radius: 5 };
                }
                this.northernRegionRenderMap();
            }
        }
    }

    public getAction(inDoa: boolean) {
        if (inDoa) return this.translation.translate('DoasAdminSession.Exclude');
        else return this.translation.translate('DoasAdminSession.Include');
    }

    public validateRadius(ctrlName: string) {
        if (this.nRegionForm.pristine) return true;
        const fc = this.nRegionForm.get(ctrlName);
        if (fc.errors) return false;
        if (fc.value) {
            if (fc.value > this.radiusLimit) return false;
            if (fc.value < 1) return false;
        } else return false;
        return true;
    }

    public validModifyLocation(): boolean {
        return this.isValidModifyLocation || this.nRegionForm.pristine;
    }

    decRadius(ctrlName: string) {
        const radCtrl = this.$ctrl(ctrlName);
        if (+radCtrl.value > 0) {
            radCtrl.setValue(+radCtrl.value - 1);
            radCtrl.markAsDirty();
        }
    }

    incRadius(ctrlName: string) {
        const radCtrl = this.$ctrl(ctrlName);
        if (+radCtrl.value < this.radiusLimit) {
            radCtrl.setValue(+radCtrl.value + 1);
            radCtrl.markAsDirty();
        }
    }

    public validateKeyPressNumber(evt: any) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
        }
    }

    public saveDoaFile() {
        const winObj = this.winRef.nativeWindow;
        if (winObj["FormData"] !== undefined) {
            const fileUpload = $("#attachments").get(0);
            const files = fileUpload.files;

            // Create FormData object  
            var fileData = new FormData();

            fileData.append(files[0].name, files[0]);

            this.dataService.saveDoaFile(fileData)
                .finally(() => {
                    $("#attachments").val('');
                    if (!/safari/i.test(navigator.userAgent)) {
                        $("#attachments").type = '';
                        $("#attachments").type = 'file';
                    }
                })
                .subscribe(data => {
                    this.nRegion.region.geography = data;
                    this.nRegion.region.source = DoaRegionSource.Geography;
                    this.$ctrl('doaGeo').setValue(this.nRegion.region.geography);

                    const message = this.translation.translate('DoasAdminSession.FileSuccess');
                    this.toastr.success(message, "", { 'positionClass': 'toast-bottom-right' });
                    this.nRegionForm.markAsDirty();
                    this.resetChangeList();
                    this.northernRegionRenderMap();
                }, error => {
                    this.nRegion.region.geography = "";
                    this.nRegion.region.source = DoaRegionSource.None;
                    this.$ctrl('doaGeo').setValue(this.nRegion.region.geography);
                    this.toastr.error(this.translation.translate('DoasAdminSession.MessageSaveAttachmentError') + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        } else {
            this.nRegion.region.geography = "";
            this.nRegion.region.source = DoaRegionSource.None;
            this.toastr.error(this.translation.translate('DoasAdminSession.MessageSaveAttachmentBrowser1'), this.translation.translate('DoasAdminSession.MessageSaveAttachmentBrowser2'), { 'positionClass': 'toast-bottom-right' });
        }
    }

    loadFirSources() {
        this.dataService.getAllFirs()
            .subscribe((firs: IFIR[]) => {
                this.firSources = firs.map(
                    function (r: any) {
                        return <Select2OptionData>{
                            id: r.id,
                            text: `${r.designator} - ${r.name}`
                        };
                    });
                this.firSources.unshift({ id: "-1", text: "" });
                this.selectedFirId = '-1';
            });
    }

    loadDoaSources() {
        this.doaSources = [
            //{ id: '0', text: this.translation.translate('DoasAdminSession.DoaSourceNone') },
            { id: '1', text: this.translation.translate('DoasAdminSession.DoaSourceGeography') },
            { id: '2', text: this.translation.translate('DoasAdminSession.DoaSourceFir') },
            { id: '3', text: this.translation.translate('DoasAdminSession.DoaSourcePoints') },
            { id: '4', text: this.translation.translate('DoasAdminSession.DoaSourcePointRadius') }
        ];
        this.selectedSourceId = 1;
    }

    public onFirChanged(data: { value: string }) {
        if (data.value) {
            if (this.nRegion.region.fir !== data.value) {
                this.nRegion.region.fir = data.value;
                this.resetChangeList();
                this.nRegionForm.markAsDirty();
                this.northernRegionRenderMap();
            }
        } else {
            this.nRegion.region.fir = '';
            this.resetChangeList();
        }
    }

    public onSourceChanged(data: { value: string }) {
        if (data.value) {
            var diss = this.doaSources.find(x => x.id === String(data.value));
            if (diss) {
                this.nRegion.region.source = +diss.id;
                this.selectedSourceId = +diss.id;

                this.nRegion.region.fir = '';
                this.nRegion.region.geography = '';
                this.nRegion.region.geoFeatures = '';
                this.nRegion.region.location = '';
                this.nRegion.region.points = '';

                if (this.nRegion.region.source === DoaRegionSource.PointAndRadius && this.nRegion.region.radius === 0) {
                    this.nRegion.region.radius = 5;
                    const radCtrl = this.$ctrl("doaRadius");
                    radCtrl.setValue(this.nRegion.region.radius);
                    this.nRegion.region.location = '';
                }
                //Delete the list of changes
                this.resetChangeList();
            }
            else {
                this.nRegion.region.source = 1;
                this.selectedSourceId = 1;
            }
        } else {
            this.nRegion.region.source = 1;
            this.selectedSourceId = 1;
        }
    }

    private resetChangeList() {
        var ctrlLocation = this.$ctrl('modifyLocation');
        var ctrlRadius = this.$ctrl('modifyRadius');
        ctrlLocation.setValue('');
        ctrlRadius.setValue(5);
        this.activeLocationPoint = { source: '', location: '', inDoa: false, radius: 0 };
        this.selectedDoaLocation = { source: '', location: '', inDoa: false, radius: 5 };
        this.doaChangesList = [];
    }

    public backToDoasList() {
        this.disposeMap();
        this.router.navigate(['/administration']);
    }

    //Map functions
    public northernRegionRenderMap() {
        let reg: IRegionMapRender = {
            region: this.nRegion.region,
            changes: this.doaChangesList
        };
        this.renderMap(reg);
    }

    private initMap() {
        const winObj = this.winRef.nativeWindow;
        this.leafletmap = winObj['app'].leafletmap;

        if (this.leafletmap) {
            this.leafletmap.initMap();
        }
    }

    private mapHealthCheck() {
        this.dataService.getMapHealth().subscribe(response => {
            if (response) {
                this.initMap();
                this.mapHealthy = true;
            }
        }, error => {
            this.mapHealthy = false;
            return false;
        }, () => this.renderRegionInMap(this.model.region.geoFeatures, "Doa")
        );
    }

    public mapAvailable() {
        if (this.leafletmap) {
            return this.leafletmap.isReady();
        }
        return false;
    }

    public renderMap(reg: IRegionMapRender) {
        this.dataService.getGeoFeatures(reg)
            .subscribe((resp: string) => {
                let geoFeatures = resp;
                this.renderRegionInMap(geoFeatures, "DoaModified");
            }, error => {
                this.toastr.error(this.translation.translate('DoasAdminSession.RenderMapErrorMsg'), this.translation.translate('DoasAdminSession.RenderMapErrorTitle'), { 'positionClass': 'toast-bottom-right' });
            });
    }

    private renderRegionInMap(geoFeatures: string, name: string) {
        if (geoFeatures !== null) {
            this.leafletmap.addDoa(geoFeatures, name, function () {
                console.log("OK");
            });
        } else {
            this.toastr.error(this.translation.translate('DoasAdminSession.RenderMapErrorMsg'), this.translation.translate('DoasAdminSession.RenderMapErrorTitle'), { 'positionClass': 'toast-bottom-right' });
        }
    }

    private disposeMap() {
        if (this.leafletmap) {
            this.leafletmap.dispose();
        }
    }
    //End Map functions
}

function radiusValidatorFn(maxRadius: number): ValidatorFn {
    return (c: AbstractControl): { [key: string]: any } => {
        if (typeof (c.value) === "string") {
            if (!c.value) {
                return { 'required': true }
            }
            if (c.value === "") {
                return { 'required': true }
            }
        }
        if (isANumber(c.value)) {
            if (+c.value < 1 || +c.value > maxRadius) {
                return { 'invalid': true }
            }
            var v: number = +c.value;
            if (v > maxRadius) {
                return { 'invalid': true }
            }
            return null;
        }
        return { 'invalid': true }
    };
}

function isANumber(str: string): boolean {
    return !/\D/.test(str);
}

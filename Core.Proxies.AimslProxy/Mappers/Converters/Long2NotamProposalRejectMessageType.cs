﻿namespace NavCanada.Core.Proxies.AimslProxy.Mappers.Converters
{
    using System;

    using AutoMapper;

    using AimslService;

    public class Long2NotamProposalRejectMessageType : ITypeConverter<long, NotamProposalRejectMessageType>
    {
        public NotamProposalRejectMessageType Convert(ResolutionContext context)
        {
            var obj = context.SourceValue;
            if (obj == null)
                throw new NullReferenceException("NotamPorposalId can't be null");

            long notamProposalId;
            if (!long.TryParse(obj.ToString(), out notamProposalId))
                throw new NullReferenceException("NotamPorposalId is not a valid long number");

            var notamProposalRejectMessageType = new NotamProposalRejectMessageType
            {
                transaction = new transactionType
                {
                    uuid = Guid.NewGuid().ToString()
                },
                RequestHeader = new RequestHeaderNotamType
                {
                    Version = "2"
                },
                NotamProposalReject = new NotamProposalRejectType
                {
                    Action = ActionType.Store,
                    NotamProposal = new NotamProposalRejectBaseType
                    {
                        NotamProposalIdentifier = notamProposalId.ToString(),
                        Reason = "Withdrawn by user"
                    }
                }
            };

            return notamProposalRejectMessageType;
        }
    }
}


"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var toastr_service_1 = require("./../../common/toastr.service");
var data_service_1 = require("../shared/data.service");
var UserAddComponent = (function () {
    function UserAddComponent(toastr, fb, dataService, memStorageService, router, activatedRoute) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.model = null;
    }
    UserAddComponent.prototype.ngOnInit = function () {
        this.roleOptions = {
            multiple: true,
            width: "100%"
        };
        this.orgOptions = { width: "100%" };
        this.action = this.activatedRoute.snapshot.data[0].action;
        this.model = this.action !== "add" ? this.activatedRoute.snapshot.data['model'] : this.createEmptyModel();
        this.isReadOnly = false;
        if (this.action === "view") {
            this.isReadOnly = true;
        }
        this.currentPageTitle = this.action;
        this.configureReactiveForm();
        this.loadRoles();
        this.loadOrganizations();
    };
    UserAddComponent.prototype.onRoleChanged = function (data) {
        if (data.value && data.value.length > 0) {
            this.userForm.get('roles').setValue(data.value.join(','));
        }
    };
    UserAddComponent.prototype.onOrgChanged = function (data) {
        if (data.value) {
            this.userForm.get('org').setValue(data.value);
        }
    };
    UserAddComponent.prototype.backToUserList = function () {
        this.router.navigate(['/administration']);
    };
    UserAddComponent.prototype.loadRoles = function () {
        var _this = this;
        this.dataService.getRoles()
            .subscribe(function (roles) {
            _this.roles = roles.map(function (r) {
                return {
                    id: r.roleId,
                    text: r.roleName
                };
            });
            _this.selectedRoleIds = _this.model.roleIds.split(',');
        });
    };
    UserAddComponent.prototype.loadOrganizations = function () {
        var _this = this;
        this.dataService.getOrganizations()
            .subscribe(function (org) {
            _this.orgs = org.map(function (o) {
                return {
                    id: o.id,
                    text: o.name
                };
            });
            _this.selectedOrgId = _this.model.organizationId;
        });
    };
    UserAddComponent.prototype.configureReactiveForm = function () {
        this.userForm = this.fb.group({
            username: { value: this.model.username, disabled: this.action !== "add" },
            firstName: { value: this.model.firstName, disabled: this.isReadOnly },
            lastName: { value: this.model.lastName, disabled: this.isReadOnly },
            position: { value: this.model.position, disabled: this.isReadOnly },
            email: { value: this.model.email, disabled: this.isReadOnly },
            roles: { value: this.model.roleIds, disabled: this.isReadOnly },
            org: { value: this.model.organizationId, disabled: this.isReadOnly },
            address: { value: this.model.address, disabled: this.isReadOnly },
            approved: { value: this.model.approved, disabled: this.isReadOnly },
            disabled: { value: this.model.disabled, disabled: this.isReadOnly },
            telephone: { value: this.model.phoneNumber, disabled: this.isReadOnly },
            fax: { value: this.model.fax, disabled: this.isReadOnly },
        });
    };
    UserAddComponent.prototype.createEmptyModel = function () {
        return {
            username: "",
            firstName: "",
            lastName: "",
            position: "",
            email: "",
            roleIds: "",
            organizationId: -1,
            address: "",
            approved: false,
            disabled: false,
            phoneMumber: "",
            fax: "",
        };
    };
    return UserAddComponent;
}());
UserAddComponent = __decorate([
    core_1.Component({
        templateUrl: '/app/admin/user/user-add.component.html'
    }),
    __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
    __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
        data_service_1.DataService,
        mem_storage_service_1.MemoryStorageService,
        router_1.Router,
        router_1.ActivatedRoute])
], UserAddComponent);
exports.UserAddComponent = UserAddComponent;
//# sourceMappingURL=user-add.component.js.map
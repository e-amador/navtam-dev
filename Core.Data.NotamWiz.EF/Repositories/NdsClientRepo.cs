﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Data.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Data.NotamWiz.EF.Extensions;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.EF.Repositories
{
    public class NdsClientRepo : Repository<NdsClient>, INdsClientRepo
    {
        public NdsClientRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public async Task<NdsClient> GetByIdAsync(int id)
        {
            return await DbSet
                .Include(e => e.Series)
                .Include(e => e.ItemsA)
                .SingleOrDefaultAsync(e => e.Id == id);
        }

        public async Task<List<NdsClientAddressDto>> GetAllSubscriptionsAsync()
        {
            var clients = await DbSet.ToListAsync();

            return GenerateNdsAddressDto(clients);
        }

        public async Task<NdsClient> GetByClientNameAsync(string clientName)
        {
            return await DbSet.SingleOrDefaultAsync(e => e.Client == clientName);
        }

        public async Task<NdsClient> GetByClientAddressAsync(string clientAddress)
        {
            return await DbSet.SingleOrDefaultAsync(e => e.Address == clientAddress);
        }

        public async Task<bool> Exists(string name)
        {
            return await DbSet.AnyAsync(d => d.Client == name);
        }

        public async Task<bool> Exists(int id, string name)
        {
            return await DbSet.AnyAsync(d => d.Client == name && d.Id != id);
        }

        public string GetClientName(string address)
        {
            return DbSet.Where(e => e.Address == address && !string.IsNullOrEmpty(e.Client)).Select(e => e.Client).FirstOrDefault();
        }

        public IEnumerable<NdsClientIdAddressDto> GetMatchingAftnAddresses(string itemA, string series, bool allowAllSeries, bool bilingual, bool onlyActive)
        {
            var query = DbSet.Where(e => e.ClientType == NdsClientType.Aftn && e.French == bilingual && (!onlyActive || e.Active == true))
                             .Include(e => e.Series)
                             .Include(e => e.ItemsA);

            foreach (var client in query)
            {
                if (ClientSubscriptionMatches(client, allowAllSeries, series, itemA))
                    yield return new NdsClientIdAddressDto { Id = client.Id, Address = client.Address };
            }
        }

        public IEnumerable<NdsClientIdAddressDto> GetMatchingAftnAddressesByGeoRef(DbGeography region, int lowerLimit, int upperLimit, bool bilingual, bool onlyActive)
        {
            return DbSet.Where(e => e.ClientType == NdsClientType.Aftn && 
                                    (!onlyActive || e.Active == true) && 
                                    e.French == bilingual && 
                                    e.Region != null && 
                                    // there are 4 cases the height limits intercept: ([) or [(] where the [] are the NOTAM limits
                                    ((lowerLimit <= e.LowerLimit && e.LowerLimit <= upperLimit) || (e.LowerLimit <= lowerLimit && lowerLimit <= e.UpperLimit)) &&
                                    // them the regions must intercept
                                    region.Intersects(e.Region)).Select(e => new NdsClientIdAddressDto { Id = e.Id, Address = e.Address });
        }

        public bool CanAddressPerformQueries(string clientAddress)
        {
            return DbSet.FirstOrDefault(c => c.Address == clientAddress && c.AllowQueries) != null;
        }

        public async Task<int> GetAllCountAsync()
        {
            return await DbSet.CountAsync();
        }

        public async Task<List<NdsClientPartialDto>> GetAllAsync(QueryOptions queryOptions)
        {
            var subscritions = await DbSet
                .Include(e => e.ItemsA)
                .Include(e => e.Series)
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            return GenerateNdsClientDto(subscritions);
        }

        private List<NdsClientPartialDto> GenerateNdsClientDto(List<NdsClient> ndsClients)
        {
            var dtos = new List<NdsClientPartialDto>();
            foreach (var client in ndsClients)
            {
                var series = string.Join(", ", from serie in client.Series select serie.Series);
                var itemsA = string.Join(", ", from serie in client.ItemsA select serie.ItemA);

                dtos.Add(new NdsClientPartialDto
                {
                    Id = client.Id,
                    Client = client.Client,
                    Address = client.Address,
                    ClientType = client.ClientType,
                    ItemsA = itemsA,
                    Series = series,
                    Operator = client.Operator,
                    Updated = client.Updated,
                    AllowQueries = client.AllowQueries,
                    French = client.French,
                    Active = client.Active,
                    LastSynced = client.LastSynced,
                });
            }
            return dtos;
        }

        private List<NdsClientAddressDto> GenerateNdsAddressDto(List<NdsClient> ndsClients)
        {
            var dtos = new List<NdsClientAddressDto>();
            foreach (var client in ndsClients)
            {
                dtos.Add(new NdsClientAddressDto
                {
                    Id = client.Id,
                    Client = client.Client,
                    Address = client.Address,
                    ClientType = client.ClientType,
                });
            }
            return dtos;
        }

        static bool ClientSubscriptionMatches(NdsClient client, bool allowAllSeries, string series, string itemA)
        {
            var ss = MatchesSeries(client.Series.ToList(), allowAllSeries, series);
            var ia = MatchesItemA(client.ItemsA.ToList(), itemA);
            return ss == true && ia != false || ss != false && ia == true;
        }

        static bool? MatchesSeries(List<NdsClientSeries> seriesList, bool allowAllSeries, string series)
        {
            // matches if there are series and the series passed matches
            if (seriesList.Count != 0)
                return seriesList.Exists(s => series.Equals(s.Series));

            // if not allowed all-series then it doesn't match
            if (!allowAllSeries)
                return false;

            // null means matches but because is an all-series subscription
            return null;
        }

        static bool? MatchesItemA(List<NdsClientItemA> itemAs, string itemA)
        {
            // empty itemA matches anything (checklist case)
            if (string.IsNullOrEmpty(itemA))
                return true;

            if (itemAs.Count != 0)
                return itemAs.Exists(i => itemA.Equals(i.ItemA));
            else
                return null;
        }
    }
}
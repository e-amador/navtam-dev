﻿define(["jquery", "Resources", "utilities", "kendo/kendo.core.min"], function ($, res, Util) {
    var RES = res.getResources("NSD-COMMON");
    var thisProposal;
    return {
        NSDURL: null,
        ReadOnly: false,
        NOTAMGenerated: false,
        PackageId: null,
        PropsalType: null,
        subject: null,
        NOTAM: null,
        Options: false,
        doaGeo: null,
        Setting: null,
        nsd: null,
        nsdTokens: null,
        validModel: false, // parsing finishing flag
        user: null,
        init: function (model, user, options) {
            thisProposal = this;
            thisProposal.ReadOnly = model.ReadOnly;
            thisProposal.PackageId = model.PackageId;
            thisProposal.PropsalType = model.PropsalType;
            thisProposal.Setting = model.Setting;
            thisProposal.doaGeo = model.DOAGeoJson;            
            thisProposal.IsFicRole = model.IsFicRole;
            thisProposal.nsd = model.nsd;
            thisProposal.NOTAM = kendo.observable($.extend({
                StartValidityText: kendo.toString(this.StartValidity, "yyMMddHHmm"),
                EndValidityText: kendo.toString(this.EndValidity, "yyMMddHHmm"),
                DisplayLower: function () {
                    return Util.pad(this.Lower, 3);
                },
                DisplayUpper: function () {
                    return Util.pad(this.Upper, 3);
                },
                EstimatedText: function () {
                    return this.Estimated ? "EST" : "";
                },
                DisplayRadius: function () {
                    return Util.pad(this.Radius, 3);
                }
            }, model.notam));


            thisProposal.nsdTokens = JSON.parse(thisProposal.NOTAM.Tokens);
            thisProposal.user = user;
            thisProposal.NOTAM.bind("change", function (e) {
                if (e.field === "Radius") {
                    thisProposal.NOTAM.trigger("change", { field: "DisplayRadius" });
                }
                else if (e.field === "Estimated") {
                    thisProposal.NOTAM.trigger("change", { field: "EstimatedText" });
                }
                else if (e.field === "Lower") {
                    thisProposal.NOTAM.trigger("change", { field: "DisplayLower" });
                }
                else if (e.field === "Upper") {
                    thisProposal.NOTAM.trigger("change", { field: "DisplayUpper" });
                }
                else if (e.field === "StartValidity") {
                    if (this.StartValidity !== null && this.StartValidity !== undefined) {
                        debugger;
                        thisProposal.NOTAM.set("StartValidityText", kendo.toString(new Date(this.StartValidity), "yyMMddHHmm"));
                    }
                    else {
                        thisProposal.NOTAM.set("StartValidityText", "          ");
                    }
                }
                else if (e.field === "EndValidity") {
                    if (this.EndValidity !== null && this.EndValidity !== undefined ) {
                        thisProposal.NOTAM.set("EndValidityText", kendo.toString(new Date(this.EndValidity), "yyMMddHHmm"));
                    }
                    else {
                        thisProposal.NOTAM.set("EndValidityText", "          ");
                    }
                }
            });              
            //fix dates
            if (thisProposal.PropsalType !== "C")
            {
                thisProposal.NOTAM.set("EndValidity", Util.fromDotNetDate(model.notam.EndValidity));
            }
            else
            {
                thisProposal.NOTAM.set("EndValidity", null);
            }
            thisProposal.NOTAM.set("StartValidity", Util.fromDotNetDate(model.notam.StartValidity));
            thisProposal.NOTAM.set("StatusTime", Util.fromDotNetDate(model.notam.StatusTime));
            thisProposal.Options = $.extend({
                //Bilingual: model.Bilingual,
                AeroServices: "/SDO/"
            }, options);
            return thisProposal;
        },
        createNOTAMViewModel: function (NSDViewModel) {
            thisProposal.nsdTokens = kendo.observable($.extend(NSDViewModel, JSON.parse(thisProposal.NOTAM.Tokens)));
            thisProposal.nsdTokens.bind("set", function () {
                thisProposal.NOTAMGenerated = false;
            });
            debugger;
            if (thisProposal.PropsalType === "C") {
                thisProposal.nsdTokens.set("SetBeginDateAtSubmit", true);
                thisProposal.nsdTokens.set("StartValidity", null);
                thisProposal.nsdTokens.set("EndValidity", null);
            }
            else {
                thisProposal.nsdTokens.set("StartValidity", thisProposal.nsdTokens.StartValidity ? new Date(thisProposal.nsdTokens.StartValidity) : null);
                thisProposal.nsdTokens.set("EndValidity", thisProposal.nsdTokens.EndValidity ? new Date(thisProposal.nsdTokens.EndValidity) : null);
            }
            kendo.bind($("#proposalEditor"), thisProposal.nsdTokens);
        },
        IsWithinDOA: function (loc) {
            var methodUrl = thisProposal.NSDURL + "/IsWithinDoa";
            var result = false;
            $.ajax({
                url: methodUrl,
                type: "POST",
                data: JSON.stringify({ location: loc }),
                contentType: "application/json",
                dataType: "json",
                async: false,
            })
                .done(function (data) {
                    if (data["WithinDOA"]) {
                        result = true;
                    }
                })
                .fail(function (jqXHR, textStatus, err) {
                }).complete(function () {
                });
            return result;
        },
        generateNOTAM: function () {
            thisProposal.generateNOTAMCheck(true);
        },
        generateNOTAMCheck: function (check) {
            if (check) {
                if (thisProposal.NOTAMGenerated) {
                    return; // prevent multiple generation
                }
            }
            var generateNOTAMUrl = thisProposal.NSDURL + "/GenerateNotamModel";
            thisProposal.NOTAMGenerated = true;
            // get notam syntax from server
            $.ajax({
                url: generateNOTAMUrl,
                type: "POST",
                data: JSON.stringify({ proposalType: thisProposal.PropsalType, packageId: thisProposal.PackageId, subjectId: thisProposal.nsdTokens.SdoEntityId, tokens: JSON.stringify(thisProposal.nsdTokens) }),
                contentType: "application/json",
                dataType: "json",
                async: true,
            })
                .done(function (data) {
                    data.Id = thisProposal.NOTAM.Id;
                    //data.PackageId = thisProposal.NOTAM.PackageId;

                    if (data["Error"] !== undefined) {
                        kendo.ui.ExtAlertDialog.show({
                            title: RES.dbRes("Error"),
                            message: data["Error"],
                            icon: "k-Ext-error"
                        });
                    }
                    else {
                        var keys = Object.keys(data);
                        for (var k in keys) {
                            if (keys[k] in thisProposal.NOTAM &&
                                typeof data[keys[k]] !== "function") {
                                if ((data[keys[k]] + "").indexOf("/Date(") > -1) {
                                    thisProposal.NOTAM.set(keys[k], Util.fromDotNetDate(data[keys[k]]));
                                }
                                else {
                                    thisProposal.NOTAM.set(keys[k], data[keys[k]]);
                                }
                            }
                        }

                        var reviewTab = $("#reviewTab").kendoTabStrip().data("kendoTabStrip");
                        thisProposal.validModel = true;
                        Util.setConditionValid(true);
                        reviewTab.enable(reviewTab.tabGroup.children().eq(1), true);

                        //if (thisProposal.nsdTokens.ICAOSubjectId > 0 && thisProposal.nsdTokens.ICAOSubjectConditionId > 0) {
                        //    thisProposal.validModel = true;
                        //    Util.setConditionValid(true);
                        //    reviewTab.enable(reviewTab.tabGroup.children().eq(1), true);
                        //} else {
                        //    thisProposal.validModel = false;
                        //    Util.setConditionValid(false);
                        //    reviewTab.enable(reviewTab.tabGroup.children().eq(1), false);
                        //}
                    }
                })
                .fail(function (jqXHR, textStatus, err) {
                }).complete(function () {
                });
        },
    };
});
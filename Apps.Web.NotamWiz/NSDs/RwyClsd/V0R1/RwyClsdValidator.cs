﻿using NavCanada.Applications.NotamWiz.Web.Code.Helpers;
using NavCanada.Core.Data.NotamWiz.Contracts;

namespace NavCanada.Applications.NotamWiz.Web.NSDs.RwyClsd.V0R1
{
    using System.Threading.Tasks;

    using FluentValidation;
    using FluentValidation.Results;


    public class RwyClsdValidator : AbstractValidator<ViewModel>
    {
        private readonly NsdBaseController controller;

        private readonly IUow uow;

        public RwyClsdValidator(NsdBaseController controller, IUow uow, IDbResWrapper dbRes)
        {
            this.controller = controller;
            this.uow = uow;

            //RuleFor(m => m.ShowFreeText).Must(ValidateFreeText).WithMessage(dbRes.T("Invalid Free Text","NSD-RwyClsd"));
            RuleFor(m => m.WingSpan).Must(a => !a.HasValue || (a.Value >= 0 && a.Value < 999)).WithMessage(dbRes.T("Invalid Wing Span", "NSD-RwyClsd")); ;
            RuleFor(m => m.Weight).Must(a=> !a.HasValue || (a.Value>=0 && a.Value<500000)).WithMessage(dbRes.T("Invalid Weight Accepted Values(0-999)", "NSD-RwyClsd")); ;
        }

     
        //private bool ValidateFreeText(ViewModel model, bool freetextRequired)
        //{
        //    if (freetextRequired)
        //    {
        //        return !(string.IsNullOrEmpty(model.FreeText) && string.IsNullOrEmpty(model.FreeTextFr));
        //    }
        //    return true;
        //}
    }
}
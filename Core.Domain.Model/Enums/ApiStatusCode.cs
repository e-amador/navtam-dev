﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum ApiStatusCode
    {
        Success,

        Error,

        ConnectionError
    }
}

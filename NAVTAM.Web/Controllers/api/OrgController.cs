﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using NAVTAM.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [RoutePrefix("api/admin")]
    [Authorize(Roles = "Administrator, Organization Administrator, CCS, NOF_ADMINISTRATOR")]
    public class OrgController : ApiBaseController
    {
        public OrgController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger)
            : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
        }

        [Route("orgs/partial")]
        [HttpGet]
        public async Task<IHttpActionResult> GetOrganizations()
        {
            return await LogAction(async () =>
            {
                var orgScopeId = await GetScopeOrganizationId();
                if (orgScopeId.HasValue)
                {
                    var org = await Uow.OrganizationRepo.GetByIdAsync(orgScopeId.Value);
                    if (org == null)
                        return NotFound();

                    return Ok(new List<OrgPartialDto> { new OrgPartialDto() { Id = org.Id, Name = org.Name } });
                }
                else
                {
                    var orgs = await Uow.OrganizationRepo.GetAllPartialAsync();
                    orgs.Sort((o1, o2) => string.Compare(o1.Name, o2.Name, false));
                    return Ok(orgs);
                }
            }, "api/admin/orgs/partial");
        }

        [HttpGet, Route("orgs")]
        [Authorize(Roles = "Administrator, CCS")]
        public async Task<IHttpActionResult> Get(int id)
        {
            return await LogAction(async () => Ok(await Uow.OrganizationRepo.GetOrgInfoAsync(id)), $"api/admin/orgs{id}");
        }

        [HttpGet, Route("orgs/users/count")]
        public async Task<IHttpActionResult> GetUserCountInOrganization(int id)
        {
            return await LogAction(async () =>
            {
                var scopeOrg = await GetScopeOrganizationId();
                if (scopeOrg.HasValue && scopeOrg.Value != id)
                    return NotFound();

                var count = await Uow.UserProfileRepo.GetUserCountInOrganization(id, true, false);

                return Ok(count);
            }, "api/admin/orgs/users/count");
        }

        [HttpGet, Route("orgs/doas")]
        public async Task<IHttpActionResult> GetDoasInOrg(int id)
        {
            return await LogAction(async () =>
            {
                var scopeOrg = await GetScopeOrganizationId();
                if (scopeOrg.HasValue && scopeOrg.Value != id)
                    return NotFound();

                var doasOrg = await Uow.OrganizationDoaRepo.GetOrganizationDoasAsync(id);

                return Ok(doasOrg.Select(d => new { d.Id, d.Name }));
            }, $"api/admin/orgs/doas/{id}");
        }

        [HttpPost, Route("orgs/page", Name = "RegisteredOrgsPage")]
        [Authorize(Roles = "Administrator, CCS")]
        public async Task<IHttpActionResult> GetOrgsInPage([FromBody]QueryOptions queryOptions)
        {
            return await LogAction(async () =>
            {
                if (queryOptions.PageSize > MaxRecordsToReturn)
                    queryOptions.PageSize = MaxRecordsToReturn;

                var totalCount = await Uow.OrganizationRepo.GetAllCountAsync(queryOptions);
                var orgs = await Uow.OrganizationRepo.GetAllAsync(queryOptions);
                if (orgs.Count == 0 && queryOptions.Page > 1)
                {
                    queryOptions.Page = queryOptions.Page - 1;
                    orgs = await Uow.OrganizationRepo.GetAllAsync(queryOptions);
                }

                return Ok(ResponsewithPagingEnvelope(queryOptions, "RegisteredOrgsPage", orgs, totalCount));
            }, "api/admin/orgs/page");
        }

        [HttpGet, Route("orgs/orgunique")]
        [Authorize(Roles = "Administrator, CCS")]
        public async Task<IHttpActionResult> IsOrganizationUnique(int id, string name)
        {
            return await LogAction(async () => Ok(await Uow.OrganizationRepo.IsNameAvailableAsync(id, name)), $"api/admin/orgs/orgunique/{id}/{name}");
        }

        [HttpGet, Route("orgs/userunique")]
        [Authorize(Roles = "Administrator, CCS")]
        public async Task<IHttpActionResult> IsUserUnique(string name)
        {
            return await LogAction(async () => Ok(await UserManager.FindByNameAsync(name) == null), "api/admin/orgs/userunique/{name}");
        }

        [HttpGet]
        [Route("orgs/getorgsbynsdid")]
        [Authorize(Roles = "Administrator, CCS")]
        public async Task<IHttpActionResult> GetOrgsByNsdId(int nsdId, string timeRequest)
        {
            return await LogAction(async () => Ok(await Uow.OrganizationRepo.GetOrganizationsByNsdId(nsdId)), $"api/admin/orgs/getorgsbynsdid/{nsdId}");
        }

        [HttpPost, Route("orgs")]
        [Authorize(Roles = "Administrator, CCS")]
        public Task<IHttpActionResult> Create([FromBody]OrganizationCreateViewModel model)
        {
            return LogAction(() => ExecuteCreate(model), "POST: api/admin/orgs");
        }

        [HttpPut, Route("orgs")]
        [Authorize(Roles = "Administrator, CCS")]
        public Task<IHttpActionResult> Update([FromBody]OrganizationUpdateViewModel model)
        {
            return LogTransaction(() => ExecuteUpdate(model), "PUT: api/admin/orgs");
        }

        [HttpDelete, Route("orgs/{id}")]
        [Authorize(Roles = "Administrator, CCS")]
        public Task<IHttpActionResult> Delete(int id)
        {
            return LogTransaction(() => ExecuteDelete(id), $"DELETE: api/admin/orgs/{id}");
        }

        private async Task<IHttpActionResult> ExecuteCreate([FromBody]OrganizationCreateViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid);

            if (await Uow.OrganizationRepo.IsNameAvailableAsync(0, model.Name) == false)
                return BadRequest($"Organization '{model.Name}' already exist!");

            if (await UserManager.FindByNameAsync(model.AdminUserName) != null)
                return BadRequest($"User '{model.AdminUserName}' already exist!");

            // try to create the administrator
            UserProfile user = null;
            try
            {
                user = await CreateOrganizationAdministrator(model);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

            // create the organization and connect to the selected Doas
            Organization org = null;
            Uow.BeginTransaction();
            try
            {
                org = await CreateOrganization(model);

                await CreateOrganizationDoas(org.Id, model.AssignedDoaIds);
                await CreateAdministratorDoas(org.Id, user.Id, model.AssignedDoaIds);
                await CreateOrganizationNsds(org.Id, model.AssignedNsdIds);

                Uow.Commit();
            }
            catch (Exception)
            {
                Uow.Rollback();

                await DeleteOrganizationAdministrator(user);

                throw;
            }

            // link the user to the organization
            await LinkUserToOrganization(user.Id, org.Id);

            return Ok(model);
        }

        private async Task<ApiStepResult> ExecuteUpdate([FromBody]OrganizationUpdateViewModel model)
        {
            if (!ModelState.IsValid)
                return ApiStepResult.Fail(BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid));

            if (await Uow.OrganizationRepo.IsNameAvailableAsync(model.Id, model.Name) == false)
                return ApiStepResult.Fail(BadRequest($"Organization '{model.Name}' already exist!"));

            var org = await Uow.OrganizationRepo.GetByIdAsync(model.Id);

            if (org != null)
            {
                //Uow.BeginTransaction();
                try
                {
                    org.Name = model.Name;
                    org.EmailAddress = model.EmailAddress;
                    org.Address = model.Address;
                    org.Telephone = model.Telephone;
                    org.HeadOffice = model.HeadOffice;
                    org.EmailDistribution = model.EmailDistribution;
                    org.TypeOfOperation = model.TypeOfOperation;
                    org.Type = model.Type;
                    Uow.OrganizationRepo.Update(org);
                    await Uow.SaveChangesAsync();

                    await UpdateOrganizationDoas(model.Id, model.AssignedDoaIds);
                    await UpdateOrganizationNsds(model.Id, model.AssignedNsdIds);

                    //Uow.Commit();
                }
                catch (Exception)
                {
                    //Uow.Rollback();
                    throw;
                }

                Logger.LogInfo(GetType(), $"Organization '{model.Name}' updated  by '{ApplicationContext.GetUserName()}'.");
            }

            return ApiStepResult.Succeed(Ok(model));
        }

        private async Task<ApiStepResult> ExecuteDelete(int id)
        {
            //var userCount = await Uow.UserProfileRepo.GetUserCountInOrganization(id, true, false);

            //if (userCount > 0)
            //    return BadRequest("Organization Contains Users");

            var org = await Uow.OrganizationRepo.GetByIdAsync(id);
            if (org == null)
                return ApiStepResult.Fail(NotFound());

            //Uow.BeginTransaction();
            try
            {
                await Uow.OrganizationDoaRepo.UpdateOrganizationDoas(org.Id, Enumerable.Empty<int>());
                await Uow.UserDoaRepo.AdjustUserDoas(org.Id, Enumerable.Empty<int>());

                var date = DateTime.Now.ToShortDateString();
                var savedName = org.Name;

                org.Name = $"{org.Name} (deleted on {date})";
                org.Deleted = true;

                Uow.OrganizationRepo.Update(org);

                var users = await Uow.UserProfileRepo.GetUsersInOrganization(id, false, false); //include disabled users but not deleted users
                foreach (var user in users)
                {
                    user.IsDeleted = true;
                    Uow.UserProfileRepo.Update(user);
                }

                await Uow.SaveChangesAsync();

                //Uow.Commit();

                Logger.LogInfo(GetType(), $"Organization '{savedName}' removed  by '{ApplicationContext.GetUserName()}'.");

                return ApiStepResult.Succeed(Ok());
            }
            catch (Exception)
            {
                ///Uow.Rollback();
                throw;
            }
        }

        private async Task<Organization> CreateOrganization(OrganizationCreateViewModel model)
        {
            var org = new Organization
            {
                Name = model.Name,
                EmailAddress = model.EmailAddress,
                Address = model.Address,
                Telephone = model.Telephone,
                HeadOffice = model.HeadOffice,
                EmailDistribution = model.EmailDistribution,
                TypeOfOperation = model.TypeOfOperation,
                Type = model.Type,
            };

            Uow.OrganizationRepo.Add(org);

            await Uow.SaveChangesAsync();

            Logger.LogInfo(GetType(), $"Organization '{model.Name}' created  by '{ApplicationContext.GetUserName()}'.");

            return org;
        }

        private async Task CreateOrganizationDoas(int orgId, IEnumerable<int> doaIds)
        {
            if (doaIds != null)
            {
                foreach (var doaId in doaIds.Distinct())
                {
                    Uow.OrganizationDoaRepo.Add(new OrganizationDoa { DoaId = doaId, OrganizationId = orgId });
                }
                await Uow.SaveChangesAsync();
            }
        }

        private async Task CreateAdministratorDoas(int orgId, string userId, IEnumerable<int> doaIds)
        {
            if (doaIds != null)
            {
                foreach (var doaId in doaIds.Distinct())
                {
                    Uow.UserDoaRepo.Add(new UserDoa { DoaId = doaId, UserId = userId, OrganizationId = orgId });
                }
                await Uow.SaveChangesAsync();
            }
        }

        private async Task CreateOrganizationNsds(int orgId, IEnumerable<int> nsdIds)
        {
            if (nsdIds != null)
            {
                foreach (var nsdId in nsdIds.Distinct())
                {
                    Uow.OrganizationNsdRepo.Add(new OrganizationNsdCategory { OrganizationId = orgId, NsdCategoryId = nsdId });
                }
                await Uow.SaveChangesAsync();
            }
        }

        private async Task UpdateOrganizationDoas(int orgId, IEnumerable<int> doaIds)
        {
            if (doaIds != null)
            {
                await Uow.OrganizationDoaRepo.UpdateOrganizationDoas(orgId, doaIds);
                await Uow.UserDoaRepo.AdjustUserDoas(orgId, doaIds);
                await Uow.SaveChangesAsync();
            }
        }

        private async Task UpdateOrganizationNsds(int orgId, List<int> nsdIds)
        {
            if (nsdIds != null)
            {
                await Uow.OrganizationNsdRepo.UpdateOrganizationNsds(orgId, nsdIds);
                await Uow.SaveChangesAsync();
            }
        }

        private async Task<UserProfile> CreateOrganizationAdministrator(OrganizationCreateViewModel model)
        {
            var user = new UserProfile
            {
                UserName = model.AdminUserName,
                Email = model.EmailAddress,
                FirstName = model.AdminFirstName,
                LastName = model.AdminLastName,
                Address = model.Address,
                IsApproved = true,
                IsDeleted = false,
                Fax = "",
                PhoneNumber = model.Telephone,
                Position = $"Administrator of {model.Name}",
                OrganizationId = null, // link later
                EmailConfirmed = false,
                LastPasswordChangeDate = DateTime.UtcNow,
            };

            var password = "^Zx0!" + Guid.NewGuid().ToString("n").Substring(0, 8);


            var result = await UserManager.CreateAsync(user, password);
            if (!result.Succeeded)
                throw new Exception($"Failed to create user '{model.Name}'.");

            UserRegistration userRegistration = null;
            var onDisasterRecovery = ConfigurationManager.AppSettings["DisasterRecoveryMode"] == "1";

            if (onDisasterRecovery)
            {
                userRegistration = await Uow.UserRegistrationRepo.GetUserRegistrationAsync(user.Id);
            }

            //var tokenConfirmation = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
            var tokenConfirmation = await UserManager.GenerateUserTokenAsync("EmailConfirmation", user.Id);
            if (onDisasterRecovery)
            {
                if (userRegistration == null)
                {
                    userRegistration = new UserRegistration
                    {
                        UserProfileId = user.Id,
                        VerificationCode = tokenConfirmation
                    };
                    Uow.UserRegistrationRepo.Add(userRegistration);
                }
                else
                {
                    userRegistration.VerificationCode = tokenConfirmation;
                    Uow.UserRegistrationRepo.Update(userRegistration);
                }
            }

            await Uow.SaveChangesAsync();

            result = await UserManager.AddToRoleAsync(user.Id, CommonDefinitions.OrgAdminRole);
            if (!result.Succeeded)
                throw new Exception($"Failed to add role '{CommonDefinitions.OrgAdminRole}' to user '{model.Name}'.");

            if (!onDisasterRecovery)
            {
                await SendVerificationEmail(user.Id, tokenConfirmation);
            }

            return user;
        }

        private async Task LinkUserToOrganization(string userId, int orgId)
        {
            var user = await UserManager.FindByIdAsync(userId);
            if (user != null)
            {
                user.OrganizationId = orgId;
                await UserManager.UpdateAsync(user);
            }
        }

        private async Task DeleteOrganizationAdministrator(UserProfile user)
        {
            await UserManager.DeleteAsync(user);
        }
    }
}
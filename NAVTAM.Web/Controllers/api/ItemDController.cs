﻿using FParsec;
using Microsoft.FSharp.Core;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using NAVTAM.ViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [RoutePrefix("api/itemd")]
    [Authorize]
    public class ItemDController : ApiBaseController
    {
        public ItemDController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger) : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
        }

        [HttpGet]
        [Route("{proposalId}")]
        public async Task<IHttpActionResult> Get(int proposalId)
        {
            return await LogAction(async () => Ok(await Uow.ItemDRepo.GetByProposalId(proposalId)), $"GET: api/itemd/{proposalId}");
        }

        [HttpPost]
        [Route("validate")]
        public IHttpActionResult Validate(ItemDValidatorViewModel viewmodel)
        {
            //var isValid = true;  

            //var blocks = viewmodel.ItemD.Split(' ');
            //foreach(var block in blocks )
            //{
            //    isValid = ValidateItemD(block);
            //    if (!isValid) break;
            //}
            //if( !isValid )
            //{
            //    return Ok(new
            //    {
            //        Case = "Failure",
            //        ErrorMessage = "One or more invalid characters were found. "
            //    });
            //}

            //var result = Parser.Agent.Run(viewmodel.ItemD);
            //if (result.IsFailure)
            //{
            //    return Ok( new
            //        {
            //            Case = "Failure",
            //            //ErrorMessage = CastFailure(result) 
            //            ErrorMessage = "The schedule is not properly formatted"
            //        });
            //}
            //else 
            return Ok(new { Case = "Success" });

            //In order to Mock the F# Parser module, inject an interface containing the Parser signature
            //return LogAction(() => Ok(Parser.Agent.Run(viewmodel.ItemD)), $"POST: api/itemd/validate => {viewmodel.ItemD}");
        }


        [HttpPost]
        public async Task<IHttpActionResult> Save(ItemDViewModel[] model)
        {
            return await LogTransaction(async () =>
            {
                if (!ModelState.IsValid) return ApiStepResult.Fail(BadRequest());

                if(model.Length == 0 ) return ApiStepResult.Fail(BadRequest());

                var savedItemDs = await Uow.ItemDRepo.GetByProposalId(model[0].ProposalId);

                //Uow.BeginTransaction();
                try
                {
                    foreach (var itemD in savedItemDs)
                    {
                        Uow.ItemDRepo.Delete(itemD);
                    }
                    await Uow.SaveChangesAsync();

                    foreach(var item in model)
                    {
                        var itemD = new ItemD
                        {
                            CalendarId = item.CalendarId,
                            CalendarType = item.CalendarType,
                            ProposalId = item.ProposalId,
                            EventId = item.EventId,
                            Event = item.Event
                        };
                        Uow.ItemDRepo.Add(itemD);
                    }
                    await Uow.SaveChangesAsync();

                    //Uow.Commit();

                    return ApiStepResult.Succeed(Ok());
                }
                catch (Exception)
                {
                    //Uow.Rollback();
                    throw;
                }
            }, "POST: api/itemd");
        }

        [HttpDelete]
        [Route("{proposalId}")]
        public async Task<IHttpActionResult> Delete(int proposalId)
        {
            return await LogTransaction(async () =>
            {
                var proposal = await Uow.ProposalRepo.GetByIdAsync(proposalId);

                //Uow.BeginTransaction();
                try
                {
                    if (proposal != null)
                    {
                        proposal.ItemD = null;
                        Uow.ProposalRepo.Update(proposal);
                    }

                    var itemDs = await Uow.ItemDRepo.GetByProposalId(proposalId);
                    foreach (var itemD in itemDs)
                    {
                        Uow.ItemDRepo.Delete(itemD);
                    }

                    await Uow.SaveChangesAsync();

                    //Uow.Commit();

                    return ApiStepResult.Succeed(Ok(true));
                }
                catch (Exception)
                {
                    //Uow.Rollback();
                    throw;
                }

            }, $"DELETE: api/itemd/{proposalId}");
        }

        private bool ValidateItemD( string block )
        {
            var isValid = true;
            switch (block)
            {
                case "DAILY":
                case "SS":
                case "SR":
                case "SS-SR":
                case "SR-SS":
                case "SUN":
                case "MON":
                case "TUE":
                case "WED":
                case "THU":
                case "FRI":
                case "SAT":
                case "JAN":
                case "FEB":
                case "MAR":
                case "APR":
                case "MAY":
                case "JUN":
                case "JUL":
                case "AUG":
                case "SEP":
                case "OCT":
                case "NOV":
                case "DEC":
                case " ":
                    break;
                default:
                    isValid = ValidateBlock(block);
                    break;
            }

            return isValid;
        }

        bool ValidateBlock(string block)
        {
            var isValid = true;
            if (block.Contains('-'))
            {
                var parts = block.Split('-');
                for( var i = 0; i < parts.Length; i++ )
                {
                    if(parts[i].Contains(" ") )
                    {
                        //isValid = false;
                        //break;
                        continue;
                    }
                    else if (parts[i].Contains("PLUS") || parts[i].Contains("MINUS")) //PLUSXX - MINUSXX
                    {
                        if( !ValidatePlusOrMinus(parts[i]) )
                        {
                            isValid = false;
                            break;
                        }
                    }
                    else if (parts[i].Contains("SR") || parts[i].Contains("SS")) //SR - SS
                    {
                        if( parts[i].Length != 2 )
                        {
                            isValid = false;
                            break;
                        }
                    }
                    else if (parts[i].Length == 4 || parts[i].Length == 2) //XXXX or XX
                    {
                        if (!parts[i].All(char.IsDigit))
                        {
                            isValid = false;
                            break;
                        }
                    }
                    else
                    {
                        isValid = false;
                        break;
                    }
                }
            }

            return isValid;
        }

        bool ValidatePlusOrMinus(string section)
        {
            var length = section.Contains("PLUS") ? 6 : 7; 

            if (section.Length == length) //PLUSXX or MINUSXX
            {
                var digits = section.Substring(section.Length - 2);
                return digits.All(char.IsDigit);
            }

            return false;
        }

        //public static string CastFailure(CharParsers.ParserResult<Common.ParserResult, Unit> fshartObj)
        //{
        //    var error = ((CharParsers.ParserResult<Common.ParserResult, Unit>.Failure)fshartObj).Item1;
        //    return error.Replace("\r\n", "").Replace("\n", "").Replace("\r", "");
        //}    
    }
 }

﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using NavCanada.SE.NotamWiz.Data.Contracts;
using NavCanada.SE.NotamWiz.Domain.Model.Entitities;
using NavCanada.SE.NotamWiz.UI.Wiz.Web.Models;

namespace NavCanada.SE.NotamWiz.UI.Wiz.Web.Code.Configurations.AutoMapperResolvers
{
    public class NSD2VMConvertor : ITypeConverter<Nsd, NsdViewModel>
    {
        public NsdViewModel Convert(ResolutionContext context)
        {
            IUow db = (IUow)context.Options.Items["db"];

            Nsd d = context.SourceValue as Nsd;
            List<long> limitedOrgs = d.LimitedToOrganizatations == null?new List<long>():d.LimitedToOrganizatations.Select(a => a.Id).ToList();

            NsdViewModel nsdVM = new NsdViewModel()
            {
                Id = d.Id,
                Name = d.Name,
                FilesLocation = d.FilesLocation,
                Category = d.Category,
                Description = d.Description,
                Active = d.Active,
                LimitedToOrganizatations = db.OrganizationRepo.GetAll().Where(o => limitedOrgs.Contains(o.Id)).Select(o => new OrganizationListViewModel() { Id = o.Id, Name = o.Name } ).ToList(),
                Versions = Mapper.Map<List<NsdVersion>, List<NsdVersionViewModel>>(d.Versions.ToList())
            };

            return nsdVM;
        }
    }


    public class VM2NSDConvertor : ITypeConverter<NSDVM, NSD>
    {
        public NSD Convert(ResolutionContext context)
        {
            NOTAMWizDBContext db = (NOTAMWizDBContext)context.Options.Items["db"];

            NSDVM d = context.SourceValue as NSDVM;    
        
            long[] orgs = d.LimitedToOrganizatations==null? new long[0]:d.LimitedToOrganizatations.Select(o=>o.Id).ToArray();

            NSD nsd = (NSD)context.DestinationValue??new NSD();
            nsd.Id = d.Id;
            nsd.Name = d.Name;
            nsd.Category = d.Category;
            nsd.Description = d.Description;
            nsd.FilesLocation = d.FilesLocation;
            nsd.Active = d.Active;
            nsd.LimitedAccess = d.LimitedToOrganizatations != null && d.LimitedToOrganizatations.Count > 0;
            nsd.LimitedToOrganizatations =d.LimitedToOrganizatations!=null?db.Organizations.Where(o => orgs.Contains(o.OrganizationId)).ToList():null;
            nsd.Versions = Mapper.Map<List<NSDVersionVM>, List<NSDVersion>>(d.Versions);


            return nsd;
        }
    }


}
﻿using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using NAVTAM.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [RoutePrefix("api/nsdcategories")]
    [Authorize]
    public class NsdCategoryController : ApiBaseController
    {
        public NsdCategoryController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger)
            : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
        }

        [Route("nsdleaves")]
        [HttpGet]
        [Authorize(Roles = "Administrator, CCS")]
        public async Task<IHttpActionResult> GetLeaves()
        {
            return await LogAction(async () =>
            {
                // get the children categories (with parents)
                var children = await Uow.NsdCategoryRepo.GetRootChildrenAsync();

                // create set of the parent categories
                var parents = new HashSet<int>(children.Select(e => e.ParentCategoryId ?? 0));

                // filter the leaves (no children exists)
                var leaves = children.Where(e => !parents.Contains(e.Id) && e.Operational).ToList();

                // convert to dto
                var dtos = leaves.Select(e => new { e.Id, e.Name });

                return Ok(dtos);
            }, "api/nsdcategories/nsdleaves");
        }

        [HttpGet]
        public async Task<IHttpActionResult> Get()
        {
            return await LogAction(async () =>
            {
                var categories = await Uow.NsdCategoryRepo.GetRootChildrenAsync();

                var nodeTree = new List<NodeTree>();
                foreach (var cat in categories)
                {
                    if (!nodeTree.Any())
                    {
                        //first grouping node
                        nodeTree.Add(CreateNode(cat));
                        continue;
                    }

                    if (nodeTree.FirstOrDefault(e => e.GroupCategoryId == cat.ParentCategoryId) != null)
                    {
                        //it is another grouping node
                        nodeTree.Add(CreateNode(cat));
                    }
                    else
                    {
                        var node = nodeTree.FirstOrDefault(e => e.Id == cat.ParentCategoryId);
                        if (node != null)
                        {
                            if (node.Children == null)
                                node.Children = new List<NodeTree>();

                            node.Children.Add(CreateNode(cat));
                        }
                    }
                }

                return Ok(nodeTree);
            }, "GET: api/nsdcategories");
        }

        [Route("getnsds")]
        [HttpGet]
        //This is a fix? for IE that cached the get request always. Everytime that a request is sent, 
        //we need to sent something different, like the parameter timeRequest that is not used on the Controller
        public async Task<IHttpActionResult> GetNsds(string timeRequest)
        {
            return await LogAction(async () =>
            {
                var nsds = await Uow.NsdCategoryRepo.GetAllNsds();
                var nsddto = new List<NsdManagementDto>();

                if (nsds.Count > 0)
                {
                    foreach (var nsd in nsds)
                    {
                        nsddto.Add(new NsdManagementDto
                        {
                            Id = nsd.Id,
                            Name = nsd.Name,
                            Operational = nsd.Operational
                        });
                    }
                }

                return Ok(nsddto);
            }, "api/nsdcategories/getnsds");
        }

        [HttpGet]
        public async Task<IHttpActionResult> Get(int orgId)
        {
            return await LogAction(async () =>
            {
                var nsds = await Uow.NsdCategoryRepo.GetActiveNsdsByOrganizationId(orgId);
                var nsddto = new List<NsdManagementDto>();

                if (nsds.Count > 0)
                {
                    foreach (var nsd in nsds)
                    {
                        nsddto.Add(new NsdManagementDto
                        {
                            Id = nsd.Id,
                            Name = nsd.Name,
                            Operational = nsd.Operational
                        });
                    }
                }

                return Ok(nsddto);
            }, $"api/nsdcategories/{orgId}");
        }

        [Route("activeorg")]
        [HttpGet]
        public async Task<IHttpActionResult> GetNSDByActiveOrg()
        {
            return await LogAction(async () =>
            {
                var orgId = GetOrganizationId();
                var nsds = await Uow.NsdCategoryRepo.GetOperationalNsdsByOrganizationId(orgId);

                var dtos = nsds.Select(nsd => new NsdManagementDto
                {
                    Id = nsd.Id,
                    Name = nsd.Name,
                    Operational = nsd.Operational
                }).ToList();

                return Ok(dtos);
            }, "api/nsdcategories/activeorg");
        }

        [Route("activensds")]
        [HttpGet]
        public async Task<IHttpActionResult> ActiveNsds()
        {
            return await LogAction(async () =>
            {
                var nsds = await Uow.NsdCategoryRepo.GetActiveNsds();
                var nsddto = new List<NsdManagementDto>();

                if (nsds.Count > 0)
                {
                    foreach (var nsd in nsds)
                    {
                        nsddto.Add(new NsdManagementDto
                        {
                            Id = nsd.Id,
                            Name = nsd.Name
                        });
                    }
                }

                return Ok(nsddto);
            }, "api/nsdcategories/activensds");
        }

        [Route("removefromallorganizations")]
        [HttpPost]
        public async Task<IHttpActionResult> RemoveFromAllOrganizations([FromBody] NsdManagementDto nsdDto)
        {
            return await LogTransaction(async () =>
            {
                //Uow.BeginTransaction();
                try
                {
                    var nsd = await Uow.NsdCategoryRepo.GetByNsdId(nsdDto.Id);
                    var nsddto = new List<NsdManagementDto>();
                    var success = await Uow.OrganizationNsdRepo.RemoveNsdFromAllOrganizationsByNsdId(nsd.Id);

                    if (success)
                    {
                        //Uow.Commit();

                        Logger.LogInfo(GetType(), $"RemoveFromAllOrganizations(NSD: '{nsdDto.Name}') removed by '{ApplicationContext.GetUserName()}'.");

                        return ApiStepResult.Succeed(Ok(nsdDto));
                    }

                    //Uow.Rollback();
                    return ApiStepResult.Fail(BadRequest());
                }
                catch (Exception)
                {
                    //Uow.Rollback();
                    throw;
                }
            }, "api/nsdcategories/removefromallorganizations");
        }

        [Route("activatensd")]
        [HttpPost]
        public async Task<IHttpActionResult> ActivateNsd([FromBody] NsdManagementDto nsdDto)
        {
            return await LogTransaction(async () =>
            {
                var nsd = await Uow.NsdCategoryRepo.GetByNsdId(nsdDto.Id);
                var nsddto = new List<NsdManagementDto>();

                if (!nsd.Operational)
                {
                    //Uow.BeginTransaction();
                    try
                    {
                        nsd.Operational = true;
                        Uow.NsdCategoryRepo.Update(nsd);

                        await Uow.SaveChangesAsync();
                        //Uow.Commit();

                        Logger.LogInfo(GetType(), $"ActivateNsd(NSD: '{nsd.Name}') activated by '{ApplicationContext.GetUserName()}'.");

                        return ApiStepResult.Succeed(Ok(nsddto));
                    }
                    catch (Exception)
                    {
                        //Uow.Rollback();
                        throw;
                    }
                }
                return ApiStepResult.Fail(BadRequest());
            }, "api/nsdcategories/activatensd");
        }

        [Route("disablensd")]
        [HttpPost]
        public async Task<IHttpActionResult> DisableNsd([FromBody] NsdManagementDto nsdDto)
        {
            return await LogTransaction(async () =>
            {
                var nsd = await Uow.NsdCategoryRepo.GetByNsdId(nsdDto.Id);
                var nsddto = new List<NsdManagementDto>();

                if (nsd.Operational)
                {
                    //Uow.BeginTransaction();
                    try
                    {
                        nsd.Operational = false;
                        Uow.NsdCategoryRepo.Update(nsd);

                        await Uow.SaveChangesAsync();
                        //Uow.Commit();

                        Logger.LogInfo(GetType(), $"DisableNsd(NSD: '{nsd.Name}') disabled by '{ApplicationContext.GetUserName()}'.");

                        return ApiStepResult.Succeed(Ok(nsddto));
                    }
                    catch (Exception)
                    {
                        //Uow.Rollback();
                        throw;
                    }
                }

                return ApiStepResult.Fail(BadRequest());
            }, "api/nsdcategories/disablensd");
        }

        private NodeTree CreateNode(NsdCategory cat)
        {
            return new NodeTree
            {
                Id = cat.Id,
                GroupCategoryId = cat.ParentCategoryId.Value,
                Name = cat.Name,
                Text = cat.Name,
            };
        }
    }
}
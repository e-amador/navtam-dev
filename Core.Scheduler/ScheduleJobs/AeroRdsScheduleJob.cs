﻿using System;
using Quartz;
using NavCanada.Core.Scheduler.Contracts;
using System.Configuration;
using NavCanada.Core.TaskEngine.Mto;
using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Core.Scheduler.ScheduleJobs
{
    public class AeroRdsScheduleJob : ScheduleJob, IJob
    {
        public static string JobIdentity = "AeroRdsScheduleJob";
        public static string TriggerIdentity = "AeroRdsScheduleTrigger";
        public static string ScheduleGroup = "SyncSdoData";

        public AeroRdsScheduleJob() : base()
        {
        }

        //Just for testing purposes
        public AeroRdsScheduleJob(IJobDetail job, ITrigger trigger, IClusterStorage clusterStorage)
            :base(job, trigger, clusterStorage)
        {
        }

        public override IJobDetail Job
        {
            get
            {
                if( _job == null)
                {
                    _job = JobBuilder.Create<AeroRdsScheduleJob>()
                        .WithIdentity(JobIdentity, ScheduleGroup)
                        .Build();
                }
                return _job;
            }
        }

        public override ITrigger Trigger
        {
            get
            {
                if (_trigger == null)
                {
                    var cronSchedule = ConfigurationManager.AppSettings.Get("AeroRdsCronTrigger");

                    _trigger = TriggerBuilder.Create()
                        .WithIdentity(TriggerIdentity, ScheduleGroup)
                        .WithCronSchedule(cronSchedule)
                        .ForJob(JobIdentity, ScheduleGroup)
                        .Build();
                }
                return _trigger;
            }
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var clusterStorage = ClusterStorage;
                var uow = clusterStorage.Uow;
                var teQueue = clusterStorage.Queues.TaskEngineQueue;

                var currentScheduleTime = context.FireTimeUtc.Value.UtcDateTime;

                var entry = uow.ScheduleJobRepo.GetById(JobIdentity);
                if(entry != null )
                {
                    if (!TriggerShouldFire(entry.LastTrigger, context.FireTimeUtc.Value, context.NextFireTimeUtc.Value))
                        return;
                }

                uow.BeginTransaction(System.Data.IsolationLevel.RepeatableRead);
                try
                {
                    //check again inside the transaction in case two thread attempt to enter at the same time...
                    if (TriggerShouldFire(entry.LastTrigger, context.FireTimeUtc.Value, context.NextFireTimeUtc.Value))
                    {
                        if (entry == null)
                        {
                            uow.ScheduleJobRepo.Add(new Domain.Model.Entitities.ScheduleJob
                            {
                                Id = JobIdentity,
                                LastTrigger = currentScheduleTime
                            });
                        }
                        else
                        {
                            entry.LastTrigger = currentScheduleTime;
                            uow.ScheduleJobRepo.Update(entry);
                        }

                        teQueue.Publish(CreateMto(currentScheduleTime));
                        uow.SaveChanges();
                    }
                    uow.Commit();
                }
                catch (Exception)
                {
                    //TODO: Log here..
                    uow.Rollback();
                }
            }
            catch (Exception)
            {
                //TODO: Log and Ignore..
            }
        }

        private MessageTransferObject CreateMto(DateTime current)
        {
            var mto = new MessageTransferObject();

            mto.Add(MessageDefs.UniqueIdentifierKey, Guid.NewGuid().ToString(), DataType.String);
            mto.Add(MessageDefs.MessageTypeKey, (byte)MessageType.ExternalServicesTask, DataType.Byte);
            mto.Add(MessageDefs.MessageIdKey, (byte)MessageTransferObjectId.AeroRdsSync, DataType.Byte);
            mto.Add(MessageDefs.MessageTimeKey, current, DataType.DateTime);

            return mto;
        }
    }
}

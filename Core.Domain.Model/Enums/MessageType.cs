﻿namespace NavCanada.Core.Domain.Model.Enums
{
    //TODO rename this (iroel)
    public enum MessageType : byte
    {
        WorkFlowTask = 0x00,

        ExternalServicesTask = 0x01,

        ScheduleTask = 0x02,
        
        NotificationTask = 0x03,

        DiagnosticTask = 0x90 //    
    }
}

﻿using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.NSDs.CatchAll.V0R1
{
    using System;
    using System.Device.Location;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.Script.Serialization;
    using Core.Common.Common;

    using Code.Extentions;
    using Code.GeoTools;
    using Code.Helpers;
    using Models;
    using Core.Common.Contracts;
    using Core.Data.NotamWiz.Contracts;
    using Core.Domain.Model.Entitities;
    using Newtonsoft.Json;

    public class CatchAllController : NsdBaseController
    {
        public readonly ILog Log = LogManager.GetLogger(typeof(CatchAllController));

        public CatchAllController(IUow uow, IClusterQueues clusterQueues) : base(uow, clusterQueues)
        {
        }

        public override ActionResult Index(string id)
        {
            var proposalDefault = new ViewModel();
            return View(proposalDefault);
        }


        protected override async Task<Tuple<ProcessResult, NotamProposalViewModel>> GenerateAsync(string proposalType, NotamProposalViewModel notamProposal, NotamProposalViewModelBase notamProposalBase)
        {
            var proposalData = notamProposalBase as ViewModel;
            if (proposalData != null)
            {
                var icaOsubject = await Uow.IcaoSubjectRepo.GetByIdAsync(proposalData.ICAOSubjectId);
                var icaOcond = await Uow.IcaoSubjectConditionRepo.GetByIdAsync(proposalData.ICAOSubjectConditionId);

                // TODO : handle cases where notamProposalBase.SubjectLocation is in decimal format
                var coordinates = string.IsNullOrEmpty(proposalData.SubjectLocation) ? string.Format("{0} {1}", GeoDataService.DecimalToDMS(Subject.SubjectGeoRefLat, true), GeoDataService.DecimalToDMS(Subject.SubjectGeoRefLong, false)) : proposalData.SubjectLocation;
                notamProposal.Type = proposalType;
                notamProposal.Code45 = icaOcond.Code;
                notamProposal.ItemE = GetItemEPrefix(Subject) + proposalData.EnglishText_FT.ToUpper();
                notamProposal.ItemEFrench = GetItemEPrefix(Subject) + (proposalData.FrenchText_FT ?? string.Empty).ToUpper();
                notamProposal.Fir = Subject.Fir;
                notamProposal.Coordinates = GeoDataService.DMSToRoundedDM(coordinates);
                notamProposal.IsCatchAll = true;
                notamProposal.ContainFreeText = true;
                if (proposalType != NSDDefinitions.CancelledNotam)
                {
                    notamProposal.Code23 = icaOsubject.Code;
                
                    notamProposal.ItemF = !icaOcond.AllowFgEntry
                        ? string.Empty
                        : proposalData.ItemFSFC
                            ? NSDDefinitions.SFC
                            : proposalData.FMeasurementUnit == enFandGMeasurementUnit.FL
                                ? NSDDefinitions.FL + proposalData.ItemF
                                : proposalData.FMeasurementUnit == enFandGMeasurementUnit.AMSL
                                    ? proposalData.ItemF + NSDDefinitions.AMSL
                                    : proposalData.ItemF + NSDDefinitions.AGL;
                
                    notamProposal.ItemG = !icaOcond.AllowFgEntry
                        ? string.Empty
                        : proposalData.ItemGUNL
                            ? NSDDefinitions.UNL
                            : proposalData.GMeasurementUnit == enFandGMeasurementUnit.FL
                                ? NSDDefinitions.FL + proposalData.ItemG
                                : proposalData.GMeasurementUnit == enFandGMeasurementUnit.AMSL
                                    ? proposalData.ItemG + NSDDefinitions.AMSL
                                    : proposalData.ItemG + NSDDefinitions.AGL;
                
                    var cord = new GeoCoordinate().FromDMS(proposalData.SubjectLocation);
                
                    notamProposal.Series = icaOsubject.AllowSeriesChange 
                        ? proposalData.Series 
                        : await GetNotamProposalSeries(Subject.Designator, icaOcond.Code, cord.Latitude, cord.Longitude);
                
                    notamProposal.Purpose = !icaOcond.AllowPurposeChange
                        ? GetPurpose(icaOcond)
                        : (proposalData.PurposeN ? NSDDefinitions.PurposeN : string.Empty) +
                          (proposalData.PurposeB ? NSDDefinitions.PurposeB : string.Empty) +
                          (proposalData.PurposeO ? NSDDefinitions.PurposeO : string.Empty) +
                          (proposalData.PurposeM ? NSDDefinitions.PurposeM : string.Empty);
                
                    notamProposal.Traffic = proposalData.Traffic;
                    notamProposal.Scope = proposalData.Scope;
                    notamProposal.Lower = proposalData.LowerLimit;
                    notamProposal.Upper = proposalData.UpperLimit;
                    notamProposal.Radius = proposalData.Radius;
                    notamProposal.ItemA = proposalData.ItemA;
                }
            }

            if (proposalType == NSDDefinitions.NewNotam)
                return new Tuple<ProcessResult, NotamProposalViewModel>(new ProcessResult(), notamProposal);
            
            notamProposal.ReferredNumber = notamProposal.SubmittedNotam.Number;
            notamProposal.ReferredSeries = notamProposal.SubmittedNotam.Series;
            notamProposal.ReferredYear = notamProposal.SubmittedNotam.Year;

            return new Tuple<ProcessResult, NotamProposalViewModel>(new ProcessResult(), notamProposal);
        }

        protected override async Task<NotamProposalViewModel> ProcessNewProposalAsync(NotamProposalViewModel notamProposal)
        {
            var validator = new NOTAMValidator(Uow);
            var notam = notamProposal.SubmittedNotam;

            try
            {
                var validationResult = await validator.ValidateAsync(notamProposal.SubmittedNotam);

                if (notamProposal.ModifiedByNof && validationResult.IsValid)
                {
                    var currentVm = new JavaScriptSerializer().Deserialize<ViewModel>(notamProposal.Tokens);
                    var subject = Uow.IcaoSubjectRepo.GetAll().FirstOrDefault(a => a.Code == notam.Code23);
                    var cond = Uow.IcaoSubjectConditionRepo.GetAll().FirstOrDefault(a => a.SubjectId == subject.Id && a.Code == notam.Code45);

                    var vm = new ViewModel
                    {
                        Bilingual = currentVm.Bilingual,
                        EndValidity = DateTime.Parse(notam.EndValidity),
                        EnglishText_FT = notam.ItemE,
                        Estimated = notam.Estimated,
                        FMeasurementUnit = 0,
                        FrenchText_FT = notam.ItemEFrench,
                        GMeasurementUnit = 0,
                        ICAOSubjectConditionId = cond == null ? 0 : cond.Id,
                        ICAOSubjectId = subject == null ? 0 : subject.Id,
                        ItemA = notam.ItemA,
                        ItemD = notam.ItemD == notamProposal.ItemD ? notamProposal.ItemD : DbRes.T("Modified by the NOF. Click 'Add Schedule' to clear Warning.", "NSD-CatchAll"),
                        ItemF = notam.ItemF == null ? null : notam.ItemF == NSDDefinitions.SFC ? 0 : (int?)int.Parse(notam.ItemF),
                        ItemFSFC = (notam.ItemF ?? string.Empty) == NSDDefinitions.SFC,
                        ItemG = notam.ItemG == null ? null : notam.ItemG == NSDDefinitions.UNL ? 0 : (int?)int.Parse(notam.ItemG),
                        ItemGUNL = (notam.ItemG ?? string.Empty) == NSDDefinitions.UNL,
                        LowerLimit = notam.Lower,
                        PurposeB = notam.Purpose.Contains(NSDDefinitions.PurposeB),
                        PurposeM = notam.Purpose.Contains(NSDDefinitions.PurposeM),
                        PurposeN = notam.Purpose.Contains(NSDDefinitions.PurposeN),
                        PurposeO = notam.Purpose.Contains(NSDDefinitions.PurposeO),
                        Radius = notam.Radius,
                        Scope = notam.Scope,
                        Series = notam.Series,
                        SdoEntityId = currentVm.SdoEntityId,
                        StartValidity = notamProposal.Type == "R" ? DateTime.UtcNow : DateTime.Parse(notam.StartValidity),
                        SubjectLocation = currentVm.SubjectLocation,
                        Traffic = notam.Traffic,
                        UpperLimit = notam.Upper,
                        ItemDChangedByNof = notam.ItemD != notamProposal.ItemD
                    };

                    notamProposal.Tokens = JsonConvert.SerializeObject(vm);
                }
            }
            catch (Exception e)
            {
                Log.Error("Error in ProcessNewProposalAsync", e);
                throw;
            }

            notamProposal.Id = 0; // Set Id to 0 so a new Proposal will be added to the package
            return notamProposal;
        }

        protected override async Task<ProcessResult> ValidateAsync(NotamProposalViewModelBase notamProposalBase)
        {
            Log.Object(new
            {
                Method = "ValidateAsync",
                Parameters = new { notamProposalBase }
            });

            try
            {
                var validator = new CatchAllValidator(this, Uow, DbRes);
                var vw = notamProposalBase as ViewModel;
                var validationResult = await validator.ValidateAsync(vw);
                if (!validationResult.IsValid)
                {
                    var result = new ProcessResult
                    {
                        Success = false,
                        ErrorCode = "CAC-0001"
                    };

                    foreach (var failure in validationResult.Errors)
                    {
                        Log.Warn(new
                        {
                            Method = "ValidateAsync",
                            Error = failure.ErrorMessage,
                            Parameters = new { vw }
                        });

                        result.Errors.Add(failure.ErrorMessage);
                    }

                    return result;
                }
            }
            catch (Exception e)
            {
                Log.Error("error in validateAsync CatchAllcontroller", e);
            }

            return new ProcessResult();
        }

        private string GetPurpose(IcaoSubjectCondition cond)
        {
            return (cond.N ? NSDDefinitions.PurposeN : string.Empty) + (cond.B ? NSDDefinitions.PurposeB : string.Empty) + (cond.O ? NSDDefinitions.PurposeO : string.Empty) + (cond.M ? NSDDefinitions.PurposeM : string.Empty);
        }

        private string GetTraffic(IcaoSubjectCondition cond)
        {
            return (cond.I ? NSDDefinitions.TrafficI : " ") + (cond.V ? NSDDefinitions.TrafficV : " ");
        }
    }
}
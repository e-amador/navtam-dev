namespace NavCanada.Applications.NotamWiz.Web
{
    using System;
    using System.Data.Entity;
    using System.Web.Http;
    using System.Web.Mvc;

    using Microsoft.Practices.Unity;

    using NavCanada.Core.Common.Contracts;
    using NavCanada.Core.Data.NotamWiz.Contracts;
    using NavCanada.Core.Data.NotamWiz.EF;
    using NavCanada.Core.Data.NotamWiz.EF.Helpers;
    using NavCanada.Core.SqlServiceBroker;

    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig 
    {
        #region Unity Container
        private static readonly Lazy<IUnityContainer> unity_container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers
            // e.g. container.RegisterType<ITestService, TestService>();

            DependencyResolver.SetResolver(new Microsoft.Practices.Unity.Mvc.UnityDependencyResolver(container));

            GlobalConfiguration.Configuration.DependencyResolver = new Microsoft.Practices.Unity.WebApi.UnityDependencyResolver(container);
        }

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return unity_container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        private static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();


            //Register as a singleton
            container.RegisterInstance(new RepositoryFactories(), new ContainerControlledLifetimeManager());

            //Register as always new instance
            container.RegisterInstance(new RepositoryProvider(container.Resolve<RepositoryFactories>()));
            container.RegisterType<IRepositoryProvider, RepositoryProvider>(new TransientLifetimeManager());

            //container.RegisterInstance(new AppDbContext("DefaultConnection"));
            container.RegisterType<DbContext, AppDbContext>(new InjectionConstructor("DefaultConnection"));
            var uow = new Uow(container.Resolve<AppDbContext>(), container.Resolve<RepositoryProvider>());

            //Register as always a new instance
            container.RegisterInstance(uow);
            container.RegisterType<IUow, Uow>(new TransientLifetimeManager());

            IQueue initiatorToTaskEngineSsbSettings = new SqlServiceBrokerQueue(container.Resolve<AppDbContext>(), SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings);
            IQueue taskEngineToInitiatorSsbSettings = new SqlServiceBrokerQueue(container.Resolve<AppDbContext>(), SqlServiceBrokerQueueSettings.TaskEngineToInitiatorSsbSettings);

            container.RegisterType<IQueue, SqlServiceBrokerQueue>(new TransientLifetimeManager());
            
            IClusterQueues queues = new ClusterQueues(
                    initiatorToTaskEngineSsbSettings, 
                    taskEngineToInitiatorSsbSettings);

            container.RegisterInstance(queues);
            container.RegisterType<IClusterQueues, ClusterQueues>(new TransientLifetimeManager());
        }
    }
}

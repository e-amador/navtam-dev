﻿import { Component, Inject, OnInit, AfterViewInit, OnDestroy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms'

import { Select2OptionData } from 'ng2-select2';
import { WindowRef } from '../../common/windowRef.service';

import { LocaleService, TranslationService } from 'angular-l10n';

import { MemoryStorageService } from '../shared/mem-storage.service'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';

import { DataService } from '../shared/data.service'

import { DoaRegionSource, IDoaRegion, IDoaAdmin, IFIR, ISelectOptions, IRegionMapRender, RegionType } from '../shared/data.model';

declare var $: any;

@Component({
    templateUrl: '/app/admin/doas/doa-new.component.html'
})
export class DoaNewComponent implements OnInit, AfterViewInit {
    model: any = null;
    action: string;

    public doasForm: FormGroup;

    isReadOnly: boolean = false;

    doa: IDoaAdmin;
    isDoaUniqueName: boolean = true;
    isValidating: boolean = false;
    isValidLocation: boolean = true;
    isSubmitting: boolean = false;

    doaSources: ISelectOptions[] = [];
    selectedSourceId?: number = 1;
    doaSourceOptions: Select2Options;

    firSources: ISelectOptions[] = [];
    selectedFirId?: string = '-1';
    firSourceOptions: Select2Options;

    radiusLimit: number = 999;
    leafletmap: any;
    mapHealthy: boolean = false;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private activatedRoute: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService) {

        if (this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);
    }

    ngOnInit() {
        this.action = this.activatedRoute.snapshot.data[0].action;
        if (this.action === 'create') {
            var region: IDoaRegion = {
                fir: "",
                geography: "",
                location: "",
                points: "",
                radius: 5,
                source: DoaRegionSource.None,
            };

            this.doa = {
                id: 0,
                description: "",
                name: "",
                region: region,
                doaType: RegionType.Doa,
                changes: null
            };
        } else if (this.action === 'copy') {
            this.doa = this.activatedRoute.snapshot.data['model'];
        }

        this.model = this.doa;

        //Sources
        this.doaSourceOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('DoasAdminSession.SelectSource')
            },
            width: "100%"
        }
        this.loadDoaSources();
        if (this.action === 'copy') {
            this.selectedSourceId = +this.doaSources.find(x => x.id === String(this.doa.region.source)).id;
        }

        //FIRS
        this.firSourceOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('DoasAdminSession.SelectFir')
            },
            width: "100%"
        }
        this.loadFirSources();

        this.configureReactiveForm();
    }

    ngAfterViewInit() {
        this.mapHealthCheck();
    }

    ngOnDestroy() {
        this.disposeMap();
    }

    loadFirSources() {
        this.dataService.getAllFirs()
            .finally(() => {
                this.selectedFirId = '-1';
            })
            .subscribe((firs: IFIR[]) => {
                this.firSources = firs.map(
                    function (r: any) {
                        return <Select2OptionData>{
                            id: r.id,
                            text: `${r.designator} - ${r.name}`
                        };
                    });
                this.firSources.unshift({ id: "-1", text: "" });
            });
    }

    loadDoaSources() {
        this.doaSources = [
            //{ id: '0', text: this.translation.translate('DoasAdminSession.DoaSourceNone') },
            { id: '1', text: this.translation.translate('DoasAdminSession.DoaSourceGeography') },
            { id: '2', text: this.translation.translate('DoasAdminSession.DoaSourceFir') },
            { id: '3', text: this.translation.translate('DoasAdminSession.DoaSourcePoints') },
            { id: '4', text: this.translation.translate('DoasAdminSession.DoaSourcePointRadius') }
        ];
        this.selectedSourceId = 1;
    }

    public onFirChanged(data: { value: string }) {
        if (data.value) {
            this.doa.region.fir = data.value;
            this.newDoaRegionRenderMap();
        } else { this.doa.region.fir = ''; }
    }

    public onSourceChanged(data: { value: string }) {
        if (data.value) {
            var diss = this.doaSources.find(x => x.id === String(data.value));
            if (diss) {
                this.doa.region.source = +diss.id;
                this.selectedSourceId = +diss.id;
                if (this.doa.region.source === DoaRegionSource.PointAndRadius && this.doa.region.radius === 0) {
                    this.doa.region.radius = 5;
                    const radCtrl = this.$ctrl("doaRadius");
                    radCtrl.setValue(this.doa.region.radius);
                }
            }
            else {
                this.doa.region.source = 1
                this.selectedSourceId = 1;
            }
        } else {
            this.doa.region.source = 1;
            this.selectedSourceId = 1;
        }
    }

    private configureReactiveForm() {
        this.doasForm = this.fb.group({
            doaName: [{ value: this.model.name, disabled: false }, []],
            doaDescription: [{ value: this.model.description, disabled: false }, []],
            doaLocation: [{ value: this.model.region.location, disabled: false }, []],
            doaRadius: [{ value: this.model.region.radius, disabled: false }, []],
            doaPoints: [{ value: this.model.region.points, disabled: false }, []],
            doaGeo: [{ value: this.model.region.geography, disabled: true }, []],
        });
        this.$ctrl("doaRadius").setValidators(radiusValidatorFn(this.radiusLimit));

        this.$ctrl("doaName").valueChanges.debounceTime(1000).subscribe(() => this.validateDoaNameUnique());
        this.$ctrl("doaLocation").valueChanges.debounceTime(1000).subscribe(() => this.validateRegionLocation());
        this.$ctrl("doaPoints").valueChanges.debounceTime(1000).subscribe(() => this.validatePointsLocation());
        this.$ctrl("doaRadius").valueChanges.debounceTime(1000).subscribe(() => this.validateRegionLocation());
    }

    onSubmit() {
        if (this.isSubmitting) return;
        this.isSubmitting = true;
        this.prepareData();
        //Clean the extra info on the data
        var geoReg: IDoaRegion = { fir: '', geography: '', location: '', points: '', radius: 0, source: this.doa.region.source };
        if (this.doa.region.source === DoaRegionSource.Geography) {
            geoReg.geography = this.doa.region.geography;
        } else if (this.doa.region.source === DoaRegionSource.FIR) {
            geoReg.fir = this.doa.region.fir;
        } else if (this.doa.region.source === DoaRegionSource.Points) {
            geoReg.points = this.doa.region.points;
        } else if (this.doa.region.source === DoaRegionSource.PointAndRadius) {
            geoReg.location = this.doa.region.location.toUpperCase();
            geoReg.radius = this.doa.region.radius;
        }
        this.doa.region = geoReg;
        
        this.dataService.validateDoaInCanada(this.doa)
            .subscribe(data => {
                if (data) {
                    this.dataService.saveDoa(this.doa)
                        .subscribe(data => {
                            let msg = this.translation.translate('DoasAdminSession.SuccessDoaCreated');
                            this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                            this.router.navigate(["/administration/doas"]);
                        }, error => {
                            this.isSubmitting = false;
                            let msg = this.translation.translate('DoasAdminSession.FailureDoaCreated');
                            this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
                        });
                } else {
                    this.isSubmitting = false;
                    let msg = this.translation.translate('DoasAdminSession.RegionOutOfCanada');
                    this.toastr.error(msg, "", { 'positionClass': 'toast-bottom-right' });
                }
            }, error => {
                this.isSubmitting = false;
                let msg = this.translation.translate('DoasAdminSession.ErrorRegionOutOfCanada');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    private prepareData() {
        this.doa.id = 0;
        this.doa.name = this.doasForm.get('doaName').value;
        this.doa.description = this.doasForm.get('doaDescription').value;
        if (this.doa.region.source === DoaRegionSource.PointAndRadius) {
            const radCtrl = this.$ctrl("doaRadius");
            this.doa.region.radius = +radCtrl.value;
        } else if (this.doa.region.source === DoaRegionSource.Points) {
            const radCtrl = this.$ctrl("doaRadius");
            this.doa.region.radius = +radCtrl.value;
        }
        this.doa.doaType = RegionType.Doa;
    }

    disableSubmit(): boolean {
        if (this.doasForm.pristine) return true;
        if (this.isValidating) return true;
        return !this.validateForm()
    }

    public saveDoaFile() {
        const winObj = this.winRef.nativeWindow;
        if (winObj["FormData"] !== undefined) {
            const fileUpload = $("#attachments").get(0);
            const files = fileUpload.files;

            // Create FormData object  
            var fileData = new FormData();

            fileData.append(files[0].name, files[0]);

            this.dataService.saveDoaFile(fileData)
                .finally(() => {
                    $("#attachments").val('');
                    if (!/safari/i.test(navigator.userAgent)) {
                        $("#attachments").type = '';
                        $("#attachments").type = 'file';
                    }
                })
                .subscribe(data => {
                    this.doa.region.geography = data;
                    this.doa.region.source = DoaRegionSource.Geography;
                    this.$ctrl('doaGeo').setValue(this.doa.region.geography);

                    const message = this.translation.translate('DoasAdminSession.FileSuccess');
                    this.toastr.success(message, "", { 'positionClass': 'toast-bottom-right' });
                    this.newDoaRegionRenderMap();
                }, error => {
                    this.doa.region.geography = "";
                    this.doa.region.source = DoaRegionSource.None;
                    this.$ctrl('doaGeo').setValue(this.doa.region.geography);
                    this.toastr.error(this.translation.translate('DoasAdminSession.MessageSaveAttachmentError') + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        } else {
            this.doa.region.geography = "";
            this.doa.region.source = DoaRegionSource.None;
            this.toastr.error(this.translation.translate('DoasAdminSession.MessageSaveAttachmentBrowser1'), this.translation.translate('DoasAdminSession.MessageSaveAttachmentBrowser2'), { 'positionClass': 'toast-bottom-right' });
        }
    }

    public backToDoasList() {
        this.router.navigate(['/administration/doas']);
    }

    private validDoaData(): boolean {
        if (this.doa.name.length > 3) { //&& (this.doa.description.length > 0) && (this.doa.region.source > DoaRegionSource.None)) {
            if (this.doa.region.source === DoaRegionSource.Geography && this.doa.region.geography.length > 0)
                return true;
            else if (this.doa.region.source === DoaRegionSource.FIR && this.doa.region.fir !== '-1' && this.doa.region.fir !== '')
                return true;
            else if (this.doa.region.source === DoaRegionSource.PointAndRadius && this.doa.region.location !== '' && isANumber(String(this.doa.region.radius)) && this.doa.region.radius > 0 && this.doa.region.radius <= this.radiusLimit)
                return this.isValidLocation;
            else if (this.doa.region.source === DoaRegionSource.Points && this.doa.region.points !== '')
                return this.isValidLocation;
        }
        return false;
    }

    public validDoaName(): boolean {
        const dName = this.$ctrl('doaName');
        if (dName.value) {
            if (dName.value.length > 0) return true;
        }
        return false;
    }

    public validDoaNameSize(): boolean {
        const dName = this.$ctrl('doaName');
        if (dName.value) {
            if (dName.value.length > 3) return true;
        }
        return false;
    }

    public validateDoaNameUnique() {
        if (this.isValidating) return;
        if (this.validDoaName() && this.validDoaNameSize()) {
            const dName = this.$ctrl('doaName');
            if (dName.value) {
                if (dName.value.length > 3) {
                    this.isValidating = true;
                    this.dataService.existsDoaName(this.doa.id, dName.value)
                        .finally(() => {
                            this.isValidating = false;
                        })
                        .subscribe((resp: boolean) => {
                            this.isDoaUniqueName = !resp;
                        });
                }
            }
        }
    }

    public validateRegionLocation() {
        if (this.isValidating) return;
        const dLocation = this.$ctrl('doaLocation');
        if (dLocation.value) {
            if (dLocation.value.length > 3) {
                this.isValidating = true;
                this.dataService.validateDoaLocation(dLocation.value)
                    .finally(() => {
                        this.isValidating = false;
                    })
                    .subscribe((resp: string) => {
                        this.isValidLocation = true;
                        this.doa.region.location = resp.toUpperCase();
                        this.isValidating = false;
                        this.validateForm();
                        this.newDoaRegionRenderMap();
                    }, error => {
                        this.isValidLocation = false;
                        this.doa.region.location = '';
                    });
            } else this.isValidLocation = false;
        } else this.isValidLocation = false;
    }

    public validatePointsLocation() {
        if (this.isValidating) return;
        const dLocation = this.$ctrl('doaPoints');
        if (dLocation.value) {
            if (dLocation.value.length > 3) {
                this.isValidating = true;
                this.dataService.validateDoaPoints(dLocation.value)
                    .finally(() => {
                        this.isValidating = false;
                    })
                    .subscribe((resp: string) => {
                        this.isValidLocation = true;
                        this.doa.region.points = resp;
                        this.isValidating = false;
                        this.validateForm();
                        this.newDoaRegionRenderMap();
                    }, error => {
                        this.isValidLocation = false;
                        this.doa.region.points = '';
                        this.isValidating = false;
                    });
            } else this.isValidLocation = false;
        } else this.isValidLocation = false;
    }

    public validateRadius(ctrlName: string) {
        if (this.doasForm.pristine) return true;
        const fc = this.doasForm.get(ctrlName);
        if (fc.errors) return false;
        if (fc.value) {
            if (fc.value > this.radiusLimit) return false;
            if (fc.value < 1) return false;
        } else return false;
        return true;
    }

    public validateForm(): boolean {
        if (this.isValidating) return false;
        if (!this.doasForm.pristine) {
            this.prepareData();
            if (this.validDoaData()) {
                return this.isDoaUniqueName;
            } 
        }
        return false; 
    }

    $ctrl(name: string): AbstractControl {
        return this.doasForm.get(name);
    }

    decRadius() {
        if (!this.isReadOnly) {
            const radCtrl = this.$ctrl("doaRadius");
            if (+radCtrl.value > 0) {
                radCtrl.setValue(+radCtrl.value - 1);
                radCtrl.markAsDirty();
            }
        }
    }

    incRadius() {
        if (!this.isReadOnly) {
            const radCtrl = this.$ctrl("doaRadius");
            if (+radCtrl.value < this.radiusLimit) {
                radCtrl.setValue(+radCtrl.value + 1);
                radCtrl.markAsDirty();
            }
        }
    }

    public validateKeyPressNumber(evt: any) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
        }
    }

    //Map functions
    public newDoaRegionRenderMap() {
        let reg: IRegionMapRender = {
            region: this.doa.region,
            changes: []
        };
        this.renderMap(reg);
    }

    public mapAvailable() {
        if (this.leafletmap) {
            return this.leafletmap.isReady();
        }
        return false;
    }

    private initMap() {
        const winObj = this.winRef.nativeWindow;
        this.leafletmap = winObj['app'].leafletmap;

        if (this.leafletmap) {
            this.leafletmap.initMap();
        }
    }

    private mapHealthCheck() {
        this.dataService.getMapHealth().subscribe(response => {
            if (response) {
                this.initMap();
                this.mapHealthy = true;
            }
        }, error => {
            this.mapHealthy = false;
            return false;
        });
    }

    public renderMap(reg: IRegionMapRender) {
        this.dataService.getGeoFeatures(reg)
            .subscribe((resp: string) => {
                let geoFeatures = resp;
                this.renderRegionInMap(geoFeatures, "DoaModified");
            }, error => {
                this.toastr.error(this.translation.translate('DoasAdminSession.RenderMapErrorMsg'), this.translation.translate('DoasAdminSession.RenderMapErrorTitle'), { 'positionClass': 'toast-bottom-right' });
            });
    }

    private renderRegionInMap(geoFeatures: string, name: string) {
        if (geoFeatures !== null) {
            this.leafletmap.addDoa(geoFeatures, name, function () {
                console.log("OK");
            });
        } else {
            this.toastr.error(this.translation.translate('DoasAdminSession.RenderMapErrorMsg'), this.translation.translate('DoasAdminSession.RenderMapErrorTitle'), { 'positionClass': 'toast-bottom-right' });
        }
    }

    private disposeMap() {
        if (this.leafletmap) {
            this.leafletmap.dispose();
        }
    }
    //End Map functions
}

function radiusValidatorFn(maxRadius: number): ValidatorFn {
    return (c: AbstractControl): { [key: string]: any } => {
        if (typeof (c.value) === "string") {
            if (!c.value) {
                return { 'required': true }
            }
            if (c.value === "") {
                return { 'required': true }
            }
        }
        if (isANumber(c.value)) {
            if (+c.value < 1 || +c.value > maxRadius) {
                return { 'invalid': true }
            }
            var v: number = +c.value;
            if (v > maxRadius) {
                return { 'invalid': true }
            }
            return null;
        }
        return { 'invalid': true }
    };
}

function isANumber(str: string): boolean {
    return !/\D/.test(str);
}

﻿namespace RSC.Model
{
    /// <summary>
    /// Distance of snowbank from edge of runway.
    /// </summary>
    public class BankEdge
    {
        /// <summary>
        /// Rwy edge to Snowbank distance in feet.
        /// </summary>
        /// <remarks>Valid value must be in the range (1 - 200).</remarks>
        public int BankEdgeFt { get; set; }
        /// <summary>
        /// The snowbank is located on the runway edge.
        /// </summary>
        public bool OnRwyEdge { get; set; }
        /// <summary>
        /// Rwy edge to Snowbank distance in inches.
        /// </summary>
        /// <remarks>Valid value must be in the range (1 - 100).</remarks>
        public int BankEdgeIns { get; set; }
    }
}
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var angular_l10n_1 = require("angular-l10n");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var toastr_service_1 = require("./../../common/toastr.service");
var windowRef_service_1 = require("../../common/windowRef.service");
var data_service_1 = require("../shared/data.service");
var data_model_1 = require("../shared/data.model");
var NofSubscCreateComponent = /** @class */ (function () {
    function NofSubscCreateComponent(toastr, fb, dataService, memStorageService, router, activatedRoute, winRef, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.winRef = winRef;
        this.locale = locale;
        this.translation = translation;
        this.bilingual = false;
        this.formReady = false;
        this.isValidating = false;
        this.isValidLocation = false;
        this.isClientUniqueName = true;
        this.geoSources = [];
        this.selectedSourceId = 1;
        this.radiusLimit = 999;
        this.upperLimit = 999;
        this.lowerLimit = 998;
        this.leafletmap = null;
        this.mapHealthy = false;
        this.mapLayer = "GeoRegionModified";
    }
    NofSubscCreateComponent.prototype.ngOnInit = function () {
        this.clientTypesOptions = {
            placeholder: {
                id: '-1',
                text: "-- Select Client Type --"
            },
            width: "100%"
        };
        this.seriesOptions = {
            maximumSelectionLength: 19,
            placeholder: {
                id: '-1',
                text: "-- Select Series --"
            },
            multiple: true,
            width: "100%"
        };
        this.itemAOptions = {
            maximumSelectionLength: 15,
            placeholder: {
                id: '-1',
                text: "-- Select ItemA --"
            },
            multiple: true,
            width: "100%"
        };
        //GeoRegion
        this.region = {
            fir: "",
            geography: "",
            location: "",
            points: "",
            radius: 5,
            source: data_model_1.RegionSource.None,
        };
        //Sources
        this.geoSourceOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('NdsClient.SelectSource')
            },
            width: "100%"
        };
        this.loadGeoSources();
        //End GeoRegion
        this.configureReactiveForm();
        this.loadItemsA();
        this.loadClientTypes();
        this.loadSeries();
    };
    NofSubscCreateComponent.prototype.ngAfterViewInit = function () {
        this.mapHealthCheck();
    };
    NofSubscCreateComponent.prototype.ngOnDestroy = function () {
        this.disposeMap();
    };
    //GeoRegion functions
    NofSubscCreateComponent.prototype.loadGeoSources = function () {
        this.geoSources = [
            { id: '1', text: this.translation.translate('NdsClient.GeoSourceGeography') },
            { id: '3', text: this.translation.translate('NdsClient.GeoSourcePoints') },
            { id: '4', text: this.translation.translate('NdsClient.GeoSourcePointRadius') }
        ];
        this.selectedSourceId = 1;
    };
    NofSubscCreateComponent.prototype.onSourceChanged = function (data) {
        if (data.value) {
            var diss = this.geoSources.find(function (x) { return x.id === String(data.value); });
            if (diss) {
                this.region.source = +diss.id;
                this.selectedSourceId = +diss.id;
                if (this.region.source === data_model_1.RegionSource.PointAndRadius && this.region.radius === 0) {
                    this.region.radius = 5;
                    var radCtrl = this.form.get("geoRadius");
                    radCtrl.setValue(this.region.radius);
                }
                this.validateGeoRegion(this.region, true);
            }
            else {
                this.region.source = 1;
                this.selectedSourceId = 1;
            }
        }
        else {
            this.region.source = 1;
            this.selectedSourceId = 1;
        }
    };
    NofSubscCreateComponent.prototype.saveRegionFile = function () {
        var _this = this;
        var winObj = this.winRef.nativeWindow;
        if (winObj["FormData"] !== undefined) {
            var fileUpload = $("#attachments").get(0);
            var files = fileUpload.files;
            // Create FormData object  
            var fileData = new FormData();
            fileData.append(files[0].name, files[0]);
            this.dataService.saveGeoFile(fileData)
                .subscribe(function (data) {
                _this.region.geography = data;
                _this.region.source = data_model_1.RegionSource.Geography;
                var message = _this.translation.translate('NdsClient.FileSuccess');
                _this.toastr.success(message, "", { 'positionClass': 'toast-bottom-right' });
                _this.isValidLocation = true;
                _this.newGeoRegionRenderMap();
            }, function (error) {
                _this.region.geography = "";
                //this.region.source = RegionSource.None;
                _this.isValidLocation = false;
                _this.toastr.error(_this.translation.translate('NdsClient.MessageSaveAttachmentError') + error.message, "", { 'positionClass': 'toast-bottom-right' });
                _this.leafletmap.resetDoaMapView();
            });
        }
        else {
            this.region.geography = "";
            //this.region.source = RegionSource.None;
            this.toastr.error(this.translation.translate('NdsClient.MessageSaveAttachmentBrowser1'), this.translation.translate('NdsClient.MessageSaveAttachmentBrowser2'), { 'positionClass': 'toast-bottom-right' });
            this.leafletmap.resetDoaMapView();
        }
    };
    NofSubscCreateComponent.prototype.decValue = function (fieldName) {
        var value = 0;
        switch (fieldName) {
            case 'geoRadius':
                var radCtrl = this.form.get("geoRadius");
                if (+radCtrl.value > 1) {
                    radCtrl.setValue(+radCtrl.value - 1);
                    this.form.get('geoRadius').updateValueAndValidity();
                    radCtrl.markAsDirty();
                }
                break;
            case 'lowerLimit':
                var lowCtrl = this.form.get('lowerLimit');
                if (+lowCtrl.value > 0) {
                    lowCtrl.setValue(+lowCtrl.value - 1);
                    this.form.get('lowerLimit').updateValueAndValidity();
                    this.form.get('upperLimit').updateValueAndValidity();
                    lowCtrl.markAsDirty();
                }
                break;
            case 'upperLimit':
                var upCtrl = this.form.get('upperLimit');
                if (+upCtrl.value > 1) {
                    upCtrl.setValue(+upCtrl.value - 1);
                    this.form.get('lowerLimit').updateValueAndValidity();
                    this.form.get('upperLimit').updateValueAndValidity();
                    upCtrl.markAsDirty();
                }
                break;
        }
    };
    NofSubscCreateComponent.prototype.incValue = function (fieldName) {
        var value = 0;
        switch (fieldName) {
            case 'geoRadius':
                var radCtrl = this.form.get("geoRadius");
                if (+radCtrl.value < this.radiusLimit) {
                    radCtrl.setValue(+radCtrl.value + 1);
                    this.form.get('geoRadius').updateValueAndValidity();
                    radCtrl.markAsDirty();
                }
                break;
            case 'lowerLimit':
                var lowCtrl = this.form.get('lowerLimit');
                if (+lowCtrl.value < this.upperLimit - 1) {
                    lowCtrl.setValue(+lowCtrl.value + 1);
                    this.form.get('lowerLimit').updateValueAndValidity();
                    this.form.get('upperLimit').updateValueAndValidity();
                    lowCtrl.markAsDirty();
                }
                break;
            case 'upperLimit':
                var upCtrl = this.form.get('upperLimit');
                if (+upCtrl.value < this.upperLimit) {
                    upCtrl.setValue(+upCtrl.value + 1);
                    this.form.get('lowerLimit').updateValueAndValidity();
                    this.form.get('upperLimit').updateValueAndValidity();
                    upCtrl.markAsDirty();
                }
                break;
        }
    };
    NofSubscCreateComponent.prototype.validateKeyPressNumber = function (evt) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.keyCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
        }
    };
    NofSubscCreateComponent.prototype.newGeoRegionRenderMap = function () {
        var reg = {
            region: this.region,
            changes: []
        };
        this.renderMap(reg);
    };
    NofSubscCreateComponent.prototype.validateGeoRegion = function (region, paint) {
        var _this = this;
        var reg = {
            region: region,
            changes: []
        };
        this.dataService.getGeoFeatures(reg)
            .subscribe(function (resp) {
            _this.isValidLocation = true;
            if (paint) {
                _this.newGeoRegionRenderMap();
                _this.refreshMap();
            }
        }, function (error) {
            _this.isValidLocation = false;
            if (paint) {
                _this.leafletmap.resetDoaMapView();
                _this.refreshMap();
            }
        });
    };
    NofSubscCreateComponent.prototype.renderMap = function (reg) {
        var _this = this;
        this.dataService.getGeoFeatures(reg)
            .subscribe(function (resp) {
            var geoFeatures = resp;
            _this.renderRegionInMap(geoFeatures, _this.mapLayer);
        }, function (error) {
            //this.toastr.error(this.translation.translate('NdsClient.RenderMapErrorMsg'), this.translation.translate('NdsClient.RenderMapErrorTitle'), { 'positionClass': 'toast-bottom-right' });
        });
    };
    NofSubscCreateComponent.prototype.renderRegionInMap = function (geoFeatures, name) {
        if (geoFeatures !== null) {
            this.leafletmap.addDoa(geoFeatures, name, function () {
                console.log("OK");
            });
        }
        else {
            //this.toastr.error(this.translation.translate('NdsClient.RenderMapErrorMsg'), this.translation.translate('NdsClient.RenderMapErrorTitle'), { 'positionClass': 'toast-bottom-right' });
        }
    };
    NofSubscCreateComponent.prototype.initMap = function () {
        var winObj = this.winRef.nativeWindow;
        this.leafletmap = winObj['app'].leafletmap;
        if (this.leafletmap) {
            this.leafletmap.initMap();
        }
    };
    NofSubscCreateComponent.prototype.mapHealthCheck = function () {
        var _this = this;
        this.dataService.getMapHealth().subscribe(function (response) {
            if (response) {
                if (!_this.mapHealthy) {
                    _this.initMap();
                    _this.mapHealthy = true;
                }
            }
        }, function (error) {
            _this.mapHealthy = false;
            return false;
        });
    };
    NofSubscCreateComponent.prototype.disposeMap = function () {
        if (this.leafletmap) {
            this.leafletmap.dispose();
        }
    };
    NofSubscCreateComponent.prototype.refreshMap = function () {
        setTimeout((function () {
            this.leafletmap.resizeMapForModal();
        }).bind(this), 500);
    };
    //End GeoRegion functions
    NofSubscCreateComponent.prototype.onClientTypeChanged = function (data) {
        this.selectedClientTypeId = data.value;
        if (data.value) {
            this.form.get('clientType').setValue(data.value);
        }
        else {
            this.form.get('clientTypes').setValue(this.clientTypes[0].id);
        }
        var addressCtrl = this.form.get('address');
        if (data.value !== this.clientTypes[0].id) {
            addressCtrl.clearValidators();
            addressCtrl.setValue("");
            addressCtrl.disable();
        }
        else {
            addressCtrl.setValue("");
            addressCtrl.setValidators([forms_1.Validators.required, forms_1.Validators.minLength(8), forms_1.Validators.maxLength(8), forms_1.Validators.pattern('^[a-zA-Z]+$')]);
            addressCtrl.enable();
        }
        if (this.formReady) {
            this.form.get('series').updateValueAndValidity();
            this.form.get('itemsA').updateValueAndValidity();
            this.form.markAsDirty();
        }
    };
    NofSubscCreateComponent.prototype.onSeriesChanged = function (data) {
        var ctrl = this.form.get('series');
        if (data.value) {
            ctrl.setValue(data.value);
        }
        else {
            ctrl.setValue("-1");
        }
        if (this.formReady) {
            this.form.get('itemsA').updateValueAndValidity();
            this.form.markAsDirty();
        }
    };
    NofSubscCreateComponent.prototype.onItemAChanged = function (data) {
        var ctrl = this.form.get('itemsA');
        if (data.value) {
            if (data.value.length < 20) {
                ctrl.setValue(data.value);
            }
        }
        else {
            ctrl.setValue("-1");
        }
        if (this.formReady) {
            this.form.get('series').updateValueAndValidity();
            this.form.markAsDirty();
        }
    };
    NofSubscCreateComponent.prototype.allSeriesChanged = function (checked) {
        var allSeriesCtrl = this.form.get('allSeries');
        var seriesCtrl = this.form.get('series');
        var itemsACtrl = this.form.get('itemsA');
        var allowQueriesCtrl = this.form.get('allowQueries');
        if (checked) {
            this.selectedSeries = null;
            itemsACtrl.clearValidators();
            seriesCtrl.clearValidators();
            itemsACtrl.setErrors(null);
            seriesCtrl.setErrors(null);
            seriesCtrl.setValue(-1);
            itemsACtrl.setValue(-1);
        }
        else {
            //seriesCtrl.setValidators([dropDownEntryRequiredValidator(allowQueriesCtrl, itemsACtrl, seriesCtrl, allSeriesCtrl)]);
            //itemsACtrl.setValidators([dropDownEntryRequiredValidator(allowQueriesCtrl, itemsACtrl, seriesCtrl, allSeriesCtrl)]);
        }
        if (this.formReady) {
            seriesCtrl.updateValueAndValidity();
            itemsACtrl.updateValueAndValidity();
            this.form.markAsDirty();
        }
    };
    NofSubscCreateComponent.prototype.bilingualChanged = function (checked) {
        var fc = this.form.get('bilingual');
        fc.setValue(checked);
        fc.markAsDirty();
    };
    NofSubscCreateComponent.prototype.allowQueriesChanged = function (checked) {
        var allSeriesCtrl = this.form.get('allSeries');
        var allowQueriesCtrl = this.form.get('allowQueries');
        var seriesCtrl = this.form.get('series');
        var itemsACtrl = this.form.get('itemsA');
        allowQueriesCtrl.setValue(checked);
        if (checked) {
            seriesCtrl.clearValidators();
            itemsACtrl.clearValidators();
            seriesCtrl.setErrors(null);
            itemsACtrl.setErrors(null);
        }
        else {
            //seriesCtrl.setValidators([dropDownEntryRequiredValidator(allowQueriesCtrl, itemsACtrl, seriesCtrl, allSeriesCtrl)]);
            //itemsACtrl.setValidators([dropDownEntryRequiredValidator(allowQueriesCtrl, itemsACtrl, seriesCtrl, allSeriesCtrl)]);
        }
        seriesCtrl.updateValueAndValidity();
        itemsACtrl.updateValueAndValidity();
        allowQueriesCtrl.updateValueAndValidity();
        this.form.markAsDirty();
    };
    NofSubscCreateComponent.prototype.activeSubscriptionChanged = function (checked) {
        var fc = this.form.get('active');
        fc.setValue(checked);
        fc.markAsDirty();
    };
    NofSubscCreateComponent.prototype.validateClient = function () {
        var ctrl = this.form.get("client");
        return ctrl.valid || this.form.pristine;
    };
    NofSubscCreateComponent.prototype.validateAddress = function () {
        if (this.selectedClientTypeId !== "AFTN") {
            return true;
        }
        var ctrl = this.form.get("address");
        return ctrl.valid || this.form.pristine;
    };
    NofSubscCreateComponent.prototype.validateClientType = function () {
        var fc = this.form.get('clientType');
        return fc.value !== "-1" || this.form.pristine;
    };
    //GeoRef validations
    NofSubscCreateComponent.prototype.validGeoData = function () {
        if (this.region.source === data_model_1.RegionSource.Geography && this.region.geography.length > 0)
            return this.isValidLocation;
        else if (this.region.source === data_model_1.RegionSource.PointAndRadius && this.region.location !== '' && isANumber(String(this.region.radius)) && this.region.radius > 0 && this.region.radius <= this.radiusLimit)
            return this.isValidLocation;
        else if (this.region.source === data_model_1.RegionSource.Points && this.region.points !== '')
            return this.isValidLocation;
        return false;
    };
    NofSubscCreateComponent.prototype.validateRegionLocation = function () {
        var _this = this;
        if (this.isValidating)
            return;
        var dLocation = this.form.get('geoLocation');
        if (dLocation.value) {
            if (dLocation.value.length > 3) {
                this.isValidating = true;
                this.dataService.validateGeoLocation(dLocation.value)
                    .finally(function () {
                    _this.isValidating = false;
                })
                    .subscribe(function (resp) {
                    _this.isValidLocation = true;
                    _this.region.location = resp.toUpperCase();
                    if (_this.region.source === data_model_1.RegionSource.PointAndRadius && !_this.form.get('geoRadius').errors) {
                        _this.region.radius = +_this.form.get('geoRadius').value;
                    }
                    _this.isValidating = false;
                    _this.newGeoRegionRenderMap();
                }, function (error) {
                    _this.isValidLocation = false;
                    _this.region.location = '';
                });
            }
            else
                this.isValidLocation = false;
        }
        else
            this.isValidLocation = false;
    };
    NofSubscCreateComponent.prototype.validatePointsLocation = function () {
        var _this = this;
        if (this.isValidating)
            return;
        var dLocation = this.form.get('geoPoints');
        if (dLocation.value) {
            if (dLocation.value.length > 3) {
                this.isValidating = true;
                this.dataService.validateGeoPoints(dLocation.value)
                    .finally(function () {
                    _this.isValidating = false;
                })
                    .subscribe(function (resp) {
                    _this.isValidLocation = true;
                    _this.region.points = resp;
                    _this.isValidating = false;
                    _this.newGeoRegionRenderMap();
                }, function (error) {
                    _this.isValidLocation = false;
                    _this.region.points = '';
                    _this.isValidating = false;
                });
            }
            else
                this.isValidLocation = false;
        }
        else
            this.isValidLocation = false;
    };
    NofSubscCreateComponent.prototype.validateRadius = function () {
        var fc = this.form.get('geoRadius');
        return !fc.errors || this.form.pristine;
    };
    NofSubscCreateComponent.prototype.validateLowerLimit = function () {
        var fc = this.form.get('lowerLimit');
        return !fc.errors || this.form.pristine;
    };
    NofSubscCreateComponent.prototype.validateUpperLimit = function () {
        var fc = this.form.get('upperLimit');
        return !fc.errors || this.form.pristine;
    };
    NofSubscCreateComponent.prototype.isLocationValid = function () {
        if (this.form.get('geoRef').value === false)
            return true;
        else
            return this.validGeoData();
    };
    //End GeoRef validations
    NofSubscCreateComponent.prototype.backToNdsClientList = function () {
        this.router.navigate(['/dashboard']);
    };
    NofSubscCreateComponent.prototype.onSubmit = function () {
        var _this = this;
        var client = this.prepareNdsClient();
        this.dataService.saveNdsClient(client)
            .subscribe(function (data) {
            var msg = _this.translation.translate('NdsClient.SuccessClientCreated');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            _this.memStorageService.save(_this.memStorageService.NSD_CLIENT_ID_KEY, data.id);
            _this.backToNdsClientList();
        }, function (error) {
            var msg = _this.translation.translate('NdsClient.FailureClientCreated');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    NofSubscCreateComponent.prototype.loadItemsA = function () {
        var _this = this;
        this.dataService.getAerodromes()
            .subscribe(function (aerodromes) {
            _this.itemsA = aerodromes.map(function (r) {
                return {
                    id: r.value,
                    text: r.value
                };
            });
            _this.selectedItemsA = ["-1"];
            var self = _this;
            window.setTimeout(function () {
                self.formReady = true;
            }, 500);
        });
    };
    NofSubscCreateComponent.prototype.loadClientTypes = function () {
        this.clientTypes = [
            { id: "AFTN", text: "AFTN" },
            { id: "SWIM", text: "SWIM" },
        ];
        this.selectedClientTypeId = this.clientTypes[0].id;
    };
    NofSubscCreateComponent.prototype.loadSeries = function () {
        this.series = [
            { id: "A", text: "A" },
            { id: "B", text: "B" },
            { id: "C", text: "C" },
            { id: "D", text: "D" },
            { id: "E", text: "E" },
            { id: "F", text: "F" },
            { id: "G", text: "G" },
            { id: "H", text: "H" },
            { id: "I", text: "I" },
            { id: "J", text: "J" },
            { id: "K", text: "K" },
            { id: "L", text: "L" },
            { id: "M", text: "M" },
            { id: "N", text: "N" },
            { id: "O", text: "O" },
            { id: "P", text: "P" },
            { id: "Q", text: "Q" },
            { id: "R", text: "R" },
            { id: "S", text: "S" },
            { id: "T", text: "T" },
            { id: "U", text: "U" },
            { id: "V", text: "V" },
            { id: "W", text: "W" },
            { id: "X", text: "X" },
            { id: "Y", text: "Y" },
            { id: "Z", text: "Z" }
        ];
    };
    NofSubscCreateComponent.prototype.configureReactiveForm = function () {
        var _this = this;
        this.form = this.fb.group({
            client: [{ value: '', disabled: false }, [forms_1.Validators.required, forms_1.Validators.minLength(3), forms_1.Validators.maxLength(100)]],
            address: [{ value: '', disabled: false }, [forms_1.Validators.required, forms_1.Validators.minLength(8), forms_1.Validators.maxLength(8), forms_1.Validators.pattern('^[a-zA-Z]+$')]],
            clientType: [{ value: "AFTN", disabled: false }],
            series: [{ value: "-1", disabled: false }],
            itemsA: [{ value: "-1", disabled: false }],
            allSeries: [{ value: false, disabled: false }],
            bilingual: [{ value: false, disabled: false }],
            allowQueries: [{ value: true, disabled: false }],
            operator: [{ value: this.winRef.appConfig.usrName, disabled: true }],
            active: [{ value: true, disabled: false }],
            geoRef: [{ value: false, disabled: false }],
            geoLocation: [{ value: this.region.location, disabled: false }, []],
            geoRadius: [{ value: this.region.radius, disabled: false }, []],
            geoPoints: [{ value: this.region.points, disabled: false }, []],
            lowerLimit: [{ value: 0, disabled: false }, []],
            upperLimit: [{ value: this.upperLimit, disabled: false }, []],
        });
        this.form.get("client").valueChanges.debounceTime(1000).subscribe(function () { return _this.validateClientNameUnique(); });
        this.form.get("geoRadius").setValidators(radiusValidatorFn(this.radiusLimit));
        this.form.get("geoLocation").valueChanges.debounceTime(1000).subscribe(function () { return _this.validateRegionLocation(); });
        this.form.get("geoPoints").valueChanges.debounceTime(1000).subscribe(function () { return _this.validatePointsLocation(); });
        this.form.get("geoRadius").valueChanges.debounceTime(1000).subscribe(function () { return _this.validateRegionLocation(); });
        this.form.get("lowerLimit").setValidators(lowerLimitValidator(this.form.get('upperLimit'), this.lowerLimit));
        this.form.get("upperLimit").setValidators(upperLimitValidator(this.form.get('lowerLimit'), this.upperLimit));
    };
    NofSubscCreateComponent.prototype.geoRefChanged = function (value) {
        if (value) {
            //this.mapHealthCheck();
            this.form.get("geoRadius").setValidators(radiusValidatorFn(this.radiusLimit));
            this.form.get("lowerLimit").setValidators(lowerLimitValidator(this.form.get('upperLimit'), this.lowerLimit));
            this.form.get("upperLimit").setValidators(upperLimitValidator(this.form.get('lowerLimit'), this.upperLimit));
            if (this.isLocationValid()) {
                this.newGeoRegionRenderMap();
            }
            else {
                this.leafletmap.resetDoaMapView();
            }
            this.refreshMap();
        }
        else {
            this.form.get("geoRadius").clearValidators();
            this.form.get("lowerLimit").clearValidators();
            this.form.get("upperLimit").clearValidators();
        }
    };
    NofSubscCreateComponent.prototype.prepareNdsClient = function () {
        var seriesValue = this.form.get('series').value;
        var itemsAValue = this.form.get('itemsA').value;
        if (typeof (seriesValue) === "object") {
            if (seriesValue.length === 1 && seriesValue[0] == "-1") {
                seriesValue = null;
            }
        }
        else {
            if (seriesValue == "-1") {
                seriesValue = null;
            }
        }
        if (typeof (itemsAValue) === "object") {
            if (itemsAValue.length === 1 && itemsAValue[0] == "-1") {
                itemsAValue = null;
            }
        }
        else {
            if (itemsAValue == "-1") {
                itemsAValue = null;
            }
        }
        var client = {
            client: this.form.get('client').value,
            clientType: this.form.get('clientType').value,
            address: this.form.get('address').value,
            series: this.form.get('allSeries').value ? this.getAllSeries() : seriesValue,
            itemsA: itemsAValue,
            allowQueries: this.form.get('allowQueries').value,
            french: this.form.get('bilingual').value,
            active: this.form.get('active').value,
            isRegionSubscription: this.form.get('geoRef').value,
            region: null,
            lowerLimit: 0,
            upperLimit: 0,
        };
        if (client.isRegionSubscription) {
            //Clean the extra info on the data
            var geoRegion = { fir: '', geography: '', location: '', points: '', radius: 0, source: this.region.source };
            if (this.region.source === data_model_1.RegionSource.Geography) {
                geoRegion.geography = this.region.geography;
            }
            else if (this.region.source === data_model_1.RegionSource.FIR) {
                geoRegion.fir = this.region.fir;
            }
            else if (this.region.source === data_model_1.RegionSource.Points) {
                geoRegion.points = this.region.points;
            }
            else if (this.region.source === data_model_1.RegionSource.PointAndRadius) {
                geoRegion.location = this.region.location.toUpperCase();
                geoRegion.radius = this.region.radius;
            }
            this.region = geoRegion;
            client.region = this.region;
            client.lowerLimit = +this.form.get('lowerLimit').value;
            client.upperLimit = +this.form.get('upperLimit').value;
        }
        if (client.address) {
            client.address = client.address.toUpperCase();
        }
        return client;
    };
    NofSubscCreateComponent.prototype.validateClientNameUnique = function () {
        var _this = this;
        if (this.isValidating)
            return;
        if (this.validateClient()) {
            var dName = this.form.get('client');
            if (dName.value) {
                if (dName.value.length > 3) {
                    this.isValidating = true;
                    this.dataService.existsClientName(-1, dName.value)
                        .subscribe(function (resp) {
                        _this.isClientUniqueName = !resp;
                        _this.isValidating = false;
                    });
                }
            }
        }
    };
    NofSubscCreateComponent.prototype.getAllSeries = function () {
        return this.series.map(function (series) {
            return series.id;
        });
    };
    NofSubscCreateComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/dashboard/nof/nof-subsc-create.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            router_1.ActivatedRoute,
            windowRef_service_1.WindowRef,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], NofSubscCreateComponent);
    return NofSubscCreateComponent;
}());
exports.NofSubscCreateComponent = NofSubscCreateComponent;
function dropDownEntryRequiredValidator(allowQueryCtrl, seriesCtrl, itemACtrl, allSeriesCtrl) {
    return function (c) {
        if (!allowQueryCtrl.value) {
            if (allSeriesCtrl.value) {
                return null;
            }
            var serieCtrlValue = seriesCtrl.value === null ? "-1" : seriesCtrl.value;
            var itemACtrlValue = itemACtrl.value === null ? "-1" : itemACtrl.value;
            serieCtrlValue = typeof (serieCtrlValue) === "object" ? serieCtrlValue[0] : serieCtrlValue;
            itemACtrlValue = typeof (itemACtrlValue) === "object" ? itemACtrlValue[0] : itemACtrlValue;
            if ((!serieCtrlValue || serieCtrlValue == "-1") && (!itemACtrlValue || itemACtrlValue == "-1")) {
                return { 'required': true };
            }
        }
        return null;
    };
}
function radiusValidatorFn(maxRadius) {
    return function (c) {
        if (typeof (c.value) === "string") {
            if (!c.value) {
                return { 'required': true };
            }
            if (c.value === "") {
                return { 'required': true };
            }
        }
        if (isANumber(c.value)) {
            if (+c.value < 1 || +c.value > maxRadius) {
                return { 'invalid': true };
            }
            var v = +c.value;
            if (v > maxRadius) {
                return { 'invalid': true };
            }
            return null;
        }
        return { 'invalid': true };
    };
}
function lowerLimitValidator(upperLimitCtrl, lowerMaxValue) {
    return function (control) {
        if (!isANumber(control.value)) {
            return { 'lowerLimitNonNumeric': true };
        }
        var v1 = +control.value;
        if (v1 < 0) {
            return { 'lowerLimitLowerThanZero': true };
        }
        if (v1 > lowerMaxValue) {
            return { 'lowerLimitHigherThanMax': true };
        }
        if (!isANumber(upperLimitCtrl.value)) {
            return { 'upperLimitNonNumeric': true };
        }
        var v2 = +upperLimitCtrl.value;
        if (v1 === 0 && v2 === 0)
            return null;
        return v1 >= v2 ? { "invalidLowerLimitRange": true } : null;
    };
}
function upperLimitValidator(lowerLimitCtrl, upperMaxValue) {
    return function (control) {
        if (!isANumber(control.value)) {
            return { 'upperLimitNonNumeric': true };
        }
        var v1 = +control.value;
        if (v1 < 0) {
            return { 'upperLimitLowerThanZero': true };
        }
        if (v1 > upperMaxValue) {
            return { 'upperLimitHigherThanMax': true };
        }
        if (!isANumber(lowerLimitCtrl.value)) {
            return { 'lowerLimitNonNumeric': true };
        }
        var v2 = +lowerLimitCtrl.value;
        return v1 <= v2 ? { "invalidUpperLimitRange": true } : null;
    };
}
function isANumber(str) {
    return !/\D/.test(str);
}
//# sourceMappingURL=nof-subsc-create.component.js.map
﻿using SolaceSystems.Solclient.Messaging;

namespace NSD.SolaceBus.Configuration
{
    public static class SessionUtils
    {
        public static SessionProperties NewSessionPropertiesFromConfig(SessionConfiguration sc)
        {
            SessionProperties sessionProps = new SessionProperties();
            // Message backbone IP
            sessionProps.Host = sc.IpPort.ip;
            // User name
            if (sc.RouterUserVpn != null)
            {
                sessionProps.UserName = sc.RouterUserVpn.user;
                if (sc.RouterUserVpn.vpn != null)
                {
                    sessionProps.VPNName = sc.RouterUserVpn.vpn;  // applies to SolOS-TR only
                }
            }
            // Turn ReapplySubscriptions on to enable api-based subscription management
            sessionProps.ReapplySubscriptions = true;

            sessionProps.Password = sc.UserPassword;
            if (sc.Compression)
            {
                //
                // Compression is set as a number from 0-9. 0 means
                // "disable compression" (the default), and 9 means maximum compression.
                // Selecting a non-zero compression level auto-selects the
                // compressed SMF port on the appliance, as long as no SMF port is
                // explicitly specified.

                sessionProps.CompressionLevel = 9;
            }
            // To enable session reconnect
            sessionProps.ReconnectRetries = 100; // retry 100 times
            sessionProps.ReconnectRetriesWaitInMsecs = 3000; // 3 seconds

            //SSL properties.
            sessionProps.SSLTrustStoreDir = sc.trustStoreDirectory;
            sessionProps.SSLTrustedCommonNameList = sc.commonNames;
            sessionProps.SSLCipherSuites = sc.cipherSuites;
            sessionProps.SSLExcludedProtocols = sc.excludedProtocols;
            sessionProps.SSLValidateCertificate = sc.validateCertificate;
            sessionProps.SSLValidateCertificateDate = sc.validateCertificateDate;
            sessionProps.AuthenticationScheme = sc.authenticationScheme;
            sessionProps.SSLClientPrivateKeyFile = sc.clientPrivateKeyFile;
            sessionProps.SSLClientPrivateKeyFilePassword = sc.clientPrivateKeyPassword;
            sessionProps.SSLClientCertificateFile = sc.clientCertificateFile;
            sessionProps.SSLConnectionDowngradeTo = sc.sslConnectionDowngradeTo;

            // Uncomment the following statement to enable automatic timestamp generation on sent messages.
            // When enabled, a send timestamp is automatically generated as a message property for each message sent.  
            // This adds a binary meta part to the message which can reduce performance.
            // sessionProps.GenerateSendTimestamps = true;

            // Uncomment the following statement to enable automatic sequence number generation on sent messages.
            // When enabled, a sequence number is automatically included in the Solace-defined fields for each message sent.   
            // This adds a binary meta part to the message which can reduce performance.
            // sessionProps.GenerateSequenceNumber = true;

            // Uncomment the following statement to enable the inclusion of senderId on sent messages.
            // When enabled, a sender ID is automatically included in the Solace-defined fields for each message sent.    
            // This adds a binary meta part to the message which can reduce performance.
            // sessionProps.IncludeSenderId = true;

            return sessionProps;
        }

    }
}

﻿namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    using System.Linq;

    using Domain.Model.Entitities;
    using System.Threading.Tasks;
    using Common.Common;
    using System.Collections.Generic;

    public interface IConfigurationValueRepo : IRepository<ConfigurationValue>
    {
        ConfigurationValue GetByName(string name);

        Task<ConfigurationValue> GetByNameAsync(string name);

        IQueryable<ConfigurationValue> GetByCategory(string category);

        ConfigurationValue GetByCategoryandName(string category, string name);

        Task<ConfigurationValue> GetByCategoryandNameAsync(string category, string name);

        string GetValue(string category, string name);
        Task<string> GetValueAsync(string category, string name);

        Task<int> GetAllCountAsync(QueryOptions queryOptions);

        Task<List<ConfigurationValue>> GetConfigValuesPartials(QueryOptions queryOptions);

        Task<bool> Exists(string name);
    }
}

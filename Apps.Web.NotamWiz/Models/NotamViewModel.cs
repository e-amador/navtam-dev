﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    public class NotamViewModel
    {
        public long Id { get; set; }

        public string Nof { get; set; }
        public string Series { get; set; }
        public int Number { get; set; }
        public int Year { get; set; }
        public string Type { get; set; }
        public string ReferredSeries { get; set; }
        public int ReferredNumber { get; set; }
        public int ReferredYear { get; set; }
        public string Fir { get; set; }
        public string Code23 { get; set; }
        public string Code45 { get; set; }
        public string Traffic { get; set; }
        public string Purpose { get; set; }
        public string Scope { get; set; }
        public int Lower { get; set; }
        public int Upper { get; set; }
        public string Coordinates { get; set; }
        public int Radius { get; set; }
        public string ItemA { get; set; }
        public string StartValidity { get; set; }
        public string EndValidity { get; set; }
        public bool Estimated { get; set; }
        public string ItemD { get; set; }
        public string ItemE { get; set; }
        public string ItemEFrench { get; set; }
        public string ItemF { get; set; }
        public string ItemG { get; set; }
        public string ItemX { get; set; }
        public string Operator { get; set; }
    }
}

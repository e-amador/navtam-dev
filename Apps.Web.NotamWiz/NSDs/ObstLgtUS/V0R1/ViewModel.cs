﻿namespace NavCanada.Applications.NotamWiz.Web.NSDs.ObstLgtUS.V0R1
{
    public class ViewModel : NotamProposalViewModelBase
    {
        public int Elevation
        {
            get;
            set;
        }

        public int Height
        {
            get;
            set;
        }

        public string Location
        {
            get;
            set;
        }

        public string ObstacleType
        {
            get;
            set;
        }
    }
}
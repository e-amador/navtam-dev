﻿namespace NavCanada.Utils.Apps.InoMock
{
    using AutoMapper;

    using NavCanada.Utils.Apps.InoMock.Extentions;
    using NavCanada.Utils.Apps.InoMock.Models;

    public class AutoMapperConfig
    {
        public static void RegisterMaps()
        {
            Mapper.CreateMap<Notam, NotamProposal>().IgnoreAllNonExisting();
            Mapper.CreateMap<NotamProposal, Notam>()
                .IgnoreAllNonExisting();

            

        }
    }
 



}
﻿import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'

import { DataService } from '../shared/data.service'

import { NsdFormComponent } from '../nsds/nsd-form.component'

@Component({ 
    templateUrl: '/app/dashboard/user/user-create.component.html'
})
export class UserCreateComponent implements OnInit, OnDestroy  {
    
    model : any = null;
    nsdForm : NsdFormComponent;

    @ViewChild(NsdFormComponent)
    set form(v: NsdFormComponent){
        this.nsdForm = v;
    }

    constructor(
        private dataService : DataService, 
        private activatedRoute: ActivatedRoute, 
        private router: Router) {
    }

    ngOnInit() {
        window['loadLeafletMapScript'].call(this, null);
        this.model = this.activatedRoute.snapshot.data['model'];
    }

    ngOnDestroy() {
        if (window["app"].leafletmap) {
            window["app"].leafletmap.dispose();
        }
    }

    saveDraft(formValues) {
/*
        this.setPayload();
        debugger;    
        if( this.nsdForm.valid ){  
            console.log('valid');
        }
        console.log('invalid');
*/
    }

    get langCulture() {
        return (window["app"].cultureInfo || "en").toUpperCase();
    }
}
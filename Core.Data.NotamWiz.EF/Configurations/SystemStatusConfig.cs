﻿namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    using System.Data.Entity.ModelConfiguration;
    using Domain.Model.Entitities;

    public class SystemStatusConfig : EntityTypeConfiguration<SystemStatus>
    {
        public SystemStatusConfig()
        {
            HasKey(e => e.Id);
        }
    }
}

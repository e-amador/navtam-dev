﻿using System;

namespace NavCanada.Core.Common.Contracts
{
    public interface ILogger
    {
        bool CheckLogStatus();
        string CheckLogLevel();
        void UserLogin(string userName, string userId);
        void InvalidLogin(Type type, string username);
        void UserLogout(string Name, string UserId);
        void LogVerbose(string Message);
        void LogDebug(Type type, string Message);
        void LogInfo(Type type, string Message);
        void LogWarn(Type Type, string Message, object obj = null);
        void LogError(Exception Exception, Type Type, string Message = null, object obj = null);        
        void LogFatal(Exception ex, Type type, string Message = null);        
    }
}
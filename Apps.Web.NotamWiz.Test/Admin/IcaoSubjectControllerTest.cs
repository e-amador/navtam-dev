﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Moq;
using NavCanada.Applications.NotamWiz.Web.Areas.Admin.Controllers;
using NavCanada.Applications.NotamWiz.Web.Code.Configurations;
using NavCanada.Applications.NotamWiz.Web.Code.Helpers;
using NavCanada.Applications.NotamWiz.Web.Models;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Domain.Model.Entitities;

namespace Apps.Web.NotamWiz.Test.Admin
{
    [TestClass]
    public class IcaoSubjectControllerTest
    {

        #region Initialize

        private HttpContextBase _httpContext;
        private IcaoSubjectController _controller;

        private Mock<IUow> _uowMock { get; set; }
        private Mock<IClusterQueues> _clusterQueuesMock { get; set; }

        private Mock<ControllerContext> _controllerContextMock;
        private Mock<IIcaoSubjectConditionRepo> _conditionRepoMock;
        private Mock<IIcaoSubjectRepo> _icaoSubjectRepoMock;
        

        private List<IcaoSubjectConditionViewModel> _validModels;
        Random rng = new Random();

       


        [TestInitialize]
        public void InitializeTest()
        {
            AutoMapperConfig.RegisterMaps();

            _validModels = CreateTestModels(5);

            #region DAL Initialize
        
            _uowMock = new Mock<IUow>();
            _clusterQueuesMock = new Mock<IClusterQueues>();
            _conditionRepoMock = new Mock<IIcaoSubjectConditionRepo>();
            _icaoSubjectRepoMock = new Mock<IIcaoSubjectRepo>();

            var icaoSubject = new IcaoSubject 
            {
                Name = "test obj",
                Code = "ls",
                AllowItemAChange = false,
                AllowScopeChange = false,
                AllowSeriesChange = true,
                EntityCode = "sss",
                Id = 1,
                Scope = "ddd",
                NameFrench = "je suis francais nomme"
            };

            _icaoSubjectRepoMock.Setup(x => x.GetByIdAsync(It.IsAny<long>())).ReturnsAsync(icaoSubject); 

            var dbresMock = new Mock<IDbResWrapper>();

            _uowMock.Setup(e => e.IcaoSubjectConditionRepo).Returns(_conditionRepoMock.Object);
            _uowMock.Setup(f => f.IcaoSubjectRepo).Returns(_icaoSubjectRepoMock.Object);
            #endregion

            #region Controller setup

            _controllerContextMock = new Mock<ControllerContext>();
            _httpContext = MvcMockHelpers.MockHttpContext("~/Admin/");
            _controllerContextMock.SetupGet(p => p.HttpContext).Returns(_httpContext);
            dbresMock.Setup(d => d.T(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns<string, string, string>((resId, resourceSet, lang) => resId);

            _controller = new IcaoSubjectController(_uowMock.Object, _clusterQueuesMock.Object)
            {
                ControllerContext = _controllerContextMock.Object,
                DbRes = dbresMock.Object
            };

            #endregion
        }

        #endregion

        #region Tests

        [TestMethod]
        public async void IcaoSubjectController_ConditionsCreate_ShouldCreateIfModelValid()
        {
            //valid model
            var model = _validModels.FirstOrDefault(x => x.SubjectId != 0);
            _validModels.Remove(model);

            Debug.Assert(model != null, "model != null");
            var result = await _controller.Conditions_Create((int) model.SubjectId, new DataSourceRequest(), model);
            
            Assert.IsNotNull(result);
            _uowMock.Verify(x => x.SaveChangesAsync(), Times.AtLeastOnce);
        }

        [TestMethod]
        public async void IcaoSubjectController_ConditionsCreate_ShouldNotCreateIfParentIdIsMissing()
        {
            var model = _validModels.FirstOrDefault(x => x.SubjectId != 0);
            _validModels.Remove(model);

            Debug.Assert(model != null, "model != null");
            model.SubjectId = 0;

            var result = await _controller.Conditions_Create((int) model.SubjectId, new DataSourceRequest(), model);

            Assert.IsNotNull(result);
            _uowMock.Verify(x => x.SaveChangesAsync(), Times.Never);
        }
        #endregion

        #region Helpers
        private List<IcaoSubjectConditionViewModel> CreateTestModels(int count)
        {
            List<IcaoSubjectConditionViewModel> iscvm = new List<IcaoSubjectConditionViewModel>();
            while (count > iscvm.Count)
            {
                var model = new IcaoSubjectConditionViewModel();

                foreach (var pi in model.GetType().GetProperties())
                {
                    switch (pi.PropertyType.Name)
                    {
                        case "Boolean":
                            pi.SetValue(model, (rng.NextDouble() >= 0.5));
                            break;
                        case "String":
                            pi.SetValue(model, RandomString(pi));
                            break;
                        case "Int64":
                            if(pi.Name == "Id")
                                break;
                            if (pi.Name == "SubjectId")
                            {
                                pi.SetValue(model, 1);
                                break;
                            }
                            pi.SetValue(model, rng.Next(0, 999));
                            break;
                        case "Int32":
                            pi.SetValue(model, rng.Next(0, 999));
                            break;
                        default:
                            pi.SetValue(model, null);
                            break;
                    }
                }
                iscvm.Add(model);
            }
            return iscvm;
        }
        private string RandomString(PropertyInfo pi)
        {
            int length = 0;
            switch (pi.Name)
            {
                case "Description":
                    length = rng.Next(1, 99);
                    break;
                case "DescriptionFrench":
                    length = rng.Next(1, 99);
                    break;
                case "Code":
                    length = 2;
                    break;
                case "Purpose":
                    var purpose = "NBOM";
                    length = 3;
                    return new string(Enumerable.Repeat(purpose, length).Select(s => s[rng.Next(s.Length)]).ToArray());
                case "Traffic":
                    length = rng.Next(1, 3);
                    break;
            }

            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ";
            return new string(Enumerable.Repeat(chars, length).Select(s => s[rng.Next(s.Length)]).ToArray());
        }
        #endregion
    }
}

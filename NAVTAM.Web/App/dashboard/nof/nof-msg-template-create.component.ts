﻿import { Component, Inject, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms'

import { LocaleService, TranslationService } from 'angular-l10n';

import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';

import { DataService } from '../shared/data.service'

import { IMessageTemplate } from '../shared/data.model';

@Component({
    templateUrl: '/app/dashboard/nof/nof-msg-template-create.component.html'
})
export class NofMsgTemplateCreateComponent implements OnInit {

    form: FormGroup;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService) {
    }

    ngOnInit() {
        this.configureReactiveForm();
    }

    validateName() {
        const ctrl = this.form.get("name");
        return ctrl.valid || this.form.pristine;
    }

    validateAddress() {
        const ctrl = this.form.get("address");
        return ctrl.valid || this.form.pristine;
    }

    validateBody() {
        const ctrl = this.form.get("body");
        return ctrl.valid || this.form.pristine;
    }


    backToMessageTemplates() {
        this.router.navigate(['/dashboard/messages/templates']);
    }

    onSubmit() {
        const template = this.prepareTemplate();
        this.dataService.saveMessageTemplate(template)
            .subscribe(data => {
                //let msg = this.translation.translate('NdsClient.SuccessClientCreated');
                let msg = "Message template successfully saved";
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                this.backToMessageTemplates();
            }, error => {
                //let msg = this.translation.translate('NdsClient.FailureClientCreated');
                let msg = "An error has ocurred: ";
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    private prepareTemplate(): IMessageTemplate {
        let name = this.form.get('name').value;
        let address = this.form.get('address').value.toUpperCase();
        let body = this.form.get('body').value;

        const template: IMessageTemplate = {
            name: name,
            addresses: address,
            body: body
        }

        return template;
    }


    private configureReactiveForm() {
        this.form = this.fb.group({
            name: [{ value: '', disabled: false }, [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
            address: [{ value: '', disabled: false }, [Validators.required, Validators.pattern('^[a-zA-Z]{8}(?:;[a-zA-Z]{8})*$')]],
            body: [{ value: '', disabled: false }, [Validators.required]],
        });
    }
}


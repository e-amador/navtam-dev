﻿import { Component, Inject, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms'
import { WindowRef } from '../../common/windowRef.service'
import { Select2OptionData } from 'ng2-select2';
import { LocaleService, TranslationService } from 'angular-l10n';
import { MemoryStorageService } from '../shared/mem-storage.service'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';
import { DataService } from '../shared/data.service'
import { ISeriesPartial, IGeoRegionPartial, ISelectOptions } from '../shared/data.model';

@Component({
    templateUrl: '/app/admin/series/series-edit.component.html'
})
export class SeriesEditComponent implements OnInit {

    model: any = null;
    action: string;

    seriesForm: FormGroup;

    subjects: ISelectOptions[] = [];
    selectedSubject?: number;
    subjectOptions: Select2Options;

    geoRegions: ISelectOptions[] = [];
    selectedRegionId?: string = '-1';
    geoRegionOptions: Select2Options;

    disseminationCategories: ISelectOptions[] = [];
    selectedDisseminationCategory?: string = '-1';
    disseminationCategoriesOptions: Select2Options;

    codes: Select2OptionData[] = [];
    codeOptions: Select2Options;
    selectedCodeId?: string = '-1';

    isReadOnly: boolean = false;
    readOnlyQCode: boolean = false;
    isSubmitting: boolean = false;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private activatedRoute: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService) {

        if (this.winRef.appConfig.ccs)
            this.router.navigate(['/administration']);
    }

    ngOnInit() {
        this.subjectOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('SeriesAllocationSession.SelectRegion')
            },
            width: "100%"
        }

        this.geoRegionOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('SeriesAllocationSession.SelectRegion')
            },
            width: "100%"
        }
        this.disseminationCategoriesOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('SeriesAllocationSession.SelectDissCat')
            },
            width: "100%"
        }
        this.codeOptions = {
            placeholder: {
                id: "-1",
                text: this.translation.translate('SeriesAllocationSession.SelectQCode')
            },
            width: "100%"
        }

        this.action = this.activatedRoute.snapshot.data[0].action;
        this.model = this.activatedRoute.snapshot.data['model'];
        this.isReadOnly = this.action === "view";

        this.loadSubjects();
        this.loadRegions();
        this.loadDissCategory();
        this.loadQCodes();

        this.configureReactiveForm();
    }

    backToSeriesList() {
        this.router.navigate(['/administration/series']);
    }

    disableSubmit(): boolean {
        if (this.seriesForm.pristine) return true;
        //if (this.seriesForm.untouched) return true;
        if (!this.seriesForm.valid) return true;
        if (this.isSubmitting) return true;
        return false;
    }

    seriesNameKeyUp(): void {
        //this.seriesForm.get('series').;
    }

    onSubmit() {
        if (this.isSubmitting) return;
        this.isSubmitting = true;
        var serieToSave = this.prepareSaveSerie();
        this.dataService.updateSeries(serieToSave)
            .subscribe(data => {
                let msg = this.translation.translate('SeriesAllocationSession.SuccessSerieUpdated');
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                this.router.navigate(["/administration/series"]);
            }, error => {
                this.isSubmitting = false;
                let msg = this.translation.translate('SeriesAllocationSession.FailureSerieUpdated');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });

    }

    private configureReactiveForm() {
        this.seriesForm = this.fb.group({
            subject: [{ value: this.model.subject, disabled: false }, [validateSubject]],
            regionId: [{ value: String(this.model.regionId), disabled: this.isReadOnly }, [validateRegionId]],
            series: [{ value: this.model.series, disabled: this.isReadOnly }, [validateSeries]],
            disseminationCategory: [{ value: String(this.model.disseminationCategory), disabled: this.isReadOnly }, [validateDisseminationCategory]],
            qCode: [{ value: this.model.qCode, disabled: this.isReadOnly }, [validateQCode]],
            disseminationCategoryName: [{ value: this.model.disseminationCategoryName, disabled: this.isReadOnly }, [Validators.required]],
            regionName: [{ value: this.model.regionName, disabled: this.isReadOnly }, [Validators.required]],
        });
    }

    private prepareSaveSerie(): ISeriesPartial {
        const serie: ISeriesPartial = {
            id: this.model.id,
            subject: this.seriesForm.get('subject').value.toString(),
            regionName: this.seriesForm.get('regionName').value.toUpperCase(),
            regionId: this.seriesForm.get('regionId').value,
            series: this.seriesForm.get('series').value.toUpperCase(),
            disseminationCategory: this.seriesForm.get('disseminationCategory').value,
            disseminationCategoryName: this.seriesForm.get('disseminationCategoryName').value.toUpperCase(),
            qCode: this.seriesForm.get('qCode').value.toUpperCase(),
        }
        return serie;
    }

    public validateSeriesName() {
        const fc = this.seriesForm.get("series");
        if (validateSeries(fc) !== null) return false;
        return true;
    }

    public validateSubject() {
        const fc = this.seriesForm.get('subject');
        if (validateSubject(fc) !== null) return false;
        return true;
    }

    public validateRegion() {
        const fc = this.seriesForm.get('regionId');
        if (validateRegionId(fc) !== null) return false;
        return true;
    }

    public validateQCode() {
        const fc = this.seriesForm.get('qCode');
        if (validateQCode(fc) !== null) return false;
        return true;
    }

    public validateDissemination() {
        const fc = this.seriesForm.get('disseminationCategory');
        if (validateDisseminationCategory(fc) !== null) return false;
        return true;
    }

    public onDisseminationChanged(data: { value: string }) {
        if (data.value) {
            var diss = this.disseminationCategories.find(x => x.id === String(data.value));
            if (diss) {
                this.seriesForm.get('disseminationCategory').setValue(diss.id);
                this.seriesForm.get('disseminationCategoryName').setValue(diss.text);
            } else {
                this.seriesForm.get('disseminationCategory').setValue('-1');
                this.seriesForm.get('disseminationCategoryName').setValue('');
            }
        } else {
            this.seriesForm.get('disseminationCategory').setValue('-1');
            this.seriesForm.get('disseminationCategoryName').setValue('');
        }
        if (data.value !== '-1' && this.selectedDisseminationCategory !== this.seriesForm.get('disseminationCategory').value) {
            this.seriesForm.markAsDirty();
        }
    }

    public onSubjectChanged(data: { value: string }) {
        if (data.value) {
            this.seriesForm.get('subject').setValue(data.value);
            if (data.value === '2') {
                var code = this.codes.find(c => c.text === 'OB');
                if (code) {
                    this.onSelectedCodeChanged({ value: code.id })
                    this.readOnlyQCode = true;
                }
            } else {
                this.readOnlyQCode = false;
            }
        }
        if (data.value !== '-1' && this.selectedSubject !== this.seriesForm.get('subject').value) {
            this.seriesForm.markAsDirty();
        }
    }

    public onRegionChanged(data: { value: string }) {
        if (data.value) {
            var reg = this.geoRegions.find(x => x.id === String(data.value));
            if (reg) {
                this.seriesForm.get('regionId').setValue(reg.id);
                this.seriesForm.get('regionName').setValue(reg.text);
            } else {
                this.seriesForm.get('regionId').setValue('-1');
                this.seriesForm.get('regionName').setValue('');
            }
        } else {
            this.seriesForm.get('regionId').setValue('-1');
            this.seriesForm.get('regionName').setValue('');
        }
        if( data.value !== '-1' && this.selectedRegionId !== this.seriesForm.get('regionId').value) {
            this.seriesForm.markAsDirty();
        }
    }

    public onSelectedCodeChanged(data: { value: string }) {
        if (data.value !== "-1") {
            this.selectedCodeId = data.value;
            this.seriesForm.get('qCode').setValue(data.value);
            this.seriesForm.markAsDirty();
        } else {
            this.seriesForm.get('qCode').setValue("-1");
        }
    }

    private loadSubjects() {
        this.subjects = [
            {
                id: "0",
                text: "Ahp"
            },
            {
                id: "1",
                text: "FIR"
            },
            {
                id: "2",
                text: "FIR (MOB)"
            },
            {
                id: "3",
                text: "Ahp (RSC)"
            }
        ];
        this.selectedSubject = this.model.subject;
    }

    private loadRegions(): void {
        this.dataService.getAllGeoRegions()
            .subscribe((geoR: IGeoRegionPartial[]) => {
                this.geoRegions = geoR.map(
                    function (r: any) {
                        return <Select2OptionData>{
                            id: r.id.toString(),
                            text: r.name
                        };
                    });
                this.geoRegions.unshift({ id: "-1", text: "" });
                this.selectedRegionId = String(this.model.regionId) || '-1';
            });

    }

    private loadDissCategory(): void {
        this.disseminationCategories = [
            { id: '-1', text: '' },
            { id: '0', text: 'National' },
            { id: '1', text: 'US' },
            { id: '2', text: 'International' },
        ];
        this.selectedDisseminationCategory = String(this.model.disseminationCategory) || '-1';

    }

    private loadQCodes(): void {
        this.dataService.getAllIcaoSubjectCodes()
            .subscribe((codes: string[]) => {
                this.codes = codes.map(
                    function (r: any) {
                        return <Select2OptionData>{
                            id: r,
                            text: r
                        };
                    });

                this.codes.unshift({
                    id: "-1",
                    text: this.translation.translate('SeriesAllocationSession.SelectQCode'),
                });

                this.selectedCodeId = this.model.qCode;
                if (this.selectedCodeId === "OB" && this.selectedSubject === 2) {
                    this.readOnlyQCode = true;
                } else {
                    this.readOnlyQCode = false;
                }
            });
    }
}

function validateSubject(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value === '-1') {
        return { 'required': true }
    }
    return null;
}

function validateRegionId(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value === '-1') {
        return { 'required': true }
    }
    return null;
}

function validateSeries(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value.length !== 1) {
        return { 'required': true }
    }
    if (!hasUpperCase(c.value)) {
        return { 'required': true }
    }
    return null;
}

function validateDisseminationCategory(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value === '-1') {
        return { 'required': true }
    }
    return null;
}

function validateQCode(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value === '-1') {
        return { 'required': true }
    }
    return null;
}

function hasUpperCase(str: string) {
    return (/[A-Za-z]/.test(str));
}

﻿namespace NavCanada.Core.TaskEngine.PerformanceCounters
{
    public interface ISimpleCounter
    {
        void Reset();
        void Increment();
        void IncrementBy(long amount);
        void Decrement();
    }
}

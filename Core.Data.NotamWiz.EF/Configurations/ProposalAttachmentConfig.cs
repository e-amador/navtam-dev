﻿using NavCanada.Core.Domain.Model.Entitities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    public class ProposalAttachmentConfig : EntityTypeConfiguration<ProposalAttachment>
    {
        public ProposalAttachmentConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            HasRequired(e => e.Proposal)
                .WithMany()
                .HasForeignKey(e => e.ProposalId)
            .WillCascadeOnDelete(false);

            HasRequired(e => e.User)
                .WithMany()
                .HasForeignKey(e => e.UserId);
        }
    }
}

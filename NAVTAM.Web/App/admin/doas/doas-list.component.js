"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DoasListComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var toastr_service_1 = require("./../../common/toastr.service");
var router_1 = require("@angular/router");
var data_service_1 = require("../shared/data.service");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var windowRef_service_1 = require("../../common/windowRef.service");
var angular_l10n_1 = require("angular-l10n");
var DoasListComponent = /** @class */ (function () {
    function DoasListComponent(toastr, fb, dataService, memStorageService, router, winRef, route, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.route = route;
        this.locale = locale;
        this.translation = translation;
        this.paging = {
            currentPage: 1,
            pageSize: 0,
            totalCount: 0,
            totalPages: 0,
        };
        this.totalPages = 0;
        this.loadingData = false;
        this.canbeDeleted = false;
        this.selectedDoa = null;
        if (this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);
        this.tableHeaders = {
            sorting: [
                { columnName: 'Name', direction: 0 },
                { columnName: 'Description', direction: 0 },
                { columnName: 'Organizations', direction: 0 }
            ],
            itemsPerPage: [
                { id: '10', text: '10' },
                { id: '20', text: '20' },
                { id: '50', text: '50' },
                { id: '100', text: '100' },
                { id: '200', text: '200' },
            ]
        };
        this.paging.pageSize = +this.tableHeaders.itemsPerPage[0].id;
        this.queryOptions = {
            entity: "doas",
            page: 1,
            pageSize: this.paging.pageSize,
            sort: this.tableHeaders.sorting[0].columnName,
            filterValue: ""
        };
    }
    ;
    DoasListComponent.prototype.ngOnInit = function () {
        //Set latest query options
        var queryOptions = this.memStorageService.get(this.memStorageService.ITEM_QUERY_OPTIONS_KEY);
        if (queryOptions && queryOptions.entity === "doas") {
            this.queryOptions = queryOptions;
        }
        this.configureReactiveForm();
        this.getDoasInPage(this.queryOptions.page);
    };
    Object.defineProperty(DoasListComponent.prototype, "langCulture", {
        get: function () {
            return (window["app"].cultureInfo || "en").toUpperCase();
        },
        enumerable: false,
        configurable: true
    });
    DoasListComponent.prototype.configureReactiveForm = function () {
        var _this = this;
        this.doaListForm = this.fb.group({
            searchDoa: [{ value: this.queryOptions.filterValue, disabled: false }, []],
        });
        this.$ctrl("searchDoa").valueChanges.debounceTime(1000).subscribe(function () { return _this.updateDoasList(); });
    };
    DoasListComponent.prototype.updateDoasList = function () {
        if (this.loadingData)
            return;
        var subject = this.$ctrl('searchDoa');
        this.queryOptions.filterValue = subject.value;
        this.getDoasInPage(this.queryOptions.page);
    };
    DoasListComponent.prototype.$ctrl = function (name) {
        return this.doaListForm.get(name);
    };
    DoasListComponent.prototype.selectDoaAllocation = function (doa) {
        if (this.selectedDoa) {
            this.selectedDoa.isSelected = false;
        }
        this.canbeDeleted = (doa.orgCount == 0);
        this.selectedDoa = doa;
        this.selectedDoa.isSelected = true;
        this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, this.selectedDoa.id);
    };
    DoasListComponent.prototype.itemsPerPageChanged = function (e) {
        this.queryOptions.pageSize = e.value;
        this.getDoasInPage(1);
    };
    DoasListComponent.prototype.onPageChange = function (page) {
        this.getDoasInPage(page);
    };
    DoasListComponent.prototype.sort = function (index) {
        var dir = this.tableHeaders.sorting[index].direction;
        for (var i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }
        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
        }
        this.getDoasInPage(this.queryOptions.page);
    };
    DoasListComponent.prototype.sortingClass = function (index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';
        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';
        return 'sorting';
    };
    DoasListComponent.prototype.getDoasInPage = function (page) {
        var _this = this;
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, this.queryOptions);
            this.dataService.getRegisteredDoas(this.queryOptions)
                .subscribe(function (doasData) {
                _this.processDoas(doasData);
            });
        }
        catch (e) {
            this.loadingData = false;
        }
    };
    DoasListComponent.prototype.processDoas = function (doasData) {
        this.paging = doasData.paging;
        this.totalPages = doasData.paging.totalPages;
        for (var iter = 0; iter < doasData.data.length; iter++)
            doasData.data[iter].index = iter;
        var lastDoaIdSelected = this.memStorageService.get(this.memStorageService.ITEM_ID_KEY);
        if (doasData.data.length > 0) {
            this.doas = doasData.data;
            var index = 0;
            if (lastDoaIdSelected) {
                index = this.doas.findIndex(function (x) { return x.id === lastDoaIdSelected; });
                if (index < 0)
                    index = 0; // when not found first should be selected;
            }
            this.doas[index].isSelected = true;
            this.selectedDoa = this.doas[index];
            this.canbeDeleted = (this.selectedDoa.orgCount == 0);
            this.loadingData = false;
        }
        else {
            this.doas = [];
            this.selectedDoa = null;
            this.loadingData = false;
        }
    };
    DoasListComponent.prototype.confirmDeleteDoa = function () {
        $('#modal-delete').modal({});
    };
    DoasListComponent.prototype.deleteDoa = function () {
        var _this = this;
        if (this.selectedDoa) {
            this.dataService.deleteDoa(this.selectedDoa.id)
                .subscribe(function (result) {
                _this.getDoasInPage(_this.paging.currentPage);
                var msg = _this.translation.translate('DoasAdminSession.SuccessDoaDeleted');
                _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            }, function (error) {
                var msg = _this.translation.translate('DoasAdminSession.FailureDoaDeleted');
                _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
        }
    };
    DoasListComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/doas/doas-list.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], DoasListComponent);
    return DoasListComponent;
}());
exports.DoasListComponent = DoasListComponent;
//# sourceMappingURL=doas-list.component.js.map
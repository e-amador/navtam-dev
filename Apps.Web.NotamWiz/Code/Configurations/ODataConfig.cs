﻿namespace NavCanada.Applications.NotamWiz.Web.Code.Configurations
{
    using System.Web.Http;
    using System.Web.OData.Builder;
    using System.Web.OData.Extensions;

    using NavCanada.Applications.NotamWiz.Web.Models.CustomValidations;
    using NavCanada.Core.Domain.Model.Entitities;

    public static class ODataConfig
    {
        public static void Register(HttpConfiguration config)
        {

            config.MapHttpAttributeRoutes(); //This has to be called before the following OData mapping, so also before WebApi mapping

            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();

            builder.EntitySet<IcaoSubject>("IcaoSubjects");
            builder.EntitySet<IcaoSubjectCondition>("IcaoSubjectConditions");
            builder.EntitySet<NsdVersion>("NsdVersions");
            builder.EntitySet<Organization>("Organizations");
            builder.EntitySet<Nsd>("Nsds");
            builder.EntitySet<SdoSubject>("SdoSubjects");
            builder.EntitySet<FirViewModel>("FIR");
            builder.EntitySet<NotamPackage>("NotamPackages");
            builder.EntitySet<Doa>("Doas");
            builder.EntitySet<UserProfile>("UserProfiles");            
            config.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());

        }
    }
}

﻿namespace NavCanada.Core.Domain.Model.Dtos
{
    public class IcaoSubjectConditionPartialDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Traffic { get; set; }
        public string Purpose { get; set; }
        public int Lower { get; set; }
        public int Upper { get; set; }
        public int Radius { get; set; }
        public string Description { get; set; }
        public string DescriptionFrench { get; set; }
        public bool requiresItemFG { get; set; }
        public bool RequiresPurpose { get; set; }
        public bool CancellationOnly { get; set; }
        public bool IsActive { get; set; }
    }
}

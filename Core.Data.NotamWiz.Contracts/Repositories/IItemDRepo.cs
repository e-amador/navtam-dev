﻿using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    public interface IItemDRepo  : IRepository<ItemD>
    {
        Task<List<ItemD>> GetByProposalId(int proposalId);
        Task<ItemD> GetCalendarItemD(int proposalId, int calendarId, string eventId);
    }
}
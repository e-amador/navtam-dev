﻿namespace NavCanada.Utils.Apps.InoMock.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Web.Mvc;

    using AutoMapper;

    using Kendo.Mvc.Extensions;
    using Kendo.Mvc.UI;

    using Models;

    public class HomeController : Controller
    {
        readonly DBContext _db = new DBContext();

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Notams_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_db.NotamProposals.ToDataSourceResult(request));
        }

        [HttpGet]
        public ActionResult Pick(int id)
        {

            NotamProposal proposal = _db.NotamProposals.FirstOrDefault(n => n.Id == id);
            
            if (proposal != null) { 
                proposal.Pick();
                _db.SaveChanges();
            }
            
            return Json(proposal.Status, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult UnPick(int id)
        {
            NotamProposal proposal = _db.NotamProposals.FirstOrDefault(n => n.Id == id);

            if (proposal != null)
            {
                proposal.Status = NotamProposalStatusType.Queued;
                _db.SaveChanges();
            }

            return Json(proposal.Status, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Reject(int id, string reason)
        {
            NotamProposal proposal = _db.NotamProposals.FirstOrDefault(n => n.Id == id);

            if (proposal != null)
            {
                proposal.RejectionReason = reason;
                proposal.Status = NotamProposalStatusType.Rejected;
                _db.SaveChanges();
            }

            return Json(proposal.Status, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Submit(int id)
        {
            NotamProposal proposal = _db.NotamProposals.FirstOrDefault(n => n.Id == id);

            return View(proposal);
        }

        [HttpPost]
        public ActionResult Submit(NotamProposal  notam)
        {
           NotamProposal proposal = _db.NotamProposals.FirstOrDefault(n => n.Id == notam.Id);

            if (proposal != null)
            {
                //if (notam.IsEqual(proposal, "QLine", "Operator", "Status", "StatusTime", "SubmittedNotam_Id", "NoteToNOF") &&
                //    notam.QLine.IsEqual(proposal.QLine))
                //{
                //    proposal.Status = NotamProposalStatusType.Submitted;
                //}
                //else
                //{
                //    proposal.Status = NotamProposalStatusType.ModifiedAndSubmitted;
                //}
                proposal.Status = NotamProposalStatusType.Submitted;

                notam.Year = DateTime.UtcNow.Year;
                notam.Number = (int)proposal.Id;

                proposal.SubmittedNotam = Mapper.Map<NotamProposal, Notam>(notam);

                _db.SaveChanges();
            }

            return Redirect("/");
        }
    }


    public static class ext
    {
        public static bool IsEqual<T>(this T self, T to, params string[] ignore) where T : class
        {
            if (self != null && to != null)
            {
                var type = typeof(T);
                var ignoreList = new List<string>(ignore);
                var unequalProperties =
                    from pi in type.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                    where !ignoreList.Contains(pi.Name)
                    let selfValue = type.GetProperty(pi.Name).GetValue(self, null)
                    let toValue = type.GetProperty(pi.Name).GetValue(to, null)
                    where selfValue != toValue && (selfValue == null || !selfValue.Equals(toValue)) &&
                            (((string) selfValue != "" && selfValue != null) || toValue != null || (string) toValue == "") &&
                            (((string) toValue != "" && toValue != null) || selfValue != null || (string) selfValue == "")

                    select new { pi.Name, selfValue, toValue };
                return !unequalProperties.Any();
            }
            return self == to;
        }
    }
}
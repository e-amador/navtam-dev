﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Testing;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Common.Security;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NAVTAM.App_Start;
using NAVTAM.Helpers;
using NAVTAM.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace NAVTAM.Controllers
{
    [Authorize]
    public class AccountController : BaseController
    {
       public AccountController(IUow uow, IGeoLocationCache geoLocationCache, ILogger logger) : base(uow, logger)
        {
            //UserManager = UserManager;
            //SignInManager = SignInManager;
            _geoLocationCache = geoLocationCache;
            _logger = logger;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            try
            {
                ViewBag.ReturnUrl = returnUrl;
                ViewBag.DisasterRecoveryMode = ConfigurationManager.AppSettings["DisasterRecoveryMode"] == "1";
                return View();
            }
            catch (Exception)
            {
                return new FilePathResult("~/500.html", "text/html");
                throw;
            }
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            return LogAction(() => _Login(model, returnUrl), "POST:/Account/Login");
        }

        private async Task<ActionResult> _Login(LoginViewModel model, string returnUrl)
        {
            ViewBag.DisasterRecoveryMode = ConfigurationManager.AppSettings["DisasterRecoveryMode"] == "1";

            if (!ModelState.IsValid)
                return View(model);

            CleanAppCookiesAndCache();

            // verify the user exists and it is valid
            var user = await Uow.UserProfileRepo.GetByUserNameAsync(model.Username);
            var userValid = await ValidateUser(user);
            if (!userValid)
            {
                await ClearAccessToken(model.Username);
                ModelState.AddModelError("", Resources.InvalidLoginAttempt);
                _logger.InvalidLogin(GetType(), model.Username);
                return View(model);
            }

            try
            {
                var rememberMe = "on".Equals(model.RememberMe);

                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, change to shouldLockout: true
                var result = await SignInManager.PasswordSignInAsync(model.Username, model.Password, rememberMe, shouldLockout:true);
                switch (result)
                {
                    case SignInStatus.Success:
                        _logger.UserLogin(user.UserName, user.Id);
                        await SetAppCookiesAndCache(model.Username);
                        return RedirectToLocal(returnUrl);

                    case SignInStatus.LockedOut:
                        //await ClearAccessToken(model.Username);
                        return View("Lockout");

                    case SignInStatus.RequiresVerification:
                        //await ClearAccessToken(model.Username);
                        return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, model.RememberMe });

                    case SignInStatus.Failure:
                    default:
                        var existingUser = await UserManager.FindByNameAsync(model.Username);
                        if (existingUser != null && !existingUser.LockoutEnabled)
                        {
                            await UserManager.SetLockoutEnabledAsync(existingUser.Id, true);
                        }
                        //await ClearAccessToken(model.Username);
                        _logger.InvalidLogin(GetType(), model.Username);
                        ModelState.AddModelError("", Resources.InvalidLoginAttempt);
                        return View(model);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, GetType(), $"Error in Login");
                //TODO: Catch and redirect to Error page...
                return new FilePathResult("~/500.html", "text/html");
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            return await LogAction(async () =>
            {
                // Require that the user has already logged in via username/password or external login
                if (!await SignInManager.HasBeenVerifiedAsync())
                {
                    return View("Error");
                }
                else
                {
                    return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
                }
            }, "/Account/VerifyCode");
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            return await LogAction(async () =>
            {
                if (!ModelState.IsValid)
                    return View(model);

                // The following code protects for brute force attacks against the two factor codes. 
                // If a user enters incorrect codes for a specified amount of time then the user account 
                // will be locked out for a specified amount of time. 
                // You can configure the account lockout settings in IdentityConfig
                var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
                switch (result)
                {
                    case SignInStatus.Success:
                        return RedirectToLocal(model.ReturnUrl);
                    case SignInStatus.LockedOut:
                        return View("Lockout");
                    case SignInStatus.Failure:
                    default:
                        ModelState.AddModelError("", Resources.InvalidCode);
                        return View(model);
                }
            }, "POST: /Account/VerifyCode");
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string code)
        {
            return await LogAction(async () =>
            {
                if( string.IsNullOrEmpty(code))
                {
                    return View("Error");
                }

                var parts = code.Split('|');
                if (parts.Length != 2)
                {
                    return View("Error");
                }

                var userid = CipherUtils.DecryptText(parts[0]);
                code = parts[1];

                var isValidToken = await UserManager.VerifyUserTokenAsync(userid, "EmailConfirmation", code);

                var user = await UserManager.FindByIdAsync(userid);

                if ( isValidToken)
                {
                    var model = new ConfirmEmailViewModel
                    {
                        UserId = userid,
                        UserName = user.UserName,
                        Code = code,
                        EmailVerificationCode = await UserManager.GenerateEmailConfirmationTokenAsync(userid),
                    };
                    return View(model);
                }

                return View("Error");

            }, "GET: /Account/ConfirmEmail");
        }

        // GET: /Account/ConfirmRegistration
        [AllowAnonymous]
        public ActionResult ConfirmRegistration()
        {
            return LogAction(() =>
            {
                return View(new ConfirmRegistrationViewModel());

            }, "GET: /Account/ConfirmEmail");
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ConfirmEmail(ConfirmEmailViewModel model)
        {
            return await LogAction(async () =>
            {
                if (ModelState.IsValid)
                {
                    var userRegistration = await Uow.UserRegistrationRepo.GetUserRegistrationAsync(model.UserId);
                    if( userRegistration  != null )
                    {
                        model.EmailVerificationCode = userRegistration.VerificationCode;
                    }
                    //else
                    //{
                    //    model.UserId = null;
                    //}

                    //var user = await UserManager.FindByIdAsync(model.UserId);

                    IdentityResult result = IdentityResult.Success;
                    var confirmedEmail = await UserManager.IsEmailConfirmedAsync(model.UserId);
                    if ( !confirmedEmail)
                    {
                        result = await UserManager.ConfirmEmailAsync(model.UserId, model.EmailVerificationCode);
                    }
                    if (result.Succeeded)
                    {
                        string code = await UserManager.GeneratePasswordResetTokenAsync(model.UserId);
                        result = await UserManager.ResetPasswordAsync(model.UserId, code, model.Password);

                        if (result.Succeeded)
                            return RedirectToAction("ConfirmEmailConfirmation", "Account");
                    }

                    if(result.Succeeded)
                    {
                        View("ConfirmEmail");
                    }
                    else
                    {
                        return new FilePathResult("~/500.html", "text/html");
                    }
                }
                return View() as ActionResult;


            }, "GET: /Account/ConfirmEmail");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ConfirmRegistration(ConfirmRegistrationViewModel model)
        {
            return await LogAction(async () =>
            {
                if (ModelState.IsValid)
                {
                    var user = await Uow.UserProfileRepo.GetByUserNameAsync(model.Username);
                    if (user != null)
                    {
                        var userRegistration = await Uow.UserRegistrationRepo.GetUserRegistrationAsync(user.Id);
                        string verificationCode = null;
                        if (userRegistration != null)
                        {
                            verificationCode = userRegistration.VerificationCode;
                        }
                        else
                        {
                            model.Username = "";
                        }

                        var result = await UserManager.ConfirmEmailAsync(user.Id, verificationCode);
                        if (result.Succeeded)
                        {
                            string passwordCode = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                            result = await UserManager.ResetPasswordAsync(user.Id, passwordCode, model.Password);

                            if (result.Succeeded)
                                return RedirectToAction("ConfirmRegistrationConfirmation", "Account");
                        }

                        if(result.Succeeded )
                        {
                            View("ConfirmRegistration");
                        }
                        else
                        {
                            return new FilePathResult("~/500.html", "text/html");
                        }
                    }
                }
                return View() as ActionResult;


            }, "GET: /Account/ConfirmRegistration");
        }


        [AllowAnonymous]
        public ActionResult ConfirmEmailConfirmation()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ConfirmRegistrationConfirmation()
        {
            return View();
        }



        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            return await LogAction(async () =>
            {
                if (ModelState.IsValid)
                {
                    var user = await UserManager.FindByNameAsync(model.Username);
                    if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                    {
                        // Don't reveal that the user does not exist or is not confirmed
                        return View("ForgotPasswordConfirmation") as ActionResult;
                    }

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);

                    var fullCode = HttpUtility.UrlEncode(CipherUtils.EncryptText(user.Id)) + "|" + HttpUtility.UrlEncode(code);
                    var host = ConfigurationManager.AppSettings["domain"];
                    var callbackUrl = host + String.Format("/Account/ResetPassword?code={0}", fullCode);

                    var body = "To reset your password, click <a href=\"" + callbackUrl + "\">here</a>. </br>" +
                               "Pour réinitialiser votre mot de passe, cliquez <a href=\"" + callbackUrl + "\">ici</a>. </br>";

                    await UserManager.SendEmailAsync(user.Id, "Reset Password - Réinitialiser votre mot de passe", body);
                    return RedirectToAction("ForgotPasswordConfirmation", "Account");
                }

                // If we got this far, something failed, redisplay form
                return View(model) as ActionResult;

            }, "POST: /Account/ForgotPassword");
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public async Task<ActionResult> ResetPassword(string code)
        {
            if( string.IsNullOrEmpty(code))
            {
                return new FilePathResult("~/500.html", "text/html");
            }

            var parts = code.Split('|');
            if( parts.Length != 2)
            {
                return new FilePathResult("~/500.html", "text/html");
            }

            var userId = CipherUtils.DecryptText(parts[0]);
            code = parts[1];

            bool isValidToken = false;
            if( ConfigurationManager.AppSettings["DisasterRecoveryMode"] == "1")
            {
                code = "TEMPCODE";
                isValidToken = true;
            }
            else
            {
                isValidToken = await UserManager.VerifyUserTokenAsync(userId, "ResetPassword", code);
            }

            if(!isValidToken)
            {
                return new FilePathResult("~/500.html", "text/html");
            }
            else
            {
                return View();
            }
        }

        [AllowAnonymous]
        public ActionResult CreatePassword(string code, bool resetPassword = false)
        {
            ViewBag.ResetPassword = resetPassword;
            if(code == null)
            {
                return new FilePathResult("~/500.html", "text/html");
            }
            else
            {
                return View();
            }
        }


        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            return await LogAction(async () =>
            {
                if (!ModelState.IsValid)
                    return View(model);

                var user = await UserManager.FindByNameAsync(model.Username);
                if (user != null)
                {
                    string verificationCode = null;
                    UserRegistration userRegistration = null;
                    if (ConfigurationManager.AppSettings["DisasterRecoveryMode"] == "1")
                    {
                        userRegistration = await Uow.UserRegistrationRepo.GetUserRegistrationAsync(user.Id);
                        if (userRegistration != null)
                        {
                            verificationCode = userRegistration.VerificationCode;
                        }
                    }
                    else
                    {
                        verificationCode = model.Code;
                    }

                    var isValidToken = await UserManager.VerifyUserTokenAsync(user.Id, "ResetPassword", verificationCode);
                    if(!isValidToken )
                    {
                        return View() as ActionResult;
                    }

                    var result = await UserManager.ResetPasswordAsync(user.Id, ConfigurationManager.AppSettings["DisasterRecoveryMode"] == "1" ? verificationCode : verificationCode, model.Password);
                    if (!result.Succeeded)
                    {
                        result = await UserManager.ResetPasswordAsync(user.Id, ConfigurationManager.AppSettings["DisasterRecoveryMode"] == "1" ? verificationCode : HttpUtility.UrlDecode(verificationCode), model.Password);
                        if (!result.Succeeded)
                        {
                            AddErrors(result);
                            return View() as ActionResult;
                        }
                    } else
                    {
                        if( userRegistration != null )
                        {
                            userRegistration.VerificationCode = null;
                            Uow.UserRegistrationRepo.Update(userRegistration);
                            await Uow.SaveChangesAsync();
                        }
                    }

                    return RedirectToAction("ResetPasswordConfirmation", "Account");
                }
                else
                {
                    var result = new IdentityResult(new List<string>() { "Invalid Email Address" });
                    AddErrors(result);
                }

                return View() as ActionResult;

            }, "POST: /Account/ResetPassword");
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            return await LogAction(async () =>
            {
                var userId = await SignInManager.GetVerifiedUserIdAsync();
                if (userId == null)
                {
                    return View("Error");
                }
                var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
                var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
                return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
            }, "GET: /Account/SendCode");
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            return await LogAction(async () =>
            {
                if (!ModelState.IsValid)
                    return View();

                // Generate the token and send it
                if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
                    return View("Error");

                return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, model.ReturnUrl, model.RememberMe }) as ActionResult;

            }, "POST: /Account/SendCode");
        }

        [HttpGet]
        [Authorize]
        public async Task<ActionResult> AccessToken(string userid)
        {
            return await LogAction(async () =>
            {
                var user = await UserManager.FindByIdAsync(userid);
                return Content(user?.AccessToken ?? "");
            }, "Account/AccessToken");
        }


        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            return await LogAction(async () =>
            {
                var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (loginInfo == null)
                    return RedirectToAction("Login");

                // Sign in the user with this external login provider if the user already has a login
                var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
                switch (result)
                {
                    case SignInStatus.Success:
                        return RedirectToLocal(returnUrl);
                    case SignInStatus.LockedOut:
                        return View("Lockout");
                    case SignInStatus.RequiresVerification:
                        return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                    case SignInStatus.Failure:
                    default:
                        // If the user does not have an account, then prompt the user to create an account
                        ViewBag.ReturnUrl = returnUrl;
                        ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                        return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
                }

            }, "GET: /Account/ExternalLoginCallback");
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            return await LogAction(async () =>
            {
                if (User.Identity.IsAuthenticated)
                    return RedirectToAction("Index", "Manage");

                if (ModelState.IsValid)
                {
                    // Get the information about the user from the external login provider
                    var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                    if (info == null)
                    {
                        return View("ExternalLoginFailure");
                    }
                    var user = new UserProfile { UserName = model.Email, Email = model.Email };
                    var result = await UserManager.CreateAsync(user);
                    if (result.Succeeded)
                    {
                        result = await UserManager.AddLoginAsync(user.Id, info.Login);
                        if (result.Succeeded)
                        {
                            await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                            return RedirectToLocal(returnUrl);
                        }
                    }
                    AddErrors(result);
                }

                ViewBag.ReturnUrl = returnUrl;

                return View(model);

            }, "POST: /Account/ExternalLoginConfirmation");
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> LogOff()
        {
            return await LogAction(async () =>
            {
                await ClearAccessToken(User.Identity.Name);
                CleanAppCookiesAndCache();

                _logger.UserLogout(User.Identity.Name, User.Identity.GetUserId());

                return RedirectToAction("Login", "Account");
            }, "POST: /Account/LogOff");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Language(string culture, string returnUrl)
        {
            return LogAction(() =>
            {
                SetLanguage(culture);
                return RedirectToLocal(returnUrl);
            }, "Account/Language");
        }

        private async Task SetAppCookiesAndCache(string userName)
        {
            var user = await UserManager.FindByNameAsync(userName);
            if (user != null)
            {
                AddCookie("usr_id", user.Id);
                if (user.OrganizationId.HasValue)
                {
                    var orgId = user.OrganizationId.Value;
                    AddCookie("org_id", orgId.ToString());
                    var org = await Uow.OrganizationRepo.GetByIdAsync(orgId);

                    _geoLocationCache.CleanGeoLocationCache(user.Id);
                    await _geoLocationCache.GetUserDoa(Uow, user.Id);

                    // todo: fix this!
                    var userDoas = await Uow.UserDoaRepo.GetDoasByUserAsync(user.Id);
                    var doaId = userDoas.Count > 0 ? userDoas.First().DoaId : -1;
                    AddCookie("doa_id", doaId.ToString());
                }
            }
        }

        private async Task SetAccessToken(string username, string password)
        {
            var testServer = TestServer.Create<Startup>();
            var requestParams = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("grant_type", "password"),
                new KeyValuePair<string, string>("username", username),
                new KeyValuePair<string, string>("password", password)
            };

            var requestParamsFormUrlEncoded = new FormUrlEncodedContent(requestParams);
            var tokenServiceResponse = await testServer.HttpClient.PostAsync("/Token", requestParamsFormUrlEncoded);

            var accessToken = tokenServiceResponse.Content.ReadAsStringAsync().Result;

            var user = await Uow.UserProfileRepo.GetByUserNameAsync(username);
            if (user != null)
            {
                user.AccessToken = accessToken;
                Uow.UserProfileRepo.Update(user);
                await Uow.SaveChangesAsync();
            }
        }

        private async Task ClearAccessToken(string username)
        {
            var user = await Uow.UserProfileRepo.GetByUserNameAsync(username);
            if (user != null)
            {
                user.AccessToken = null;
                Uow.UserProfileRepo.Update(user);
                await Uow.SaveChangesAsync();
            }
        }

        private void CleanAppCookiesAndCache()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            DeleteCookie("usr_id");
            DeleteCookie("org_id");
            DeleteCookie("doa_id");
            DeleteCookie("_vsp");
            _geoLocationCache.CleanGeoLocationCache(User?.Identity?.GetUserId());
        }

        private void AddCookie(string name, string value)
        {
            var cookie = new HttpCookie(name, value)
            {
                HttpOnly = true,
                //Secure = true,
            };
            Response.AppendCookie(cookie);
        }

        private void DeleteCookie(string name)
        {
            var cookie = HttpContext.Request.Cookies[name];
            if (cookie != null)
            {
                cookie.Value = null;
                cookie.Expires = DateTime.Now.AddDays(-20);
                Response.Cookies.Add(cookie);
            }
        }

        private async Task<bool> ValidateUser(UserProfile user)
        {
            if (user == null || user.IsDeleted || user.Disabled)
                return false;

            var roles = await UserManager.GetRolesAsync(user.Id);

            return roles.Count > 0;
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion

        class ReCpatchaResponse
        {
            public string Success { get; set; }
        }

        private ApplicationSignInManager _signInManager;
        private IGeoLocationCache _geoLocationCache;
        private ILogger _logger;
    }
}
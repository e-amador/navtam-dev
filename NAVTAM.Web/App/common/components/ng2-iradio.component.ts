﻿import {Component, Input, Output, EventEmitter } from '@angular/core';

import {Observable} from 'rxjs/Observable';

@Component({
    selector: 'iRadio',
    styles: [`
    :host {
        font-family: "Consolas", "Microsoft YaHei", Arial, arial, sans-serif;
        overflow: hidden;
    }
  `,`
    :host > div > div {
        width: 24px;
        height: 24px;
        display: inline-block;
        vertical-align: middle;
        background: url('content/css/icheck.skins/square/grey.png') no-repeat left;
        background-position: -120px 0;
        cursor: pointer;
    }
  `,`
    :host > div > div:hover {
        background-position: -144px 0;
    }
  `,`
    :host > div > div.disabled {
        background-position: -192px 0;
        cursor: default;
    }    
  `,`
    :host > div > div.checked {
        background-position: -168px 0;
    }
  `,`
    :host > div > div.checked.disabled {
        background-position: -216px 0;
    }
  `,`
    :host .label {
        display: inline-block;
        vertical-align: middle;
    }
  `],
    template: `
    <div (click)="toggleCheck()" class="ng2-iradio">
        <div [class.checked]="isChecked" [class.disabled]="disabled"></div>
        <ng-content class="label"></ng-content>
    </div>
  `
})

export class IRadioComponent {
    @Input() isChecked: boolean = false;
    @Input() disabled: boolean = false;
    @Output() change : EventEmitter<boolean> = new EventEmitter<boolean>();
    constructor() {
    }

    toggleCheck() {
        if (!this.disabled) {
            this.isChecked = !this.isChecked;
            this.change.emit(this.isChecked);
        }
    }
}
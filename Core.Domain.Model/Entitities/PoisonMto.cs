﻿using System;
using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class PoisonMto
    {
        /// <summary>
        ///     Unique identifier of the poison Mto
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        ///     MessageType of Mto
        /// </summary>
        public MessageType MessageType { get; set; }

        /// <summary>
        ///     MessageId
        /// </summary>
        public MessageTransferObjectId MessageId { get; set; }

        /// <summary>
        ///     Contents of the poison Mto
        /// </summary>
        public byte[] Payload { get; set; }

        /// <summary>
        ///     Type of Mto: 0 for taskengine (default)
        /// </summary>
        public int ReporterType { get; set; }

        /// <summary>
        ///     IP of Reporter
        /// </summary>
        public string Reporter { get; set; }

        /// <summary>
        ///     Content
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        ///     Exception
        /// </summary>
        public string Exception { get; set; }

        /// <summary>
        ///     Report Time
        /// </summary>
        public DateTime? ReportTime { get; set; }
    }
}
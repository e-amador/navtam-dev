﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Core.TaskEngine.Triggers
{
    public class AeroRDSCacheSyncJob : ScheduleJobTriggerBase
    {
        protected override string GetScheduleTaskId() => ScheduleIdentity.AeroRdsEffectiveCacheSyncScheduler;
        protected override MessageTransferObjectId GetSheduleTaskMessageId() => MessageTransferObjectId.AeroRDSCacheEffectiveDataMsg;
    }
}

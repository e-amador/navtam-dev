﻿using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.EF.Repositories
{
    public class OriginatorInfoRepo : Repository<OriginatorInfo>, IOriginatorInfoRepo
    {
        public Task<OriginatorInfo> GetByIdAsync(int id)
        {
            return DbSet.FirstOrDefaultAsync(r => r.Id == id);
        }


        public OriginatorInfoRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public Task<List<OriginatorInfo>> FilterAsync(string match, int pageSize, int orgId)
        {
            return DbSet.Where(r => r.OrganizationId == orgId && r.Fullname.Contains(match))
                                   .OrderBy(r => r.Fullname)
                                   .Take(pageSize)
                                   .ToListAsync();

        }

        public Task<OriginatorInfo> FindAsync(string fullname, int orgId)
        {
            return DbSet.FirstOrDefaultAsync(r => r.OrganizationId == orgId && r.Fullname == fullname);
        }

    }
}

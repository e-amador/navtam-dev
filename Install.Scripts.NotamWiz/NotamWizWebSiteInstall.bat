setlocal
cd /d %~dp0

set MSDeployPath=C:\Program Files (x86)\IIS\Microsoft Web Deploy V3

"%MSDeployPath%\msdeploy.exe" -source:package=NES.zip  -dest:auto -verb:sync -disableLink:AppPoolExtension -disableLink:ContentExtension -disableLink:CertificateExtension -setParamFile:NES.SetParameters.xml

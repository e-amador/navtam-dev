﻿using NavCanada.Core.Data.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Domain.Model.Entitities;
using System.Data.Entity;
using System.Linq;

namespace NavCanada.Core.Data.EF.Repositories
{
    public class AeroRDSCacheStatusRepo : Repository<AeroRDSCacheStatus>, IAeroRDSCacheStatusRepo
    {
        public AeroRDSCacheStatusRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public AeroRDSCacheStatus GetStatus() => DbSet.FirstOrDefault();
    }
}

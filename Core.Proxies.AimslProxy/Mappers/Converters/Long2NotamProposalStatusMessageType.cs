﻿namespace NavCanada.Core.Proxies.AimslProxy.Mappers.Converters
{
    using System;

    using AutoMapper;

    using AimslService;

    public class Long2NotamProposalStatusMessageType : ITypeConverter<long, NotamProposalStatusMessageType>
    {
        public NotamProposalStatusMessageType Convert(ResolutionContext context)
        {
            var obj = context.SourceValue;
            if( obj == null )
                throw new NullReferenceException("NotamPorposalId can't be null");

            long notamProposalId;
            if (!long.TryParse(obj.ToString(), out notamProposalId))
                throw new NullReferenceException("NotamPorposalId is not a valid long number");

            var response = new NotamProposalStatusMessageType
            {
                transaction = new transactionType
                {
                    uuid = Guid.NewGuid().ToString()
                },
                RequestHeader = new RequestHeaderNotamType
                {
                    Version = "2"
                },
                NotamProposalStatus = new NotamProposalStatusType
                {
                    Action = ActionType.Query,
                    NotamProposal = new SingleNotamProposalStatusType[]
                    {
                        new SingleNotamProposalStatusType
                        {
                            NotamProposalIdentifier = notamProposalId.ToString()
                        }
                    }
                }
            };

            return response;
        }
    }
}


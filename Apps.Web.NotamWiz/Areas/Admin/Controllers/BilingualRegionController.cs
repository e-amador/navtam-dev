﻿using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.Areas.Admin.Controllers
{
    using System.Data.Entity.Spatial;
    using System.Linq;
    using System.Web.Mvc;


    using NavCanada.Applications.NotamWiz.Web.Code.Extentions;
    using NavCanada.Applications.NotamWiz.Web.Code.GeoTools;
    using NavCanada.Applications.NotamWiz.Web.Code.Helpers;
    using NavCanada.Applications.NotamWiz.Web.Controllers;
    using NavCanada.Applications.NotamWiz.Web.Filters;
    using NavCanada.Core.Common.Contracts;
    using NavCanada.Core.Data.NotamWiz.Contracts;
    using NavCanada.Core.Domain.Model.Dtos;
    using NavCanada.Core.Domain.Model.Entitities;

    [Authorize(Roles = "Administrator")]
    public class BilingualRegionController : BaseController
    {
        readonly ILog log = LogManager.GetLogger(typeof(BilingualRegionController));

        public BilingualRegionController(IUow uow, IClusterQueues clusterQueues) 
            : base(uow, clusterQueues)
        {
        }

        // GET: Admin/BilingualRegion
        public ActionResult Index()
        {
            this.log.Object(new { Method = "Index", Parameters = new { } });

            BilingualRegion bilingualRegion = Uow.BilingualRegionRepo.GetAll().FirstOrDefault();
            if (bilingualRegion == null)
            {
                bilingualRegion = new BilingualRegion();
                Uow.BilingualRegionRepo.Add(bilingualRegion);
                Uow.SaveChanges();
            }



            return View(GeoDataService.GeoJsonMultiPolygonFromGeography(bilingualRegion.TheRegion));
        }

  

        // return items within a bounding box
        [HttpPost]
        [CompressFilter]
        public ActionResult GetTheRegion()
        {

            this.log.Object(new { Method = "GetTheRegion", Parameters = new { } });

            BilingualRegion bilingualRegion = Uow.BilingualRegionRepo.GetAll().FirstOrDefault();
            if (bilingualRegion == null)
            {
                bilingualRegion = new BilingualRegion();
                Uow.BilingualRegionRepo.Add(bilingualRegion);
                Uow.SaveChanges();
            }

            DbGeography region = bilingualRegion.TheRegion;


            return ProcessResult.GetSuccessResult(new { Success = true, geo = GeoDataService.GeoJsonMultiPolygonFromGeography(region) });

        }

    




        public ActionResult Save(MultiPolygonDto updatedRegion)
        {


            BilingualRegion bilingualRegion = Uow.BilingualRegionRepo.GetAll().FirstOrDefault();
            if (bilingualRegion == null)
            {
                bilingualRegion = new BilingualRegion();
                Uow.BilingualRegionRepo.Add(bilingualRegion);               
            }

            bilingualRegion.TheRegion = updatedRegion == null ? null : GeoDataService.GetDbGeographyFromMultiPolygon(updatedRegion);

            Uow.SaveChanges();

            return ProcessResult.GetSuccessResult();

        }



        public ActionResult Reset()
        {
            BilingualRegion bilingualRegion = Uow.BilingualRegionRepo.GetAll().FirstOrDefault();
            if (bilingualRegion == null)
            {
                bilingualRegion = new BilingualRegion();
                Uow.BilingualRegionRepo.Add(bilingualRegion);
            }
            bilingualRegion.TheRegion = null;
            Uow.SaveChanges();

            return ProcessResult.GetSuccessResult();

        }

    }
}
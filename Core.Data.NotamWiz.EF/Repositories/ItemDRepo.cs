﻿using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.EF.Repositories
{
    public class ItemDRepo : Repository<ItemD>, IItemDRepo
    {
        public ItemDRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public Task<List<ItemD>> GetByProposalId(int proposalId)
        {
            return DbSet.Where(e => e.ProposalId == proposalId).ToListAsync();
        }

        public Task<ItemD> GetCalendarItemD(int proposalId, int calendarId, string eventId)
        {
            return DbSet.SingleOrDefaultAsync(e =>
                        e.ProposalId == proposalId &&
                        e.CalendarId == calendarId &&
                        e.EventId == eventId);
        }

    }
}

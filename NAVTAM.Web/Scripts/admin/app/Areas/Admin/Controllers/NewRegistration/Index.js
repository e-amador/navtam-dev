﻿define(["jquery", "Resources", "kendoExt"], function ($, res)
{
    var labels = {};
    var umObject, ADMIN_ACTION;

    return {

        init: function (lbls)
        {
            labels = lbls;

            $(document).ajaxStart(function ()
            {
                kendo.ui.progress($(".RegDetailAjax-loading"), true);
            });

            $(document).ajaxStop(function ()
            {
                kendo.ui.progress($(".RegDetailAjax-loading"), false);
            });

            model = $("#dataModel").data("model");
            ADMIN_ACTION = "START_UP";
            umObject = this;
            umObject.initUI();
        },

        initUI: function ()
        {
            var detailsTemplate = kendo.template($("#reviewDetailTemplate").html());

            var windowAdmin = $("#userDetail-Popup").kendoWindow({
                actions: ["Cancel"],
                width: 520,
                visible: false,
                title: labels.newReg_userRegInfo,
                modal: true,
                close: function ()
                {
                    $("#userGrid").data('kendoGrid').dataSource.read();
                    $("#userGrid").data('kendoGrid').refresh();
                }
            }).data("kendoWindow");

            windowAdmin.bind("refresh", function ()
            {
                $("#userDetail-Popup").data("kendoWindow").center();
                $("#userDetail-Popup").data("kendoWindow").open();
            });

            var assignOrSelectOrgWindow = $("#assignOrSelectOrganization-Popup").kendoWindow({
                actions: ["Cancel"],
                width: 520,
                visible: false,
                title: labels.newReg_organizationInfo,
                modal: true,
            }).data("kendoWindow");

            assignOrSelectOrgWindow.bind("refresh", function ()
            {
                $("#assignOrSelectOrganization-Popup").data("kendoWindow").center();
                $("#assignOrSelectOrganization-Popup").data("kendoWindow").open();
            });

            $("#userGrid").kendoGrid({
                sortable: true,
                scrollable: true,
                resizable: { columns: true },
                selectable: true,
                filterable: true,
                reorderable: true,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [
                    { field: "userVM.UserName", title: labels.newReg_userName, width: "~100" },
                    { field: "userVM.FirstName", title: labels.newReg_firstName, width: "~100" },
                    { field: "userVM.LastName", title: labels.newReg_lastName, width: "~100" }
                ],
                toolbar: [
                    { name: "Review", text: labels.newReg_showDetails }

                ],
                change: function (e)
                {
                    $("#userGrid").find(".k-grid-Review").show();
                },
                dataSource: {
                    transport: {
                        read: { url: "/Admin/NewRegistration/ReadNewRegistrationUsers", dataType: "json", type: "POST" },
                    },
                    pageSize: 10,
                    schema: {
                        data: "Data",
                        model: {
                            id: "UserId"
                        },
                    },
                    sort: [
                        { field: "IsDisabled", dir: "desc" }
                    ]
                },
                dataBound: function (e)
                {
                    $("#userGrid").find(".k-grid-Review").hide();
                }
            });
            $("#userGrid .k-grid-toolbar .k-grid-Review").on("click", function (e)
            {
                showDetails(e);
            });

            function StartRegProcess()
            {
                var grid = $("#userGrid").data("kendoGrid");
                var dataItem = grid.dataItem(grid.select());
                $("#assignOrSelectOrganization-Popup").data("kendoWindow")
                     .content("")
                     .refresh({
                         url: "NewRegistration/StartRegProcess",
                         type: "POST",
                         complete: function ()
                         {
                             require(["admin/Controllers/NewRegistration/StartRegProcess"], function (StartRegProcess)
                             {
                                 StartRegProcess.init(JSON.stringify(dataItem), labels);
                             });
                         }

                     });

            };
            function RejectRecord()
            {
                var grid = $("#userGrid").data("kendoGrid");
                var dataItem = grid.dataItem(grid.select());
                kendo.ui.ExtOkCancelDialog.show({

                    title: labels.newReg_rejectUser_title + " " + dataItem.userVM.UserName,
                    message: labels.newReg_rejectUser_title + " " + dataItem.userVM.UserName + " " + labels.newReg_rejectUser_message,
                    width: '400px',
                    height: '150px',
                    icon: "k-Ext-warning"
                }).done(function (response)
                {
                    if (response.button === 'OK')
                    {
                        $.ajax({
                            url: '/Admin/NewRegistration/RejectUser',
                            type: 'POST',
                            dataType: 'json',
                            async: false,
                            cache: false,
                            data: { packageId: dataItem.userVM.UserId },
                            success: function (data)
                            {
                                if (data.Success === true)
                                { //need to refresh grid
                                    $("#userGrid").data('kendoGrid').dataSource.read();
                                    $("#userGrid").data('kendoGrid').refresh();
                                }
                                else
                                {
                                    var errorMessage = labels.newReg_rejectError_message + " " + labels.newReg_rejectError_message_cont + (data.Message !== undefined ? " - " + data.Message : "");

                                    kendo.ui.ExtAlertDialog.show({ //need to refresh grid
                                        title: labels.newReg_rejectError_unidentify,
                                        message: errorMessage,
                                        icon: "k-Ext-information"
                                    });
                                }
                            }

                        });
                    }
                });
            };
            function showDetails(e)
            {
                e.preventDefault();
                windowAdmin.content(detailsTemplate);
                var grid = $("#userGrid").data("kendoGrid");
                var dataItem = grid.dataItem(grid.select());
                kendo.bind($('#reviewDetailContent'), dataItem);
                $("#userDetail-Popup").data("kendoWindow").refresh();
                $("#ButtonRejectUser").on("click", function (e)
                {

                    RejectRecord();
                    $(this).closest("[data-role=window]").kendoWindow("close");

                });

                $("#ButtonStRegUser").on("click", function (e)
                {
                    StartRegProcess();
                });
            };

            function cancelReg()
            {
                kendo.ui.ExtOkCancelDialog.show({
                    message: labels.newReg_cancelRegistration,
                    width: '400px',
                    height: '150px',
                    icon: "k-Ext-warning"
                }).done(function (response)
                {
                    if (response.button === 'OK')
                    {
                        $("#userDetail-Popup").data("kendoWindow").close();
                        $("#assignOrSelectOrganization-Popup").data("kendoWindow").close();
                    }

                });
            }
            $("#userDetail-Popup").data("kendoWindow").wrapper.find(".k-i-cancel").click(function (e)
            {
                cancelReg();
            });
            $("#assignOrSelectOrganization-Popup").data("kendoWindow").wrapper.find(".k-i-cancel").click(function (e)
            {
                cancelReg();
            });

        },

    };
});

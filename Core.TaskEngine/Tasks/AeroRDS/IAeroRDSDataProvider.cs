﻿using NavCanada.Core.Proxies.AeroRdsProxy.Model;
using System;
using System.Collections.Generic;

namespace NavCanada.Core.TaskEngine.Tasks.AeroRDS
{
    public interface IAeroRDSDataProvider
    {
        IEnumerable<LoadRecord> GetLoadRecords();
        IEnumerable<Airspace> GetAirspaces();
        IEnumerable<Aerodrome> GetAerodromes();
        IEnumerable<Runway> GetRunways();
        IEnumerable<RunwayDirection> GetRunwayDirections();
        IEnumerable<RunwayDirectionDeclaredDistance> GetRunwayDirectionDeclaredDistances();
        IEnumerable<Taxiway> GetTaxiways();
        IEnumerable<ServiceUnit> GetServiceUnits();
        IEnumerable<Fix> GetFixes(IEnumerable<string> airspaces);
        DateTime GetActiveDate();
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;


namespace NAVTAM.ViewModels
{
    public class MessageTemplateViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Body { get; set; }
        [Required]
        public string Addresses { get; set; }
        public DateTime Created { get; set; }
    }
}
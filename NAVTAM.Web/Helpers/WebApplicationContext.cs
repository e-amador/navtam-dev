﻿using Microsoft.AspNet.Identity;
using NavCanada.Core.Common.Common;
using NAVTAM.App_Start;
using System;
using System.Configuration;
using System.Threading.Tasks;
using System.Web;

namespace NAVTAM.Helpers
{
    public class WebApplicationContext : IApplicationContext
    {
        public WebApplicationContext(HttpContextBase httpContext, ApplicationUserManager userManager)
        {
            _httpContext = httpContext;
            _userManager = userManager;
        }

        public string GetCookie(string cookieName, string defaultValue = "")
        {
            var cookie = _httpContext.Request.Cookies[cookieName];
            return cookie != null ? cookie.Value : defaultValue;
        }

        public void SetCookie(string name, string value, int ttl = 30)
        {
            _httpContext.Response.Cookies.Add(new HttpCookie(name, value) { HttpOnly= true, Secure=false, Expires = DateTime.Now.AddMinutes(ttl) });
        }

        public Task<bool> IsUserInRole(string userId, string role)
        {
            return _userManager.IsInRoleAsync(userId, role);
        }

        public string GetUserId()
        {
            return _httpContext.User.Identity.GetUserId();
        }

        public string GetUserName()
        {
            return _httpContext.User.Identity.GetUserName();
        }

        public string GetConfigValue(string name, string defaultValue)
        {
            return ConfigurationManager.AppSettings[name] ?? defaultValue;
        }

        public void SetResponseStatusCode(int code)
        {
            _httpContext.Response.StatusCode = code;
        }

		public bool IsValidUserRequest(string userIdCookie)
        {
            var userId = GetCookie(userIdCookie, string.Empty);
            return !string.IsNullOrEmpty(userId) && userId.Equals(GetUserId());
        }

        readonly HttpContextBase _httpContext;
        readonly ApplicationUserManager _userManager;
    }
}
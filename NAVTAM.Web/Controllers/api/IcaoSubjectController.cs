﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using NAVTAM.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using NavCanada.Core.Common.Contracts;

namespace NAVTAM.Controllers.api
{
    [RoutePrefix("api/icaosubjects")]
    [Authorize]
    public class IcaoSubjectController : ApiBaseController
    {
        public IcaoSubjectController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger) 
            : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
        }

        [HttpGet]
        [Route("Model")]
        public async Task<IHttpActionResult> GetModel(int id)
        {
            return await LogAction(async () => Ok(await Uow.IcaoSubjectRepo.GetModelAsync(id)), $"api/icaosubjects/model/{id}");
        }

        [HttpGet]
        [Route("Filter")]
        public async Task<IHttpActionResult> Filter(string code)
        {
            return await LogAction(async () =>
            {
                var upperCode = code.ToUpper();
                var culture = ApplicationContext.GetCookie("_culture", "en");

                List<IcaoSubjectDto> subjects = null;
                switch (upperCode)
                {
                    case "AHP":
                        subjects = await Uow.IcaoSubjectRepo.GetAhpSubjectsAsync(culture);
                        break;
                    case "FIR":
                        subjects = await Uow.IcaoSubjectRepo.GetFirSubjectsAsync(culture);
                        break;
                    default:
                        subjects = await Uow.IcaoSubjectRepo.GetSubjectsAsync(culture);
                        break;
                }

                return Ok(subjects);

            }, $"api/icaosubjects/filter/{code}");
        }

        [HttpGet]
        [Route("Codes")]
        public async Task<IHttpActionResult> GetCodes()
        {
            return await LogAction(async () =>
            {
                return Ok(await Uow.IcaoSubjectRepo.GetAllDistinctCodesAsync());
            }, "api/icaosubjects/codes");
        }

        [HttpPost, Route("page", Name = "RegisteredIcaoSubjectsPage")]
        [Authorize(Roles = "NOF_ADMINISTRATOR, Administrator")]
        public async Task<IHttpActionResult> GetIcaoSubjectsInPage([FromBody]QueryOptions queryOptions)
        {
            return await LogAction(async () =>
            {
                if (queryOptions.PageSize > MaxRecordsToReturn)
                    queryOptions.PageSize = MaxRecordsToReturn;

                var totalCount = await Uow.IcaoSubjectRepo.GetAllCountAsync(queryOptions);
                var icaoSubjects = await Uow.IcaoSubjectRepo.GetAllAsync(queryOptions);
                if (icaoSubjects.Count == 0 && queryOptions.Page > 1)
                {
                    queryOptions.Page = queryOptions.Page - 1;
                    icaoSubjects = await Uow.IcaoSubjectRepo.GetAllAsync(queryOptions);
                }

                return Ok(ResponsewithPagingEnvelope(queryOptions, "RegisteredIcaoSubjectsPage", icaoSubjects, totalCount));

            }, "api/icaosubjects/page");

        }

        [HttpPost, Route("create")]
        [Authorize(Roles = "NOF_ADMINISTRATOR, Administrator")]
        public async Task<IHttpActionResult> Create([FromBody]IcaoSubjectViewModel vm)
        {
            return await LogAction(async () =>
            {
                if (!ModelState.IsValid)
                    return BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid);

                var icaoSubject = await Uow.IcaoSubjectRepo.GetByIdAsync(vm.Id);
                if (icaoSubject == null)
                {
                    icaoSubject = new NavCanada.Core.Domain.Model.Entitities.IcaoSubject
                    {
                        Name = vm.Name,
                        NameFrench = vm.NameFrench,
                        Scope = vm.Scope,
                        Code = vm.Code,
                        EntityCode = vm.EntityCode,
                        AllowItemAChange = vm.RequiresItemA,
                        AllowScopeChange = vm.RequiresScope,
                        AllowSeriesChange = vm.RequiresSeries
                    };

                    Uow.IcaoSubjectRepo.Add(icaoSubject);

                    await Uow.SaveChangesAsync();

                    Logger.LogInfo(GetType(), $"ADD: {SummarizeIcaoSubject(icaoSubject)} created  by '{ApplicationContext.GetUserName()}'.");
                }

                return Ok(vm);

            }, "api/icaosubjects/create");
        }

        [HttpPut, Route("update")]
        [Authorize(Roles = "NOF_ADMINISTRATOR, Administrator")]
        public async Task<IHttpActionResult> Update([FromBody]IcaoSubjectViewModel vm)
        {
            return await LogAction(async () =>
            {
                if (!ModelState.IsValid)
                    return BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid);

                var icaoSubject = await Uow.IcaoSubjectRepo.GetByIdAsync(vm.Id);
                if (icaoSubject != null)
                {
                    var beforeSummary = SummarizeIcaoSubject(icaoSubject);

                    icaoSubject.Name = vm.Name;
                    icaoSubject.NameFrench = vm.NameFrench;
                    icaoSubject.Scope = vm.Scope;
                    icaoSubject.Code = vm.Code;
                    icaoSubject.EntityCode = vm.EntityCode;
                    icaoSubject.AllowItemAChange = vm.RequiresItemA;
                    icaoSubject.AllowScopeChange = vm.RequiresScope;
                    icaoSubject.AllowSeriesChange = vm.RequiresSeries;

                    Uow.IcaoSubjectRepo.Update(icaoSubject);

                    await Uow.SaveChangesAsync();

                    var afterSummary = SummarizeIcaoSubject(icaoSubject);

                    Logger.LogInfo(GetType(), $"UPDATE: {beforeSummary} updated to {afterSummary}  by '{ApplicationContext.GetUserName()}'.");
                }

                return Ok(vm);

            }, "api/icaosubjects/update");
        }

        [HttpDelete, Route("delete/{id}")]
        [Authorize(Roles = "NOF_ADMINISTRATOR, Administrator")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            return await LogTransaction(async () =>
            {
                var icaoSubject = await Uow.IcaoSubjectRepo.GetByIdAsync(id);

                if (icaoSubject == null)
                    return ApiStepResult.Fail(NotFound());

                //Uow.BeginTransaction();
                try
                {
                    var icaoConditions = await Uow.IcaoSubjectConditionRepo.GetBySubjectIdAsync(icaoSubject.Id);
                    if (icaoConditions != null)
                    {
                        foreach (var condition in icaoConditions)
                        {
                            Uow.IcaoSubjectConditionRepo.Delete(condition);
                        }
                        await Uow.SaveChangesAsync();
                    }

                    Uow.IcaoSubjectRepo.Delete(icaoSubject);
                    await Uow.SaveChangesAsync();

                    //Uow.Commit();

                    Logger.LogInfo(GetType(), $"DELETE: {SummarizeIcaoSubject(icaoSubject)} deleted  by '{ApplicationContext.GetUserName()}'.");

                    return ApiStepResult.Succeed(Ok());
                }
                catch (Exception e)
                {
                    //Uow.Rollback();
                    return ApiStepResult.Fail(InternalServerError(e));
                }

            }, $"api/icaosubjects/delete/{id}");
        }

        static string SummarizeIcaoSubject(IcaoSubject s)
        {
            return $"ICAOSubject(Name: '{s.Name} / {s.NameFrench}', Code23: '{s.Code}', Scope: '{s.Scope}', Entity: '{s.EntityCode}', ItemA: {s.AllowItemAChange}, Scope: {s.AllowScopeChange}, Series: {s.AllowSeriesChange})";
        }
    }
}
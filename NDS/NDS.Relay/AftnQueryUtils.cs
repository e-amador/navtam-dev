﻿using Business.Common;
using System;
using System.Linq;
using System.Collections.Generic;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Dtos;

namespace NDS.Relay
{
    public static class AftnQueryUtils
    {
        const int MaxRqnReplies = 100;
        
        /// <summary>
        /// Parses an AFTN RQN query.
        /// </summary>
        /// <param name="query">Query expects this format: A1234/17-A1234/17 A1234/17 A1234/17-A1234/17</param>
        /// <returns></returns>
        public static IEnumerable<AftnQueryRange> ParseRqnQuery(string query)
        {
            var queryParts = (query ?? string.Empty).Split(new char[] { ' ', '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var queryPart in queryParts)
            {
                string series;
                int number, year;
                if (ICAOTextUtils.TryParseNotamId(queryPart, out series, out number, out year))
                {
                    yield return new AftnQueryRange(series, number, number, year);
                }
                else
                {
                    yield return ParseRqnQueryRange(queryPart);
                }
            }
        }

        public static bool IsValid(this AftnQueryRange range)
        {
            return range != null && range.FromNumber <= range.ToNumber;
        }

        public static bool AreValid(this List<AftnQueryRange> queries)
        {
            return queries != null && queries.Count > 0 && queries.All(q => q?.IsValid() == true);
        }

        /// <summary>
        /// Calculate total notams (expects sinitized ranges).
        /// </summary>
        /// <param name="ranges">Should be sanitized ranges.</param>
        /// <returns></returns>
        public static int CalculateTotalNotams(this IEnumerable<AftnQueryRange> ranges)
        {
            return ranges.Sum(r => r.ToNumber - r.FromNumber + 1);
        }

        /// <summary>
        /// Search all the ranges and return an enumerable of results to convert to messages
        /// </summary>
        /// <param name="queryRanges">Should be sanitized ranges.</param>
        /// <param name="searchLambda"></param>
        /// <returns></returns>
        public static IEnumerable<AftnRqnResult> ExecuteRqnQueries(List<AftnQueryRange> queryRanges, Func<AftnQueryRange, IEnumerable<RqnNotamResultDto>> searchLambda)
        {
            var totalNotams = queryRanges.CalculateTotalNotams();
            if (totalNotams <= MaxRqnReplies)
            {
                // collect missing notam numbers
                var missingNotams = new List<string>();

                // search all ranges
                foreach (var range in queryRanges)
                {
                    var series = range.Series;
                    var year = range.Year;

                    // collect expected numbers
                    var numberSet = new HashSet<int>(EnumerateNotamNumbers(range));

                    var rangeResults = searchLambda.Invoke(range);
                    foreach (var result in rangeResults)
                    {
                        var number = result.Notam.Number;
                        if (numberSet.Contains(number))
                        {
                            yield return AftnRqnResult.CreateNotamResult(GetRqnResultType(result), result.Notam, result.ReplaceNotamId);
                            numberSet.Remove(number);
                        }
                    }

                    // save missing numbers
                    var missingRangeNumbers = numberSet.OrderBy(n => n).Select(number => ICAOTextUtils.FormatNotamId(series, number, year));
                    missingNotams.AddRange(missingRangeNumbers);
                }

                // return all the missing notams
                if (missingNotams.Count > 0)
                    yield return AftnRqnResult.CreateMissingNotamsResult(missingNotams);
            }
            else
            {
                // tell the limit has been reached
                yield return AftnRqnResult.CreateLimitReachedResult();
            }
        }

        static AftnRqnResultType GetRqnResultType(RqnNotamResultDto notamResult)
        {
            if (notamResult.Notam == null)
                return AftnRqnResultType.Missing;

            if (notamResult.Notam.IsObsolete())
                return AftnRqnResultType.Obsolete;

            if (notamResult.ReplaceNotamType == NotamType.C)
                return AftnRqnResultType.Canceled;

            if (notamResult.ReplaceNotamType == NotamType.R)
                return AftnRqnResultType.Replaced;

            return notamResult.Notam.IsExpired() ? AftnRqnResultType.Expired : AftnRqnResultType.Active;
        }

        static bool IsExpired(this Notam notam)
        {
            return notam != null && notam.EndValidity.HasValue && !notam.Estimated && notam.EndValidity < DateTime.UtcNow;
        }

        static bool IsObsolete(this Notam notam)
        {
            // a notam is obsolete when EffectiveEndValidity older than 3 months or Notam expired more than 3 month ago.
            return notam != null && 
                (notam.EffectiveEndValidity.HasValue && notam.EffectiveEndValidity.Value.AddMonths(3) < DateTime.UtcNow || 
                notam.IsExpired() && notam.EndValidity.Value.AddMonths(3) < DateTime.UtcNow);
        }

        static IEnumerable<int> EnumerateNotamNumbers(AftnQueryRange range) => Enumerable.Range(range.FromNumber, range.ToNumber - range.FromNumber + 1);

        static AftnQueryRange ParseRqnQueryRange(string queryPart)
        {
            var notamIds = queryPart.Split('-');
            if (notamIds.Length == 2)
            {
                string fromSeries, toSeries;
                int fromNumber, fromYear;
                int toNumber, toYear;
                if (ICAOTextUtils.TryParseNotamId(notamIds[0], out fromSeries, out fromNumber, out fromYear) && 
                    ICAOTextUtils.TryParseNotamId(notamIds[1], out toSeries, out toNumber, out toYear) && 
                    fromSeries == toSeries && fromYear == toYear && fromNumber <= toNumber)
                {
                    return new AftnQueryRange(fromSeries, fromNumber, toNumber, fromYear);
                }
            }
            return null;
        }
    }
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SidebarComponent = void 0;
var core_1 = require("@angular/core");
var angular_l10n_1 = require("angular-l10n");
var windowRef_service_1 = require("../common/windowRef.service");
var SidebarComponent = /** @class */ (function () {
    function SidebarComponent(locale, translation, winRef) {
        this.locale = locale;
        this.translation = translation;
        this.winRef = winRef;
        this.txtNavigationHeader = "Navigation";
        this.txtUserManagment = "User Managment";
        this.txtOrgManagment = "Organizations";
        this.txtIcaoSubCond = "ICAO Subject & Conditions";
        this.txtSeriesManagement = "Serie Allocation Management";
        this.txtADCManagement = "Dissemination Category";
        this.txtDoasManagement = "Domain of Authority";
        this.txtNsdsManagement = "Notam Scenario Definitions";
        this.txtBilingualManagement = "Bilingual Region";
        this.txtNorthernManagement = "Northern Region";
        this.txtGeoRegionManagement = "Geographic Region";
        this.txtConfigManagement = "Configuration";
        this.txtCheckList = "Checklist";
        this.txtAeroRDSSync = "AeroRDS Sync";
        this.txtSubscriptionManagement = "Client Subscriptions";
    }
    SidebarComponent.prototype.ngOnInit = function () {
        var self = this;
        window.setTimeout(function () {
            self.setLabels();
        }, 1000);
    };
    SidebarComponent.prototype.isSysAdmin = function () {
        var appConfig = this.winRef.appConfig;
        return appConfig && appConfig.sysAdmin;
    };
    SidebarComponent.prototype.isCCS = function () {
        var appConfig = this.winRef.appConfig;
        return appConfig && appConfig.ccs;
    };
    SidebarComponent.prototype.isNofAdmin = function () {
        var appConfig = this.winRef.appConfig;
        return appConfig && appConfig.nofAdmin;
    };
    SidebarComponent.prototype.isOrgAdmin = function () {
        var appConfig = this.winRef.appConfig;
        return appConfig && appConfig.orgAdmin;
    };
    SidebarComponent.prototype.setLabels = function () {
        this.txtNavigationHeader = this.translation.translate('LeftNavbar.NavHeader');
        this.txtUserManagment = this.translation.translate('LeftNavbar.UserManagment');
        this.txtOrgManagment = this.translation.translate('LeftNavbar.OrgManagment');
        this.txtIcaoSubCond = this.translation.translate('LeftNavbar.IcaoSubCond');
        this.txtSeriesManagement = this.translation.translate('LeftNavbar.SeriesAllocation');
        this.txtADCManagement = this.translation.translate('LeftNavbar.AerodromeDisseminationCategory');
        this.txtDoasManagement = this.translation.translate('LeftNavbar.DomainOfAuthority');
        this.txtNsdsManagement = this.translation.translate('LeftNavbar.NSD');
        this.txtBilingualManagement = this.translation.translate('LeftNavbar.BilingualRegion');
        this.txtNorthernManagement = this.translation.translate('LeftNavbar.NorthernRegion');
        this.txtGeoRegionManagement = this.translation.translate('LeftNavbar.GeoRegion');
        this.txtConfigManagement = this.translation.translate('LeftNavbar.Config');
        this.txtCheckList = this.translation.translate('LeftNavbar.CheckList');
        this.txtAeroRDSSync = this.translation.translate('LeftNavbar.AeroRDS');
        this.txtSubscriptionManagement = this.translation.translate('LeftNavbar.Subscriptions');
    };
    SidebarComponent = __decorate([
        core_1.Component({
            selector: 'sidebar',
            templateUrl: '/app/admin/sidebar.component.html'
        }),
        __metadata("design:paramtypes", [angular_l10n_1.LocaleService, angular_l10n_1.TranslationService, windowRef_service_1.WindowRef])
    ], SidebarComponent);
    return SidebarComponent;
}());
exports.SidebarComponent = SidebarComponent;
//# sourceMappingURL=sidebar.component.js.map
﻿using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System;
using System.Data.Entity.Spatial;

namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    public class UserDoaRepo : Repository<UserDoa>, IUserDoaRepo
    {
        public UserDoaRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public Task<List<UserDoa>> GetDoasByUserAsync(string userId)
        {
            return DbSet.Where(d => d.UserId == userId).ToListAsync();
        }

        public async Task AdjustUserDoas(int orgId, IEnumerable<int> doaIds)
        {
            var doaSet = new HashSet<int>(doaIds);
            var userDoas = await DbSet.Where(e => e.OrganizationId == orgId).ToListAsync();
            foreach (var ud in userDoas.Where(e => !doaSet.Contains(e.DoaId)))
            {
                Delete(ud);
            }
        }

        public async Task<DbGeography> JoinUserDoas(string userId)
        {
            var doas = await DbSet.Include(e => e.Doa)
                                  .Where(e => e.UserId == userId)
                                  .Select(e => e.Doa.DoaGeoArea)
                                  .ToListAsync();

            DbGeography union = null;

            foreach (var doa in doas)
            {
                union = union == null ? doa : union.Union(doa);
            }

            return union;
        }
    }
}

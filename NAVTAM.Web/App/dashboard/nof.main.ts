﻿import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { NofModule } from './nof.module';
import { enableProdMode } from '@angular/core';

const platform = platformBrowserDynamic();
enableProdMode();
platform.bootstrapModule(NofModule);
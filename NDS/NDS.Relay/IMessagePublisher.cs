﻿using NavCanada.Core.Domain.Model.Entitities;
using System;

namespace NDS.Relay
{
    public interface IMessagePublisher
    {
        void PublishHubMessage(NdsOutgoingMessage message);
        void QueueNotamForDistribution(Guid notamId);
        void QueueMessageGroupForPublishing(Guid groupId);
    }
}

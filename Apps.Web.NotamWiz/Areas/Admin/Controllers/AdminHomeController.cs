﻿namespace NavCanada.Applications.NotamWiz.Web.Areas.Admin.Controllers
{
    using System.Threading.Tasks;
    using System.Web.Mvc;

    using Microsoft.AspNet.Identity;

    using NavCanada.Applications.NotamWiz.Web.Controllers;
    using NavCanada.Core.Common.Common;
    using NavCanada.Core.Common.Contracts;
    using NavCanada.Core.Data.NotamWiz.Contracts;

    [Authorize(Roles = "Administrator, Organization Administrator, CCS")]
    public class AdminHomeController : BaseController
    {
        public AdminHomeController(IUow uow, IClusterQueues clusterQueues) 
            : base(uow, clusterQueues)
        {
        }

        //public AdminHomeController(IUow uow, AppUserManager userManager, AppRoleManager roleManager)
        //    : base(uow, userManager, roleManager)
        //{
        //}

        public async Task<ActionResult> Index()
        {

            string Id = HttpContext.User.Identity.GetUserId();

            if (await IsUserInRoleAsync(Id, CommonDefinitions.CcsRole))
            {
                ViewBag.CCSRole = true;
            }
            if (await IsUserInRoleAsync(Id, CommonDefinitions.OrgAdminRole))
            {
                ViewBag.OrgAdminRole = true;
            }

            var currentAdmin = UserManager.FindById(User.Identity.GetUserId());
            if (currentAdmin != null)
            {
                return View();
            }

            // if no user is logged in
            TempData["UserMessage"] = "You are not an admin and do not have " +
                "access to this page.";
            return RedirectToRoute(new { controller = "Account", action = "Login", area = "" });
        }
    }
}
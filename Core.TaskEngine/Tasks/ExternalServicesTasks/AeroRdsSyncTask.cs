﻿namespace NavCanada.Core.TaskEngine.Tasks.ExternalServicesTasks
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Spatial;
    using System.Linq;

    using log4net;

    using Data.NotamWiz.Contracts;
    using Domain.Model.Entitities;
    using Domain.Model.Enums;
    using Proxies.AeroRdsProxy.Default;
    using Proxies.AeroRdsProxy.Extentions;
    using Proxies.AeroRdsProxy.GeoTools;
    using Proxies.AeroRdsProxy.SDOData.Models.SDOEntities;
    using Contracts;

    using Newtonsoft.Json;

    public class AeroRdsSyncTask : IMessageTask
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(AeroRdsSyncTask));

        public void Execute(IMessageTaskContext context)
        {
            Logger.Debug("AeroRdsSyncTask: Task start excecuting...");
            var uow = context.ClusterStorage.Uow;
            var scheduleTaskId = string.Format("DbSync:{0}", DateTime.UtcNow.ToShortDateString());
            var scheduleTask = uow.ScheduleTaskRepo.GetById(scheduleTaskId);
            if (scheduleTask != null && scheduleTask.IsProcessed)
            {
                //this avoid a raise condition when TE start and a DbSyncSchedule was already pushed to the queue.
                Logger.Info("AeroRdsSyncTask task is aborted. The task was already processed today.");
                return;
            }
            
            if (context.ExternalServices.AeroRdsProxy == null)
            {
                Logger.Error("The AeroRdsProxy was not properly configured and the task will be aborted");
                return;
            }
            if (context.ExternalServices.AeroRdsProxy.ODataDataContext == null)
            {
                Logger.Error("The AeroRdsProxy was not properly configured and the task will be aborted");
                return;
            }
            var cvsdoLastUpdateTime = uow.ConfigurationValueRepo.GetByCategoryandName("NOTAMWizWorker", "SDOLastUpdateTime");
            var airncEffectiveDate = GetEffectiveAirnc();
            if (!string.IsNullOrEmpty(cvsdoLastUpdateTime.Value) && DateTime.Parse(cvsdoLastUpdateTime.Value) >= airncEffectiveDate)
            {
                Logger.Debug("AeroRdsSyncTask- No need for update. SDO subjects are up to date.");
                return;
            }
            var updateTime = airncEffectiveDate.ToString("MM/dd/yyyy H:mm:ss zzz");
            Logger.Debug("AeroRdsSyncTask - Updating SDO Subjects...");
            Update(uow, airncEffectiveDate, context);
            cvsdoLastUpdateTime.Value = updateTime;
            uow.ConfigurationValueRepo.Update(cvsdoLastUpdateTime);
            SetScheduleTaskFlagToExcecuted(uow);
            Logger.Debug("AeroRdsSyncTask - Task finish executing: SDO Subjects updated successfully to AIRNC Publication date at " + updateTime);
        }

        private double ConvertToMeter(string val, string unit)
        {
            double unitToMeter = 1;
            switch (unit)
            {
                case "NM":
                    unitToMeter = 1852;
                    break;
                case "M":
                    unitToMeter = 1;
                    break;
                case "KM":
                    unitToMeter = 1000;
                    break;
                case "FT":
                    unitToMeter = 0.3048;
                    break;
            }
            return double.Parse(val) * unitToMeter;
        }

        private DbGeography GetAdGeo(Ahp ahp, Dictionary<string, DbGeography> rwyGeoCash)
        {
            var items = new List<DbGeography>();
            items.Add(ahp.GEOLocation.ToEntityDbGeography().Buffer(10));
            foreach (var r in ahp.Rwy)
            {
                items.Add(GetRwyGeo(ahp, r, rwyGeoCash));
            }
            var collectionGeo = "GEOMETRYCOLLECTION(\n";
            collectionGeo += string.Join("", items.Select(a => a.WellKnownValue.WellKnownText + ",\n").ToArray());
            collectionGeo = collectionGeo.Remove(collectionGeo.LastIndexOf(",\n", StringComparison.Ordinal)) + ")";
            return DbGeography.GeographyCollectionFromText(collectionGeo, GeoDataService.SRID);
        }

        private int GetIntConfigurationSetting(IUow uow, string name, int defaultValue)
        {
            var result = defaultValue;
            var cv = uow.ConfigurationValueRepo.GetByCategoryandName("NOTAMWizWorker", name);
            if (cv != null)
            {
                if (!int.TryParse(cv.Value, out result))
                {
                    Logger.Warn(string.Format("Invalid Configuration value for {0}. Value provided is {1}. Value should be positive integer.", name, cv.Value ?? "NULL"));
                }
            }
            else
            {
                Logger.Warn(string.Format("Missing Configuration value for {0}.", name));
            }
            return result;
        }

        private string GetConfigurationSetting(IUow uow, string name, string defaultValue)
        {
            var result = defaultValue;
            var cv = uow.ConfigurationValueRepo.GetByCategoryandName("NOTAMWizWorker", name);
            if (cv != null)
            {
                result = cv.Value;
            }
            else
            {
                Logger.Warn(string.Format("Missing Configuration value for {0}.", name));
            }
            return result;
        }

        private DateTime GetEffectiveAirnc()
        {
            var result = new DateTime(2014, 2, 6);
            var tmp = result;
            while (tmp <= DateTime.UtcNow)
            {
                result = tmp;
                tmp = tmp.AddDays(56);
            }
            return result;
        }

        private DbGeography GetRwyGeo(Ahp ahp, Rwy rwy, Dictionary<string, DbGeography> rwyGeoCash)
        {
            try
            {
                if (!rwyGeoCash.Keys.Contains(rwy.Mid))
                {
                    var rdn = rwy.Rdn.ToList();
                    if (rdn.Count() != 2 || rdn[0].GEOLocation == null || rdn[1].GEOLocation == null)
                    {
                        rwyGeoCash[rwy.Mid] = ahp.GEOLocation.ToEntityDbGeography().Buffer(10);
                    }
                    else
                    {
                        var longitude = rdn.First().GEOLocation.ToEntityDbGeography().Longitude;
                        if (longitude != null)
                        {
                            var lon1 = (double)longitude;
                            var latitude = rdn.First().GEOLocation.ToEntityDbGeography().Latitude;
                            if (latitude != null)
                            {
                                var lat1 = (double)latitude;
                                var d = rdn.Last().GEOLocation.ToEntityDbGeography().Longitude;
                                if (d != null)
                                {
                                    var lon2 = (double)d;
                                    var latitude1 = rdn.Last().GEOLocation.ToEntityDbGeography().Latitude;
                                    if (latitude1 != null)
                                    {
                                        var lat2 = (double)latitude1;
                                        var rwyWidth = ConvertToMeter(rwy.ValWid, rwy.UomDimRwy);
                                        rwyGeoCash[rwy.Mid] = DbGeography.FromText(string.Format("LINESTRING ({0} {1}, {2} {3})", lon1, lat1, lon2, lat2), GeoDataService.SRID).Buffer(rwyWidth);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(string.Format("Defaulting Rwy {0}geo in AD {1} to AD Geo", rwy.TxtDesig, ahp.CodeId), e);
                rwyGeoCash[rwy.Mid] = ahp.GEOLocation.ToEntityDbGeography().Buffer(10);
            }
            return rwyGeoCash[rwy.Mid];
        }

        private DbGeography GetTwyGeo(Ahp ahp)
        {
            var items = new List<DbGeography>();
            items.Add(ahp.GEOLocation.ToEntityDbGeography().Buffer(10));
            var collectionGeo = "GEOMETRYCOLLECTION(\n";
            collectionGeo += string.Join("", items.Select(a => a.WellKnownValue.WellKnownText + ",\n").ToArray());
            collectionGeo = collectionGeo.Remove(collectionGeo.LastIndexOf(",\n", StringComparison.Ordinal)) + ")";
            return DbGeography.GeographyCollectionFromText(collectionGeo, GeoDataService.SRID);
        }

        private void RemoveDeleted(IUow uow, DateTime lastUpdateDate)
        {
            try
            {
                Logger.Debug("Marking Deleted SDO Subjects started.");
                var deleted = uow.SdoSubjectRepo.GetDeleted(lastUpdateDate);
                foreach (var del in deleted)
                {
                    del.IsDeleted = true;
                    del.Mid = string.Format("{0} Deleted On:{1}", del.Mid, DateTime.UtcNow.ToUniversalTime());
                }
                uow.SaveChanges( );
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
            finally
            {
                Logger.Debug("Marking Deleted SDO Subjects finished");
            }
        }

        private static void SetScheduleTaskFlagToExcecuted(IUow uow)
        {
            try
            {
                var scheduleTaskId = string.Format("DbSync:{0}", DateTime.UtcNow.ToShortDateString());
                var scheduleTask = uow.ScheduleTaskRepo.GetById(scheduleTaskId);
                if (scheduleTask != null)
                {
                    scheduleTask.IsProcessed = true;
                    scheduleTask.ProcessedDate = DateTime.UtcNow;
                }
                uow.ScheduleTaskRepo.Update(scheduleTask);
                uow.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error("Error updating the AeroRds task flag", e);
                throw;
            }
        }

        private void Update(IUow uow, DateTime updateDate,IMessageTaskContext context)
        {
            var container = context.ExternalServices.AeroRdsProxy.ODataDataContext as Container;
            var globalContainer = context.ExternalServices.AeroRdsProxy.ODataGlobalDataContext as Container;

            var rwyGeoCash = new Dictionary<string, DbGeography>();
            UpdateFirs(uow, updateDate, container);
            UpdateAhps(uow, updateDate, rwyGeoCash, container);
            UpdateGlobalAhps(uow, updateDate, rwyGeoCash, globalContainer);
            UpdateUnitedStatesNavAids(uow, updateDate, globalContainer);
            RemoveDeleted(uow, updateDate);

            UpdateGeoRegions(uow, container);
        }

        private void UpdateGeoRegions(IUow uow, Container container)
        {
            Logger.Debug("Geo Regions updater started.");

            // get Southern and Northern regions
            var regions = container.Ase.Where(a => a.CodeId == "CCDANORTH" || a.CodeId == "CCDASOUTH");

            if (regions.Count() != 2)
            {
                Logger.Warn("Could not load Northern and Southern regions from SDO");
                return;
            }

            foreach (Ase r in regions)
            {
                var geo = container.GeoInstanceCache.Where(a => a.EntityType == "Ase" && a.Mid == r.Mid).FirstOrDefault();
                if (geo == null)
                {
                    Logger.Warn("Could not load Geo info for SDO Ase " + r.CodeId);
                    continue;
                }

                var regionName = r.CodeId == "CCDANORTH" ? "Northern Region" : "Southern Region";
                var notamRegion = uow.GeoRegionRepo.GetByName(regionName);
                if (notamRegion == null)
                {
                    uow.GeoRegionRepo.Add(new GeoRegion()
                    {
                        Name = regionName,
                        Geo = geo.GeoInstance.ToEntityDbGeography()
                    });
                }
                else
                {
                    notamRegion.Geo = geo.GeoInstance.ToEntityDbGeography();
                }

            }

            uow.SaveChanges();

            Logger.Debug("Geo Regions updater Finished.");
        }

        private void UpdateUnitedStatesNavAids(IUow uow, DateTime lastUpdateDate, Container container)
        {
            // load each America Nav aid  under nearst Fir
            var unitatedStateOrg = container.CreateQuery<Org>("Org")
                .AddQueryOption("$filter","txtName eq 'UNITED STATES OF AMERICA'")
                .AddQueryOption("$select", "mid");
            if (unitatedStateOrg == null || unitatedStateOrg.Count() == 0 )
            {
                Logger.Warn("Could not load United State of America  org record");
                return;
            }

            var firs = uow.SdoSubjectRepo.GetFirs();
            if (!firs.Any())
                return;

            var batchSize = GetIntConfigurationSetting(uow, "AhpUpdaterBatchSize", 10);
            var recordNo = 0;
            // vor loop
            while (true)
            {
                var vors = container.CreateQuery<Vor>("Vor")
                    .AddQueryOption("$filter","FK_OrgUid eq '"+unitatedStateOrg.First().Mid+ "'")
                    .AddQueryOption("$expand", "Dme,Tcn")
                    .AddQueryOption("$skip", recordNo.ToString()).AddQueryOption("$top", batchSize.ToString()).ToList();
                recordNo += batchSize;
                if (vors.Count() == 0)
                {
                    break;
                }

                foreach (var vor in vors)
                {
                    // find nearest Fir
                    var vorLocation = vor.GEOLocation.ToEntityDbGeography();
                    var fir = firs.Aggregate((i1, i2) => i1.SubjectGeoRefPoint.Distance(vorLocation) < i2.SubjectGeoRefPoint.Distance(vorLocation) ? i1 : i2);
                    UpdateVors(uow, vor,fir, fir.Designator, lastUpdateDate, container);                                    
                }
            }

            // ndb loop
            while (true)
            {
                var ndbs = container.CreateQuery<Ndb>("Ndb")
                    .AddQueryOption("$filter", "FK_OrgUid eq '" + unitatedStateOrg.First().Mid + "'")
                    .AddQueryOption("$skip", recordNo.ToString()).AddQueryOption("$top", batchSize.ToString()).ToList();
                recordNo += batchSize;
                if (ndbs.Count() == 0)
                {
                    break;
                }

                foreach (var ndb in ndbs)
                {
                    // find nearest Fir
                    var vorLocation = ndb.GEOLocation.ToEntityDbGeography();
                    var fir = firs.Aggregate((i1, i2) => i1.SubjectGeoRefPoint.Distance(vorLocation) < i2.SubjectGeoRefPoint.Distance(vorLocation) ? i1 : i2);
                    UpdateNdbs(uow, ndb, fir, fir.Designator, lastUpdateDate, container);
                }
            }

            // Dme loop
            while (true)
            {
                var dmes = container.CreateQuery<Dme>("Dme")
                    .AddQueryOption("$filter", "FK_OrgUid eq '" + unitatedStateOrg.First().Mid + "'")
                    .AddQueryOption("$skip", recordNo.ToString()).AddQueryOption("$top", batchSize.ToString()).ToList();
                recordNo += batchSize;
                if (dmes.Count() == 0)
                {
                    break;
                }

                foreach (var dme in dmes)
                {
                    // find nearest Fir
                    var vorLocation = dme.GEOLocation.ToEntityDbGeography();
                    var fir = firs.Aggregate((i1, i2) => i1.SubjectGeoRefPoint.Distance(vorLocation) < i2.SubjectGeoRefPoint.Distance(vorLocation) ? i1 : i2);
                    UpdateDmes(uow, dme, fir, fir.Designator, lastUpdateDate, container);
                }
            }

            // Tcn loop
            while (true)
            {
                var tcns = container.CreateQuery<Tcn>("Tcn")
                    .AddQueryOption("$filter", "FK_OrgUid eq '" + unitatedStateOrg.First().Mid + "'")
                    .AddQueryOption("$skip", recordNo.ToString()).AddQueryOption("$top", batchSize.ToString()).ToList();
                recordNo += batchSize;
                if (tcns.Count() == 0)
                {
                    break;
                }

                foreach (var tcn in tcns)
                {
                    // find nearest Fir
                    var vorLocation = tcn.GEOLocation.ToEntityDbGeography();
                    var fir = firs.Aggregate((i1, i2) => i1.SubjectGeoRefPoint.Distance(vorLocation) < i2.SubjectGeoRefPoint.Distance(vorLocation) ? i1 : i2);
                    UpdateTcns(uow, tcn, fir, fir.Designator, lastUpdateDate, container);
                }
            }

        }

        private void UpdateGlobalAhps(IUow uow, DateTime lastUpdateDate, Dictionary<string, DbGeography> rwyGeoCash,Container container)
        {
            try
            {
                Logger.Debug("Global Ahp updater started.");
                var entityName = "Ahp";
                var includedGlobalAhps =  GetConfigurationSetting(uow, "IncludedGlobalAhps", "").Split(',');
                var firs = uow.SdoSubjectRepo.GetByEntityNameAndNotDeleted("FIR");
                foreach (var gAhp in includedGlobalAhps)
                {

                    
                    var ahps = container.CreateQuery<Ahp>("Ahp").AddQueryOption("$select", "mid,EffectiveDate,GEOLocation,codeId,txtName,txtRmk,codeType ").AddQueryOption("$filter", "codeId eq '"+gAhp+"'")
                        .AddQueryOption("$expand", "Twy($select=mid,EffectiveDate,txtDesig,codeComposition,codeCondSfc,valWid)")
                        .AddQueryOption("$expand", "Rwy($select=mid,EffectiveDate,txtDesig,codeComposition,codeCondSfc,valWid,uomDimRwy;$expand=Rdn($select=GEOLocation))")
                        .AddQueryOption("$expand", "Ana($select=mid;$expand=Vor,Dme,Tcn,Ndb)").ToList();

                    foreach (var ahp in ahps)
                    {
                        var mid = ahp.Mid;
                        var adc = uow.AerodromeDisseminationCategoryRepo.GetByAhpMid(mid);
                        var adName = ahp.TxtName;
                    
                        if (adc == null)
                        {
                            uow.AerodromeDisseminationCategoryRepo.Add(new AerodromeDisseminationCategory
                            {
                                AhpMid = ahp.Mid,
                                AhpCodeId = ahp.CodeId,
                                AhpName = adName,
                                DisseminationCategory = DisseminationCategory.International,
                                Location = ahp.GEOLocation.ToEntityDbGeography()
                            });
                        }
                        else
                        {
                            adc.AhpCodeId = ahp.CodeId;
                            adc.AhpName = adName;
                            adc.Location = ahp.GEOLocation.ToEntityDbGeography();
                            uow.AerodromeDisseminationCategoryRepo.Update(adc);
                        }

                        var ahpId = entityName + mid;
                        var local = uow.SdoSubjectRepo.GetById(ahpId);
                        var geo = GetAdGeo(ahp, rwyGeoCash);
                        var dbGeoRef = ahp.GEOLocation.ToEntityDbGeography();
                        var fir = firs.FirstOrDefault(a => a.SubjectGeo.Intersects(dbGeoRef));
                        if (fir == null)
                        {
                            fir = firs.OrderBy(a => a.SubjectGeo.Distance(dbGeoRef)).First();
                        }
                        if (local == null)
                        {
                            local = new SdoSubject
                            {
                                Id = ahpId,
                                Mid = ahp.Mid,
                                EffectiveDate = lastUpdateDate,
                                SdoEntityName = entityName,
                                Designator = ahp.CodeId,
                                SubjectGeo = geo,
                                SubjectGeoRefPoint = dbGeoRef,
                                Fir = fir.Designator,
                                Name = adName,
                                Description = ahp.TxtRmk
                            };
                            uow.SdoSubjectRepo.Add(local);
                        }
                        else
                        {
                            local.EffectiveDate = lastUpdateDate;
                            local.SdoEntityName = entityName;
                            local.Designator = ahp.CodeId;
                            local.SubjectGeo = geo;
                            local.SubjectGeoRefPoint = dbGeoRef;
                            local.Fir = fir.Designator;
                            local.Name = adName;
                            local.Description = ahp.TxtRmk;
                            local.Mid = mid;
                            local.IsDeleted = false;

                            uow.SdoSubjectRepo.Update(local);
                        }
                        uow.SaveChanges();
                        UpdateRwys(uow, ahp, local, fir.Designator, lastUpdateDate, rwyGeoCash);
                        uow.SaveChanges();
                        UpdateTwys(uow, ahp, local, fir.Designator, lastUpdateDate);
                        uow.SaveChanges();
                        UpdateAdNavAids(uow, ahp, local, fir.Designator, lastUpdateDate, container);

                    }
                    uow.SaveChanges();
                    rwyGeoCash.Clear();
                }
                uow.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
            finally
            {
                Logger.Debug("Global Ahp updater finished.");
            }

        }

        private void UpdateAhps(IUow uow, DateTime lastUpdateDate, Dictionary<string, DbGeography> rwyGeoCash, Container container)
        {
            try
            {
                Logger.Debug("Ahp updater started.");
                var entityName = "Ahp";
                var batchSize = GetIntConfigurationSetting(uow, "AhpUpdaterBatchSize", 10);
                var recordNo = 0;
                var firs = uow.SdoSubjectRepo.GetByEntityNameAndNotDeleted("FIR");
                while (true)
                {
                    var ahps = container.CreateQuery<Ahp>("Ahp").AddQueryOption("$select", "mid,EffectiveDate,GEOLocation,codeId,txtName,txtRmk,codeType ")
                        .AddQueryOption("$filter", "length(codeId) eq 4")
                        .AddQueryOption("$expand", "Twy($select=mid,EffectiveDate,txtDesig,codeComposition,codeCondSfc,valWid)")
                        .AddQueryOption("$expand", "Rwy($select=mid,EffectiveDate,txtDesig,codeComposition,codeCondSfc,valWid,uomDimRwy;$expand=Rdn($select=GEOLocation))")
                        .AddQueryOption("$expand", "Ana($select=mid;$expand=Vor,Dme,Tcn,Ndb)")
                        .AddQueryOption("$expand", "Uni($select=txtName)")
                        .AddQueryOption("$skip", recordNo.ToString()).AddQueryOption("$top", batchSize.ToString()).ToList();
                    recordNo += batchSize;
                    if (ahps.Count() == 0)
                    {
                        break;
                    }
                    foreach (var ahp in ahps)
                    {
                        var mid = ahp.Mid;
                        var adc = uow.AerodromeDisseminationCategoryRepo.GetByAhpMid(mid);
                        var adName = ahp.TxtName;
                      
                        var sdoAttributes = JsonConvert.SerializeObject("CARS : False");
                        foreach ( var uni in ahp.Uni)
                        {
                            if (uni.TxtName.Contains("CARS"))
                            {
                                sdoAttributes = JsonConvert.SerializeObject("CARS : True");
                                break;
                            }
                        }
                        
                        /*
                        if (ahp.CodeType == "HP")
                        {
                            adName += "(HELI)";
                        }
                        else
                        {                            
                            // check if this is a water AD
                            if (ahp.Rwy.Any(r => r.CodeComposition == "WATER"))
                            {
                                adName += "(WATER)";
                            }

                        }
*/

                        if (adc == null)
                        {
                            uow.AerodromeDisseminationCategoryRepo.Add(new AerodromeDisseminationCategory
                            {
                                AhpMid = ahp.Mid,
                                AhpCodeId = ahp.CodeId,
                                AhpName = adName,
                                DisseminationCategory = DisseminationCategory.International,
                                Location = ahp.GEOLocation.ToEntityDbGeography()
                            });
                        }
                        else
                        {
                            adc.AhpCodeId = ahp.CodeId;
                            adc.AhpName = adName;
                            adc.Location = ahp.GEOLocation.ToEntityDbGeography();
                            uow.AerodromeDisseminationCategoryRepo.Update(adc);
                        }

                        var ahpId = entityName + mid;
                        var local = uow.SdoSubjectRepo.GetById(ahpId);
                        var geo = GetAdGeo(ahp, rwyGeoCash);
                        var dbGeoRef = ahp.GEOLocation.ToEntityDbGeography();
                        var fir = firs.FirstOrDefault(a => a.SubjectGeo.Intersects(dbGeoRef));
                        if (fir == null)
                        {
                            fir = firs.OrderBy(a => a.SubjectGeo.Distance(dbGeoRef)).First();
                        }
                        if (local == null)
                        {
                            local = new SdoSubject
                            {
                                Id = ahpId,
                                Mid = ahp.Mid,
                                EffectiveDate = lastUpdateDate,
                                SdoEntityName = entityName,
                                Designator = ahp.CodeId,
                                SubjectGeo = geo,
                                SubjectGeoRefPoint = dbGeoRef,
                                Fir = fir.Designator,
                                Name = adName,
                                Description = ahp.TxtRmk,
                                SdoAttributes = sdoAttributes

                            };
                            uow.SdoSubjectRepo.Add(local);
                        }
                        else
                        {
                            local.EffectiveDate = lastUpdateDate;
                            local.SdoEntityName = entityName;
                            local.Designator = ahp.CodeId;
                            local.SubjectGeo = geo;
                            local.SubjectGeoRefPoint = dbGeoRef;
                            local.Fir = fir.Designator;
                            local.Name = adName;
                            local.Description = ahp.TxtRmk;
                            local.Mid = mid;
                            local.IsDeleted = false;
                            local.SdoAttributes = sdoAttributes;

                            uow.SdoSubjectRepo.Update(local);
                        }
                        uow.SaveChanges();
                        UpdateRwys(uow, ahp, local, fir.Designator, lastUpdateDate, rwyGeoCash);
                        uow.SaveChanges();
                        UpdateTwys(uow, ahp, local, fir.Designator, lastUpdateDate);
                        uow.SaveChanges();
                        UpdateAdNavAids(uow, ahp, local, fir.Designator, lastUpdateDate, container);

                    }
                    uow.SaveChanges();
                    rwyGeoCash.Clear();
                }
                uow.SaveChanges( );
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
            finally
            {
                Logger.Debug("Ahp updater finished.");
            }
        }

        private void UpdateFirs(IUow uow, DateTime lastUpdateDate, Container container)
        {
            try
            {
                Logger.Debug("FIR updater started.");
                var entityName = "FIR";
                var firs = container.Ase.Where(ase => ase.CodeType == entityName);
                foreach (var fir in firs)
                {
                    var mid = fir.Mid;
                    var firGeo = container.GeoInstanceCache.Where(a => a.EntityType == "Ase" && a.Mid == mid).FirstOrDefault();
                    if (firGeo == null)
                    {
                        Logger.Warn(string.Format("FIR {0} GEO is null, FIR skipped", fir.CodeId));
                        continue;
                    }
                    var geo = firGeo.GeoInstance.ToEntityDbGeography();
                    var refCoord = geo.GetReferancePoint();
                    var refPoint = DbGeography.PointFromText(string.Format("POINT({0} {1})", refCoord.Longitude, refCoord.Latitude), GeoDataService.SRID);
                    var id = entityName + fir.Mid;
                    var local = uow.SdoSubjectRepo.GetById(id);
                    if (local == null)
                    {
                        local = new SdoSubject
                        {
                            Id = id,
                            Mid = fir.Mid,
                            EffectiveDate = lastUpdateDate,
                            SdoEntityName = entityName,
                            Designator = fir.CodeId,
                            SubjectGeo = geo,
                            SubjectGeoRefPoint = refPoint,
                            Fir = fir.CodeId,
                            Name = fir.TxtName,
                            Description = ""
                        };
                        uow.SdoSubjectRepo.Add(local);
                    }
                    else //if (local.EffectiveDate <= fir.EffectiveDate)
                    {
                        local.IsDeleted = false; // in case it brought back to life
                        local.EffectiveDate = fir.EffectiveDate.DateTime;
                        local.Designator = fir.CodeId;
                        local.SubjectGeo = geo;
                        local.SubjectGeoRefPoint = refPoint;
                        local.Fir = fir.CodeId;
                        local.Name = fir.TxtName;
                        local.Description = fir.TxtRmk;
                        uow.SdoSubjectRepo.Update(local);
                    }

                    //UpdateFirNavAids(uow, local, lastUpdateDate, container);
                }
                uow.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(e);
                throw; // re-throw exception so the message will be back to the queue
            }
            Logger.Debug("FIR updater finished.");
        }

        private void UpdateRwys(IUow uow, Ahp ahp, SdoSubject parent, string fir, DateTime lastUpdateDate, Dictionary<string, DbGeography> rwyGeoCash)
        {
            
            var entityName = "Rwy";
            foreach (var rwy in ahp.Rwy)
            {
                var id = entityName + rwy.Mid;
                var local = uow.SdoSubjectRepo.GetById(id);
                var geo = GetRwyGeo(ahp, rwy, rwyGeoCash);
                var refCoord = geo.GetReferancePoint();
                var refPoint = DbGeography.PointFromText(string.Format("POINT({0} {1})", refCoord.Longitude, refCoord.Latitude), GeoDataService.SRID);
                if (local == null)
                {
                    uow.SdoSubjectRepo.Add(new SdoSubject
                    {
                        Id = id,
                        Mid = rwy.Mid,
                        ParentId = parent.Id,
                        EffectiveDate = lastUpdateDate,
                        SdoEntityName = entityName,
                        Designator = rwy.TxtDesig,
                        SubjectGeo = geo,
                        SubjectGeoRefPoint = refPoint,
                        Fir = fir,
                        Name = rwy.CodeComposition,
                        Description = rwy.CodeCondSfc
                    });
                }
                else
                {
                    local.EffectiveDate = lastUpdateDate;
                    local.Designator = rwy.TxtDesig;
                    local.SubjectGeo = geo;
                    local.SubjectGeoRefPoint = refPoint;
                    local.Fir = fir;
                    local.Name = ahp.TxtName;
                    local.IsDeleted = false;
                    local.Mid = rwy.Mid;
                    local.Description = ahp.TxtRmk;
                }
            }
        }

        private void UpdateTwys(IUow uow, Ahp ahp, SdoSubject parent, string fir, DateTime lastUpdateDate)
        {
            if (ahp.Twy.Count > 0)
            {
                

                var entityName = "Twy";
                foreach (var twy in ahp.Twy)
                {
                    var mid = twy.Mid;
                    var id = entityName + mid;
                    var local = uow.SdoSubjectRepo.GetById(id);
                    var geo = GetTwyGeo(ahp);
                    var refCoord = geo.GetReferancePoint();
                    var refPoint = DbGeography.PointFromText(string.Format("POINT({0} {1})", refCoord.Longitude, refCoord.Latitude), GeoDataService.SRID);
                    if (local == null)
                    {
                        uow.SdoSubjectRepo.Add(new SdoSubject
                        {
                            Id = id,
                            Mid = twy.Mid,
                            ParentId = parent.Id,
                            EffectiveDate = lastUpdateDate,
                            SdoEntityName = entityName,
                            Designator = twy.TxtDesig,
                            SubjectGeo = geo,
                            SubjectGeoRefPoint = refPoint,
                            Fir = fir,
                            Name = twy.CodeComposition,
                            Description = twy.CodeCondSfc
                        });
                    }
                    else
                    {
                        local.EffectiveDate = lastUpdateDate;
                        local.Designator = twy.TxtDesig;
                        local.SubjectGeo = geo;
                        local.SubjectGeoRefPoint = refPoint;
                        local.Fir = fir;
                        local.Name = ahp.TxtName;
                        local.IsDeleted = false;
                        local.Mid = mid;
                        local.Description = ahp.TxtRmk;

                        uow.SdoSubjectRepo.Update(local);
                    }
                }
                
            }
        }

        //private void UpdateFirNavAids(IUow uow, SdoSubject parent, DateTime lastUpdateDate, Container container)
        //{
        //    //var Ana = container.Ana.ToList();
        //}

        private void UpdateAdNavAids(IUow uow, Ahp ahp, SdoSubject parent, string fir, DateTime lastUpdateDate, Container container)
        {
            Logger.Debug("NAVAIDS updater started for "+ahp.CodeId);
            foreach (var a in ahp.Ana)
            {
                if (a.Vor != null)
                {
                    UpdateVors(uow, a.Vor, parent, fir, lastUpdateDate, container);
                }
                else if (a.Dme != null)
                {
                    UpdateDmes(uow, a.Dme, parent, fir, lastUpdateDate, container);
                }
                else if (a.Tcn != null)
                {
                    UpdateTcns(uow, a.Tcn, parent, fir, lastUpdateDate, container);
                }
                else if (a.Ndb != null)
                {
                    UpdateNdbs(uow, a.Ndb, parent, fir, lastUpdateDate, container);
                }
            }
            
        }

        private void UpdateVors(IUow uow, Vor v, SdoSubject parent, string fir, DateTime lastUpdateDate, Container container)
        {
            var expandedVor = container.Vor.AddQueryOption("$filter", "mid eq '" + v.Mid + "' ").AddQueryOption("$expand", "Tcn,Dme").First();
            var entityName = expandedVor.Tcn.Count() > 0 ? "VORTAC" : expandedVor.Dme.Count() > 0 ? "VOR/DME" : "VOR";
            var id = entityName + v.Mid + "@" + parent.Id;

            var local = uow.SdoSubjectRepo.GetById(id);
            var geo = v.GEOLocation.ToEntityDbGeography().Buffer(10);
            var refCoord = geo.GetReferancePoint();
            var refPoint = DbGeography.PointFromText(string.Format("POINT({0} {1})", refCoord.Longitude, refCoord.Latitude), GeoDataService.SRID);
            var sdoAttributes = JsonConvert.SerializeObject(expandedVor);

            if (local == null)
            {
                uow.SdoSubjectRepo.Add(new SdoSubject
                {
                    Id = id,
                    Mid = v.Mid,
                    ParentId = parent.Id,
                    EffectiveDate = lastUpdateDate,
                    SdoEntityName = entityName,
                    Designator = v.CodeId,
                    SubjectGeo = geo,
                    SubjectGeoRefPoint = refPoint,
                    Fir = fir,
                    Name = entityName + "-" + v.TxtName,
                    Description = v.TxtRmk,
                    SdoAttributes = sdoAttributes
                });
            }
            else
            {
                local.EffectiveDate = lastUpdateDate;
                local.Designator = v.CodeId;
                local.SubjectGeo = geo;
                local.SubjectGeoRefPoint = refPoint;
                local.Fir = fir;
                local.Name = entityName + "-" + v.TxtName;
                local.IsDeleted = false;
                local.Mid = v.Mid;
                local.Description = v.TxtRmk;
                local.SdoAttributes = sdoAttributes;

                uow.SdoSubjectRepo.Update(local);
            }
        }
        private void UpdateDmes(IUow uow, Dme d, SdoSubject parent, string fir, DateTime lastUpdateDate, Container container)
        {
            var expandedDme = container.Dme.AddQueryOption("$filter", "mid eq '" + d.Mid + "' ").AddQueryOption("$expand", "Vor").First();
            if (expandedDme.Vor != null)
            {
                return; // Will be added with the associated Vor
            }

            var entityName = "DME";
            var id = entityName + d.Mid + "@" + parent.Id;
            var local = uow.SdoSubjectRepo.GetById(id);
            var geo = d.GEOLocation.ToEntityDbGeography().Buffer(10);
            var refCoord = geo.GetReferancePoint();
            var refPoint = DbGeography.PointFromText(string.Format("POINT({0} {1})", refCoord.Longitude, refCoord.Latitude), GeoDataService.SRID);
            var sdoAttributes = JsonConvert.SerializeObject(d);
            if (local == null)
            {
                uow.SdoSubjectRepo.Add(new SdoSubject
                {
                    Id = id,
                    Mid = d.Mid,
                    ParentId = parent.Id,
                    EffectiveDate = lastUpdateDate,
                    SdoEntityName = entityName,
                    Designator = d.CodeId,
                    SubjectGeo = geo,
                    SubjectGeoRefPoint = refPoint,
                    Fir = fir,
                    Name = entityName + "-" + d.TxtName,
                    Description = d.TxtRmk,
                    SdoAttributes = sdoAttributes
                });
            }
            else
            {
                local.EffectiveDate = lastUpdateDate;
                local.Designator = d.CodeId;
                local.SubjectGeo = geo;
                local.SubjectGeoRefPoint = refPoint;
                local.Fir = fir;
                local.Name = entityName + "-" + d.TxtName;
                local.IsDeleted = false;
                local.Mid = d.Mid;
                local.Description = d.TxtRmk;
                local.SdoAttributes = sdoAttributes;

                uow.SdoSubjectRepo.Update(local);
            }

        }
        private void UpdateTcns(IUow uow, Tcn t, SdoSubject parent, string fir, DateTime lastUpdateDate, Container container)
        {
            var expandedTcn = container.Tcn.AddQueryOption("$filter", "mid eq '" + t.Mid + "' ").AddQueryOption("$expand", "Vor").First();
            if (expandedTcn.Vor != null)
            {
                return; // Will be added with the associated Vor
            }

            var entityName = "TACAN";
            var id = entityName + t.Mid + "@" + parent.Id;
            var local = uow.SdoSubjectRepo.GetById(id);
            var geo = t.GEOLocation.ToEntityDbGeography().Buffer(10);
            var refCoord = geo.GetReferancePoint();
            var refPoint = DbGeography.PointFromText(string.Format("POINT({0} {1})", refCoord.Longitude, refCoord.Latitude), GeoDataService.SRID);
            var sdoAttributes = JsonConvert.SerializeObject(t);
            if (local == null)
            {
                uow.SdoSubjectRepo.Add(new SdoSubject
                {
                    Id = id,
                    Mid = t.Mid,
                    ParentId = parent.Id,
                    EffectiveDate = lastUpdateDate,
                    SdoEntityName = entityName,
                    Designator = t.CodeId,
                    SubjectGeo = geo,
                    SubjectGeoRefPoint = refPoint,
                    Fir = fir,
                    Name = entityName + "-" + t.TxtName,
                    Description = t.TxtRmk,
                    SdoAttributes = sdoAttributes
                });
            }
            else
            {
                local.EffectiveDate = lastUpdateDate;
                local.Designator = t.CodeId;
                local.SubjectGeo = geo;
                local.SubjectGeoRefPoint = refPoint;
                local.Fir = fir;
                local.Name = entityName + "-" + t.TxtName;
                local.IsDeleted = false;
                local.Mid = t.Mid;
                local.Description = t.TxtRmk;
                local.SdoAttributes = sdoAttributes;

                uow.SdoSubjectRepo.Update(local);
            }
        }
        private void UpdateNdbs(IUow uow, Ndb n, SdoSubject parent, string fir, DateTime lastUpdateDate, Container container)
        {
            var entityName = "NDB";
            var id = entityName + n.Mid + "@" + parent.Id;
            var local = uow.SdoSubjectRepo.GetById(id);
            var geo = n.GEOLocation.ToEntityDbGeography().Buffer(10);
            var refCoord = geo.GetReferancePoint();
            var refPoint = DbGeography.PointFromText(string.Format("POINT({0} {1})", refCoord.Longitude, refCoord.Latitude), GeoDataService.SRID);
            var sdoAttributes = JsonConvert.SerializeObject(n);
            if (local == null)
            {
                uow.SdoSubjectRepo.Add(new SdoSubject
                {
                    Id = id,
                    Mid = n.Mid,
                    ParentId = parent.Id,
                    EffectiveDate = lastUpdateDate,
                    SdoEntityName = entityName,
                    Designator = n.CodeId,
                    SubjectGeo = geo,
                    SubjectGeoRefPoint = refPoint,
                    Fir = fir,
                    Name = entityName + "-" + n.TxtName,
                    Description = n.TxtRmk,
                    SdoAttributes = sdoAttributes
                });
            }
            else
            {
                local.EffectiveDate = lastUpdateDate;
                local.Designator = n.CodeId;
                local.SubjectGeo = geo;
                local.SubjectGeoRefPoint = refPoint;
                local.Fir = fir;
                local.Name = entityName + "-" + n.TxtName;
                local.IsDeleted = false;
                local.Mid = n.Mid;
                local.Description = n.TxtRmk;
                local.SdoAttributes = sdoAttributes;

                uow.SdoSubjectRepo.Update(local);
            }
        }

    }
}
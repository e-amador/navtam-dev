﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RSC.WebService.Helpers
{
    public static class ModelExtensions
    {
        /// <summary>
        /// Converts a Model.RSCResponse into a RSC.Schemas.RSCResponseType instance.
        /// </summary>
        public static Schemas.RSCResponseType ToSchema(this Model.RSCResponse model)
        {
            return new Schemas.RSCResponseType
            {
                version = (Schemas.RSCResponseTypeVersion)model.Version,
                Status = (Schemas.RSCResponseTypeStatus)model.Status,
                MODE = model.Mode,
                Message = MapMessages(model.Messages),
                RSC = model.RSC?.ToSchema()
            };
        }

        static Schemas.RSCResponseTypeMessage[] MapMessages(IEnumerable<Model.RSCResponseMessage> messages)
        {
            return (messages ?? new Model.RSCResponseMessage[] {}).Select(m => m.ToSchema()).ToArray();
        }

        static Schemas.RSCResponseTypeRSC ToSchema(this Model.RSCText model)
        {
            return new Schemas.RSCResponseTypeRSC()
            {
#if DEBUG
                EnglishRSC = $"\n{model.EnglishRSC}\n",
                FrenchRSC = $"\n{model.FrenchRSC}\n"
#else
                EnglishRSC = $"{model.EnglishRSC}",
                FrenchRSC = $"{model.FrenchRSC}"
#endif
            };
        }

        static Schemas.RSCResponseTypeMessage ToSchema(this Model.RSCResponseMessage model)
        {
            var rscMessage = new Schemas.RSCResponseTypeMessage()
            {
                MsgNumber = model.Number.ToString(),
                Description = model.Description,
                RunwayId = model.RunwayId
            };
            if (model.Third.HasValue)
            {
                rscMessage.Third = (Schemas.RSCResponseTypeMessageThird)model.Third.Value;
                rscMessage.ThirdSpecified = true;
            }
            return rscMessage;
        }
    }
}
﻿
namespace NavCanada.Core.Domain.Model.Dtos
{
    public class IcaoSubjectConditionDto
    {
        public long Id { get; set; }

        public string Code { get; set; }

        public bool I { get; set; }

        public bool V { get; set; }

        public bool N { get; set; }

        public bool B { get; set; }

        public bool O { get; set; }

        public bool M { get; set; }

        public int Lower { get; set; }

        public int Upper { get; set; }

        public int Radius { get; set; }

        public bool requiresItemFG { get; set; }

        public bool RequiresPurpose { get; set; }
        public bool CancellationOnly { get; set; }
        public bool Active { get; set; }

        public string Description { get; set; }
        public string DescriptionFrench { get; set; }
    }
}

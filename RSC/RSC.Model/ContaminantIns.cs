﻿using RSC.Model.Enums;

namespace RSC.Model
{
    /// <summary>
    /// Contaminant identification, coverage and depth (if appropriate) in feet or inches.
    /// </summary>
    public class ContaminantIns
    {
        /// <summary>
        /// Percentage of the current runway portion covered by the indicated contamination.
        /// The total of all defined percentages must not exceed 100. If less than 100 it will be assumed
        /// that the remaining percentage is bare and dry.
        /// These are pre-defined values in the schema.
        /// </summary>
        public int ContamCoverage { get; set; }
        /// <summary>
        /// Type of contamination to be found on this portion of the runway - that
        /// must be accompanied by a depth modifier.This element provides the ability to specify
        /// differing layers of contamination.
        /// </summary>
        public ContaminantWithDepthEnum? ContamType { get; set; }
        /// <summary>
        /// Depth of contamination.
        /// </summary>
        public ContamDepth ContamDepthI { get; set; }
        /// <summary>
        /// Type of contamination that does not have a Depth modifier to be found on this
        /// portion of the runway.This element provides the ability to specify differing layers of
        /// contamination.
        /// </summary>
        public ContaminantWithNoDepthEnum? ContamTypeNoD { get; set; }
        /// <summary>
        /// The contaminant has depth or not.
        /// </summary>
        public bool HasDepdth => ContamType.HasValue;
    }
}
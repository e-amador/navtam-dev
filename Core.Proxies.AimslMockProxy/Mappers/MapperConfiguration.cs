﻿namespace NavCanada.Core.Proxies.AimslMockProxy.Mappers
{
    using AutoMapper;

    using NavCanada.Core.Common.Responses;
    using NavCanada.Core.Domain.Model.Entitities;
    using NavCanada.Core.Domain.Model.Enums;
    using NavCanada.Core.Proxies.AimslMockProxy.Mappers.Converters;

    public static class AutoMapperConfig
    {
        public static void RegisterMaps()
        {
            Mapper.CreateMap<NotamProposal, Proxy.NotamProposal>().ConvertUsing(new NotamMProposal2ProxyNotamProposalConvertor());
            Mapper.CreateMap<Proxy.NotamProposalStatusMessage, AismlResponse>().ConvertUsing(new NotamProposalStatusMessage2AismlResponse());
            Mapper.CreateMap<Proxy.NotamProposalStatusType, NotamProposalStatusCode>().ConvertUsing(new NotamProposalStatusType2NotamProposalStatusCode());

            Mapper.CreateMap<Proxy.Notam, Notam>()
                .ForMember(a => a.Code23, opt => opt.MapFrom(s => s.QLine.Code23))
                .ForMember(a => a.Code45, opt => opt.MapFrom(s => s.QLine.Code45))
                .ForMember(a => a.Fir, opt => opt.MapFrom(s => s.QLine.FIR))
                .ForMember(a => a.Lower, opt => opt.MapFrom(s => s.QLine.Lower))
                .ForMember(a => a.Purpose, opt => opt.MapFrom(s => s.QLine.Purpose))
                .ForMember(a => a.Scope, opt => opt.MapFrom(s => s.QLine.Scope))
                .ForMember(a => a.Traffic, opt => opt.MapFrom(s => s.QLine.Traffic))
                .ForMember(a => a.Upper, opt => opt.MapFrom(s => s.QLine.Upper))
                .ForMember(a => a.Permanent, opt => opt.Ignore());
        }
    }
}

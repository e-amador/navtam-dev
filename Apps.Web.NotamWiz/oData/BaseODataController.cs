﻿//using System.Web.Http.OData.Routing;

namespace NavCanada.Applications.NotamWiz.Web.oData
{
    using System.Linq;
    using System.Web;
    using System.Web.OData;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;

    using NavCanada.Applications.NotamWiz.Web.Code.IdentityManagers;
    using NavCanada.Core.Data.NotamWiz.Contracts;
    using NavCanada.Core.Domain.Model.Entitities;

    public abstract class BaseODataController : ODataController
    {
        protected IUow Uow { get; private set; }
        private AppUserManager _userManager;
        private AppRoleManager _roleManager;

        protected BaseODataController(IUow uow)
        {
            Uow = uow;
        }
        protected AppUserManager UserManager
        {
            get { return this._userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<AppUserManager>(); }
            private set { this._userManager = value; }
        }

        protected AppRoleManager RoleManager
        {
            get { return this._roleManager ?? HttpContext.Current.GetOwinContext().Get<AppRoleManager>(); }
            private set { this._roleManager = value; }
        }
        public bool IsUserInRole(string userId, string roleName)
        {
           
            var role = RoleManager.FindByName(roleName);
            var currentUser = UserManager.FindById(userId);

            if (role == null)
                return false;

            return currentUser.Roles.Any(r => r.RoleId == role.Id);
        }

        public Doa GetUserDOA()
        {
            long? doaID = GetUserOrganization().AssignedDoaId;
            return Uow.DoaRepo.GetById(doaID);
        }

        public Organization GetUserOrganization()
        {
            var currentUser = UserManager.FindById(User.Identity.GetUserId());
            long orgId = (long)currentUser.OrganizationId;
            Organization currentOrg = Uow.OrganizationRepo.GetById(orgId);
            return currentOrg;
        }


    }
}
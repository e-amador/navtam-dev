﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by xsd, Version=4.6.1055.0.
// 
namespace NDS.Common.Xml {
    using System.Xml.Serialization;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.navcanada.ca/NES/CommonTypes")]
    [System.Xml.Serialization.XmlRootAttribute("NOTAM", Namespace="http://www.navcanada.ca/NES/CommonTypes", IsNullable=false)]
    public partial class NotamType {
        
        private string nOFField;
        
        private string seriesField;
        
        private string numberField;
        
        private string yearField;
        
        private NotamTypeType typeField;
        
        private string referredSeriesField;
        
        private string referredNumberField;
        
        private string referredYearField;
        
        private QLineType qLineField;
        
        private string fullCoordinatesField;
        
        private string[] itemAField;
        
        private string startValidityField;
        
        private string endValidityField;
        
        private string estimationField;
        
        private string itemDField;
        
        private string itemEField;
        
        private string itemEFrenchField;
        
        private string itemFField;
        
        private string itemGField;
        
        private string operatorField;
        
        private int versionField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string NOF {
            get {
                return this.nOFField;
            }
            set {
                this.nOFField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Series {
            get {
                return this.seriesField;
            }
            set {
                this.seriesField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType="positiveInteger")]
        public string Number {
            get {
                return this.numberField;
            }
            set {
                this.numberField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType="integer")]
        public string Year {
            get {
                return this.yearField;
            }
            set {
                this.yearField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public NotamTypeType Type {
            get {
                return this.typeField;
            }
            set {
                this.typeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ReferredSeries {
            get {
                return this.referredSeriesField;
            }
            set {
                this.referredSeriesField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType="positiveInteger")]
        public string ReferredNumber {
            get {
                return this.referredNumberField;
            }
            set {
                this.referredNumberField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType="integer")]
        public string ReferredYear {
            get {
                return this.referredYearField;
            }
            set {
                this.referredYearField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public QLineType QLine {
            get {
                return this.qLineField;
            }
            set {
                this.qLineField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string FullCoordinates {
            get {
                return this.fullCoordinatesField;
            }
            set {
                this.fullCoordinatesField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ItemA", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string[] ItemA {
            get {
                return this.itemAField;
            }
            set {
                this.itemAField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string StartValidity {
            get {
                return this.startValidityField;
            }
            set {
                this.startValidityField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string EndValidity {
            get {
                return this.endValidityField;
            }
            set {
                this.endValidityField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Estimation {
            get {
                return this.estimationField;
            }
            set {
                this.estimationField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ItemD {
            get {
                return this.itemDField;
            }
            set {
                this.itemDField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ItemE {
            get {
                return this.itemEField;
            }
            set {
                this.itemEField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ItemEFrench {
            get {
                return this.itemEFrenchField;
            }
            set {
                this.itemEFrenchField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ItemF {
            get {
                return this.itemFField;
            }
            set {
                this.itemFField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ItemG {
            get {
                return this.itemGField;
            }
            set {
                this.itemGField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Operator {
            get {
                return this.operatorField;
            }
            set {
                this.operatorField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int Version {
            get {
                return this.versionField;
            }
            set {
                this.versionField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.navcanada.ca/NES/CommonTypes")]
    public enum NotamTypeType {
        
        /// <remarks/>
        N,
        
        /// <remarks/>
        R,
        
        /// <remarks/>
        C,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.navcanada.ca/NES/CommonTypes")]
    public partial class QLineType {
        
        private string fIRField;
        
        private string code23Field;
        
        private string code45Field;
        
        private TrafficType trafficField;
        
        private PurposeType purposeField;
        
        private ScopeType scopeField;
        
        private string lowerField;
        
        private string upperField;
        
        private string coordinatesField;
        
        private string radiusField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string FIR {
            get {
                return this.fIRField;
            }
            set {
                this.fIRField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Code23 {
            get {
                return this.code23Field;
            }
            set {
                this.code23Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Code45 {
            get {
                return this.code45Field;
            }
            set {
                this.code45Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public TrafficType Traffic {
            get {
                return this.trafficField;
            }
            set {
                this.trafficField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public PurposeType Purpose {
            get {
                return this.purposeField;
            }
            set {
                this.purposeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public ScopeType Scope {
            get {
                return this.scopeField;
            }
            set {
                this.scopeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType="nonNegativeInteger")]
        public string Lower {
            get {
                return this.lowerField;
            }
            set {
                this.lowerField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType="nonNegativeInteger")]
        public string Upper {
            get {
                return this.upperField;
            }
            set {
                this.upperField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Coordinates {
            get {
                return this.coordinatesField;
            }
            set {
                this.coordinatesField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType="nonNegativeInteger")]
        public string Radius {
            get {
                return this.radiusField;
            }
            set {
                this.radiusField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.navcanada.ca/NES/CommonTypes")]
    public enum TrafficType {
        
        /// <remarks/>
        I,
        
        /// <remarks/>
        V,
        
        /// <remarks/>
        IV,
        
        /// <remarks/>
        K,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.navcanada.ca/NES/CommonTypes")]
    public enum PurposeType {
        
        /// <remarks/>
        NB,
        
        /// <remarks/>
        BO,
        
        /// <remarks/>
        M,
        
        /// <remarks/>
        K,
        
        /// <remarks/>
        NBO,
        
        /// <remarks/>
        B,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.navcanada.ca/NES/CommonTypes")]
    public enum ScopeType {
        
        /// <remarks/>
        A,
        
        /// <remarks/>
        E,
        
        /// <remarks/>
        W,
        
        /// <remarks/>
        AE,
        
        /// <remarks/>
        AW,
        
        /// <remarks/>
        K,
    }
}

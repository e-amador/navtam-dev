﻿using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.TaskEngine.Mto;
using System;

namespace NavCanada.Core.TaskEngine.Helpers
{
    public static class MtoHelper
    {
        public static MessageTransferObject CreateMtoMessage(MessageTransferObjectId mtoId, Action<MessageTransferObject> setupAction)
        {
            var mto = new MessageTransferObject();

            // add unique identifier
            mto.Add(MessageDefs.UniqueIdentifierKey, Guid.NewGuid().ToString(), DataType.String);

            // create Message type
            mto.Add(MessageDefs.MessageTypeKey, (byte)MessageType.WorkFlowTask, DataType.Byte);

            // create inbound type
            mto.Add(MessageDefs.MessageIdKey, mtoId, DataType.Int32);

            // date/time when the message was sent
            mto.Add(MessageDefs.MessageTimeKey, DateTime.UtcNow, DataType.DateTime);

            //  update the message
            setupAction.Invoke(mto);

            return mto;
        }

        public static MessageTransferObject CreateMtoMessageWithKeyValue<KeyType>(MessageTransferObjectId mtoId, KeyType keyValue)
        {
            return CreateMtoMessage(mtoId, m => m.Add(MessageDefs.EntityIdentifierKey, keyValue, DataType.String));
        }
    }
}

﻿using NavCanada.Core.Domain.Model.Enums;

namespace NAVTAM.NsdEngine.MOBST.V10
{
    public class MObstViewModel : TokenViewModel
    {
        public string ObstacleType { get; set; }
        public string OtherObstacleNameE { get; set; }
        public string OtherObstacleNameF { get; set; }

        public ObstacleAreaType AreaType { get; set; }

        public string Location { get; set; }
        public double Radius { get; set; }
        public ObstacleRadiusUnit RadiusUnit { get; set; }

        public string FromPoint { get; set; }
        public string ToPoint { get; set; }
        public string ObstacleDescriptionE { get; set; }
        public string ObstacleDescriptionF { get; set; }

        public int Elevation { get; set; }
        public int Height { get; set; }

        public SwitchState Lighted { get; set; }
        public SwitchState Painted { get; set; }
        public SwitchState Ballmarks { get; set; }
    }
}

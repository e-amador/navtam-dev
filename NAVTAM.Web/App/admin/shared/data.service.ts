﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Response } from '@angular/http';

import { WindowRef } from '../../common/windowRef.service'
import { XsrfTokenService } from '../../common/xsrf-token.service'

import {
    IQueryOptions,
    IUserQueryOptions,
    IUserPartial,
    IOrgPartial,
    ISeriesPartial,
    IDoaPartial,
    IIcaoSubjectPartial,
    IIcaoSubjectCondition,
    IIcaoSubjectConditionPartial,
    IUser,
    IUsersDelete,
    IRole,
    IOrganization,
    IGeoRegionPartial,
    IAerodromeDisseminationCategory,
    IAhpNoDC,
    IDoaAdminPartial,
    IDoaAdmin,
    //IDoaRegion,
    IRegionMapRender,
    //DoaRegionSource,
    INdsClient,
    INdsClientPartial,
    INdsClientFilter,
    INsdPartial,
    INsdCategories,
    INsdManagementDto,
    IFIR,
    IDoaPointModel,
    IRegionLocationModel,
    IGeoRegion,
    IConfigValuePartial,
    ISeriesChecklist
} from './data.model';

@Injectable()
export class DataService {
    apiUrl: string;

    constructor(private http: Http, private winRef: WindowRef, private xsrfTokenService: XsrfTokenService) {
        this.apiUrl = winRef.nativeWindow['app'].apiUrl;
        this.app = winRef.nativeWindow['app'];
        if (this.app.antiForgeryTokenValue) {
            this.xsrfTokenService.addForgeryTokenToHeader(this.app.antiForgeryTokenValue);
        }
    }

    public app;

    getRoles(): Observable<IRole[]> {
        return this.http.get(`${this.apiUrl}admin/user/roles`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    //USERS
    getRegisteredUsers(queryOptions: IUserQueryOptions): Observable<IUserPartial[]> {
        return this.http.post(`${this.apiUrl}admin/users/page/?includeDisabled=${queryOptions.includeDisabled}&includeDeleted=${queryOptions.includeDeleted}`, queryOptions)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getAvailableRolesByOrg(orgId: string): Observable<IRole[]> {
        return this.http.get(`${this.apiUrl}admin/user/availableroles/?id=${orgId}`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getUserModel(username: string): Observable<IUser> {
        return this.http.get(`${this.apiUrl}admin/users/?username=${username}`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getDoasInOrg(orgId: string): Observable<IDoaPartial[]> {
        return this.http.get(`${this.apiUrl}admin/orgs/doas/?id=${orgId}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    saveUser(user: IUser): Observable<IUser> {
        return this.http.post(`${this.apiUrl}admin/users`, user)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    updateUser(user: IUser): Observable<IUser> {
        return this.http.put(`${this.apiUrl}admin/users`, user)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    resetUserPassword(data: any): Observable<any> {
        return this.http.put(`${this.apiUrl}admin/users/resetPassword`, data)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    sendVerificationEmail(userId: string): Observable<any> {
        return this.http.post(`${this.apiUrl}admin/users/resendVerificationEmail?userId=${userId}`, null)
            .map((response: Response) => {
                return response;
            }).catch(this.handleError);
    }

    resetPasswordVerificationCode(userId: string): Observable<any> {
        return this.http.post(`${this.apiUrl}admin/users/resetPasswordVerificationCode?userId=${userId}`, null)
            .map((response: Response) => {
                return response;
            }).catch(this.handleError);
    }

    deleteUser(username: string): Observable<any> {
        return this.http.delete(`${this.apiUrl}admin/users/${username}`)
            .map((response: Response) => {
                return response;
            }).catch(this.handleError);
    }

    deleteMultiUsers(usernames: IUsersDelete): Observable<any> {
        return this.http.put(`${this.apiUrl}admin/multiusers/`, usernames)
            .map((response: Response) => {
                return response;
            }).catch(this.handleError);
    }

    //ORGANIZATIONS
    getRegisteredOrgs(queryOptions: IQueryOptions): Observable<IOrgPartial[]> {
        return this.http.post(`${this.apiUrl}admin/orgs/page`, queryOptions)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getOrganizations(): Observable<IOrganization[]> {
        return this.http.get(`${this.apiUrl}admin/orgs/partial`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getOrgUserCount(id: number): Observable<number> {
        return this.http.get(`${this.apiUrl}admin/orgs/users/count?id=${id}`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getOrgModel(id: number): Observable<IOrganization> {
        return this.http.get(`${this.apiUrl}admin/orgs/?id=${id}`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    saveOrg(org: IOrganization): Observable<IOrganization> {
        return this.http.post(`${this.apiUrl}admin/orgs`, org)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    updateOrg(org: IOrganization): Observable<IOrganization> {
        return this.http.put(`${this.apiUrl}admin/orgs`, org)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    deleteOrg(orgId: number): Observable<any> {
        return this.http.delete(`${this.apiUrl}admin/orgs/${orgId}`)
            .map((response: Response) => {
                return response;
            }).catch(this.handleError);
    }

    isOrgNameUnique(id: number, name: string): Observable<boolean> {
        return this.http.get(`${this.apiUrl}admin/orgs/orgunique/?id=${id}&name=${name}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    isOrgAdminNameUnique(name: string): Observable<boolean> {
        return this.http.get(`${this.apiUrl}admin/orgs/userunique/?name=${name}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getOrganizationsWithNsdId(nsdId: number): Observable<IOrganization> {
        //This is a fix? related with IE that cached the get request always. Everytime that a request is sent,
        //we need to sent something different, like the parameter that is not used on the Controller
        return this.http.get(`${this.apiUrl}admin/orgs/getorgsbynsdid/?nsdId=${nsdId}&timeRequest=${new Date().getTime()}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    //ICAO Subjects
    getRegisteredIcaoSubjects(queryOptions: IQueryOptions): Observable<IIcaoSubjectPartial[]> {
        return this.http.post(`${this.apiUrl}icaosubjects/page`, queryOptions)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getIcaoSubjectModel(id: number): Observable<IIcaoSubjectPartial> {
        return this.http.get(`${this.apiUrl}icaosubjects/model?id=${id}`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getAllIcaoSubjectCodes(): Observable<string[]> {
        return this.http.get(`${this.apiUrl}icaosubjects/codes`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    createIcaoSubject(icaoSubject: IIcaoSubjectPartial): Observable<IIcaoSubjectPartial> {
        return this.http.post(`${this.apiUrl}icaosubjects/create`, icaoSubject)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    updateIcaoSubject(icaoSubject: IIcaoSubjectPartial): Observable<IIcaoSubjectPartial> {
        return this.http.put(`${this.apiUrl}icaosubjects/update`, icaoSubject)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    deleteIcaoSubject(icaoSubjectId: number): Observable<any> {
        return this.http.delete(`${this.apiUrl}icaosubjects/delete/${icaoSubjectId}`)
            .map((response: Response) => {
                return response;
            }).catch(this.handleError);
    }

    //ICAO SubjectConditions
    getRegisteredIcaoSubjectConditions(subjectId: number, queryOptions: IQueryOptions): Observable<IIcaoSubjectConditionPartial[]> {
        return this.http.post(`${this.apiUrl}icaoconditions/page?subId=${subjectId}`, queryOptions)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getAllIcaoConditionCodes(): Observable<string[]> {
        return this.http.get(`${this.apiUrl}icaoconditions/codes`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getIcaoConditionModel(id: number): Observable<IIcaoSubjectCondition> {
        return this.http.get(`${this.apiUrl}icaoconditions/model?id=${id}`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    createIcaoCondition(icaoSubject: IIcaoSubjectCondition): Observable<IIcaoSubjectCondition> {
        return this.http.post(`${this.apiUrl}icaoconditions/create`, icaoSubject)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    updateIcaoCondition(icaoSubject: IIcaoSubjectCondition): Observable<IIcaoSubjectCondition> {
        return this.http.put(`${this.apiUrl}icaoconditions/update`, icaoSubject)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    deleteIcaoCondition(icaoCondition: number): Observable<any> {
        return this.http.delete(`${this.apiUrl}icaoconditions/delete/${icaoCondition}`)
            .map((response: Response) => {
                return response;
            }).catch(this.handleError);
    }

    //DOAS
    getDoas(): Observable<IDoaPartial[]> {
        //This is a fix? related with IE that cached the get request always. Everytime that a request is sent, 
        //we need to sent something different, like the parameter that is not used on the Controller
        return this.http.get(`${this.apiUrl}doas/admin/partials/?timeRequest=${new Date().getTime()}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    //SERIES
    getRegisteredSeries(queryOptions: IQueryOptions): Observable<ISeriesPartial[]> {
        return this.http.post(`${this.apiUrl}series/page`, queryOptions)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    deleteSerie(serieId: number): Observable<any> {
        return this.http.delete(`${this.apiUrl}series/delete/${serieId}`)
            .map((response: Response) => {
                return response;
            }).catch(this.handleError);
    }

    serieCanBeDeleted(serieId: number): Observable<boolean> {
        return this.http.get(`${this.apiUrl}series/candelete/?serieId=${serieId}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getSerieModel(id: number): Observable<ISeriesPartial> {
        return this.http.get(`${this.apiUrl}series/getserie/?id=${id}`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getAllGeoRegions(): Observable<IGeoRegionPartial[]> {
        return this.http.get(`${this.apiUrl}geo/getGeoRegions`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    saveSeries(serie: ISeriesPartial): Observable<ISeriesPartial> {
        return this.http.post(`${this.apiUrl}series/create`, serie)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    updateSeries(serie: ISeriesPartial): Observable<ISeriesPartial> {
        return this.http.put(`${this.apiUrl}series/update`, serie)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    //Aerodrome Dissemination Category
    getRegisteredADC(queryOptions: IQueryOptions): Observable<IAerodromeDisseminationCategory[]> {
        return this.http.post(`${this.apiUrl}adc/page`, queryOptions)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getRegisteredAhpNoDC(queryOptions: IQueryOptions): Observable<IAhpNoDC[]> {
        return this.http.post(`${this.apiUrl}adc/sdo/page`, queryOptions)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getAhpNoDCTotalPending(): Observable<Number> {
        return this.http.get(`${this.apiUrl}adc/pending/count`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    saveADC(adc: IAerodromeDisseminationCategory): Observable<IAerodromeDisseminationCategory> {
        return this.http.post(`${this.apiUrl}adc/create`, adc)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getAhpModel(mid: string): Observable<IAhpNoDC> {
        return this.http.get(`${this.apiUrl}adc/getsdo/?id=${mid}`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getADCModel(id: number): Observable<IAerodromeDisseminationCategory> {
        return this.http.get(`${this.apiUrl}adc/getadc/?id=${id}`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    updateADC(adc: IAerodromeDisseminationCategory): Observable<IAerodromeDisseminationCategory> {
        return this.http.post(`${this.apiUrl}adc/update`, adc)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    //DOA Admin
    getRegisteredDoas(queryOptions: IQueryOptions): Observable<IDoaAdminPartial[]> {
        return this.http.post(`${this.apiUrl}doas/admin/page`, queryOptions)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getDoaAdmin(id: number): Observable<IDoaAdmin> {
        return this.http.get(`${this.apiUrl}doas/admin/find/?doaId=${id}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    copyDoaAdmin(id: number): Observable<IDoaAdmin> {
        return this.http.get(`${this.apiUrl}doas/admin/copy/?doaId=${id}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    saveDoaFile(data: any): Observable<any> {
        return this.http.post(`${this.apiUrl}doas/admin/uploadfile`, data)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    saveDoa(data: IDoaAdmin): Observable<IDoaAdmin> {
        return this.http.post(`${this.apiUrl}doas/admin/save`, data)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    validateDoaInCanada(data: IDoaAdmin): Observable<IDoaAdmin> {
        return this.http.post(`${this.apiUrl}doas/admin/validatedoaincanada`, data)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    existsDoaName(id: number, name: string): Observable<boolean> {
        return this.http.get(`${this.apiUrl}doas/admin/exists/?doaId=${id}&name=${name}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    deleteDoa(doaId: number): Observable<any> {
        return this.http.delete(`${this.apiUrl}doas/admin/delete/?doaId=${doaId}`)
            .map((response: Response) => {
                return response;
            }).catch(this.handleError);
    }

    getAllFirs(): Observable<IFIR[]> {
        return this.http.get(`${this.apiUrl}doas/admin/firs`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    validateDoaLocation(loc: string): Observable<string> {
        return this.http.get(`${this.apiUrl}doas/admin/validatepoint/?value=${loc}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    validateDoaGeoLocation(id: number, loc: string): Observable<IDoaPointModel> {
        return this.http.get(`${this.apiUrl}doas/admin/PointInDoa/?doaId=${id}&value=${loc}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    validateLocInRegion(locRegion: IRegionLocationModel): Observable<IDoaPointModel> {
        return this.http.post(`${this.apiUrl}doas/admin/PointInRegion/`, locRegion)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    validateDoaPoints(points: string): Observable<string> {
        return this.http.get(`${this.apiUrl}doas/admin/validatepoints/?value=${points}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getDoaBilingualRegion(): Observable<IDoaAdmin> {
        return this.http.get(`${this.apiUrl}doas/admin/bilingual/`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getDoaNorthernRegion(): Observable<IDoaAdmin> {
        return this.http.get(`${this.apiUrl}doas/admin/northern/`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getGeoFeatures(region: IRegionMapRender): Observable<string> {
        return this.http.post(`${this.apiUrl}doas/admin/GetGeoFeatures/`, region)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    //Geographic Regions
    getRegisteredGeoRegions(queryOptions: IQueryOptions): Observable<IGeoRegionPartial[]> {
        return this.http.post(`${this.apiUrl}geo/admin/page`, queryOptions)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    deleteGeoRegion(geoId: number): Observable<any> {
        return this.http.delete(`${this.apiUrl}geo/admin/delete/?geoId=${geoId}`)
            .map((response: Response) => {
                return response;
            }).catch(this.handleError);
    }

    getGeoRegionModel(id: number): Observable<IGeoRegion> {
        return this.http.get(`${this.apiUrl}geo/admin/find/?geoId=${id}`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    saveGeoRegion(data: IGeoRegion): Observable<IGeoRegion> {
        return this.http.post(`${this.apiUrl}geo/admin/save`, data)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    existsGeoRegionName(id: number, name: string): Observable<boolean> {
        return this.http.get(`${this.apiUrl}geo/admin/exists/?geoId=${id}&name=${name}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    // NSD Admin
    getNsdLeaves(): Observable<INsdPartial[]> {
        return this.http.get(`${this.apiUrl}nsdcategories/nsdleaves`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getNsdsByOrgId(id: number): Observable<INsdCategories> {
        return this.http.get(`${this.apiUrl}organizationnsdcategory/get/?ordId=${id}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getNsds(): Observable<INsdCategories> {
        //This is a fix? related with IE that cached the get request always. Everytime that a request is sent, 
        //we need to sent something different, like the parameter that is not used on the Controller
        return this.http.get(`${this.apiUrl}nsdcategories/GetNsds/?timeRequest=${new Date().getTime()}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getActiveNsds(): Observable<INsdCategories> {
        return this.http.get(`${this.apiUrl}nsdcategories/activensds`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    activateNsd(nsd: INsdManagementDto): Observable<INsdCategories> {
        return this.http.post(`${this.apiUrl}nsdcategories/activatensd/`, nsd)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getDisabledNsds(): Observable<INsdCategories> {
        return this.http.get(`${this.apiUrl}nsdcategories/disablednsds`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    removeFromAllOrganizations(nsd: INsdManagementDto): Observable<INsdManagementDto> {
        return this.http.post(`${this.apiUrl}nsdcategories/removefromallorganizations/`, nsd)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    disableNsd(nsd: INsdManagementDto): Observable<INsdManagementDto> {
        return this.http.post(`${this.apiUrl}nsdcategories/disablensd/`, nsd)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getActiveNsdsByOrganization(id: number): Observable<INsdCategories> {
        return this.http.get(`${this.apiUrl}nsdcategories/?orgId=${id}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getDisabledNsdsByOrganizationId(id: number): Observable<INsdCategories> {
        return this.http.get(`${this.apiUrl}nsdcategories/disablednsds/?orgId=${id}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    removeNsdFromOrganization(nsd: INsdManagementDto): Observable<INsdCategories> {
        return this.http.post(`${this.apiUrl}organizationnsdcategory/removensdsinorganizationbynsdid/`, nsd)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    addNsdToOrganization(nsd: INsdManagementDto): Observable<INsdCategories> {
        return this.http.post(`${this.apiUrl}organizationnsdcategory/addnsdtoorganization/`, nsd)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    //Config Values
    getRegisteredConfigValues(queryOptions: IQueryOptions): Observable<IConfigValuePartial[]> {
        return this.http.post(`${this.apiUrl}configvalues/admin/page`, queryOptions)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    deleteConfig(cat: string, name: string): Observable<any> {
        return this.http.delete(`${this.apiUrl}configvalues/admin/delete/?cat=${cat}&name=${name}`)
            .map((response: Response) => {
                return response;
            }).catch(this.handleError);
    }

    saveConfig(configRec: IConfigValuePartial): Observable<IConfigValuePartial> {
        return this.http.post(`${this.apiUrl}configvalues/admin/save`, configRec)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    existsConfigName(name: string): Observable<boolean> {
        return this.http.get(`${this.apiUrl}configvalues/admin/exists/?name=${name}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getConfigRecAdmin(cat: string, name: string): Observable<IConfigValuePartial> {
        return this.http.get(`${this.apiUrl}configvalues/admin/find/?cat=${cat}&name=${name}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    //Checklist
    getChecklistValue(): Observable<IConfigValuePartial> {
        return this.http.get(`${this.apiUrl}checklist/admin/findvalue`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    saveChecklistConfig(configRec: IConfigValuePartial): Observable<IConfigValuePartial> {
        return this.http.post(`${this.apiUrl}checklist/admin/save`, configRec)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    pushChecklist(): Observable<any> {
        return this.http.post(`${this.apiUrl}checklist/push`, null)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getRegisteredSeriesChecklistValues(queryOptions: IQueryOptions): Observable<IConfigValuePartial[]> {
        return this.http.post(`${this.apiUrl}checklist/admin/page`, queryOptions)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    deleteSeriesChecklist(serie: string): Observable<any> {
        return this.http.delete(`${this.apiUrl}checklist/admin/delete/?serie=${serie}`)
            .map((response: Response) => {
                return response;
            }).catch(this.handleError);
    }

    getSeriesChecklist(series: string): Observable<ISeriesChecklist> {
        return this.http.get(`${this.apiUrl}checklist/admin/findseries/?series=${series}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    verifySeriesChecklist(series: string): Observable<boolean> {
        return this.http.get(`${this.apiUrl}checklist/admin/verifyseries/?series=${series}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    validateFirsNames(names: string): Observable<boolean> {
        return this.http.get(`${this.apiUrl}checklist/admin/validatefirs/?names=${names}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    saveSeriesChecklist(seriesRec: ISeriesChecklist): Observable<ISeriesChecklist> {
        return this.http.post(`${this.apiUrl}checklist/admin/saveseries`, seriesRec)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    //GeoRef functions
    saveGeoFile(data: any): Observable<any> {
        return this.http.post(`${this.apiUrl}nof/ndsclients/uploadfile`, data)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    //getGeoFeatures(region: IRegionMapRender): Observable<string> {
    //    return this.http.post(`${this.apiUrl}nof/ndsclients/GetGeoFeatures/`, region)
    //        .map((response: Response) => {
    //            return response.json();
    //        }).catch(this.handleError);
    //}

    validateGeoLocation(loc: string): Observable<string> {
        return this.http.get(`${this.apiUrl}nof/ndsclients/validatepoint/?value=${loc}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    validateGeoPoints(points: string): Observable<string> {
        return this.http.get(`${this.apiUrl}nof/ndsclients/validatepoints/?value=${points}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }
    //End GeoRef functions

    //NDS Clients subscriptions
    getAerodromes(): Observable<[string, string]> {
        return this.http.get(this.app.apiUrl + `sdosubjects/aerodromes`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getNdsClient(id: number): Observable<INdsClient> {
        return this.http.get(`${this.apiUrl}nof/ndsclients/${id}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getNdsClients(queryOptions: IQueryOptions): Observable<INdsClientPartial[]> {
        return this.http.post(`${this.apiUrl}nof/ndsclients/page`, queryOptions)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    getNdsClientAddresses(): Observable<INdsClientFilter[]> {
        return this.http.get(`${this.apiUrl}nof/ndsclients`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    validateRegionInCanada(data: INdsClient): Observable<INdsClient> {
        return this.http.post(`${this.apiUrl}nof/validateregionincanada`, data)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    saveNdsClient(client: INdsClient): Observable<INdsClient> {
        debugger;
        return this.http.post(`${this.apiUrl}nof/ndsclients/`, client)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    updateNdsClient(client: INdsClient): Observable<INdsClient> {
        return this.http.put(`${this.apiUrl}nof/ndsclients/`, client)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    deleteNdsClient(clientId: number): Observable<any> {
        return this.http.delete(`${this.apiUrl}nof/ndsclients/${clientId}`)
            .map((response: Response) => {
                return response;
            }).catch(this.handleError);
    }

    sendNotamsToSubscriptions(clientIds: string): Observable<any> {
        let viewmodel = { subsClientIds: clientIds };
        return this.http.post(`${this.apiUrl}transition/queuejob/`, viewmodel)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    existsClientName(id: number, name: string): Observable<boolean> {
        return this.http.get(`${this.apiUrl}nof/ndsclients/exists/?Id=${id}&name=${name}`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    // AeroRDS
    getAeroRDSCacheStatus(): Observable<any> {
        return this.http.get(`${this.apiUrl}sdo/status`)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    pushAeroRDSGlobalSync(): Observable<any> {
        return this.http.post(`${this.apiUrl}sdo/globalsync`, null)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    pushAeroRDSEffectiveCacheSync(): Observable<any> {
        return this.http.post(`${this.apiUrl}sdo/effectivecachesync`, null)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    pushAeroRDSEffectiveCacheLoad(): Observable<any> {
        return this.http.post(`${this.apiUrl}sdo/effectivecacheload`, null)
            .map((response: Response) => {
                return response.json();
            }).catch(this.handleError);
    }

    //Map API
    getMapHealth(): Observable<boolean> {
        return this.http.get(this.apiUrl + `map/getmaphealth`).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    //PRIVATE SESSION
    private handleError(error: Response) {
        if (error.status === 401) {
            window.location.reload();
        }
       return Observable.throw(error.json());
    }
}
﻿namespace NavCanada.Core.SqlServiceBroker
{

    public class SqlServiceBrokerQueueSettings
    {
        public static readonly SqlServiceBrokerQueueSettings InitiatorToTaskEngineSsbSettings = new SqlServiceBrokerQueueSettings
        {
            SenderServiceName = "//navcanada.se.notamwiz/InitiatorToTaskEngine-SendService",
            ReceiverServiceName = "//navcanada.se.notamwiz/InitiatorToTaskEngine-ReceiveService",
            ReceiveQueueName = "InitiatorToTaskEngine-ReceiveQueue"
        };

        public static readonly SqlServiceBrokerQueueSettings TaskEngineToInitiatorSsbSettings = new SqlServiceBrokerQueueSettings();


        public string ContractName = "//navcanada.se.notamwiz/mto-queue-contract";
        public string MessageTypeName = "//navcanada.se.notamwiz/mto";
        public int ReadWaitTimeout = 60000; // sleep for 60 seconds when queues are empty
        public string ReceiveQueueName;
        public string ReceiverServiceName;
        public string SenderServiceName;
    }
}
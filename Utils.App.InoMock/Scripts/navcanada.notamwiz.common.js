﻿var outerLayout, middleLayout, innerLayout;

$(document).ready(function () {

    outerLayout = $('body').layout({
        center__paneSelector: ".outer-center"
    , west__paneSelector: ".outer-west"
    , east__paneSelector: ".outer-east"
    , north__togglerLength_open: 0
    , south__togglerLength_open: 0
    , showErrorMessages: false
    , north:{
        resizable: false,
        paneSelector: ".outer-north",
        spacing_open:0
    }
    , south: {
        resizable: false,
        paneSelector: ".outer-south",
        spacing_open:0
    }

    });

    

    middleLayout = $('#content').layout({
        center__paneSelector: ".middle-center"
     , west__paneSelector: ".middle-west"
     , east__paneSelector: ".middle-east"
     , south__paneSelector: ".middle-south"
     , north__paneSelector: ".middle-north"
     , north__togglerLength_open: 0
     , west__size: "275"
     , east__size: "400"
     , north__spacing_open:0
     , showErrorMessages: false
    });

    if ($('div.middle-center').length > 0) {
        innerLayout = $('div.middle-center').layout({
            center__paneSelector: ".inner-center"
        , west__paneSelector: ".inner-west"
        , east__paneSelector: ".inner-east"
        , north__paneSelector: ".inner-north"
        , south__paneSelector: ".inner-south"
        , north__togglerLength_open: 0
         , showErrorMessages: false
        });

        if ($('div.inner-center').length > 0) {
            nestedInnerLayout = $('div.inner-center').layout({
                center__paneSelector: ".nested-inner-center"
            , west__paneSelector: ".nested-inner-west"
            , east__paneSelector: ".nested-inner-east"
            , north__paneSelector: ".nested-inner-north"
            , south__paneSelector: ".nested-inner-south"
            , north__togglerLength_open: 0
            , west__size: "500"
            , showErrorMessages: false
            });
        }

    }

});
using System;
using System.Collections.Generic;
using System.Text;

namespace NAVTAM.NsdEngine.RWYCLSD
{
    public class RwyClsdFacilitator
    {
        public RwyClsdFacilitator(string name, string eFieldName)
        {
            Name = name;
            EFieldName = eFieldName;
        }

        public string Id { get; }
        public string Name { get; }
        public string EFieldName { get; }
        
    }

    public class RwyClsdReasons
    {
        public static Dictionary<string, RwyClsdFacilitator> Get(string language) => _dictionary[language];

        public Dictionary<string, RwyClsdFacilitator> this[string language] => _dictionary[language];

        public static bool HasType(string type)
        {
            return _dictionary["en"].ContainsKey(type);
        }

        static readonly Dictionary<string, Dictionary<string, RwyClsdFacilitator>> _dictionary = new Dictionary<string, Dictionary<string, RwyClsdFacilitator>>
        {
            {
                "en", new Dictionary<string, RwyClsdFacilitator>
                {
                    { "Re1", BuildReasonTypeEN("Painting", "PAINTING") },
                    { "Re2", BuildReasonTypeEN("Line Painting", "LINE PAINTING") },
                    { "Re3", BuildReasonTypeEN("Construction", "CONST") },
                    { "Re4", BuildReasonTypeEN("Maintenance", "MAINT") },
                    { "Re5", BuildReasonTypeEN("Crack Filling", "CRACK FILLING") },
                    { "Re6", BuildReasonTypeEN("Re-surfacing", "RESURFACING") },
                    { "Re7", BuildReasonTypeEN("Grading and/or Packing", "GRADING AND PACKING") },
                    { "Re8", BuildReasonTypeEN("Disabled Aircraft", "DISABLED ACFT") },
                    { "Re9", BuildReasonTypeEN("Other:", "OTHER") }
                }
            },
            {
                "fr", new Dictionary<string, RwyClsdFacilitator>
                {
                    { "Re1", BuildReasonTypeFR("PEINTURE", "PEINTURE") },
                    { "Re2", BuildReasonTypeFR("PEINTURE DE LIGNES", "PEINTURE DE LIGNES") },
                    { "Re3", BuildReasonTypeFR("CONST", "CONST") },
                    { "Re4", BuildReasonTypeFR("MAINT", "MAINT") },
                    { "Re5", BuildReasonTypeFR("COLMATAGE", "COLMATAGE") },
                    { "Re6", BuildReasonTypeFR("SURACAGE DU REVETEMENT", "SURACAGE DU REVETEMENT") },
                    { "Re7", BuildReasonTypeFR("NIVELAGE ET REMPLISSAGE", "NIVELAGE ET REMPLISSAGE") },
                    { "Re8", BuildReasonTypeFR("ACFT EN PANNE", "ACFT EN PANNE") },
                    { "Re9", BuildReasonTypeFR("AUTRE", "AUTRE") }
                }
            }
        };

        private static RwyClsdFacilitator BuildReasonTypeEN(string name, string eFieldName)
        {
            return new RwyClsdFacilitator(name, eFieldName);
        }

        private static RwyClsdFacilitator BuildReasonTypeFR(string name, string eFieldName)
        {
            return new RwyClsdFacilitator(name, eFieldName);
        }
    }
}    
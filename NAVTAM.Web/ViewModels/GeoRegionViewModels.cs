﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NAVTAM.ViewModels
{
    public class GeoRegionViewModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public RegionViewModel Region { get; set; }

        public List<DoaRegionChange> Changes { get; set; }
    }
}
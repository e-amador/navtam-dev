﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Core.TaskEngine.Triggers
{
    public class AeroRDSCacheLoadJob : ScheduleJobTriggerBase
    {
        protected override string GetScheduleTaskId() => ScheduleIdentity.AeroRdsEffectiveCacheLoadScheduler;
        protected override MessageTransferObjectId GetSheduleTaskMessageId() => MessageTransferObjectId.AeroRDSLoadCachedDataMsg;
    }
}

﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Helpers;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace NAVTAM.Helpers.XsrfValidator
{
    public class ForgeryTokenValidationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            try
            {
                ValidateRequestHeader(actionContext.Request);
            }
            catch( Exception)
            {
                OwinContext ctx = actionContext.Request.Properties["MS_OwinContext"] as OwinContext;
                if (!ctx.Environment.ContainsKey("StripAspCookie"))
                {
                    ctx.Environment.Add("StripAspCookie", true);
                }
                actionContext.Response =
                    actionContext.Request.CreateErrorResponse(
                        HttpStatusCode.BadRequest, "Malicious attempt detected!");
            }
        }

        public void ValidateRequestHeader(HttpRequestMessage request)
        {
            string cookieToken = "";
            string formToken = "";

            IEnumerable<string> tokenHeaders;
            if (request.Headers.TryGetValues("RequestVerificationToken", out tokenHeaders))
            {
                string[] tokens = tokenHeaders.First().Split(':');
                if (tokens.Length == 2)
                {
                    cookieToken = tokens[0].Trim();
                    formToken = tokens[1].Trim();
                }
            }
            AntiForgery.Validate(cookieToken, formToken);
        }

    }
}
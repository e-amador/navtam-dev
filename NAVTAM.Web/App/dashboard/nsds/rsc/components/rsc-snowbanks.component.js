"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SnowBanksComponent = void 0;
var core_1 = require("@angular/core");
var angular_l10n_1 = require("angular-l10n");
var forms_1 = require("@angular/forms");
var rsc_service_1 = require("../rsc.service");
var toastr_service_1 = require("../../../../common/toastr.service");
var SnowBanksComponent = /** @class */ (function () {
    function SnowBanksComponent(fb, translation, rscService, toastr) {
        this.fb = fb;
        this.translation = translation;
        this.rscService = rscService;
        this.toastr = toastr;
        this.mainForm = null;
        this.model = null;
        this.isReadOnly = false;
        this.cardinalOptions = {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('ClearedWidthComponent.Select')
            }
        };
    }
    SnowBanksComponent.prototype.ngOnInit = function () {
        this.isFormLoadingData = false;
        this.cardinalValues = this.model.parent.cardinalSideEnum;
        this.cardinalPointsByDirections = this.model.parent.cardinalPointsByDirections;
        this.configureReactiveForm();
    };
    SnowBanksComponent.prototype.ngOnChanges = function () {
    };
    SnowBanksComponent.prototype.ngOnDestroy = function () {
    };
    SnowBanksComponent.prototype.$ctrl = function (name) {
        return this.frmGroup.get(name);
    };
    SnowBanksComponent.prototype.configureReactiveForm = function () {
        var frmArrControls = this.mainForm.controls['frmSnowBanksArr'];
        this.frmGroup = this.fb.group({
            grpFirstSnowbank: this.fb.group({
                bankHeightFt: [{ value: this.model.firstSnowbank.bankHeight.bankHeightFt, disabled: this.isReadOnly }, []],
                bankHeightInch: [{ value: this.model.firstSnowbank.bankHeight.bankHeightInch, disabled: this.isReadOnly }, []],
                bankEdgeFt: [{ value: this.model.firstSnowbank.bankEdgeFt, disabled: this.isReadOnly }, []],
                bankSide: [{ value: this.model.firstSnowbank.bankSide, disabled: this.isReadOnly }, []],
            }),
            grpSecondSnowbank: this.fb.group({
                bankHeightFt: [{ value: this.model.secondSnowbank.bankHeight.bankHeightFt, disabled: this.isReadOnly }, []],
                bankHeightInch: [{ value: this.model.secondSnowbank.bankHeight.bankHeightInch, disabled: this.isReadOnly }, []],
                bankEdgeFt: [{ value: this.model.secondSnowbank.bankEdgeFt, disabled: this.isReadOnly }, []],
                bankSide: [{ value: this.model.secondSnowbank.bankSide, disabled: this.isReadOnly }, []],
            }),
        });
        frmArrControls.push(this.frmGroup);
    };
    SnowBanksComponent.prototype.onBankHeightFtChange = function (groupName) {
        if (groupName === 'first') {
            //Do something
        }
        else {
            //Do something
        }
    };
    SnowBanksComponent.prototype.onBankHeightInchChange = function (groupName) {
        if (groupName === 'first') {
            //Do something
        }
        else {
            //Do something
        }
    };
    SnowBanksComponent.prototype.onBankEdgeFtChange = function (groupName) {
        if (groupName === 'first') {
            //Do something
        }
        else {
            //Do something
        }
    };
    SnowBanksComponent.prototype.onBankSideChanged = function (groupName, e) {
        if (e.value) {
            var cardinalId = e.value;
            if (groupName === 'first') {
                var control = this.frmGroup.get('grpFirstSnowbank.bankSide');
                if (this.isFormLoadingData) {
                    cardinalId = control.value;
                }
                else {
                    if (cardinalId !== control.value) {
                        control.setValue(cardinalId);
                        control.markAsDirty();
                        this.model.firstSnowbank.bankSide = cardinalId;
                    }
                }
            }
            else {
                var control = this.frmGroup.get('grpFirstSnowbank.bankSide');
                if (this.isFormLoadingData) {
                    cardinalId = control.value;
                }
                else {
                    if (cardinalId !== control.value) {
                        control.setValue(cardinalId);
                        control.markAsDirty();
                        this.model.secondSnowbank.bankSide = cardinalId;
                    }
                }
            }
        }
    };
    //General functions
    SnowBanksComponent.prototype.isValidNumber = function (value) {
        return ((value != null) &&
            (value !== '') &&
            !isNaN(Number(value.toString())));
    };
    SnowBanksComponent.prototype.isANumber = function (str) {
        return !/\D/.test(str);
    };
    SnowBanksComponent.prototype.validateKeyPressNumber = function (evt) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!this.isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
        }
    };
    // End General functions
    //Export functions
    SnowBanksComponent.prototype.isFormValid = function () {
        return true;
    };
    SnowBanksComponent.prototype.exportComponentInternalData = function () {
        var first = {
            bankHeight: {
                bankHeightFt: this.model.firstSnowbank.bankHeight.bankHeightFt,
                bankHeightInch: this.model.firstSnowbank.bankHeight.bankHeightInch
            },
            bankEdgeFt: this.model.firstSnowbank.bankEdgeFt,
            bankSide: this.model.firstSnowbank.bankSide,
        };
        var second = {
            bankHeight: {
                bankHeightFt: this.model.secondSnowbank.bankHeight.bankHeightFt,
                bankHeightInch: this.model.secondSnowbank.bankHeight.bankHeightInch
            },
            bankEdgeFt: this.model.secondSnowbank.bankEdgeFt,
            bankSide: this.model.secondSnowbank.bankSide,
        };
        var data = {
            parent: this.model.parent,
            runway: this.model.runway,
            firstSnowbank: first,
            secondSnowbank: second,
        };
        return data;
    };
    SnowBanksComponent.prototype.clearValues = function () {
        alert('Clear Component Values: Snowbanks');
    };
    __decorate([
        core_1.Input('form'),
        __metadata("design:type", forms_1.FormGroup)
    ], SnowBanksComponent.prototype, "mainForm", void 0);
    __decorate([
        core_1.Input('model'),
        __metadata("design:type", Object)
    ], SnowBanksComponent.prototype, "model", void 0);
    __decorate([
        core_1.Input('isReadOnly'),
        __metadata("design:type", Boolean)
    ], SnowBanksComponent.prototype, "isReadOnly", void 0);
    SnowBanksComponent = __decorate([
        core_1.Component({
            selector: 'rsc-snowbanks',
            templateUrl: './app/dashboard/nsds/rsc/components/rsc-snowbanks.component.html'
        }),
        __param(3, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [forms_1.FormBuilder,
            angular_l10n_1.TranslationService,
            rsc_service_1.RscService, Object])
    ], SnowBanksComponent);
    return SnowBanksComponent;
}());
exports.SnowBanksComponent = SnowBanksComponent;
//# sourceMappingURL=rsc-snowbanks.component.js.map
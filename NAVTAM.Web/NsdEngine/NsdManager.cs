﻿using Core.Common.Geography;
using Microsoft.Ajax.Utilities;
using NavCanada.Core.Common.Common;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NavCanada.Core.Proxies.AeroRdsProxy.Model;
using NAVTAM.Helpers;
using NAVTAM.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Threading.Tasks;

namespace NAVTAM.NsdEngine
{
    public abstract class NsdManager : INsdManager
    {
        public NsdManager()
        {
        }

        public NsdManager(int categoryId, string categoryName, string version)
        {
            CategoryId = categoryId;
            CategoryName = categoryName;
            Version = version;
        }

        #region INsdManager implementation

        public int CategoryId { get; }
        public string CategoryName { get; }
        public string Version { get; }

        public string FullyQualifiedModelName => $"{GetType().Namespace}.{CategoryName}ViewModel";

        public ProposalViewModel CreateViewModel()
        {
            var viewModel = new ProposalViewModel(CategoryId, Version, CategoryName, GetDisplayName(), GetNsdHelpLink(), null);
            viewModel.Token = CreateTokenViewModel();
            return viewModel;
        }

        public ProposalViewModel CreateViewModelFromProposal(Proposal proposal)
        {
            var viewModel = new ProposalViewModel(CategoryId, Version, CategoryName, GetDisplayName(), GetNsdHelpLink(), proposal);
            viewModel.Token = DeserializeToken(proposal.Tokens);
            return viewModel;
        }

        public void FillProposalFromViewModel(Proposal proposal, ProposalViewModel viewModel)
        {
            viewModel.FillProposal(proposal);
            proposal.Tokens = SerializeObject(viewModel.Token);
        }

        public string SerializeObject(object obj)
        {
            return JsonConvert.SerializeObject(obj, new JsonSerializerSettings
            {
                ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver()
            });
        }

        public async Task UpdateContext(ProposalViewModel model, ProposalContext context)
        {
            var token = model.Token;

            // preprosess token data if needed
            PreProcessToken(token);

            // set location 
            var tokenLocation = GetNotamLocation(token) ?? "";
            context.Location = DMSLocation.FromKnownFormat(tokenLocation.ToUpper());
            if (context.Location == null)
                return;

            // set bilingual
            context.IsBilingual = IsInBilingualRegion(context.Location, context.Uow);

            // set cancelation
            context.IsCancelation = model.ProposalType == NotamType.C;

            // update particular nsd context
            // we've moved this to here because there are some NSD like the TWY CLSD 
            // that use particular data on the context to calculate Code23 and Code45
            await UpdateNsdContext(token, context);

            // icao subject
            context.IcaoSubject = await GetIcaoSubject(token, context);
            if (context.IcaoSubject == null)
                return;

            // icao subject condition
            context.IcaoCondition = await GetIcaoCondition(token, context, context.IcaoSubject.Id);
            if (context.IcaoCondition == null)
                return;

            // nearest sdo subject
            context.NearestSubject = GetNearestSdoSubject(token, context);

            var forbiddenItemEWords = await context.Uow.ConfigurationValueRepo.GetValueAsync("NOTAM", "NOTAMForbiddenWords");
            context.AddContext("ForbiddenItemEWords", forbiddenItemEWords);

        }

        public async Task<ValidationResult> Validate(ProposalViewModel model, ProposalContext context)
        {
            var token = model.Token;

            // validate start activity
            var validation = ValidateStartActivity(token);
            if (validation.HasErrors)
                return validation;

            // validate end validity date
            validation = ValidateEndValidity(token, context.MinValidityPeriod, context.IsCancelation);
            if (validation.HasErrors)
                return validation;

            // validate immediat or permanet or estimated NOTAM with an schedule. 
            validation = ValidateScheduleCombinations(token);
            if (validation.HasErrors)
                return validation;

            // validate location (coordinates)
            var location = context.Location;
            if (location == null)
                return ValidationResult.Fail(Resources.InvalidLocation);

            // validate DOA exists
            if (context.UserDoa == null)
                return ValidationResult.Fail(Resources.InvalidDOA);

            // validate location is inside the DOA
            if (!IsLocationWithinDOA(location, context.UserDoa))
                return ValidationResult.Fail(Resources.LocationOutsideDOA);

            // validate icao subject
            if (context.IcaoSubject == null)
                return ValidationResult.Fail(Resources.InvalidSubjectId);

            // validate icao subject
            if (context.IcaoCondition == null)
                return ValidationResult.Fail(Resources.InvalidICAOCondition);

            // originator
            if (token.Originator.IsNullOrWhiteSpace())
                return ValidationResult.Fail(Resources.ErrorOriginatorEmpty);

            // forbidden words
            var forbiddenWords = context.GetContext<string>("ForbiddenItemEWords");

            // ItemD
            if (!ValidateItemD(token, forbiddenWords))
                return ValidationResult.Fail(Resources.InvalidItemD);

            if (ContainsForbiddenWord(token.AmendPubMessage, forbiddenWords))
                return ValidationResult.Fail(Resources.AmenPubContainsForbiddenWords);

            if (ContainsForbiddenWord(token.AmendPubMessageFrench, forbiddenWords))
                return ValidationResult.Fail(Resources.AmenPubFrenchContainsForbiddenWords);

            if (!AftnHelper.TryValidateAftnChars(token.AmendPubMessage))
                return ValidationResult.Fail(Resources.AmenPubContainsForbiddenChars);

            if (!AftnHelper.TryValidateAftnChars(token.AmendPubMessageFrench))
                return ValidationResult.Fail(Resources.AmenPubFrenchContainsForbiddenChars);

            if (model.ReferredSeries != null && model.ReferredSeries.Length > 0)
            {
                var series = await GetSeries(token, context);
                if (series != model.ReferredSeries)
                    return ValidationResult.Fail(string.Format(Resources.ChangeOnSeries,model.ReferredSeries, series));
            }

            return ValidateModel(model, context);
        }

        public async Task<bool> GenerateIcao(ProposalViewModel model, ProposalContext context)
        {
            var token = model.Token;

            // transfer common properties from the toke to the model
            TransferCommonPropertiesFromToken(model);

            // Is Catch All
            model.IsCatchAll = IsCatchAll;

            // contains free text
            model.ContainFreeText = ContainFreeText;

            // Coordinates
            model.Coordinates = context.Location.ToDMS();

            // Fir
            model.Fir = GetFir(token, context);
            ValidateFir(model.Fir);

            // Item A
            model.ItemA = GenerateItemA(token, context);

            // Item D
            model.ItemD = GenerateItemD(token, context);

            // Item E
            var itemE = GenerateItemE(token, context);

            ValidateAftnCharacters(itemE);
            ValidateItemELength(itemE);

            model.ItemE = itemE.English;
            model.ItemEFrench = itemE.French;

            // Item F
            model.ItemF = GenerateItemF(token, context);

            // Item G
            model.ItemG = GenerateItemG(token, context);

            // Radius
            model.Radius = GetRadius(token, context);

            // Scope
            model.Scope = GetScope(token, context);

            // Series
            model.Series = await GetSeries(token, context);

            // Traffic
            model.Traffic = GetTraffic(token, context);

            // Code 23
            model.Code23 = GetCode23(token, context);
            model.IcaoSubjectId = context.IcaoSubject.Id;

            // Code 45
            model.Code45 = GetCode45(token, context);
            model.IcaoConditionId = context.IcaoCondition.Id;

            // Purpose
            model.Purpose = GetPurpose(token, context);

            // Lower limit
            model.LowerLimit = GetLowerLimit(token, context);

            // Upper limit
            model.UpperLimit = GetUpperLimit(token, context);

            return true;
        }

        public DbGeography GetProposalLocation(ProposalViewModel model)
        {
            var location = DMSLocation.FromKnownFormat(GetNotamLocation(model.Token)?.ToUpper());
            return location != null ? DbGeography.FromText(location.GeoPointText) : null;
        }

        #endregion

        public virtual TokenViewModel CopyTokenForGroupCancellation(TokenViewModel master, TokenViewModel current) => current;

        protected const int MaxUpperLimit = 999;
        protected const int EndValidyMaxDays = 92; // 92 days
        protected const string DefaultNofValue = "CYHQ"; // Todo: Ask why this text.. 

        protected abstract TokenViewModel CreateTokenViewModel();
        protected abstract TokenViewModel DeserializeToken(string tokens);

        protected virtual bool IsCatchAll => false;
        protected virtual bool ContainFreeText => false;

        protected virtual void PreProcessToken(TokenViewModel token)
        {
            token.AmendPubMessage = token.AmendPubMessage?.ToUpper();
            token.AmendPubMessageFrench = token.AmendPubMessageFrench?.ToUpper();
        }

        protected abstract Task UpdateNsdContext(TokenViewModel token, ProposalContext context);
        protected abstract ValidationResult ValidateModel(ProposalViewModel model, ProposalContext context);
        protected abstract NearSdoSubject GetNearestSdoSubject(TokenViewModel token, ProposalContext context);
        protected abstract string GetNotamLocation(TokenViewModel token);
        protected abstract string GetFir(TokenViewModel token, ProposalContext context);
        protected abstract string GenerateItemA(TokenViewModel token, ProposalContext context);
        protected virtual string GenerateItemD(TokenViewModel token, ProposalContext context) => !context.IsCancelation ? token.ItemD?.ToUpper() : string.Empty;
        protected abstract BilingualText GenerateItemE(TokenViewModel token, ProposalContext context);
        protected virtual string GenerateItemF(TokenViewModel token, ProposalContext context) => string.Empty;
        protected virtual string GenerateItemG(TokenViewModel token, ProposalContext context) => string.Empty;
        protected virtual int GetRadius(TokenViewModel token, ProposalContext context) => 5;
        protected abstract string GetScope(TokenViewModel token, ProposalContext context);
        protected abstract Task<string> GetSeries(TokenViewModel token, ProposalContext context);
        protected abstract string GetTraffic(TokenViewModel token, ProposalContext context);
        protected abstract string GetCode23(TokenViewModel token, ProposalContext context);
        protected abstract string GetCode45(TokenViewModel token, ProposalContext context);
        protected abstract string GetPurpose(TokenViewModel token, ProposalContext context);
        protected virtual int GetLowerLimit(TokenViewModel token, ProposalContext context) => 0;
        protected virtual int GetUpperLimit(TokenViewModel token, ProposalContext context) => 999;

        protected abstract string GetDisplayName();

        protected abstract string GetNsdHelpLink();

        protected virtual Task<IcaoSubject> GetIcaoSubject(TokenViewModel token, ProposalContext context)
        {
            return context.Uow.IcaoSubjectRepo.GetByCodeAsync(GetCode23(token, context));
        }

        protected virtual Task<IcaoSubjectCondition> GetIcaoCondition(TokenViewModel token, ProposalContext context, int icaoSubjectId)
        {
            return context.Uow.IcaoSubjectConditionRepo.GetByCodeAsync(icaoSubjectId, GetCode45(token, context));
        }

        protected static int RoundLowerLimitToFlightLevel(int limit)
        {
            return Math.Min(MaxUpperLimit, limit / 100);
        }

        protected static int RoundUpperLimitToFlightLevel(int limit)
        {
            if (limit < 100) return 1;
            else
            {
                int rem = 0;
                int div = Math.DivRem(limit, 100, out rem);
                if (rem != 0) div += 1;
                return Math.Min(MaxUpperLimit, div);
            }

            //return Math.Min(MaxUpperLimit, (limit < 100 ? 1 : ( limit / 100 + 1)));
        }

        protected static bool ContainsForbiddenWord(string text, string forbiddenWords)
        {
            if (string.IsNullOrEmpty(text))
                return false;

            //var words = text.Split(' ');
            //return words.Any(w => w.Length > 0 && forbiddenWords.IndexOf(w, StringComparison.OrdinalIgnoreCase) != -1);

            //This is the version that consider STILL as a forbidden word, in this case it is acceptable because AFTN will fail 
            //if it found any TIL inside of the message, doesn't matter if it is as part of a word

            var forbidden = forbiddenWords.Split('|');
            return forbidden.Any(f => f.Length > 0 && text.IndexOf(f, StringComparison.OrdinalIgnoreCase) > -1);

            // This is the version for not evaluating as an error, words as STILL, but here the word NNNNN won't be signal as an error
            //even as NNNN is included as a forbidden word
            //var forbidden = forbiddenWords.Split('|');
            //var words = (text ?? "").ToUpper().Split(SPACE, StringSplitOptions.RemoveEmptyEntries).ToList();
            //return forbidden.Any(f => f.Length > 0 && words.FindAll(w => w == f).Count > 0);
        }

        protected static bool IsLocationWithinDOA(DMSLocation location, DbGeography userDoa)
        {
            var geoPoint = DbGeography.PointFromText(location.GeoPointText, CommonDefinitions.Srid);
            return userDoa != null && geoPoint.Intersects(userDoa);
        }

        protected static T Deserialize<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json, new JsonSerializerSettings
            {
                ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver()
            });
        }

        protected static BilingualText GetAmendPubMessagePrefix(TokenViewModel token)
        {
            var amendPub = !string.IsNullOrWhiteSpace(token.AmendPubMessage) ? $"{token.AmendPubMessage.Trim()} " : string.Empty;
            var amendPubFrench = !string.IsNullOrWhiteSpace(token.AmendPubMessageFrench) ? $"{token.AmendPubMessageFrench.Trim()} " : string.Empty;
            return new BilingualText(amendPub, amendPubFrench, true);
        }

        protected static BilingualText GetItemEAerodromeNamePrefix(ProposalContext context, string name, string modifiedName = "")
        {
            var subject = context.NearestSubject.Subject;
            var designator = subject.Designator;

            if (!designator.Any(char.IsDigit))
                return new BilingualText();

            var ahpAttrs = GetCachedAttributes<Aerodrome>(subject);

            var isWaterAerodrome = context.Uow.SdoCacheRepo.IsWaterAerodrome(subject.Id);
            var isHeliport = "HP".Equals(ahpAttrs?.Type);
            var ahpName = (modifiedName.Length > 0) ? modifiedName.Trim() : name.Trim();
            return GetItemEPrefix(designator, ahpName, ahpAttrs?.ServedCity, isWaterAerodrome, isHeliport);
        }

        protected static BilingualText GetItemEPrefix(string designator, string name, string cityServed, bool waterRunways, bool isHeliport)
        {
            if (designator.Any(char.IsDigit))
            {
                var composedName = GetAerodromeComposedName(name, cityServed);
                var surfaceE = GetSurfaceTextEnglish(waterRunways, isHeliport);
                var surfaceF = GetSurfaceTextFrench(waterRunways, isHeliport);
                var prefixE = $"{designator} {composedName} {surfaceE}\n";
                var prefixF = $"{designator} {composedName} {surfaceF}\n";
                return new BilingualText(prefixE, prefixF);
            }
            return new BilingualText();
        }

        protected static async Task<string> GetNotamSeriesForSubject(IUow uow, SdoCache subject, string qCode, bool fir)
        {
            var dc = fir ? DisseminationCategory.National : await GetSubjectDisseminationCategory(uow, subject.Id, fir);
            return await GetNotamSeriesForDisseminationCategory(uow, subject.RefPoint, dc, qCode, (fir ? SeriesAllocationSubject.FIR : SeriesAllocationSubject.Aerodrome));
        }

        protected static async Task<string> GetNotamSeriesForDisseminationCategory(IUow uow, DbGeography point, DisseminationCategory dc, string qCode, SeriesAllocationSubject subject)
        {
            var region = await uow.GeoRegionRepo.GetByGeoLocationAsync(point);
            if (region == null)
                return "?";

            var series = await uow.SeriesAllocationRepo.GetByRegionIdAndCodeAndCategoryIdAsync(region.Id, qCode, dc);
            if (series.Count == 0)
                return "?";

            // try to find the first series to match the subject (fir or ahp)
//            var subject = fir ? SeriesAllocationSubject.FIR : SeriesAllocationSubject.Aerodrome;
            var first = series.FirstOrDefault(s => s.Subject == subject);
            if (first != null)
                return first.Series;

            // pick the second (1) if more than one and it is for an fir, otherwiese pick the first (0)
            var pickNo = series.Count > 1 && subject != SeriesAllocationSubject.Aerodrome ? 1 : 0;

            return series[pickNo].Series;
        }

        static async Task<DisseminationCategory> GetSubjectDisseminationCategory(IUow uow, string subjectId, bool fir)
        {
            var adc = await uow.AerodromeDisseminationCategoryRepo.GetByAhpMidAsync(subjectId);
            return adc != null ? adc.DisseminationCategory : DisseminationCategory.International;
        }

        protected static string CovertAerodromeDesignatorToItemA(string designator)
        {
            return (designator ?? "").Any(char.IsDigit) ? "CXXX" : designator;
        }

        protected static string GetLocationFir(DbGeography point, IUow uow)
        {
            var fir = uow.SdoCacheRepo.GetFirContaining(point);
            return fir?.Designator ?? string.Empty;
        }

        protected static IEnumerable<string> GetAlternateNearbyFirs(DbGeography point, string fir, IUow uow)
        {
            var twoNM = NMToMeters(2.0);
            return uow.SdoCacheRepo.GetFirs()
                      .Where(f => f.Fir != fir && f.Geography != null && f.Fir.StartsWith("CZ"))
                      .Select(f => new { Fir = f, Distance = f.Geography.Distance(point) } )
                      .Where(p => p.Distance <= twoNM)
                      .OrderBy(p => p.Distance)
                      .Select(p => p.Fir.Fir);
        }

        protected static IEnumerable<string> GetIntersectingOrNearbyFirs(DbGeography point, double radius, IUow uow)
        {
            var area = point.Buffer(NMToMeters(Math.Max(2, radius))); 
            return uow.SdoCacheRepo.GetFirs()
                      .Where(f => area.Intersects(f.Geography) && f.Fir.StartsWith("CZ"))
                      .Select(f => new { Fir = f, Distance = f.Geography.Distance(point) })
                      .OrderBy(p => p.Distance)
                      .Select(p => p.Fir.Fir);
        }

        protected static async Task<SubjectViewModel> GetSubjectViewModel(string subjectId, IUow uow)
        {
            var sdoCache = await uow.SdoCacheRepo.GetByIdAsync(subjectId);
            if (sdoCache == null)
                return null;

            var parentAhp = await GetParentAerodrome(sdoCache, uow) ?? sdoCache;

            return new SubjectViewModel()
            {
                SubjectGeoRefLat = sdoCache.RefPoint.Latitude ?? 0.0,
                SubjectGeoRefLong = sdoCache.RefPoint.Longitude ?? 0.0,
                Designator = sdoCache.Designator,
                Fir = sdoCache.Fir,
                Ad = parentAhp.Designator,
                SubjectMid = sdoCache.Id,
                SubjectId = sdoCache.Id,
                Subject = sdoCache
            };
        }

        static async Task<SdoCache> GetParentAerodrome(SdoCache subject, IUow uow)
        {
            if (subject.Type == SdoEntityType.Aerodrome || subject.ParentId == null)
                return subject;
            return await uow.SdoCacheRepo.GetByIdAsync(subject.ParentId);
        }

        protected static async Task<NearSdoSubject> GetNearestAerodrome(DbGeography geoLocation, IUow uow, double fromRadius = 5.0)
        {
            var maxRadius = 10000.0; 
            var radius = fromRadius;
            while (radius < maxRadius)
            {
                var radiusInMeters = GeoDataService.NMToMeters(radius);
                var nearbyAerodromes = await uow.SdoCacheRepo.GetNearbyAerodromesAsync(geoLocation, radiusInMeters);
                if (nearbyAerodromes.Count > 0)
                {
                    var selected = GetClosestAerodrome(nearbyAerodromes, radius);
                    if (selected != null)
                        return new NearSdoSubject(selected.SdoCache, GeoDataService.MetersToNM(selected.Distance), selected.DisseminationCategory);
                }
                radius += 5.0;
            }
            throw new Exception("Error: No aerodrome found!");
        }

        protected static async Task<List<NearSdoSubject>> GetNearestAerodromes(DbGeography geoLocation, IUow uow, double radius)
        {
            var radiusInMeters = GeoDataService.NMToMeters(radius);
            var nearbyAerodromes = await uow.SdoCacheRepo.GetNearbyAerodromesAsync(geoLocation, radiusInMeters);
            return nearbyAerodromes.Select(e => new NearSdoSubject(e.SdoCache, GeoDataService.MetersToNM(e.Distance), e.DisseminationCategory)).ToList();
        }

        protected static T GetCachedAttributes<T>(SdoCache subject) where T : class
        {
            try
            {
                return Deserialize<T>(subject.Attributes);
            }
            catch (Exception)
            {
                return null;
            }
        }

        static NearbyAerodromeDto GetClosestAerodrome(List<NearbyAerodromeDto> nearby, double radius)
        {
            if (radius > 5.0)
            {
                nearby.Sort(CompareNearbyAerodromesByDistance);
            }
            else
            {
                nearby.Sort(CompareNearbyAerodromesByCategoryAndDistance);
            }
            return nearby.FirstOrDefault();
        }

        static int CompareNearbyAerodromesByDistance(NearbyAerodromeDto a, NearbyAerodromeDto b) => a.Distance.CompareTo(b.Distance);

        static int CompareNearbyAerodromesByCategoryAndDistance(NearbyAerodromeDto a, NearbyAerodromeDto b)
        {
            var cmpCat = a.DisseminationCategory.CompareTo(b.DisseminationCategory);
            return cmpCat == 0 ? CompareNearbyAerodromesByDistance(a, b) : -cmpCat;
        }

        protected static double NMToMeters(double nm)
        {
            return nm / 0.000539957;
        }

        static void TransferCommonPropertiesFromToken(ProposalViewModel model)
        {
            var token = model.Token;
            model.Immediate = token.Immediate;
            model.Estimated = token.Estimated;
            model.Permanent = token.Permanent;
            model.Originator = token.Originator;
            model.OriginatorEmail = token.OriginatorEmail;
            model.OriginatorPhone = token.OriginatorPhone;
            model.StartActivity = token.StartActivity;
            model.EndValidity = GetEndValidity(token);
            model.Urgent = token.Urgent;
            model.Nof = model.Nof ?? DefaultNofValue;
        }

        static DateTime? GetEndValidity(TokenViewModel token)
        {
            if (token.Permanent)
                return null;

            if (!token.Estimated)
                return token.EndValidity;

            var startDate = token.StartActivity.HasValue ? token.StartActivity.Value : DateTime.UtcNow;
            var endDate = startDate.AddDays(EndValidyMaxDays);

            return token.EndValidity <= endDate ? token.EndValidity : endDate;
        }

        private ValidationResult ValidateStartActivity(TokenViewModel token)
        {
            if (!token.Immediate)
            {
                // must have value
                if (!token.StartActivity.HasValue)
                    return ValidationResult.Fail(Resources.InvalidStartActivity);

                // must be after now UTC
                if (token.StartActivity.Value < DateTime.UtcNow.AddSeconds(-59)) // ignore seconds during the comparison
                    return ValidationResult.Fail(Resources.InvalidStartActivity);
            }
            return ValidationResult.Succeed();
        }

        private ValidationResult ValidateEndValidity(TokenViewModel token, int mvp, bool isCancelation)
        {
            if (isCancelation)
                return ValidationResult.Succeed();

            if (!token.Permanent)
            {
                // must have an end validity
                if (!token.EndValidity.HasValue)
                    return ValidationResult.Fail(Resources.EndValidityRequired);

                var endDate = token.EndValidity.Value;

                if (!token.Immediate)
                {
                    var startDate = token.StartActivity.HasValue ? token.StartActivity.Value : DateTime.UtcNow;

                    // end validity passes (92 days) 
                    if (endDate > startDate.AddDays(EndValidyMaxDays))
                        return ValidationResult.Fail(Resources.ValidityPeriodRangeExceed);

                    // end date is within a minute from start date ?? mvp maybe?
                    if (endDate < startDate.AddMinutes(1))
                        return ValidationResult.Fail(Resources.ValidityPeriodRangeShort1);
                }
                else
                {
                    // end date is less than 'mvp' minutes of the start date
                    if (endDate < DateTime.UtcNow.AddMinutes(mvp))
                        return ValidationResult.Fail(Resources.ValidityPeriodRangeShort.Replace("30", mvp.ToString()));

                    // end validity passes (92 days) 
                    if (endDate > DateTime.UtcNow.AddDays(EndValidyMaxDays))
                        return ValidationResult.Fail(Resources.ValidityPeriodRangeExceed);

                }
            }

            return ValidationResult.Succeed();
        }

        static ValidationResult ValidateScheduleCombinations(TokenViewModel token)
        {
            if (!string.IsNullOrEmpty(token.ItemD) && (token.Immediate || token.Permanent || token.Estimated))
                return ValidationResult.Fail(Resources.InvalidScheduleCombination);
            return ValidationResult.Succeed();
        }

        static bool IsInBilingualRegion(DMSLocation location, IUow uow)
        {
            var geoPoint = DbGeography.PointFromText(location.GeoPointText, CommonDefinitions.Srid);
            return GeoDataService.IsInBilingualRegion(geoPoint, uow);
        }

        static bool ValidateItemD(TokenViewModel token, string forbiddenWords)
        {
            var itemD = (token.ItemD ?? string.Empty).ToUpper();
            return AftnHelper.TryValidateAftnChars(itemD) && !ContainsForbiddenWord(itemD, forbiddenWords);

            //if (token.ItemD.IsNullOrWhiteSpace())
            //    return true;

            //token.ItemD = token.ItemD.Trim();

            //return CommonValidators.ValidateSchedule(token.ItemD);
            /*
            var startActivity = token.StartActivity.HasValue ? token.StartActivity.Value : DateTime.UtcNow;
            var endValidity = token.EndValidity.HasValue ? token.EndValidity.Value : DateTime.MaxValue;

            return CommonValidators.ValidateTimeSchedule(token.ItemD, startActivity, endValidity);
            */
        }

        static void ValidateAftnCharacters(BilingualText bilingualText)
        {
            AftnHelper.ValidateAftnChars(bilingualText.English, Resources.ItemEEnglishInvalidChars);
            AftnHelper.ValidateAftnChars(bilingualText.French, Resources.ItemEFrenchInvalidChars);
        }

        const int MaxItemELength = 40000;

        static void ValidateItemELength(BilingualText bilingualText)
        {
            var totalLength = (bilingualText.English ?? "").Length + (bilingualText.French ?? "").Length;
            if (totalLength > MaxItemELength)
                throw new Exception(Resources.ItemETooLong);
        }

        static void ValidateFir(string fir)
        {
            if (string.IsNullOrEmpty(fir))
            {
                throw new Exception(Resources.NotamOutsideAllCanadianFirs);
            }
            if (!fir.StartsWith("CZ"))
            {
                throw new Exception(Resources.NotamInsideNonCanadianFir);
            }
        }

        protected static string GetAerodromeComposedName(string name, string cityServed) => !string.IsNullOrEmpty(cityServed) ? $"{cityServed}/{name}" : name;

        protected static string GetSurfaceTextEnglish(bool water, bool heliport) => GetSurfaceText(water ? "(WATER)" : string.Empty, heliport ? "(HELI)" : string.Empty);

        protected static string GetSurfaceTextFrench(bool water, bool heliport) => GetSurfaceText(water ? "(HYDRO)" : string.Empty, heliport ? "(HELI)" : string.Empty);

        protected static string GetSurfaceText(string surface, string type) => $"{type} {surface}".Trim();
    }

    internal static class NsdManagerExt
    {
        internal static DbGeography GeoLocation(this ProposalContext context)
        {
            return DbGeography.PointFromText(context.Location.GeoPointText, CommonDefinitions.Srid);
        }
    }
}

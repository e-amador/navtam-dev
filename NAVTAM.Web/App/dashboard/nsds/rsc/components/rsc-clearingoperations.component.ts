﻿
import { Component, OnInit, OnChanges, OnDestroy, Inject, Input } from '@angular/core';
import { TranslationService } from 'angular-l10n';
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms';
import { RscService } from '../rsc.service';
import { ICleaningOperationsModel, IBilingualText } from '../rsc.model';
import { TOASTR_TOKEN, Toastr } from '../../../../common/toastr.service';
import * as moment from 'moment';

@Component({
    selector: 'rsc-clearing-operations',
    templateUrl: './app/dashboard/nsds/rsc/components/rsc-clearingoperations.component.html'
})


export class ClearingOperationsComponent {
    @Input('form') mainForm: FormGroup = null;
    @Input('isReadOnly') isReadOnly: boolean = false;
    @Input('model') model: ICleaningOperationsModel = null;

    frmGroup: FormGroup;
    isFormLoadingData: boolean;

    isClearingUnderway: boolean;

    constructor(
        private fb: FormBuilder,
        public translation: TranslationService,
        private rscService: RscService,
        @Inject(TOASTR_TOKEN) private toastr: Toastr) {
    }

    ngOnInit() {
        this.isFormLoadingData = false;
        this.configureReactiveForm();
    }

    ngOnChanges() {
    }

    ngOnDestroy() {
    }

    clearingUnderwayClick() {
        if (this.isClearingUnderway) {
            this.isClearingUnderway = false;
        } else {
            this.isClearingUnderway = true;
        }
    }

    setClearingCompletedBy() {
    
    }

    setClearingStartBy() {

    }

    private configureReactiveForm() {
        const frmArrControls = <FormArray>this.mainForm.controls['frmCleaningOperationsArr'];

        this.frmGroup = this.fb.group({
            startAt: [{ value: this.model.startAt, disabled: this.isReadOnly }, []],
            completedBy: [{ value: this.model.completedBy, disabled: this.isReadOnly }, []],
            remarks: [{ value: this.model.remarks.english, disabled: this.isReadOnly }, []],
            clearingUnderway: [{ value: this.model.clearingUnderway, disabled: this.isReadOnly }, []],
        });

        frmArrControls.push(this.frmGroup);
    }

    $ctrl(name: string): AbstractControl {
        return this.frmGroup.get(name);
    }

    public validateKeyPressHours(evt: any, ctrlName: string) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var num = +key;
            var pos: number = (<any>evt).currentTarget.selectionStart;
            if (pos === 0) { //val.length
                //First number
                if (key !== '0' && key !== '1' && key !== '2') {
                    evt.returnValue = false;
                    if (evt.preventDefault) evt.preventDefault();
                    return;
                }
            } else if (pos === 1) { //val.length === 1
                //Second number
                const fc = this.$ctrl(ctrlName);
                const val: string = fc.value;
                if (val[0] === '2' && num > 3) {
                    evt.returnValue = false;
                    if (evt.preventDefault) evt.preventDefault();
                    return;
                }
            } else if (pos === 2) { //val.length === 2
                //Third number
                if (num > 5) {
                    evt.returnValue = false;
                    if (evt.preventDefault) evt.preventDefault();
                    return;
                }
            }
        }
    }

    //Export functions
    public isFormValid(): boolean {

        return true;
    }

    public exportComponentInternalData(): ICleaningOperationsModel {
        let data: ICleaningOperationsModel = {
            parent: this.model.parent,
            clearingUnderway: this.model.clearingUnderway,
            startAt: this.model.startAt,
            completedBy: this.model.completedBy,
            remarks: this.model.remarks,
        };
        return data;
    }

    public clearValues(): void {
        alert('Clear component values: Cleaning Operations');
    }

}

  function isANumber(str: string): boolean {
        return !/\D/.test(str);
  }
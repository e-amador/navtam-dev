﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NAVTAM.ViewModels;
using RSC.Business;
using RSC.Business.SDO;
using RSC.Model;
using RSC.Model.Enums;
using RSCProposal = RSC.Model.RSCProposal;

namespace NAVTAM.NsdEngine.RSC.V10
{
    public class RSCNsdManager : NsdManager
    {
        public RSCNsdManager(int categoryId, string categoryName, string version) : base(categoryId, categoryName, version)
        {

        }

        protected override TokenViewModel CreateTokenViewModel()
        {
            return new RSCViewModel();
        }

        protected override TokenViewModel DeserializeToken(string tokens)
        {
            return Deserialize<RSCViewModel>(tokens);
        }

        protected override string GenerateItemA(TokenViewModel token, ProposalContext context)
        {
            return CovertAerodromeDesignatorToItemA(context.RSCContext().RSCSdoCacheSubject.Designator);
        }

        protected override BilingualText GenerateItemE(TokenViewModel token, ProposalContext context)
        {
            var rscToken = token as RSCViewModel;
            var dummyMode = ConfigurationManager.AppSettings["RSC_DUMMY_MODE"] == "1";

            var rscEngine = new RSCEngine(new SDOCacheRepository(context.Uow), dummyMode);
            var proposal = context.RSCContext().RSCProposal;
            var response = rscEngine.Process(proposal, true);

            var itemE = new BilingualText()
            {
                English = response.RSC.EnglishRSC,
                French = response.RSC.FrenchRSC
            };

            return itemE;
        }

        protected override string GetCode23(TokenViewModel token, ProposalContext context)
        {
            return "FA";
        }

        protected override string GetCode45(TokenViewModel token, ProposalContext context)
        {
            return "XX";
        }

        protected override string GetDisplayName()
        {
            //to do
            return "Runway Surface Condition";
        }

        protected override string GetFir(TokenViewModel token, ProposalContext context)
        {
            return context.RSCContext().RSCSdoCacheSubject.Fir;
        }

        protected override NearSdoSubject GetNearestSdoSubject(TokenViewModel token, ProposalContext context)
        {
            return (context.RSCContext() != null) ? new NearSdoSubject(context.RSCContext().RSCSdoCacheSubject, 0.0) : null;
            //return new NearSdoSubject(context.RSCContext().RSCSdoCacheSubject, 0.0);
        }

        protected override string GetNotamLocation(TokenViewModel token)
        {
            return (token as RSCViewModel).SubjectLocation;
        }

        protected override string GetNsdHelpLink()
        {
            //fake data
            return Resources.MultiObstHelpLink;
        }

        protected override string GetPurpose(TokenViewModel token, ProposalContext context)
        {
            //fake data
            return "NBO";
        }

        protected override string GetScope(TokenViewModel token, ProposalContext context)
        {
            return "A";
        }

        protected override async Task<string> GetSeries(TokenViewModel token, ProposalContext context)
        {
            var subject = context.RSCContext().RSCSdoCacheSubject;
            var dc = await context.Uow.AerodromeDisseminationCategoryRepo.GetByAhpMidAsync(subject.Id);
            return await GetNotamSeriesForDisseminationCategory(context.Uow, subject.RefPoint,dc.DisseminationCategory,"FA",SeriesAllocationSubject.RSC);
            //return await GetNotamSeriesForSubject(context.Uow, context.RSCContext().RSCSdoCacheSubject, "FA", false);
        }

        static Task<string> GetDefaultSeries(ProposalContext context)
        {
            //fake data
            var sdoCache = context.RSCContext().RSCSdoCacheSubject;
            return GetNotamSeriesForSubject(context.Uow, sdoCache, context.IcaoSubject.Code, sdoCache.Type == SdoEntityType.Fir);
        }


        protected override string GetTraffic(TokenViewModel token, ProposalContext context)
        {
            return "IV";
        }

        protected override async Task UpdateNsdContext(TokenViewModel token, ProposalContext context)
        {
            var rscToken = token as RSCViewModel;

            // Add SdoSubject
            var subjectId = (token as RSCViewModel).SubjectId;
            var subject = await context.Uow.SdoCacheRepo.GetByIdAsync(subjectId);

            var rscCont = new RSCContext()
            {
                RSCSdoCacheSubject = subject,
                RSCProposal = CreateRSCProposal(rscToken)
            };

            context.AddContext("RSCContext", rscCont);
        }

        protected override ValidationResult ValidateModel(ProposalViewModel model, ProposalContext context)
        {
            var rscEngine = new RSCEngine(new SDOCacheRepository(context.Uow), true);
            var proposalContext = rscEngine.GetProposalContext((global::RSC.Model.RSCProposal)context.RSCContext().RSCProposal);
            var rscValidator = new RSCValidator(proposalContext);

            var messages = new List<RSCResponseMessage>();
            rscValidator.ExecuteValidations(message => messages.Add(message));
            var lastMessage = messages.LastOrDefault();

            var failed = lastMessage?.Status == ResponseStatus.Error;

            return failed ? ValidationResult.Succeed() : ValidationResult.Fail(lastMessage.Description);
        }

        static RSCProposal CreateRSCProposal(RSCViewModel token)
        {
            return new global::RSC.Model.RSCProposal()
            {
                Version = "0.1",
                Created = DateTime.UtcNow,
                Origin = "NES",
                Aerodrome = token.Aerodrome,
                ConditionsChangingRapidly = GetConditionsChangingRapidly(token),
                Runways = token.RSCRunways.ToArray(),
                Taxiways = token.Taxiways?.ToFreeformText(),
                Aprons = token.Aprons?.ToFreeformText(),
                GeneralRemarks = token.GeneralRemarks?.ToFreeformText()
            };
        }

        static Telephone GetConditionsChangingRapidly(RSCViewModel token)
        {
            if (!token.ConditionsChanging)
                return null;

            var telLen = (token.Telephone ?? "").Length;

            return new Telephone
            {
                AreaCode = telLen > 3 ? token.Telephone.Substring(0, 3) : token.Telephone,
                Number = telLen > 3 ? token.Telephone.Substring(3) : string.Empty,
                Extension = token.Extension
            };
        }
    }

    internal class RSCContext
    {
        public SdoCache RSCSdoCacheSubject { get; set; }
        public RSCProposal RSCProposal { get; set; }
    }

    static class RSCNsdManagerExt
    {
        internal static RSCContext RSCContext(this ProposalContext context) => context.GetContext<RSCContext>("RSCContext");
        internal static FreeformText ToFreeformText(this BilingualText bt) => new FreeformText { EnglishText = bt.English, FrenchText = bt.French };
        internal static BilingualText ToBilingualText(this FreeformText fft) => new BilingualText { English = fft.EnglishText, French = fft.FrenchText };
    }
}
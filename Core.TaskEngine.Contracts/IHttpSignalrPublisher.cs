﻿using System.Collections.Specialized;

namespace NavCanada.Core.TaskEngine.Contracts
{
    public interface IHttpSignalrPublisher
    {
        string PostRequest(NameValueCollection requestParams);
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utils.Libs.SchedulerParser;
using System;
using NotamWizWeb = NavCanada.Applications.NotamWiz.Web.NSDs;
using FParsec;
using Microsoft.FSharp.Core;

namespace Apps.Web.NotamWiz.Test.NSDs.CommonValidators
{
    [TestClass]
    public class ScheduleValidatorTest
    {
        #region TimeOnly

        [TestMethod]
        public void ScheduleValidator_TimeOnly_ShouldReturnSuccess_WhenH24()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("H24", new DateTime(2017, 01, 01, 00, 00, 00), new DateTime(2017, 04, 01, 23, 59, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_TimeOnly_ShouldReturnSuccess_WhenDAILY()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("DAILY", new DateTime(2017, 01, 01, 00, 00, 00), new DateTime(2017, 04, 01, 23, 59, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_TimeOnly_ShouldReturnSuccess_WhenTimeRange()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("0100-0200", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_TimeOnly_ShouldReturnSuccess_WhenDailyTimeRange()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("DAILY 0100-0200", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_TimeOnly_ShouldReturnSuccess_WhenSunriseSunset()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("SR-SS", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_TimeOnly_ShouldReturnSuccess_WhenCaseDailySunriseSunset()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("DAILY SR-SS", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_TimeOnly_ShouldReturnSuccess_WhenSunriseMinusSunsetPlus()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("SR MINUS10-SS PLUS10", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_TimeOnly_ShouldReturnSuccess_WhenDailySunriseMinusSunsetPlusAndSpaces()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("DAILY SR MINUS10-SS PLUS10", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_TimeOnly_ShouldReturnSuccess_WhenDailySunriseSunsetPlusAndTimeRange()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("DAILY SR MINUS10-SS PLUS10 0100-0200", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_TimeOnly_ShouldReturnSuccess_WhenDailyTimeRangeAndSunsetPLusAndTimeRange()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("DAILY 0100-SS PLUS20 0320-0200", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_TimeOnly_ShouldReturnSuccess_WhenDailyWithMultipleTimeRangeAndSunsetPlus()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("DAILY 0100-SS PLUS20 0320-0430 0440-0500", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 05, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_TimeOnly_ShouldReturnSuccess_WhenDailyWithMultipleTimeRangeAndSunsetPlusandEXC()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("DAILY 0100-SS PLUS20 0320-0430 0440-0450, EXC JAN 10-20", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 04, 50, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_TimeOnly_ShouldReturnFailure_WhenDAILY()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("DAILY", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 01, 01, 01));
            Assert.IsTrue(scheduleValidatorResult == false);

        }
        [TestMethod]
        public void ScheduleValidator_TimeOnly_ShouldReturnFailure_WhenH24()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("H24", new DateTime(2017, 01, 01, 00, 00, 00), new DateTime(2017, 04, 01, 00, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == false);
        }

        [TestMethod]
        public void ScheduleValidator_TimeOnly_ShouldReturnFailure_WhenStTimeisLargerThanEndTime()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("0202-0101", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == false);
        }

        [TestMethod]
        public void ScheduleValidator_TimeOnly_ShouldReturnFailure_WhenTimeisinChronologicalOrder() 
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("0400-0500 0202-0300", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == false);
        }

        [TestMethod]
        public void ScheduleValidator_TimeOnly_ShouldReturnFailure_WhenTimeWithCalanderInclude()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("0100-0200, JAN 4 0100-0200", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == false);
        }

        [TestMethod]
        public void ScheduleValidator_TimeOnly_ShouldReturnFailure_WhenTimeWithWeekDayInclude()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("0100-0200, SUN MON", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == false);
        }
        [TestMethod]
        public void ScheduleValidator_TimeOnly_ShouldReturnFailure_WhenTimeWithH24Include()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("H24 0100-0200", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == false);
        }
        // leave for esteban 
        //[TestMethod]
        //public void ScheduleValidator_TimeOnly_ShouldReturnFailure_WhenTimeWithSunsetMinus()
        //{
        //    bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("DAILY  0100-SS MINUS10", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
        //    Assert.IsTrue(scheduleValidatorResult == false);
        //}

        [TestMethod]
        public void ScheduleValidator_TimeOnly_ShouldReturnFailure_WhenTimeWithSunrisePlus()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("DAILY  SR PLUS10-0100", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == false);
        }
        #endregion

        #region CalanderOnly

        //// needs to be fixed by estbain
        //[TestMethod]
        //public void ScheduleValidator_Calendar_ShouldParseDayRange_WhenSingleDate()
        //{
        //    bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("JAN 08", new DateTime(2017, 01, 08, 00, 00, 00), new DateTime(2017, 01, 08, 23, 59, 00));
        //    Assert.IsTrue(scheduleValidatorResult == true);
        //}

        //// needs to be fixed by estbain
        //[TestMethod]
        //public void ScheduleValidator_Calendar_ShouldParseDayRange_WhenDateRange()
        //{
        //    bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("JAN 08-10", new DateTime(2017, 01, 08, 00, 00, 00), new DateTime(2017, 01, 10, 23, 59, 00));
        //    Assert.IsTrue(scheduleValidatorResult == true);
        //}
        [TestMethod] 
        public void ScheduleValidator_Calendar_ShouldParseDayRange_WhenSingleDateAndSingleTime()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("JAN 08 0100-0200", new DateTime(2017, 01, 08, 01, 00, 00), new DateTime(2017, 01, 08, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_Calendar_ShouldParseDayRange_WhenMultipeDatesAndSingleTime()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("JAN 01-10 13-28 0100-0200", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 01, 28, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_Calendar_ShouldParseDayRange_WhenSingleDateAndMultiTimes()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("JAN 10-20 0100-0200 0300-0400", new DateTime(2017, 01, 10, 01, 00, 00), new DateTime(2017, 01, 20, 04, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_Calendar_ShouldParseDayRange_WhenMultiDateAndMultiTimes()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("JAN 05-10 12-15 21 23 0100-0200", new DateTime(2017, 01, 05, 01, 00, 00), new DateTime(2017, 01, 23, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_Calendar_WhenSingleDateAndSingleTimesUsingSunriseAndSunset()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("FEB 01-10 SR-SS", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_Calendar_WhenSingleDateAndSingleTimesUsingSunriseAndSunsetWithPlusAndMinus()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("FEB 01-10 SR MINUS10-SS PLUS20", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_Calendar_WhenSingleDateAndMultiTimesUsingSunriseAndSunset()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("FEB 01-10 0100-0200 0300-0400 SR-SS", new DateTime(2017, 02, 01, 01, 00, 00), new DateTime(2017, 02, 10, 04, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_Calendar_ShouldParseContinues_WhenRangeDateAndTimeWith4Digits()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("JAN 01 0100-APR 01 0200", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_Calendar_ShouldParseDatesandTimesRanges()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("JAN 01-28 0100-0200 0300-0400,FEB 01 0100-APR 01 0200", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_Calendar_ShouldEXCParseDatesandTimesRanges()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("EXC JAN 08-18,EXC FEB 10-FEB 23", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_Calendar_ShouldReturnFailure_DateRanges()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("JAN 08-18, FEB 10-MAR 02", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == false);
        }

        [TestMethod]
        public void ScheduleValidator_Calendar_ShouldReturnFailure_DatesandTimesRanges()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("JAN 08-18 0100-0200 0300-0400, FEB 10 0100-FEB 23 0909", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == false);
        }

        [TestMethod]
        public void ScheduleValidator_Calendar_ShouldReturnFailure_WhenSingleDateAndSingleTime()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("JAN 08 0100-0200", new DateTime(2017, 01, 08, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == false);
        }

        #endregion

        #region WeekOnly
        [TestMethod]
        public void ScheduleValidator_WeekDay_ShouldReturnSuccess_WhenSingleDay()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("SUN", new DateTime(2017, 01, 01, 00, 00, 00), new DateTime(2017, 04, 02, 23, 59, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }


        [TestMethod]
        public void ScheduleValidator_WeekDay_ShouldReturnSuccess_WhenMultipleDays()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("MON TUE WED", new DateTime(2017, 01, 02, 00, 00, 00), new DateTime(2017, 04, 03, 23, 59, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_WeekDay_ShouldReturnSuccess_WhenMultipleDaysAndSingleMinutesRange()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("THU FRI 0100-0200", new DateTime(2017, 01, 05, 01, 00, 00), new DateTime(2017, 04, 06, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_WeekDay_ShouldReturnSuccess_WhenMultipleDaysAndMultipleMinutesSSSRRange()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("THU FRI 0100-0200 SR MINUS10-SS PLUS10", new DateTime(2017, 01, 05, 01, 00, 00), new DateTime(2017, 04, 06, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }


        [TestMethod]
        public void ScheduleValidator_WeekDay_ShouldReturnSuccess_WhenRangeDay()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("SUN-SAT", new DateTime(2017, 01, 01, 00, 00, 00), new DateTime(2017, 04, 02, 23, 59, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_WeekDay_ShouldReturnSuccess_WhenMultipleRangeDays()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("SUN-TUE THU-SAT", new DateTime(2017, 01, 01, 00, 00, 00), new DateTime(2017, 04, 02, 23, 59, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_WeekDay_ShouldReturnSuccess_WhenMultipleRangeDaysAndSingleMinutesRange()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("SUN-TUE THU-SAT 0100-0200", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_WeekDay_ShouldReturnSuccess_WhenMultipleRangeDaysAndMultipleMinutesRange()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("SUN-TUE THU-SAT 0100-0200 SR MINUS10-SS PLUS10", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_WeekDay_ShouldReturnSuccess_WhenSingleDayAndSingleRangeDay()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("SUN WED-FRI", new DateTime(2017, 01, 01, 00, 00, 00), new DateTime(2017, 04, 02, 23, 59, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_WeekDay_ShouldReturnSuccess_WhenMultipleDaysAndMultipleRangeDays()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("SUN MON TUE THU-SAT", new DateTime(2017, 01, 01, 00, 00, 00), new DateTime(2017, 04, 02, 23, 59, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_WeekDay_ShouldReturnSuccess_WhenMultipleDaysAndMultipleRangeDaysAndMultipleMinutes()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("SUN MON TUE THU-SAT 0100-0200 SR MINUS10-SS PLUS10", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_WeekDay_ShouldReturnSuccess_WhenMultipleDaysAndMultipleRangeDaysWithoudMinutes()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("SUN MON TUE THU-SAT", new DateTime(2017, 01, 01, 00, 00, 00), new DateTime(2017, 04, 01, 23, 59, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }


        [TestMethod]
        public void ScheduleValidator_WeekDay_ShouldReturnSuccess_WhenMultipleDaysAndSingleMinutesRangeAndExc()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("THU FRI 0100-0200,EXC JAN 04", new DateTime(2017, 01, 05, 01, 00, 00), new DateTime(2017, 04, 06, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == true);
        }

        [TestMethod]
        public void ScheduleValidator_WeekDay_ShouldReturnFailure_WhenSingleDay()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("SUN", new DateTime(2017, 01, 01, 01, 00, 00), new DateTime(2017, 04, 01, 02, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == false);
        }

        [TestMethod]
        public void ScheduleValidator_WeekDay_ShouldReturnFailur_WhenMultipleDays()
        {
            bool scheduleValidatorResult = NotamWizWeb.CommonValidators.ValidateTimeSchedule("MON TUE WED", new DateTime(2017, 01, 01, 00, 00, 00), new DateTime(2017, 04, 01, 00, 00, 00));
            Assert.IsTrue(scheduleValidatorResult == false);
        }

        #endregion

        private string CastFailure(CharParsers.ParserResult<Common.ParserResult, Unit> fshartObj)
        {
            var error = ((CharParsers.ParserResult<Common.ParserResult, Unit>.Failure)fshartObj).Item1;
            return error.Replace("\r\n", "").Replace("\n", "").Replace("\r", "");
        }

        private Common.ParserResult CastSuccess(CharParsers.ParserResult<Common.ParserResult, Unit> fshartObj)
        {
            return ((CharParsers.ParserResult<Common.ParserResult, Unit>.Success)fshartObj).Item1 as Common.ParserResult;
        }


    }

}

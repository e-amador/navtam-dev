﻿using System.IO;
using System.Xml.Serialization;

namespace RSC.WebService.Helpers
{
    public class XMLSerializer
    {
        public static T Deserialize<T>(string input) where T : class
        {
            var serializer = new XmlSerializer(typeof(T));
            using (var reader = new StringReader(input))
            {
                return (T)serializer.Deserialize(reader);
            }
        }

        public static string Serialize<T>(T obj)
        {
            if (obj == null)
                return string.Empty;

            var xmlSerializer = new XmlSerializer(obj.GetType());
            using (var textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, obj);
                return textWriter.ToString();
            }
        }
    }
}
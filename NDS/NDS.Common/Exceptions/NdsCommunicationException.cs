﻿using System;

namespace Nds.Common.Exceptions
{
    public class NdsCommunicationException : Exception
    {
        public NdsCommunicationException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}

﻿using NavCanada.Core.Data.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Domain.Model.Entitities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.EF.Repositories
{
    public class LogRepo : Repository<Log>, ILogRepo
    {
        public LogRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public Task<List<Log>> GetByStartandEndDate(DateTime startTime, DateTime endTime)
        {
            return DbSet.Where(e => e.TimeStamp > startTime && e.TimeStamp < endTime).ToListAsync();
        }

        public Task<List<Log>> GetLogsBeforeDate(DateTime date, int total)
        {
            return DbSet.Where(e => e.TimeStamp <= date).OrderByDescending(e => e.TimeStamp).Take(total).ToListAsync();
        }
    }
}
﻿/**
 * System configuration for Angular samples
 * Adjust as necessary for your application needs.
 */

(function (global) {
  System.config({
    paths: {
      // paths serve as alias
      'npm:': '/node_modules/',
      'ctrls:': '/app/common/components/'
    },
    // map tells the System loader where to look for things
    map: {
      // our app is within the app folder
      app: '/app',
      // angular bundles
      '@angular/core': 'npm:@angular/core/bundles/core.umd.js',
      '@angular/common': 'npm:@angular/common/bundles/common.umd.js',
      '@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
      '@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
      '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
      '@angular/http': 'npm:@angular/http/bundles/http.umd.js',
      '@angular/router': 'npm:@angular/router/bundles/router.umd.js',
      '@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',
      'angular-l10n': 'node_modules/angular-l10n/bundles/angular-l10n.umd.js',
        // other libraries
      'rxjs': 'npm:rxjs',
      'ng2-select2': 'npm:ng2-select2',
      'ng2-signalr': 'npm:ng2-signalr/bundles/ng2-signalr.umd.js',
      'moment': 'npm:moment',
      'lodash': 'npm:lodash',
      'ng2-date-time-picker': 'ctrls:ng2-date-time-picker',
    },
    // packages tells the System loader how to load when no filename and/or no extension
    packages: {
        app: {
            main: './main.js',
            defaultExtension: 'js'
        },
        rxjs: {
            //main: '/bundles/Rx.umd.js',
            defaultExtension: 'js'
        },
        'ng2-select2': {
            main: 'ng2-select2.js',
            defaultExtension: 'js'
        },
        'moment': {
          main: 'moment.js',
          defaultExtension: 'js'
        },
        'lodash': {
            main: 'lodash.js',
            defaultExtension: 'js'
        },
        'ng2-date-time-picker': {
            main: 'index.js',
            defaultExtension: 'js'
        },
    },
  });
})(this);
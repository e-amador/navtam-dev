﻿using NavCanada.Core.Domain.Model.Entitities;
using NDS.Relay;

namespace NAVTAM.Helpers
{
    public static class ICAOFormatUtils
    {
        public static string Generate(INotam notam, bool bilingual)
        {
            return RelayUtils.GetAftnMessageTextFromNotam(notam, string.Empty, bilingual);
        }
    }
}
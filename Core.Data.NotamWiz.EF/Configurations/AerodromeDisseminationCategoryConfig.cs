﻿namespace NavCanada.Core.Data.NotamWiz.EF.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    using Domain.Model.Entitities;

    public class AerodromeDisseminationCategoryConfig : EntityTypeConfiguration<AerodromeDisseminationCategory>
    {
        public AerodromeDisseminationCategoryConfig()
        {
            HasKey(e => e.Id);
            Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(e => e.AhpCodeId).IsRequired();
            Property(e => e.AhpMid).IsRequired();
            Property(e => e.AhpName).IsRequired();
        }
    }
}

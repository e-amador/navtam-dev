﻿using System;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Linq;
using Business.Common;
using Core.Common.Geography;
using NavCanada.Core.Common.Extensions;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.SqlServiceBroker;
using NavCanada.Core.TaskEngine.Mto;
using NDS.Relay;
using Newtonsoft.Json;
using RSC.NDS.Helpers;

namespace RSC.NDS
{
    public class RSCDisseminationManager
    {
        readonly IUow _uow;
        readonly UserProfile _user;

        #region RSC Constants fields

        const string RSCCode23 = "MR";
        const string RSCCode45 = "XX";
        const string RSCTraffic = "IV";
        const string RSCPurpose = "NBO";
        const string RSCScope = "A";
        const int RSCLowerLimit = 0;
        const int RSCUpperLimit = 999;
        const int RSCCategoryId = 18;
        const int RSCRadius = 5;

        #endregion

        public RSCDisseminationManager(IUow uow, UserProfile user)
        {
            _uow = uow;
            _user = user;
        }

        /// <summary>
        /// Disseminates a RSC NOTAM
        /// </summary>
        /// <param name="designator">Aerodrome designator.</param>
        /// <param name="rscEnglish">RSC report (English)</param>
        /// <param name="rscFrench">RSC report (French)</param>
        /// <param name="startActivity"></param>
        /// <param name="endValidity"></param>
        public void Disseminate(string designator, string rscEnglish, string rscFrench, DateTime startActivity, DateTime endValidity)
        {
            var sdoAerodrome = _uow.SdoCacheRepo.GetAerodromeByDesignator(designator);
            if (sdoAerodrome == null)
                throw new Exception($"Trying to disseminate for unknown aerodrome '{designator}'.");

            // 1- Get series
            var series = GetRSCSeries(sdoAerodrome);
            if (string.IsNullOrEmpty(series) || series == "?")
                throw new Exception($"Unable to find the series for aerodrome '{designator}'.");

            var year = DateTime.UtcNow.Year;

            _uow.BeginTransaction();
            try
            {
                // 2- Get the RSC entry
                var rsc = GetOrCreateRSC(designator);

                // 3- Get or create the proposal
                var proposal = GetProposal(rsc);

                // 4- Update Proposal static fields
                UpdateProposalStaticFields(proposal, sdoAerodrome.Fir, designator);

                // 5- series
                UpdateSeriesAndNumber(proposal, series, year, ReserveSeriesNumber(series, year));

                // 6- user, operator and organization
                UpdateUserInfo(proposal);

                // 7- Update NSD category
                UpdateNsdCategory(proposal);

                // 8- Update Icao subject and condition
                UpdateIcaoSubjectAndCondition(proposal);

                // 9- Update location
                UpdateLocation(proposal, sdoAerodrome);

                // 10- Update validity period
                UpdateValidityPeriod(proposal, startActivity, endValidity);

                // 11- ItemE
                UpdateItemE(proposal, sdoAerodrome, rscEnglish, rscFrench);

                // 12- Add or update the proposal
                AddOrUpdateProposalToRepository(proposal);

                // 13- Update proposal history, if applies
                UpdateProposalHistory(proposal);

                // 14- Create NOTAM
                var notamId = CreateNotamFromProposal(proposal, GetPrevRSCNotam(rsc));

                // 15- Update RSC 
                UpdateAerodromeRSCEntry(rsc, proposal.Id, notamId);

                // 16- Disseminate NOTAM
                QueueDisseminateNotamMessage(notamId);

                // 17- Update RSC expiration date time
                rsc.ExpirationDate = endValidity;

                // save all
                _uow.SaveChanges();

                _uow.Commit();
            }
            catch (Exception)
            {
                _uow.Rollback();
                throw;
            }
        }

        private void UpdateAerodromeRSCEntry(AerodromeRSC rsc, int proposalId, Guid notamId)
        {
            rsc.ProposalId = proposalId;
            rsc.NotamId = notamId;            
        }

        private void QueueDisseminateNotamMessage(Guid notamId)
        {
            var mto = MtoHelper.CreateMtoMessageWithKeyValue(MessageTransferObjectId.DisseminateNotam, notamId);
            SubmitToServiceBrokerQueue(_uow, mto);
        }

        private void SubmitToServiceBrokerQueue(IUow uow, MessageTransferObject mto)
        {
            var taskEngineQueue = new SqlServiceBrokerQueue(uow.DbContext, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings);
            taskEngineQueue.Publish(mto);
        }

        private Guid CreateNotamFromProposal(Proposal proposal, Notam replaceNotam)
        {
            var toDisseminate = new Notam();
            proposal.CopyPropertiesTo(toDisseminate);

            toDisseminate.Id = Guid.NewGuid();
            toDisseminate.ProposalId = proposal.Id;
            toDisseminate.Type = proposal.ProposalType;

            toDisseminate.Coordinates = proposal.Coordinates;
            toDisseminate.EffectArea = CalculateEffectArea(proposal);
            toDisseminate.Published = DateTime.UtcNow.RemoveSeconds();

            if (replaceNotam != null)
            {
                toDisseminate.ReferredNotamId = ICAOTextUtils.FormatNotamId(replaceNotam.Series, replaceNotam.Number, replaceNotam.Year);
                toDisseminate.RootId = replaceNotam.Id;

                replaceNotam.ReplaceNotamId = toDisseminate.Id;
                replaceNotam.EffectiveEndValidity = proposal.StartActivity;

                _uow.NotamRepo.Update(replaceNotam);
            }
            else
            {
                toDisseminate.RootId = toDisseminate.Id;
            }

            // save the Icao Text in the NOTAM
            toDisseminate.IcaoText = RelayUtils.GetAftnMessageTextFromNotam(toDisseminate, string.Empty, true);

            // roll notam number
            var toRoll = _uow.NotamRepo.GetReferredNotam(toDisseminate.Series, toDisseminate.Number, toDisseminate.Year);
            if (toRoll != null)
            {
                toRoll.NumberRolled = true;
                _uow.NotamRepo.Update(toRoll);
            }

            _uow.NotamRepo.Add(toDisseminate);

            _uow.SaveChanges();

            return toDisseminate.Id;
        }

        private DbGeography CalculateEffectArea(Proposal proposal)
        {
            return proposal.Location.Buffer(proposal.Radius * 1852.0);
        }

        private Notam GetPrevRSCNotam(AerodromeRSC rsc)
        {
            return MustReplaceNotam(rsc) ? _uow.NotamRepo.GetById(rsc.NotamId.Value) : null;
        }

        private void AddOrUpdateProposalToRepository(Proposal proposal)
        {
            if (proposal.Id == 0)
            {
                _uow.ProposalRepo.Add(proposal);
            }
            else
            {
                _uow.ProposalRepo.Update(proposal);
            }
        }

        private void UpdateItemE(Proposal proposal, SdoCache arerodrome, string rscEnglish, string rscFrench)
        {
            var prefix = GetItemEPrefix(arerodrome);
            proposal.ItemE = $"{prefix}{rscEnglish}";
            proposal.ItemEFrench = $"{prefix}{rscFrench}";
        }

        static string GetItemEPrefix(SdoCache aerodrome)
        {
            if (IsNumericDesignator(aerodrome.Designator))
            {
                var servedCity = GetCachedAttributes<AerodromeAttributes>(aerodrome)?.ServedCity;
                var composedName = GetAerodromeComposedName(aerodrome.Name, servedCity);
                return $"{aerodrome.Designator} {composedName}\n";
            }
            return string.Empty;
        }

        private void UpdateValidityPeriod(Proposal proposal, DateTime startActivity, DateTime endValidity)
        {
            proposal.StartActivity = startActivity;
            proposal.EndValidity = endValidity;
            proposal.Received = DateTime.UtcNow;
        }

        private void UpdateLocation(Proposal proposal, SdoCache sdoAerodrome)
        {
            var latitude = (sdoAerodrome.RefPoint.Latitude ?? 0).ToString(CultureInfo.InvariantCulture);
            var longitude = (sdoAerodrome.RefPoint.Longitude ?? 0).ToString(CultureInfo.InvariantCulture);
            var dmsLocation = DMSLocation.FromDecimal($"{latitude} {longitude}");
            proposal.Location = sdoAerodrome.RefPoint;
            proposal.Coordinates = dmsLocation.ToDMS();
        }

        private void UpdateIcaoSubjectAndCondition(Proposal proposal)
        {
            var icaoSubject = _uow.IcaoSubjectRepo.GetByCode(RSCCode23);
            if (icaoSubject == null)
                throw new Exception($"Cannot find ICAO Subject for code {RSCCode23} in the DB!");

            proposal.IcaoSubjectId = icaoSubject.Id;
            proposal.IcaoSubject = icaoSubject;

            var icaoCondition = _uow.IcaoSubjectConditionRepo.GetByCode(icaoSubject.Id, RSCCode45);
            if (icaoCondition == null)
                throw new Exception($"Cannot find ICAO Subject Condition for code {RSCCode45} in the DB!");

            proposal.IcaoConditionId = icaoCondition.Id;
            proposal.IcaoSubjectCondition = icaoCondition;
        }

        private void UpdateNsdCategory(Proposal proposal)
        {
            var rscCategory = _uow.NsdCategoryRepo.GetById(RSCCategoryId);
            if (rscCategory == null)
                throw new Exception("Cannot find the RSC category in the DB!");

            proposal.CategoryId = RSCCategoryId;
            proposal.Category = rscCategory;
        }

        private void UpdateUserInfo(Proposal proposal)
        {
            proposal.User = _user;
            proposal.UserId = _user.Id;
            proposal.Operator = _user.UserName;
            proposal.Originator = _user.UserName;
            proposal.ModifiedByUsr = _user.UserName;
            proposal.ModifiedByOrg = _user.Organization?.Name;
        }

        private void UpdateSeriesAndNumber(Proposal proposal, string series, int year, int number)
        {
            if (proposal.ProposalType != NotamType.N)
            {
                proposal.ReferredSeries = proposal.Series;
                proposal.ReferredYear = proposal.Year;
                proposal.ReferredNumber = proposal.Number;
            }
            proposal.Series = series;
            proposal.Year = year;
            proposal.Number = number;
            proposal.NotamId = ICAOTextUtils.FormatNotamId(series, number, year);
        }

        const int MaxSeriesNumber = 9999;

        private int ReserveSeriesNumber(string series, int year)
        {
            var seriesNumber = _uow.SeriesNumberRepo.GetSeriesNumber(series[0], year);
            if (seriesNumber == null)
                throw new Exception($"Series {series} has not been initialized for year {year}!");

            var number = Math.Max(1, seriesNumber.Number);

            // update the series number
            seriesNumber.Number = number < MaxSeriesNumber ? number + 1 : 1;
            _uow.SeriesNumberRepo.SafeUpdateSeriesNumber(seriesNumber);

            return number;
        }

        private void UpdateProposalStaticFields(Proposal proposal, string fir, string designator)
        {
            proposal.Name = "RSC";
            proposal.Fir = fir;
            proposal.ItemA = GetItemAFromDesignator(designator);
            proposal.Code23 = RSCCode23;
            proposal.Code45 = RSCCode45;
            proposal.Traffic = RSCTraffic;
            proposal.Purpose = RSCPurpose;
            proposal.Scope = RSCScope;
            proposal.LowerLimit = RSCLowerLimit;
            proposal.UpperLimit = RSCUpperLimit;
            proposal.Nof = "CYHQ";
            proposal.ProposalType = proposal.Id == 0 ? NotamType.N : NotamType.R;
            proposal.Status = NotamProposalStatusCode.Disseminated;
            proposal.Radius = RSCRadius;
        }

        private string GetRSCSeries(SdoCache sdoAerodrome)
        {
            var region = _uow.GeoRegionRepo.GetByGeoLocation(sdoAerodrome.RefPoint);
            if (region == null)
                return "?";

            var adc = _uow.AerodromeDisseminationCategoryRepo.GetByAhpMid(sdoAerodrome.Id);
            if (adc == null)
                return "?";

            var series = _uow.SeriesAllocationRepo.GetSeries(SeriesAllocationSubject.RSC, region.Id, adc.DisseminationCategory, RSCCode23);

            return series?.Series ?? "?";
        }

        private void UpdateProposalHistory(Proposal proposal)
        {
            var proposalHistory = new ProposalHistory();

            var id = proposalHistory.Id;
            proposal.CopyPropertiesTo(proposalHistory);

            proposalHistory.Id = id;
            proposalHistory.ProposalId = proposal.Id;
            proposalHistory.Proposal = proposal;

            _uow.ProposalHistoryRepo.Add(proposalHistory);
        }

        private Proposal GetProposal(AerodromeRSC rsc)
        {
            if (RequiresNewProposal(rsc))
            {
                return new Proposal();
            }
            else
            {
                return _uow.ProposalRepo.GetById(rsc.ProposalId.Value);
            }
        }

        static bool RequiresNewProposal(AerodromeRSC rsc)
        {
            return !rsc.ProposalId.HasValue || (rsc.ExpirationDate.HasValue && rsc.ExpirationDate.Value < DateTime.UtcNow);
        }

        static bool MustReplaceNotam(AerodromeRSC rsc)
        {
            return rsc.NotamId.HasValue && rsc.ExpirationDate.HasValue && rsc.ExpirationDate.Value > DateTime.UtcNow;
        }

        private AerodromeRSC GetOrCreateRSC(string designator) => _uow.AerodromeRSCRepo.GetByDesignator(designator) ?? CreateRSC(designator);

        private AerodromeRSC CreateRSC(string designator)
        {
            var rsc = new AerodromeRSC() { Designator = designator };
            _uow.AerodromeRSCRepo.Add(rsc);
            return rsc;
        }

        static bool IsNumericDesignator(string designator) => (designator ?? "").Any(char.IsNumber);
        static string GetItemAFromDesignator(string designator) => IsNumericDesignator(designator) ? "CXXX" : designator;
        static string GetAerodromeComposedName(string name, string cityServed) => !string.IsNullOrEmpty(cityServed) ? $"{cityServed}/{name}" : name;

        static T GetCachedAttributes<T>(SdoCache subject) where T : class
        {
            try
            {
                return Deserialize<T>(subject.Attributes);
            }
            catch (Exception)
            {
                return null;
            }
        }

        static T Deserialize<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json, new JsonSerializerSettings
            {
                ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver()
            });
        }
    }

    class AerodromeAttributes
    {
        public string ServedCity { get; set; }
    }
}
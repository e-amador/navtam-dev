﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using NavCanada.Core.Domain.Model.Enums;

namespace NavCanada.Core.NotificationEngine
{
    public class Notifier : INotifier
    {
        public Notifier(string hostName, int smtpPort, string senderEmailAddress)
        {
            _hostName = hostName;
            _smtpPort = smtpPort;
            _senderEmailAddress = senderEmailAddress;
        }

        public NotifierResponse NotifyByEmail(string recipients, string subject, string message)
        {
            var template = new EmailMessage
            {
                Emailaddress = SplitEmailAddresses(recipients),
                EmailSubject = subject,
                MessageBody = message
            };
            return NotifyByEmail(template);
        }

        private NotifierResponse NotifyByEmail(EmailMessage template)
        {
            try
            {
                string errormessage;
                if (SendEmail(out errormessage, template))
                {
                    return new NotifierResponse
                    {
                        Code = ApiStatusCode.Success
                    };
                }

                return new NotifierResponse
                {
                    Code = ApiStatusCode.Error,
                    ErrorMessage = errormessage
                };
            }
            catch (Exception e)
            {
                return new NotifierResponse
                {
                    Code = ApiStatusCode.Error,
                    ErrorMessage = e.Message
                };
            }
        }

        private bool SendEmail(out string errormessage, EmailMessage template)
        {
            errormessage = null;
            try
            {
                using (var message = new MailMessage())
                {
                    message.IsBodyHtml = true;
                    using (var client = new SmtpClient
                    {
                        Port = _smtpPort,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Host = _hostName
                    })
                    {
                        message.From = new MailAddress(_senderEmailAddress);
                        foreach (var emailaddress in template.Emailaddress)
                            message.To.Add(emailaddress);

                        message.Subject = template.EmailSubject;
                        message.Body = template.MessageBody;

                        client.Send(message);

                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                errormessage = ex.Message;
                return false;
            }
        }

        const char EmailAddressSeparator = ',';

        static List<string> SplitEmailAddresses(string recipients)
        {
            return (recipients ?? "")
                .Split(EmailAddressSeparator)
                .Select(addr => addr.Trim())
                .Where(addr => !string.IsNullOrEmpty(addr))
                .ToList();
        }

        readonly string _hostName;
        readonly int _smtpPort;
        readonly string _senderEmailAddress;
    }

    class EmailMessage
    {
        public List<string> Emailaddress { get; internal set; }
        public string EmailSubject { get; internal set; }
        public string MessageBody { get; internal set; }
    }
}
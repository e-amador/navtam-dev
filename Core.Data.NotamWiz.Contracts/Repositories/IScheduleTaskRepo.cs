﻿namespace NavCanada.Core.Data.NotamWiz.Contracts.Repositories
{
    using System.Collections.Generic;

    using Domain.Model.Entitities;

    public interface IScheduleTaskRepo : IRepository<ScheduleTask>
    {
        List<ScheduleTask> GetAllProcessed();
        List<ScheduleTask> GetAllProcessedUntilYesterday();
        void SafeUpdateScheduleTask(ScheduleTask scheduleTask);
    }
}

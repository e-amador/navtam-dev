﻿using Business.Common;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NDS.Common.Contracts;
using NDS.Common.Xml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace NDS.Relay
{
    public static class RelayUtils
    {
        static RelayUtils()
        {
            _rootNameSpace = ConfigurationManager.AppSettings["RootNameSpace"] ?? "NES";
        }

        #region Relay Constants

        public const int AftnTextMaxLength = 1800;
        public const int AftnMaxAddresses = 21;
        public const int AftnAddressBlockSize = 7;
        public const int AftnLineLength = 69;

        public const char CR = '\r';
        public const char LF = '\n';
        public const string CRLF = "\r\n";

        public const char MetadataSeparator = '\t';

        #endregion

        #region Namespaces

        public static string GetHubBroadcastNamespace(Notam notam)
        {
            return GetNotamNamespace(notam, "B", "H");
        }

        public static string GetAftnBroadcastNamespace(Notam notam)
        {
            return GetNotamNamespace(notam, "B", "A");
        }

        public static string GetAftnExchangeMessageNamespace() => $"{_rootNameSpace}/B/A/M/";

        public static string GetNotamNamespace(Notam notam, string clientId, string channel)
        {
            var ns = new StringBuilder();

            ns.Append(_rootNameSpace).Append('/')
              .Append(clientId).Append('/')
              .Append(channel).Append('/')
              .Append("N_V1/") // hardcode to NOTAM message type and version
              .Append(notam.Series).Append('/')
              .Append(notam.ItemA).Append('/')
              .Append(notam.Code23).Append(notam.Code45).Append('/')
              .Append(notam.Traffic).Append('/')
              .Append(notam.Purpose).Append('/')
              .Append(notam.Scope).Append('/')
              .Append(notam.LowerLimit.ToString("000")).Append('/')
              .Append(notam.UpperLimit.ToString("000"));

            return ns.ToString();
        }

        #endregion

        #region Metadata

        public static IEnumerable<Property> GetHubMetadata(Notam notam, string messageId)
        {
            return new List<Property>()
            {
                Property.Create(RelayFields.MSG_TYPE, RelayValues.XML_NOTAM),
                Property.Create(RelayFields.MSG_ID, messageId),
                Property.Create(RelayFields.Series, notam.Series),
                Property.Create(RelayFields.Item_A, notam.ItemA),
                Property.Create(RelayFields.Q_Code, $"{notam.Code23}{notam.Code45}"),
                Property.Create(RelayFields.Traffic, notam.Traffic),
                Property.Create(RelayFields.Purpose, notam.Purpose),
                Property.Create(RelayFields.Scope, notam.Scope),
                Property.Create(RelayFields.Lower, notam.LowerLimit),
                Property.Create(RelayFields.Upper, notam.UpperLimit)
            };
        }

        public static IEnumerable<Property> GetAftnNotamMetadata(Notam notam, string messageId)
        {
            return new List<Property>()
            {
                Property.Create(RelayFields.MSG_TYPE, RelayValues.AFTN_NOTAM),
                Property.Create(RelayFields.MSG_ID, messageId),
                Property.Create(RelayFields.Series, notam.Series),
                Property.Create(RelayFields.Item_A, notam.ItemA)
            };
        }

        public static IEnumerable<Property> GetAftnMessageMetadata(string messageId)
        {
            return new List<Property>()
            {
                Property.Create(RelayFields.MSG_TYPE, RelayValues.AFTN_MESSAGE),
                Property.Create(RelayFields.MSG_ID, messageId),
            };
        }

        public static IEnumerable<Property> GetMessageProperties(this NdsOutgoingMessage message) => DeserializeNdsMetadata(message.Metadata);

        static string GetIcaoXmlMetadataAsText(Notam notam, string messageId)
        {
            return SerializeNdsMetadata(GetHubMetadata(notam, messageId));
        }

        static string GetAftnNotamMetadataAsText(Notam notam, string messageId)
        {
            return SerializeNdsMetadata(GetAftnNotamMetadata(notam, messageId));
        }

        static string GetAftnMessageMetadataAsText(string messageId)
        {
            return SerializeNdsMetadata(GetAftnMessageMetadata(messageId));
        }

        #endregion 

        #region Messages

        public static NdsOutgoingMessage GetHubBroadCastMessage(Notam notam)
        {
            var messageId = Guid.NewGuid();
            var destination = GetHubBroadcastNamespace(notam);
            var content = notam.ToXml();
            var metadata = GetIcaoXmlMetadataAsText(notam, messageId.ToString());
            var format = NdsMessageFormat.Xml;
            var groupId = messageId; // this is a single message so we must use the same id for both Id and GroupId
            var groupOrder = 1;
            return CreateNdsMessage(messageId, groupId, groupOrder, format, destination, content, metadata);
        }

        public static IEnumerable<NdsOutgoingMessage> GetAftnBroadcastMessages(Notam notam, Guid groupId, IEnumerable<string> addresses, bool bilingual, int blockSize = AftnMaxAddresses)
        {
            var destination = GetAftnBroadcastNamespace(notam);

            // split AFTN messages at 'AftnMessageMaxLength' chars.
            var textParts = GetAftnMessageTextParts(notam, "", bilingual, AftnTextMaxLength);

            var groupOrder = 1;
            foreach (var addressBlock in EnumerateAddressBlocks(addresses, blockSize))
            {
                foreach (var textPart in textParts)
                {
                    var formattedAddresses = FormatAftnInternalAddresBlock(addressBlock);
                    var aftnMessage = AftnMessageFormatter.FormatMessage(notam.GetPriority(), formattedAddresses, AftnConsts.NOFAddress, textPart, DateTime.UtcNow);
                    var messageId = Guid.NewGuid();
                    var metadata = GetAftnNotamMetadataAsText(notam, messageId.ToString());
                    var format = NdsMessageFormat.Aftn;
                    yield return CreateNdsMessage(messageId, groupId, groupOrder++, format, destination, aftnMessage, metadata);
                }
            }
        }

        public static IEnumerable<NdsOutgoingMessage> GetAftnRqnResponseMessages(IEnumerable<AftnRqnResult> rqnResults, Guid groupId, string address, bool bilingual)
        {
            var groupOrder = 0;
            foreach (var rqnResult in rqnResults)
            {
                foreach (var message in GetAftnRqnMessagesFromResult(groupId, rqnResult, address, bilingual))
                {
                    message.GroupOrder = groupOrder++;
                    yield return message;
                }
            }
        }

        public static IEnumerable<NdsOutgoingMessage> CreateMessageGroupOrdering(IEnumerable<NdsOutgoingMessage> messages)
        {
            var messageList = messages.ToList();
            var count = messageList.Count;

            // link the messages to the next using the id
            Guid? nextId = null;
            for (var i = count; i > 0; i--)
            {
                var message = messageList[i - 1];
                message.GroupOrder = i;
                message.NextId = nextId;
                nextId = message.Id;
            }

            return messageList;
        }

        public static IEnumerable<NdsOutgoingMessage> GetAftnRqlResponseMessages(IEnumerable<string> series, Guid groupId, string address, Func<string, AftnRqlResult> searchLambda)
        {
            foreach (var serie in series)
            {
                var searchResult = searchLambda.Invoke(serie);
                if (searchResult.SeriesExists)
                {
                    var seriesResults = searchResult.Results.ToList();
                    if (seriesResults.Count > 0)
                    {
                        // series header "RQR CYHQ [series]"
                        var seriesHeader = GetRqrResponseLine(serie);

                        // prepare the payload
                        var messagePayload = string.Join($"{AftnConsts.CRLF}", seriesResults.FormatRqlSeriesString());

                        // reformat to 69 chars per line
                        messagePayload = ReformatAftnTextLines(messagePayload, AftnLineLength);

                        // split the message in 1800 or less chars
                        var payloadParts = SplitAftnMessageText(messagePayload, seriesHeader);
                        foreach (var part in payloadParts)
                        {
                            yield return GetRqlResponseMessage(groupId, serie, address, part);
                        }
                    }
                    else
                    {
                        // no valid notam numbers in this series
                        yield return GetSingleRqrResponseMessage(groupId, serie, address, AftnConsts.NoValidNotamInDatabase);
                    }
                }
                else
                {
                    // this series is not supported
                    yield return GetSingleRqrResponseMessage(groupId, serie, address, AftnConsts.SeriesNotManaged);
                }
            }
        }

        public static NdsOutgoingMessage GetSingleRqrResponseMessage(Guid groupId, string subject, string aftnAddress, string message)
        {
            var messageId = Guid.NewGuid();
            var destination = GetAftnExchangeMessageNamespace();
            var metadata = GetAftnMessageMetadataAsText(messageId.ToString());

            var messageText = GetAftnRqrHeading(subject, message);
            var aftnMessage = AftnMessageFormatter.FormatMessage("GG", aftnAddress, AftnConsts.NOFAddress, messageText, DateTime.UtcNow);

            return CreateNdsMessage(messageId, groupId, 0, NdsMessageFormat.Aftn, destination, aftnMessage, metadata);
        }

        public static IEnumerable<NdsOutgoingMessage> GetAftnExchangeMessages(Guid groupId, string originAddress, IEnumerable<string> recipients, string text, string priorityCode = "GG", int blockSize = AftnMaxAddresses )
        {
            var groupOrder = 1;
            var destination = GetAftnExchangeMessageNamespace();

            // correct the text linebreaks
            var lineBreakCorrectedText = CorrectLineBreaks(text);

            // the text is reformatted into 69 char per line, then splitted in 1800 max chars
            var textParts = SplitAftnMessageText(ReformatAftnTextLines(lineBreakCorrectedText, AftnLineLength)).ToList();

            foreach (var addressBlock in EnumerateAddressBlocks(recipients, blockSize))
            {
                foreach (var textPart in textParts)
                {
                    var messageId = Guid.NewGuid();
                    var formattedAddresses = FormatAftnInternalAddresBlock(addressBlock);
                    var aftnMessage = AftnMessageFormatter.FormatMessage(priorityCode, formattedAddresses, originAddress, textPart, DateTime.UtcNow);
                    yield return CreateNdsMessage(messageId, groupId, groupOrder++, NdsMessageFormat.Aftn, destination, aftnMessage, GetAftnMessageMetadataAsText(messageId.ToString()));
                }
            }
        }

        public static string FormatAftnInternalAddresBlock(string addresses, int blockSize = AftnAddressBlockSize)
        {
            return string.Join(CRLF, EnumerateAddressBlocks(addresses.Split(' '), blockSize));
        }

        public static string GetAftnMessageTextFromNotam(INotam notam, string heading, bool bilingual)
        {
            var message = GetAftnNotamMessageHeader(notam, heading, ICAOTextUtils.FormatNotamNumber(notam));

            var itemEDCL = "E) ";
            var itemE = ReformatAftnTextLines(GetItemETextForAftn(notam, bilingual), AftnLineLength - itemEDCL.Length);
            message.Append($"{itemEDCL}{itemE}");

            if (!string.IsNullOrWhiteSpace(notam.ItemF))
                message.Append(CRLF).Append($"F) {notam.ItemF} G) {notam.ItemG}");

            return message.Append($"){CRLF}").ToString();
        }

        public static StringBuilder GetAftnMessagePartHeaderFromNotam(Notam notam, string heading, int number, int total)
        {
            return GetAftnNotamMessageHeader(notam, heading, ICAOTextUtils.FormatMultipartNotamNumber(notam, number, total));
        }

        public static IEnumerable<string> GetAftnMessageTextParts(Notam notam, string heading, bool bilingual, int chunkLength = AftnTextMaxLength)
        {
            var msgContent = GetAftnMessageTextFromNotam(notam, heading, bilingual);

            if (EstimateAftnTextLength(msgContent) <= chunkLength)
                return new List<string>() { msgContent };

            return SplitMessageContent(notam, heading, bilingual, chunkLength);
        }

        public static IEnumerable<Range> GetTextChunkRanges(string text, int chunkLength)
        {
            var start = 0;
            var chunk = 0;
            do
            {
                start += chunk;
                chunk = TryToChunkText(text, start, chunkLength);

                if (chunk > 0)
                    yield return new Range(start, chunk);
            }
            while (chunk > 0);
        }

        public static int TryToChunkText(string text, int start, int chunkLength)
        {
            var length = (text ?? "").Length;

            // there is no more text
            if (start >= length)
                return 0;

            // take the rest of the string
            if (chunkLength > length - start)
                return length - start;

            // initially take the proposed chunk length
            var count = chunkLength;

            // backup until find a place to break
            while (count > 0 && !CanBreakAtChar(text[start + count - 1]))
                count--;

            // could not break anywhere, take chuckLength
            if (count == 0)
                count = chunkLength;

            // try not to split a CRLF
            if (text[start + count - 1] == CR)
                count--;

            return count != 0 ? count : chunkLength;
        }

        public static int EstimateAftnTextLength(string text, int maxLength = 69)
        {
            if (string.IsNullOrEmpty(text))
                return 0;

            var returns = GetPrintedLineRanges(text, CRLF).Sum(range => Math.Max(0, range.Length - 1) / maxLength);

            return text.Length + 2 * returns;
        }

        public static string ReformatAftnTextLines(string text, int lineLength)
        {
            return !string.IsNullOrEmpty(text) ? string.Join(CRLF, SplitAftnTextLines(text, lineLength)) : text;
        }

        public static string CreateChecklistMessage(List<RqlNotamResultDto> list, string header, string footer)
        {
            var sb = new StringBuilder();

            sb.Append($"{header}{CRLF}");

            if (list.Count != 0)
            {
                var lastYear = 0;
                foreach (var yearList in list)
                {
                    var year = yearList.Year;

                    if (lastYear != 0 && year != lastYear + 1)
                    {
                        for (var y = lastYear + 1; y < year; y++)
                            sb.Append($"YEAR={y} NIL{CRLF}");
                    }

                    var numbers = string.Join(" ", yearList.Numbers.Select(n => ICAOTextUtils.FormatNotamNumber(n)));
                    var yearLine = $"YEAR={year} {numbers}{CRLF}";

                    sb.Append(yearLine);

                    lastYear = year;
                }
            }
            else
            {
                sb.Append($"YEAR={DateTime.UtcNow.Year} NIL{CRLF}");
            }

            sb.Append($"{footer}{CRLF}");

            return sb.ToString();
        }


        static IEnumerable<string> SplitAftnMessageText(string text, string header = "", int splitLength = AftnTextMaxLength)
        {
            var textLength = text.Length;
            var crlfLength = AftnConsts.CRLF.Length;

            var headerLine = !string.IsNullOrEmpty(header) ? $"{header}{AftnConsts.CRLF}" : string.Empty;
            var headerLength = headerLine.Length;

            if (headerLength + textLength + crlfLength <= splitLength)
            {
                yield return $"{headerLine}{text}{AftnConsts.CRLF}";
            }
            else
            {
                var sampleFooter = GetAftnMessagePartFooter(2, 2);
                var adjustedSplitLength = splitLength - headerLength - sampleFooter.Length - 2 * crlfLength;

                var chunkVector = GetTextChunkRanges(text, adjustedSplitLength).ToArray();
                var chunkVectorLength = chunkVector.Length;

                for (var i = 0; i < chunkVectorLength; i++)
                {
                    // take the text part
                    var range = chunkVector[i];
                    var textPart = text.Substring(range.Start, range.Length);

                    // prepare the footer
                    var footer = GetAftnMessagePartFooter(i + 1, chunkVectorLength);

                    yield return $"{headerLine}{textPart}{AftnConsts.CRLF}{footer}{AftnConsts.CRLF}";
                }
            }
        }

        static IEnumerable<string> FormatRqlSeriesString(this IEnumerable<RqlNotamResultDto> results)
        {
            return results.Select(r => r.FormatRqlSeriesString());
        }

        static string FormatRqlSeriesString(this RqlNotamResultDto result)
        {
            string notamResults = string.Join(" ", result.Numbers.Select(number => ICAOTextUtils.FormatNotamNumber(number)));
            return $"YEAR={result.Year} {notamResults}";
        }

        static NdsOutgoingMessage GetRqlResponseMessage(Guid groupId, string series, string aftnAddress, string message)
        {
            var messageId = Guid.NewGuid();
            var destination = GetAftnExchangeMessageNamespace();
            var metadata = GetAftnMessageMetadataAsText(messageId.ToString());
            var aftnMessage = AftnMessageFormatter.FormatMessage("GG", aftnAddress, AftnConsts.NOFAddress, message, DateTime.UtcNow);
            return CreateNdsMessage(messageId, groupId, 0, NdsMessageFormat.Aftn, destination, aftnMessage, metadata);
        }

        static IEnumerable<NdsOutgoingMessage> GetAftnRqnMessagesFromResult(Guid groupId, AftnRqnResult rqnResult, string address, bool bilingual)
        {
            if (rqnResult.ResultType == AftnRqnResultType.LimitReached)
                return GetSingleRqrResponseMessage(groupId, null, address, AftnConsts.RequestExceedsMaxNumber).Yield();

            if (rqnResult.ResultType == AftnRqnResultType.Missing)
                return GetSingleRqrResponseMessage(groupId, rqnResult.NotamNumber, address, AftnConsts.NotamNotIssued).Yield();

            if (rqnResult.ResultType == AftnRqnResultType.Obsolete)
                return GetSingleRqrResponseMessage(groupId, rqnResult.NotamNumber, address, AftnConsts.NotamNotInDatabase).Yield();

            return GetAftnRqnMessagesFromNotamResult(groupId, rqnResult, address, bilingual);
        }

        static IEnumerable<NdsOutgoingMessage> GetAftnRqnMessagesFromNotamResult(Guid groupId, AftnRqnResult rqnResult, string address, bool bilingual)
        {
            var notam = rqnResult.Notam;
            var destination = GetAftnExchangeMessageNamespace();

            var messageHeading = GetAftnRqrHeading(rqnResult);
            var textParts = GetAftnMessageTextParts(rqnResult.Notam, messageHeading, bilingual, AftnTextMaxLength);

            foreach (var textPart in textParts)
            {
                var aftnMessage = AftnMessageFormatter.FormatMessage(notam.GetPriority(), address, AftnConsts.NOFAddress, textPart, DateTime.UtcNow);
                var messageId = Guid.NewGuid();
                var metadata = GetAftnMessageMetadataAsText(messageId.ToString());
                var format = NdsMessageFormat.Aftn;
                yield return CreateNdsMessage(messageId, groupId, 0, format, destination, aftnMessage, metadata);
            }
        }

        static string GetAftnRqrHeading(AftnRqnResult rqnResult)
        {
            var notam = rqnResult.Notam;
            var description = string.Empty;
            if (rqnResult.ResultType == AftnRqnResultType.Expired)
            {
                description = AftnConsts.NotamExpired;
            }
            else if (rqnResult.ResultType == AftnRqnResultType.Canceled)
            {
                description = $"{AftnConsts.NotamCanceled} {rqnResult.ReplaceNumber}";
            }
            else if (rqnResult.ResultType == AftnRqnResultType.Replaced)
            {
                description = $"{AftnConsts.NotamReplaced} {rqnResult.ReplaceNumber}";
            }
            return GetAftnRqrHeading(rqnResult.NotamNumber, description);
        }

        static string GetRqrResponseLine(string subject)
        {
            return string.IsNullOrEmpty(subject) ? AftnConsts.RQRHeader : $"{AftnConsts.RQRHeader} {subject}";
        }

        static string GetAftnRqrHeading(string rqrSubject, string description)
        {
            var sb = new StringBuilder().Append(GetRqrResponseLine(rqrSubject)).Append(CRLF);

            if (!string.IsNullOrEmpty(description))
                sb.Append(description).Append(CRLF);

            var heading = ReformatAftnTextLines(sb.ToString(), AftnLineLength);

            return !heading.EndsWith(CRLF) ? $"{heading}{CRLF}" : heading;
        }

        static IEnumerable<Range> GetPrintedLineRanges(string text, string lineSeparator)
        {
            var sepChars = lineSeparator.ToArray();
            var length = text.Length;
            var last = 0;
            while (last < length)
            {
                var index = text.IndexOfAny(sepChars, last);
                if (index < 0)
                {
                    yield return new Range(last, length - last);
                    last = length;
                }
                else
                {
                    yield return new Range(last, index - last);
                    var separatorMatches = string.Compare(text, index, lineSeparator, 0, lineSeparator.Length, true) == 0;
                    last = separatorMatches ? index + lineSeparator.Length : index + 1;
                }
            }
        }

        static string CorrectLineBreaks(string text)
        {
            return Regex.Replace(text, "(?<!\r)\n", "\r\n");
        }

        static StringBuilder GetAftnNotamMessageHeader(INotam notam, string heading, string notamId)
        {
            var header = new StringBuilder().Append(heading); // used for RQN message responses

            if (!string.IsNullOrEmpty(notamId))
            {
                header.Append($"({notamId}").Append(CRLF);
            }
            else
            {
                header.Append("(");
            }

            header.Append($"Q) {ICAOTextUtils.FormatQLine(notam)}").Append(CRLF)
                  .Append($"A) {notam.ItemA} B) {ICAOTextUtils.FormatStartActivity(notam.Immediate, notam.StartActivity)}");

            // end validity could be empty
            var endDateText = ICAOTextUtils.FormatEndValidity(notam);
            if (!string.IsNullOrEmpty(endDateText))
                header.Append($" C) {endDateText}");

            // close A) B) and C) line
            header.Append(CRLF);

            // item D could be empty
            if (!string.IsNullOrEmpty(notam.ItemD))
            {
                var reformattedItemD = ReformatAftnTextLines($"D) {notam.ItemD}", AftnLineLength);
                header.Append(reformattedItemD).Append(CRLF);
            }

            return header;
        }

        static string GetItemETextForAftn(INotam notam, bool bilingual)
        {
            if (bilingual && !string.IsNullOrWhiteSpace(notam.ItemEFrench))
            {
                // ItemE and ItemEFrench combined
                return $"{CorrectLineBreaks(notam.ItemE)}{CRLF}{CRLF}FR:{CRLF}{CorrectLineBreaks(notam.ItemEFrench)}";
            }
            else
            {
                // ItemE only
                return CorrectLineBreaks(notam.ItemE);
            }
        }

        static NdsOutgoingMessage CreateNdsMessage(Guid messageId, Guid groupId, int groupOrder, /*Guid notamId,*/ NdsMessageFormat format, string destination, string content, string metadata)
        {
            return new NdsOutgoingMessage()
            {
                Id = messageId,
                GroupId = groupId,
                GroupOrder = groupOrder,
                //NotamId = notamId,
                Format = format,
                Destination = destination,
                Content = content,
                Metadata = metadata,
                Status = NdsMessageStatus.Pending,
                SubmitDate = DateTime.UtcNow
            };
        }

        static IEnumerable<string> EnumerateAddressBlocks(IEnumerable<string> addresses, int blockSize)
        {
            var count = 0;
            var sb = new StringBuilder();
            foreach (var addr in addresses)
            {
                sb.Append(addr).Append(' ');
                if (++count == blockSize)
                {
                    yield return sb.ToString().TrimEnd();
                    sb.Clear();
                    count = 0;
                }
            }
            if (sb.Length > 0)
            {
                yield return sb.ToString().TrimEnd();
            }
        }

        static IEnumerable<string> SplitAftnTextLines(string text, int lineLength)
        {
            var lines = GetPrintedLineRanges(text, CRLF).Select(range => text.Substring(range.Start, range.Length));
            foreach (var line in lines)
            {
                if (string.IsNullOrEmpty(line))
                {
                    yield return line;
                }
                else
                {
                    var chunks = GetTextChunkRanges(line, lineLength).Select(range => line.Substring(range.Start, range.Length));
                    foreach (var chunk in chunks)
                        yield return chunk;
                }
            }
        }

        static IEnumerable<string> SplitMessageContent(Notam notam, string heading, bool bilingual, int splitLength)
        {
            var itemEDCL = "E) ";
            var closingBraket = $"){CR}{LF}";

            // 1- Get sample header
            var sampleHeader = GetAftnMessagePartHeaderFromNotam(notam, heading, 0, 1);

            // 2- Get itemE+F text
            var itemE = ReformatAftnTextLines(GetItemETextForAftn(notam, bilingual), AftnLineLength - itemEDCL.Length);

            // 3- get itemFG text
            var itemFG = !string.IsNullOrEmpty(notam.ItemF) ? $"F) {notam.ItemF} G) {notam.ItemG})" : string.Empty;

            // 4- get sample footer
            var sampleFooter = GetAftnMessagePartFooter(2, 2);

            // 5- calculate the new split size
            var adjustedSplitLength = splitLength - sampleHeader.Length - "E) ".Length - itemFG.Length - sampleFooter.Length - closingBraket.Length;

            // 6- get the split vector
            var chunkVector = GetTextChunkRanges(itemE, adjustedSplitLength).ToArray();

            var chunkVectorLength = chunkVector.Length;
            for (var i = 0; i < chunkVectorLength; i++)
            {
                // append the message with the header only
                var message = GetAftnMessagePartHeaderFromNotam(notam, heading, i, chunkVectorLength);

                // take itemE part
                var range = chunkVector[i];
                var itemEPart = itemE.Substring(range.Start, range.Length);

                // append itemE part
                message.Append($"{itemEDCL}{itemEPart}");

                // append the itemFG if the last chunk
                if (i == chunkVectorLength - 1)
                {
                    if (!string.IsNullOrEmpty(itemFG))
                        message.Append(CRLF).Append(itemFG);
                }

                // append the closing bracket )<CR><LF>
                message.Append(closingBraket);

                // append the message footer
                message.Append(GetAftnMessagePartFooter(i + 1, chunkVectorLength));

                yield return message.ToString();
            }
        }

        static string GetAftnMessagePartFooter(int part, int count)
        {
            var partStr = part.ToString().PadLeft(2, '0');
            var countStr = count.ToString().PadLeft(2, '0');
            return part < count ? $"//END PART {partStr}//{CRLF}" : $"//END PART {partStr}/{countStr}//{CRLF}";
        }

        static bool CanBreakAtChar(char c) => c == ' ' || c == LF;

        #endregion

        #region Relay Extensions

        public static string SerializeNdsMetadata(IEnumerable<Property> metadata)
        {
            return string.Join($"{MetadataSeparator}", metadata.Select(p => $"{p.Name}={p.Value}"));
        }

        public static IEnumerable<Property> DeserializeNdsMetadata(string metadata)
        {
            foreach (var row in (metadata ?? "").Split(MetadataSeparator))
            {
                var eqPos = row.IndexOf('=');
                if (eqPos > 0 && eqPos < row.Length - 1)
                {
                    yield return Property.Create(row.Substring(0, eqPos), row.Substring(eqPos + 1));
                }
            }
        }

        public static IEnumerable<T> Yield<T>(this T item)
        {
            yield return item;
        }

        public static string GetPriority(this Notam notam) => notam.Urgent ? "DD" : "GG";

        #endregion

        static string _rootNameSpace;
    }

    public struct Range
    {
        public Range(int start, int length)
        {
            Start = start;
            Length = length;
        }

        public int Start { get; set; }
        public int Length { get; set; }
    }
}

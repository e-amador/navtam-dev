﻿using System;
using Microsoft.Practices.Unity;

namespace RSC.WebService
{
    public class Global : System.Web.HttpApplication, IContainerAccessor
    {
        public IUnityContainer Container => _container;

        protected void Application_Start(object sender, EventArgs e)
        {
            UnityConfig.RegisterComponents(_container = new UnityContainer());
        }

        static IUnityContainer _container { get; set; }
    }
}
﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NAVTAM.ViewModels
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(ResourceType = typeof(Resources), Name = nameof(Resources.Email))]
        public string Email { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(ResourceType = typeof(Resources), Name = nameof(Resources.Username))]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources), Name = nameof(Resources.Password))]
        public string Password { get; set; }

        [Display(ResourceType = typeof(Resources), Name = nameof(Resources.RememberMe))]
        public string RememberMe { get; set; }
    }

    public class ConfirmEmailViewModel
    {
        [Required(ErrorMessage = "The user name is required / Le nom d'utilisateur est requis")]
        [Display(Name = "Username / Nom d'utilisateur")]
        public string UserId { get; set; }
        public string UserName { get; set; }

        [Required(ErrorMessage = "The password is required / Le mot de passe est requis")]
        [DataType(DataType.Password)]
        [RegularExpression(@"^((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\]\[?\/<>~#`!@$%^&*()+={}|_:"";',{ ]).{8,99})$", ErrorMessage = "Password does not meet requirements / Le mot de passe ne répond pas aux exigences")]
        [Display(Name = "Password / Mot de passe")]
        public string Password { get; set; }


        [Required(ErrorMessage = "Confirm password is required / Confirmez le mot de passe est requis")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password / Confirmez le mot de passe")]
        [Compare("Password", ErrorMessage = "Password does not match / Le mot de passe ne correspond pas")]
        public string ConfirmPassword { get; set; }

        [Required]
        public string Code { get; set; }
        [Required]
        public string EmailVerificationCode { get; set; }
    }

    public class ConfirmRegistrationViewModel
    {
        [Required(ErrorMessage = "The user name is required / Le nom d'utilisateur est requis")]
        [Display(Name = "Username / Nom d'utilisateur")]
        public string Username { get; set; }

        [Required(ErrorMessage = "The password is required / Le mot de passe est requis")]
        [RegularExpression(@"^((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\]\[?\/<>~#`!@$%^&*()+={}|_:"";',{ ]).{8,99})$", ErrorMessage = "Password does not meet requirements / Le mot de passe ne répond pas aux exigences")]
        [DataType(DataType.Password)]
        [Display(Name = "Password / Mot de passe")]
        public string Password { get; set; }


        [Required(ErrorMessage = "Confirm password is required / Confirmez le mot de passe est requis")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password / Confirmez le mot de passe")]
        [Compare("Password", ErrorMessage = "Password does not match / Le mot de passe ne correspond pas")]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required(ErrorMessage = "The user name is required / Le nom d'utilisateur est requis")]
        [Display(Name = "Username / Nom d'utilisateur")]
        public string Username { get; set; }

        [Required(ErrorMessage = "The password is required / Le mot de passe est requis")]
        [RegularExpression(@"^((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\]\[?\/<>~#`!@$%^&*()+={}|_:"";',{ ]).{8,99})$", ErrorMessage = "Password does not meet requirements / Le mot de passe ne répond pas aux exigences")]
        [DataType(DataType.Password)]
        [Display(Name = "Password / Mot de passe")]
        //[Display(Name = "")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm password is required / Confirmez le mot de passe est requis")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password / Confirmez le mot de passe")]
        [Compare("Password", ErrorMessage = "Confirm Password does not match / Le mot de passe ne correspond pas")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
        public bool ResetPassword { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "The user name is required / Le nom d'utilisateur est requis")]
        [Display(Name = "Username / Nom d'utilisateur")]
        public string Username { get; set; }

        [Display(Name = "E-Mail / Courriel")]
        public string Email { get; set; }
    }
}

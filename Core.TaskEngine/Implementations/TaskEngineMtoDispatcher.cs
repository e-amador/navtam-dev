﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using NavCanada.Core.Common.Exceptions;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.TaskEngine.Contracts;
using NavCanada.Core.TaskEngine.Exceptions;
using NavCanada.Core.TaskEngine.Mto;
using NavCanada.Core.Common.Contracts;
using Nds.Common.Exceptions;
using NavCanada.Core.TaskEngine.PerformanceCounters;
using NavCanada.Core.TaskEngine.MtoProcessor;
using System.Diagnostics;
using NavCanada.Core.TaskEngine.Helpers;
using System.Configuration;

namespace NavCanada.Core.TaskEngine.Implementations
{
    public sealed class TaskEngineMtoDispatcher : MultithreadProcessor
    {
        public TaskEngineMtoDispatcher(IClusterStorageFactory storageFactory,
            IMessageTaskFactory messageTaskFactory,
            IMtoProcessorFactory mtoProcessorFactory,
            ILogger logger,
            TaskEngineDispatcherSettings dispatcherSettings) : base(logger)
        {
            if (storageFactory == null)
                throw new ArgumentException(@"The repositories factory can not be null", "storageFactory");
            if (messageTaskFactory == null)
                throw new ArgumentException(@"The task factory can not be null", "messageTaskFactory");
            if (mtoProcessorFactory == null)
                throw new ArgumentException(@"The Mto processor factory can not be null", "mtoProcessorFactory");
            if (dispatcherSettings == null)
                throw new ArgumentException(@"The Mto dispatcher settings can not be null", "dispatcherSettings");

            _storageFactory = storageFactory;
            _messageTaskFactory = messageTaskFactory;
            _mtoProcessorFactory = mtoProcessorFactory;
            _settings = dispatcherSettings;

            _failMessageCounters = new ConcurrentDictionary<string, FailureCounter>();

            RegisterPerfCounters(_settings.EnabledPerfCounter);

            ProcessorName = "TaskEngineMtoDispatcher";
        }

        protected override bool Process()
        {
            using (var clusterStorage = GetClusterStorage())
            {
                var loggerType = typeof(TaskEngineMtoDispatcher);

                var uow = clusterStorage.Uow;
                var mtoGuid = string.Empty;

                MessageTransferObject mto = null;
                int mtoFailCount = 0;
                bool continueProcessing = true;

                uow.BeginTransaction();
                try
                {
                    // read message from the TaskEngine queue
                    mto = clusterStorage.Queues.TaskEngineQueue.Retrieve<MessageTransferObject>();

                    // we could successfully read from the queue
                    ReportQueueUp();

                    if (mto == null)
                    {
                        uow.Rollback();
                        return false; // scheduled task was set, returning false to go for a nap
                    }

                    // verify the message unique identifier (this SHOULD never fail!!!)
                    mtoGuid = GetMtoGuid(mto);
                    if (mtoGuid == null)
                    {
                        _logger.LogWarn(loggerType, $"Mto message does not the 'UniqueId' property. Mto: {mto}");
                        uow.Commit();
                        return true;
                    }

                    // verify the message type (this SHOULD never fail!!!)
                    if (mto[MessageDefs.MessageTypeKey] == null)
                    {
                        _logger.LogWarn(loggerType, $"Mto message does not have the 'MessageType' property. Mto: {mto}");
                        uow.Commit();
                        return true;
                    }

                    // verify message has not exeeced the max retry count
                    FailureCounter failureCounter;
                    mtoFailCount = _failMessageCounters.TryGetValue(mtoGuid, out failureCounter) ? failureCounter.Count : 0;
                    if (mtoFailCount >= _settings.MaxPoisonMtoRetryCount)
                    {
                        _logger.LogWarn(loggerType, $"Mto message has exeeced the max retry count ({mtoFailCount}). Mto: {mto}");

                        // taking the message out of the queue
                        uow.Commit();

                        // take the message out of the failed messages
                        _failMessageCounters.TryRemove(mtoGuid, out failureCounter);

                        // reporting error (PoisonMto record created, system set to Critical and email system Admin)
                        ReportFailureMessage(mto, failureCounter);

                        // exit and continue processing other messages 
                        return true;
                    }

                    _logger.LogDebug(loggerType, $"Begin processing MTO message '{mtoGuid}'..");

                    // create a new mto processor and process the message
                    var mtoProcessorSettings = new MtoProcessorSettings { SoontoExpireHrsDifference = _settings.SoonToExpireHrsDifference };
                    var mtoProcessor = _mtoProcessorFactory.CreateMtoProcessor(clusterStorage, _messageTaskFactory, mtoProcessorSettings);

                    // process current message 
                    mtoProcessor.Run(mto);

                    // commit the transaction (this may still throw exceptions)
                    uow.Commit();

                    // update the NDS status on success
                    SetNdsStatusUpIfApplies(mto);

                    // remove message from fail queue
                    RemoveMessageCounter(mtoGuid);

                    _logger.LogDebug(loggerType, $"End processing MTO message '{mtoGuid}'..");
                }
                catch (InvalidMtoException ex)
                {
                    // if mto cannot be deserialized, it is considered corrupt and discarded by the system, payload is added to the log file for inspection
                    _logger.LogError(ex, loggerType, "Error deserializing MTO message!");
                    uow.Commit();
                }
                catch (MessageDeserializationException ex)
                {
                    // if mto cannot be deserialized, it is considered corrupt and discarded by the system, payload is added to the log file for inspection
                    _logger.LogError(ex, loggerType, "Error deserializing MTO message!");
                    uow.Commit();
                }
                catch (NdsCommunicationException ex)
                {
                    // message is going back to the queue
                    uow.Rollback();

                    // set NDS status as down
                    SetNdsStatusDown(ex);

                    // let this thread sleep to free the CPU 
                    return false;
                }
                catch (Exception ex)
                {
                    if (mto == null)
                    {
                        // reading from the queue failed
                        ReportQueueDown(ex);

                        // slow down the processing (avoid CPU overuse on queue down)
                        continueProcessing = false;
                    }
                    else
                    {
                        // to avoid too many repeated error logs, just log the error on the first occurrence
                        if (mtoFailCount == 0)
                            _logger.LogError(ex, loggerType, "Unexpected error processing MTO message!");

                        // handle processing message unexpected failures
                        IncrementMessageFailures(mto, ex.ToString());
                    }

                    // message is going back to the queue
                    uow.Rollback();
                }

                // return true to continue processing messages
                return continueProcessing;
            }
        }

        private void RemoveMessageCounter(string mtoGuid)
        {
            FailureCounter currentValue;
            _failMessageCounters.TryRemove(mtoGuid, out currentValue);
        }

        private void IncrementMessageFailures(MessageTransferObject mto, string errorDetails)
        {
            var mtoGuid = GetMtoGuid(mto);
            FailureCounter currentValue;
            if (!_failMessageCounters.TryGetValue(mtoGuid, out currentValue))
            {
                var critical = !mto.GetProperty(MessageDefs.OverridesCriticalFailureKey, false);
                _failMessageCounters.TryAdd(mtoGuid, new FailureCounter(errorDetails, critical));
            }
            else
            {
                currentValue.Increase();
            }
        }

        private void ReportFailureMessage(MessageTransferObject mto, FailureCounter failureCounter)
        {
            if (mto.GetProperty(MessageDefs.EmailNotificationKey, false))
            {
                _logger.LogDebug(GetType(), "Email Notification failed, we are not reporting the MTO message or it will potential create an infinite loop. Message: {mto}");
                return;
            }

            var mtoGuid = GetMtoGuid(mto);

            _logger.LogWarn(GetType(), $"A record with ID: '{mtoGuid}' has been created in the PoisonMto DB table. The system staus has been set to 'Critical'.\nError: {failureCounter.ErrorDetails}");

            // create PoisonMto record with the troubled mto object
            CreatePoisonMto(mto);

            if (failureCounter.Critical)
            {
                // mark the system as in critical error mode
                if (SetSystemStatus(SystemStatusEnum.CriticalError, status => status != SystemStatusEnum.CriticalError, failureCounter.ErrorDetails))
                {
                    // do not send email twice if the system was already marked as critical
                    NotifyAdministratorMessageFailure(mto, failureCounter.ErrorDetails);
                }
            }
            else
            {
                // this is a problem but the system won't go down; just notifying the administrator
                NotifyAdministratorMessageFailure(mto, failureCounter.ErrorDetails);
            }
        }

        private void NotifyAdministratorHubStatus(bool isUp)
        {
            try
            {
                using (var clusterStorage = GetClusterStorage())
                {
                    var uow = clusterStorage.Uow;

                    var hubStatus = isUp ? "UP" : "DOWN";
                    var subject = $"NES connection to NAVCan HUB is '{hubStatus}'";
                    var content = $"<html><body><p>The NAVCan HUB status has changed to '{hubStatus}' at {DateTime.UtcNow} UTC.</p></body></html>";

                    EmailHelper.QueueEmailMessage(clusterStorage.Queues.TaskEngineQueue, EmailHelper.AdministratorEmail, subject, content);

                    uow.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                // queueing email fails, to avoid the infinite loop we won't propagate the exception
                _logger.LogFatal(ex, GetType(), "Queueing of Email failed in application");
            }
        }

        private void NotifyAdministratorMessageFailure(MessageTransferObject mto, string errorDetails)
        {
            try
            {
                using (var clusterStorage = GetClusterStorage())
                {
                    var uow = clusterStorage.Uow;

                    var mtoGuid = GetMtoGuid(mto);
                    var subject = $"NES TaskEngine message '{mtoGuid}' failed to process";
                    var content = $"<html><body><p>The TaskEngine failed to process the message shown below. </p><p><b>Message:</b></p><pre>{mto}</pre><p><b>Error details:</b></p><pre>{errorDetails}</pre></body></html>";

                    EmailHelper.QueueEmailMessage(clusterStorage.Queues.TaskEngineQueue, EmailHelper.AdministratorEmail, subject, content);

                    uow.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                // queueing email fails, to avoid the infinite loop we won't propagate the exception
                _logger.LogFatal(ex, GetType(), "queueing email fails");
            }
        }

        static string GetMtoGuid(MessageTransferObject mto) => (string)mto[MessageDefs.UniqueIdentifierKey]?.Value;

        private bool SetSystemStatus(SystemStatusEnum newStatus, Predicate<SystemStatusEnum> validateStatus, string lastError = "")
        {
            using (var clusterStorage = GetClusterStorage())
            {
                var uow = clusterStorage.Uow;
                var systemStatus = uow.SystemStatusRepo.GetStatus();
                if (systemStatus != null && validateStatus.Invoke(systemStatus.Status))
                {
                    uow.BeginTransaction();
                    try
                    {
                        systemStatus.Status = newStatus;
                        systemStatus.LastError = lastError;
                        systemStatus.LastUpdated = DateTime.UtcNow;
                        uow.SystemStatusRepo.Update(systemStatus);
                        uow.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        uow.Rollback();
                        _logger.LogError(ex, GetType(), $"Could not set the system status to {newStatus}");
                    }
                }
                return false;
            }
        }

        private void SetNdsStatusDown(Exception ex)
        {
            // set as down only if on OK status
            if (SetSystemStatus(SystemStatusEnum.NDSDown, status => status == SystemStatusEnum.Ok))
            {
                _logger.LogError(ex, GetType(), "NDS communication failed!");
                NotifyAdministratorHubStatus(false);
            }
        }

        private void SetNdsStatusUpIfApplies(MessageTransferObject mto)
        {
            // verify whether this message can put the system up or not
            var ndsStatus = mto[MessageDefs.NDSUpTaskKey];
            if (ndsStatus != null && (bool)ndsStatus.Value)
            {
                // put the system up (OK) only when it was in NDS Down state
                if (SetSystemStatus(SystemStatusEnum.Ok, status => status == SystemStatusEnum.NDSDown))
                {
                    NotifyAdministratorHubStatus(true);
                }
            }
        }

        private IClusterStorage GetClusterStorage()
        {
            IClusterStorage clusterStorage = null;
            if (!_storageFactory.NewStorageConnection(out clusterStorage) || clusterStorage == null)
            {
                var message = "Unable to create a Uow to access the Database.";
                _logger.LogWarn(typeof(TaskEngineMtoDispatcher), message);
                throw new InvalidOperationException(message);
            }
            return clusterStorage;
        }

        private void CreatePoisonMto(MessageTransferObject mto)
        {
            try
            {
                var mtoGuid = GetMtoGuid(mto);
                using (var clusterStorage = GetClusterStorage())
                {
                    var uow = clusterStorage.Uow;
                    uow.PoisonMtoRepo.Add(new PoisonMto
                    {
                        Id = mtoGuid,
                        MessageType = (MessageType)Convert.ToByte(mto[MessageDefs.MessageTypeKey].Value),
                        MessageId = (MessageTransferObjectId)Convert.ToInt32(mto[MessageDefs.MessageIdKey].Value),
                        Payload = SerializeForArchival(mto),
                        ReporterType = (int)MtoReporterType.TaskEngine,
                        Content = mto.ToString()
                    });
                    uow.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, GetType(), "Failed to create PoisonMto record.");
            }
        }

        private void RegisterPerfCounters(bool perfCountersEnabled)
        {
            try
            {
                const string categoryName = "NES.SingleInstance.TaskEngine";

                if (perfCountersEnabled)
                    PerfCounterManager.ActivateCounters();

                PerfCounterManager.RegisterSimplePerfCounter(categoryName, TaskEngineCounters.MtoNumberOperationCount.ToString(), PerformanceCounterType.NumberOfItems32);
                PerfCounterManager.RegisterSimplePerfCounter(categoryName, TaskEngineCounters.TaskEngineOutgoingMessageCount.ToString(), PerformanceCounterType.NumberOfItems32);
                PerfCounterManager.RegisterSimplePerfCounter(categoryName, TaskEngineCounters.MessagesProcessedPerSecond.ToString(), PerformanceCounterType.RateOfCountsPerSecond32);
                PerfCounterManager.RegisterSimplePerfCounter(categoryName, TaskEngineCounters.MessagesInErrorPerSecond.ToString(), PerformanceCounterType.RateOfCountsPerSecond32);
                PerfCounterManager.RegisterAveragePerfCounter(categoryName, TaskEngineCounters.MtoPerOperationAverageTime.ToString());
                PerfCounterManager.InitializeCategory(categoryName, PerformanceCounterCategoryType.SingleInstance);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, typeof(TaskEngineMtoDispatcher), "RegisterPerfCounters(), There was an error configuring Performance Counters");
            }
        }

        private byte[] SerializeForArchival(MessageTransferObject mto)
        {
            using (var ms = new MemoryStream())
            {
                var fmt = new BinaryFormatter();
                fmt.Serialize(ms, mto);
                return ms.ToArray();
            }
        }

        static object QueueLock = typeof(TaskEngineMtoDispatcher);
        static bool QueueDown = false;

        private void ReportQueueUp()
        {
            lock (QueueLock)
            {
                if (QueueDown)
                {
                    QueueDown = false;
                    _logger.LogWarn(GetType(), $"TaskEngine's queue is back to normal on '{_settings.InstanceName}'.");
                }
            }
        }

        private void ReportQueueDown(Exception ex)
        {
            lock (QueueLock)
            {
                if (!QueueDown)
                {
                    QueueDown = true;
                    _logger.LogError(ex, GetType(), $"TaskEngine's queue read failed on '{_settings.InstanceName}'!");
                }
            }
        }

        private readonly IMessageTaskFactory _messageTaskFactory;
        private readonly IMtoProcessorFactory _mtoProcessorFactory;
        private readonly TaskEngineDispatcherSettings _settings;
        private readonly IClusterStorageFactory _storageFactory;
        private readonly ConcurrentDictionary<string, FailureCounter> _failMessageCounters;
    }

    class FailureCounter
    {
        public FailureCounter(string errorDetails, bool critical)
        {
            ErrorDetails = errorDetails;
            Count = 1;
            Critical = critical;
        }

        public int Count { get; set; }
        public bool Critical { get; set; }
        public string ErrorDetails { get; set; }

        public void Increase()
        {
            lock (this)
            {
                Count++;
            }
        }
    }
}
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PhoneValidator = void 0;
var PhoneValidator = /** @class */ (function () {
    function PhoneValidator() {
    }
    PhoneValidator.isValidPhoneNumber = function (value, length) {
        value = this.stripFormat(value);
        return value && (!length || value.length == length) && !isNaN(+value);
    };
    PhoneValidator.formatTenDigits = function (value) {
        value = this.stripFormat(value);
        if (!value || value.length != 10)
            return value;
        var area = value.substring(0, 3);
        var block1 = value.substring(3, 6);
        var block2 = value.substring(6, 10);
        return "(" + area + ") " + block1 + "-" + block2;
    };
    PhoneValidator.stripFormat = function (value) {
        if (!value)
            return value;
        var numbers = [];
        for (var i = 0; i < value.length; i++)
            if (this.isNumber(value[i]))
                numbers.push(value[i]);
        return numbers.join('');
    };
    PhoneValidator.isNumber = function (c) {
        return c >= '0' && c <= '9';
    };
    return PhoneValidator;
}());
exports.PhoneValidator = PhoneValidator;
//# sourceMappingURL=phone.validator.js.map
﻿namespace NavCanada.Core.Domain.Model.Dtos
{
    public class UserDto
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public string PhoneNumber { get; set; }
        public string Fax { get; set; }
        public int OrganizationId { get; set; }
        public string Organization { get; set; }
        public string RoleId { get; set; }
        public string Roles { get; set; }
        public string DoaIds { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string Address { get; set; }
        public bool Deleted { get; set; }
        public bool Approved { get; set; }

        public bool Disabled { get; set; }
    }
}

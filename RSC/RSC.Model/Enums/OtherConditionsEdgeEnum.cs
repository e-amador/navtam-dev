﻿namespace RSC.Model.Enums
{
    public enum OtherConditionsEdgeEnum : int
    {
        SnowDrifts = 120,
        Windrows = 121,
        SnowBanks = 125
    }
}

﻿namespace RSC.Model
{
    /// <summary>
    /// Other conditions affecting the cleared portion of the runway.
    /// </summary>
    public class OtherConditions
    {
        /// <summary>
        /// Structure to define area type 'Other Conditions' ON the runway and which do NOT have an associated height.
        /// </summary>
        public OtherConditArea OtherConditArea { get; set; }
        /// <summary>
        /// Structure is used to define other, generally 'edge' related conditions such as
        /// windrows and snow drifts in terms of the location(as a distance from a specified runway edge or
        /// edges, and across intersections with other runways) and height.
        /// </summary>
        public OtherConditEdge OtherConditEdge { get; set; }
        /// <summary>
        /// An additional text comment may be provided. This field should only be used for
        /// information that cannot be accommodated using existing defined fields.
        /// </summary>
        public FreeformText OtherConditionsComment { get; set; }
    }
}
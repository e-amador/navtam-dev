﻿namespace NavCanada.Services.Api.Diagnostics.Models
{
    using System;

    public class KeepAlive
    {
        public Guid Id { get; set; }
        
        public string Component { get; set; }

        public TickType Tick { get; set; }

        public DateTime Received {  get; set; }
    }
}

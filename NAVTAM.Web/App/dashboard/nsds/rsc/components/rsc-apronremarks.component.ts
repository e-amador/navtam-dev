﻿import { Component, OnInit, OnChanges, OnDestroy, Inject, Input } from '@angular/core';
import { TranslationService } from 'angular-l10n';
import { FormGroup, FormBuilder, FormArray, AbstractControl, Validators } from '@angular/forms';
import { RscService } from '../rsc.service';
import { IApronRemarksModel } from '../rsc.model';

import {
} from '../../../shared/data.model';

import * as moment from 'moment';
import { TOASTR_TOKEN, Toastr } from '../../../../common/toastr.service';

@Component({
    selector: 'rsc-apron-remarks',
    templateUrl: './app/dashboard/nsds/rsc/components/rsc-apronremarks.component.html'
})
export class ApronRemarksComponent implements OnInit, OnChanges, OnDestroy {
    @Input('form') mainForm: FormGroup = null;
    @Input('model') model: IApronRemarksModel = null;
    @Input('isReadOnly') isReadOnly: boolean = false;

    frmGroup: FormGroup;
    isFormLoadingData: boolean;

    constructor(
        private fb: FormBuilder,
        public translation: TranslationService,
        private rscService: RscService,
        @Inject(TOASTR_TOKEN) private toastr: Toastr) {
    }

    ngOnInit() {
        this.isFormLoadingData = false;
        this.configureReactiveForm();
    }

    ngOnChanges() {
    }

    ngOnDestroy() {
    }

    $ctrl(name: string): AbstractControl {
        return this.frmGroup.get(name);
    }

    private configureReactiveForm() {
        const frmArrControls = <FormArray>this.mainForm.controls['frmApronRemarksArr'];

        this.frmGroup = this.fb.group({
            apronRemarks: [{ value: this.model.apronRemarks, disabled: this.isReadOnly }, [Validators.required]],
        });

        frmArrControls.push(this.frmGroup);

    }

    //Export functions
    public isFormValid(): boolean {
        //Validations

        return true;
    }

    public exportComponentInternalData(): IApronRemarksModel {
        let data: IApronRemarksModel = {
            parent: this.model.parent,
            apronRemarks: this.model.apronRemarks,
        };
        return data;
    }

    public clearValues(): void {
        alert('Clear Component values: ApronRemarks');
    }


}
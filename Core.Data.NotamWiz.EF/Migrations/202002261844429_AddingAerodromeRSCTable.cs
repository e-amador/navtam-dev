namespace NavCanada.Core.Data.NotamWiz.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingAerodromeRSCTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AerodromeRSC",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Designator = c.String(nullable: false, maxLength: 4),
                        ProposalId = c.Int(),
                        NotamId = c.Guid(),
                        ExpirationDate = c.DateTime(),
                        Tokens = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Designator, unique: true, name: "IX_DESIGNATOR");
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.AerodromeRSC", "IX_DESIGNATOR");
            DropTable("dbo.AerodromeRSC");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using NDS.Common.Contracts;
using NDS.SolaceVMR;

namespace DataBus.Publisher.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public MainWindowViewModel()
        {
            PostQueueCommand = new DelegateCommand<object>(obj => PostQueueExecute(), obj => PostQueueCanExecute());
            PostTopicCommand = new DelegateCommand<object>(obj => PostTopicExecute(), obj => PostTopicCanExecute());
            //ConnectionString = "address=10.128.232.192;username=nds;password=password"; // "Endpoint=sb://win-ffk2v1a3k25/TestServiceBus;StsEndpoint=https://win-ffk2v1a3k25:9355/TestServiceBus;RuntimePort=5671;ManagementPort=9355;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=Bj7Tsw6JXG8RPSObwr7L1+P6R0f5DDRpi9k3u8cEL0k=;TransportType=Amqp";
            ConnectionString = "address=tcps:10.128.197.16:55443;username=nds@default;password=password;seclevel=2"; // "Endpoint=sb://win-ffk2v1a3k25/TestServiceBus;StsEndpoint=https://win-ffk2v1a3k25:9355/TestServiceBus;RuntimePort=5671;ManagementPort=9355;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=Bj7Tsw6JXG8RPSObwr7L1+P6R0f5DDRpi9k3u8cEL0k=;TransportType=Amqp";
            QueueName = "NDS-B-AFTN-SNK-DEV";
            TopicName = "NDS/B/A/";
            MessageBody = "This is sample message. Please type more here...";
        }

        #region Bound properties

        private string _connectionString;
        public string ConnectionString
        {
            get
            {
                return _connectionString;
            }
            set
            {
                _connectionString = value;
                OnPropertyChanged();
                UpdateConnectionString();
            }
        }

        private string _queueName;
        public string QueueName
        {
            get
            {
                return _queueName;
            }
            set
            {
                if (_queueName != value)
                {
                    _queueName = value;
                    _queue = null;
                    OnPropertyChanged();
                    UpdateCommands();
                }
            }
        }

        private string _topicName;
        public string TopicName
        {
            get
            {
                return _topicName;
            }
            set
            {
                if (_topicName != value)
                {
                    _topicName = value;
                    _topic = null;
                    OnPropertyChanged();
                    UpdateCommands();
                }
            }
        }

        private string _messageBody;
        public string MessageBody
        {
            get
            {
                return _messageBody;
            }
            set
            {
                _messageBody = value;
                OnPropertyChanged();
                UpdateCommands();
            }
        }

        private string _messageMetadata;
        public string MessageMetadata
        {
            get
            {
                return _messageMetadata;
            }
            set
            {
                _messageMetadata = value;
                OnPropertyChanged();
            }
        }

        private string _feedback;
        public string Feedback
        {
            get
            {
                return _feedback;
            }
            set
            {
                _feedback = value;
                OnPropertyChanged();
            }
        }

        #endregion

        public DelegateCommand<object> PostQueueCommand { get; set; }
        public DelegateCommand<object> PostTopicCommand { get; set; }

        private bool PostQueueCanExecute()
        {
            return CanPost(_queueName);
        }

        private void PostQueueExecute()
        {
            CreateQueue();
            _queue.PostMessage(GetCurrentMessage(_dataBus));
        }

        private void CreateQueue()
        {
            var sw = Stopwatch.StartNew(); 
            if (_queue == null)
            {
                sw.Start();
                _queue = _dataBus.CreateQueue(_queueName);
                sw.Stop();
                Feedback = $"{Math.Round(sw.Elapsed.TotalMilliseconds)}ms";
            }
        }

        private bool PostTopicCanExecute()
        {
            return !string.IsNullOrEmpty(_topicName) && !string.IsNullOrEmpty(_messageBody);
        }

        private void PostTopicExecute()
        {
            if (_dataBus == null)
                UpdateConnectionString();

            _topic = _topic ?? _dataBus.CreateTopic(_topicName);
            using (var message = GetCurrentMessage(_dataBus))
            {
                _topic.PostMessage(message);
            }
        }

        private bool CanPost(string name)
        {
            return !string.IsNullOrEmpty(name) && _dataBus != null && !string.IsNullOrEmpty(_messageBody);
        }

        private void UpdateConnectionString()
        {
            if (!string.IsNullOrEmpty(_connectionString))
            {
                _dataBus = new SolaceVMRWrapper(ConnectionString);
                _queue = null;
                _topic = null;
                UpdateCommands();
            }
        }

        private void UpdateCommands()
        {
            PostQueueCommand.RaiseCanExecuteChanged();
            PostTopicCommand.RaiseCanExecuteChanged();
        }

        private INdsMessage GetCurrentMessage(IDataBus sourceBus)
        {
            return sourceBus.CreateMessage(MessageBody, GetCurrentProperties());
        }

        private int _lastId = 1;

        private IEnumerable<Property> GetCurrentProperties()
        {
            var idFound = false;
            var lines = (_messageMetadata ?? "").Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var line in lines)
            {
                var cols = line.Split(new char[] { ' ', '=' }, StringSplitOptions.RemoveEmptyEntries);
                if (cols.Length == 2)
                {
                    idFound |= "id".Equals(cols[0]);
                    int value;
                    yield return int.TryParse(cols[1], out value) 
                        ? Property.Create(cols[0], value)
                        : Property.Create(cols[0], cols[1]);
                }
            }
            if (!idFound)
            {
                yield return Property.Create("id", _lastId++);
            }
        }

        private IDataBus _dataBus;
        private IPostQueue _queue;
        private IPostQueue _topic;
    }
}

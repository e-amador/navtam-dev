﻿using System.Collections.Generic;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class IcaoSubject
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string NameFrench { get; set; }
        public string EntityCode { get; set; }
        public string Code { get; set; }
        public string Scope { get; set; }
        public bool AllowItemAChange { get; set; }
        public bool AllowScopeChange { get; set; }
        public bool AllowSeriesChange { get; set; }
        public int? Version { get; set; }
        public virtual List<IcaoSubjectCondition> Conditions { get; set; }
    }
}
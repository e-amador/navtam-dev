﻿define('bingMaps', ['jquery', 'utilities', 'async!//ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0!OnScriptLoad', 'kendo/kendo.panelbar.min', 'kendo/kendo.dropdownlist.min', 'kendo/kendo.numerictextbox.min', 'kendo/kendo.maskedtextbox.min', 'kendo/kendo.progressbar.min'], function ($, util) {


    var isShapesModuleLoaded = false;
    var mapReadyDfd = $.Deferred(),
        mapReadyTest = function () {
            if (window.Microsoft && window.Microsoft.Maps && window.Microsoft.Maps.Location) {
                mapReadyDfd.resolve(window.Microsoft.Maps);
            }
            else {
                setTimeout(mapReadyTest, 100);
            }
        };

    mapReadyTest();

    var mm = Microsoft.Maps;
    var thisMap;

    //return mapReadyDfd.promise();
    return {
        key: 'AsnetwydeC-II7WzjzdOwSxSJKzLWaUt_69it1mR-38YRSWCKFKI0WAC9Rvwimf4', // TODO this my own(omar) development key,  should change to actual purchase bing key        
        userLayer: null,
        displayLayer: null,
        myLayer: null,
        infobox: null,
        modelLayer: null,
        infoboxLayer: null,
        infobox: null,
        locs: [],
        options: null,
        mapContainer: null,
        regionTypeData: [
                            { text: "Select ...", value: "0" },
                            { text: "FIR", value: "1" },
                            { text: "Aerodrome", value: "2" },
                            { text: "Location", value: "3" },
                            { text: "Polygon", value: "4" }
        ],

        // map entity drawing options
        inputLineOptions: {
            strokeColor: new Microsoft.Maps.Color(255, 100, 100, 100),
            strokeWidth: 5
        },

        inputPolygonOptions: {
            strokeColor: new Microsoft.Maps.Color(255, 100, 255, 100),
            strokeWidth: 5,
            fillColor: new Microsoft.Maps.Color(100, 200, 100, 255)
        },
        pointLocations: [],
        readyCallBack: null,
        inputFinished: false,
        newGeo: null,
        modelGeo: null,





        inputLocationPushpinOptions: {
            visible: true,
            anchor: new Microsoft.Maps.Point(12, 12),
            width: 25,
            height: 25,
            icon: "/Images/GeoImages/red_crosshairs.png"
        },

        drawingOptions: {
            "DOA": {
                strokeColor: new Microsoft.Maps.Color(100, 255, 0, 0),
                strokeWidth: 4,
                fillColor: new Microsoft.Maps.Color(70, 25, 255, 25)
            },
            "MODEL": {
                strokeColor: new Microsoft.Maps.Color(100, 255, 0, 0),
                strokeWidth: 4,
                fillColor: new Microsoft.Maps.Color(70, 25, 255, 25)
            },
            "FIR": {
                strokeColor: new Microsoft.Maps.Color(100, 0, 0, 0),
                strokeWidth: 4,
                fillColor: new Microsoft.Maps.Color(50, 255, 100, 100)
            },
            "LOCATION": {
                strokeColor: new Microsoft.Maps.Color(100, 0, 0, 0),
                strokeWidth: 4,
                fillColor: new Microsoft.Maps.Color(50, 255, 100, 100)
            },
            "AD": {
                strokeColor: new Microsoft.Maps.Color(100, 0, 0, 0),
                strokeWidth: 4,
                fillColor: new Microsoft.Maps.Color(50, 255, 100, 100)
            }
            },

        setModelGeo: function (geo, options) {            
            thisMap.modelLayer.clear();

            if (options) {

                thisMap.drawingOptions["MODEL"] = $.extend(options, thisMap.drawingOptions["MODEL"]);
            }

            thisMap.modelGeo = geo;

            if (geo) {
                
                thisMap.addMultiPolygonToLayer(geo, null, thisMap.drawingOptions["MODEL"], thisMap.modelLayer);
                thisMap.showAll();
            }
        },

        theMap: null,
        mapTools: null,
        firs: null,
        locationRadious: null,
        ads: null,
        adRadious:null,
        location: null,
        regionType: null,
        firSelector: null,
        aerodromeSelector: null,
        locationSelector: null,
        polygonSelector: null,
        btnInclude: null,
        btnExclude: null,
        btnShowAll: null,
        btnReset: null,
        info: null,
        infoTitle: null,
        mapInfo: null,
        InfoLatitude: null,
        InfoLongitude: null,
        InfoZoom: null,
        infoTable: null,
        ItemProperties: null,

        init: function (mapContainer, mapOptions, callBack) {

            thisMap = this;

            $(thisMap).off();

            


            thisMap.options = $.extend({
                credentials: thisMap.key,
                center: new mm.Location(63.3, -104),
                mapTypeId: mm.MapTypeId.road,
                zoom: 3,
                showScalebar: false,
                showCopyright: false,
                showBreadcrumb: false,
                showDashboard: false,
                showMapTypeSelector: false,
                enableSearchLogo: false,
                enableClickableLogo: false,
                showTools: false,
                showLoadingProgressBar: true,
                disableKeyboardInput: true
            }, mapOptions);

            thisMap.readyCallBack = callBack;
            thisMap.mapContainer = mapContainer;
            $.ajax({
                url: "/Map/GetMapView",
                cache: false,
                async: false,
                type: "GET",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    thisMap.mapContainer.html(data);
                }
            });

            // get UI hooks
            thisMap.theMap              = thisMap.mapContainer.find(".theMap")[0];
            thisMap.mapTools            = $(thisMap.mapContainer.find(".mapTools")[0]);
            thisMap.firs                = $(thisMap.mapContainer.find(".firs")[0]);
            thisMap.adRadious           = $(thisMap.mapContainer.find(".adRadious")[0]);
            thisMap.locationRadious     = $(thisMap.mapContainer.find(".locationRadious")[0]);
            thisMap.ads                 = $(thisMap.mapContainer.find(".ads")[0]);
            thisMap.adRadious           = $(thisMap.mapContainer.find(".adRadious")[0]);
            thisMap.location            = $(thisMap.mapContainer.find(".location")[0]);
            thisMap.regionType          = $(thisMap.mapContainer.find(".regionType")[0]);
            thisMap.firSelector         = $(thisMap.mapContainer.find(".firSelector")[0]);
            thisMap.aerodromeSelector   = $(thisMap.mapContainer.find(".aerodromeSelector")[0]);
            thisMap.locationSelector    = $(thisMap.mapContainer.find(".locationSelector")[0]);
            thisMap.polygonSelector     = $(thisMap.mapContainer.find(".polygonSelector")[0]);
            thisMap.btnInclude          = $(thisMap.mapContainer.find("button[class*='btnInclude']")[0]);
            thisMap.btnReset            = $(thisMap.mapContainer.find("button[class*='btnReset']")[0]);  
            thisMap.btnExclude          = $(thisMap.mapContainer.find("button[class*='btnExclude']")[0]);
            thisMap.btnShowAll          = $(thisMap.mapContainer.find("button[class*='btnShowAll']")[0]);
            thisMap.info                = $(thisMap.mapContainer.find(".info")[0]);
            thisMap.infoTitle           = $(thisMap.mapContainer.find(".infoTitle")[0]);
            thisMap.mapInfo             = $(thisMap.mapContainer.find(".mapInfo")[0]);
            thisMap.InfoLatitude        = $(thisMap.mapContainer.find(".InfoLatitude")[0]);
            thisMap.InfoLongitude       = $(thisMap.mapContainer.find(".InfoLongitude")[0]);
            thisMap.InfoZoom            = $(thisMap.mapContainer.find(".InfoZoom")[0]);
            thisMap.infoTable           = $(thisMap.mapContainer.find(".infoTable")[0]);
            thisMap.ItemProperties      = $(thisMap.mapContainer.find(".ItemProperties")[0]);

        
            if (thisMap.options.showLoadingProgressBar) {
                // Setup ajax working overlay
                $(document).ajaxStart(function () {
                    kendo.ui.progress(thisMap.mapContainer, true);
                });

                $(document).ajaxStop(function () {
                    kendo.ui.progress(thisMap.mapContainer, false);
                });
            }




            thisMap.map = new mm.Map(thisMap.theMap, thisMap.options);

            if (!isShapesModuleLoaded)
            {

                

                Microsoft.Maps.loadModule('Microsoft.Maps.AdvancedShapes', {
                    callback: function()
                    {
                        mm.registerModule("clusterModule", "/Scripts/V7ClientSideClustering.js");
                        mm.registerModule("WKTModule", "/Scripts/WKTModule.js");
                        mm.loadModule("clusterModule", { callback: thisMap.shapesModuleLoaded });
                        mm.loadModule("WKTModule");
                    }
                });
            }else
            {
                thisMap.shapesModuleLoaded();
            }
            
         
            



        },
        createPin: function(data)
        {
              var pin = new Microsoft.Maps.Pushpin(data._LatLong);

	        pin.title = "Single Location";
	        pin.description = "GridKey: " + data.GridKey;

	        //Add handler for the pushpin click event.
	        Microsoft.Maps.Events.addHandler(pin, 'click', thisMap.displayEventInfo);

	        return pin;

        },
        createClusteredPin : function(cluster, latlong)
        {
            var pin = new Microsoft.Maps.Pushpin(latlong, { text: '+' });

	        pin.title = "Cluster";
	        pin.description = "GridKey: " + cluster[0].GridKey + " Cluster Size: " + cluster.length + " Zoom in for more details.";

	        //Add handler for the pushpin click event.
	        Microsoft.Maps.Events.addHandler(pin, 'click', thisMap.displayEventInfo);

	        return pin;

        },

        displayEventInfo: function(e)
        {ge
            if (e.targetType == "pushpin")
            {
                infobox.setLocation(e.target.getLocation());
                infobox.setOptions({
                    title: e.target.title,
                    description: e.target.description,
                    visible: true
                });
            }
        },


        initUI: function () {

            if (!thisMap.options.showTools) {
                thisMap.mapContainer.find(".mapTools").hide();
            } else {
                 thisMap.mapTools.show();
                 thisMap.mapTools.kendoPanelBar({
                    animation: {
                        expand: {
                            duration: 200,
                            effects: "expandVertical"
                        }
                    },
                    expandMode: "single",
                    select: function (e) {                      
                        if ($(e.item).is(".k-state-active")) {
                            var that = this;
                             thisMap.mapTools.css('opacity', '0.6');
                            window.setTimeout(function () { that.collapse(e.item); }, 1);

                        } else {
                             thisMap.mapTools.css('opacity', '1');
                        }

                    }
                });


                 thisMap.firs.kendoDropDownList({
                    optionLabel: "Select FIR...",
                    dataTextField: "Name",
                    dataValueField: "Id",
                    dataSource: {
                        transport: {
                            read: {
                                cache: false,
                                type: "POST",
                                dataType: "json",
                                url: "/Map/GetFIRs"
                            }
                        }
                    },
                    change: function (e) {
                        var f =  thisMap.firs.val();
                        if (f) {
                            $.ajax({
                                data: { fir: f },
                                cache: false,
                                success: thisMap.receiveData,
                                type: "POST",
                                dataType: "json",
                                url: "/Map/GetFIRGeo"
                            });
                        }

                    }
                });

                 thisMap.adRadious.kendoNumericTextBox({
                    format: "0",
                    min: 1,
                    max: 100,
                    step: 1,
                    spin: thisMap.getADGeo,
                    change: thisMap.getADGeo
                });

                 thisMap.locationRadious.kendoNumericTextBox({
                    format: "0",
                    min: 1,
                    max: 100,
                    step: 1,
                    spin: thisMap.getLocationGeo,
                    change: thisMap.getLocationGeo
                });


                thisMap.ads.kendoDropDownList({
                    optionLabel: "Select AD...",
                    filter: "contains",
                    dataTextField: "txtName",
                    template: '<span class="k-state-default">#: codeId#</span> <span class="k-state-default">#: txtName#</span>',
                    dataValueField: "mid",
                    dataSource: {
                        type: "odata-v4",
                        serverFiltering: true,
                        pageSize: 20,
                        transport: {
                            read: {
                                url: "/SDO/Ahp?$select=mid,codeId,txtName",
                                dataType: "json"
                            },
                            parameterMap: function (data) {
                                var d = kendo.data.transports.odata.parameterMap(data);
                                delete d.$format;
                                delete d.$inlinecount;
                                d.$count = true;

                                if (d.$filter) {
                                    if (d.$filter.substring(0, 12) == 'substringof(') {
                                        var parms = d.$filter.substring(12, d.$filter.length - 1).split(',');
                                        d.$filter = "contains(txtName," + parms[0] + ") or contains(codeId," + parms[0] + ")";
                                    }
                                }

                                return d;
                            }
                        },
                        schema: {
                            data: function (data) {
                                return data["value"];
                            },
                            total: function (data) {
                                return data["odata.count"];
                            },
                            model: {
                                Id: "mid"
                            }
                        }
                    },
                    change: thisMap.getADGeo
                });



                thisMap.location.kendoMaskedTextBox({

                    mask: "000000N 0000000W",
                    change: thisMap.getLocationGeo
                });

                thisMap.regionType.kendoDropDownList({
                    dataTextField: "text",
                    dataValueField: "value",
                    dataSource: thisMap.regionTypeData,
                    index: 0,
                    change: function () {
                        thisMap.inputFinished = false;
                        thisMap.pointLocations = [];
                        var regionType = thisMap.regionType.val();
                        switch (regionType) {
                            case "0":
                                thisMap.firSelector.hide();
                                thisMap.aerodromeSelector.hide();
                                thisMap.locationSelector.hide();
                                thisMap.polygonSelector.hide();
                                break;
                            case "1":
                                thisMap.firSelector.show();
                                thisMap.aerodromeSelector.hide();
                                thisMap.locationSelector.hide();
                                thisMap.polygonSelector.hide();
                                break;
                            case "2":
                                thisMap.firSelector.hide();
                                thisMap.aerodromeSelector.show();
                                thisMap.locationSelector.hide();
                                thisMap.polygonSelector.hide();
                                break;
                            case "3":
                                thisMap.firSelector.hide();
                                thisMap.aerodromeSelector.hide();
                                thisMap.locationSelector.show();
                                thisMap.polygonSelector.hide();
                                break;
                            case "4":
                                thisMap.firSelector.hide();
                                thisMap.aerodromeSelector.hide();
                                thisMap.locationSelector.hide();
                                thisMap.polygonSelector.show();
                                break;
                        }
                    }
                });

                thisMap.btnInclude.on('click', thisMap.Include);
                thisMap.btnExclude.on('click', thisMap.Exclude);
                thisMap.btnShowAll.on('click', thisMap.showAll);
                thisMap.btnReset.on('click', thisMap.reset);



            }


            thisMap.info.kendoPanelBar({
                animation: {
                    expand: {
                        duration: 200,
                        effects: "expandVertical"
                    }
                },
                expandMode: "single",
                select: function (e) {
                    if ($(e.item).is(".k-state-active")) {
                        var that = this;
                        thisMap.mapTools.css('opacity', '0.6');
                        window.setTimeout(function () { that.collapse(e.item); }, 1);

                    } else {
                        thisMap.mapTools.css('opacity', '1');
                    }

                }
            });
        },

        hideAttribute: ['RowCheckSum', 'Mid', 'EffectiveDate', 'GEOLocation'],
        showInfo: function (title, info) {
            thisMap.infoTitle.text(title);


            var keys = Object.keys(info);
            for (var k in keys) {
                if (info[keys[k]] && info[keys[k]] !== "" && typeof info[keys[k]] !== "object" && thisMap.hideAttribute.indexOf(keys[k]) === -1 && !keys[k].endsWith("Crc") && keys[k].indexOf('FK_') !== 0) {
                    var myRow = "<tr><td align='left'>" + keys[k] + "</td><td align='left'><strong>" + info[keys[k]] + "</strong></td></tr>";
                    thisMap.infoTable.find("tr:last").after(myRow);
                }
            }






            thisMap.info.show();
        },

        getLocationGeo: function () {

            var loc = thisMap.location.val();
            var r = thisMap.locationRadious.val();

            if (loc.length !== 16) {
                thisMap.location.css('color', 'red');
            }
            else {
                thisMap.location.css('color', 'black');
                var la = util.DMSTodecimal(loc.substring(0, 7));
                var lo = util.DMSTodecimal(loc.substring(8, 16));

                $.ajax({
                    data: { lat: la, lon: lo, radius: r },
                    cache: false,
                    success: thisMap.receiveData,
                    type: "POST",
                    dataType: "json",
                    url: "/Map/GetLocationGeo"
                });

            }
        },
        getADGeo: function () {
            var a = thisMap.ads.val();
            var r = thisMap.adRadious.val();
            if (a && r) {
                $.ajax({
                    data: { mid: a, radius: r },
                    cache: false,
                    success: thisMap.receiveData,
                    type: "POST",
                    dataType: "json",
                    url: "/Map/GetADGeo"
                });
            }


        },

        shapesModuleLoaded: function () {      

            isShapesModuleLoaded = true;
            thisMap.initUI();


           thisMap.myLayer = new ClusteredEntityCollection(thisMap.map , {
	                    singlePinCallback: thisMap.createPin,
	                    clusteredPinCallback: thisMap.createClusteredPin
	                });

	        //Add infobox layer that is above the clustered layers.
	        thisMap.infoboxLayer = new Microsoft.Maps.EntityCollection();
	        
	        

            thisMap.modelGeo = null;
            thisMap.map.entities.clear();
            thisMap.modelLayer = new mm.EntityCollection();
            thisMap.userLayer = new mm.EntityCollection();
            thisMap.displayLayer = new mm.EntityCollection();
            thisMap.infoboxLayer = new mm.EntityCollection();
            thisMap.infobox = new Microsoft.Maps.Infobox(new Microsoft.Maps.Location(0, 0),thisMap.infoboxOptions);


            thisMap.map.entities.push(thisMap.userLayer);
            thisMap.map.entities.push(thisMap.displayLayer);

            thisMap.map.entities.push(thisMap.modelLayer);
            thisMap.map.entities.push(thisMap.infoboxLayer);



            //Prep InfoBox & add to infobox layer
            var infoboxOptions = {
                width: 300,
                height: 200,
                visible: false,
                offset: new mm.Point(0, 20)
            };
            

            if (thisMap.options.showTools) {
                mm.Events.addHandler(thisMap.map, "click", thisMap.MouseClicked);
                mm.Events.addHandler(thisMap.map, "dblclick", thisMap.MouseDoubleClicked);
                
            }

            // we track mouse movement for demonstration purposes
            mm.Events.addHandler(thisMap.map, "mousemove", thisMap.MouseMoved);
            mm.Events.addHandler(thisMap.map, "mouseout", thisMap.MouseOut);

            $(thisMap).on("GeoChanged", thisMap.onGeoChanged);

            


            if (thisMap.readyCallBack !== null)
                thisMap.readyCallBack();
        },


        receiveData: function (data, status, xhr) {

            thisMap.userLayer.clear();
            thisMap.displayLayer.clear();

            if (!data) {
                return;
            }


            if (data.Name === "MODEL") {

                $(thisMap).trigger("GeoChanged", [data ? data.Features[0].Geometry : null]);

            } else {
                thisMap.newGeo = data ? data.Features[0].Geometry : null;
                thisMap.addGeoToLayer(data, thisMap.displayLayer);
            }

            thisMap.showAll();

        },

        onGeoChanged: function (event, multiPolygon) {
            thisMap.newGeo = null;
            thisMap.modelGeo = multiPolygon;
            thisMap.modelLayer.clear();
            thisMap.addMultiPolygonToLayer(multiPolygon, null, thisMap.drawingOptions["MODEL"], thisMap.modelLayer);

        },
        MouseClicked: function (e) {            
            var mm = Microsoft.Maps;
            var location = thisMap.map.tryPixelToLocation(new mm.Point(e.pageX, e.pageY), mm.PixelReference.page);
            if (location == null || isNaN(location.latitude) || isNaN(location.longitude)) {
                return;
            }

            var currentTool = thisMap.regionType.val();

            switch (currentTool) {
                case "3": // Location
                    //thisMap.userLayer.clear();
                    //var pp = new mm.Pushpin(location, thisMap.inputLocationPushpinOptions);
                    //thisMap.userLayer.push(pp);

                    //thisMap.pointLocations = [location];

                    // we are finished with input
                    thisMap.inputFinished = true;

                    //var strLoc = location.latitude.toFixed(8)+ " "+location.longitude.toFixed(8);
                    thisMap.location.val(util.decimalToDMS(location.latitude.toFixed(8), 'lat') + " " + util.decimalToDMS(location.longitude.toFixed(8), 'long'));
                    thisMap.location.data("kendoMaskedTextBox").trigger("change");
                    break;

                case "Line":
                    if (thisMap.inputFinished) {
                        return;
                    }
                    thisMap.pointLocations.push(location);

                    var m = new mm.Pushpin(location, thisMap.inputLocationPushpinOptions);
                    thisMap.userLayer.push(m);

                    if (thisMap.pointLocations.length >= 2) {
                        var line = new mm.Polyline(thisMap.pointLocations, thisMap.inputLineOptions);
                        thisMap.userLayer.push(line);
                    }
                    break;

                case "4": // polygon                    
                    if (thisMap.inputFinished) {
                        return;
                    }
                    thisMap.pointLocations.push(location);
                    var c = new mm.Pushpin(location, thisMap.inputLocationPushpinOptions);
                    thisMap.userLayer.push(c);

                    if (thisMap.pointLocations.length >= 2) {
                        var circ = new mm.Polyline(thisMap.pointLocations, thisMap.inputLineOptions);
                        thisMap.userLayer.push(circ);
                    }
                    break;
            }
        },

        // Finish up a user input object, of a type depending on the QueryType.
        // Viewport: none (ignore)
        // Proximity: single Point (ignore)
        // Line: LineString
        // Area: Polygon
        //
        // This is a map callback.
        // The callback argument is a Microsoft.Maps.MouseEventArgs
        MouseDoubleClicked: function (e) {
            if (thisMap.inputFinished) {
                return;
            }
            var mm = Microsoft.Maps;
            var location = thisMap.map.tryPixelToLocation(new mm.Point(e.pageX, e.pageY), mm.PixelReference.page);
            if (location == null || isNaN(location.latitude) || isNaN(location.longitude)) {
                return;
            }

            var currentTool = thisMap.regionType.val();

            // a point may already have been pushed, so we check for dups
            var pl = thisMap.pointLocations;
            var dup = (pl.length > 0) && mm.Point.areEqual(pl[pl.length - 1], location);

            // ignore Proximity and Viewport
            switch (currentTool) {
                case "Line":
                    if (!dup) {
                        thisMap.pointLocations.push(location);
                    }

                    thisMap.userLayer.clear();
                    var line = new mm.Polyline(thisMap.pointLocations, thisMap.inputLineOptions);
                    thisMap.userLayer.push(line);
                    break;

                case "4": // polygon                    
                    if (!dup) {
                        thisMap.pointLocations.push(location);
                    }                 
                    thisMap.pointLocations.push(thisMap.pointLocations[0]);
                    thisMap.userLayer.clear();                 
                    var polygon = new mm.Polygon(thisMap.pointLocations, thisMap.inputPolygonOptions);              
                    thisMap.userLayer.push(polygon);
                    thisMap.newGeo = { coordinates: [[[]]] };
                    $.each(thisMap.pointLocations, function (index, loc) {
                        thisMap.newGeo.coordinates[0][0][index] = [loc.latitude, loc.longitude];
                    });

                    break;
            }

            // we are finished with input
            thisMap.inputFinished = true;

            // prevent map from moving
            e.handled = true;
        },

        // This is a map callback.
        // The callback argument is a Microsoft.Maps.MouseEventArgs
        MouseMoved: function (e) {
            var mm = Microsoft.Maps;
            var location = thisMap.map.tryPixelToLocation(new mm.Point(e.pageX, e.pageY), mm.PixelReference.page);
            if (location == null || isNaN(location.latitude) || isNaN(location.longitude)) {
                return;
            }




            var $panel = thisMap.mapInfo;
            $panel.show();
            thisMap.InfoLatitude.text(util.decimalToDMS(location.latitude.toFixed(8), 'lat'));
            thisMap.InfoLongitude.text(util.decimalToDMS(location.longitude.toFixed(8), 'long'));
            thisMap.InfoZoom.text(thisMap.map.getZoom().toFixed(0));
        },

        MouseOut: function(e) {
            var $panel = thisMap.mapInfo;
            $panel.hide();
        },

        reset: function() {
            thisMap.clearAll();
        },

        clearAll: function () {
            thisMap.displayLayer.clear();
            thisMap.userLayer.clear();

            thisMap.inputFinished = true;
        },

        centerMap: function (location) {
            thisMap.map.setView({ center: location });
        },

        addPushPin: function (lat, lon, options, info) {
            pushPinOptions = $.extend({
                zIndex: 2,
            }, options)
            var location = new mm.Location(lat, lon);
            var pin = new mm.Pushpin(location, pushPinOptions);
            pin.info = info;
            thisMap.userLayer.push(pin);
            mm.Events.addHandler(pin, 'click', thisMap.displayInfobox);
            return pin;
        },

        removePushPin: function(pin) {
            var index = thisMap.userLayer.indexOf(pin);
            if (index != -1) {
                thisMap.userLayer.removeAt(index);
            }
        },


        displayInfobox: function (e) {            
            var map = thisMap.map;
            if (this.target.id && this.target.id.indexOf("pin") != -1) {
                alert("click event fired on pushpin");

                infobox.setOptions({
                    title: this.target.info.Title,
                    description: this.target.info.Description,
                    visible: true,
                    offset: new Microsoft.Maps.Point(0, 25)
                });

                infobox.setLocation(this.target.getLocation());

                //A buffer limit to use to specify the infobox must be away from the edges of the map.

                var buffer = 30;
                var infoboxOffset = infobox.getOffset();
                var infoboxAnchor = infobox.getAnchor();
                var infoboxLocation = map.tryLocationToPixel(this.target.getLocation(), Microsoft.Maps.PixelReference.control);
                var dx = infoboxLocation.x + infoboxOffset.x - infoboxAnchor.x;
                var dy = infoboxLocation.y - 25 - infoboxAnchor.y;

                if (dy < buffer) { //Infobox overlaps with top of map.
                    //#### Offset in opposite direction.
                    dy *= -1;
                    //#### add buffer from the top edge of the map.
                    dy += buffer;
                } else {
                    //#### If dy is greater than zero than it does not overlap.
                    dy = 0;
                }

                if (dx < buffer) { //Check to see if overlapping with left side of map.
                    //#### Offset in opposite direction.
                    dx *= -1;
                    //#### add a buffer from the left edge of the map.
                    dx += buffer;
                } else { //Check to see if overlapping with right side of map.
                    dx = map.getWidth() - infoboxLocation.x + infoboxAnchor.x - infobox.getWidth();
                    //#### If dx is greater than zero then it does not overlap.
                    if (dx > buffer) {
                        dx = 0;
                    } else {
                        //#### add a buffer from the right edge of the map.
                        dx -= buffer;
                    }
                }

                //#### Adjust the map so infobox is in view
                if (dx != 0 || dy != 0) {
                    map.setView({
                        centerOffset: new Microsoft.Maps.Point(dx, dy),
                        center: map.getCenter()
                    });
                }
            }
        },

  



        showAll: function () {

            var locs = [];
            function addLocationFromCollection(entityCollection) {

                for (var i = 0; i < entityCollection.getLength() ; i++) {
                    var entity = entityCollection.get(i);
                    if (!entity._locationsArrays) {
                        if (entity._location) {
                            locs.push(entity._location);
                        }
                    } else {

                        for (var ii = 0; ii < entity._locationsArrays.length; ii++) {
                            for (var iii = 0; iii < entity._locationsArrays[ii].length; iii++) {
                                locs.push(entity._locationsArrays[ii][iii]);
                            }
                        }
                    }

                }

            }

            addLocationFromCollection(thisMap.userLayer);
            addLocationFromCollection(thisMap.displayLayer);
            addLocationFromCollection(thisMap.modelLayer);





            if (locs.length == 1) {
                thisMap.map.setView({ center: locs[0], zoom: 17 });
            } else {
                var viewBoundaries = mm.LocationRect.fromLocations(locs);
                thisMap.map.setView({ bounds: viewBoundaries });
            }
        },

        addGeoToLayer: function (geo, layer, drawingOptions, info) {
            if (!geo || !geo.Features) {
                return;
            }
            var option = $.extend(drawingOptions, thisMap.drawingOptions[geo.Name]);

            $.each(geo.Features, function (index, feature) {
                thisMap.addMultiPolygonToLayer(feature.Geometry, feature.Properties, option, layer);
            });
        },

        addMultiPolygonToLayer: function (geometry, info, drawoptions, layer, clickCallback) {
            if (!layer) {
                layer = thisMap.modelLayer;
            }

            var firstPolygon = null;
            $.each(geometry.Coordinates, function (index, poly) {

                this.polyCoordinates = [];

                var self = this
                $.each(poly, function (rindex, ring) {
                    self.polyCoordinates[rindex] = [];
                    $.each(ring, function (cindex, coord) {
                        var location = new mm.Location(coord[0], coord[1]);
                        thisMap.locs.push(location);
                        self.polyCoordinates[rindex].push(location);
                    });
                });

                var polygon = new mm.Polygon(self.polyCoordinates, drawoptions);

                polygon["feature"] = info;
                mm.Events.addHandler(polygon, "mouseover", thisMap.ItemMouseover);
                mm.Events.addHandler(polygon, "mouseout", thisMap.ItemMouseout);
                if (clickCallback) {
                    mm.Events.addHandler(polygon, 'click', clickCallback);
                }

                if (!firstPolygon) firstPolygon = polygon;
                layer.push(polygon);
            });

            return firstPolygon;

        },



        // Show item properties when the mouse hovers over it.
        //
        // This is a map callback.
        // The callback argument is a Microsoft.Maps.MouseEventArgs
        ItemMouseover: function (mea) {

            var $props = thisMap.ItemProperties;
            $("tbody tr", $props).remove();
            var properties = mea.target.feature;

            if (!properties)
            {
                return;
            }

            $.each(properties, function (key, value) {
                var row = $('<tr><td>' + key + ':</td><td>' + value + '</td></tr>');
                $("tbody", $props).append(row);
            });

            var top = mea.pageY + 10;
            var left = mea.pageX + 10;
            $props.css({ top: top, left: left });
            $props.show();
        },

        // Clear the properties display when the mouse leaves it.
        //
        // This is a map callback.
        // The callback argument is a Microsoft.Maps.MouseEventArgs
        ItemMouseout: function (mea) {
            var $props = thisMap.ItemProperties;
            $props.hide();
        },


        Include: function (e) {        
            if (!thisMap.newGeo) {
                return;
            }

            thisMap.clearAll();

            var modelGeo = thisMap.modelGeo ? (typeof ((thisMap.modelGeo).toJSON) === 'function') ? (thisMap.modelGeo).toJSON() : thisMap.modelGeo : null;

            $.ajax({
                data: { mutliPolygon: modelGeo, includedRegion: thisMap.newGeo },
                cache: false,
                success: thisMap.receiveData,
                type: "POST",
                dataType: "json",
                url: "/Map/Include"
            });

            thisMap.regionType.data("kendoDropDownList").select(0);
            thisMap.firs.data("kendoDropDownList").select(0);
            thisMap.regionType.data("kendoDropDownList").trigger("change");           

        },


        Exclude: function () {

            if (!thisMap.newGeo) {
                return;
            }

            thisMap.clearAll();

            var modelGeo = thisMap.modelGeo ? (typeof ((thisMap.modelGeo).toJSON) === 'function') ? (thisMap.modelGeo).toJSON() : thisMap.modelGeo : null;

            $.ajax({
                data: { mutliPolygon: modelGeo, excludedRegion: thisMap.newGeo },
                cache: false,
                success: thisMap.receiveData,
                type: "POST",
                dataType: "json",
                url: "/Map/Exculde"
            });

            thisMap.regionType.data("kendoDropDownList").select(0);
            thisMap.firs.data("kendoDropDownList").select(0);
            thisMap.regionType.data("kendoDropDownList").trigger("change");

        },
        
    }
});
﻿import { Component, OnInit, OnDestroy, Inject, Injector, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms'

import { Select2OptionData } from 'ng2-select2';

import { ISubject, ISdoSubject, IcaoSubject, IcaoCondition, IInDoaValidationResult } from '../../shared/data.model'
import { DataService } from '../../shared/data.service'

import { CatchAllService } from './catchall.service'
//import { ISdoSubject } from './catchall.model'
import { TOASTR_TOKEN, Toastr } from './../../../common/toastr.service';
import { WindowRef } from '../../../common/windowRef.service'

import { LocaleService, TranslationService } from 'angular-l10n';

declare var $: any;

@Component({
    templateUrl: '/app/dashboard/nsds/catchall/catchall-cancel.component.html',
    styleUrls: ['app/dashboard/nsds/catchall/catchall-draft.component.css']
})
export class CatchAllCancelComponent implements OnInit, OnDestroy {
    token: any = null;
    isReadOnly: boolean = true
    isBilingualRegion: boolean = false;
    overrideBilingual: boolean = false;

    subjectSelected: ISdoSubject = null;
    icaoSubjectSelected: IcaoSubject = null;
    icaoConditionSelected: IcaoCondition = null;

    subjects: ISdoSubject[];
    icaoSubjects: IcaoSubject[];
    icaoConditions: IcaoCondition[];

    icaoSubjectSelectOptions = {
        width: "100%"
    }
    icaoConditionSelectOptions = {
        width: "100%",
        placeholder: {
            id: '-1',
            text: this.translation.translate('Nsd.ICAOConditionPlaceholder')
        }
    }

    units: Select2OptionData[];
    scopes: Select2OptionData[];
    series: Select2OptionData[];

    nsdForm: FormGroup;
    nonIcaoAerodrome: boolean = false;
    nsdFrmGroup: FormGroup;
    leafletmap: any;
    prevItemE : string
    prevItemEFrench: string

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private injector: Injector,
        private dataService: DataService,
        private catchAllService: CatchAllService,
        private fb: FormBuilder,
        private windowRef: WindowRef,
        private changeDetectionRef: ChangeDetectorRef,
        public translation: TranslationService) {

        this.token = this.injector.get('token');
        this.nsdForm = this.injector.get('form');
    }

    ngOnInit() {
        const app = this.catchAllService.app;

        this.leafletmap = app.leafletmap;

        this.createUnitsListBox();
        this.createSeriesListBox();

        let frmArr = <FormArray>this.nsdForm.controls['frmArr'];

        if (frmArr.length === 0) {
            this.configureReactiveForm();
        } else {
            /*
            this is required because of the grouping funtionallity. When the NsdFormGroup 
            was configured and any of items in the group has changed (new set of tokens values)
            the NsdForm Group needs to be updated with the new tokens values */
            this.updateReactiveForm(frmArr);
        }


        this.loadSubject(this.token.subjectId);

        if (this.token.type === "cancel") {
            let fc = this.nsdFrmGroup.get('itemEValue');
            this.prevItemE = fc.value;

            fc = this.nsdFrmGroup.get('itemEFrenchValue');
            this.prevItemEFrench = fc.value;
        }

        this.overrideBilingual = this.token.overrideBilingual;
        this.loadMapLocations(app.doaId);
    }

    ngOnDestroy() {
        this.leafletmap.dispose();
        this.leafletmap = null;
    }

    requireLowerUpperLimit(): boolean {
        return this.nsdFrmGroup.get('icaoConditionId').value != -1 &&
            (!this.nsdFrmGroup.get('requiresItemFG').value ||
                this.nsdFrmGroup.get('grpItemFG.itemFUnits').value === 2 ||
                this.nsdFrmGroup.get('grpItemFG.itemGUnits').value === 2);
    }

    requireItemFG(): boolean {
        return this.nsdFrmGroup.get('requiresItemFG').value;
    }

    icaoConditionChanged(e: any): void {
        let conditionId = +e.value;

        const icaoConditionIdCtrl = this.nsdFrmGroup.get('icaoConditionId');

        if (conditionId !== +icaoConditionIdCtrl.value) {
            icaoConditionIdCtrl.setValue(conditionId);
        }
        icaoConditionIdCtrl.markAsDirty();

        if (this.icaoConditionSelected && this.icaoConditionSelected.id === conditionId) {
            return; //when multiples change events occurring react only to the first one
        }

        this.icaoConditionSelected = this.icaoConditions.filter(item => item.id === conditionId)[0];
    }

    validateIcaoConditionId() {
        const fc = this.nsdFrmGroup.get('icaoConditionId');
        return fc.value !== -1 || this.nsdFrmGroup.pristine;
    }

    validateAerodromeName() {
        const fc = this.nsdFrmGroup.get('aerodromeName');
        return !fc.errors || this.nsdFrmGroup.pristine;
    }

    select2Options(): any {
        const self = this;
        const app = this.catchAllService.app;
        return {
            minimumInputLength: 2,
            width: "100%",
            templateResult: function (data: any) {
                return self.dataService.getSdoExtendedName(data);
            },
            templateSelection: function (selection: ISdoSubject) {
                self.nonIcaoAerodrome = self.designatorHasNumbers(selection.designator);
                self.createScopesListBox(selection.sdoEntityName);
                return self.dataService.getSdoExtendedName(selection);
            },
        }
    }

    private loadSubject(subjectId: string) {
        this.dataService.getSdoSubject(subjectId)
            .subscribe((sdoSubjectResult: ISdoSubject) => {
                this.subjectSelected = sdoSubjectResult;
                this.subjects = [];
                this.subjects.push(sdoSubjectResult);
                this.nonIcaoAerodrome = this.designatorHasNumbers(sdoSubjectResult.designator);
                if (this.nonIcaoAerodrome) {
                    const aerodromeCtrl = this.nsdFrmGroup.get('aerodromeName');
                    if (!aerodromeCtrl.value) {
                        aerodromeCtrl.markAsDirty();
                    }
                    aerodromeCtrl.setValidators(Validators.required);
                }

                this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);
                this.loadIcaoSubjects(sdoSubjectResult.sdoEntityName);

                this.updateBilingualFromTokenLocation(this.token.subjectLocation);
            });
    }

    private updateBilingualFromTokenLocation(value: string) {
        this.dataService.validateLocation(value)
            .subscribe((result: IInDoaValidationResult) => this.setBilingualRegion(result.inBilingualRegion));
    }

    private loadSeries(qCode: string): void {
        this.dataService.getSeries()
            .subscribe((series: Array<Select2OptionData>) => {
                this.setFirstSelectItemInCombo(series, this.token.series, false);
                this.series = series;
            });
    }

    private loadMapLocations(doaId: number) {
        const self = this;
        this.dataService.getDoaLocation(doaId)
            .subscribe((geoJsonStr: any) => {
                const interval = window.setInterval(function () {
                    if (self.leafletmap && self.leafletmap.isReady() ) {
                        window.clearInterval(interval);
                        self.leafletmap.addDoa(geoJsonStr, 'DOA');
                        self.updateMap(self.subjectSelected);
                    };
                }, 100);
            }, (error: any) => {
                this.toastr.error(this.translation.translate('Nsd.DOAGeoError') + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    private loadIcaoSubjects(sdoEntityName: string) {
        if (sdoEntityName) {
            this.dataService.getIcaoSubjects(sdoEntityName)
                .subscribe((icaoSubjects: IcaoSubject[]) => {
                    this.loadIcaoConditions();
                    this.setFirstSelectItemInCombo(icaoSubjects, this.token.icaoSubjectId);
                    this.icaoSubjects = icaoSubjects.map(
                        function (item: IcaoSubject) {
                            if (item.code !== undefined) {
                                if (item.name.length > 90) {
                                    item.text = item.code + ' - ' + item.name.substr(0, 80) + '...';
                                } else {
                                    item.text = item.code + ' - ' + item.name;
                                }
                            }
                            return item;
                        });
                });
        }
    }

    private loadIcaoConditions() {
        this.dataService.getIcaoConditions(this.token.icaoSubjectId, true)
            .subscribe((icaoConditions: IcaoCondition[]) => {
                icaoConditions.unshift({
                    id: -1,
                    text: '',
                });
                this.setFirstSelectItemInCombo(icaoConditions, this.token.icaoConditionId);
                this.icaoConditions = icaoConditions.map(
                    function (item: IcaoCondition) {
                        if (item.code !== undefined) {
                            if (item.description.length > 90) {
                                item.text = item.code + ' - ' + item.description.substr(0, 80) + '...';
                            } else {
                                item.text = item.code + ' - ' + item.description;
                            }
                        }
                        return item;
                    });
                this.changeDetectionRef.detectChanges();
            });
    }

    private updateMap(sdoSubject: ISubject) {
        this.leafletmap.clearFeaturesOnLayers(['DOA']);
        if (sdoSubject && sdoSubject.subjectGeoRefPoint) {
            const geojson = JSON.parse(sdoSubject.subjectGeoRefPoint);
            if (geojson && geojson.features.length === 1) {
                const markerGeojson = this.getGeoJsonFromCurrentLocation();
                this.leafletmap.addMarker(geojson, sdoSubject.name, () => {
                    this.leafletmap.addFeature(sdoSubject.subjectGeo, sdoSubject.name, () => {
                        if (markerGeojson) {
                            this.leafletmap.setNewMarkerLocation(markerGeojson);
                        }
                    });
                });
            }
        }
    }

    private getGeoJsonFromCurrentLocation() {
        return this.token && this.token.latitude && this.token.longitude
            ? this.leafletmap.latitudeLongitudeToGeoJson(this.token.latitude, this.token.longitude)
            : null;
    }

    private setFirstSelectItemInCombo(items: any, itemId: any, insertUndefinedAtZero: boolean = true) {
        const index = items.findIndex(x => x.id === itemId);
        if (index > 0) { //move selected item to position 0
            items.splice(0, 0, items.splice(index, 1)[0]);
        }
    }

    private designatorHasNumbers(designator: string) {
        for (let i = 0; i < designator.length; i++) {
            if (parseFloat(designator[i])) {
                return true;
            }
        }
        return false;
    }

    private createUnitsListBox(): void {
        this.units = [
            { id: '0', text: 'FL' },
            { id: '1', text: 'AMSL' },
            { id: '2', text: 'AGL' }
        ];
    }

    private createScopesListBox(entityName: string): void {
        switch (entityName) {
            case "Ahp":
                this.scopes = [
                    { id: 'A', text: 'A' },
                    { id: 'AE', text: 'AE' }];
                break;
            case "FIR":
                this.scopes = [
                    { id: 'E', text: 'E' },
                    { id: 'W', text: 'W' }];
                break;
            default:
                this.scopes = [
                    { id: 'A', text: 'A' },
                    { id: 'E', text: 'E' },
                    { id: 'W', text: 'W' },
                    { id: 'AE', text: 'AE' },
                    { id: 'AW', text: 'AW' },
                    { id: 'K', text: 'K' }];
                break;
        }
    }

    private createSeriesListBox(): void {
        this.dataService.getSeries()
            .subscribe((series: Array<Select2OptionData>) => {
                this.series = series;
            });
    }

    private setBilingualRegion(value: boolean) {
        this.token.isBilingual = value;
        this.isBilingualRegion = value;
        let itemEFCtrl = this.nsdFrmGroup.get('itemEFrenchValue');
        if (itemEFCtrl) {
            if (value || this.overrideBilingual) {
                if (!this.isReadOnly && !itemEFCtrl.enabled) {
                    itemEFCtrl.enable();
                    itemEFCtrl.setValidators(Validators.required);
                }
            } else {
                if (!this.isReadOnly && itemEFCtrl.enabled) {
                    itemEFCtrl.disable();
                    itemEFCtrl.clearValidators();
                }
            }
        }
    }

    private configureReactiveForm() {

        const frmArrControls = <FormArray>this.nsdForm.controls['frmArr'];

        if (!this.windowRef.appConfig.isNof) {
            this.token.icaoConditionId = -1; //NOF User should see the cancellation entry from the user
        }
        if (this.token.itemEFrenchValue && this.token.itemEFrenchValue.length > 0) {
            this.isBilingualRegion = true;
        }

        this.nsdFrmGroup = this.fb.group({
            subjectId: [this.token.subjectId, []],
            icaoSubjectId: [this.token.icaoSubjectId, []],
            icaoConditionId: [this.token.icaoConditionId || -1, [icaoConditionValidator]],
            subjectLocation: [{ value: this.token.subjectLocation, disabled: this.isReadOnly }, []],
            latitude: [{ value: this.token.latitude, disabled: this.isReadOnly }, []],
            longitude: [{ value: this.token.longitude, disabled: this.isReadOnly }, []],
            scope: this.token.scope,
            series: this.token.series,
            itemA: { value: this.token.itemA, disabled: this.isReadOnly },
            itemEValue: [{ value: this.token.itemEValue, disabled: false }, [Validators.required, itemEMustChangeValidator(this.token.itemEValue)]],
            itemEFrenchValue: [{ value: this.token.itemEFrenchValue, disabled: !this.isBilingualRegion }],
            aerodromeName: { value: this.token.aerodromeName, disabled: this.isReadOnly },
            grpPurposes: this.fb.group({
                purposeB: this.token.purposeB,
                purposeM: this.token.purposeM,
                purposeN: this.token.purposeN,
                purposeO: this.token.purposeO,
            }),
            lowerLimit: { value: this.token.lowerLimit, disabled: this.isReadOnly },
            upperLimit: { value: this.token.upperLimit, disabled: this.isReadOnly },
            radius: { value: this.token.radius, disabled: this.isReadOnly },
            grpTraffic: this.fb.group({
                trafficI: this.token.trafficI,
                trafficV: this.token.trafficV,
            }),
            grpItemFG: this.fb.group({
                itemFValue: { value: this.token.itemFValue, disabled: this.isReadOnly },
                itemFSurface: { value: this.token.itemFSurface, disabled: this.isReadOnly },
                itemFUnits: this.token.itemFUnits,
                itemGValue: { value: this.token.itemGValue, disabled: this.isReadOnly },
                itemGUnlimited: { value: this.token.itemGUnlimited, disabled: this.isReadOnly },
                itemGUnits: this.token.itemGUnits,
            }),
            requiresItemA: this.token.itemA !== null,
            requiresSeries: this.token.series !== null,
            requiresScope: this.token.scope !== null,
            requiresPurpose: this.token.purposeB || this.token.purposeM || this.token.purposeN || this.token.purposeO,
            requiresItemFG: this.token.requiresItemFG // itemFValue || this.token.itemGValue
        });

        if (this.token.scope) {
            this.createScopesListBox(this.token.subjectId.substring(0, 3));
            const scope = this.scopes.filter(item => item.id === this.token.scope);
            if (scope.length === 1) {
                const fc = this.nsdFrmGroup.get('scope');
                //fc.setValue(scope[0].id);
            }
        }
        if (this.token.series) {
            const series = this.series.filter(item => item.id === this.token.series);
            if (series.length === 1) {
                const fc = this.nsdFrmGroup.get('series');
                fc.setValue(series[0].id);
            }
        }

        if (this.isBilingualRegion) {
            const control = this.nsdFrmGroup.get('itemEFrenchValue');
            control.setValidators([Validators.required, itemEFrenchMustChangeValidator(this.token.itemEFrenchValue)]);
        }

        frmArrControls.push(this.nsdFrmGroup);
    }

    private updateReactiveForm(formArr: FormArray) {
        this.nsdFrmGroup = <FormGroup>formArr.controls[0];

        //It is better to update only those controls that tokens values have changed.
        this.updateFormControlValue("subjectId", this.token.subjectId);
        this.updateFormControlValue("icaoSubjectId", this.token.icaoSubjectId);
        this.updateFormControlValue("icaoConditionId", this.token.icaoConditionId);
        this.updateFormControlValue("subjectLocation", this.token.subjectLocation);
        this.updateFormControlValue("latitude", this.token.latitude);
        this.updateFormControlValue("longitude", this.token.longitude);
        this.updateFormControlValue("scope", this.token.scope);
        this.updateFormControlValue("series", this.token.series);
        this.updateFormControlValue("itemA", this.token.itemA);
        this.updateFormControlValue("aerodromeName", this.token.aerodromeName);
        this.updateFormControlValue("itemEValue", this.token.itemEValue);
        this.updateFormControlValue("itemEFrenchValue", this.token.itemEFrenchValue);
        this.updateFormControlValue("lowerLimit", this.token.lowerLimit);
        this.updateFormControlValue("upperLimit", this.token.upperLimit);
        this.updateFormControlValue("radius", this.token.radius);
        this.updateFormControlValue("requiresItemA", this.token.requiresItemA);
        this.updateFormControlValue("requiresSeries", this.token.requiresSeries);
        this.updateFormControlValue("requiresPurpose", this.token.requiresPurpose);
        this.updateFormControlValue("requiresItemFG", this.token.requiresItemFG);
        this.updateFormControlValue("grpPurposes.purposeB", this.token.purposeB);
        this.updateFormControlValue("grpPurposes.purposeM", this.token.purposeM);
        this.updateFormControlValue("grpPurposes.purposeN", this.token.purposeN);
        this.updateFormControlValue("grpPurposes.purposeO", this.token.purposeO);
        this.updateFormControlValue("grpTraffic.trafficI", this.token.trafficI);
        this.updateFormControlValue("grpTraffic.trafficV", this.token.trafficV);
        this.updateFormControlValue("grpItemFG.itemFValue", this.token.itemFValue);
        this.updateFormControlValue("grpItemFG.itemFSurface", this.token.itemFSurface);
        this.updateFormControlValue("grpItemFG.itemFUnits", this.token.itemFUnits);
        this.updateFormControlValue("grpItemFG.itemGValue", this.token.itemGValue);
        this.updateFormControlValue("grpItemFG.itemGUnlimited", this.token.itemGUnlimited);
        this.updateFormControlValue("grpItemFG.itemGUnits", this.token.itemGUnits);
    }

    private updateFormControlValue(controlName: string, value) {
        let control = this.nsdFrmGroup.get(controlName);
        if (control && control.value !== value) {
            control.setValue(value);
        }
    }
}



//Custom Validators
function icaoConditionValidator(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value === -1) {
        return { 'required': true }
    }
    return null;
}

function itemEMustChangeValidator(itemEValue: string): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        if (window["app"].isNof) {
            return null;
        }

        var v: string = control.value;
        return v === itemEValue ? { "mustchange": true } : null;
    };
}

function itemEFrenchMustChangeValidator(itemEFrenchValue: string): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        if (window["app"].isNof) {
            return null;
        }

        var v: string = control.value;
        return v === itemEFrenchValue ? { "mustchange": true } : null;
    };
}



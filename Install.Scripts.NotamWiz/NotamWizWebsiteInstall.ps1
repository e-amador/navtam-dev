Set-PSDebug -Trace 1
Import-Module WebAdministration

#Stop website and install

$site = "Default Web Site"
Stop-Website -Name $site
Stop-WebAppPool -Name "DefaultAppPool"

start-sleep -s 15


$scriptPath = split-path -parent $MyInvocation.MyCommand.Definition
$migrateToolPath = $scriptPath + "\migrate.exe"
$versionPath = $scriptPath + "\version.txt"

$deployPath = $scriptPath+"\NES.deploy.cmd /Y"
Invoke-Expression $deployPath

##copy migrate command
Copy-Item -Path $migrateToolPath -Destination "C:\inetpub\wwwroot\bin\migrate.exe"
#copy version information
Copy-Item -Path $versionPath -Destination "C:\inetpub\wwwroot"

##migrate database
$migrateCommand = ' cd C:\inetpub\wwwroot\bin\'
Invoke-Expression $migrateCommand

$migrateCommand = 'C:\inetpub\wwwroot\bin\migrate.exe Core.Data.EF.dll /startupConfigurationFile="..\web.config"'
Invoke-Expression $migrateCommand

Start-Website -Name "Default Web Site"
Start-WebAppPool -Name "DefaultAppPool"
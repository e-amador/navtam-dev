﻿using System.Device.Location;
using Microsoft.Ajax.Utilities;
using NavCanada.Applications.NotamWiz.Web.Code.Extentions;
using NavCanada.Applications.NotamWiz.Web.Code.GeoTools;
using NavCanada.Applications.NotamWiz.Web.Code.Helpers;
using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.NSDs.CatchAll.V0R1
{
    using System.Linq;
    using System.Threading.Tasks;

    using FluentValidation;
    using FluentValidation.Results;

    using Core.Data.NotamWiz.Contracts;
    using Core.Domain.Model.Entitities;


    public class CatchAllValidator : AbstractValidator<ViewModel>
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(CatchAllValidator));

        public CatchAllValidator(NsdBaseController controller, IUow uow, IDbResWrapper dbRes)
        {
            Controller = controller;
            Uow = uow;

            RuleFor(v => v.SubjectLocation).Must(GeoDataService.IsValidLocation).WithMessage(dbRes.T("Invalid Location", "NSD-CatchAll")).MustAsync(BeWithinDoa).WithMessage(dbRes.T("Subject Location is not Within your Domain of Authority.", "NSD-CatchAll"));
            //RuleFor(v => IcaoCondition).Cascade(CascadeMode.StopOnFirstFailure).Must(CheckIcaoCondition).WithMessage(dbRes.T("Invalid ICAO Subject Condition.", "NSD-CatchAll")); ;
            RuleFor(v => v.Traffic).Must( t=> !string.IsNullOrEmpty(t) && (t == "V" || t == "I" || t== "IV")).WithMessage("Invalid Traffic");

            RuleFor(v => v.Scope).Must(ValidateScope).WithMessage("Invalid Scope");
            //RuleFor(v => v.Scope).Matches("^(A|E|W|AE|AW|EW|K)$").WithMessage("Invalid Scope");
            
            //// ICAO Subject and Condition
            RuleFor(v => v).Must(v => IcaoSubject != null).WithName("ICAO Subject").WithMessage(dbRes.T("Invalid ICAO Subject and/or Condition", "NSD-CatchAll"));
            RuleFor(v => v.ICAOSubjectConditionId).Cascade(CascadeMode.StopOnFirstFailure).MustAsync(t=> CheckIcaoCondition(t, Uow)).WithMessage(dbRes.T("Invalid ICAO Subject Condition.", "NSD-CatchAll")); ;

            RuleFor(v => v).Must(v => IcaoSubject.EntityCode.Contains(Controller.Subject.SubjectType)).WithName("ICAO Subject").When(v => IcaoSubject != null).WithMessage(dbRes.T("Invalid ICAO Subject code", "NSD-CatchAll"));
            When(v => IcaoSubject != null, () =>
            {
                // ItemA
                RuleFor(v => v.ItemA).Cascade(CascadeMode.StopOnFirstFailure).NotEmpty().WithMessage(dbRes.T("Item A is Required", "NSD-CatchAll")).Matches("^([^0-9]*)$").WithMessage(dbRes.T("No digits allowed in Item A", "NSD-CatchAll")).Must(BeValidItemA).WithMessage(dbRes.T("Invalid Item A", "NSD-CatchAll")).MustAsync(ItemAbeInDoa).WithMessage(dbRes.T("One or more Item A subjects are not in your DOA.", "NSD-CatchAll"));

                // Purpose
                RuleFor(v => v).Must(v => IcaoCondition != null && v.PurposeB == IcaoCondition.B && v.PurposeM == IcaoCondition.M && v.PurposeN == IcaoCondition.N && v.PurposeO == IcaoCondition.O).WithName("Purpose").When(v => IcaoCondition != null && !IcaoCondition.AllowPurposeChange).WithMessage(dbRes.T("Invalid Purpose, Purpose change is not allowed.", "NSD-CatchAll"));
                RuleFor(v => v).Must(v => !(v.PurposeB && v.PurposeM && v.PurposeN && v.PurposeO)).WithName("Purpose").When(v => IcaoCondition != null &&  IcaoCondition.AllowPurposeChange).WithMessage(dbRes.T("Invalid Purpose, More than 3 Purposes are defined.", "NSD-CatchAll"));
                RuleFor(v => v).Must(v => v.PurposeB || v.PurposeM || v.PurposeN || v.PurposeO).WithName("Purpose").When(v => IcaoCondition != null && IcaoCondition.AllowPurposeChange).WithMessage(dbRes.T("Purpose is required.", "NSD-CatchAll"));

                // Traffic
                RuleFor(v => v.Traffic).NotEmpty().WithMessage(dbRes.T("Traffic is Required.", "NSD-CatchAll")).Matches("^(I|V|IV)$").When(v => IcaoSubject.AllowScopeChange).WithMessage(dbRes.T("Invalid Traffic.", "NSD-CatchAll"));

                // ItemF and ItemG
                RuleFor(v => v).Must(v => v.ItemF == null && !v.ItemFSFC).WithName("ItemF").When(v => IcaoCondition != null && !IcaoCondition.AllowFgEntry).WithMessage(dbRes.T("ItemF is not allowed", "NSD-CatchAll"));
                RuleFor(v => v).Must(v => v.ItemG == null && !v.ItemGUNL).WithName("ItemG").When(v => IcaoCondition != null && !IcaoCondition.AllowFgEntry).WithMessage(dbRes.T("ItemG is not allowed", "NSD-CatchAll"));
                RuleFor(v => v.ItemF).NotEmpty().When(v => IcaoCondition != null && IcaoCondition.AllowFgEntry && !v.ItemFSFC).WithMessage(dbRes.T("Item F is required", "NSD-CatchAll"));
                RuleFor(v => v.ItemG).NotEmpty().When(v => IcaoCondition != null && IcaoCondition.AllowFgEntry && !v.ItemGUNL).WithMessage(dbRes.T("Item G is required", "NSD-CatchAll"));
                RuleFor(v => v.ItemF).GreaterThanOrEqualTo(0).When(v => IcaoCondition != null && IcaoCondition.AllowFgEntry && !v.ItemFSFC).WithMessage(dbRes.T("Item F should be greater than or equal to 0", "NSD-CatchAll"));
                RuleFor(v => v.ItemF).LessThanOrEqualTo(999999).When(v => IcaoCondition != null && IcaoCondition.AllowFgEntry && !v.ItemFSFC && (v.FMeasurementUnit == enFandGMeasurementUnit.AGL || v.FMeasurementUnit == enFandGMeasurementUnit.AMSL)).WithMessage(dbRes.T("Item F should be less than or equal to 999999", "NSD-CatchAll"));
                RuleFor(v => v.ItemF).LessThanOrEqualTo(999).When(v => IcaoCondition != null && IcaoCondition.AllowFgEntry && !v.ItemFSFC && v.FMeasurementUnit == enFandGMeasurementUnit.FL).WithMessage(dbRes.T("Item F should be less than or equal to 999", "NSD-CatchAll"));
                RuleFor(v => v.ItemG).LessThanOrEqualTo(999999).When(v => IcaoCondition != null && IcaoCondition.AllowFgEntry && !v.ItemGUNL && (v.GMeasurementUnit == enFandGMeasurementUnit.AGL || v.GMeasurementUnit == enFandGMeasurementUnit.AMSL)).WithMessage(dbRes.T("Item G should be less than or equal to 999999", "NSD-CatchAll"));
                RuleFor(v => v.ItemG).LessThanOrEqualTo(999).When(v => IcaoCondition != null && IcaoCondition.AllowFgEntry && !v.ItemGUNL && v.GMeasurementUnit == enFandGMeasurementUnit.FL).WithMessage(dbRes.T("Item G should be less than or equal to 999", "NSD-CatchAll"));
                RuleFor(v => v.ItemF).LessThanOrEqualTo(v => v.ItemG).When(v => IcaoCondition != null && IcaoCondition.AllowFgEntry && !v.ItemFSFC && !v.ItemGUNL).WithMessage(dbRes.T("Item G should be less than or equal to item F", "NSD-CatchAll"));

                // Series
                RuleFor(v => v.Series).Cascade(CascadeMode.StopOnFirstFailure).NotEmpty().When(v => IcaoSubject.AllowSeriesChange).WithMessage(dbRes.T("Series is Required.", "NSD-CatchAll")).Length(1).When(v => IcaoSubject.AllowSeriesChange).WithMessage(dbRes.T("Only one Series can be used.", "NSD-CatchAll")).Must(BeValidSeries).When(v => IcaoSubject.AllowSeriesChange).WithMessage(dbRes.T("Series is not in Use.", "NSD-CatchAll"));

                // Scope
                RuleFor(v => v.Scope).Cascade(CascadeMode.StopOnFirstFailure).NotEmpty().When(v => IcaoSubject.AllowScopeChange).WithMessage(dbRes.T("Scope is Required.", "NSD-CatchAll")).Matches("^(A|E|W|AE|AW|EW|K)$").When(v => IcaoSubject.AllowScopeChange).WithMessage(dbRes.T("Invalid Scope.", "NSD-CatchAll"));

                // ItemE
                RuleFor(v => v.EnglishText_FT).Must(t => !t.IsNullOrWhiteSpace() && CommonValidators.IsValidFreeText(t, Uow)).WithName("Invalid ItemE.").WithMessage(dbRes.T("ItemE is required.", "NSD-CatchAll"));
                When(t => controller.IsInBilingualRegion(), () =>
                {
                    RuleFor(v => v.FrenchText_FT).Must(t => !t.IsNullOrWhiteSpace() && CommonValidators.IsValidFreeText(t, Uow)).WithName("Invalid ItemE French.").WithMessage(dbRes.T("ItemE in French is required.", "NSD-CatchAll"));
                });
            });
        }

        private NsdBaseController Controller
        {
            get;
            set;
        }

        private IcaoSubjectCondition IcaoCondition
        {
            get;
            set;
        }

        private IcaoSubject IcaoSubject
        {
            get;
            set;
        }

        private IUow Uow
        {
            get;
            set;
        }

        public override Task<ValidationResult> ValidateAsync(ValidationContext<ViewModel> context)
        {
            IcaoSubject = Uow.IcaoSubjectRepo.GetByIdWithConditions(context.InstanceToValidate.ICAOSubjectId);
            IcaoCondition = IcaoSubject != null ? IcaoSubject.Conditions.FirstOrDefault(c => c.Id == context.InstanceToValidate.ICAOSubjectConditionId) : null;
            return base.ValidateAsync(context);
        }

        private bool BeValidItemA(string itemA)
        {
            var parts = itemA.Split(' ');

            // validate length of 4
            if (parts.Any(p => p.Length != 4))
            {
                return false;
            }

            // validate all items are unique 
            if (parts.Distinct().Count() != parts.Length)
            {
                return false;
            }
            return true;
        }

        private async Task<bool> BeWithinDoa(string location)
        {
            if (!GeoDataService.IsValidLocation(location))
                return false;

            var loc = new GeoCoordinate().FromLocation(location);
            this._log.Object(new
            {
                Method = "BeWithinDoa",
                Parameters = new
                {
                    location = loc
                }
            });
            var doa = await Controller.GetUserDoaAsync();
            return GeoDataService.IsLocationWithinDOA(loc, doa);
        }

        private bool BeValidSeries(string series)
        {
            var usedSeries = Uow.ConfigurationValueRepo.GetAll().Single(c => c.Category == "NOTAM" && c.Name == "NOTAMUsedSeries").Value;
            return usedSeries.Any(s => s == series[0]);
        }

        private async Task<bool> CheckIcaoCondition(long subjectId, IUow uow)
        {
            var subject = await uow.IcaoSubjectConditionRepo.GetByIdAsync(subjectId);
            return subject != null;
        }

        private bool ValidateScope(string scope)
        {
            if (!string.IsNullOrEmpty(scope))
            {
                return System.Text.RegularExpressions.Regex.IsMatch(scope, "^(A|E|W|AE|AW|EW|K)$");
            }

            return false;
        }

        private async Task<bool> ItemAbeInDoa(string itemA)
        {
            var parts = itemA.Split(' ');
            // validate all item A subject are in our DOA
            var doa = await Controller.GetUserDoaAsync();
            foreach (var p in parts)
            {
                if (p == "CXXX")
                {
                    continue;
                }
                if (!Uow.SdoSubjectRepo.GetAll().Any(s => s.SubjectGeo.Intersects(doa.DoaGeoArea) && s.Designator == p))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
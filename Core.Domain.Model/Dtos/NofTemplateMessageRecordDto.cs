﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NavCanada.Core.Domain.Model.Dtos
{
    public class NofTemplateMessageRecordDto
    {
        public int Id { get; set; }
        public Guid TemplateId { get; set; }
        public string TemplateName { get; set; }
        public string Username { get; set; }
        public int SlotNumber { get; set; }
    }
}

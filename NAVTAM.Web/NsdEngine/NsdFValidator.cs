﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Threading;
using NAVTAM.NsdEngine.Helpers;

namespace NAVTAM.NsdEngine
{
    public class NsdFValidator : AbstractValidator<NsdViewModel>
    {
        private readonly NsdViewModel _model;
        private readonly NsdContext _context;

        public NsdFValidator(NsdViewModel model, NsdContext context)
        {
            _model = model;
            _context = context;

            /*
            RuleFor(v => v.StartActivity)
                .NotNull()
                .GreaterThanOrEqualTo(DateTime.UtcNow)
                .When(v => !v.SetBeginDateAtSubmit)
                .WithMessage("Invalid Start Validity Date and/or Time");
            */
            RuleFor(v => v.StartActivity).NotNull()
                .GreaterThanOrEqualTo(DateTime.UtcNow)
                .When(v => !v.Immediate)
                .WithMessage("Invalid Start Validity Date and/or Time");

            When(v => !v.Immediate, () =>
            {
                RuleFor(v => v).MustAsync(MustBeValidVsp)
                    .WithMessage("Start Date/Time is invalid - cannot be greater than {0} hours from current UTC time", _context.VSP)
                    .WithName("Start Validaity");
            });

            When(v => !v.Permanent, () => 
            {
                RuleFor(v => v.EndValidity)
                    .NotNull()
                    .WithMessage("End Validity is required.");

                RuleFor(v => v.EndValidity)
                    .LessThanOrEqualTo(v => v.StartActivity.Value.AddHours(2208))
                    .When(v => !v.Immediate)
                    .WithMessage("Validaity period must not exceed 2208 hourse");

                RuleFor(v => v.EndValidity)
                    .GreaterThanOrEqualTo(v => v.StartActivity.Value.AddMinutes(30))
                    .When(v => !v.Immediate)
                    .WithMessage("Validaity period must exceed 30 minutes");

                RuleFor(v => v.EndValidity)
                    .GreaterThanOrEqualTo(v => DateTime.UtcNow.AddMinutes(30))
                    .When(v => v.Immediate)
                    .WithMessage("Validaity period must exceed 30 minutes");
            });

            When(v => !(string.IsNullOrEmpty(v.ItemD)), () =>
            {
                RuleFor(v => v).MustAsync(MustBeValidTimeScheduleData)
                    .WithMessage("Invalid ItemD", v => v.ItemD)
                    .WithName("ItemD");
            });

        }

        private Task<bool> MustBeValidTimeScheduleData(NsdViewModel model, CancellationToken arg2)
        {
            return Task.FromResult(CommonValidators.ValidateTimeSchedule(model.ItemD, (DateTime)model.StartActivity, (DateTime)model.EndValidity));
        }

        private async Task<bool> MustBeValidVsp(NsdViewModel vm, CancellationToken arg2)
        {
            if (vm.StartActivity == null) return await Task.FromResult(true);
            var hours = (vm.StartActivity - DateTime.UtcNow).Value.TotalHours;
            return _context.VSP == 0 || hours <= _context.VSP;
        }

    }
}
﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using NAVTAM.ViewModels;
using System.Threading.Tasks;
using System.Web.Http;

namespace NAVTAM.Controllers.api
{
    [Authorize]
    [RoutePrefix("api/icaoconditions")]
    public class IcaoConditionsController : ApiBaseController
    {
        public IcaoConditionsController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger) : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
        }

        [HttpGet]
        [Route("Model")]
        public async Task<IHttpActionResult> GetModel(int id)
        {
            return await LogAction(async () => Ok(await Uow.IcaoSubjectConditionRepo.GetModelAsync(id)), "api/icaoconditions/Model");
        }

        [HttpGet]
        [Route("Filter")]
        public async Task<IHttpActionResult> Filter( int subId, bool cancelledOnly )
        {
            return await LogAction(async () =>
            {
                var culture = ApplicationContext.GetCookie("_culture", "en");

                var conditions = cancelledOnly
                        ? await Uow.IcaoSubjectConditionRepo.GetCancelledActiveOnlyAsync(subId, culture)
                        : await Uow.IcaoSubjectConditionRepo.GetBySubjectIdActiveAsync(subId, culture);

                return Ok(conditions);

            }, "api/icaoconditions/filter");
        }

        [HttpPost, Route("page", Name = "RegisteredIcaoSubjectConditionsPage")]
        [Authorize(Roles = "NOF_ADMINISTRATOR, Administrator")]
        public async Task<IHttpActionResult> GetIcaoSubjectConditionsInPage(int subId, [FromBody]QueryOptions queryOptions)
        {
            return await LogAction(async () =>
            {
                if (queryOptions.PageSize > MaxRecordsToReturn)
                    queryOptions.PageSize = MaxRecordsToReturn;

                var totalCount = await Uow.IcaoSubjectConditionRepo.GetAllCountAsync(subId);
                var icaoSubjectConditions = await Uow.IcaoSubjectConditionRepo.GetAllAsync(subId, queryOptions);
                if (icaoSubjectConditions.Count == 0 && queryOptions.Page > 1)
                {
                    queryOptions.Page = queryOptions.Page - 1;
                    icaoSubjectConditions = await Uow.IcaoSubjectConditionRepo.GetAllAsync(subId, queryOptions);
                }

                return Ok(ResponsewithPagingEnvelope(queryOptions, "RegisteredIcaoSubjectConditionsPage", icaoSubjectConditions, totalCount));

            }, "api/icaoconditions/page");
        }

        [HttpGet]
        [Route("Codes")]
        public async Task<IHttpActionResult> GetCodes()
        {
            return await LogAction(async () => Ok(await Uow.IcaoSubjectConditionRepo.GetAllDistinctCodesAsync()), 
                "api/icaoconditions/codes");
        }

        [HttpPost, Route("create")]
        [Authorize(Roles = "NOF_ADMINISTRATOR, Administrator")]
        public async Task<IHttpActionResult> Create([FromBody]IcaoConditionViewModel vm)
        {
            return await LogAction(async () =>
            {
                if (!ModelState.IsValid)
                    return BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid);

                var icaoCondition = await Uow.IcaoSubjectConditionRepo.GetByIdAsync(vm.Id);
                if (icaoCondition == null)
                {
                    icaoCondition = new IcaoSubjectCondition
                    {
                        SubjectId = vm.SubjectId,
                        Active = vm.Active,
                        AllowFgEntry = vm.requiresItemFG,
                        AllowPurposeChange = vm.RequiresPurpose,
                        CancelNotamOnly = vm.CancellationOnly,
                        Description = vm.Description,
                        DescriptionFrench = vm.DescriptionFrench,
                        B = vm.B,
                        M = vm.M,
                        O = vm.O,
                        N = vm.N,
                        I = vm.I,
                        V = vm.V,
                        Lower = vm.Lower,
                        Upper = vm.Upper,
                        Radius = vm.Radius,
                        Code = vm.Code
                    };

                    Uow.IcaoSubjectConditionRepo.Add(icaoCondition);

                    await Uow.SaveChangesAsync();

                    Logger.LogInfo(GetType(), $"NEW: {SummarizeCondition(icaoCondition)} created  by '{ApplicationContext.GetUserName()}'.");
                }

                return Ok(vm);

            }, "api/icaoconditions/create");
        }

        [HttpPut, Route("update")]
        [Authorize(Roles = "NOF_ADMINISTRATOR, Administrator")]
        public async Task<IHttpActionResult> Update([FromBody]IcaoConditionViewModel vm)
        {
            return await LogAction(async () =>
            {
                if (!ModelState.IsValid)
                    return BadRequest(Resources.BadRequest + " " + Resources.ModelInvalid);

                var icaoCondition = await Uow.IcaoSubjectConditionRepo.GetByIdAsync(vm.Id);
                if (icaoCondition != null)
                {
                    var beforeSummary = SummarizeCondition(icaoCondition);

                    icaoCondition.Active = vm.Active;
                    icaoCondition.AllowFgEntry = vm.requiresItemFG;
                    icaoCondition.AllowPurposeChange = vm.RequiresPurpose;
                    icaoCondition.CancelNotamOnly = vm.CancellationOnly;
                    icaoCondition.Description = vm.Description;
                    icaoCondition.DescriptionFrench = vm.DescriptionFrench;
                    icaoCondition.B = vm.B;
                    icaoCondition.M = vm.M;
                    icaoCondition.O = vm.O;
                    icaoCondition.N = vm.N;
                    icaoCondition.I = vm.I;
                    icaoCondition.V = vm.V;
                    icaoCondition.Lower = vm.Lower;
                    icaoCondition.Upper = vm.Upper;
                    icaoCondition.Radius = vm.Radius;
                    icaoCondition.Code = vm.Code;

                    Uow.IcaoSubjectConditionRepo.Update(icaoCondition);

                    await Uow.SaveChangesAsync();

                    var afterSummary = SummarizeCondition(icaoCondition);

                    Logger.LogInfo(GetType(), $"UPDATE: {beforeSummary} updated to {afterSummary}  by '{ApplicationContext.GetUserName()}'.");
                }

                return Ok(vm);

            }, "api/icaoconditions/update");
        }

        [HttpDelete, Route("delete/{id}")]
        [Authorize(Roles = "NOF_ADMINISTRATOR, Administrator")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            return await LogAction(async () =>
            {
                var icaoCondition = await Uow.IcaoSubjectConditionRepo.GetByIdAsync(id);

                if (icaoCondition == null)
                    return NotFound();
                try
                {
                    Uow.IcaoSubjectConditionRepo.Delete(icaoCondition);
                    await Uow.SaveChangesAsync();
                }
                catch 
                {
                    return BadRequest(Resources.ErrorDeletingICAOCondition);
                }

                Logger.LogInfo(GetType(), $"DELETE: {SummarizeCondition(icaoCondition)} deleted  by '{ApplicationContext.GetUserName()}'.");

                return Ok();

            }, $"api/icaoconditions/delete/{id}");
        }

        static string SummarizeCondition(IcaoSubjectCondition c)
        {
            return $"IcaoSubjectCondition(Subject: {c.Subject} Code: '{c.Code}', Desc: '{c.Description} / {c.DescriptionFrench}', I: {c.I}, V: {c.V}, N: {c.N}, B: {c.B}, O: {c.O}, M: {c.M}, Radius: {c.Radius}, CancelOnly: {c.CancelNotamOnly}, Active: {c.Active}, FG: {c.AllowFgEntry}, Purpose: {c.AllowPurposeChange})";
        }
    }
}
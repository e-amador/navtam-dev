"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NofMsgComposeComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var angular_l10n_1 = require("angular-l10n");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var toastr_service_1 = require("./../../common/toastr.service");
var windowRef_service_1 = require("../../common/windowRef.service");
var data_service_1 = require("../shared/data.service");
var data_model_1 = require("../shared/data.model");
var MessageType = /** @class */ (function () {
    function MessageType() {
    }
    MessageType.Notification = 0;
    MessageType.Error = 1;
    return MessageType;
}());
var NofMsgComposeComponent = /** @class */ (function () {
    function NofMsgComposeComponent(toastr, fb, dataService, memStorageService, router, winRef, activatedRoute, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.winRef = winRef;
        this.activatedRoute = activatedRoute;
        this.locale = locale;
        this.translation = translation;
        this.model = null;
        this.selectedPriorityCodeId = "-1";
        this.selectedTemplateId = "-1";
        this.userTemplates = [];
        this.maxNumberOfTemplates = 4;
        this.selectedSubscription = null;
        this.searchFilter = {
            clientTypeStr: "",
            client: "",
            address: ""
        };
        this.addresses = [];
        this.addressIndexSelected = -1;
        this.replyToAddress = "";
        this.replyToId = "";
    }
    NofMsgComposeComponent.prototype.ngOnInit = function () {
        this.prioriTyCodeOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('')
            },
            width: "100%"
        };
        this.setPriorityCodes();
        this.templateOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('MessageQueue.SelectTemplate')
            },
            width: "100%"
        };
        var model = this.activatedRoute.snapshot.data['model'];
        if (model) {
            this.replyToAddress = model.clientAddress;
            this.replyToId = model.id;
        }
        this.configureReactiveForm();
        this.loadTemplates();
    };
    NofMsgComposeComponent.prototype.onPriorityCodeChanged = function (data) {
        this.selectedPriorityCodeId = data.value;
    };
    NofMsgComposeComponent.prototype.onTemplateChanged = function (data) {
        var _this = this;
        if (data.value === "-1") {
            return;
        }
        this.selectedTemplateId = data.value;
        this.dataService.getMessageTemplate(this.selectedTemplateId)
            .subscribe(function (template) {
            var bodyCtrl = _this.msgForm.get('messageBody');
            var recipientsCtrl = _this.msgForm.get('recipients');
            bodyCtrl.setValue(template.body);
            recipientsCtrl.setValue(template.addresses);
            bodyCtrl.markAsDirty();
            recipientsCtrl.markAsDirty();
        }, function (error) {
            var msg = _this.translation.translate('MessageQueue.FailureTemplateLoad');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    NofMsgComposeComponent.prototype.backToMsgList = function () {
        var _this = this;
        if (this.replyToId) {
            this.dataService.unlockMessage(this.replyToId)
                .subscribe(function (result) {
                _this.router.navigate(['/dashboard/']);
            });
        }
        else {
            this.router.navigate(['/dashboard/']);
        }
    };
    NofMsgComposeComponent.prototype.isOriginatorRequired = function () {
        var fc = this.msgForm.get('originator');
        return !this.msgForm.pristine && fc.errors && fc.errors.required;
    };
    NofMsgComposeComponent.prototype.isOriginatorFormatInvalid = function () {
        var fc = this.msgForm.get('originator');
        return !this.msgForm.pristine && fc.errors && !fc.errors.required && fc.errors.invalidOriginatorFormat;
    };
    NofMsgComposeComponent.prototype.validateRecipients = function () {
        var fc = this.msgForm.get('recipients');
        return fc.value || this.msgForm.pristine;
    };
    NofMsgComposeComponent.prototype.validateMessageBody = function () {
        var fc = this.msgForm.get('messageBody');
        return fc.value || this.msgForm.pristine;
    };
    NofMsgComposeComponent.prototype.openSearchDialog = function () {
        var _this = this;
        this.subscriptions = [];
        this.dataService.getNdsClientAddresses()
            .subscribe(function (subscriptions) {
            _this.selectedSubscription = null;
            for (var iter = 0; iter < subscriptions.length; iter++) {
                var subscription = subscriptions[iter];
                subscription.index = iter;
                subscription.clientTypeStr = _this.toClientTypeStr(subscription.clientType);
                subscription.isSelected = false;
                _this.subscriptions.push(subscription);
            }
            if (subscriptions.length > 0) {
                _this.selectedSubscription = _this.subscriptions[0];
                _this.selectedSubscription.isSelected = true;
            }
            ;
        }, function (error) {
            //TODO
        });
        var elm = $('#modal-search');
        var self = this;
        elm.modal({});
        elm.on('hidden.bs.modal', function (e) {
            self.closeSearchDialog();
        });
    };
    NofMsgComposeComponent.prototype.selectSubscription = function (client) {
        this.selectedSubscription.isSelected = false;
        this.selectedSubscription = client;
        this.selectedSubscription.isSelected = true;
    };
    NofMsgComposeComponent.prototype.addAddress = function () {
        if (this.addresses.length < 14) {
            this.addresses.push(this.selectedSubscription.address);
        }
    };
    NofMsgComposeComponent.prototype.selectAddress = function (index) {
        this.addressIndexSelected = this.addressIndexSelected !== index ? index : -1;
    };
    NofMsgComposeComponent.prototype.deleteAddress = function (index) {
        if (this.addressIndexSelected >= 0) {
            this.addresses.splice(this.addressIndexSelected, 1);
        }
    };
    NofMsgComposeComponent.prototype.appendAddressToRecipientList = function () {
        var ctrl = this.msgForm.get('recipients');
        var len = ctrl.value.length;
        var recipients = ctrl.value.split(";");
        var addressesCount = 0;
        for (var i = 0; i < recipients.length; i++) {
            if (recipients[i] !== "") {
                addressesCount++;
            }
        }
        var addresses = len === 0 ? "" : (ctrl.value[len - 1] === ";" ? ctrl.value : (ctrl.value + ";"));
        for (var i_1 = 0; i_1 < this.addresses.length; i_1++) {
            if (addressesCount + i_1 < 21) {
                if (addresses.length > 0 && addresses[addresses.length - 1] !== ";") {
                    addresses += ";";
                }
                addresses += this.addresses[i_1];
            }
        }
        ctrl.setValue(addresses);
        ctrl.markAsDirty();
    };
    NofMsgComposeComponent.prototype.closeSearchDialog = function () {
        this.addresses = [];
        this.addressIndexSelected = -1;
    };
    NofMsgComposeComponent.prototype.toClientTypeStr = function (clientType) {
        switch (clientType) {
            case data_model_1.ClientType.AFTN: return 'AFTN';
            case data_model_1.ClientType.SWIM: return 'SWIM';
            case data_model_1.ClientType.REST: return 'REST';
            default: return '';
        }
    };
    NofMsgComposeComponent.prototype.onSubmit = function () {
        var _this = this;
        var queueMessage = this.prepareQueueMessagePayload();
        this.dataService.sendQueueMessage(queueMessage)
            .subscribe(function (data) {
            var msg = _this.translation.translate('MessageQueue.SuccessMessageSent');
            _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
            _this.router.navigate(['/dashboard']);
        }, function (error) {
            var msg = _this.translation.translate('MessageQueue.FailureMessageSent');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    NofMsgComposeComponent.prototype.prepareQueueMessagePayload = function () {
        var now = new Date();
        var messageQueue = {
            messageType: MessageType.Notification,
            clientType: data_model_1.ClientType.AFTN,
            priorityCode: this.selectedPriorityCodeId,
            inbound: false,
            recipients: this.msgForm.get('recipients').value,
            body: this.msgForm.get('messageBody').value,
            clientName: this.winRef.appConfig.usrName,
            clientAddress: this.msgForm.get('originator').value,
            created: now,
            read: true,
            locked: false,
            replyToId: this.replyToId,
            status: data_model_1.MessageStatus.None
        };
        var lastChar = messageQueue.recipients.slice(-1);
        if (lastChar == ';') {
            messageQueue.recipients = messageQueue.recipients.slice(0, -1);
        }
        return messageQueue;
    };
    NofMsgComposeComponent.prototype.configureReactiveForm = function () {
        this.msgForm = this.fb.group({
            originator: [{ value: this.winRef.appConfig.nofAddress, disabled: false }, [forms_1.Validators.required, originatorValidator]],
            recipients: [{ value: this.replyToAddress, disabled: false }, [forms_1.Validators.required, recipientsValidator]],
            messageBody: [{ value: "", disabled: false }, [forms_1.Validators.required]],
        });
    };
    NofMsgComposeComponent.prototype.setPriorityCodes = function () {
        this.priorityCodes = [
            { id: "GG", text: "GG" },
            { id: "SS", text: "SS" },
            { id: "DD", text: "DD" },
            { id: "FF", text: "FF" },
            { id: "KK", text: "KK" }
        ];
        this.selectedPriorityCodeId = this.priorityCodes[0].id;
    };
    NofMsgComposeComponent.prototype.loadTemplates = function () {
        var _this = this;
        this.dataService.getMessageTemplateNames()
            .subscribe(function (templates) {
            _this.templates = [{ id: "-1", text: "" }];
            for (var i = 0; i < templates.length; i++) {
                var r = templates[i];
                _this.templates.push({ id: r.id, text: r.name });
            }
            _this.selectedTemplateId = "-1";
        }, function (error) {
            //TODO
        });
    };
    NofMsgComposeComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/dashboard/nof/nof-msg-compose.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], NofMsgComposeComponent);
    return NofMsgComposeComponent;
}());
exports.NofMsgComposeComponent = NofMsgComposeComponent;
function recipientsValidator(c) {
    var addresses = c.value.split(";");
    var reg = /^[a-z]+$/i;
    var isValid = true;
    for (var i = 0; i < addresses.length; i++) {
        var address = addresses[i];
        if (address.length === 0) {
            continue;
        }
        if (address.length !== 8) {
            isValid = false;
            break;
        }
        if (!reg.test(address)) {
            isValid = false;
            break;
        }
    }
    return isValid ? null : { 'invalidRecipientFormat': true };
}
function originatorValidator(c) {
    var address = c.value || "";
    var reg = /^[a-z]+$/i;
    var isValid = address.length === 8 && reg.test(address);
    return isValid ? null : { 'invalidOriginatorFormat': true };
}
//# sourceMappingURL=nof-msg-compose.component.js.map
﻿using System.ComponentModel.DataAnnotations;

namespace NAVTAM.ViewModels
{
    public class FeedbackViewModel
    {
        [Required]
        [MinLength(3)]
        public string Firstname { get; set;  }
        [Required]
        [MinLength(3)]
        public string Lastname { get; set; }
        [Required]
        [EmailAddress]
        public string EmailAddress { get; set; }
        [Required]
        public string Organization { get; set; }
        [Required]
        public string Department { get; set; }
        [Required]
        [Phone]
        public string PhoneNumber { get; set; }
        [Required]
        [MinLength(3)]
        public string Comment { get; set; }
        public bool InternalError { get; set; }
    }
}
﻿using System;

namespace NAVTAM.NsdEngine
{
    public class ProposalAnnotationViewModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public DateTime Received { get; set; }
        public string Note { get; set; }
    }
}
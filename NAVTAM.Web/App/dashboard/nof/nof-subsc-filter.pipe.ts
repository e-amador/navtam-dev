﻿import { Pipe, PipeTransform } from '@angular/core';
import { INdsClientFilter } from '../shared/data.model'

@Pipe({
    name: 'subscfilter',
    pure: false
})
export class SubscriptionFilterPipe implements PipeTransform {
    transform(items: INdsClientFilter[], filter: INdsClientFilter): INdsClientFilter[] {
        if (!items || !filter) {
            return items;
        }
        // filter items array, items which match and return true will be kept, false will be filtered out
        return items.filter((item: INdsClientFilter) => this.applyFilter(item, filter));
    }

    applyFilter(client: INdsClientFilter, filter: INdsClientFilter): boolean {
        for (let field in filter) {
            if (filter[field]) {
                if (typeof filter[field] === 'string') {
                    if (client[field].toLowerCase().indexOf(filter[field].toLowerCase()) === -1) {
                        return false;
                    }
                } else if (typeof filter[field] === 'number') {
                    if (client[field] !== filter[field]) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
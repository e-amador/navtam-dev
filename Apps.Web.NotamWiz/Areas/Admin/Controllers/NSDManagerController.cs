﻿using System;
using System.Web.Helpers;
using System.Web.Http;

namespace NavCanada.Applications.NotamWiz.Web.Areas.Admin.Controllers
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    using AutoMapper;

    using Kendo.Mvc.Extensions;
    using Kendo.Mvc.UI;

    using NavCanada.Applications.NotamWiz.Web.Controllers;
    using NavCanada.Applications.NotamWiz.Web.Models;
    using NavCanada.Core.Common.Contracts;
    using NavCanada.Core.Data.NotamWiz.Contracts;
    using NavCanada.Core.Domain.Model.Entitities;

    [Authorize(Roles = "Administrator")]
    public class NSDManagerController : BaseController
    {
        public NSDManagerController(IUow uow, IClusterQueues clusterQueues) 
            : base(uow, clusterQueues)
        {
        }

        // GET: Admin/NSDManager
        public async Task<ActionResult> Index()
        {
            var orgs = await Uow.OrganizationRepo.GetAllAsync();
            ViewData["Orgs"] = orgs.Select(o => new OrganizationListViewModel { Name = o.Name, Id = o.Id }).ToList();

            return View();
        }

        public async Task<ActionResult> GetNsdList([DataSourceRequest] DataSourceRequest request)
        {
            var nsds = await Uow.NsdRepo.GetAll().Include("LimitedToOrganizatations").ToListAsync();
            var result = Mapper.Map<List<Nsd>, List<NsdViewModel>>(nsds);

            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetNSDVersions([DataSourceRequest] DataSourceRequest request, long nsdId)
        {
            var versions = await Uow.NsdVersionRepo.GetAllAsync();
            List<NsdVersionViewModel> result = Mapper.Map<List<NsdVersion>, List<NsdVersionViewModel>>(versions.Where(v => v.NsdId == nsdId).ToList());

            return Json(result.ToDataSourceResult(request));

        }

        public async Task<ActionResult> GetSubjectsList()
        {
            var subjects = await Uow.NotamSubjectRepo.GetAllAsync();
            var results = subjects.Select(a => new { a.Id, a.EntityType, a.Description });

            return Json(results, JsonRequestBehavior.AllowGet);
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Create([DataSourceRequest] DataSourceRequest request, NsdViewModel nsdVM)
        {
            Nsd newNsd = Mapper.Map<NsdViewModel, Nsd>(nsdVM);
            Uow.NsdRepo.Add(newNsd);
            await Uow.SaveChangesAsync();

            return Json(new[] { Mapper.Map<Nsd, NsdViewModel>(newNsd) }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, NsdViewModel nsdVM, string nsdVmModels)
        {


            Nsd nsd = Uow.NsdRepo.GetById(nsdVM.Id);
            if (ModelState.IsValid)
            {
                
                if (nsd != null)
                {
                    nsd = Mapper.Map<NsdViewModel, Nsd>(nsdVM, nsd);
                    
                    Uow.NsdRepo.Update(nsd);
                    Uow.SaveChanges();
                }
            }

            return Json( Mapper.Map<Nsd, NsdViewModel>(nsd));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateList(List<NsdViewModel> nsdVmModels)
        {
            try
            {
                var nsds = nsdVmModels.Select(Mapper.Map<NsdViewModel, Nsd>).ToList();
                if (nsds.Count > 0)
                {
                    foreach (var nsdItem in nsds)
                    {
                        Uow.NsdRepo.Update(nsdItem);
                    }

                    Uow.SaveChanges();
                }
                else
                {
                    return Json(null);
                }
            }
            catch (Exception e)
            {
                return Json(new {success = false});
                throw;
            }

            return Json(new { success = true });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete([DataSourceRequest]DataSourceRequest request, NsdViewModel nsdVM)
        {
            if (ModelState.IsValid)
            {
                Nsd nsd = Uow.NsdRepo.GetById(nsdVM.Id);
                if (nsd != null)
                {
                    Uow.NsdRepo.Delete(nsd);
                    Uow.SaveChanges();

                    return Json(Mapper.Map<Nsd, NsdViewModel>(nsd));
                }
            }
            // Return the removed product. Also return any validation errors.
            //return Json(new[] { Mapper.Map<NSD, NSDVM>(NSD, opt => { opt.Items["db"] = db; }) }.ToDataSourceResult(request, ModelState));
            return null;
        }
    }
}
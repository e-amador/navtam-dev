﻿namespace RSC.Model
{
    /// <summary>
    /// Data Elements related to the Cleared Area of the Runway.
    /// </summary>
    public class ClearedSurfaceThird
    {
        /// <summary>
        /// Simple element indicating which portion of the runway that the next higher level
        /// structure refers to: 1 refers to third closest to lowest runway designator. 2 refers to middle portion. 
        /// 3 refers to runway third closest to highest runway designator.
        /// If using average, use 1.
        /// </summary>
        public int Portion { get; set; }
        /// <summary>
        /// Contaminant descriptions for runway.
        /// </summary>
        public ContaminantIns[] Contaminants { get; set; }
        /// <summary>
        /// Complex Structure describing the Runway Friction.
        /// </summary>
        public RwyFriction RwyFrictionThird { get; set; }
        /// <summary>
        /// Structure describing the Runway Condition Code.
        /// </summary>
        public string RwyCC { get; set; }

        public string NESCalculatedRwyCC { get; set; }
    }
}
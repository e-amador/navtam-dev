﻿using Business.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NavCanada.Core.Domain.Model.Entitities;
using NDS.Common.Xml;
using System;

namespace NAVTAM.Web.Test.NotamXml
{
    [TestClass]
    public class NotamXmlTests
    {
        [TestMethod]
        public void NotamXmlTests_validate_NOF()
        {
            var notam = GetValidNotam();

            notam.Nof = "CYHQ";
            Assert.IsNull(ValidateNotamXml(notam), "valid when there are 4 letters");

            notam.Nof = null;
            Assert.IsNotNull(ValidateNotamXml(notam), "NOF is required");

            notam.Nof = "A";
            Assert.IsNotNull(ValidateNotamXml(notam), "NOF is too short");

            notam.Nof = "AAAAA";
            Assert.IsNotNull(ValidateNotamXml(notam), "NOF is too long");

            notam.Nof = "AA12";
            Assert.IsNotNull(ValidateNotamXml(notam), "NOF should be only letters");
        }

        [TestMethod]
        public void NotamXmlTests_validate_Series()
        {
            var notam = GetValidNotam();

            notam.Series = "C";
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a one-letter series");

            notam.Series = null;
            Assert.IsNotNull(ValidateNotamXml(notam), "Must fail validation on missing series");

            notam.Series = "AA";
            Assert.IsNotNull(ValidateNotamXml(notam), "Must fail validation on more-than-one letter series");

            notam.Series = "1";
            Assert.IsNotNull(ValidateNotamXml(notam), "Must fail validation on none letter series");
        }

        [TestMethod]
        public void NotamXmlTests_validate_notam_number()
        {
            var notam = GetValidNotam();

            notam.Number = 1;
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid number (1-9999).");

            notam.Number = -1;
            Assert.IsNotNull(ValidateNotamXml(notam), "Must fail validation on negative number.");

            notam.Number = 10000;
            Assert.IsNotNull(ValidateNotamXml(notam), "Must fail validation on a larger than 9999 number.");
        }

        [TestMethod]
        public void NotamXmlTests_validate_notam_year()
        {
            var notam = GetValidNotam();

            notam.Year = 2019;
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid number (1-9999).");
        }

        [TestMethod]
        public void NotamXmlTests_validate_notam_type()
        {
            var notam = GetValidNotam();

            notam.Type = NavCanada.Core.Domain.Model.Enums.NotamType.N;
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid type.");

            notam.ReferredNumber = 1;
            notam.ReferredSeries = notam.Series;
            notam.ReferredYear = notam.Year;

            notam.Type = NavCanada.Core.Domain.Model.Enums.NotamType.R;
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid type.");

            notam.Type = NavCanada.Core.Domain.Model.Enums.NotamType.C;
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid type.");
        }

        [TestMethod]
        public void NotamXmlTests_validate_notam_FIR()
        {
            var notam = GetValidNotam();

            notam.Fir = "CZUL";
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid FIR.");

            notam.Fir = "czul";
            Assert.IsNotNull(ValidateNotamXml(notam), "Must not pass validation on a invalid FIR.");

            notam.Fir = "ZUL";
            Assert.IsNotNull(ValidateNotamXml(notam), "Must not pass validation on a invalid FIR.");
        }

        [TestMethod]
        public void NotamXmlTests_validate_notam_Code23()
        {
            var notam = GetValidNotam();

            notam.Code23 = "OB";
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid Code23.");

            notam.Code23 = "O1";
            Assert.IsNotNull(ValidateNotamXml(notam), "Must not pass validation on a invalid Code23.");

            notam.Code23 = "AAA";
            Assert.IsNotNull(ValidateNotamXml(notam), "Must not pass validation on a invalid Code23.");
        }

        [TestMethod]
        public void NotamXmlTests_validate_notam_Code45()
        {
            var notam = GetValidNotam();

            notam.Code23 = "CN";
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid Code45.");

            notam.Code23 = "O1";
            Assert.IsNotNull(ValidateNotamXml(notam), "Must not pass validation on a invalid Code45.");

            notam.Code23 = "AAA";
            Assert.IsNotNull(ValidateNotamXml(notam), "Must not pass validation on a invalid Code45.");
        }

        [TestMethod]
        public void NotamXmlTests_validate_notam_Traffic()
        {
            var notam = GetValidNotam();

            notam.Traffic = "I";
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid Traffic.");

            notam.Traffic = "V";
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid Traffic.");

            notam.Traffic = "IV";
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid Traffic.");

            notam.Traffic = "K";
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid Traffic.");
        }

        [TestMethod]
        public void NotamXmlTests_validate_notam_Purpose()
        {
            var notam = GetValidNotam();

            notam.Purpose = "NB";
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid Purpose.");

            notam.Purpose = "BO";
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid Purpose.");

            notam.Purpose = "M";
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid Purpose.");

            notam.Purpose = "K";
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid Purpose.");

            notam.Purpose = "NBO";
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid Purpose.");

            notam.Purpose = "B";
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid Purpose.");
        }

        [TestMethod]
        public void NotamXmlTests_validate_notam_Scope()
        {
            var notam = GetValidNotam();

            notam.Scope = "A";
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid Scope.");

            notam.Scope = "E";
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid Scope.");

            notam.Scope = "W";
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid Scope.");

            notam.Scope = "AE";
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid Scope.");

            notam.Scope = "AW";
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid Scope.");

            notam.Scope = "K";
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid Scope.");
        }

        [TestMethod]
        public void NotamXmlTests_validate_notam_LowerLimit()
        {
            var notam = GetValidNotam();

            notam.LowerLimit = 10;
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid LowerLimit.");

            notam.LowerLimit = -10;
            Assert.IsNotNull(ValidateNotamXml(notam), "Must not pass validation on an invalid LowerLimit.");

            notam.LowerLimit = 10000;
            Assert.IsNotNull(ValidateNotamXml(notam), "Must not pass validation on an invalid LowerLimit.");
        }

        [TestMethod]
        public void NotamXmlTests_validate_notam_UpperLimit()
        {
            var notam = GetValidNotam();

            notam.UpperLimit = 100;
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid UpperLimit.");

            notam.UpperLimit = -100;
            Assert.IsNotNull(ValidateNotamXml(notam), "Must pass validation on a valid UpperLimit.");

            notam.UpperLimit = 10000;
            Assert.IsNotNull(ValidateNotamXml(notam), "Must pass validation on a valid UpperLimit.");
        }

        [TestMethod]
        public void NotamXmlTests_validate_notam_Coordinates()
        {
            var notam = GetValidNotam();

            notam.Coordinates = "491140N 1231240W";
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid Coordinates.");

            notam.Coordinates = null;
            Assert.IsNotNull(ValidateNotamXml(notam), "Must pass validation on a valid Coordinates.");

            notam.Coordinates = "491140N1231240W";
            Assert.IsNotNull(ValidateNotamXml(notam), "Must pass validation on a valid Coordinates.");

            notam.Coordinates = "1231240W 491140N";
            Assert.IsNotNull(ValidateNotamXml(notam), "Must pass validation on a valid Coordinates.");
        }

        [TestMethod]
        public void NotamXmlTests_validate_notam_Radius()
        {
            var notam = GetValidNotam();

            notam.Radius = 5;
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid Radius.");

            notam.Radius = -5;
            Assert.IsNotNull(ValidateNotamXml(notam), "Must not pass validation on an invalid Radius.");

            notam.Radius = 10000;
            Assert.IsNotNull(ValidateNotamXml(notam), "Must not pass validation on an invalid Radius.");
        }

        [TestMethod]
        public void NotamXmlTests_validate_notam_ItemA()
        {
            var notam = GetValidNotam();

            notam.ItemA = "CYUL";
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid ItemA.");

            notam.ItemA = "";
            Assert.IsNotNull(ValidateNotamXml(notam), "Must not pass validation on an invalid ItemA.");
        }

        [TestMethod]
        public void NotamXmlTests_validate_notam_StartValidity()
        {
            var notam = GetValidNotam();

            notam.StartActivity = new DateTime(2019, 2, 20);
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid StartValidity.");

            notam.StartActivity = null;
            Assert.IsNotNull(ValidateNotamXml(notam), "Must not pass validation on an invalid StartValidity.");
        }

        [TestMethod]
        public void NotamXmlTests_validate_notam_EndValidity()
        {
            var notam = GetValidNotam();

            notam.EndValidity = new DateTime(2019, 2, 28);
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid EndValidity.");

            notam.Permanent = true;
            notam.EndValidity = null;
            Assert.IsNull(ValidateNotamXml(notam), "Must not pass validation on an invalid EndValidity.");

            notam.EndValidity = new DateTime(2019, 2, 28);
            notam.Permanent = false;
            notam.Estimated = true;
            Assert.IsNull(ValidateNotamXml(notam), "Must pass validation on a valid EndValidity.");
        }

        [TestMethod]
        public void NotamXmlTests_valid_notam()
        {
            var notam = GetValidNotam();
            var validation = ValidateNotamXml(notam);
            Assert.IsNull(validation, validation);
        }

        [TestMethod]
        public void NotamXmlTests_notam_invalid()
        {
            var notamXml = "<nes:NOTAM Version=\"1\" xmlns:nes=\"http://www.navcanada.ca/NES/CommonTypes\"><NOF>CYHQ</NOF><Series>A</Series><Number>11</Number><Year>18</Year><Type>N</Type><QLine><FIR>CZUL</FIR><Code23>OB</Code23><Code45>CN</Code45><Traffic>IV</Traffic><Purpose>BO</Purpose><Scope>A</Scope><Lower>000</Lower><Upper>999</Upper><Coordinates>4912N12313W</Coordinates><Radius>000</Radius></QLine><FullCoordinates>491140N1231240W</FullCoordinates><ItemA>CYOW</ItemA><StartValidity>1803060000</StartValidity><EndValidity>1803310000</EndValidity><ItemE>RWY 18/0W CLOSED DUE GEESE</ItemE><ItemEFrench>RWY 18/0W FERME AVEC LE CANUCK</ItemEFrench></nes:NOTAM>";

            var validation = XmlSerializerExtensions.ValidateNotamXml(notamXml);

            Assert.IsNull(validation, validation);
        }

        static string ValidateNotamXml(Notam notam) => XmlSerializerExtensions.ValidateNotamXml(notam.ToXml());

        static Notam GetValidNotam(string itemE = "RWY 18/0W CLOSED DUE GEESE", string itemEF = "RWY 18/0W FERME AVEC LE CANUCK")
        {
            return new Notam()
            {
                Nof = "CYHQ",
                Series = "A",
                ItemA = "CYOW",
                Fir = "CZUL",
                Coordinates = "491140N 1231240W",
                Code23 = "OB",
                Code45 = "CN",
                Traffic = "IV",
                Purpose = "BO",
                Number = 11,
                Year = 2018,
                Scope = "A",
                Type = NavCanada.Core.Domain.Model.Enums.NotamType.N,
                LowerLimit = 0,
                UpperLimit = 999,
                StartActivity = new DateTime(2018, 03, 06),
                EndValidity = new DateTime(2018, 03, 31),
                ItemE = itemE,
                ItemEFrench = itemEF,
                NotamId = ICAOTextUtils.FormatNotamId("A", 11, 2018)
            };
        }
    }
}

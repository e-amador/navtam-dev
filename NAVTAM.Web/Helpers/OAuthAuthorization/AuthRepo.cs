﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using NavCanada.Core.Data.NotamWiz.EF;
using NavCanada.Core.Domain.Model.Entitities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NAVTAM.Helpers.OAuthAuthorization
{
    public class AuthRepository : IDisposable
    {
        private AppDbContext _ctx;
        private UserManager<UserProfile> _userManager;

        public AuthRepository()
        {
            _ctx = new AppDbContext();
            _userManager = new UserManager<UserProfile>(new UserStore<UserProfile>(_ctx));
        }

        public async Task<UserProfile> FindUser(string userName, string password)
        {
            UserProfile user = await _userManager.FindAsync(userName, password);
            return user;
        }

        public async Task<IList<string>> FindRoles(string userId)
        {
            return await _userManager.GetRolesAsync(userId);
        }

        public async Task UpdateAccessToken(string userId, string accessToken )
        {
            UserProfile user = await _userManager.FindByIdAsync(userId);
            if( user != null )
            {
                user.AccessToken = accessToken;
                await _userManager.UpdateAsync(user);
            }

            throw new NullReferenceException("User not found");
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                    _ctx.Dispose();
                    _userManager.Dispose();
                }

                // free unmanaged resources (unmanaged objects) and override a finalizer below.
                // set large fields to null.

                disposedValue = true;
            }
        }

        // override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~AuthRepository() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
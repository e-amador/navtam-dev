﻿import { Component, Inject, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms'
import { WindowRef } from '../../common/windowRef.service'
import { Select2OptionData } from 'ng2-select2';
import { LocaleService, TranslationService } from 'angular-l10n';
import { MemoryStorageService } from '../shared/mem-storage.service'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';
import { PhoneValidator } from '../shared/phone.validator'
import { DataService } from '../shared/data.service'
import { IOrganization, IDoaPartial, INsdPartial } from '../shared/data.model';

@Component({
    templateUrl: '/app/admin/org/org-edit.component.html'
})
export class OrgEditComponent implements OnInit {
    model: any = null;
    action: string;

    orgForm: FormGroup;

    isReadOnly: boolean;

    public isOrgNameUnique: boolean = true;
    public isValidatingOrg: boolean = false;
    public isSubmitting: boolean = false;

    public orgTypeOptions: Select2Options;
    public orgTypes: Array<Select2OptionData>;
    public orgType?: number;

    public doaOptions: Select2Options;
    public selectedDoaIds: any[];
    public doas: Array<Select2OptionData>;

    public nsdOptions: Select2Options;
    public selectedNsdIds?: number[];
    public nsds: Array<Select2OptionData>;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private activatedRoute: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService) {

        if (this.winRef.appConfig.nofAdmin)
            this.router.navigate(['/administration']);
    }

    ngOnInit() {
        this.doaOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('OrgSession.SelectDoa')
            },
            multiple: true,
            width: "100%"
        }

        this.nsdOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('OrgSession.SelectNsds')
            },
            multiple: true,
            width: "100%"
        };

        this.orgTypeOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('OrgSession.SelectDoa')
            },
            multiple: false,
            width: "100%"
        };

        this.orgTypes = [
            {
                id: '0',
                text: this.translation.translate('OrgSession.OrgTypeExternal')
            },
            {
                id: '1',
                text: this.translation.translate('OrgSession.OrgTypeFic')
            },
            {
                id: '2',
                text: this.translation.translate('OrgSession.OrgTypeNof')
            },
            {
                id: '3',
                text: this.translation.translate('OrgSession.OrgTypeAdministration')
            }
        ];

        this.action = this.activatedRoute.snapshot.data[0].action;
        this.model = this.activatedRoute.snapshot.data['model'];

        this.orgType = this.model.type;
        this.model.telephone = PhoneValidator.formatTenDigits(this.model.telephone);

        this.isReadOnly = this.action === "view";

        this.configureReactiveForm();

        this.loadDoas();
        this.loadNsds();
    }

    onDoaChanged(data: { value: string[] }) {
        if (data.value && data.value.length > 0) {
            this.$ctrl("doas").setValue(data.value.join(','));
        } else {
            this.$ctrl("doas").setValue("");
        }

        if (this.orgForm.pristine && data.value && !this.matchIds(data.value, this.model.doaIds)) {
            this.orgForm.markAsDirty();
        }
    }

    onNsdChanged(data: { value: string[] }) {
        if (data.value && data.value.length > 0) {
            this.orgForm.get('nsds').setValue(data.value.join(','));
        } else {
            this.orgForm.get('nsds').setValue("");
        }

        if (this.orgForm.pristine && data.value && !this.matchIds(data.value, this.model.nsdIds)) {
            this.orgForm.markAsDirty();
        }
    }

    onOrgTypeChanged(data: { value: string }) {
        if (data.value && data.value.length > 0) {
            this.$ctrl("orgType").setValue(data.value);
        } else {
            this.$ctrl("orgType").setValue("");
        }

        if (this.orgForm.pristine && data.value && data.value !== this.model.type) {
            this.orgForm.markAsDirty();
        }
    }

    getAction(): string {
        return this.action;
    }

    backToOrgList() {
        this.router.navigate(['/administration/orgs']);
    }

    onSubmit() {
        if (this.isSubmitting) return;
        this.isSubmitting = true;
        const org = this.prepareUpdateOrg();
        this.dataService.updateOrg(org)
            .subscribe(data => {
                let msg = this.translation.translate('OrgSession.SuccessOrgUpdated');
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                this.router.navigate(["/administration/orgs"]);
            }, error => {
                this.isSubmitting = false;
                let msg = this.translation.translate('OrgSession.FailureOrgUpdated');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    validateName() {
        const usernameCtrl = this.orgForm.get("name");
        return usernameCtrl.valid || this.orgForm.pristine;
    }

    validateEmailAddress() {
        const emailCtrl = this.orgForm.get("emailAddress");
        return emailCtrl.valid || this.orgForm.pristine;
    }

    validateAddress() {
        const emailCtrl = this.orgForm.get("address");
        return emailCtrl.valid || this.orgForm.pristine;
    }

    validateTelephone() {
        const emailCtrl = this.orgForm.get("telephone");
        return emailCtrl.valid || this.orgForm.pristine;
    }

    validateTypeOfOperation() {
        const typeOfOperationCtrl = this.orgForm.get("typeOfOperation");
        return typeOfOperationCtrl.valid || this.orgForm.pristine;
    }

    validateDoas() {
        const fc = this.orgForm.get('doas');
        return fc.value !== -1 || this.orgForm.pristine;
    }

    validateNsds() {
        const fc = this.orgForm.get('nsds');
        return fc.value || this.orgForm.pristine;
    }

    disableSave() {
        return this.orgForm.pristine || !this.orgForm.valid || !this.isOrgNameUnique || this.isSubmitting;
    }

    private loadDoas(): void {
        this.dataService.getDoas()
            .subscribe((doa: IDoaPartial[]) => {
                this.doas = doa.map(
                    function (r: any) {
                        return <Select2OptionData>{
                            id: r.id,
                            text: r.name
                        };
                    });
                this.selectedDoaIds = this.model.doaIds || -1;
            });
    }

    private loadNsds(): void {
        this.dataService.getNsdLeaves()
            .subscribe((nsds: INsdPartial[]) => {
                this.nsds = nsds.map(
                    function (r: any) {
                        return <Select2OptionData>{
                            id: r.id,
                            text: r.name
                        };
                    });
                this.selectedNsdIds = this.model.nsdIds || -1;
            });
    }

    private validateOrgNameUnique() {
        if (this.isValidatingOrg) return;
        if (this.validateName()) {
            const orgName = this.$ctrl('name');
            if (orgName.value) {
                if (orgName.value.length >= 3) {
                    this.isValidatingOrg = true;
                    this.dataService.isOrgNameUnique(this.model.id, orgName.value)
                        .finally(() => {
                            this.isValidatingOrg = false;
                        })
                        .subscribe((resp: boolean) => {
                            this.isOrgNameUnique = resp;
                        });
                }
            }
        }
    }

    private configureReactiveForm() {
        this.orgForm = this.fb.group({
            name: [{ value: this.model.name, disabled: this.isReadOnly }, [Validators.required, Validators.minLength(3), Validators.maxLength(100), Validators.pattern('[a-zA-Z].*')]],
            address: [{ value: this.model.address, disabled: this.isReadOnly }, [Validators.required]],
            emailAddress: [{ value: this.model.emailAddress, disabled: this.isReadOnly }, [Validators.required, Validators.pattern("[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}")]],
            headOffice: [{ value: this.model.headOffice, disabled: this.isReadOnly }, []],
            typeOfOperation: [{ value: this.model.typeOfOperation, disabled: this.isReadOnly }, [Validators.required]],
            emailDistribution: [{ value: this.model.emailDistribution, disabled: this.isReadOnly }, []],
            doas: [{ value: this.model.assignedDoaIds || -1, disabled: false }, [doasValidator]],
            nsds: [{ value: this.model.assignedNsdIds || -1, disabled: false }, [nsdsValidator]],
            telephone: [{ value: this.model.telephone, disabled: this.isReadOnly }, [phoneNumberValidator]],
            orgType: { value: this.model.type, disabled: this.isReadOnly },
        });

        this.$ctrl("name").valueChanges.debounceTime(1000).subscribe(() => this.validateOrgNameUnique());
    }

    private $ctrl(name: string): AbstractControl {
        return this.orgForm.get(name);
    }

    phoneNumberFocus() {
        const phoneCtrl = this.$ctrl("telephone");
        const value = PhoneValidator.stripFormat(phoneCtrl.value);
        phoneCtrl.setValue(value);
    }

    phoneNumberBlur() {
        const phoneCtrl = this.$ctrl("telephone");
        const value = PhoneValidator.formatTenDigits(phoneCtrl.value);
        phoneCtrl.setValue(value);
    }

    private prepareUpdateOrg(): IOrganization {
        const org: IOrganization = {
            id: this.model.id,
            name: this.orgForm.get('name').value,
            address: this.orgForm.get('address').value,
            emailAddress: this.orgForm.get('emailAddress').value,
            emailDistribution: this.orgForm.get('emailDistribution').value,
            headOffice: this.orgForm.get('headOffice').value,
            telephone: this.orgForm.get('telephone').value,
            typeOfOperation: this.orgForm.get('typeOfOperation').value,
            assignedDoaIds: this.parseIds(this.orgForm.get('doas').value),
            assignedNsdIds: this.parseIds(this.orgForm.get('nsds').value),
            type: this.parseOrgType(this.orgForm.get('orgType').value),
        }
        return org;
    }

    private parseOrgType(value: string): number {
        return +(value || 0);
    }

    private parseIds(value: string): number[] {
        return (value || "").split(",").map(v => +v);
    }

    private matchIds(newValues: string[], oldValues: string[]) {
        const newJoined = newValues ? newValues.join(' ') : "";
        const oldJoined = oldValues ? oldValues.join(' ') : "";
        return newJoined === oldJoined;
    }
}

function doasValidator(c: AbstractControl): { [key: string]: boolean } | null {
    if (!c.value) {
        return { 'required': true }
    }
    return null;
}

function nsdsValidator(c: AbstractControl): { [key: string]: boolean } | null {
    if (!c.value) {
        return { 'required': true }
    }
    return null;
}

function phoneNumberValidator(c: AbstractControl): { [key: string]: boolean } | null {
    const value = c.value;
    if (!value)
        return { 'required': true }

    if (!PhoneValidator.isValidPhoneNumber(c.value, 10)) {
        return { 'pattern': true }
    }

    return null;
}
﻿using System;

namespace NDS.Relay
{
    public static class RelayValues
    {
        public static string XML_NOTAM => nameof(XML_NOTAM);
        public static string AFTN_NOTAM => nameof(AFTN_NOTAM);
        public static string AFTN_CONFIRMATION => nameof(AFTN_CONFIRMATION);
        public static string AFTN_ERROR => nameof(AFTN_ERROR);
        public static string AFTN_MESSAGE => nameof(AFTN_MESSAGE);
    }
}

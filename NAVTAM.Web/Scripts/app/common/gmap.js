﻿var app = app || {};
app.gmap = (function () {

    var map;
    var infoWindow;

    var initMap = function () {
        map = new google.maps.Map(document.getElementById("map_div"), {
            center: new google.maps.LatLng(45.4215, -75.6973),
            zoom: 13,
            maxZoom: 19,
            mapTypeId: google.maps.MapTypeId.TERRAIN
        });

        infowindow = new google.maps.InfoWindow();

        //Markers
        google.maps.Map.prototype.markers = new Array();
        google.maps.Map.prototype.markersVisible = false;
        google.maps.Map.prototype.addMarker = function (marker) {
            this.markers[this.markers.length] = marker;
            return;
        };
        google.maps.Map.prototype.getMarkers = function () {
            return this.markers
        };
        google.maps.Map.prototype.clearMarkers = function () {
            this.markers.forEach(function (marker, i) {
                marker.setMap(null);
            });
            markers = [];
            return;
        };
        google.maps.Map.prototype.hideMarkers = function () {
            this.getMarkers().forEach(function (marker, i) {
                marker.setMap(null);
            });
            map.markersVisible = false;
            return;
        };
        google.maps.Map.prototype.showMarkers = function () {
            this.getMarkers().forEach(function (marker, i) {
                marker.setMap(map);
            });
            map.markersVisible = true;
            return;
        };

        //Poligons
        google.maps.Map.prototype.polygons = new Array();
        google.maps.Map.prototype.polygonsVisible = true;
        google.maps.Map.prototype.addPolygon = function (polygon) {
            this.polygons[this.polygons.length] = polygon;
            return;
        };
        google.maps.Map.prototype.getPolygons = function () {
            return this.polygons;
        };
        google.maps.Map.prototype.clearPolygons = function () {
            for (var i = 0; i < this.polygons.length; i++) {
                this.polygons[i].setMap(null);
            }
            this.polygons = [];
            return;
        };
        google.maps.Map.prototype.clearPolygonsExcludingDoa = function () {
            for (var i = 0; i < this.polygons.length; i++) {
                if (this.polygons[i].name !== 'DOA' ) {
                    this.polygons[i].setMap(null);
                }
            }

            if (this.polygons.length === 0) {
                this.polygons = [];
            }
            return;
        };
        google.maps.Map.prototype.hidePolygons = function () {
            this.getPolygons().forEach(function (polygon, i) {
                polygon.setMap(null);
            });
            map.polygonsVisible = false;
            return;
        };
        google.maps.Map.prototype.showPolygons = function () {
            this.getPolygons().forEach(function (polygon, i) {
                polygon.setMap(map);
            });
            map.polygonsVisible = true;
            return;
        };
        google.maps.Polygon.prototype.getBounds = function () {
            var bounds = new google.maps.LatLngBounds();
            this.getPath().forEach(function (coord, i) {
                bounds.extend(coord);
            });
            return bounds;
        }
        google.maps.Polygon.prototype.getCenter = function () {
            return this.getBounds().getCenter();
        }
        google.maps.Map.prototype.FitBounds = function () {
            var bounds = new google.maps.LatLngBounds();

            // iterate polygons
            this.getPolygons().forEach(function (polygon, i) {
                // iterate paths
                polygon.getPath().forEach(function (coord, i) {
                    // extend bounds
                    bounds.extend(coord);
                });
            })
            // fit map bounds
            this.fitBounds(bounds);
        }
    }

    var resize = function () {
        if (!map) {
            return;
        }
        google.maps.event.trigger(window, 'resize', {});
    }
    var findCenter = function () {
        map.clearMarkers();

        map.getPolygons().forEach(function (polygon, i) {
            var marker = new google.maps.Marker({
                position: polygon.getCenter(),
                map: map
            });

            // add marker to collection
            map.addMarker(marker);
        });

        // toggle marker visibility
        map.markersVisible = true;
    }
    var fitBounds = function () {
        map.FitBounds();
    }
    var hideMarkers = function () {
        if (map.markersVisible) {
            map.hideMarkers();
        } else {
            map.showMarkers();
        }
    }
    var hidePolygons = function () {
        if (map.polygonsVisible) {
            map.hidePolygons();
        } else {
            map.showPolygons();
        }
    }
    var clearPolygons = function () {
        map.clearPolygons();
    }
    var clearPolygonsExcludingDoa = function () {
        map.clearPolygonsExcludingDoa();
    }

    var changeMarkerLocation = function (geoString) {
        debugger;
        if (geoString && geoString.features) {
            var coordinates = geoString.features[0].geometry.coordinates;
            var markers = map.getMarkers();
            var locationChanged = false;
            for (var i = 0; i < markers.length; i++) {
                var position = markers[0].getPosition();
                if (position) {
                    var lat = position.lat();
                    var lng = position.lng();

                    if (lat.toFixed(4) === coordinates[1].toFixed(4) && lng.toFixed(4) === coordinates[0].toFixed(4)) {
                        locationChanged = true;
                    }
                }
            }
        }
        if (locationChanged) {
            map.clearMarkers();
            addGeoJsonFeature("LOCATION", geoString, false);
        }
    }

    var addGeoJsonFeature = function (name, geoString, zoomto) {

        if (zoomto === undefined) { zoomto = true; }

        var geojson = JSON.parse(geoString);
        var geoResult = new geometryConverter(geojson, {
                    name: name,
                    strokeColor: "#00FF00",
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillOpacity: name === 'DOA' ? 0.10 : 0.35,
                    fillColor: "#FF0000",
                    map: map
        });

        if (geoResult.error) {
            // Handle the error.
        } else {
            if (geoResult.length) {
                addPolygonsToMap(geoResult)
            } else {
                addPolygonToMap(geoResult)
            }
        }
    }

    var addPolygonToMap = function (polygon) {
        if (polygon.isPoint) {
            map.addMarker(polygon);
        } else {
            map.addPolygon(polygon);
        }

        if (polygon.name !== 'DOA') {
            google.maps.event.addListener(polygon, 'mouseover', function (event) {
                this.setOptions({
                    strokeColor: "#ff0000",
                    fillColor: "#00ff00"
                });
            });

            // polygon exit
            google.maps.event.addListener(polygon, 'mouseout', function (event) {
                this.setOptions({
                    strokeColor: "#00ff00",
                    fillColor: "#ff0000"
                });
            });
        }

        // polygon click
        google.maps.event.addListener(polygon, 'click', function (event) {

            if (!this['getBounds'])
                return;

            map.fitBounds(this.getBounds());

            // remove previous info windows
            if (infoWindow) {
                infoWindow.setMap(null);
            }

            infoWindow = new google.maps.InfoWindow({
                position: this.getCenter(),
                map: map,
                content: polygon.name
                //content: latitude.toFixed(4) + ', ' + longitude.toFixed(4)
            });
        });

        if (polygon['getBounds']) {
            map.fitBounds(polygon.getBounds());
        }
    }

    var addMarker = function (marker) {
        map.addMarker(marker);
        map.markersVisible = true;
    }

    var addPolygonsToMap = function (polygons) {
        for (var i = 0; i < polygons.length; i++) {
            if (polygons[i].length) {
                for (var j = 0; j < polygons[i].length; j++)
                    addPolygonToMap(polygons[i][j]);
            }
            else {
                addPolygonToMap(polygons[i]);
            }
        }
    }

    var geometryConverter = function (geojson, options) {

        var _geometryToGoogleMaps = function (geojsonGeometry, opts, geojsonProperties) {

            var googleObj;

            switch (geojsonGeometry.type) {
                case "Point":
                    opts.position = new google.maps.LatLng(geojsonGeometry.coordinates[1], geojsonGeometry.coordinates[0]);
                    googleObj = new google.maps.Marker(opts);
                    googleObj.isPoint = true;
                    if (geojsonProperties) {
                        googleObj.set("geojsonProperties", geojsonProperties);
                    }
                    break;

                case "MultiPoint":
                    googleObj = [];
                    for (var i = 0; i < geojsonGeometry.coordinates.length; i++) {
                        opts.position = new google.maps.LatLng(geojsonGeometry.coordinates[i][1], geojsonGeometry.coordinates[i][0]);
                        googleObj.push(new google.maps.Marker(opts));
                    }
                    if (geojsonProperties) {
                        for (var k = 0; k < googleObj.length; k++) {
                            googleObj[k].set("geojsonProperties", geojsonProperties);
                        }
                    }
                    break;

                case "LineString":
                    var path = [];
                    for (var i = 0; i < geojsonGeometry.coordinates.length; i++) {
                        var coord = geojsonGeometry.coordinates[i];
                        var ll = new google.maps.LatLng(coord[1], coord[0]);
                        path.push(ll);
                    }
                    opts.path = path;
                    googleObj = new google.maps.Polyline(opts);
                    if (geojsonProperties) {
                        googleObj.set("geojsonProperties", geojsonProperties);
                    }
                    break;

                case "MultiLineString":
                    googleObj = [];
                    for (var i = 0; i < geojsonGeometry.coordinates.length; i++) {
                        var path = [];
                        for (var j = 0; j < geojsonGeometry.coordinates[i].length; j++) {
                            var coord = geojsonGeometry.coordinates[i][j];
                            var ll = new google.maps.LatLng(coord[1], coord[0]);
                            path.push(ll);
                        }
                        opts.path = path;
                        googleObj.push(new google.maps.Polyline(opts));
                    }
                    if (geojsonProperties) {
                        for (var k = 0; k < googleObj.length; k++) {
                            googleObj[k].set("geojsonProperties", geojsonProperties);
                        }
                    }
                    break;

                case "Polygon":
                    var paths = new google.maps.MVCArray;

                    for (var i = 0; i < geojsonGeometry.coordinates.length; i++) {
                        var path = new google.maps.MVCArray;
                        for (var j = 0; j < geojsonGeometry.coordinates[i].length; j++) {
                            var ll = new google.maps.LatLng(geojsonGeometry.coordinates[i][j][1], geojsonGeometry.coordinates[i][j][0]);
                            path.insertAt(path.length, ll);
                        }
                        paths.insertAt(paths.length, path);
                    }
                    opts.paths = paths;
                    googleObj = new google.maps.Polygon(opts);
                    if (geojsonProperties) {
                        googleObj.set("geojsonProperties", geojsonProperties);
                    }
                    break;

                case "MultiPolygon":
                    googleObj = [];
                    for (var i = 0; i < geojsonGeometry.coordinates.length; i++) {
                        var paths = [];
                        for (var j = 0; j < geojsonGeometry.coordinates[i].length; j++) {
                            var path = [];
                            for (var k = 0; k < geojsonGeometry.coordinates[i][j].length; k++) {
                                var ll = new google.maps.LatLng(geojsonGeometry.coordinates[i][j][k][1], geojsonGeometry.coordinates[i][j][k][0]);
                                path.push(ll);
                            }
                            paths.push(path);
                        }
                        opts.paths = paths;
                        googleObj.push(new google.maps.Polygon(opts));
                    }
                    if (geojsonProperties) {
                        for (var k = 0; k < googleObj.length; k++) {
                            googleObj[k].set("geojsonProperties", geojsonProperties);
                        }
                    }
                    break;

                case "GeometryCollection":
                    googleObj = [];
                    if (!geojsonGeometry.geometries) {
                        googleObj = _error("Invalid GeoJSON object: GeometryCollection object missing \"geometries\" member."); //TODO: Error 0
                    } else {
                        for (var i = 0; i < geojsonGeometry.geometries.length; i++) {
                            googleObj.push(_geometryToGoogleMaps(geojsonGeometry.geometries[i], opts, geojsonProperties || null));
                        }
                    }
                    break;

                default:
                    googleObj = _error("Invalid GeoJSON object: Geometry object must be one of \"Point\", \"LineString\", \"Polygon\" or \"MultiPolygon\"."); //TODO: Error 1
            }

            return googleObj;

        };

        var _error = function (message) {
            return {
                type: "Error",
                message: message
            };

        };

        var obj;

        var opts = options || {};

        switch (geojson.type) {
            case "FeatureCollection":
                if (!geojson.features) {
                    obj = _error("Invalid GeoJSON object: FeatureCollection object missing \"features\" member."); //TODO: Error 2
                } else {
                    obj = [];
                    for (var i = 0; i < geojson.features.length; i++) {
                        obj.push(_geometryToGoogleMaps(geojson.features[i].geometry, opts, geojson.features[i].properties));
                    }
                }
                break;
            case "GeometryCollection":
                if (!geojson.geometries) {
                    obj = _error("Invalid GeoJSON object: GeometryCollection object missing \"geometries\" member."); //TODO: Error 3
                } else {
                    obj = [];
                    for (var i = 0; i < geojson.geometries.length; i++) {
                        obj.push(_geometryToGoogleMaps(geojson.geometries[i], opts));
                    }
                }
                break;
            case "Feature":
                if (!(geojson.properties && geojson.geometry)) {
                    obj = _error("Invalid GeoJSON object: Feature object missing \"properties\" or \"geometry\" member."); //TODO: Error 4
                } else {
                    obj = _geometryToGoogleMaps(geojson.geometry, opts, geojson.properties);
                }
                break;
            case "Point": case "MultiPoint": case "LineString": case "MultiLineString": case "Polygon": case "MultiPolygon":
                obj = geojson.coordinates
                    ? obj = _geometryToGoogleMaps(geojson, opts)
                    : _error("Invalid GeoJSON object: Geometry object missing \"coordinates\" member."); //TODO: Error 5
                break;

            default:
                obj = _error("Invalid GeoJSON object: GeoJSON object must be one of \"Point\", \"LineString\", \"Polygon\", \"MultiPolygon\", \"Feature\", \"FeatureCollection\" or \"GeometryCollection\"."); //TODO: Error 6
        }
        return obj;
    };

    var serializePolygon = function (gpoly) {
        var gjp = {
            "type": "Polygon",
            "coordinates": []
        };
        for (var i = 0; i < gpoly.latLngs.length; i++) {
            gjp.coordinates[i] = []
            var ring = gpoly.latLngs.getAt(i)
            for (var j = 0; j < ring.length; j++) {
                var ll = ring.getAt(j)
                gjp.coordinates[i].push([ll.lng(), ll.lat()])
            }
        }
        return gjp
    }

    var printGeoJSON = function () {
        $('#geojson').text(js_beautify(JSON.stringify(serializePolygon(app.googleObj))))
    }

    var createPolygonsFromGeoJson = function (geojson) {
        debugger;
        var polygons = [];
        for (var y = 0; y < geojson.features.length; y++) {
            var points = geojson.features[y];
            for (var z = 0; z < points.geometry.coordinates.length; z++) {
                var coords = points.geometry.coordinates[z];
                var paths = [];
                $.each(coords, function (i, n) {
                    $.each(n, function (j, o) {
                        var path = [];
                        $.each(o, function (k, p) {
                            var ll = new google.maps.LatLng(p[1], [0]);
                            path.push(ll);
                        });
                        paths.push(path);
                    });
                });

                var polygon = new google.maps.Polygon({
                    paths: paths,
                    strokeColor: "#FF7800",
                    strokeOpacity: 1,
                    strokeWeight: 2,
                    fillColor: "#46461F",
                    fillOpacity: 0.25,
                    map: map
                });

                polygons.push(polygon);
            }
        }
        return polygons;
    }

    var textCordinatesToGeojson = function (textCoordinates) {

        function convert(str) {
            try{
                var degrees = parseInt(str.length === 7 ? str.substring(0, 2) : str.substring(0, 3));
                var minutes = parseInt(str.length === 7 ? str.substring(2, 4) : str.substring(3, 5));
                var seconds = parseInt(str.length === 7 ? str.substring(4, 6) : str.substring(5, 7));
                var sufx = str.length === 7 ? str.substring(6, 7) : str.substring(7, 8);

                var sing = (sufx === 'W' || sufx === 'S') ? -1 : 1;
                return sing * (degrees + ((minutes * 60.0 + seconds) / 3600.0));
            }
            catch(e){
                return -1;
            }
        }

        var parts = textCoordinates.split(' ');
        if (parts.length != 2) 
            return null;

        if (parts[0].length > 8 || parts[0].length < 7) {
            return null;
        }
        if (parts[1].length > 8 || parts[1].length < 7) {
            return null;
        }

        var lng = -1;
        var lat = convert(parts[0]);
        if (lat !== 1) {
            lng = convert(parts[1]);
        }

        if (lat === -1 || lng === -1) {
            return null;
        }

        return {
            "type": "FeatureCollection",
            "features": [
                {
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                        "type": "Point",
                        "bbox": [lat, lng, lat, lng],
                        "coordinates": [lng, lat]
                    }
                }]
        };

    }

    var geojsonToTextCordinates = function (geoString) {

        function pad(str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        };

        function convert(dec, type) {
            try{
                var minus = dec < 0;
                dec = Math.abs(dec);
                var degrees = Math.floor(dec);
                var seconds = (dec - degrees) * 3600;
                var minutes = Math.floor(seconds / 60);
                seconds = Math.floor(seconds - (minutes * 60));

                if (type === 'lat') {
                    return pad(degrees, 2) + pad(minutes, 2) + pad(seconds, 2) + (minus ? 'S' : 'N');
                }

                return pad(degrees, 3) + pad(minutes, 2) + pad(seconds, 2) + (minus ? 'W' : 'E');
            }
            catch(e)
            {
                return ""
            }
        }

        var geojson = JSON.parse(geoString);
        var coordinates = geojson.features[0].geometry.coordinates;

        var lat = convert(coordinates[1], 'lat');
        var lng = convert(coordinates[0], 'lng');

        return lat + ' ' + lng;
    }

    return {
        //map: map,
        //infoWindow: infoWindow,
        initMap: initMap,
        addGeoJsonFeature: addGeoJsonFeature,
        changeMarkerLocation: changeMarkerLocation,
        clearPolygons: clearPolygons,
        resize: resize,
        geojsonToTextCordinates: geojsonToTextCordinates,
        textCordinatesToGeojson: textCordinatesToGeojson,
        clearPolygonsExcludingDoa: clearPolygonsExcludingDoa
    }

})();





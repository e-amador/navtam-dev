﻿import { Component, OnInit, Input, AfterViewInit, Inject, ViewChild } from '@angular/core'
import { TOASTR_TOKEN, Toastr } from '../../common/toastr.service';
import { ActivatedRoute, Router } from '@angular/router'

import { LocaleService, TranslationService } from 'angular-l10n';

import { DataService } from '../shared/data.service'
import { MemoryStorageService } from '../shared/mem-storage.service'
import { WindowRef } from '../../common/windowRef.service'

import {
    INotam,
    IPaging,
    ITableHeaders,
    IQueryOptions,
    ProposalType
} from '../shared/data.model'

import * as moment from 'moment';
import * as _ from 'lodash';

import { ReportFilterComponent } from './report-filter.component';

declare var $: any;

@Component({
    selector: 'report-active',
    templateUrl: '/app/dashboard/shared/report-active.component.html',
})
export class ReportActiveComponent implements OnInit, AfterViewInit  {
    @ViewChild(ReportFilterComponent) filter: ReportFilterComponent;
    @Input('exportFunc') exportFunc: any;

    notams: INotam[]; 
    selectedNotam: INotam;

    queryOptions: IQueryOptions;

    tableHeaders: ITableHeaders;
    paging: IPaging = {
        currentPage: 1,
        pageSize: 0,
        totalCount: 0,
        totalPages: 0,
    };
    totalPages: number = 0;
    loadingData: boolean = true;
    isFilterApplied: boolean = false;
    filterPannelOpen: boolean = false;
    filterByActive: string = 'All';

    start: any = null;
    starttime: any = null;
    endtime: any = null;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private route: ActivatedRoute,
        private translation: TranslationService) {

        this.tableHeaders = {
            sorting:
                [
                    { columnName: 'notamId', direction: 0 },
                    { columnName: 'type', direction: 0 },
                    { columnName: 'referredNotamId', direction: 0 },
                    { columnName: 'series', direction: 0 },
                    { columnName: 'itemA', direction: 0 },
                    { columnName: 'siteId', direction: 0 },
                    { columnName: 'permanent', direction: 0 },
                    { columnName: 'estimated', direction: 0 },
                    { columnName: 'itemE', direction: 0 },
                    { columnName: 'originator', direction: 0 },
                    { columnName: 'modifiedByUsr', direction: 0 },
                    { columnName: 'startActivity', direction: 0 },
                    { columnName: 'endValidity', direction: 0 },
                    { columnName: 'received', direction: 0 },
                    { columnName: 'published', direction: 1 }
                ],
            itemsPerPage:
                [
                    { id: '10', text: '10' },
                    { id: '25', text: '25' },
                    { id: '50', text: '50' },
                    { id: '100', text: '100' },
                    { id: '1000', text: '1000' },
                ]
        };
        this.paging.pageSize = +this.tableHeaders.itemsPerPage[3].id;
        this.queryOptions = {
            page: 1,
            pageSize: this.paging.pageSize,
            sort: "_Published"
        }
    }

    ngOnInit() {
        let self = this;

        this.starttime = "00:00";
        this.endtime = "59:59";

        $('.rpt-notam-datepicker').each(function () {
            $(this).bootstrapDatepicker({
                autoclose: false,
                todayHighlight: true,
                orientation: "top",
                startView: 0, // 0: month view , 1: year view, 2: multiple year view
                language: "en",
                forceParse: false,
                daysOfWeekDisabled: "", // Disable 1 or various day. For monday and thursday: 1,3
                calendarWeeks: false, // Display week number 
                toggleActive: true, // Close other when open
                multidate: false, // Allow to select various days
            }).on('changeDate', function (e) {
                self.dateChanged(e);
                $(this).bootstrapDatepicker('hide');
            });
        });

        $('#w-starttime').mask('00:00', { onComplete: function (cep) { self.starttime = cep; } });
        $('#w-endtime').mask('00:00', { onComplete: function (cep) { self.endtime = cep; } });

        this.isFilterApplied = this.memStorageService.get(this.memStorageService.REPORT_FILTER_QUERY_OPTIONS_KEY);
        if (this.isFilterApplied) {
            this.filter.loadFilterState();
        } else {
            let queryOptions = this.memStorageService.get(this.memStorageService.REPORT_NOTAM_ACTIVE_QUERY_OPTIONS_KEY);
            if (queryOptions !== null) {
                this.queryOptions = queryOptions;
            }
        }

        if (this.isFilterApplied) {
            this.filter.runReportActiveFilter(false, this.start.toDate(), this.starttime, this.endtime, this.queryOptions.page, this.queryOptions.pageSize)
        } else {
            this.getNotams(this.queryOptions.page);
        }
    }

    ngAfterViewInit() {
        this.filter.parent = this;
    }

    itemsPerPageChanged(e: any): void {
        this.queryOptions.pageSize = e.value;
        if (this.isFilterApplied) {
            this.loadingData = true;
            this.filter.runFilter(false, 1, this.queryOptions.pageSize);
        }
        else {
            this.getNotams(1);
        }
    }

    onFilterChanged(event: any) {
        if (event.error) {
            this.toastr.error("Filter error: " + event.error.message, "", { 'positionClass': 'toast-bottom-right' });
        } else {
            this.isFilterApplied = event.isFilterApplied;
            this.filterPannelOpen = false;
            if (this.isFilterApplied) {
                this.processNotams(event.data);
            } else {
                this.queryOptions = this.memStorageService.get(this.memStorageService.REPORT_NOTAM_QUERY_OPTIONS_KEY);
                this.getNotams(this.queryOptions.page);
            }
        }
        this.loadingData = false;
    }

    onPageChange(page: number) {
        this.getNotams(page);
    }

    dateChanged(ev) {
        this.start = moment(new Date(ev.currentTarget.value));        
        $(this).datepicker('hide');
    }

    sort(index) {
        const dir = this.tableHeaders.sorting[index].direction;

        for (let i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }

        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            if (this.isFilterApplied) {
                this.filter.changeFilterSort('_' + this.tableHeaders.sorting[index].columnName);
            } else {
                this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
            }
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            if (this.isFilterApplied) {
                this.filter.changeFilterSort(this.tableHeaders.sorting[index].columnName);
            } else {
                this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
            }
        }

        if (this.isFilterApplied) {
            this.loadingData = true;
            this.filter.runFilter(false, 1);
        } else {
            this.getNotams(1);
        }
    }

    sortingClass(index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';

        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';

        return 'sorting';
    }


    onRowSelected(index) {
        let notam = this.notams[index];
        if (this.selectedNotam) {
            this.selectedNotam.isSelected = false;
        }
        this.selectedNotam = notam
        this.selectedNotam.isSelected = true;
    }

    isStartHasValue() {
        return $('#rpt-notam-startdate').bootstrapDatepicker('getDate') != null;
    }

    isRunReportButtonEnabled() {
        return this.isStartHasValue();
    }

    runReport() {
        this.getNotams(1);
    }

    exportReport() {
        let filename = window.prompt(this.translation.translate('Reports.PromptFilename'));
        if (filename) {
            filename = filename.replace('.csv', '') + '.csv';

            this.loadingData = true;

            const payload = {
                Conditions: this.isFilterApplied ? this.filter.filters : null,
                queryOptions: this.queryOptions,
                start: this.start.toDate(),
                startTime: this.starttime,
                endTime: this.endtime
            };

            this.dataService.exportNofReportActiveNotams(payload)
                .subscribe((response: any) => {
                    var csvData = new Blob([response], { type: 'text/csv;charset=utf-8;' });
                    var csvURL = window.URL.createObjectURL(csvData);
                    var tempLink = document.createElement('a');
                    tempLink.href = csvURL;
                    tempLink.setAttribute('download', filename);
                    tempLink.click();
                    this.loadingData = false;
                });
        }
    }

    filterByActives(status : string) {
        this.filterByActive = status;
    }

    notamList() {
        if (this.notams) {
            return this.notams;
        }
    }

    private getNotams(page) {
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            if (this.isFilterApplied) {
                this.filter.runReportActiveFilter(false, this.start.toDate(), this.starttime, this.endtime, page, this.queryOptions.pageSize);
            }
            else {
                this.memStorageService.save(this.memStorageService.REPORT_NOTAM_ACTIVE_QUERY_OPTIONS_KEY, this.queryOptions);
                this.dataService.getNofReportActiveNotams(this.start.toDate(), this.starttime, this.endtime, this.queryOptions)
                    .subscribe((response: any) => {
                        this.processNotams(response);
                    });
            }
        }
        catch (e) {
            this.loadingData = false;
        }
    }

    private processNotams(notams: any) {
        this.paging = notams.paging;
        this.totalPages = notams.paging.totalPages;

        for (var iter = 0; iter < notams.data.length; iter++) {
            notams.data[iter].index = iter;
            notams.data[iter].typeStr = this.toNotamTypeStr(notams.data[iter].notamType);
            //notams[iter].itemEFull = notams[iter].itemE;
            //notams[iter].itemE = notams[iter].itemE.length > 3 ? notams[iter].itemE.substr(0, 3) + ' ...' : notams[iter].itemE;
        }

        const lastNotamSelected = this.memStorageService.get(this.memStorageService.NOTAM_ID_KEY);
        if (notams.data.length > 0) {
            this.notams = notams.data;
            let index = 0;
            if (lastNotamSelected) {
                index = this.notams.findIndex(x => x.id === lastNotamSelected);
                if (index < 0) index = 0; // when not found first should be selected;
            }
            this.notams[index].isSelected = true;
            this.selectedNotam = this.notams[index];
            this.loadingData = false;
            //this.onRowSelected(0);
        } else {
            this.notams = [];
            this.selectedNotam = null;
            this.loadingData = false;
        }

    }

    private toNotamTypeStr(notamType) {
        switch (notamType) {
            case ProposalType.New: return 'N';
            case ProposalType.Replace: return 'R';
            case ProposalType.Cancel: return 'C';
            default: return '';
        }
    }
}


using NDS.SolaceVMR;

namespace NavCanada.Core.TaskEngine.Implementations
{
    using System;
    using System.Configuration;

    using Common.Contracts;
    using Data.NotamWiz.EF;
    using Data.NotamWiz.EF.Helpers;
    using NotificationEngine;
    using Proxies.DiagnosticProxy;
    using SqlServiceBroker;
    using Contracts;
    using MessageTask;
    using MtoProcessor;
    using Quartz;
    using Quartz.Impl;
    using Triggers;
    using QueueListener;
    using Proxies.AeroRdsProxy;
    using NavCanada.Core.Common.Security;

    public class TaskEngineSettings
    {
        public int MtoProcessorThreadCount { get; set; }
        public int MtoProcessorNapTime { get; set; }
        public int MaxPoisonMtoRetryCount { get; set; }
        public string ClusterDbConnectionString { get; set; }
    }

    public class TaskEngineShell : IClusterStorageFactory, IMtoProcessorFactory
    {
        public TaskEngineShell(ILogger logger)
        {
            _settings = ReadTaskEngineSettings();
            _logger = logger;

            var messageTaskFactory = new MessageTaskFactory();
            

            _mtoDispatcher = new TaskEngineMtoDispatcher(
                this,
                messageTaskFactory,
                this,
                logger,

                new TaskEngineDispatcherSettings
                {
                    MaxPoisonMtoRetryCount = _settings.MaxPoisonMtoRetryCount,
                    EnabledPerfCounter = ConfigurationManager.AppSettings["EnablePerfCounter"] != null && 
                        Convert.ToBoolean(ConfigurationManager.AppSettings["EnablePerfCounter"]),
                    SoonToExpireHrsDifference = Convert.ToInt32(ConfigurationManager.AppSettings["SoontoExpireHrsDifference"]),
                    InstanceName = ConfigurationManager.AppSettings["TaskEngineInstanceName"]
                });

            _externalServices = SetExternalServices();

            CreateSchedulersWithJobs();

            var hubConnectionString = GetHubConnectionString();
            var hubQueue = ConfigurationManager.AppSettings["HubQueue"];
            _ndsQueueListener = NdsQueueListener.Create(new NdsQueueListenerContext(this, _logger, hubConnectionString, hubQueue));
        }

        private TaskEngineSettings ReadTaskEngineSettings()
        {
            return new TaskEngineSettings
            {
                MtoProcessorThreadCount = Convert.ToInt32(ConfigurationManager.AppSettings["MtoProcessorThreadCount"]),
                MtoProcessorNapTime = Convert.ToInt32(ConfigurationManager.AppSettings["MtoProcessorNapTime"]),
                MaxPoisonMtoRetryCount = Convert.ToInt32(ConfigurationManager.AppSettings["MaxPoisonMtoRetryCount"]),
                ClusterDbConnectionString = GetAppConnectionString(),
            };
        }

        private void CreateSchedulersWithJobs()
        {
            //IClusterStorage storage;
            //NewStorageConnection(out storage);

            _scheduler = StdSchedulerFactory.GetDefaultScheduler();

            CreateSdoEffectiveCacheSyncScheduler();
            CreateSdoEffectiveCacheLoadScheduler();

            CreateHubConnectionTestScheduler();

            CreateChecklistScheduler();

            CreateCheckSeriesNumbersScheduler();

            CreateExpiredProposalJob();

            CreateQueryListenerGuardJob();
        }

        private void CreateHubConnectionTestScheduler()
        {
            JobKey testConnectionJobKey = new JobKey("HUB_CONNECTION_TEST", "HUB_CONNECTION");

            IJobDetail testHubConnectionJob = JobBuilder.Create<HubConnectionTestJob>()
                .WithIdentity(testConnectionJobKey)
                .Build();

            ITrigger testHubConnectionTrigger = TriggerBuilder.Create()
                .WithIdentity("HUB_CONNECTION_TEST", "HUB_CONNECTION")
                .StartNow()
                .WithSimpleSchedule(x => x.WithIntervalInMinutes(5) // every 5 minutes (check!!)
                .RepeatForever())
                .Build();

            _scheduler.ScheduleJob(testHubConnectionJob, testHubConnectionTrigger);
        }

        public void Start()
        {
            _mtoDispatcher.StartProcessing(_settings.MtoProcessorThreadCount, _settings.MtoProcessorNapTime);
            _scheduler.Start();
            _ndsQueueListener.StartListening();
        }

        public void Stop()
        {
            _ndsQueueListener.StopListening();
            _scheduler.Shutdown();
            _mtoDispatcher.StopProcessing(1000);
        }

        public bool NewStorageConnection(out IClusterStorage clusterStorage)
        {
            try
            {
                var provider = new RepositoryProvider(new RepositoryFactories());
                var uow = new Uow(provider);

                IClusterQueues clusterQueues = new ClusterQueues(
                    new SqlServiceBrokerQueue(uow.DbContext, SqlServiceBrokerQueueSettings.InitiatorToTaskEngineSsbSettings),
                    new SqlServiceBrokerQueue(uow.DbContext, SqlServiceBrokerQueueSettings.TaskEngineToInitiatorSsbSettings));

                clusterStorage = new ClusterStorage(uow, clusterQueues);

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogFatal(ex, typeof(TaskEngineShell));
                clusterStorage = null;
                return false;
            }
        }

        public IMtoProcessor CreateMtoProcessor(IClusterStorage repositories, IMessageTaskFactory taskFactory, MtoProcessorSettings mtoProcessorSettings)
        {
            return new MtoProcessor(repositories, _externalServices, taskFactory, mtoProcessorSettings);
        }

        #region Jobs and Schedulers

        private void CreateSdoEffectiveCacheSyncScheduler()
        {
            var sdoSyncCronScheduler = ConfigurationManager.AppSettings["SdoCacheSyncCronScheduler"] ?? "0 0 22 * * ?"; // or daily at @22:00

            var sdoSyncJobKey = new JobKey("SDO_CACHE_SYNC_JOB", "SCHEDULER_GROUP_RDS");

            var sdoSyncJob = JobBuilder.Create<AeroRDSCacheSyncJob>()
                .WithIdentity(sdoSyncJobKey)
                .Build();

            var sdoSyncTrigger = TriggerBuilder.Create()
                .WithIdentity("SDO_CACHE_SYNC_TRIGGER", "SCHEDULER_GROUP_RDS")
                .StartNow()
                .WithCronSchedule(sdoSyncCronScheduler)
                .Build();

            _scheduler.ScheduleJob(sdoSyncJob, sdoSyncTrigger);
        }

        private void CreateSdoEffectiveCacheLoadScheduler()
        {
            var sdoSyncCronScheduler = ConfigurationManager.AppSettings["SdoCacheLoadCronScheduler"] ?? "0 30 0 * * ?"; // or daily at @00:30

            var sdoLoadJobKey = new JobKey("SDO_CACHE_LOAD_JOB", "SCHEDULER_GROUP_RDS");

            var sdoLoadJob = JobBuilder.Create<AeroRDSCacheLoadJob>()
                .WithIdentity(sdoLoadJobKey)
                .Build();

            var sdoSyncTrigger = TriggerBuilder.Create()
                .WithIdentity("SDO_CACHE_LOAD_TRIGGER", "SCHEDULER_GROUP_RDS")
                .StartNow()
                .WithCronSchedule(sdoSyncCronScheduler)
                .Build();

            _scheduler.ScheduleJob(sdoLoadJob, sdoSyncTrigger);
        }

        private void CreateChecklistScheduler()
        {
            var checklistCronScheduler = ConfigurationManager.AppSettings["ChecklistCronScheduler"] ?? "0 30 23 L * ?"; // last day of every month @23:30
            var checklistJobKey = new JobKey("CHECKLIST_JOB", "CHECKLIST_GROUP");

            var checklistJob = JobBuilder.Create<ChecklistJob>()
                .WithIdentity(checklistJobKey)
                .Build();

            var checklistTrigger = TriggerBuilder.Create()
                .WithIdentity("CHECKLIST_TRIGGER", "CHECKLIST_GROUP")
                .StartNow()
                .WithCronSchedule(checklistCronScheduler)
                .Build();

            _scheduler.ScheduleJob(checklistJob, checklistTrigger);
        }

        private void CreateCheckSeriesNumbersScheduler()
        {
            JobKey jobKey = new JobKey("CHECK_SERIES_NUMBERS_JOB", "SCHEDULER_GROUP_SERIES_NUMBERS");

            IJobDetail jobDetails = JobBuilder.Create<CheckSeriesNumbersJob>()
                .WithIdentity(jobKey)
                .Build();

            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("CHECK_SERIES_NUMBERS_TRIGGER", "SCHEDULER_GROUP_SERIES_NUMBERS")
                .StartNow()
#if DEBUG
                .WithSimpleSchedule(x => x.WithIntervalInMinutes(5) // every 5 minutes
#else
                .WithSimpleSchedule(x => x.WithIntervalInHours(1) // every hour
#endif
                .RepeatForever())
                .Build();

            _scheduler.ScheduleJob(jobDetails, trigger);
        }

        private void CreateExpiredProposalJob()
        {
            var cronScheduler = ConfigurationManager.AppSettings["ExpiredNotamsCronScheduler"] ?? "2 0 0 * * ?"; // or daily at @02:00
            JobKey expireProposalSyncJobKey = new JobKey("EXPIRE_PROPOSAL_SYNC_JOB", "SCHEDULER_GROUP_PROPSALSTATE");

            IJobDetail expireProposalJob = JobBuilder.Create<CheckExpireProposalSyncJob>()
                .WithIdentity(expireProposalSyncJobKey)
                .Build();

            ITrigger expireProposalTrigger = TriggerBuilder.Create()
                .WithIdentity("EXPIRE_PROPOSAL_TRIGGER", "SCHEDULER_GROUP_PROPSALSTATE")
                .StartNow()
                .WithCronSchedule(cronScheduler)
                .Build();

            _scheduler.ScheduleJob(expireProposalJob, expireProposalTrigger);
        }

        private void CreateQueryListenerGuardJob()
        {
            var ndsSyncJobKey = new JobKey("NDS_QUERY_GUARD_JOB");

            var ndsSyncJob = JobBuilder.Create<QueryListenerGuardJob>()
                .WithIdentity(ndsSyncJobKey)
                .Build();

            var ndsSyncTrigger = TriggerBuilder.Create()
                .WithIdentity("NDS_QUERYGARD_TRIGGER", "SCHEDULER_GROUP_NDS")
                .StartNow()
                .WithSimpleSchedule(x => x.WithIntervalInSeconds(30)
                .RepeatForever())
                .Build();

            _scheduler.ScheduleJob(ndsSyncJob, ndsSyncTrigger);
        }

#endregion

        private IExternalServices SetExternalServices()
        {
            var useNotificationEngine = ConfigurationManager.AppSettings["AddNotifications"];
            var dianosticServiceEndpoint = ConfigurationManager.AppSettings["DianosticServiceEndpoint"];

            IDiagnosticProxy diagnosticProxy = null;
            if (!string.IsNullOrEmpty(dianosticServiceEndpoint))
            {
                var taskEngineInstanceName = ConfigurationManager.AppSettings["TaskEngineInstanceName"];
                if (taskEngineInstanceName != null)
                    diagnosticProxy = new DiagnosticProxy(dianosticServiceEndpoint + "/api/keepalive");
            }

            INotifier notifier = null;
            if (useNotificationEngine != null && useNotificationEngine == "True")
            {
                var host = ConfigurationManager.AppSettings["SmtpHost"];
                var port = ConfigurationManager.AppSettings["SmtpPort"];

                var senderEmailAddress = ConfigurationManager.AppSettings["SenderEmail"];

                if (string.IsNullOrEmpty(host))
                    throw new InvalidOperationException("The smtp host for notifications is required");

                if (string.IsNullOrEmpty(port))
                    throw new InvalidOperationException("The smtp port for notifications is required");

                if (string.IsNullOrEmpty(senderEmailAddress))
                    throw new InvalidOperationException("The sender email address for notifications is required");

                notifier = new Notifier(host, int.Parse(port), senderEmailAddress);
            }

            var solaceConnectionString = GetHubConnectionString();
            var solaceEndpoint = new SolaceNotificationEndpoint(solaceConnectionString);

            var aeroRdsFeaturesUrl = ConfigurationManager.AppSettings["AeroRDSFeaturesUrl"] ?? string.Empty;
            var aeroRdsProxy = new AeroRdsFeatureProxy(aeroRdsFeaturesUrl);

            return new ExternalServices(diagnosticProxy, notifier, solaceEndpoint, aeroRdsProxy, _logger);
        }

        static string GetAppConnectionString() => 
            CipherUtils.DecryptConnectionString(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

        static string GetHubConnectionString() =>
            CipherUtils.DecryptConnectionString(ConfigurationManager.ConnectionStrings["HubConnectionString"]?.ConnectionString);

        private readonly ILogger _logger;
        private readonly IMultithreadProcessor _mtoDispatcher;
        private readonly IExternalServices _externalServices;
        private readonly NdsQueueListener _ndsQueueListener;
        private readonly TaskEngineSettings _settings;
        private IScheduler _scheduler; 
    }
}

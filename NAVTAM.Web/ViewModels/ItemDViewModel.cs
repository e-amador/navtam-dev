﻿using NavCanada.Core.Domain.Model.Enums;

namespace NAVTAM.ViewModels
{
    public class ItemDViewModel
    {
        public int? Id { get; set; }
        public string EventId { get; set; }
        public int ProposalId { get; set; }
        public CalendarType CalendarType { get; set; }
        public int CalendarId { get; set; }
        public string Event { get; set; }
        public string ItemD { get; set; }
    }

    public class ItemDValidatorViewModel
    {
        public string ItemD { get; set; }
    }

}
﻿namespace NavCanada.Core.Domain.Model.Enums
{
    public enum DoaFilterType
    {
        SubjectType,
        SubjectId
    }
}
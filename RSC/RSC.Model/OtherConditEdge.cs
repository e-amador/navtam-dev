﻿using RSC.Model.Enums;

namespace RSC.Model
{
    /// <summary>
    /// Structure is used to define other, generally 'edge' related conditions such as
    /// windrows and snow drifts in terms of the location(as a distance from a specified runway edge or
    /// edges, and across intersections with other runways) and height.
    /// </summary>
    public class OtherConditEdge
    {
        /// <summary>
        /// Type of additional surface condition. 'R' = Right HMI pane.
        /// </summary>
        public OtherConditionsEdgeEnum ConditTypeR { get; set; }
        /// <summary>
        /// Other Condition Height.
        /// </summary>
        public OCHeightType OCHeight { get; set; }
        /// <summary>
        /// The location of the condition.
        /// </summary>
        public OCLocationEnum OCLocation { get; set; }
        /// <summary>
        /// Distance: integer value from 0 to half the runway width.
        /// </summary>
        public int OCDistance { get; set; }
        /// <summary>
        /// Distance Unit of Measure: either ft or ins.
        /// </summary>
        public UnitOfMeasurement OCDistanceUOM { get; set; }
        /// <summary>
        /// Define which sides of the runway are affected by this condition.
        /// </summary>
        public CardinalSideEnum[] OCSide { get; set; }
        /// <summary>
        /// Runway ident in the form 'RWY-xx/yy'. This ident must exist in the
        /// ADMS for the specified aerodrome.Identifies a runway intersection which is affected
        /// by the conditions being reported for the subject runway.
        /// </summary>
        public string[] AcrossRwy { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public class UserProfile : IdentityUser
    {
        public short DefaultLanguage { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public override bool LockoutEnabled { get; set; }

        public string Address { get; set; }

        public string Position { get; set; }

        public string Fax { get; set; }

        public bool IsApproved { get; set; }

        public bool IsDeleted { get; set; }

        public virtual Organization Organization { get; set; }
        //Leave set as private since we can never set New Organization from User

        public int? OrganizationId { get; set; }

        public UserPreference UserPreference { get; set; }

        public int? UserPreferenceId { get; set; }

        public virtual Registration Registration { get; set; }
        // Leave set as private since we can never set New Registration from User

        public int? RegistrationId { get; set; }

        public DigitalSla DigitalSla { get; set; }

        public int? DigitalSlaId { get; set; }

        public DateTime LastPasswordChangeDate { get; set; }

        public string AccessToken { get; set; }

        public bool Disabled { get; set; }

        //public virtual ICollection<UserDoa> Doas { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<UserProfile> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}
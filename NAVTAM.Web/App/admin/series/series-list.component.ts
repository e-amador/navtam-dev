﻿import { Component, OnInit, Inject } from '@angular/core'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators, ValidatorFn } from '@angular/forms'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';

import { ActivatedRoute, Router } from '@angular/router'

import { Select2OptionData } from 'ng2-select2';

import { DataService } from '../shared/data.service'
import { MemoryStorageService } from '../shared/mem-storage.service'
import { WindowRef } from '../../common/windowRef.service'

import { LocaleService, TranslationService } from 'angular-l10n';

import {
    IQueryOptions,
    IPaging,
    ISeriesPartial,
    ITableHeaders
} from '../shared/data.model'

declare var $: any;

@Component({
    templateUrl: '/app/admin/series/series-list.component.html'
})
export class SeriesListComponent implements OnInit {

    seriesAllocations: ISeriesPartial[];
    selectedSerie: ISeriesPartial = null;

    queryOptions: IQueryOptions;
    paging: IPaging = {
        currentPage: 1,
        pageSize: 0,
        totalCount: 0,
        totalPages: 0,
    };
    totalPages: number = 0;

    itemsPerPageData: Array<any>;
    itemsPerPageStartValue: string;
    loadingData: boolean = false;
    tableHeaders: ITableHeaders;
    canbeDeleted: boolean = true;
    serieListForm: FormGroup; 

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private route: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService) {

        if (this.winRef.appConfig.ccs)
            this.router.navigate(['/administration']);

        this.tableHeaders = {
            sorting:
            [
                { columnName: 'Subject', direction: 0 },
                { columnName: 'RegionId', direction: 0 },
                { columnName: 'DisseminationCategory', direction: 0 },
                { columnName: 'QCode', direction: 0 },
                { columnName: 'Series', direction: 0 },
            ],
            itemsPerPage:
            [
                { id: '10', text: '10' },
                { id: '20', text: '20' },
                { id: '50', text: '50' },
                { id: '100', text: '100' },
                { id: '200', text: '200' },
            ]
        };

        this.paging.pageSize = +this.tableHeaders.itemsPerPage[0].id;
        this.queryOptions = {
            entity: "serie",
            page: 1,
            pageSize: this.paging.pageSize,
            sort: "_" + this.tableHeaders.sorting[0].columnName, //Default Received Descending
            filterValue: ""
        }
    };

    ngOnInit() {
        if (this.winRef.appConfig.ccs)
            this.router.navigate(['/administration']);

        //Set latest query options
        const queryOptions = this.memStorageService.get(this.memStorageService.ITEM_QUERY_OPTIONS_KEY);
        if (queryOptions && queryOptions.entity === "serie") {
            this.queryOptions = queryOptions;
        } else {
            this.queryOptions.sort = this.tableHeaders.sorting[1].columnName; // region
        }
        this.configureReactiveForm();
        this.getSeriesInPage(this.queryOptions.page);
    }

    get langCulture() {
        return (window["app"].cultureInfo || "en").toUpperCase();
    }

    private configureReactiveForm() {
        this.serieListForm = this.fb.group({
            searchSerie: [{ value: this.queryOptions.filterValue, disabled: false }, []],
        });
        this.$ctrl("searchSerie").valueChanges.debounceTime(1000).subscribe(() => this.updateSerieList());
    }

    updateSerieList(): void {
        if (this.loadingData) return;
        const subject = this.$ctrl('searchSerie');
        this.queryOptions.filterValue = subject.value;
        this.getSeriesInPage(this.queryOptions.page);
    }

    $ctrl(name: string): AbstractControl {
        return this.serieListForm.get(name);
    }

    selectSerieAllocation(serie: ISeriesPartial) {
        if (this.selectedSerie) {
            this.selectedSerie.isSelected = false;
        }
        //this.canbeDeleted = true;
        this.selectedSerie = serie;
        this.setCanBeDeleted(this.selectedSerie.id);
        this.selectedSerie.isSelected = true;
        this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, this.selectedSerie.id);

    }

    itemsPerPageChanged(e: any): void {
        this.queryOptions.pageSize = e.value;
        this.getSeriesInPage(1);
    }

    onPageChange(page: number) {
        this.getSeriesInPage(page);
    }

    sort(index) {
        const dir = this.tableHeaders.sorting[index].direction;

        for (let i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }

        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
        }

        this.getSeriesInPage(this.queryOptions.page);
    }

    sortingClass(index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';

        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';

        return 'sorting';
    }

    confirmDeleteSerieAllocation() {
        if (this.selectedSerie) {
            if (this.canbeDeleted) {
                this.deleteSeries();
            } else {
                $('#modal-delete').modal({});
            }
        }
    }

    deleteSeries() {
        if (this.selectedSerie) {
            this.dataService.deleteSerie(this.selectedSerie.id)
                .subscribe(result => {
                    this.getSeriesInPage(this.paging.currentPage);
                    let msg = this.translation.translate('SeriesAllocationSession.SuccessSerieDeleted');
                    this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                }, error => {
                    let msg = this.translation.translate('SeriesAllocationSession.FailureSerieDeleted');
                    this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        }
    }

    setCanBeDeleted(id: number): void {
        this.dataService.serieCanBeDeleted(id)
            .subscribe(result => {
                this.canbeDeleted = result;
            }, error => {
                let msg = this.translation.translate('SeriesAllocationSession.FailureSerieDeleted');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });

    }

    private getSeriesInPage(page) {
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, this.queryOptions);

            this.dataService.getRegisteredSeries(this.queryOptions)
                .subscribe((series: any) => {
                    this.processSeries(series);
                });
        }
        catch (e) {
            this.loadingData = false;
        }
    }

    private processSeries(series: any) {
        this.paging = series.paging;
        this.totalPages = series.paging.totalPages;

        for (var iter = 0; iter < series.data.length; iter++) series.data[iter].index = iter;

        const lastSerieIdSelected = this.memStorageService.get(this.memStorageService.ITEM_ID_KEY);
        if (series.data.length > 0) {
            this.seriesAllocations = series.data;
            let index = 0;
            if (lastSerieIdSelected) {
                index = this.seriesAllocations.findIndex(x => x.id === lastSerieIdSelected);
                if (index < 0) index = 0; // when not found first should be selected;
            }
            this.seriesAllocations[index].isSelected = true;
            this.selectedSerie = this.seriesAllocations[index];
            this.loadingData = false;
            //this.canbeDeleted = true;
            this.setCanBeDeleted(this.selectedSerie.id);
        } else {
            this.seriesAllocations = [];
            this.selectedSerie = null;
            this.loadingData = false;
            this.canbeDeleted = false;
        }
    }


}

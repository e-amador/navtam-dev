﻿using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NDS.Common;
using NDS.DataBus.Contracts;
using NDS.QueryMonitor;

namespace NDS.Tests
{
    [TestClass]
    public class QueryMonitorTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Missing dataBus must throw exeption!")]
        public void MissingDataBuss_MustFail()
        {
            var headAppend = "PROCCESSED";
            var queryService = new QueryServiceMock(headAppend);
            var logger = new Logger();
            var queryMonitor = new DataBusQueryMonitor(null, queryService, logger);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Missing queryService must throw exeption!")]
        public void MissingQueryService_MustFail()
        {
            var headAppend = "PROCCESSED";
            var dataBus = new DataBusMock();
            var queryService = new QueryServiceMock(headAppend);
            var logger = new Logger();
            var queryMonitor = new DataBusQueryMonitor(dataBus, null, logger);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Missing logger must throw exeption!")]
        public void MissingLogger_MustFail()
        {
            var headAppend = "PROCCESSED";
            var dataBus = new DataBusMock();
            var queryService = new QueryServiceMock(headAppend);
            var logger = new Logger();
            var queryMonitor = new DataBusQueryMonitor(dataBus, queryService, null);
        }

        [TestMethod]
        public void StartListening_MustCreateQueue()
        {
            var headAppend = "PROCCESSED";
            var dataBus = new DataBusMock();
            var queryService = new QueryServiceMock(headAppend);
            var logger = new Logger();

            var queryMonitor = new DataBusQueryMonitor(dataBus, queryService, logger);

            var queueName = "TestQueue";
            queryMonitor.StartListening(queueName, "replyto");

            Assert.IsTrue(dataBus.QueueExists(queueName), $"Queue '{queueName}' must exists!");
        }

        [TestMethod]
        public void StopListening_MustUnsubscribeFromTheListenQueue()
        {
            var headAppend = "PROCCESSED";
            var dataBus = new DataBusMock();
            var queryService = new QueryServiceMock(headAppend);
            var logger = new Logger();

            var queryMonitor = new DataBusQueryMonitor(dataBus, queryService, logger);

            var queueName = "TestQueue";
            queryMonitor.StartListening(queueName, "replyto");

            queryMonitor.StopListening();

            var subscriptionCount = dataBus.GetQueueSubscriptionCount(queueName);

            Assert.IsTrue(subscriptionCount == 0, $"After StopListening '{queueName}' subscription ({subscriptionCount}) must be zero!");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Listen queue name must be valid!")]
        public void StartListening_WithEmptyQueueName_MustFail()
        {
            var headAppend = "PROCCESSED";
            var dataBus = new DataBusMock();
            var queryService = new QueryServiceMock(headAppend);
            var logger = new Logger();

            var queryMonitor = new DataBusQueryMonitor(dataBus, queryService, logger);

            queryMonitor.StartListening(string.Empty, "replyto");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Reply to attribute name must be valid!")]
        public void StartListening_WithEmptyReplyToAttributeName_MustFail()
        {
            var headAppend = "PROCCESSED";
            var dataBus = new DataBusMock();
            var queryService = new QueryServiceMock(headAppend);
            var logger = new Logger();

            var queryMonitor = new DataBusQueryMonitor(dataBus, queryService, logger);

            queryMonitor.StartListening("TestQueue", string.Empty);
        }

        [TestMethod]
        public void WhenMessagePosted_MustPerformQuery_ThenPostBackToReplyQueue()
        {
            var headAppend = "PROCCESSED";
            var dataBus = new DataBusMock();
            var queryService = new QueryServiceMock(headAppend);
            var logger = new Logger();

            var queryMonitor = new DataBusQueryMonitor(dataBus, queryService, logger);

            var requestQueueName = "RequestQueue";
            queryMonitor.StartListening(requestQueueName, "replyto");

            var replyQueueName = "ReplyQueue";
            dataBus.CreateQueue(replyQueueName);

            var queryMessage = "[<QUERY>]";

            var waitHandle = new AutoResetEvent(false);

            bool acknowledged = false;
            dataBus.ReadFromQueue(replyQueueName, msg =>
            {
                // the mock query service concats Processed-> with the query, so this is what I expect back
                acknowledged = msg.Body.StartsWith(headAppend) && msg.Body.EndsWith(queryMessage);
                waitHandle.Set();
            });

            var message = new MessageMock(queryMessage, Property.Create("replyto", replyQueueName));
            dataBus.SimulateQueuePost(requestQueueName, message);

            // wait for the post 5 seconds
            waitHandle.WaitOne(TimeSpan.FromSeconds(5));

            Assert.IsTrue(acknowledged, $"The monitor did not post a proper reply message in a 5 seconds time span!");
        }
    }

    class Logger : ILogger
    {
        public void Log(string category, string message)
        {
            LogCalls++;
        }

        public int LogCalls { get; set; }
    }

    /// <summary>
    /// Queries are processed by replying with a single instance appending the passed headAppend string
    /// </summary>
    class QueryServiceMock : INotamQueryService
    {
        private string _headAppend;

        public QueryServiceMock(string headAppend)
        {
            _headAppend = headAppend;
        }

        public IEnumerable<string> ExecuteQuery(string query)
        {
            yield return $"{_headAppend}{query}";
        }
    }
}

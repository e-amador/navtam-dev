﻿namespace NavCanada.Services.Api.Diagnostics.Data
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Threading.Tasks;

    using NavCanada.Services.Api.Diagnostics.Data.Contracts;

    public class Repository<T> : IRepository<T> where T : class
    {
        public Repository(DbContext dbContext)
        {
            if( dbContext == null )
                throw new NullReferenceException("DbContext can't be null reference at this point");
            DbContext = dbContext;

            DbSet = DbContext.Set<T>();
        }

        protected DbContext DbContext { get; set; }

        protected IDbSet<T> DbSet
        {
            get; 
            set;
        }

        public IQueryable<T> GetAll()
        {
            return DbSet;
        }
        public Task<List<T>> GetAllAsync()
        {
            return DbSet.ToListAsync();
        }
        public T GetById(object id)
        {
            //TODO: Verify what should be better here (consider always ID as int or Guid)
            return DbSet.Find(id);
        }
        public void Add(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Added;
            }
            else
            {
                DbSet.Add(entity);
            }
        }

        public void Update(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }
            dbEntityEntry.State = EntityState.Modified;
        }

        public void Delete(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Deleted)
            {
                dbEntityEntry.State = EntityState.Deleted;
            }
            else
            {
                DbSet.Attach(entity);
                DbSet.Remove(entity);
            }
        }

        public void Delete(int id)
        {
            var entity = GetById(id);
            if (entity == null) 
                return; // not found; assume already deleted.
            
            Delete(entity);
        }
    }
}

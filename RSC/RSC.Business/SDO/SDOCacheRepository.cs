﻿using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Domain.Model.Entitities;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using RunwayAttributes = NavCanada.Core.Proxies.AeroRdsProxy.Model.Runway;

namespace RSC.Business.SDO
{
    public class SDOCacheRepository : ISDORepository
    {
        public SDOCacheRepository(IUow uow)
        {
            _uow = uow;
        }

        public IAerodrome GetAerodrome(string designator)
        {
            var repo = _uow.SdoCacheRepo;

            var sdoCache = repo.GetAerodromeByDesignator(designator);
            if (sdoCache == null)
                return null;

            return new Aerodrome(sdoCache, repo.GetAerodromeRunways(sdoCache.Id).Select(GetRunwayAttributes));
        }

        public string[] GetForbiddenAftnWords()
        {
            var words = _uow.ConfigurationValueRepo.GetValue("NOTAM", "NOTAMForbiddenWords");
            return words.Split('|');
        }

        static RunwayAttributes GetRunwayAttributes(SdoCache runway)
        {
            return JsonConvert.DeserializeObject<RunwayAttributes>(runway.Attributes);
        }

        readonly IUow _uow;
    }

    class Aerodrome : IAerodrome
    {
        public Aerodrome(SdoCache aerodrome, IEnumerable<RunwayAttributes> runways)
        {
            _aerodrome = aerodrome;
            _runways = runways.Select(ConvertRunway).ToList();
        }

        public string Id => _aerodrome.Id;

        public string Designator => _aerodrome.Designator;

        public IEnumerable<IRunway> Runways => _runways;

        public bool ContainsRunway(string designator) => _runways.Any(rwy => rwy.Designator == designator);

        public IRunway GetRunway(string designator) => _runways.FirstOrDefault(rwy => rwy.Designator == designator);

        static IRunway ConvertRunway(RunwayAttributes runway) => new Runway(runway);

        readonly SdoCache _aerodrome;
        readonly List<IRunway> _runways;
    }

    class Runway : IRunway
    {
        public Runway(RunwayAttributes runway)
        {
            _runway = runway;
        }

        public string Id => _runway.RunwayId;

        public string Designator => _runway.Designator;

        public int Length => int.Parse(_runway.RunwayLengthInFeet ?? "0");

        public int Width => int.Parse(_runway.RunwayWidthInFeet ?? "0");

        readonly RunwayAttributes _runway;
    }
}
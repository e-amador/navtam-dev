﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    using NavCanada.Core.Domain.Model.Enums;

    public class AerodromeDisseminationCategoryViewModel
    {
        public int Id { get; set; }

        public string AhpMid { get; set; }

        public string AhpCodeId { get; set; }

        public string AhpName { get; set; }

        public DisseminationCategory DisseminationCategory { get; set; }

       
    }
}
﻿namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    using Contracts.Repositories;
    using Domain.Model.Entitities;
    using Domain.Model.Enums;

    public class PoisonMtoRepo : Repository<PoisonMto>, IPoisonMtoRepo
    {
        public PoisonMtoRepo(DbContext dbContext)
            : base(dbContext)
        {
        }

        public List<PoisonMto> GetAllMessagesToRetry()
        {
            var since = DateTime.UtcNow.AddDays(-2); // two days old messages
            return DbSet
                .Where(e =>
                    e.MessageType == MessageType.WorkFlowTask ||
                    e.MessageType == MessageType.ExternalServicesTask ||
                    e.MessageType == MessageType.NotificationTask && 
                    e.ReportTime >= since ) 
                .ToList();
        }
    }
}

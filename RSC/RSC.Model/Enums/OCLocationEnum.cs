﻿namespace RSC.Model.Enums
{
    /// <summary>
    /// The location of the condition
    /// </summary>
    public enum OCLocationEnum : int
    {
        AlongInsideRedl = 127, // LE LONG INTERIEUR REDL
        AlongInsideRwyEdge = 128, // LE LONG INTERIEUR DU BORD DE PISTE
        AlongClearedWidth = 129, // LE LONG DE LA LARGEUR DEGAÉE
        FMCL = 134, // FM CL
    }
}
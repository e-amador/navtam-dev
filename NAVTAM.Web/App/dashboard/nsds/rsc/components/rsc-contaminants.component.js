"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RscContaminantsComponent = void 0;
var core_1 = require("@angular/core");
var angular_l10n_1 = require("angular-l10n");
var forms_1 = require("@angular/forms");
var rsc_service_1 = require("../rsc.service");
var toastr_service_1 = require("../../../../common/toastr.service");
//declare var $: any;
var RscContaminantsComponent = /** @class */ (function () {
    function RscContaminantsComponent(fb, translation, rscService, toastr) {
        this.fb = fb;
        this.translation = translation;
        this.rscService = rscService;
        this.toastr = toastr;
        this.mainForm = null;
        this.isReadOnly = false;
        this.isAverage = false;
        this.model = null;
        this.isCalculatingCC = false;
        this.isFirstContaminantDepthEnabled = false;
        this.isSecondContaminantDepthEnabled = false;
        this.isFirstContaminantCoverageEnabled = false;
        this.isSecondContaminantCoverageEnabled = false;
        this.isFinalRwyCCDisable = false;
        this.contaminantOptions = {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('ContaminantsComponent.SelectContaminant'),
                contaminatId: '-1',
                needDepth: false,
                temperature: 0,
            }
        };
        this.depthOptions = {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('ContaminantsComponent.SelectDepth')
            }
        };
        this.coefficientsOptions = {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('ContaminantsComponent.SelectCoefficient')
            }
        };
        this.coverageOptions = {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('ContaminantsComponent.SelectCoverage')
            }
        };
        this.rwyCCOptions = {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('ContaminantsComponent.SelectRwyCC')
            }
        };
        this.rwyCCActionOptions = {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('ContaminantsComponent.SelectRwyCCAction')
            }
        };
        this.intervalHandler = null;
    }
    RscContaminantsComponent.prototype.ngOnInit = function () {
        this.isFormLoadingData = false;
        this.rwyContaminants = this.model.parent.contaminants;
        this.depths = this.model.parent.depths;
        this.coefficients = this.model.parent.coefficients;
        this.coverages = this.model.parent.coverages;
        this.rwyCC = this.model.parent.conditionCodes;
        this.rwyCCAction = this.model.parent.conditionCodeAction;
        if (!this.isAverage) {
            this.rwyContaminants = this.rwyContaminants.filter(function (c) { return c.id !== '29'; });
        }
        this.configureReactiveForm();
        this.setIntervalCalculateCC();
    };
    RscContaminantsComponent.prototype.ngOnChanges = function () {
    };
    RscContaminantsComponent.prototype.ngOnDestroy = function () {
        if (this.intervalHandler) {
            window.clearInterval(this.intervalHandler);
        }
    };
    RscContaminantsComponent.prototype.setIntervalCalculateCC = function () {
        var self = this;
        this.intervalHandler = window.setInterval(function () {
            if (self.isFormLoadingData)
                return;
            if (self.isCalculateCCDisabled() === false) {
                self.calculateRwyCC();
            }
            else {
                self.clearCalculatedCC();
            }
        }, 3000);
    };
    RscContaminantsComponent.prototype.clearCalculatedCC = function () {
        if (this.model.data.rwyCCCalculated !== '') {
            this.model.data.rwyCCCalculated = '';
            var ctrl = this.frmGroup.get('rwyCCCalculated');
            if (ctrl.value !== this.model.data.rwyCCCalculated) {
                ctrl.setValue(this.model.data.rwyCCCalculated);
                ctrl.markAsDirty();
            }
        }
    };
    RscContaminantsComponent.prototype.isCalculateCCDisabled = function () {
        var _this = this;
        var cont = this.rwyContaminants.find(function (x) { return x.id === _this.model.data.firstContaminant.id; });
        if (!cont) {
            this.clearCalculatedCC();
            return true;
        }
        if (+this.model.data.firstContaminant.coverage < 10) {
            this.clearCalculatedCC();
            return true;
        }
        if (cont.needDepth) {
            if (+this.model.data.firstContaminant.depth < 0) {
                this.clearCalculatedCC();
                return true;
            }
        }
        cont = this.rwyContaminants.find(function (x) { return x.id === _this.model.data.secondContaminant.id; });
        if (cont) {
            if (+this.model.data.secondContaminant.coverage < 10) {
                this.clearCalculatedCC();
                return true;
            }
            if (cont.needDepth) {
                if (+this.model.data.secondContaminant.depth < 0) {
                    this.clearCalculatedCC();
                    return true;
                }
            }
        }
        return false;
    };
    RscContaminantsComponent.prototype.calculateRwyCC = function () {
        var _this = this;
        if (this.isCalculatingCC)
            return;
        this.isCalculatingCC = true;
        var data = this.exportClearedSurfaceThirdData();
        this.rscService.calculateRwyCC(data).subscribe(function (res) {
            _this.model.data.rwyCCCalculated = res.nesCalculatedRwyCC;
            _this.isCalculatingCC = false;
            var ctrl = _this.frmGroup.get('rwyCCCalculated');
            if (ctrl.value !== _this.model.data.rwyCCCalculated) {
                ctrl.setValue(_this.model.data.rwyCCCalculated);
                ctrl.markAsDirty();
            }
        }, function (error) {
            _this.isCalculatingCC = false;
            var msg = _this.translation.translate('ContaminantsComponent.ErrorCalculatingRWYCC');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    RscContaminantsComponent.prototype.calculateRwyCondCode = function () {
        var _this = this;
        if (this.isCalculatingCC)
            return;
        this.isCalculatingCC = true;
        var data = this.exportRwyCCInputData();
        this.rscService.calculateRwyCondCode(data).subscribe(function (res) {
            _this.model.data.rwyCCCalculated = res;
            _this.isCalculatingCC = false;
            var ctrl = _this.frmGroup.get('rwyCCCalculated');
            if (ctrl.value !== _this.model.data.rwyCCCalculated) {
                ctrl.setValue(_this.model.data.rwyCCCalculated);
                ctrl.markAsDirty();
            }
        }, function (error) {
            _this.isCalculatingCC = false;
            var msg = _this.translation.translate('ContaminantsComponent.ErrorCalculatingRWYCC');
            _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
        });
    };
    RscContaminantsComponent.prototype.isCalendarActive = function () {
        return true;
    };
    RscContaminantsComponent.prototype.onContaminantChanged = function (groupName, e) {
        if (e.value) {
            var contaminantId_1 = e.value;
            var control = groupName === 'first'
                ? this.frmGroup.get('grpFirstContaminant.contaminantId')
                : this.frmGroup.get('grpSecondContaminant.contaminantId');
            if (this.isFormLoadingData) {
                contaminantId_1 = control.value;
            }
            else {
                if (contaminantId_1 !== control.value) {
                    if (contaminantId_1 === '00') {
                        contaminantId_1 = '-1';
                        if (groupName === 'first') {
                            this.resetFirstContaminant();
                            this.isFirstContaminantCoverageEnabled = false;
                            this.isFirstContaminantDepthEnabled = false;
                        }
                        else {
                            this.resetSecondContaminant();
                            this.isSecondContaminantCoverageEnabled = false;
                            this.isSecondContaminantDepthEnabled = false;
                        }
                    }
                    else {
                        control.patchValue(contaminantId_1);
                        control.markAsDirty();
                        var contaminant = this.rwyContaminants.find(function (x) { return x.id === contaminantId_1; });
                        this.model.data.temperature = contaminant.temperature;
                        if (groupName === 'first') {
                            this.model.data.firstContaminant.contaminantId = contaminant.contaminatId;
                            this.model.data.firstContaminant.id = contaminant.id;
                            this.isFirstContaminantCoverageEnabled = true;
                            if (contaminant.needDepth) {
                                this.isFirstContaminantDepthEnabled = true;
                            }
                            else {
                                this.isFirstContaminantDepthEnabled = false;
                                var depthCtrl = this.frmGroup.get('grpFirstContaminant.depth');
                                depthCtrl.setValue('-1');
                            }
                        }
                        else {
                            this.model.data.secondContaminant.contaminantId = contaminant.contaminatId;
                            ;
                            this.model.data.secondContaminant.id = contaminant.id;
                            this.isSecondContaminantCoverageEnabled = true;
                            if (contaminant.needDepth) {
                                this.isSecondContaminantDepthEnabled = true;
                            }
                            else {
                                this.isSecondContaminantDepthEnabled = false;
                                var depthCtrl = this.frmGroup.get('grpSecondContaminant.depth');
                                depthCtrl.setValue('-1');
                            }
                        }
                    }
                }
            }
        }
    };
    RscContaminantsComponent.prototype.onDepthChanged = function (groupName, e) {
        if (e.value) {
            var depthId = e.value;
            if (groupName === 'first') {
                var control = this.frmGroup.get('grpFirstContaminant.depth');
                if (this.isFormLoadingData) {
                    depthId = control.value;
                }
                else {
                    if (depthId !== control.value) {
                        control.setValue(depthId);
                        control.markAsDirty();
                        this.model.data.firstContaminant.depth = depthId;
                    }
                }
            }
            else {
                var control = this.frmGroup.get('grpSecondContaminant.depth');
                if (this.isFormLoadingData) {
                    depthId = control.value;
                }
                else {
                    if (depthId !== control.value) {
                        control.setValue(depthId);
                        control.markAsDirty();
                        this.model.data.secondContaminant.depth = depthId;
                    }
                }
            }
        }
    };
    RscContaminantsComponent.prototype.onCoverageChanged = function (groupName, e) {
        if (e.value) {
            var coverageId = e.value;
            var control = groupName === 'first'
                ? this.frmGroup.get('grpFirstContaminant.coverage')
                : this.frmGroup.get('grpSecondContaminant.coverage');
            if (this.isFormLoadingData) {
                coverageId = control.value;
            }
            else {
                if (coverageId !== control.value) {
                    control.setValue(coverageId);
                    control.markAsDirty();
                    if (groupName === 'first')
                        this.model.data.firstContaminant.coverage = coverageId;
                    else
                        this.model.data.secondContaminant.coverage = coverageId;
                }
            }
        }
    };
    RscContaminantsComponent.prototype.onCoefficientChanged = function (e) {
        if (e.value) {
            var coefficientId = e.value;
            var control = this.frmGroup.get('grpCrfi.coefficient');
            if (this.isFormLoadingData) {
                coefficientId = control.value;
            }
            else {
                if (coefficientId !== control.value) {
                    control.setValue(coefficientId);
                    control.markAsDirty();
                    this.model.data.coefficient = coefficientId;
                }
            }
        }
    };
    RscContaminantsComponent.prototype.onRwyCCChange = function (e) {
        if (e.value) {
            var rwyCCId = e.value;
            var control = this.frmGroup.get('rwyCC');
            if (this.isFormLoadingData) {
                rwyCCId = control.value;
            }
            else {
                if (rwyCCId !== control.value) {
                    control.setValue(rwyCCId);
                    control.markAsDirty();
                    this.model.data.rwyCC = rwyCCId;
                }
            }
        }
    };
    RscContaminantsComponent.prototype.onRwyCCActionChange = function (e) {
        if (e.value) {
            var rwyCCActionId = e.value;
            var control = this.frmGroup.get('rwyCCAction');
            if (this.isFormLoadingData) {
                rwyCCActionId = control.value;
            }
            else {
                if (rwyCCActionId !== control.value) {
                    control.setValue(rwyCCActionId);
                    control.markAsDirty();
                    this.model.data.rwyCCActionSelected = rwyCCActionId;
                    this.isFinalRwyCCDisable = (this.model.data.rwyCCActionSelected === '0');
                    this.resetFinalRwyCCComboValues(rwyCCActionId);
                }
            }
        }
    };
    RscContaminantsComponent.prototype.resetFinalRwyCCComboValues = function (value) {
        var _this = this;
        this.rwyCC = [
            { id: '0', text: '0' },
            { id: '1', text: '1' },
            { id: '2', text: '2' },
            { id: '3', text: '3' },
            { id: '4', text: '4' },
            { id: '5', text: '5' },
            { id: '6', text: '6' }
        ];
        if (value === '0') {
            this.rwyCC = this.rwyCC.filter(function (c) { return +c.id === +_this.model.data.rwyCCCalculated; });
            this.model.data.rwyCC = this.model.data.rwyCCCalculated;
            this.onRwyCCChange({ value: this.model.data.rwyCC });
        }
        else if (value === '1') {
            this.rwyCC = this.rwyCC.filter(function (c) { return +c.id < +_this.model.data.rwyCCCalculated; });
            this.model.data.rwyCC = '-1';
            this.onRwyCCChange({ value: this.model.data.rwyCC });
        }
        else if (value === '2') {
            this.rwyCC = this.rwyCC.filter(function (c) { return +c.id > +_this.model.data.rwyCCCalculated; });
            this.model.data.rwyCC = '-1';
            this.onRwyCCChange({ value: this.model.data.rwyCC });
        }
    };
    RscContaminantsComponent.prototype.validateContaminantId = function (groupName) {
        var fc = groupName === 'first'
            ? this.frmGroup.get('grpFirstContaminant.contaminantId')
            : this.frmGroup.get('grpSecondContaminant.contaminantId');
        return fc.value !== '-1' || this.frmGroup.pristine;
    };
    RscContaminantsComponent.prototype.validateDepth = function (groupName) {
        var fc = groupName === 'first'
            ? this.frmGroup.get('grpFirstContaminant.depth')
            : this.frmGroup.get('grpSecondContaminant.depth');
        var contaminant = groupName === 'first'
            ? this.frmGroup.get('grpFirstContaminant.contaminantId')
            : this.frmGroup.get('grpSecondContaminant.contaminantId');
        if (contaminant.value !== '-1') {
            var needDepth = this.rwyContaminants.find(function (x) { return x.id === contaminant.value; });
            if (needDepth && needDepth.needDepth) {
                return fc.value !== '-1' || this.frmGroup.pristine;
            }
        }
        return true;
    };
    RscContaminantsComponent.prototype.validateCoverage = function (groupName) {
        return true;
    };
    RscContaminantsComponent.prototype.validateCoefficient = function () {
        var fc = this.frmGroup.get('grpCrfi.coefficient');
        return !fc.errors || this.frmGroup.pristine;
    };
    RscContaminantsComponent.prototype.validateKeyPressNumber = function (evt) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!this.isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
        }
    };
    RscContaminantsComponent.prototype.resetFirstContaminant = function () {
        this.model.data.firstContaminant.contaminantId = '-1';
        this.model.data.firstContaminant.id = '-1';
        this.model.data.firstContaminant.depth = '-1';
        this.model.data.firstContaminant.coverage = '-1';
        var ctrl = this.$ctrl('grpFirstContaminant.contaminantId');
        ctrl.patchValue(this.model.data.firstContaminant.contaminantId);
        ctrl = this.$ctrl('grpFirstContaminant.depth');
        ctrl.patchValue(this.model.data.firstContaminant.depth);
        ctrl = this.$ctrl('grpFirstContaminant.coverage');
        ctrl.patchValue(this.model.data.firstContaminant.coverage);
        this.isFirstContaminantCoverageEnabled = false;
        this.isFirstContaminantDepthEnabled = false;
    };
    RscContaminantsComponent.prototype.resetSecondContaminant = function () {
        this.model.data.secondContaminant.contaminantId = '-1';
        this.model.data.secondContaminant.id = '-1';
        this.model.data.secondContaminant.depth = '-1';
        this.model.data.secondContaminant.coverage = '-1';
        var ctrl = this.$ctrl('grpSecondContaminant.contaminantId');
        ctrl.patchValue(this.model.data.secondContaminant.contaminantId);
        ctrl = this.$ctrl('grpSecondContaminant.depth');
        ctrl.patchValue(this.model.data.secondContaminant.depth);
        ctrl = this.$ctrl('grpSecondContaminant.coverage');
        ctrl.patchValue(this.model.data.secondContaminant.coverage);
        this.isSecondContaminantCoverageEnabled = false;
        this.isSecondContaminantDepthEnabled = false;
    };
    RscContaminantsComponent.prototype.$ctrl = function (name) {
        return this.frmGroup.get(name);
    };
    RscContaminantsComponent.prototype.configureReactiveForm = function () {
        var frmArrControls = this.mainForm.controls['frmContaminantArr'];
        this.frmGroup = this.fb.group({
            grpFirstContaminant: this.fb.group({
                contaminantId: [this.model.data.firstContaminant.contaminantId || '-1', [validContaminantValidator]],
                depth: this.model.data.firstContaminant.depth,
                coverage: this.model.data.firstContaminant.coverage
            }),
            grpSecondContaminant: this.fb.group({
                contaminantId: this.model.data.secondContaminant.contaminantId || '-1',
                depth: this.model.data.secondContaminant.depth,
                coverage: this.model.data.secondContaminant.coverage
            }),
            rwyCC: { value: this.model.data.rwyCC, disabled: this.isReadOnly },
            rwyCCCalculated: { value: this.model.data.rwyCCCalculated, disabled: true },
            rwyCCAction: { value: this.model.data.rwyCCActionSelected, disabled: true },
            grpCrfi: this.fb.group({
                coefficient: this.model.data.coefficient,
                temperature: this.model.data.temperature,
            }),
        });
        frmArrControls.push(this.frmGroup);
    };
    RscContaminantsComponent.prototype.isANumber = function (str) {
        return !/\D/.test(str);
    };
    //Export Functions
    RscContaminantsComponent.prototype.isFormValid = function () {
        return true;
    };
    RscContaminantsComponent.prototype.refreshValues = function (newValues) {
        if (!newValues)
            return;
        this.model.data.rwyCCCalculated = newValues.data.rwyCCCalculated;
        this.onContaminantChanged('first', { value: newValues.data.firstContaminant.contaminantId });
        this.onDepthChanged('first', { value: newValues.data.firstContaminant.depth });
        this.onCoverageChanged('first', { value: newValues.data.firstContaminant.coverage });
        this.onContaminantChanged('second', { value: newValues.data.secondContaminant.contaminantId });
        this.onDepthChanged('second', { value: newValues.data.secondContaminant.depth });
        this.onCoverageChanged('second', { value: newValues.data.secondContaminant.coverage });
        this.onCoefficientChanged({ value: newValues.data.coefficient });
        this.onRwyCCActionChange({ value: newValues.data.rwyCCActionSelected });
        this.onRwyCCChange({ value: newValues.data.rwyCC });
    };
    RscContaminantsComponent.prototype.processClean = function () {
        var oldId = this.model.id;
        this.model = this.model.parent.createContaminantModel(oldId);
        this.model.data.firstContaminant.contaminantId = "00";
        this.model.data.secondContaminant.contaminantId = "00";
        this.model.data.firstContaminant.id = "00";
        this.model.data.secondContaminant.id = "00";
        this.model.data.rwyCCCalculated = '';
        this.onContaminantChanged('first', { value: this.model.data.firstContaminant.id });
        this.onDepthChanged('first', { value: this.model.data.firstContaminant.depth });
        this.onCoverageChanged('first', { value: this.model.data.firstContaminant.coverage });
        this.onContaminantChanged('second', { value: this.model.data.secondContaminant.id });
        this.onDepthChanged('second', { value: this.model.data.secondContaminant.depth });
        this.onCoverageChanged('second', { value: this.model.data.secondContaminant.coverage });
        this.onCoefficientChanged({ value: this.model.data.coefficient });
        this.onRwyCCActionChange({ value: this.model.data.rwyCCActionSelected });
        this.onRwyCCChange({ value: this.model.data.rwyCC });
        this.clearCalculatedCC();
    };
    RscContaminantsComponent.prototype.exportComponentData = function () {
        var data = {
            id: this.model.id,
            clearedSurfaceThird: this.exportClearedSurfaceThirdData(),
            rwyCCCalculated: this.model.data.rwyCCCalculated,
            rwyCCActionSelected: this.model.data.rwyCCActionSelected,
            rwyCC: this.model.data.rwyCC,
        };
        return data;
    };
    RscContaminantsComponent.prototype.exportComponentInternalData = function () {
        var data = {
            parent: this.model.parent,
            id: this.model.id,
            data: {
                firstContaminant: {
                    contaminantId: this.model.data.firstContaminant.contaminantId,
                    id: this.model.data.firstContaminant.id,
                    depth: this.model.data.firstContaminant.depth,
                    coverage: this.model.data.firstContaminant.coverage,
                },
                secondContaminant: {
                    contaminantId: this.model.data.secondContaminant.contaminantId,
                    id: this.model.data.firstContaminant.id,
                    depth: this.model.data.secondContaminant.depth,
                    coverage: this.model.data.secondContaminant.coverage,
                },
                rwyCC: this.model.data.rwyCC,
                rwyCCCalculated: this.model.data.rwyCCCalculated,
                rwyCCActionSelected: this.model.data.rwyCCActionSelected,
                coefficient: this.model.data.coefficient,
                temperature: this.model.data.temperature,
            }
        };
        return data;
    };
    RscContaminantsComponent.prototype.exportClearedSurfaceData = function () {
        var _this = this;
        if (!this.isAverage)
            return null;
        var contams = [];
        var cont = this.rwyContaminants.find(function (x) { return x.id === _this.model.data.firstContaminant.id; });
        var depth = {
            contamInches: (cont.needDepth) ? +this.model.data.firstContaminant.depth : 0,
            contamTrace: false,
        };
        var fCont = {
            hasDepdth: cont.needDepth,
            contamCoverage: +this.model.data.firstContaminant.coverage,
            contamDepthI: depth,
            contamType: (cont.needDepth) ? +cont.contaminatId : null,
            contamTypeNoD: (!cont.needDepth) ? +cont.contaminatId : null,
        };
        contams.push(fCont);
        cont = this.rwyContaminants.find(function (x) { return x.id === _this.model.data.secondContaminant.id; });
        if (cont) {
            depth = {
                contamInches: (cont.needDepth) ? +this.model.data.secondContaminant.depth : 0,
                contamTrace: false,
            };
            var sCont = {
                hasDepdth: cont.needDepth,
                contamCoverage: +this.model.data.secondContaminant.coverage,
                contamDepthI: depth,
                contamType: (cont.needDepth) ? +cont.contaminatId : null,
                contamTypeNoD: (!cont.needDepth) ? +cont.contaminatId : null,
            };
            contams.push(sCont);
        }
        var data = {
            contaminants: contams,
            rwyCC: this.model.data.rwyCC,
        };
        return data;
    };
    RscContaminantsComponent.prototype.exportClearedSurfaceThirdData = function () {
        var _this = this;
        if (this.isAverage)
            return null;
        var contams = [];
        var cont = this.rwyContaminants.find(function (x) { return x.id === _this.model.data.firstContaminant.id; });
        var depth = {
            contamInches: (cont.needDepth) ? +this.model.data.firstContaminant.depth : 0,
            contamTrace: false,
        };
        var fCont = {
            hasDepdth: cont.needDepth,
            contamCoverage: +this.model.data.firstContaminant.coverage,
            contamDepthI: depth,
            contamType: (cont.needDepth) ? +cont.contaminatId : null,
            contamTypeNoD: (!cont.needDepth) ? +cont.contaminatId : null,
        };
        contams.push(fCont);
        cont = this.rwyContaminants.find(function (x) { return x.id === _this.model.data.secondContaminant.id; });
        if (cont) {
            depth = {
                contamInches: (cont.needDepth) ? +this.model.data.secondContaminant.depth : 0,
                contamTrace: false,
            };
            var sCont = {
                hasDepdth: cont.needDepth,
                contamCoverage: +this.model.data.secondContaminant.coverage,
                contamDepthI: depth,
                contamType: (cont.needDepth) ? +cont.contaminatId : null,
                contamTypeNoD: (!cont.needDepth) ? +cont.contaminatId : null,
            };
            contams.push(sCont);
        }
        //Prepare RwyFriction data
        var rwyFriction = {
            rfObsTime: null,
            device: 130,
            temperature: this.model.data.temperature.toString(),
            unreliable: false,
            frictionCoefficients: +this.model.data.coefficient
        };
        var portion = 0;
        if (this.model.id.trim().toUpperCase() === 'TOUCHDOWN') {
            //Touchdown
            portion = 1;
        }
        else if (this.model.id.trim().toUpperCase() === 'MIDPOINT') {
            //Midpoint
            portion = 2;
        }
        else if (this.model.id.trim().toUpperCase() === 'ROLLOUT') {
            //Rollout
            portion = 3;
        }
        //Prepare ClearedSurfaceThird data
        var data = {
            portion: portion,
            rwyCC: this.model.data.rwyCC,
            nesCalculatedRwyCC: this.model.data.rwyCCCalculated,
            frictionCoefficient: +this.model.data.coefficient,
            rwyFrictionThird: rwyFriction,
            contaminants: contams
        };
        return data;
    };
    RscContaminantsComponent.prototype.exportRwyCCInputData = function () {
        var data = {
            firstContaminant: {
                contaminantId: this.model.data.firstContaminant.contaminantId,
                id: this.model.data.firstContaminant.id,
                depth: this.model.data.firstContaminant.depth,
                coverage: this.model.data.firstContaminant.coverage,
            },
            secondContaminant: {
                contaminantId: this.model.data.secondContaminant.contaminantId,
                id: this.model.data.secondContaminant.id,
                depth: this.model.data.secondContaminant.depth,
                coverage: this.model.data.secondContaminant.coverage,
            },
            temperature: 0,
        };
        return data;
    };
    __decorate([
        core_1.Input('form'),
        __metadata("design:type", forms_1.FormGroup)
    ], RscContaminantsComponent.prototype, "mainForm", void 0);
    __decorate([
        core_1.Input('isReadOnly'),
        __metadata("design:type", Boolean)
    ], RscContaminantsComponent.prototype, "isReadOnly", void 0);
    __decorate([
        core_1.Input('isAverage'),
        __metadata("design:type", Boolean)
    ], RscContaminantsComponent.prototype, "isAverage", void 0);
    __decorate([
        core_1.Input('model'),
        __metadata("design:type", Object)
    ], RscContaminantsComponent.prototype, "model", void 0);
    RscContaminantsComponent = __decorate([
        core_1.Component({
            selector: 'rsc-contaminants',
            templateUrl: '/app/dashboard/nsds/rsc/components/rsc-contaminants.component.html'
        }),
        __param(3, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [forms_1.FormBuilder,
            angular_l10n_1.TranslationService,
            rsc_service_1.RscService, Object])
    ], RscContaminantsComponent);
    return RscContaminantsComponent;
}());
exports.RscContaminantsComponent = RscContaminantsComponent;
function validContaminantValidator(c) {
    if (c.value === '-1') {
        return { 'required': true };
    }
    return null;
}
//# sourceMappingURL=rsc-contaminants.component.js.map
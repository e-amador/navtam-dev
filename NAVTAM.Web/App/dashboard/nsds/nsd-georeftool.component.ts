﻿import { Component, Input, OnChanges, AfterViewInit, Inject } from '@angular/core'
import { WindowRef } from '../../common/windowRef.service';
import { IPointsViewModel, InternationalAerodromeDto } from '../shared/data.model';
import { DataService } from '../shared/data.service';
import { TOASTR_TOKEN, Toastr } from '../../common/toastr.service';
import { LocationService } from '../shared/location.service';
import { TranslationService } from 'angular-l10n';

@Component({
    selector: 'nsd-georeftool',
    templateUrl: '/app/dashboard/nsds/nsd-georeftool.component.html'
})
export class NsdGeoRefToolComponent implements OnChanges, AfterViewInit {
    @Input() model: any;

    mapHealthy: boolean = false;
    leafletgeomap: any;

    calcPoint: string = "";
    calcRadius: string = "";
    calcError: string = "";
    locationPoints: string = "";
    locationPoint: string = "";
    locationRadius: string = "";
    entryDataPoints: boolean = true;

    constructor(private windowRef: WindowRef,
        private dataService: DataService,
        private locationService: LocationService,
        public translation: TranslationService,
        @Inject(TOASTR_TOKEN) private toastr: Toastr) {

    }

    ngOnInit() {
        this.initMap();
        this.mapHealthCheck();
        this.leafletgeomap.resetMapView();
    }

    ngAfterViewInit() {
        if (this.leafletgeomap) {
            this.leafletgeomap.resizeGeoMapForModal();
        }
    } 

    ngOnDestroy() {
        this.disposeMap();
    }

    ngOnChanges() {
    }

    entryDataChange() {
        this.entryDataPoints = !this.entryDataPoints;
    }

    copyCalcPointText() {
        this.copyToClipBoard(this.calcPoint);
    }

    copyCalcRadiusText() {
        this.copyToClipBoard(this.calcRadius);
    }

    private copyToClipBoard(text) {
        const el = document.createElement('textarea');
        el.value = text;
        el.setAttribute('readonly', '');
        el.style.position = 'absolute';
        el.style.left = '-9999px';
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    }

    calculateGeoRef() {
        if (this.entryDataPoints === true) {
            this.calculateGeo();
        } else {
            this.calculateGeoByPoint();
        }
    }

    private calculateGeo() {
        if (this.locationPoints.length > 0) {
            this.calcError = "";
            this.calcPoint = "";
            this.calcRadius = "";

            var data: IPointsViewModel = {
                points: this.convertEntryToPoints(this.locationPoints),
                calculatedPoint: "",
                calculatedRadius: 5,
                error: "",
                pointRadius: 0,
                internationalAerodromesDto: [],
            };

            this.dataService.calculateGeoRef(data)
                .subscribe(response => {
                    this.calcError = response.error;
                    this.leafletgeomap.resetMapView();
                    if (this.calcError.length === 0) {
                        this.calcPoint = response.calculatedPoint;
                        this.calcRadius = response.calculatedRadius.toString();
                        this.renderMap(response.calculatedPoint, response.calculatedRadius);
                        this.renderGeoPoints(response.points);
                        this.renderAerodromes(response.internationalAerodromesDto);
                        this.renderAerodromesPopup(response.internationalAerodromesDto);
                    }
                 }, error => {
                 });
        }
    }

    private calculateGeoByPoint() {
        if (this.locationPoint.trim().length > 0 && this.locationRadius.trim().length > 0) {
            this.calcError = "";
            this.calcPoint = "";
            this.calcRadius = "";

            var data: IPointsViewModel = {
                points: this.convertEntryToPoints(this.locationPoint),
                calculatedPoint: "",
                calculatedRadius: 5,
                error: "",
                pointRadius: +this.locationRadius,
                internationalAerodromesDto: [],
            };

            if (data.points.length === 1) {
                this.dataService.calculateGeoRefByPoint(data)
                    .subscribe(response => {
                        this.calcError = response.error;
                        this.leafletgeomap.resetMapView();
                        if (this.calcError.length === 0) {
                            this.calcPoint = response.calculatedPoint;
                            this.calcRadius = response.calculatedRadius.toString();
                            this.renderMap(response.calculatedPoint, response.calculatedRadius);
                            this.renderAerodromes(response.internationalAerodromesDto);
                            this.renderAerodromesPopup(response.internationalAerodromesDto);
                        }
                    }, error => {
                    });
            }
            else {
                //Error
            }
        }
    }

    public isCalcError(): boolean {
        if (this.calcError && this.calcError.length > 0) return true;
        return false;
    }

    private convertEntryToPoints(loc: string): string[] {
        var result: string[] = [];
        if (loc.length > 0) {
            var line = this.removeNewlines(loc); //loc.replace(/(\r\n|\n|\r)/gm, " ");
            var secPoints = line.split(",");
            secPoints.forEach(obj => {
                result.push(obj.trim().toUpperCase());
            });
        }
        return result;
    }

    private removeNewlines(str): string {
        //remove line breaks from str
        str = str.replace(/\t/g, ' ');
        str = str.toString().trim().replace(/(\r\n|\n|\r)/gm, " ");
        str = str.replace(/\s{2,}/g, ' ');
        return str;
    }

    isGeoPointInputEmpty() {
        if (this.entryDataPoints === true) {
            if (this.locationPoints.length > 0) return false;
            return true;
        } else {
            if (this.locationPoint.trim().length > 0 && this.locationRadius.trim().length > 0) return false;
            return true;
        }
    }

    //Map functions
    public mapAvailable(): boolean {
        return this.mapHealthy;
    }

    private initMap() {
        const winObj = this.windowRef.nativeWindow;
        this.leafletgeomap = winObj['app'].leafletgeomap;

        if (this.leafletgeomap) {
            this.leafletgeomap.initGeoMap();
        }
    }

    public mapHealthCheck() {
        this.dataService.getMapHealth().subscribe(response => {
            if (response) {
                this.mapHealthy = true;
            }
        }, error => {
            this.mapHealthy = false;
            return false;
        });
    }

    public renderMap(point: string, rad: number) {
        const pair = this.locationService.parse(point);
        if (pair) {
            var radP = this.locationService.nmToMeters(rad);
            this.leafletgeomap.addGeoLocationAndRadius(pair.latitude, pair.longitude, radP);
        }
    }

    public renderGeoPoints(points: string[]) {
        this.leafletgeomap.addPolygonPoints(points);
    }

    public renderAerodromes(aerodromesInfo: InternationalAerodromeDto[]) {
        this.leafletgeomap.clearAerodromeLayer();
        if (aerodromesInfo.length > 0) {
            this.leafletgeomap.addAerodromeLocationAndRadius(aerodromesInfo);
        }
    }

    public renderAerodromesPopup(aerodromesInfo: InternationalAerodromeDto[]) {
        this.leafletgeomap.clearAerodromePopupLayer();
        if (aerodromesInfo.length > 0) {
            this.leafletgeomap.addAerodromesPopup(aerodromesInfo);
        }
    }

    public renderCirclePoints(points: string[], centerPoint: string) {
        const pair = this.locationService.parse(centerPoint);
        if (pair) {
            this.leafletgeomap.addCircleFromPoints(points, pair.latitude, pair.longitude);
        }
    }

    public renderPoints(points: string[], self): any {
        points.forEach(function (point) {
            self.leafletgeomap.renderPoints(point.split(' ')[0], point.split(' ')[1]);
        });
    }

    public disposeMap() {
        if (this.leafletgeomap) {
            this.leafletgeomap.disposeGeoMap();
        }
    }
    //End Map functions

    public validateKeyPressNumber(evt: any) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
        }
    }

}

function isANumber(str: string): boolean {
    return !/\D/.test(str);
}
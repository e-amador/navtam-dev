﻿using System;
using System.Data.Entity.Spatial;
using FluentValidation.Results;
using NavCanada.Applications.NotamWiz.Web.Code.Helpers;
using NavCanada.Applications.NotamWiz.Web.Models;
using Westwind.Globalization;
using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.NSDs.CarsClsd.V0R1
{
    using System.Device.Location;
    using System.Threading.Tasks;

    using FluentValidation;
    using Core.Data.NotamWiz.Contracts;
    using NavCanada.Applications.NotamWiz.Web.Code.Extentions;
    using NavCanada.Applications.NotamWiz.Web.Code.GeoTools;


    public class CarsValidator : AbstractValidator<ViewModel>
    {
        private readonly ILog log = LogManager.GetLogger(typeof(CarsValidator));

        private IUow Uow
        {
            get;
            set;
        }


        public override Task<ValidationResult> ValidateAsync(ValidationContext<ViewModel> context)
        {
            var location = DbGeography.FromText(String.Format("POINT ({0} {1})", Controller.Subject.SubjectGeoRefLong, Controller.Subject.SubjectGeoRefLat));
            this.log.Object(new
            {
                Method = "BeWithinDOA",
                Parameters = new
                {
                    location
                }
            });
            var doa = Task.FromResult(Controller.GetUserDoaAsync()).Result;
            if (!GeoDataService.IsLocationWithinDOA(location, doa.Result))
                return Task.FromResult(new ValidationResult(new[] { new ValidationFailure("NSD-Cars", "AD Location is not Within your Domain of Authority.") }));

            return base.ValidateAsync(context);
        }

     

        public CarsValidator(NsdBaseController controller,IDbResWrapper dbRes)
        {

            this.log.Object(new
            {
                Method = "ObstValidator",
                Parameters = new
                {
                    controller
                }
            });
            Controller = controller;
            RuleFor(v => v.SchText).Must(a=> !String.IsNullOrEmpty(a) && !String.IsNullOrWhiteSpace(a)).When(a => a.CondStatusValue == 1).WithMessage(dbRes.T("Invalid schedule.", "NSD-Cars"));
            //RuleFor(v =>v).MustAsync(BeWithinDoa(Controller.Subject)).WithMessage(dbRes.T("AD Location is not Within your Domain of Authority.", "NSD-Cars"));

        }

        private NsdBaseController Controller
        {
            get;
            set;
        }

      
    }
}
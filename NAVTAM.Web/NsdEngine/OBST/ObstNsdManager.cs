﻿using NavCanada.Core.Domain.Model.Entitities;
using System;

namespace NAVTAM.NsdEngine.OBST
{
    public abstract class ObstNsdManager : INsdManager
    {
        public string ComponentName
        {
            get
            {
                return "Obst";
            }
        }

        public abstract string FullyQualifiedModelName { get; }
        public abstract NsdViewModel ToViewModel(NotamProposal2 proposal);
    }
}

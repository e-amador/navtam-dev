"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminModule = exports.initLocalization = exports.LocalizationConfig = void 0;
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var ng2_select2_1 = require("ng2-select2");
var ng2_date_time_picker_1 = require("../common/components/ng2-date-time-picker");
//import { StartupService } from './startup.service';
var angular_l10n_1 = require("angular-l10n");
var index_1 = require("../common/index");
var admin_component_1 = require("./admin.component");
var index_2 = require("./index");
var admin_routes_1 = require("./admin.routes");
//export function startupServiceFactory(startupService: StartupService): Function {
//    return () => startupService.load();
//}
var LocalizationConfig = /** @class */ (function () {
    function LocalizationConfig(locale, translation) {
        this.locale = locale;
        this.translation = translation;
        this.resourceCount = 0;
    }
    LocalizationConfig.prototype.load = function () {
        var _this = this;
        this.locale.addConfiguration()
            .addLanguage('en', 'ltr')
            .addLanguage('fr', 'ltr')
            //.setCookieExpiration(30)
            .defineLanguage(window['app'].cultureInfo)
            .defineCurrency("CAD")
            .defineDefaultLocale('en', 'CA');
        this.locale.init();
        this.translation.addConfiguration()
            .addProvider('./app/resources/admin.locale-');
        var promise = new Promise(function (resolve) {
            _this.translation.translationChanged.subscribe(function () {
                resolve(true);
            });
        });
        this.translation.init();
        return promise;
    };
    LocalizationConfig = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [angular_l10n_1.LocaleService, angular_l10n_1.TranslationService])
    ], LocalizationConfig);
    return LocalizationConfig;
}());
exports.LocalizationConfig = LocalizationConfig;
// AoT compilation requires a reference to an exported function.
function initLocalization(localizationConfig) {
    return function () { return localizationConfig.load(); };
}
exports.initLocalization = initLocalization;
var AdminModule = /** @class */ (function () {
    function AdminModule() {
    }
    AdminModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                http_1.HttpModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                router_1.RouterModule.forRoot(admin_routes_1.adminRoutes),
                ng2_select2_1.Select2Module,
                ng2_date_time_picker_1.DateTimePickerModule,
                index_1.Ng2ICheckModule,
                angular_l10n_1.LocalizationModule.forRoot() // New instance of TranslationService
            ],
            declarations: [
                admin_component_1.AdminComponent,
                //common
                index_1.DatexPipe,
                index_1.DateTimeFormatDirective,
                index_1.ModalTriggerDirective,
                index_1.SimpleModalComponent,
                index_1.ServerPaginationComponent,
                index_2.SidebarComponent,
                index_1.SystemStatusComponent,
                //User,
                index_2.UserListComponent,
                index_2.UserEditComponent,
                index_2.UserNewComponent,
                index_2.UserResetPasswordComponent,
                //Org
                index_2.OrgListComponent,
                index_2.OrgNewComponent,
                index_2.OrgEditComponent,
                //IcaoSubjects
                index_2.IcaoSubjectsListComponent,
                index_2.IcaoSubjectEditComponent,
                index_2.IcaoSubjectNewComponent,
                index_2.IcaoConditionEditComponent,
                index_2.IcaoConditionNewComponent,
                //SeriesAllocation
                index_2.SeriesListComponent,
                index_2.SeriesNewComponent,
                index_2.SeriesEditComponent,
                //AerodromeDisseminationCategory
                index_2.ADCTabListComponent,
                index_2.ADCAddSdoComponent,
                index_2.ADCEditComponent,
                //DOAS
                index_2.DoasListComponent,
                index_2.DoaNewComponent,
                index_2.DoaEditComponent,
                index_2.DoaEditModifyComponent,
                //BilingualRegion
                index_2.BilingualRegionEditComponent,
                //NorthernRegion
                index_2.NorthernRegionEditComponent,
                //NSDs
                index_2.NsdListComponent,
                //GeoRegion
                index_2.GeoRegionListComponent,
                index_2.GeoRegionShowComponent,
                index_2.GeoRegionNewComponent,
                //Config
                index_2.ConfigListComponent,
                index_2.ConfigNewComponent,
                //Checklist
                index_2.ChecklistComponent,
                index_2.SeriesChecklistNewComponent,
                //AeroRDS
                index_2.AerordsComponent,
                //Subscription
                index_2.SubscComponent,
                index_2.SubscEditComponent,
                index_2.SubscCreateComponent,
            ],
            providers: [
                index_2.DataService,
                index_2.MemoryStorageService,
                index_2.RolesResolver,
                index_2.UserModelResolver,
                index_2.OrgModelResolver,
                index_2.SeriesModelResolver,
                index_2.AhpModelResolver,
                index_2.AdcModelResolver,
                index_2.DoaModelResolver,
                index_2.DoaCopyModelResolver,
                index_2.IcaoSubjectModelResolver,
                index_2.IcaoConditionModelResolver,
                index_2.GeoRegionModelResolver,
                index_2.BilingualRegionModelResolver,
                index_2.NorthernRegionModelResolver,
                index_2.ConfigModelResolver,
                index_2.ChecklistModelResolver,
                index_2.SeriesChecklistResolver,
                //StartupService,
                index_2.LocationService,
                index_2.SubscriptionResolver,
                LocalizationConfig,
                {
                    provide: core_1.APP_INITIALIZER,
                    useFactory: initLocalization,
                    deps: [LocalizationConfig],
                    multi: true
                },
                //common
                //{
                //    // Provider for APP_INITIALIZER
                //    provide: APP_INITIALIZER,
                //    useFactory: startupServiceFactory,
                //    deps: [StartupService],
                //    multi: true
                //},
                { provide: index_1.TOASTR_TOKEN, useValue: toastr },
                index_1.XsrfTokenService,
                index_1.WindowRef
            ],
            bootstrap: [admin_component_1.AdminComponent]
        })
    ], AdminModule);
    return AdminModule;
}());
exports.AdminModule = AdminModule;
//# sourceMappingURL=admin.module.js.map
﻿import { Component, OnInit, OnDestroy, Inject } from '@angular/core'
import { FormGroup, FormBuilder, AbstractControl } from '@angular/forms'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';
import { ActivatedRoute, Router } from '@angular/router'
import { DataService } from '../shared/data.service'
import { MemoryStorageService } from '../shared/mem-storage.service'
import { WindowRef } from '../../common/windowRef.service'
import { LocaleService, TranslationService } from 'angular-l10n';

import {
    IQueryOptions,
    IPaging,
    IIcaoSubjectPartial,
    IIcaoSubjectConditionPartial,
    ITableHeaders
} from '../shared/data.model'

declare var $: any;

@Component({
    templateUrl: '/app/admin/icaosubjects/icaosubjects-list.component.html'
})
export class IcaoSubjectsListComponent implements OnInit, OnDestroy {
    icaoSubjects: IIcaoSubjectPartial[];
    icaoConditions: IIcaoSubjectConditionPartial[];
    queryOptions: IQueryOptions;
    conditionQueryOptions: IQueryOptions;
    selectedIcaoSubject: IIcaoSubjectPartial = null;
    selectedIcaoCondition: IIcaoSubjectConditionPartial = null;

    paging: IPaging = {
        currentPage: 1,
        pageSize: 0,
        totalCount: 0,
        totalPages: 0,
    };
    totalPages: number = 0;
    icaoSubjectListForm: FormGroup; 

    itemsPerPageData: Array<any>;
    itemsPerPageStartValue: string;
    loadingData: boolean = false;
    loadingConditions: boolean = false;
    tableSubjectHeaders: ITableHeaders;
    tableConditionHeaders: ITableHeaders;
    leftMenuHeight: string = null;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private fb: FormBuilder,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private route: ActivatedRoute,
        public locale: LocaleService,
        public translation: TranslationService) {

        if (this.winRef.appConfig.ccs)
            this.router.navigate(['/administration']);

        this.tableSubjectHeaders = {
            sorting:
                [
                    { columnName: 'EntiTyCode', direction: 0 },
                    { columnName: 'Name', direction: 0 },
                    { columnName: 'NameFrench', direction: 0 },
                    { columnName: 'Code', direction: 0 },
                    { columnName: 'Scope', direction: 0 },
                    { columnName: 'AllowItemAChange', direction: 0 },
                    { columnName: 'AllowScopeChange', direction: 0 },
                    { columnName: 'AllowSeriesChange', direction: 0 }
                ],
            itemsPerPage:
                [
                    { id: '8', text: '8' },
                    { id: '20', text: '20' },
                    { id: '50', text: '50' },
                    { id: '100', text: '100' },
                    { id: '200', text: '200' },
                ]
        };

        this.tableConditionHeaders = {
            sorting:
                [
                    { columnName: 'Description', direction: 0 },
                    { columnName: 'DescriptionFrench', direction: 0 },
                    { columnName: 'Code', direction: 0 },
                    { columnName: 'Lower', direction: 0 },
                    { columnName: 'Upper', direction: 0 },
                    { columnName: 'Radius', direction: 0 },
                    { columnName: 'Code', direction: 0 },
                    { columnName: 'Traffic', direction: 0 },
                    { columnName: 'Purpose', direction: 0 },
                    { columnName: 'AllowFgEntry', direction: 0 },
                    { columnName: 'AllowPurposeChange', direction: 0 },
                    { columnName: 'CancelNotamOnly', direction: 0 },
                    { columnName: 'Active', direction: 0 },
                ],
            itemsPerPage: []
        };

        this.paging.pageSize = +this.tableSubjectHeaders.itemsPerPage[0].id;
        this.queryOptions = {
            entity: "icaosubjects",
            page: 1,
            pageSize: this.paging.pageSize,
            sort: "_" + this.tableSubjectHeaders.sorting[0].columnName, //Default Received Descending
            filterValue: ""
        }
        this.conditionQueryOptions = {
            entity: "icaoconditions",
            page: 1,
            pageSize: 20000,
            sort: "_Id", // + this.tableConditionHeaders.sorting[0].columnName //Default Received Descending
            filterValue: ""
        }
    };

    ngOnInit() {
        //Set latest query options
        this.updateLeftMenuHeight();

        const queryOptions = this.memStorageService.get(this.memStorageService.ITEM_QUERY_OPTIONS_KEY);
        if (queryOptions && queryOptions.entity === "icaosubjects") {
            this.queryOptions = queryOptions;
        }
        this.configureReactiveForm();

        this.getIcaoSubjectsInPage(this.queryOptions.page);
    }

    ngOnDestroy() {
        this.updateLeftMenuHeight();
    }

    get langCulture() {
        return (window["app"].cultureInfo || "en").toUpperCase();
    }

    private configureReactiveForm() {
        this.icaoSubjectListForm = this.fb.group({
            searchSubject: [{ value: this.queryOptions.filterValue, disabled: false }, []],
        });
        this.$ctrl("searchSubject").valueChanges.debounceTime(1000).subscribe(() => this.updateSubjectList());
    }

    updateSubjectList(): void {
        if (this.loadingData) return;
        const subject = this.$ctrl('searchSubject');
        this.queryOptions.filterValue = subject.value;
        this.getIcaoSubjectsInPage(this.queryOptions.page);
    }

    $ctrl(name: string): AbstractControl {
        return this.icaoSubjectListForm.get(name);
    }

    selectIcaoSubject(icaoSubject) {
        if (this.selectedIcaoSubject) {
            this.selectedIcaoSubject.isSelected = false;
        }

        this.selectedIcaoSubject = icaoSubject;
        this.selectedIcaoSubject.isSelected = true;
        this.memStorageService.save(this.memStorageService.ITEM_ID_KEY, this.selectedIcaoSubject.id);

        this.getIcaoConditions(this.selectedIcaoSubject.id);
    }

    selectIcaoCondition(icaoCondition) {
        if (this.selectedIcaoCondition) {
            this.selectedIcaoCondition.isSelected = false;
        }

        this.selectedIcaoCondition = icaoCondition;
        this.selectedIcaoCondition.isSelected = true;
    }

    itemsPerPageChanged(e: any): void {
        this.queryOptions.pageSize = e.value;
        this.getIcaoSubjectsInPage(1);
    }

    onPageChange(page: number) {
        this.getIcaoSubjectsInPage(page);
    }

    sort(index) {
        const dir = this.tableSubjectHeaders.sorting[index].direction;

        for (let i = 0; i < this.tableSubjectHeaders.sorting.length; i++) {
            this.tableSubjectHeaders.sorting[i].direction = 0;
        }

        if (dir > 0) {
            this.tableSubjectHeaders.sorting[index].direction = -1;
            this.queryOptions.sort = '_' + this.tableSubjectHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableSubjectHeaders.sorting[index].direction = 1;
            this.queryOptions.sort = this.tableSubjectHeaders.sorting[index].columnName;
        }

        this.getIcaoSubjectsInPage(this.queryOptions.page);
    }

    sortCondition(index) {
        const dir = this.tableConditionHeaders.sorting[index].direction;

        for (let i = 0; i < this.tableConditionHeaders.sorting.length; i++) {
            this.tableConditionHeaders.sorting[i].direction = 0;
        }

        if (dir > 0) {
            this.tableConditionHeaders.sorting[index].direction = -1;
            this.conditionQueryOptions.sort = '_' + this.tableConditionHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableConditionHeaders.sorting[index].direction = 1;
            this.conditionQueryOptions.sort = this.tableConditionHeaders.sorting[index].columnName;
        }

        this.getIcaoConditions(this.selectedIcaoSubject.id);
    }

    sortingClass(index, isSubject) {
        const tableHeaders = isSubject ? this.tableSubjectHeaders : this.tableConditionHeaders;

        if (tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';

        if (tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';

        return 'sorting';
    }

    confirmDeleteIcaoSubject() {
        $('#modal-delete-subject').modal({});
    }

    confirmDeleteIcaoCondition() {
        $('#modal-delete-condition').modal({});
    }

    deleteIcaoSubject() {
        if (this.selectedIcaoSubject) {
            this.dataService.deleteIcaoSubject(this.selectedIcaoSubject.id)
                .subscribe(result => {
                    this.getIcaoSubjectsInPage(this.paging.currentPage);
                    let msg = this.translation.translate('IcaoSubjectSession.SuccessDeleted');
                    this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                }, error => {
                    let msg = this.translation.translate('IcaoSubjectSession.FailureDeleted');
                    this.toastr.error(msg + " " + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        }
    }

    deleteIcaoCondition() {
        if (this.selectedIcaoSubject) {
            this.dataService.deleteIcaoCondition(this.selectedIcaoCondition.id)
                .subscribe(result => {
                    this.getIcaoConditions(this.selectedIcaoSubject.id);
                    let msg = this.translation.translate('IcaoSubjectSession.SuccessConditionDeleted');
                    this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                }, error => {
                    let msg = this.translation.translate('IcaoSubjectSession.FailureConditionDeleted');
                    this.toastr.error(msg + " " + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        }
    }

    private getIcaoSubjectsInPage(page) {
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            this.memStorageService.save(this.memStorageService.ITEM_QUERY_OPTIONS_KEY, this.queryOptions);

            this.dataService.getRegisteredIcaoSubjects(this.queryOptions)
                .subscribe((icaoSubjects: any) => {
                    this.processIcaoSubjects(icaoSubjects);
                });
        }
        catch (e) {
            this.loadingData = false;
        }
    }

    private getIcaoConditions(subjectId: number) {
        try {
            this.loadingConditions = true;

            this.dataService.getRegisteredIcaoSubjectConditions(subjectId, this.conditionQueryOptions)
                .subscribe((icaoConditions: any) => {
                    this.processIcaoConditions(icaoConditions);
                });
        }
        catch (e) {
            this.loadingConditions = false;
        }
    }

    private processIcaoSubjects(icaoSubjects: any) {
        this.paging = icaoSubjects.paging;
        this.totalPages = icaoSubjects.paging.totalPages;

        for (var iter = 0; iter < icaoSubjects.data.length; iter++) icaoSubjects.data[iter].index = iter;

        const lastICaoSubjectIdSelected = this.memStorageService.get(this.memStorageService.ITEM_ID_KEY);
        if (icaoSubjects.data.length > 0) {
            this.icaoSubjects = icaoSubjects.data;
            let index = 0;
            if (lastICaoSubjectIdSelected) {
                index = this.icaoSubjects.findIndex(x => x.id === lastICaoSubjectIdSelected);
                if (index < 0) index = 0; // when not found first should be selected;
            }
            this.icaoSubjects[index].isSelected = true;
            this.selectedIcaoSubject = this.icaoSubjects[index];

            this.getIcaoConditions(this.selectedIcaoSubject.id);

            this.loadingData = false;
        } else {
            this.icaoSubjects = [];
            this.icaoConditions = [];
            this.selectedIcaoSubject = null;
            this.selectedIcaoCondition = null;
            this.loadingData = false;
        }
    }

    private processIcaoConditions(icaoConditions: any) {
        if (icaoConditions.data.length > 0) {
            this.icaoConditions = icaoConditions.data;
            this.icaoConditions[0].isSelected = true;
            this.selectedIcaoCondition = this.icaoConditions[0];
            this.loadingConditions = false;
        } else {
            this.icaoConditions = [];
            this.selectedIcaoCondition = null;
            this.loadingConditions = false;
        }
    }

    private updateLeftMenuHeight() {
        const elem = $('.sidebar-inner');

        if (this.leftMenuHeight) {
            elem.height(this.leftMenuHeight);
        } else {
            this.leftMenuHeight = elem.height();
            elem.height("2000px");
        }
    }
}
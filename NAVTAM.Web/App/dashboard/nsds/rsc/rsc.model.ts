﻿import { Moment } from "moment";
import { RscComponent } from './rsc.component';

//export interface IRSCViewModel {
//    subjectId: string,
//    subjectLocation: string,
//    aerodrome: string,
//    conditionsChanging: boolean,
//    telephone: string,
//    extension: string,
//    taxiways: IBilingualText,
//    aprons: IBilingualText,
//    generalRemarks: IBilingualText,
//    nextPlannedObs: Date,
//    rscRunways: IRSCRunway[],
//    isBilingual: boolean,
//}

/**Data elements related to a single runway. */
export interface IRSCRunway {
    runwayId: string;
    obsTimeUTC: Date | string | null;
    clearedPortion: IRunwayClearedPortion;
    clearedFull: boolean;
    centredClearedWidth: number;
    clearedPortionOffsets: IClearedPortionOffset[];
    //remainingWidth: IRemainingWidth;
    remainingWidth: IBilingualText;
    snowbanks: ISnowbanksRunway;
    noWinterMaintenance: boolean;
    clearingOperations: IClearingOperation;
    treaments: IRwyTreatment;
}

export interface IClearingOperation {
    startAt: Date | string | null;
    completeBy: Date | string | null;
    remarks: IBilingualText;
}

export interface IRwyTreatment {
    sand: boolean;
    sandTimeApplied: Date | string | null;
    chemicallyTreated: boolean;
    chemicalTimeApplied: Date | string | null;
    remarks: IBilingualText;
}

/**Remaining width of the runway */
export interface IRemainingWidth {
    contaminantFI: IContaminantFtIns;
    remarks: IBilingualText;
}

/**Contaminant identification, coverage and depth (if appropriate) in feet or inches */
export interface IContaminantFtIns {
    contamType: ContaminantWithDepthEnum | null;
    contamDepthFI: IContamDepthFtIn;
    contamTypeNoDepth: ContaminantWithNoDepthEnum | null;
    hasDepth: boolean;
}

/**Contaminant height/depth as Trace or inches or feet */
export interface IContamDepthFtIn {
    contamTrace: boolean;
    contamInches: number;
    contamFeet: number;
}

export interface IClearedPortion {
    average?: IClearedSurface;
    runwayThirds?: IClearedSurfaceThird[];
    isAverage: boolean;
}

/**Complex structure describing the surface condition of the cleared portion.  This structure allows reporting in thirds or as an average across the entire surface. */
export interface IRunwayClearedPortion extends IClearedPortion {
    //treatments: TreatmentEnum[];
    contaminantComment: IBilingualText;
    otherConditions: IOtherConditions;
    averageRwyFriction: IRwyFriction;
}

/**Height of snowbank in feet and/or inches. */
export interface IBankHeight {
    bankHeightFt: number;
    bankHeightInch: number;
}

/**Distance of snowbank from edge of runway. */
export interface IBankEdge {
    bankEdgeFt: number;
    onRwyEdge: boolean;
    bankEdgeIns: number;
}

/**Offset of the cleared portion limits from the runway centreline. */
export interface IClearedPortionOffset {
    offsetFT: number;
    offsetSide: CardinalSideEnum;
}

/**Data Elements related to the Cleared Area of the Runway. */
export interface IClearedSurface {
    contaminants: IContaminantIns[];
    rwyCC: string;
}

/**Data Elements related to the Cleared Area of the Runway. */
export interface IClearedSurfaceThird {
    portion: number;
    contaminants: IContaminantIns[];
    frictionCoefficient?: number;
    rwyFrictionThird: IRwyFriction;
    rwyCC: string;
    nesCalculatedRwyCC: string;
}

/**Contaminant depth as Trace or inches. */
export interface IContamDepth {
    contamInches: number;
    contamTrace: boolean;
}

/**Contaminant identification, coverage and depth (if appropriate) in feet or inches. */
export interface IContaminantIns {
    contamCoverage: number;
    contamType: ContaminantWithDepthEnum | null;
    contamDepthI: IContamDepth;
    contamTypeNoD: ContaminantWithNoDepthEnum | null;
    hasDepdth: boolean;
}

export interface IOCHeightType {
    inInches: boolean;
    inFeet: boolean;
    value: number;
}

/**Other conditions affecting the cleared portion of the runway. */
export interface IOtherConditions {
    otherConditArea: IOtherConditArea;
    otherConditEdge: IOtherConditEdge;
    otherConditionsComment: IBilingualText;
}

/**
 * Structure to define area type 'Other Conditions' ON the runway and which do NOT
 * have an associated height.
 */
export interface IOtherConditArea {
    conditTypeL: OtherConditionsAreaEnum;
    distFromThreshold: number;
    thresholdNum: number;
}

/**
 * Structure is used to define other, generally 'edge' related conditions such as
 * windrows and snow drifts in terms of the location(as a distance from a specified runway edge or
 * edges, and across intersections with other runways) and height.
 */
export interface IOtherConditEdge {
    conditTypeR: OtherConditionsEdgeEnum,
    ocHeight: IOCHeightType,
    ocLocation: OCLocationEnum,
    ocDistance: number,
    ocDistanceUOM: UnitOfMeasurement,
    ocSide: CardinalSideEnum[],
    acrossRwy: string[],
}

/**Runway friction on cleared portion of runway. */
export interface IRwyFriction {
    rfObsTime: Date | string;
    frictionCoefficients: number;
    temperature: string;
    device: MeasurementDeviceEnum | null;
    unreliable: boolean;
}

/**Description of significant snowbanks by runway. */
export interface ISnowbanksRunway {
    bankHeight: IBankHeight;
    bankEdge: IBankEdge;
    bankSides: CardinalSideEnum[];
}

/**Defines which side of the runway is affected by a condition.  */
export enum CardinalSideEnum {
    E = 250,
    SE = 251,
    S = 252,
    SW = 253,
    W = 254,
    NW = 255,
    N = 256,
    NE = 257
}

export enum ContaminantWithDepthEnum {
    StandingWater = 60,
    DrySnow = 61,
    WetSnow = 62,
    Slush = 63,
    Unused64 = 64,
    DrySnowOnTopOfIce = 65,
    WetSnowOnTopOfIce = 66,
    SlushOnTopOfIce = 67,
    DrySnowOnTopOfCompactedSnow = 68,
    Unused69 = 69,
    Unused70 = 70,
    WaterOnTopOfCompactedSnow = 71,
    WetSnowOnTopOfCompactedSnow = 72
}

export enum ContaminantWithNoDepthEnum {
    Dry = 20,
    Unused21 = 21,
    Wet = 22,
    Frost = 23,
    Ice = 24,
    CompactedSnow = 25,
    WetIce = 27,
    Unused28 = 28,
    CompactedSnowGravelMix = 29,
    Unused30 = 30,
    Unused31 = 31,
    SlipperyWhenWet = 32
}

export enum MeasurementDeviceEnum {
    TapleyMeter = 130,
    JamesDecelerometer = 131,
    Bowmonk = 132,
    DecelerometerTES = 133
}

/**The location of the condition */
export enum OCLocationEnum {
    AlongInsideRedl = 127, // LE LONG INTERIEUR REDL
    AlongInsideRwyEdge = 128, // LE LONG INTERIEUR DU BORD DE PISTE
    AlongClearedWidth = 129, // LE LONG DE LA LARGEUR DEGAÉE
    FMCL = 134, // FM CL
}

/**
* The side of the centreline for the current edge of the cleared area. Either E-W
* or N-S the latter enumerations depending upon exact runway orientation: Low end # from 05 to 13
* use N/S; anything else use E/W.
*/
export enum OffsetSideEnum {
    East = 11,
    West = 12,
    North = 13,
    South = 14
}

export enum OtherConditionsAreaEnum {
    IcePatches = 122,
    CompactedSnowPatches = 123,
    StandingWaterPatches = 124
}

export enum OtherConditionsEdgeEnum {
    SnowDrifts = 120,
    Windrows = 121,
    SnowBanks = 125
}
export enum TreatmentEnum {
    LoseSand = 100,
    Unused101 = 101,
    Graded = 104,
    Packed = 106,
    Unused107 = 107,
    ChemicallyTreated = 108,
    Scarified = 109
}

/**Distance Unit of Measure: either ft or ins. */
export enum UnitOfMeasurement {
    Inches = 9, // INCHES (INS) / POUCES (PO)
    Feet = 10, // FEET (FT) / PIEDS (PI)
}


export interface IBilingualText {
    english: string,
    french: string,
}

//To delete
export interface IDirectionData {
    runwayDirectionId: string,
    designator: string,
    firstData: ISurfaceData,
    middleData: ISurfaceData,
    endData: ISurfaceData,
}

export interface ISurfaceData {
    data: string,
}

export interface IRSCData {
    runwayId: string,
    designator: string,
    noMaintenance: boolean,
    average?: boolean,
    direction?: IDirectionData,
    full?: ISurfaceData,
}
// End To delete



//Don't touch this part. This is the clases that are coming as plain data from the SDO
export interface SelectContaminantOptionData {
    id: string;
    text: string;
    contaminatId: string;
    needDepth: boolean;
    temperature: number;
}


export interface IRunwaySubject {
    runwayId: string,
    status: string,
    designator: string,
    runwayLength: string,
    runwayLengthInFeet: string,
    runwayWidth: string,
    runwayWidthInFeet: string,
    runwayDirections: IRunwayDirection[],
    selected: boolean,
}

export interface IRunwayDirection {
    runwayDirectionId: string,
    status: string,
    designator: string,
    selected: boolean,
}

export interface IComponentChildModel {
    parent: RscComponent
}

export interface IContaminantModel {
    contaminantId: string,
    id: string, //Combo index
    depth: string,
    coverage: string,
}

export interface IContaminantComponentData {
    firstContaminant: IContaminantModel,
    secondContaminant: IContaminantModel,
    rwyCC: string,
    rwyCCCalculated: string,
    rwyCCActionSelected: string,
    coefficient: string,
    temperature: number,
}

export interface IContaminantComponentModel extends IComponentChildModel {
    id: string,
    data: IContaminantComponentData,
}

export interface IRwyFrictionModel extends IComponentChildModel {
    measureDate: Date | string | null,
    temperature: string,
    coefficient: string,
}

export interface IRwyCCInput {
    firstContaminant: IContaminantModel,
    secondContaminant: IContaminantModel,
    temperature?: number,
}

export interface IClearedWidthModel extends IComponentChildModel {
    runway: IRunwaySubject;
    clearedFull: boolean;
    centredClearedWidth: number;
    clearedPortionOffsets: IClearedPortionOffset[];
}

export interface IRemainingWidthModel extends IComponentChildModel {
    remainingWidthRemarks: string,
}

export interface INextObservationModel extends IComponentChildModel {
    nextScheduledObservation: Moment,
    generalRemarks: string,
}

export interface IWindrowsModel extends IComponentChildModel {
    windrowsRemarks: string,
}

export interface ITaxiwaysRemarksModel extends IComponentChildModel {
    taxiwaysRemarks: string,
}

export interface IApronRemarksModel extends IComponentChildModel {
    apronRemarks: string,
}

export interface ISnowbank {
    bankHeight: IBankHeight;
    bankEdgeFt: number;
    bankSide: CardinalSideEnum;
}

export interface ISnowbanksModel extends IComponentChildModel {
    runway: IRunwaySubject;
    firstSnowbank: ISnowbank,
    secondSnowbank: ISnowbank,
}

export interface ITreatmentsModel extends IComponentChildModel {
    sand: boolean,
    sandTimeApplied: string,
    chemicallyTreated: boolean,
    chemicalTimeApplied: string,
    remarks: string,
}

export interface ICleaningOperationsModel extends IComponentChildModel {
    clearingUnderway: boolean,
    startAt: string,
    completedBy: string,
    remarks: IBilingualText,
}

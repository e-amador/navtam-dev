﻿namespace NavCanada.Applications.NotamWiz.Web.Code.Configurations.AutoMapperResolvers
{
    using System.Collections.Generic;
    using System.Data.Entity.Spatial;
    using System.Linq;

    using AutoMapper;

    using NavCanada.Applications.NotamWiz.Web.Code.GeoTools;
    using NavCanada.Applications.NotamWiz.Web.Models;
    using NavCanada.Core.Data.NotamWiz.Contracts;
    using NavCanada.Core.Domain.Model.Entitities;

    public class DOAVM2DOAConvertor : ITypeConverter<DOAViewModel, Doa>
    {


        public Doa Convert(ResolutionContext context)
        {

            IUow db = (IUow)context.Options.Items["db"];
            //BaseController controller = (BaseController)context.Options.Items["controller"];

            DOAViewModel doavm = (DOAViewModel)context.SourceValue;

            Doa newDOA = context.DestinationValue != null ? (Doa)context.DestinationValue : new Doa();
            
            newDOA.Id = doavm.Id;
            newDOA.Name = doavm.Name;
            newDOA.Description = doavm.Description;            
            newDOA.DoaGeoArea = doavm.DOAGeoArea==null?null:DbGeography.FromText(GeoDataService.GeographyFromGeoJsonMultiPolygon(doavm.DOAGeoArea).ToString(), 4326);

            // remove and re add all filters from VM
            if (newDOA.DoaFilters != null)
            {
                foreach (DoaFilter filter in newDOA.DoaFilters.ToList())
                {
                    db.DoaFilterRepo.Delete(filter);
                }
            }

            newDOA.DoaFilters = new List<DoaFilter>();
            foreach (DOAFilterViewModel fvm in doavm.DoaFilters)
            {
                fvm.Id = 0;
                newDOA.DoaFilters.Add(Mapper.Map<DOAFilterViewModel, DoaFilter>(fvm));
            }

            return newDOA;

        }
    }


   
}
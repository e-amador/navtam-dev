﻿using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Domain.Model.Entitities;
using System.Data.Entity;
using System.Linq;

namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    public class SystemStatusRepo : Repository<SystemStatus>, ISystemStatusRepo
    {
        public SystemStatusRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public SystemStatus GetStatus() => DbSet.FirstOrDefault();
    }
}

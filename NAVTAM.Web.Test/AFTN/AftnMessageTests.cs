﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NDS.Relay;
using System;

namespace NAVTAM.Web.Test.AFTN
{
    [TestClass]
    public class AftnMessageTests
    {
        public static DateTime Date => new DateTime(2018, 12, 14, 03, 35, 08);

        [TestMethod]
        public void Relay_AftnMessage_basic_format_test()
        {
            var expected = $"\u0001%TRANS_ID_DATE%\r\nGG CYYCYFYX\r\n140335 CYEGYFYX\r\n\u0002NOTAM HAPPENED\r\n\v\u0003";
            var result = AftnMessageFormatter.FormatMessage("GG", "CYYCYFYX", "CYEGYFYX", "NOTAM HAPPENED\r\n", Date);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void Relay_AftnMessageParser_ParseMessage_tests()
        {
            Assert.AreEqual(AftnMessageParser.ParseMessage(""), null);

            // missing <SOH>
            Assert.IsNull(AftnMessageParser.ParseMessage("ABC0044 14033608\r\nGG CYYCYFYX\r\n140335 CYEGYFYX\r\n\u0002NOTAM HAPPENED\r\n\v\u0003"));

            // missing <STX>
            Assert.IsNull(AftnMessageParser.ParseMessage("\u0001ABC0044 14033608\r\nGG CYYCYFYX\r\n140335 CYEGYFYX\r\nNOTAM HAPPENED\r\n\v\u0003"));

            // missing <VT>
            //Assert.IsNull(AftnMessageParser.ParseMessage("\u0001ABC0044 14033608\r\nGG CYYCYFYX\r\n140335 CYEGYFYX\r\n\u0002NOTAM HAPPENED\r\n\u0003"));

            // missing <ETX>
            Assert.IsNull(AftnMessageParser.ParseMessage("\u0001ABC0044 14033608\r\nGG CYYCYFYX\r\n140335 CYEGYFYX\r\n\u0002NOTAM HAPPENED\r\n\v"));

            // correct message
            var msg = AftnMessageParser.ParseMessage("\u0001ABC0044 14033608\r\nGG CYYCYFYX\r\n140335 CYEGYFYX\r\n\u0002NOTAM HAPPENED\r\n\v\u0003");

            Assert.IsNotNull(msg);
            Assert.AreEqual(msg.Headers, "ABC0044 14033608\r\nGG CYYCYFYX\r\n140335 CYEGYFYX\r\n");
            Assert.AreEqual(msg.Text, "NOTAM HAPPENED\r\n");
        }

        [TestMethod]
        public void Relay_AftnMessageParser_ParseMessage_headers_tests()
        {
            var message = AftnMessageParser.ParseMessage("\u0001ABC0044 14033608\r\nGG CTSCNCHN\r\n140335 CYEGYFYX\r\n\u0002NOTAM HAPPENED\r\n\v\u0003");

            Assert.IsNotNull(message);

            Assert.AreEqual(message.TransId , "ABC0044");
            Assert.AreEqual(message.TransTime, "14033608");

            Assert.AreEqual(message.Priority, "GG");
            Assert.AreEqual(message.DestinationAddress, "CTSCNCHN");

            Assert.AreEqual(message.FilingTime, "140335");
            Assert.AreEqual(message.OriginAddress, "CYEGYFYX");
        }


        [TestMethod]
        public void Relay_AftnMessageParser_ParseValidRqn_test()
        {
            var aftnMessage = AftnMessageParser.ParseMessage($"\u0001CGA0005 29133553\r\nGG {AftnConsts.NOFAddress}\r\n290935 CTSCNDSE\r\n\u0002RQN CYHQ A0001/18-A0100/18, LANG=C\r\n\v\u0003");
            var request = AftnMessageParser.ParseRequestMessage(aftnMessage);

            Assert.IsNotNull(request);
            Assert.AreEqual(request.Type, AftnRequestType.Rqn);
            Assert.AreEqual(request.ReplyAddress, "CTSCNDSE");
            Assert.AreEqual(request.RequestBody, "A0001/18-A0100/18");
            Assert.AreEqual(request.Language, "C");
        }

        [TestMethod]
        public void Relay_AftnMessageParser_ParseValidRqn_NoLanguage_test()
        {
            var aftnMessage = AftnMessageParser.ParseMessage($"\u0001CGA0005 29133553\r\nGG {AftnConsts.NOFAddress}\r\n290935 CTSCNDSE\r\n\u0002RQN CYHQ A0001/18-A0100/18\r\n\v\u0003");
            var request = AftnMessageParser.ParseRequestMessage(aftnMessage);

            Assert.IsNotNull(request);
            Assert.AreEqual(request.Language, "");
        }

        [TestMethod]
        public void Relay_AftnMessageParser_ParseValidRql_test()
        {
            var aftnMessage = AftnMessageParser.ParseMessage($"\u0001CGA0005 29133553\r\nGG {AftnConsts.NOFAddress}\r\n290935 CTSCNDSE\r\n\u0002RQL CYHQ A D\r\n\v\u0003");
            var request = AftnMessageParser.ParseRequestMessage(aftnMessage);

            Assert.IsNotNull(request);
            Assert.AreEqual(request.Type, AftnRequestType.Rql);
            Assert.AreEqual(request.ReplyAddress, "CTSCNDSE");
            Assert.AreEqual(request.RequestBody, "A D");
        }

        [TestMethod]
        public void Relay_AftnMessageParser_ParseInvalidRqn_test()
        {
            // no headers nor text
            Assert.IsNull(AftnMessageParser.ParseRequestMessage(new AftnMessage()));

            // no headers
            Assert.IsNull(AftnMessageParser.ParseRequestMessage(new AftnMessage() { Text = "RQN CYHQ A0001/18-A0100/18" }));

            // no text
            Assert.IsNull(AftnMessageParser.ParseRequestMessage(new AftnMessage() { Headers = "CGA0005 29133553\r\nGG CYHQYNYX\r\n290935 CTSCNDSE\r\n", }));

            // invalid header (missing first line)
            Assert.IsNull(AftnMessageParser.ParseRequestMessage(new AftnMessage { Headers = "GG CYHQYNYX\r\n290935 CTSCNDSE\r\n", Text = "RQN CYHQ A0001/18-A0100/18" }));

            // invalid header (invalid second line: no GG)
            Assert.IsNull(AftnMessageParser.ParseRequestMessage(new AftnMessage { Headers = "CGA0005 29133553\r\nCYHQYNYX\r\n290935 CTSCNDSE\r\n", Text = "RQN CYHQ A0001/18-A0100/18" }));

            // invalid header (invalid second line: no NOF address)
            Assert.IsNull(AftnMessageParser.ParseRequestMessage(new AftnMessage { Headers = "CGA0005 29133553\r\nGG YX\r\n290935 CTSCNDSE\r\n", Text = "RQN CYHQ A0001/18-A0100/18" }));

            // invalid header (invalid third line: response address too short)
            Assert.IsNull(AftnMessageParser.ParseRequestMessage(new AftnMessage { Headers = "CGA0005 29133553\r\nGG CYHQYNYX\r\n290935 CNDSE\r\n", Text = "RQN CYHQ A0001/18-A0100/18" }));

            // invalid header (invalid third line: no response address)
            Assert.IsNull(AftnMessageParser.ParseRequestMessage(new AftnMessage { Headers = "CGA0005 29133553\r\nGG CYHQYNYX\r\n290935\r\n", Text = "RQN CYHQ A0001/18-A0100/18" }));

            // invalid text (missing RQN or RQL)
            Assert.IsNull(AftnMessageParser.ParseRequestMessage(new AftnMessage { Headers = "CGA0005 29133553\r\nGG CYHQYNYX\r\n290935 CTSCNDSE\r\n", Text = "CYHQ A0001/18-A0100/18" }));

            // invalid text (invalid RQN format, missing CYHQ)
            Assert.IsNull(AftnMessageParser.ParseRequestMessage(new AftnMessage { Headers = "CGA0005 29133553\r\nGG CYHQYNYX\r\n290935 CTSCNDSE\r\n", Text = "RQN A0001/18-A0100/18" }));

            // wrong spacing
            Assert.IsNull(AftnMessageParser.ParseRequestMessage(new AftnMessage() { Text = "RQN  CYHQ A0001/18-A0100/18" }));
            Assert.IsNull(AftnMessageParser.ParseRequestMessage(new AftnMessage() { Text = "RQN CYHQ  A0001/18-A0100/18" }));
        }

        [TestMethod]
        public void Relay_AftnMessageParser_ParseSenderAddress_test()
        {
            Assert.AreEqual(AftnMessageParser.ParseSenderAddress(""), "");
            Assert.AreEqual(AftnMessageParser.ParseSenderAddress("XXX XXXX"), "");
            Assert.AreEqual(AftnMessageParser.ParseSenderAddress("XXX XXXX\r\nGG CTSCNDSE\r\n121909 CTSCNCHN"), "CTSCNDSE");
            Assert.AreEqual(AftnMessageParser.ParseSenderAddress("XXX XXXX\rGG CTSCNDSE\r121909 CTSCNCHN"), "CTSCNDSE");
        }
    }
}

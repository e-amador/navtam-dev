﻿using NavCanada.Core.Common.Contracts;
using NavCanada.Core.TaskEngine.Contracts;
using NDS.Common.Contracts;
using System;

namespace NavCanada.Core.TaskEngine.QueueListener
{
    public interface INdsQueueListenerContext
    {
        ILogger Logger { get; }
        IClusterStorageFactory StorageFactory { get; }
        ISubscription SubscribeToRequestTopic(Action<INdsMessage> subscriptionCallback);
    }
}

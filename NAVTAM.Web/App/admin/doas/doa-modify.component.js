"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DoaModifyComponent = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var windowRef_service_1 = require("../../common/windowRef.service");
var angular_l10n_1 = require("angular-l10n");
var mem_storage_service_1 = require("../shared/mem-storage.service");
var toastr_service_1 = require("./../../common/toastr.service");
var data_service_1 = require("../shared/data.service");
var data_model_1 = require("../shared/data.model");
var DoaModifyComponent = /** @class */ (function () {
    function DoaModifyComponent(toastr, fb, dataService, memStorageService, router, windowRef, activatedRoute, locale, translation) {
        this.toastr = toastr;
        this.fb = fb;
        this.dataService = dataService;
        this.memStorageService = memStorageService;
        this.router = router;
        this.windowRef = windowRef;
        this.activatedRoute = activatedRoute;
        this.locale = locale;
        this.translation = translation;
        this.model = null;
        this.isDoaUniqueName = true;
        this.isValidating = false;
        this.isValidLocation = true;
        this.doaChangesList = [];
        this.selectedDoaLocation = null;
        this.activeLocationPoint = null;
        this.doaSource = this.translation.translate('DoasAdminSession.DoaSourceGeography');
    }
    DoaModifyComponent.prototype.ngOnInit = function () {
        this.activeLocationPoint = { source: '', location: '', inDoa: false, radius: 0 };
        this.selectedDoaLocation = { source: '', location: '', inDoa: false, radius: 5 };
        this.action = this.activatedRoute.snapshot.data[0].action;
        this.model = this.activatedRoute.snapshot.data['model'];
        this.doa = this.model;
        this.configureReactiveForm();
    };
    DoaModifyComponent.prototype.onSubmit = function () {
        var _this = this;
        var doaUpdate = this.prepareData();
        if (doaUpdate) {
            this.dataService.saveDoa(doaUpdate)
                .subscribe(function (data) {
                var msg = _this.translation.translate('DoasAdminSession.SuccessDoaCreated');
                _this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                _this.router.navigate(["/administration/doas"]);
            }, function (error) {
                var msg = _this.translation.translate('DoasAdminSession.FailureDoaCreated');
                _this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
        }
        else {
            var msg = this.translation.translate('DoasAdminSession.ErrorDoaData');
            this.toastr.error(msg, "", { 'positionClass': 'toast-bottom-right' });
        }
    };
    DoaModifyComponent.prototype.configureReactiveForm = function () {
        var _this = this;
        this.doasForm = this.fb.group({
            doaName: [{ value: this.model.name, disabled: false }, []],
            doaDescription: [{ value: this.model.description, disabled: false }, []],
            doaSource: [{ value: this.doaSource, disabled: true }, []],
            doaGeo: [{ value: this.model.region.geography, disabled: true }, []],
            doaLocation: [{ value: this.selectedDoaLocation.location, disabled: false }, []],
            doaRadius: [{ value: this.selectedDoaLocation.radius, disabled: false }, [doaRadiusValidator]],
        });
        this.$ctrl("doaName").valueChanges.debounceTime(1000).subscribe(function () { return _this.validateDoaNameUnique(); });
        this.$ctrl("doaLocation").valueChanges.debounceTime(1000).subscribe(function () { return _this.validateRegionLocation(); });
    };
    DoaModifyComponent.prototype.validateForm = function () {
        if (this.isValidating)
            return false;
        if (!this.doasForm.pristine) {
            if (this.prepareData())
                return this.isDoaUniqueName;
            return true;
        }
        return false;
    };
    DoaModifyComponent.prototype.prepareData = function () {
        if (!this.isDoaUniqueName)
            return null;
        var dName = this.$ctrl('doaName');
        var dDesc = this.$ctrl('doaDescription');
        var doaUpdate = {
            id: (this.action === 'copy') ? 0 : this.doa.id,
            name: dName.value,
            description: dDesc.value,
            region: this.doa.region,
            doaType: data_model_1.RegionType.Doa,
            changes: this.doaChangesList
        };
        return doaUpdate;
    };
    DoaModifyComponent.prototype.validateRegionLocation = function () {
        var _this = this;
        if (this.isValidating)
            return;
        var dLocation = this.$ctrl('doaLocation');
        if (dLocation.value) {
            if (dLocation.value.length > 3) {
                this.isValidating = true;
                this.isValidLocation = false;
                this.dataService.validateDoaGeoLocation(this.doa.id, dLocation.value)
                    .subscribe(function (resp) {
                    _this.isValidLocation = true;
                    _this.activeLocationPoint = resp;
                    _this.isValidating = false;
                    _this.validateForm();
                }, function (error) {
                    _this.activeLocationPoint.location = '';
                    _this.isValidating = false;
                });
            }
        }
    };
    DoaModifyComponent.prototype.prepareLocationData = function () {
        if (this.isValidLocation) {
            var radiusCtrl = this.$ctrl('doaRadius');
            var loc = { source: '', location: '', inDoa: false, radius: 5 };
            loc.source = this.activeLocationPoint.source.toUpperCase();
            loc.location = this.activeLocationPoint.location.toUpperCase();
            loc.radius = +radiusCtrl.value;
            loc.inDoa = this.activeLocationPoint.inDoa;
            var point = this.doaChangesList.find(function (x) { return x.source === loc.source && x.location === loc.location; });
            if (point) {
                point.radius = loc.radius;
                this.selectedDoaLocation = point;
            }
            else {
                var index = this.doaChangesList.push(loc);
                this.selectedDoaLocation = this.doaChangesList[index - 1];
            }
        }
    };
    DoaModifyComponent.prototype.includeLocationEnabled = function () {
        if (this.isValidLocation && !this.isValidating) {
            var radiusCtrl = this.$ctrl('doaRadius');
            if (this.activeLocationPoint && +radiusCtrl.value > 0 && this.activeLocationPoint.source !== '' && this.activeLocationPoint.location !== '') {
                return !this.activeLocationPoint.inDoa;
            }
        }
        return false;
    };
    DoaModifyComponent.prototype.excludeLocationEnabled = function () {
        if (this.isValidLocation && !this.isValidating) {
            var radiusCtrl = this.$ctrl('doaRadius');
            if (this.activeLocationPoint && +radiusCtrl.value > 0 && this.activeLocationPoint.source !== '' && this.activeLocationPoint.location !== '')
                return this.activeLocationPoint.inDoa;
        }
        return false;
    };
    DoaModifyComponent.prototype.disableSubmit = function () {
        if (this.doasForm.pristine)
            return true;
        if (this.isValidating)
            return true;
        return !this.validateForm();
    };
    DoaModifyComponent.prototype.validDoaName = function () {
        var dName = this.$ctrl('doaName');
        if (dName.value) {
            if (dName.value.length > 0)
                return true;
        }
        return false;
    };
    DoaModifyComponent.prototype.validDoaNameSize = function () {
        var dName = this.$ctrl('doaName');
        if (dName.value) {
            if (dName.value.length > 3)
                return true;
        }
        return false;
    };
    DoaModifyComponent.prototype.validateDoaNameUnique = function () {
        var _this = this;
        if (this.isValidating)
            return;
        if (this.validDoaName() && this.validDoaNameSize()) {
            var dName = this.$ctrl('doaName');
            if (dName.value) {
                if (dName.value.length > 3) {
                    this.isValidating = true;
                    var id = (this.action === 'copy') ? 0 : this.doa.id;
                    this.dataService.existsDoaName(id, dName.value)
                        .subscribe(function (resp) {
                        _this.isDoaUniqueName = !resp;
                        _this.isValidating = false;
                    });
                }
            }
        }
    };
    DoaModifyComponent.prototype.pickLocation = function (loc) {
        if (this.doaChangesList) {
            var point = this.doaChangesList.find(function (x) { return (x.source === loc.source && x.location === loc.location && x.inDoa === loc.inDoa); });
            if (point) {
                this.selectedDoaLocation = point;
                var ctrlLocation = this.$ctrl('doaLocation');
                var ctrlRadius = this.$ctrl('doaRadius');
                ctrlLocation.setValue(this.selectedDoaLocation.source);
                ctrlRadius.setValue(this.selectedDoaLocation.radius);
            }
        }
    };
    DoaModifyComponent.prototype.$ctrl = function (name) {
        return this.doasForm.get(name);
    };
    DoaModifyComponent.prototype.rowColor = function (loc) {
        if (!this.selectedDoaLocation)
            return '';
        if (this.selectedDoaLocation.source === loc.source &&
            this.selectedDoaLocation.location === loc.location &&
            this.selectedDoaLocation.inDoa === loc.inDoa)
            return 'bg-submitted';
        return '';
    };
    DoaModifyComponent.prototype.deletePoint = function (loc) {
        if (this.doaChangesList) {
            var index = this.doaChangesList.findIndex(function (x) { return (x.source === loc.source && x.location === loc.location && x.inDoa === loc.inDoa); });
            if (index > -1) {
                this.doaChangesList.splice(index, 1);
                if (this.doaChangesList.length > 0) {
                    this.selectedDoaLocation = this.doaChangesList[0];
                    var ctrlLocation = this.$ctrl('doaLocation');
                    var ctrlRadius = this.$ctrl('doaRadius');
                    ctrlLocation.setValue(this.selectedDoaLocation.source);
                    ctrlRadius.setValue(this.selectedDoaLocation.radius);
                }
                else {
                    var ctrlLocation = this.$ctrl('doaLocation');
                    var ctrlRadius = this.$ctrl('doaRadius');
                    ctrlLocation.setValue('');
                    ctrlRadius.setValue(5);
                    this.activeLocationPoint = { source: '', location: '', inDoa: false, radius: 0 };
                    this.selectedDoaLocation = { source: '', location: '', inDoa: false, radius: 5 };
                }
            }
        }
    };
    DoaModifyComponent.prototype.getAction = function (inDoa) {
        if (inDoa)
            return this.translation.translate('DoasAdminSession.Exclude');
        else
            return this.translation.translate('DoasAdminSession.Include');
    };
    DoaModifyComponent.prototype.validateRadius = function () {
        var fc = this.doasForm.get('doaRadius');
        return !fc.errors || this.doasForm.pristine;
    };
    DoaModifyComponent.prototype.validateLocation = function () {
        return this.isValidLocation || this.doasForm.pristine;
    };
    DoaModifyComponent.prototype.decRadius = function () {
        var radCtrl = this.$ctrl("doaRadius");
        if (+radCtrl.value > 0) {
            radCtrl.setValue(+radCtrl.value - 1);
            radCtrl.markAsDirty();
        }
    };
    DoaModifyComponent.prototype.incRadius = function () {
        var radCtrl = this.$ctrl("doaRadius");
        if (+radCtrl.value < 9999) {
            radCtrl.setValue(+radCtrl.value + 1);
            radCtrl.markAsDirty();
        }
    };
    DoaModifyComponent.prototype.backToDoasList = function () {
        this.router.navigate(['/administration/doas']);
    };
    DoaModifyComponent = __decorate([
        core_1.Component({
            templateUrl: '/app/admin/doas/doa-modify.component.html'
        }),
        __param(0, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [Object, forms_1.FormBuilder,
            data_service_1.DataService,
            mem_storage_service_1.MemoryStorageService,
            router_1.Router,
            windowRef_service_1.WindowRef,
            router_1.ActivatedRoute,
            angular_l10n_1.LocaleService,
            angular_l10n_1.TranslationService])
    ], DoaModifyComponent);
    return DoaModifyComponent;
}());
exports.DoaModifyComponent = DoaModifyComponent;
function doaRadiusValidator(c) {
    if (typeof (c.value) === "string") {
        if (!c.value) {
            return { 'required': true };
        }
    }
    if (isANumber(c.value)) {
        if (+c.value < 0 || +c.value > 999999) {
            return { 'invalid': true };
        }
        return null;
    }
    return { 'invalid': true };
}
function isANumber(str) {
    return !/\D/.test(str);
}
//# sourceMappingURL=doa-modify.component.js.map
﻿using NavCanada.Core.Domain.Model.Entitities;
using NAVTAM.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace NAVTAM.NsdEngine.TWYCLSD.V10
{
    public class TwyClsdNsdManager : NsdManager
    {
        public TwyClsdNsdManager(int categoryId, string categoryName, string version) : base(categoryId, categoryName, version)
        {
        }

        protected override TokenViewModel CreateTokenViewModel()
        {
            return new TwyClsdViewModel();
        }

        protected override TokenViewModel DeserializeToken(string tokens)
        {
            return Deserialize<TwyClsdViewModel>(tokens);
        }

        protected override ValidationResult ValidateModel(ProposalViewModel model, ProposalContext context)
        {
            var token = model.Token as TwyClsdViewModel;
            if (token == null) return ValidationResult.Fail(Resources.InvalidTokenType);

            var subject = context.TwyContext().TwySdoCacheSubject; // SdoSubject();
            if (subject == null) return ValidationResult.Fail(Resources.InvalidSubjectId);
            var forbiddenWords = context.GetContext<string>("ForbiddenItemEWords");

            //Do Validation here
            if (!token.AllTaxiwaysSelected)
            {
                var closedTaxiways = token.AllTaxiways.Where(t => t.Selected).ToList();
                if (closedTaxiways.Count == 0) return ValidationResult.Fail("You need to specify at least 1 taxiway to close.");

                var partialClosedTaxiways = closedTaxiways.Where(t => !t.Full).ToList();
                foreach (var twy in partialClosedTaxiways)
                {
                    if (twy.Start.Length == 0) return ValidationResult.Fail(string.Format("Error on Start closure for {0}", twy.FullName));
                    if (twy.End.Length == 0) return ValidationResult.Fail(string.Format("Error on End closure for {0}", twy.FullName));
                    if (ContainsForbiddenWord(twy.Start, forbiddenWords))
                        return ValidationResult.Fail(Resources.CARS_ReasonForbiddenWords); //It use the same message text as CARS
                    if (ContainsForbiddenWord(twy.End, forbiddenWords))
                        return ValidationResult.Fail(Resources.CARS_ReasonForbiddenWords); //It use the same message text as CARS
                    if (context.IsBilingual)
                    {
                        if (twy.StartFr.Length == 0) return ValidationResult.Fail(string.Format("Missing Start closure in French for {0}", twy.FullName));
                        if (twy.EndFr.Length == 0) return ValidationResult.Fail(string.Format("Missing End closure in French for {0}", twy.FullName));
                        if (ContainsForbiddenWord(twy.StartFr, forbiddenWords))
                            return ValidationResult.Fail(Resources.CARS_ReasonForbiddenWords); //It use the same message text as CARS
                        if (ContainsForbiddenWord(twy.EndFr, forbiddenWords))
                            return ValidationResult.Fail(Resources.CARS_ReasonForbiddenWords); //It use the same message text as CARS
                    }
                }
            }

            if (token.ClosureReasonId > -1)
            {
                var twyClosureReason = TwyClosureReasons.GetClosureReasons().FirstOrDefault(c => c.Id == token.ClosureReasonId);
                if (twyClosureReason == null) return ValidationResult.Fail("Error on the Closure Reason Id.");
                if (twyClosureReason.Text == "OTHER")
                {
                    if (token.ClosureReasonFreeText == null || token.ClosureReasonFreeText.Length == 0) return ValidationResult.Fail("You need to specify a Reason for the closure.");
                    if (ContainsForbiddenWord(token.ClosureReasonFreeText, forbiddenWords))
                        return ValidationResult.Fail(Resources.CARS_ReasonForbiddenWords); //It use the same message text as CARS

                    if (context.IsBilingual)
                    {
                        if (token.ClosureReasonFreeTextFr == null || token.ClosureReasonFreeTextFr.Length == 0) return ValidationResult.Fail("You need to specify a Reason for the closure in French.");
                        if (ContainsForbiddenWord(token.ClosureReasonFreeTextFr, forbiddenWords))
                            return ValidationResult.Fail(Resources.CARS_ReasonForbiddenWordsFr); //It use the same message text as CARS
                    }
                }
            }

            return ValidationResult.Succeed();
        }

        protected override async Task UpdateNsdContext(TokenViewModel token, ProposalContext context)
        {
            var twyCont = new TwyContext();
            // Add SdoSubject
            var subjectId = (token as TwyClsdViewModel).SubjectId;
            var subject = await context.Uow.SdoCacheRepo.GetByIdAsync(subjectId);
            twyCont.TwySdoCacheSubject = subject;

            if (subject != null)
                twyCont.HasFastExitTwy = HasFastExitTaxiway(context, (token as TwyClsdViewModel), subject.Id);
            else
                twyCont.HasFastExitTwy = false;

            context.AddContext("TwyContext", twyCont);
        }

        protected override string GetNotamLocation(TokenViewModel token)
        {
            return (token as TwyClsdViewModel).SubjectLocation;
        }

        protected override NearSdoSubject GetNearestSdoSubject(TokenViewModel token, ProposalContext context)
        {
            return (context.TwyContext() != null) ? new NearSdoSubject(context.TwyContext().TwySdoCacheSubject, 0.0) : null;
            //return new NearSdoSubject(context.TwyContext().TwySdoCacheSubject, 0.0);
        }

        protected override string GetFir(TokenViewModel token, ProposalContext context)
        {
            return context.TwyContext().TwySdoCacheSubject.Fir;
        }

        protected override string GenerateItemA(TokenViewModel token, ProposalContext context)
        {
            // Aerodrome Item A
            return CovertAerodromeDesignatorToItemA(context.TwyContext().TwySdoCacheSubject.Designator);
        }

        protected override BilingualText GenerateItemE(TokenViewModel token, ProposalContext context)
        {
            var twyCont = context.TwyContext();

            var itemEPrefix = GetItemEAerodromeNamePrefix(context, twyCont.TwySdoCacheSubject.Name).Append(GetAmendPubMessagePrefix(token));

            var itemE = !context.IsCancelation ? GenerateNonCancelItemE(context, token as TwyClsdViewModel) : GenerateCancelItemE(context, token as TwyClsdViewModel);
            // Add the prefix to itemE
            if (itemEPrefix.English != null)
            {
                itemE.English = itemEPrefix.English + itemE.English;
                if (context.IsBilingual) itemE.French = itemEPrefix.French + itemE.French;
            }
            return itemE;
        }

        protected override int GetRadius(TokenViewModel token, ProposalContext context)
        {
            return 5;
        }

        protected override string GetScope(TokenViewModel token, ProposalContext context)
        {
            return "A";
        }

        protected override async Task<string> GetSeries(TokenViewModel token, ProposalContext context)
        {
            return await GetNotamSeriesForSubject(context.Uow, context.TwyContext().TwySdoCacheSubject, "MX", false);
        }

        protected override string GetTraffic(TokenViewModel token, ProposalContext context)
        {
            return "IV";
        }

        protected override string GetCode23(TokenViewModel token, ProposalContext context)
        {
            return (context.TwyContext().HasFastExitTwy) ? "MY" : "MX";
        }

        protected override string GetCode45(TokenViewModel token, ProposalContext context)
        {
            return !context.IsCancelation ? "LC" : "AK";
        }

        protected override string GetPurpose(TokenViewModel token, ProposalContext context)
        {
            return (context.TwyContext().HasFastExitTwy) ? "NBO" : "M";
        }

        protected override int GetLowerLimit(TokenViewModel baseToken, ProposalContext context)
        {
            return 0;
        }

        protected override int GetUpperLimit(TokenViewModel token, ProposalContext context)
        {
            return 999;
        }

        private BilingualText GenerateCancelItemE(ProposalContext context, TwyClsdViewModel twyClsViewModel)
        {
            var itemE = new BilingualText("", "");
            if (twyClsViewModel.AllTaxiwaysSelected)
            {
                itemE.English = "ALL TWYS OPN";
                if (context.IsBilingual) itemE.French = "TOUTES TWYS OPN";
            }
            else
            {
                var closedTaxiways = twyClsViewModel.AllTaxiways.Where(t => t.Selected).ToList();
                var twyDesign = BuildTwyDesig(closedTaxiways);
                itemE.English = string.Format("{0} OPN", twyDesign.English).ToUpper();
                if (context.IsBilingual) itemE.French = string.Format("{0} OPN", twyDesign.French).ToUpper();
            }
            return itemE;
        }

        private bool HasFastExitTaxiway(ProposalContext context, TwyClsdViewModel twyClsViewModel, string sdoSubjectMid)
        {
            var twysFastExit = context.Uow.SdoCacheRepo.GetAerodromeTaxiways(sdoSubjectMid).FindAll(t => t.TwyType == "FASTEXIT");

            if (twysFastExit.Count == 0) return false;

            if (twyClsViewModel.AllTaxiwaysSelected)
                return true;

            var twySel = twyClsViewModel.AllTaxiways.Where(t => t.Selected).ToList();
            foreach (var elem in twysFastExit)
            {
                var f = twySel.Find(t => t.Id == elem.Id);
                if (f != null)
                    return true;
            }

            return false;
        }

        private BilingualText GenerateClosureReason(ProposalContext context, TwyClsdViewModel token)
        {
            var closureReason = new BilingualText("", "");
            if (token.ClosureReasonId > 0)
            {
                //var reason = TwyClosureReasons.GetClosureReasons().FirstOrDefault(r => r.Id == token.ClosureReasonId);
                var reason = TwyClosureReasons.GetItemEClosureReasons().FirstOrDefault(r => r.Id == token.ClosureReasonId);
                if (reason.Text == "OTHER")
                {
                    closureReason.English = string.Format("DUE {0}", token.ClosureReasonFreeText);
                    if (context.IsBilingual) closureReason.French = string.Format("CAUSE {0}", token.ClosureReasonFreeTextFr);
                }
                else
                {
                    closureReason.English = string.Format("DUE {0}", reason.Text);
                    if (context.IsBilingual) closureReason.French = string.Format("CAUSE {0}", reason.TextFr);
                }
            }
            return closureReason;
        }

        private BilingualText GenerateNonCancelItemE(ProposalContext context, TwyClsdViewModel twyClsViewModel)
        {
            var itemE = new BilingualText("", "");
            if (twyClsViewModel.AllTaxiwaysSelected)
            {
                var closureReason = this.GenerateClosureReason(context, twyClsViewModel);
                itemE.English = string.Format("ALL TWYS CLSD{0}.",
                    (closureReason.English.Trim().Length > 0) ? " " + closureReason.English : "").Trim().ToUpper();
                if (context.IsBilingual) itemE.French = string.Format("TOUTES TWYS CLSD{0}.",
                    (closureReason.French.Trim().Length > 0) ? " " + closureReason.French : "").Trim().ToUpper();
            }
            else
            {
                int a = twyClsViewModel.AllTaxiways.RemoveAll(t => t.Id == "0"); //Removing the All TWYS from the list
                var closedTaxiways = twyClsViewModel.AllTaxiways.Where(t => t.Selected).ToList();
                if (closedTaxiways != null && closedTaxiways.Count() > 0)
                {
                    var totalClosed = BuildTwyDesig(closedTaxiways.Where(t => t.Full).ToList());
                    var partClosed = BuildTwyDesigPartialClosure(closedTaxiways.Where(t => !t.Full).ToList());

                    var closureReason = this.GenerateClosureReason(context, twyClsViewModel);

                    itemE.English = string.Format("{0}{1}{2} CLSD{3}.",
                        totalClosed.English,
                        (totalClosed.English.Trim().Length > 0 && partClosed.English.Trim().Length > 0) ? ",\n " : "",
                        partClosed.English,
                        (closureReason.English.Trim().Length > 0) ? " " + closureReason.English : "").Trim().ToUpper();

                    if (context.IsBilingual) itemE.French = string.Format("{0}{1}{2} CLSD{3}.",
                                totalClosed.French,
                                (totalClosed.French.Trim().Length > 0 && partClosed.French.Trim().Length > 0) ? ",\n " : "",
                                partClosed.French,
                                (closureReason.French.Trim().Length > 0) ? " " + closureReason.French : "").Trim().ToUpper();
                }
            }
            return itemE;
        }

        private BilingualText BuildTwyDesig(List<TaxiwayViewModel> closedTaxiways)
        {
            var retValue = new BilingualText("", "");

            if (closedTaxiways != null && closedTaxiways.Count > 0)
            {
                retValue.English = closedTaxiways[0].FullName;
                retValue.French = closedTaxiways[0].FullName;
                for (var i = 1; i < closedTaxiways.Count; i++)
                {
                    if (i == closedTaxiways.Count - 1)
                    {
                        retValue.English = string.Format("{0} AND {1}", retValue.English, closedTaxiways[i].FullName);
                        retValue.French = string.Format("{0} ET {1}", retValue.French, closedTaxiways[i].FullName);
                    }
                    else
                    {
                        retValue.English = string.Format("{0}, {1}", retValue.English, closedTaxiways[i].FullName);
                        retValue.French = string.Format("{0}, {1}", retValue.French, closedTaxiways[i].FullName);
                    }
                }
            }
            return retValue;
        }

        private BilingualText BuildTwyDesigPartialClosure(List<TaxiwayViewModel> closedTaxiways)
        {
            var retValue = new BilingualText("", "");
            if (closedTaxiways != null && closedTaxiways.Count > 0)
            {
                foreach (var closedTaxiway in closedTaxiways)
                {
                    if (retValue.English.Trim().Length == 0)
                    {
                        retValue.English = string.Format("{0} BTN {1} AND {2}", closedTaxiway.FullName, closedTaxiway.Start, closedTaxiway.End);
                        retValue.French = string.Format("{0} BTN {1} ET {2}", closedTaxiway.FullName, closedTaxiway.StartFr, closedTaxiway.EndFr);
                    }
                    else
                    {
                        retValue.English = string.Format("{0},\n {1} BTN {2} AND {3}", retValue.English, closedTaxiway.FullName, closedTaxiway.Start, closedTaxiway.End);
                        retValue.French = string.Format("{0},\n {1} BTN {2} ET {3}", retValue.French, closedTaxiway.FullName, closedTaxiway.StartFr, closedTaxiway.EndFr);
                    }
                }
            }
            return retValue;
        }

        protected override string GetDisplayName()
        {
            return Resources.TwyClsdDisplayName;
        }

        protected override string GetNsdHelpLink()
        {
            return Resources.TwyClsdHelpLink;
        }
    }

    internal class TwyContext
    {
        public SdoCache TwySdoCacheSubject { get; set; }
        public bool HasFastExitTwy { get; set; }

    }


    internal static class TwyClsdNsdManagerExt
    {
        internal static TwyContext TwyContext(this ProposalContext context) => context.GetContext<TwyContext>("TwyContext");
    }
}
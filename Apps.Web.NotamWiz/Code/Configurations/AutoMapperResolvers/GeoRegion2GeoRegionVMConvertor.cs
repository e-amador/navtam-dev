﻿using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace NavCanada.Applications.NotamWiz.Web.Code.Configurations.AutoMapperResolvers
{
    using AutoMapper;


    using NavCanada.Applications.NotamWiz.Web.Code.Extentions;
    using NavCanada.Applications.NotamWiz.Web.Code.GeoTools;
    using NavCanada.Applications.NotamWiz.Web.Models;
    using NavCanada.Core.Domain.Model.Entitities;

    public class GeoRegion2GeoRegionVmConvertor: ITypeConverter<GeoRegion, GeoRegionViewModel>
    {
        ILog log = LogManager.GetLogger(typeof(GeoRegion2GeoRegionVmConvertor));

        public GeoRegionViewModel Convert(ResolutionContext context)
        {
            this.log.Object(new { Method = "Convert", Parameters = new { context = context } });

            var region = context.SourceValue as GeoRegion;

            return new GeoRegionViewModel
            {
                Id = region.Id,
                Name = region.Name,
                Geo = GeoDataService.GeoJsonMultiPolygonFromGeography(region.Geo)

            };
        }
    }


}
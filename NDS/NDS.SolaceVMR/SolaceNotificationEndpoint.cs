﻿using NavCanada.Core.TaskEngine.Contracts;
using Nds.Common.Exceptions;
using System;
using System.Collections.Generic;
using System.Configuration;
using NDSProperty = NDS.Common.Contracts.Property;

namespace NDS.SolaceVMR
{
    public class SolaceNotificationEndpoint : ISolaceEndpoint
    {
        public SolaceNotificationEndpoint(string connectionString)
        {
            _connectionString = connectionString;
            _dummyMode = ConfigurationManager.AppSettings["HubDummyMode"] == "on";
        }

        public void TestConnection()
        {
            if (_dummyMode)
                return;

            try
            {
                using (var solaceBus = new SolaceVMRWrapper(_connectionString))
                {
                    using (var topic = solaceBus.TestConnection())
                    {
                        // leave this empty
                    }
                }
            }
            catch(Exception ex)
            {
                throw new NdsCommunicationException($"Testing connection to the Hub failed. \nError: {ex.ToString()}", ex);
            }
        }

        public void PostToTopic(string topicNamespace, string content, IEnumerable<NDSProperty> properties)
        {
            if (_dummyMode)
                return; 

            try
            {
                using (var solaceBus = new SolaceVMRWrapper(_connectionString))
                {
                    using (var message = solaceBus.CreateMessage(content, properties))
                    {
                        using (var topic = solaceBus.CreateTopic(topicNamespace))
                        {
                            topic.PostMessage(message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new NdsCommunicationException($"Failed to post a message on the Hub. \nError: {ex.ToString()}", ex);
            }
        }

        readonly string _connectionString;
        readonly bool _dummyMode;
    }
}

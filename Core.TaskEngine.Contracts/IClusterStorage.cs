﻿using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using System;

namespace NavCanada.Core.TaskEngine.Contracts
{
    public interface IClusterStorage: IDisposable
    {
        IUow Uow { get; set; }
        IClusterQueues Queues { get; set; }
    }
}

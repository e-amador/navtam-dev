"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TwyClsdService = void 0;
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Rx_1 = require("rxjs/Rx");
var xsrf_token_service_1 = require("../../../common/xsrf-token.service");
var windowRef_service_1 = require("../../../common/windowRef.service");
var TwyClsdService = /** @class */ (function () {
    function TwyClsdService(http, winRef, xsrfTokenService) {
        this.http = http;
        this.xsrfTokenService = xsrfTokenService;
        this.app = winRef.nativeWindow['app'];
    }
    TwyClsdService.prototype.getSdoTwys = function (sdoId) {
        return this.http.get(this.app.apiUrl + "taxiways/GetTwys/?sdoId=" + sdoId)
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    TwyClsdService.prototype.getTwyClsdReasons = function () {
        return this.http.get(this.app.apiUrl + "taxiways/GetTwyClsdReasons")
            .map(function (response) {
            return response.json();
        }).catch(this.handleError);
    };
    TwyClsdService.prototype.handleError = function (error) {
        return Rx_1.Observable.throw(error.statusText);
    };
    TwyClsdService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http, windowRef_service_1.WindowRef, xsrf_token_service_1.XsrfTokenService])
    ], TwyClsdService);
    return TwyClsdService;
}());
exports.TwyClsdService = TwyClsdService;
//# sourceMappingURL=twyclsd.service.js.map
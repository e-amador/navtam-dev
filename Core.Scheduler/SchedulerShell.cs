﻿using NavCanada.Core.Scheduler.Implementations;
using Quartz.Impl;

namespace NavCanada.Core.Scheduler
{
    public class SchedulerSettings
    {
        public string DbConnectionString { get; set; }
    }

    public class SchedulerShell
    {
        public SchedulerShell(SchedulerSettings settings)
        {
            _settings = settings;

            var scheduler = StdSchedulerFactory.GetDefaultScheduler();
            var provider = new SchedulerJobProvider(new SchedulerJobFactory());

            _quartzScheduler = new QuartzScheduler(scheduler, provider);
        }

        public void Start()
        {
            _quartzScheduler.Start();
        }

        public void Stop()
        {
            _quartzScheduler.Shutdown();
        }

        public void Pause()
        {
            _quartzScheduler.StandBy();
        }

        public void StartJobs()
        {
            _quartzScheduler.StartJobs();
        }

        private readonly SchedulerSettings _settings;
        private readonly QuartzScheduler _quartzScheduler;
    }
}

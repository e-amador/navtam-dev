﻿namespace NavCanada.Core.Domain.Model.Entitities
{
    public class IcaoSubjectCondition
    {
        public int Id { get; set; }
        public int SubjectId { get; set; }
        public IcaoSubject Subject { get; set; }
        public string Description { get; set; }
        public string DescriptionFrench { get; set; }
        public string Code { get; set; }
        public bool I { get; set; }
        public bool V { get; set; }
        public bool N { get; set; }
        public bool B { get; set; }
        public bool O { get; set; }
        public bool M { get; set; }
        public int Lower { get; set; }
        public int Upper { get; set; }
        public int Radius { get; set; }
        public bool CancelNotamOnly { get; set; }
        public bool Active { get; set; }
        public bool AllowFgEntry { get; set; }
        public bool AllowPurposeChange { get; set; }
        public int? Version { get; set; }
    }
}
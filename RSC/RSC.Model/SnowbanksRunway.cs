﻿using RSC.Model.Enums;

namespace RSC.Model
{
    /// <summary>
    /// Description of significant snowbanks by runway.
    /// </summary>
    public class SnowbanksRunway
    {
        /// <summary>
        /// Height of snowbank in feet and/or inches.
        /// </summary>
        public BankHeight BankHeight { get; set; }
        /// <summary>
        /// Distance from edge of runway to snowbank. i.e. distance OUTSIDE runway edge.
        /// </summary>
        public BankEdge BankEdge { get; set; }
        /// <summary>
        /// Location of snowbank with respect to the runway. Either E-W or N-S enumerations
        /// depending upon exact runway orientation: Low end # from 05 to 13 use N/S; anything else use E/W.
        /// </summary>
        public CardinalSideEnum[] BankSides { get; set; }
    }
}
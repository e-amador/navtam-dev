"use strict";
/**
 * picker.service
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PickerService = void 0;
var core_1 = require("@angular/core");
var dialog_component_1 = require("./dialog.component");
var moment = require("moment/moment");
var Rx_1 = require("rxjs/Rx");
var PickerService = /** @class */ (function () {
    function PickerService() {
        this.eventSource = new Rx_1.Subject();
        this.events = this.eventSource.asObservable();
    }
    Object.defineProperty(PickerService.prototype, "dtLocale", {
        get: function () {
            return this._dtLocale;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PickerService.prototype, "dtViewFormat", {
        get: function () {
            return this._dtViewFormat;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PickerService.prototype, "dtReturnObject", {
        get: function () {
            return this._dtReturnObject;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PickerService.prototype, "dtDialogType", {
        get: function () {
            return this._dtDialogType;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PickerService.prototype, "dtPickerType", {
        get: function () {
            return this._dtPickerType;
        },
        set: function (value) {
            this._dtPickerType = value;
            if (value === 'both' || value === 'date') {
                this._dtDialogType = dialog_component_1.DialogType.Date;
            }
            else if (value === 'time') {
                this._dtDialogType = dialog_component_1.DialogType.Time;
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PickerService.prototype, "dtPositionOffset", {
        get: function () {
            return this._dtPositionOffset;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PickerService.prototype, "dtMode", {
        get: function () {
            return this._dtMode;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PickerService.prototype, "dtHourTime", {
        get: function () {
            return this._dtHourTime;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PickerService.prototype, "dtTheme", {
        get: function () {
            return this._dtTheme;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PickerService.prototype, "moment", {
        get: function () {
            return this._moment;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PickerService.prototype, "selectedMoment", {
        get: function () {
            return this._selectedMoment;
        },
        set: function (value) {
            this._selectedMoment = value;
            this.eventSource.next(value);
        },
        enumerable: false,
        configurable: true
    });
    PickerService.prototype.setPickerOptions = function (dtLocale, dtViewFormat, dtReturnObject, dtPositionOffset, dtMode, dtHourTime, dtTheme, dtPickerType) {
        this._dtLocale = dtLocale;
        this._dtViewFormat = dtViewFormat;
        this._dtReturnObject = dtReturnObject;
        this._dtPositionOffset = dtPositionOffset;
        this._dtMode = dtMode;
        this._dtHourTime = dtHourTime;
        this._dtTheme = dtTheme;
        this.dtPickerType = dtPickerType;
    };
    PickerService.prototype.setMoment = function (value) {
        if (value) {
            this._moment = this._dtReturnObject === 'string' ? moment(value, this._dtViewFormat) :
                moment(value);
            this.selectedMoment = this._moment.clone();
        }
        else {
            this._moment = moment();
        }
    };
    PickerService.prototype.setDate = function (moment) {
        var m = this.selectedMoment ? this.selectedMoment.clone() : this.moment;
        var daysDifference = moment.clone().startOf('date').diff(m.clone().startOf('date'), 'days');
        this.selectedMoment = m.add(daysDifference, 'd');
    };
    PickerService.prototype.setTime = function (hour, minute, meridian) {
        var m = this.selectedMoment ? this.selectedMoment.clone() : this.moment.clone();
        if (this.dtHourTime === '12') {
            if (meridian === 'AM') {
                if (hour === 12) {
                    m.hours(0);
                }
                else {
                    m.hours(hour);
                }
            }
            else {
                if (hour === 12) {
                    m.hours(12);
                }
                else {
                    m.hours(hour + 12);
                }
            }
        }
        else if (this.dtHourTime === '24') {
            m.hours(hour);
        }
        m.minutes(minute);
        this.selectedMoment = m;
    };
    PickerService.prototype.parseToReturnObjectType = function () {
        switch (this.dtReturnObject) {
            case 'string':
                return this.selectedMoment.format(this.dtViewFormat);
            case 'moment':
                return this.selectedMoment;
            case 'json':
                return this.selectedMoment.toJSON();
            case 'array':
                return this.selectedMoment.toArray();
            case 'iso':
                return this.selectedMoment.toISOString();
            case 'object':
                return this.selectedMoment.toObject();
            case 'js':
            default:
                return this.selectedMoment.toDate();
        }
    };
    PickerService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], PickerService);
    return PickerService;
}());
exports.PickerService = PickerService;
//# sourceMappingURL=picker.service.js.map
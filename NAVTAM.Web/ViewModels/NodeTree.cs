﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NAVTAM.ViewModels
{
    public class NodeTree
    {
        public int Id { get; set; }
        public int GroupCategoryId { get; set; }
        public string Name { get; set; }
        //public string Category { get; set; }
        public string Text { get; set; }
        public bool IsSelected { get; set; }
        public List<NodeTree> Children { get; set; }
    }
}
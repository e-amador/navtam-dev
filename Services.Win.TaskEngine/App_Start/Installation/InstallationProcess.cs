﻿namespace NavCanada.Services.Win.TaskEngine.Installation
{
    using System;
    using System.Collections;
    using System.Configuration.Install;
    using System.ServiceProcess;

    public static class InstallationProcess
    {
        private static bool IsInstalled()
        {
            using (ServiceController controller =
                new ServiceController("Services.Win.TaskEngine"))
            {
                try
                {
                    ServiceControllerStatus status = controller.Status;
                }
                catch
                {
                    return false;
                }
                return true;
            }
        }

        public static bool IsRunning()
        {
            using (ServiceController controller =
                new ServiceController("Services.Win.TaskEngine"))
            {
                if (!IsInstalled()) return false;
                return (controller.Status == ServiceControllerStatus.Running);
            }
        }

        private static AssemblyInstaller GetInstaller()
        {
            AssemblyInstaller installer = new AssemblyInstaller(
                typeof(ProjectInstaller).Assembly, null);
            installer.UseNewContext = true;
            return installer;
        }

        public static void InstallService()
        {
            if (IsInstalled()) return;

            try
            {
                using (AssemblyInstaller installer = GetInstaller())
                {
                    IDictionary state = new Hashtable();
                    try
                    {
                        installer.Install(state);
                        installer.Commit(state);
                    }
                    catch
                    {
                        try
                        {
                            installer.Rollback(state);
                        }
                        catch { }
                        throw;
                    }
                }
            }
            catch (Exception)
            {
                //Logger.Error("UninstallService", ex);
            }
        }

        public static void UninstallService()
        {
            if (!IsInstalled()) return;
            try
            {
                using (AssemblyInstaller installer = GetInstaller())
                {
                    IDictionary state = new Hashtable();
                    try
                    {
                        installer.Uninstall(state);
                    }
                    catch
                    {
                        throw;
                    }
                }
            }
            catch (Exception)
            {
                //Logger.Error("UninstallService", ex);
            }
        }

        public static void StartService()
        {
            if (!IsInstalled()) return;

            //Logger.Debug("StartService");

            using (ServiceController controller =
                new ServiceController("Services.Win.TaskEngine"))
            {
                try
                {
                    if (controller.Status != ServiceControllerStatus.Running)
                    {
                        controller.Start();
                        controller.WaitForStatus(ServiceControllerStatus.Running,
                            TimeSpan.FromSeconds(120));
                    }
                }
                catch (Exception)
                {
                    //Logger.Error("StartService", ex);
                }
            }
        }

        public static void StopService()
        {
            if (!IsInstalled()) return;

            using (ServiceController controller =
                new ServiceController("Services.Win.TaskEngine"))
            {
                try
                {
                    if (controller.Status != ServiceControllerStatus.Stopped)
                    {
                        controller.Stop();
                        controller.WaitForStatus(ServiceControllerStatus.Stopped,
                             TimeSpan.FromSeconds(120));
                    }
                }
                catch (Exception)
                {
                    //Logger.Error("StopService", ex);
                }
            }
        }
    }
}
﻿namespace NavCanada.Applications.NotamWiz.Web.Areas.Admin.Controllers
{
    using System.Web.Mvc;

    [Authorize(Roles = "Administrator")]
    public class LocalizationController : Controller
    {
        // GET: Admin/Localization
        public ActionResult Index()
        {
            return View();
        }
    }
}
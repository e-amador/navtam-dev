﻿using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.TaskEngine.Contracts;
using NavCanada.Core.TaskEngine.Helpers;
using NDS.Common.Contracts;
using NDS.Relay;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NavCanada.Core.TaskEngine.QueueListener
{
    public class NdsQueueListener : IDisposable
    {
        static NdsQueueListener()
        {
            _dummyMode = ConfigurationManager.AppSettings["HubDummyMode"] != null;
        }

        public static NdsQueueListener Create(INdsQueueListenerContext context)
        {
            lock (typeof(NdsQueueListener))
            {
                return _ndsQueueListener ?? (_ndsQueueListener = new NdsQueueListener(context));
            }
        }

        public static NdsQueueListener Instance => _ndsQueueListener;

        private NdsQueueListener(INdsQueueListenerContext context)
        {
            _context = context;
            _completedResetEvent = new ManualResetEvent(false);
            _listening = false;
        }

        public bool IsStarting => _starting;
        public bool IsListening => _listening;

        public void StopListening()
        {
            _completedResetEvent?.Set();
            _listening = false;
        }

        public void StartListening()
        {
            _starting = !_dummyMode;

            _completedResetEvent.Reset();

            if (_dummyMode)
                return;

            IClusterStorage clusterStorage;
            if (!_context.StorageFactory.NewStorageConnection(out clusterStorage))
                throw new InvalidOperationException("The data sources factory failed to create a new data source set");

            var listener = Task.Factory.StartNew(() =>
            {
                try
                {
                    var subscription = _context.SubscribeToRequestTopic(msg => OnReadMessage(msg, clusterStorage));
                    _completedResetEvent.WaitOne();
                    subscription.Unsubscribe();
                }
                finally
                {
                    _listening = false;
                }
            });

            _listening = true;
            _starting = false;
        }

        private void OnReadMessage(INdsMessage message, IClusterStorage clusterStorage)
        {
            lock (this)
            {
                // convert the message
                var requestMessage = MapMessageToRequestModel(message);

                if (requestMessage == null)
                {
                    // this should never happen, but if there was a critical problem mapping the message 
                    // MapMessageToRequestModel should have logged the error.
                    return;
                }

                // verify we have a valid message
                if (!IsValidMessage(requestMessage))
                {
                    // log the warning
                    _context.Logger.LogWarn(typeof(NdsQueueListener), $"Invalid Message Detected", requestMessage);

                    // correct the MSG_ID so we can process it
                    if (string.IsNullOrWhiteSpace(requestMessage.MessageId))
                    {
                        requestMessage.MessageId = Guid.NewGuid().ToString();
                    }

                    // make it an AFTN message
                    requestMessage.MessageType = RelayValues.AFTN_MESSAGE;
                }

                var uow = clusterStorage.Uow;
                uow.BeginTransaction();
                try
                {
                    // 1- DB message
                    uow.NdsIncomingRequestRepo.Add(requestMessage);
                    uow.SaveChanges();

                    // 2- Add Process Nds Request Message to the Service Broker queue
                    var mto = MtoHelper.CreateMtoMessageWithKeyValue(MessageTransferObjectId.ProcessNdsRequestMsg, requestMessage.Id);
                    clusterStorage.Queues.TaskEngineQueue.Publish(mto);

                    uow.Commit();
                }
                catch (Exception ex)
                {
                    // stop listening because we may stop receiving message
                    StopListening();

                    uow.Rollback();
                    _context.Logger.LogError(ex, typeof(NdsQueueListener));

                    throw;
                }
            }
        }

        static bool IsValidMessage(NdsIncomingRequest message)
        {
            return !string.IsNullOrEmpty(message.MessageId) && !string.IsNullOrEmpty(message.MessageType);
        }

        private NdsIncomingRequest MapMessageToRequestModel(INdsMessage message)
        {
            try
            {
                var metadata = message.Properties.ToDictionary(p => p.Name);

                var msgId = ReadMessageProperty(metadata, RelayFields.MSG_ID, string.Empty);
                var msgType = ReadMessageProperty(metadata, RelayFields.MSG_TYPE, string.Empty);

                return new NdsIncomingRequest()
                {
                    Id = Guid.NewGuid(),
                    MessageId = msgId,
                    MessageType = msgType,
                    Body = message.Body,
                    Metadata = RelayUtils.SerializeNdsMetadata(metadata.Values),
                    RecievedDate = DateTime.UtcNow
                };
            }
            catch (Exception ex)
            {
                _context.Logger.LogError(ex, typeof(NdsQueueListener));
                return null;
            }
        }

        static string ReadMessageProperty(Dictionary<string, Property> props, string name, string defValue)
        {
            var ppValue = props.ContainsKey(name) ? props[name].Value?.ToString() : null;
            return !string.IsNullOrWhiteSpace(ppValue) ? ppValue : defValue;
        }

        #region IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    StopListening();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }

        #endregion IDisposable Support

        private bool _starting;
        private bool _listening;

        private readonly INdsQueueListenerContext _context;
        private readonly ManualResetEvent _completedResetEvent;

        private static bool _dummyMode;
        private static NdsQueueListener _ndsQueueListener;
    }
}
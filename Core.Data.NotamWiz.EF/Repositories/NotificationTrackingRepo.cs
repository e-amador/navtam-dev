﻿namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    using System.Data.Entity;
    using System.Linq;

    using Contracts.Repositories;
    using Domain.Model.Entitities;
    using Domain.Model.Enums;

    public class NotificationTrackingRepo : Repository<NotificationTracking>, INotificationTrackingRepo
    {
        public NotificationTrackingRepo(DbContext dbContext)
            : base(dbContext)
        {
        }

        public NotificationTracking GetLastEmailSent(int packageId, TemplateType templateType, NotificationType notificationType)
        {
            return DbSet.FirstOrDefault(e => 
                e.PackageId == packageId && 
                e.TemplateType == templateType && 
                e.NotifiationType == notificationType);
        }
    }
}

﻿namespace NavCanada.Utils.Apps.InoMock
{
    using Microsoft.AspNet.SignalR;
    using Microsoft.AspNet.SignalR.Hubs;

    [HubName("NotificationHub")]
    public class NotificationHub : Hub
    {        
        public void dataChanged()
        {
            Clients.All.refreshData();
        }

        public void subscribeNP()
        {

           // Clients.All.refreshData();

        }
    }
}
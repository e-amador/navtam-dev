﻿//using Microsoft.SqlServer.Types;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Proxies.AeroRdsProxy.Model;
using NavCanada.Core.Proxies.AeroRdsProxy.SDOData.Models.SDOEntities;
using System;
using System.Data.Entity.Spatial;

namespace NAVTAM.Helpers
{
    /// <summary>
    /// Class for providing NSDGeo utilities.
    /// TODO: ENTIRE CLASS NEEDS TO BE TESTED
    /// </summary>
    public class NSDUtilities
    {
        /* Test that this function returns correct values */
        /// <summary>
        /// Returns a detailed cardinal direction. This method eats a double in degrees.
        /// </summary>
        /// <param name="degrees">the degree heading</param>
        /// <returns>a detailed cardinal direction string</returns>
        public static string DegreesToCardinalDetailed(double degrees)
        {
            string[] caridnals = { "N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW", "N" };
            return caridnals[(int)Math.Round(((double)degrees * 10 % 3600) / 225)];
        }
        /// <summary>
        /// Sets the radius to a digit with one decimal point if under 1.91 OR rounds to zero decimal points.
        /// </summary>
        /// <param name="radius">Method eats a decimal #</param>
        /// <returns>a formatted decimal number</returns>
        public static decimal ParseRadius(decimal radius)
        {
            try
            {
                if (radius > 1.91m)
                {
                    Math.Round(radius);
                    return radius;
                }
                if (radius < 1.91m)
                {
                    return radius;
                }
            }
            catch (OverflowException)
            {
                //Log.Error(err.Message + "SystemOverFlow in ParseRadius" + "NSDUtilities", err);
            }
            catch (Exception)
            {
                //Log.Error(err.Message + "Generic Exception in ParseRadius" + "NSDUtilities", err);
            }
            /* If reached, something failed. */
            //Log.Error(DbRes.T("End of ParseRadius should not have been reached.", "NSDUtilities"));
            return radius;
        }

        /// <summary>
        /// Checks to see if UserDOA intersects with the Item you want to check. Takes two DbGeo obj.
        /// </summary>
        /// <param name="UserDOA">User's DOA DbGeo Type</param>
        /// <param name="ItemCheck">Obstacle DbGeo Type</param>
        /// <returns>True or False</returns>
        public static bool CheckCoordInDoa(DbGeography UserDOA, DbGeography ItemCheck)
        {
            return UserDOA.Intersects(ItemCheck);
        }

        /// <summary>
        /// Converts SqlGeography obj to DbGeography obj. Default srid if not provided is 4326
        /// </summary>
        /// <param name="sqlgeo"></param>
        /// <param name="srid"></param>
        /// <returns></returns>
        //public static DbGeography SqlGeographyToDbGeography(SqlGeography sqlgeo, int srid)
        //{
        //    DbGeography convertedGeo = DbGeography.FromText(sqlgeo.ToString(), srid);
        //    return convertedGeo;
        //}

        /// <summary>
        /// converts DbGeography obj to SqlGeography obj. Default srid if not provided is 4326.
        /// </summary>
        /// <param name="dbgeo"></param>
        /// <param name="srid"></param>
        /// <returns></returns>
        //public static SqlGeography DbGeographyToSqlGeography(DbGeography dbgeo, int srid)
        //{
        //    SqlGeography convertedSql = SqlGeography.Parse(dbgeo.AsText()).MakeValid();
        //    return convertedSql;
        //}

        /// <summary>
        /// Create a DbGeography point based on latitude and longitude. Eats doubles.
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <returns></returns>
        public static DbGeography CreateDbGeographyPointDbl(double latitude, double longitude)
        {
            var text = string.Format("POINT({0} {1})", longitude, latitude);
            // 4326 is most common coordinate system used by GPS/Maps
            return DbGeography.PointFromText(text, 4326);
        }

        /// <summary>
        /// Create a DbGeography point based on latitude and longitude. Eats a string.
        /// </summary>
        /// <param name="latitudeLongitude">
        /// String should be two values either single comma or space delimited
        /// 45.710030,-121.516153
        /// 45.710030 -121.516153
        /// </param>
        /// <returns></returns>
        public static DbGeography CreateDbGeographyPointString(string latitudeLongitude)
        {
            var tokens = latitudeLongitude.Split(',', ' ');
            if (tokens.Length != 2)
                throw new ArgumentException("Invalid Location Entered.");
            var text = string.Format("POINT({0} {1})", tokens[1], tokens[0]);
            return DbGeography.PointFromText(text, 4326);
        }

        /// <summary>
        /// Converts MetersToNauticalMiles
        /// </summary>
        /// <param name="meters"></param>
        /// <returns></returns>
        public static double MetersToNauticalMiles(double meters)
        {
            double nauticalmiles = (meters * (0.000539957));
            return nauticalmiles;
        }

        public static float GetRdnBearing(IUow uow, Ahp ahp, Rdn rdn)
        {
            var trueBrg = rdn.ValTrueBrg;
            if (string.IsNullOrEmpty(trueBrg))
                return float.MinValue;

            var northrenReagion = uow.DoaRepo.GetNorthernRegion();
            if (northrenReagion != null && ahp.GEOLocation.ToEntityDbGeography().Intersects(northrenReagion.DoaGeoArea))
                return float.Parse(trueBrg);


            if (string.IsNullOrEmpty(rdn.ValMagBrg))
                return float.Parse(trueBrg);

            float magVar = 0;
            if (!float.TryParse(ahp.ValMagVar, out magVar))
                return float.Parse(trueBrg);


            return float.Parse(trueBrg) - magVar;
        }

        public static float GetAhpMagVar(IUow uow, Ahp ahp)
        {
            var northrenRegion = uow.DoaRepo.GetNorthernRegion();
            if (northrenRegion != null && ahp.GEOLocation.ToEntityDbGeography().Intersects(northrenRegion.DoaGeoArea)) return 0;

            if (string.IsNullOrEmpty(ahp.ValMagVar)) return 0;

            float magVar = 0;
            if (!float.TryParse(ahp.ValMagVar, out magVar)) return 0;

            //return magVar; Original result, changed on Dec 5, 2017 by Luis

            float magVarChg = 0;
            if (!float.TryParse(ahp.ValMagVarChg, out magVarChg)) return magVar;

            int dateMagVar = 0;
            if (!int.TryParse(ahp.DateMagVar, out dateMagVar)) return magVar;

            //Applying formula ValMarVar + ((ActualYear - DateMagVar) * ValMagVarChg)
            var result = magVar + ((DateTime.Now.Year - dateMagVar) * magVarChg);

            return result;
        }

        public static float GetAerodromeMagneticVariation(IUow uow, Aerodrome ahp, DbGeography refPoint)
        {
            var northrenRegion = uow.DoaRepo.GetNorthernRegion();
            if (northrenRegion != null && refPoint.Intersects(northrenRegion.DoaGeoArea))
                return 0;

            if (string.IsNullOrEmpty(ahp.MagneticVariation))
                return 0;

            float magVar = 0;
            if (!float.TryParse(ahp.MagneticVariation, out magVar))
                return 0;

            //return magVar; Original result, changed on Dec 5, 2017 by Luis

            float magVarChg = 0;
            if (!float.TryParse(ahp.MagneticVariationChange, out magVarChg))
                return magVar;

            int yearMagVar = 0;
            if (!int.TryParse(ahp.MagneticVariationYear, out yearMagVar))
                return magVar;

            // applying formula ValMarVar + ((ActualYear - DateMagVar) * ValMagVarChg)
            var result = magVar + ((DateTime.UtcNow.Year - yearMagVar) * magVarChg);

            return result;
        }
    }
}
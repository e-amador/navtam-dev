﻿namespace NavCanada.Applications.NotamWiz.Web.Code.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Dynamic;
    using System.Linq;
    using System.Web.Mvc;

    public class ProcessResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public string ErrorCode { get; set; }
        List<string> _errors = new List<string>();
        public List<string> Errors { get { return _errors; } set { _errors = value; } }

        public ProcessResult()
        {
            Success = true;
        }

        public ProcessResult(string message)
        {
            Success = true;
            Message = message;

        }

        public ProcessResult(string code, string error)
        {
            Success = false;
            ErrorCode = code;
            Errors.Add(error);
        }

        public dynamic Result
        {
            get
            {
                dynamic result = new
                {
                    Success = Success,
                    Message = Message,
                    Error = String.Join("\n", this.Errors.ToArray().ToArray()),
                    Code = ErrorCode
                };

                return result;
            }
        }

        public static ContentResult GetErrorResult(string code, string error, dynamic extar = null)
        {
            return new ContentResult { Content = Newtonsoft.Json.JsonConvert.SerializeObject(Combine((new ProcessResult(code, error)).Result, extar)), ContentType = "application/json" };
        }

        public static ContentResult GetSuccessResult(dynamic extar = null)
        {
            return new ContentResult { Content = Newtonsoft.Json.JsonConvert.SerializeObject(Combine((new ProcessResult(null)).Result, extar)), ContentType = "application/json" };
        }

        public static ContentResult GetSuccessResult(string message, dynamic extar = null)
        {
            return new ContentResult { Content = Newtonsoft.Json.JsonConvert.SerializeObject(Combine((new ProcessResult(message)).Result, extar)), ContentType = "application/json" };
        }

        static dynamic Combine(object item1, object item2)
        {
            if (item1 == null || item2 == null)
                return item1 ?? item2 ?? new ExpandoObject();

            dynamic expando = new ExpandoObject();
            var result = expando as IDictionary<string, object>;
            foreach (System.Reflection.PropertyInfo fi in item1.GetType().GetProperties())
            {
                result[fi.Name] = fi.GetValue(item1, null);
            }
            foreach (System.Reflection.PropertyInfo fix in item2.GetType().GetProperties())
            {
                result[fix.Name] = fix.GetValue(item2, null);
            }
            return result;
        }

    }
}
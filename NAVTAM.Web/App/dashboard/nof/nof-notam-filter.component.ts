﻿import { Component, Input, Output, OnChanges, OnInit, EventEmitter, ChangeDetectorRef } from '@angular/core'
import { TranslationService } from 'angular-l10n';

import * as moment from 'moment';

import {
    IQueryOptions,
    IPaging,
    ITableHeaders,
    IFilterCriteria,
    FilterOperator,
    ProposalType
} from '../shared/data.model'

import { DataService } from '../shared/data.service'
import { MemoryStorageService } from '../shared/mem-storage.service'
import { forEach } from '@angular/router/src/utils/collection';
import { AbstractControl } from '@angular/forms';

declare var $: any; 

@Component({
    selector: 'notam-filter',
    templateUrl: '/app/dashboard/nof/nof-notam-filter.component.html'
})
export class NofNotamFilterComponent implements OnChanges, OnInit {

    @Input() pageSize: number;
    @Input() filterType: number;
    @Output() onFilterChanged: EventEmitter<any> = new EventEmitter<any>();

    filterQueryOptions: IQueryOptions;
    filterColumnsData: Array<any>;
    filterColumnStartValue: Array<string> = [];
    filterSlotsActivated: Array<number> = [];
    filterSlotsActive: number;
    filterColumnsDataSelectedIndex: number;

    parent: any = null;

    filterOperatorsData: Array<any>;

    filters: Array<IFilterCriteria> = [];

    slots: Array<number> = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
    filterErrorMessage: string[] = [];

    constructor(
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        public translation: TranslationService,
        private changeDetectionRef: ChangeDetectorRef) {
        const emptyFilter = {
            field: "",
            operator: FilterOperator.EQ,
            value: "",
            type: "text"
        }
        this.filterQueryOptions = {
            page: 1,
            pageSize: 10,
            sort: "_Published" //Default Published Descending
        }

        this.filters.push(emptyFilter);
        this.filterColumnsData = [
            { id: 'EMPTY', text: this.translation.translate('Filter.SelectFilterCriteria'), type: "text", operators: [], currentOper: FilterOperator.EQ,  values: [], currentValue: "" },
            { id: 'NotamId', text: this.translation.translate('Filter.NotamId'), type: "text", operators: [], currentOper: FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'ReferredNotamId', text: this.translation.translate('Filter.ReferredNotamId'), type: "text", operators: [], currentOper: FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'ItemA', text: this.translation.translate('Filter.ItemA'), type: "text", operators: [], currentOper: FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'ItemE', text: this.translation.translate('Filter.ItemE'), type: "text", operators: [], currentOper: FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Type', text: this.translation.translate('Filter.NotamType'), type: "combobox", operators: [], currentOper: FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Series', text: this.translation.translate('Filter.Series'), type: "combobox", operators: [], currentOper: FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Scope', text: this.translation.translate('Filter.Scope'), type: "combobox", operators: [], currentOper: FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Code23', text: this.translation.translate('Filter.Code23'), type: "text", operators: [], currentOper: FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Code45', text: this.translation.translate('Filter.Code45'), type: "text", operators: [], currentOper: FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Traffic', text: this.translation.translate('Filter.Traffic'), type: "combobox", operators: [], currentOper: FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Purpose', text: this.translation.translate('Filter.Purpose'), type: "combobox", operators: [], currentOper: FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Radius', text: this.translation.translate('Filter.Radius'), type: "number", operators: [], currentOper: FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'LowerLimit', text: this.translation.translate('Filter.LowerLimit'), type: "number", operators: [], currentOper: FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'UpperLimit', text: this.translation.translate('Filter.UpperLimit'), type: "number", operators: [], currentOper: FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Published', text: this.translation.translate('Filter.Published'), type: "datetime", operators: [], currentOper: FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'StartActivity', text: this.translation.translate('Filter.StartActivity'), type: "datetime", operators: [], currentOper: FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'EndValidity', text: this.translation.translate('Filter.EndValidity'), type: "datetime", operators: [], currentOper: FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Operator', text: this.translation.translate('Filter.Approver'), type: "text", operators: [], currentOper: FilterOperator.EQ, values: [], currentValue: "" },
            { id: 'Originator', text: this.translation.translate('Filter.Originator'), type: "text", operators: [], currentOper: FilterOperator.EQ, values: [], currentValue: "" },
        ];
        this.filterColumnStartValue.push(this.filterColumnsData[0].id);

        this.filterOperatorsData = [
            { id: FilterOperator.EQ, text: this.translation.translate('Filter.Equal') },
            { id: FilterOperator.NEQ, text: this.translation.translate('Filter.NotEqual') },
            { id: FilterOperator.LT, text: this.translation.translate('Filter.LessThan')},
            { id: FilterOperator.GT, text: this.translation.translate('Filter.GreaterThan') },
            { id: FilterOperator.LTE, text: this.translation.translate('Filter.LessThanOrEqual') },
            { id: FilterOperator.GTE, text: this.translation.translate('Filter.GreaterThanOrEqual') },
            { id: FilterOperator.SW, text: this.translation.translate('Filter.StartWith') },
            { id: FilterOperator.EW, text: this.translation.translate('Filter.EndWith') },
            { id: FilterOperator.CT, text: this.translation.translate('Filter.Contains') },
        ];
        for (let index = 0; index < this.filterColumnsData.length; index++) {
            this.filterColumnsData[index].operators = this.getAllowFilterOperator(index);
            this.filterErrorMessage.push("");
        }
    }

    ngOnChanges() {
    }

    ngOnInit() {
        this.getActiveUserQueryFilters();
    }

    changeFilterSort(sortField: string) {
        this.filterQueryOptions.sort = sortField;
    }

    onFilterColumnChanged(e: any, index: number): void {
        this.setFilterStatus(e.value, index, "");
        this.filters[index].field = e.value;
        const operIndex = this.filterColumnsData.findIndex(x => x.id == e.value);
        this.filterColumnsData[index].operators = this.getAllowFilterOperator(operIndex);
        this.filterColumnStartValue[index] = e.value;
    }

    getFilterOperators(fieldName) {
        let pos = this.filterColumnsData.findIndex(x => x.id === fieldName)
        return this.getAllowFilterOperator(pos < 0 ? 0 : pos);
    }

    onFilterValueChanged(e: any,  index: number): void {
        this.filters[index].value = e.value;
    }

    onFilterOperatorChanged(e: any, index: number): void {
        this.filters[index].operator = e.value;
        this.filterColumnsData[index].currentOper = e.value;
    }

    isFilterSlotActivated(slotNumber: number) {
        return this.filterSlotsActivated.findIndex(x => x == slotNumber) >= 0;
    }

    isFilterAddButtonEnabled(index: number) {
        if (index < this.filters.length - 1) {
            return false
        }

        const f = this.filters[index];
        const columnData = this.filterColumnsData.find(x => x.id == f.field);
        if (columnData === undefined) {
            this.updateFilterError(index, null); //ignore if not field selected.
            return false;
        }

        if (f.value === "") {
            this.updateFilterError(index, this.translation.translate('Filter.MatchValueRequired')); 
            return false;
        }

        switch (columnData.type) {
            case "datetime":
                if (f.field === "StartActivity" && f.value === "IMMEDIATE") {
                    this.updateFilterError(index, null); 
                }
                else if (f.field === "EndValidity" && f.value === "EndValidity") {
                    this.updateFilterError(index, null); 
                } else {
                    this.filterErrorMessage[index] = this.translation.translate('Filter.InvalidDateFormat');
                    if ($.isNumeric(f.value) && f.value.length === 10) {
                        let momentDate = moment(f.value, "YYMMDDHHmm");
                        if (momentDate.isValid()) {
                            this.updateFilterError(index, null); 
                        }
                    }
                }
                break;
            case "combobox":
                this.updateFilterError(index, null); 
                break;
            case "number":
                if ($.isNumeric(f.value)) {
                    if (f.field !== "EMPTY") {
                        this.updateFilterError(index, null); 
                    }
                } else {
                    this.updateFilterError(index, this.translation.translate('Filter.NumericValueRequired')); 
                }
                break;
            default: //text
                if (f.value !== "" && f.field !== "EMPTY") {
                    this.updateFilterError(index, null); 
                }
                break;
        }

        return this.filterErrorMessage[index] === null;
    }
    
    isApplyFilterButtonDisabled() {
        if (this.parent && !this.parent.isRunReportButtonEnabled()) {
            return true;
        }

        return !this.isFilterAddButtonEnabled(this.filters.length - 1); 
    }

    addFilterCondition() {
        this.filters.push({
            field: "",
            operator: FilterOperator.EQ,
            value: "",
            type: "text"
        });

        const index = this.filters.length - 1;
        this.filterColumnStartValue.push(this.filterColumnsData[0].id);
        this.filterColumnsData[index].currentOper = FilterOperator.EQ;
    }

    removeFilterCondition(index: number) {
        this.filters.splice(index, 1);
        this.filterColumnStartValue.splice(index, 1);
        //let pos = this.filterColumnStartValue.length >= index ? this.filterColumnsData.findIndex(x => x.id === this.filterColumnStartValue[index]) : 0;
        this.filterColumnsData[index].currentOper = FilterOperator.EQ;
    }

    activateFilter(page: number) {
        page = page || 1;
        this.memStorageService.save(this.memStorageService.FILTER_APPLIED_KEY, true);
        this.memStorageService.save(this.memStorageService.NOTAM_FILTER_KEY, this.filters);

        this.runFilter(true, page, this.pageSize);
    }

    loadFilterState() {
        this.filterQueryOptions = this.memStorageService.get(this.memStorageService.NOTAM_QUERY_OPTIONS_KEY);
        this.updateFilters(this.memStorageService.get(this.memStorageService.NOTAM_FILTER_KEY));
    }

    runFilter(togglePannel: boolean, page?: number, pageSize?: number) {
        this.filterQueryOptions.pageSize = pageSize || this.filterQueryOptions.pageSize;
        this.filterQueryOptions.page = page || this.filterQueryOptions.page;
        this.memStorageService.save(this.memStorageService.NOTAM_QUERY_OPTIONS_KEY, this.filterQueryOptions);

        this.showProgressBar();
        this.dataService.filterNotams(this.filters, this.filterQueryOptions)
            .subscribe(result => {
                if (togglePannel) {
                    this.toogleFilterPanel();
                }
                this.hideProgressBar();
                this.onFilterChanged.emit(
                    {
                        isFilterApplied: true,
                        data: result
                    });
            }, error => {
                this.hideProgressBar();
                this.onFilterChanged.emit(
                    {
                        error: error
                    });
            });
    }

    runReportFilter(togglePannel: boolean, startdate, enddate ) {
        this.filterQueryOptions.pageSize = 100000000;
        this.filterQueryOptions.page = 1;
        this.memStorageService.save(this.memStorageService.REPORT_NOTAM_QUERY_OPTIONS_KEY, this.filterQueryOptions);
        this.showProgressBar();
        this.dataService.filterReportNotams(this.filters, this.filterQueryOptions, startdate, enddate)
            .subscribe(result => {
                if (togglePannel) {
                    this.toogleFilterPanel();
                }
                this.hideProgressBar();
                this.onFilterChanged.emit(
                    {
                        isFilterApplied: true,
                        data: result
                    });
            }, error => {
                this.hideProgressBar();
                this.onFilterChanged.emit(
                    {
                        error: error
                    });
            });
    }

    clearFilter(event: any, openPannel: boolean) {
        if (event) {
            event.preventDefault();
        }
        this.filters = [];
        this.addFilterCondition();

        //if (openPannel !== false) {
        //     this.toogleFilterPanel();
        //}

        this.memStorageService.remove(this.memStorageService.FILTER_APPLIED_KEY);
        this.memStorageService.remove(this.memStorageService.NOTAM_FILTER_KEY);

        for (let i = 0; i < this.filterColumnsData.length; i++) {
            this.filterColumnsData[i].operators = this.getAllowFilterOperator(i);
            this.filterColumnsData[i].currentOper = FilterOperator.EQ;
        }

        this.filterColumnStartValue = [];
        this.filterColumnStartValue.push(this.filterColumnsData[0].id);

        this.onFilterChanged.emit({ isFilterApplied: false });
        this.filterSlotsActive = -1;
    }

    getUserQueryFilter(slotNumber: number) {
        this.dataService.getQueryFilter(slotNumber, this.filterType)
            .subscribe(result => {
                this.updateFilters(result);
                this.filterSlotsActive = slotNumber;
            }, error => {
                //TODO: ERROR
            });
    }

    getActiveUserQueryFilters() {
        this.dataService.getActiveQueryFilters(this.filterType)
            .subscribe(result => {
                this.filterSlotsActivated= result;
            }, error => {
                //TODO: ERROR
            });
    }

    saveUserQueryFilter(event: any, slotNumber: number): void {
        event.preventDefault();
        let self = this;
        this.dataService.saveQueryFilter(slotNumber, this.filters, this.filterType)
            .subscribe(result => {
                self.filterSlotsActive = slotNumber;
                if (self.filterSlotsActivated.findIndex(x => x == slotNumber) < 0) {
                    self.filterSlotsActivated.push(slotNumber);
                }
            }, error => {
                //TODO: ERROR
            });
    }

    deleteUserQueryFilter(event: any, slotNumber: number): void {
        event.preventDefault();
        let self = this;
        this.dataService.deleteQueryFilter(slotNumber, this.filterType)
            .subscribe(result => {
                self.filterSlotsActive = -1;
                const index = self.filterSlotsActivated.findIndex(x => x == slotNumber);
                if (index >= 0) {
                    self.filterSlotsActivated.splice(index, 1);
                }
            }, error => {
                //TODO: ERROR
            });
    }

    private updateFilterError(index: number, errorMessage: string) {
        if (this.filterErrorMessage[index] !== errorMessage) {
            this.filterErrorMessage[index] = errorMessage;
        }
    }

    private updateFilters(filters: IFilterCriteria[]) {
        this.filterColumnStartValue = [];
        this.filters = filters;
        for (let i = 0; i < this.filters.length; i++) {
            this.setFilterStatus(filters[i].field, i, filters[i].value); 
            this.filterColumnStartValue.push(filters[i].field);
            this.filterColumnsData[i].currentOper = <string>this.filters[i].operator;
        }
    }

    private setFilterStatus(field, index, currentValue) {
        switch (field) {
            case "EMPTY":
            case "NotamId":
            case "ReferredNotamId":
            case "ItemA":
            case "ItemE":
            case "Originator":
            case "Operator":
            case "ModifiedByUsr":
            case "Code23":
            case "Code45":
            case "Code45":
                this.setNonComboboxFilterData(currentValue, index, "text");
                break;
            case "Radius":
            case "LowerLimit":
            case "UpperLimit":
                this.setNonComboboxFilterData(currentValue, index, "number");
                break;
            case "Published":
            case "StartActivity":
            case "EndValidity":
                this.setNonComboboxFilterData(currentValue, index, "datetime");
                break;
            case "Type":
                this.setComboboxFilterData(["N", "C", "R"], index, currentValue );
                break;
            case "Traffic":
                this.setComboboxFilterData(["I", "V", "IV"], index, currentValue);
                break;
            case "Scope":
                this.setComboboxFilterData(["A", "AE", "E", "K", "W"], index, currentValue);
                break;
            case "Purpose":
                this.setComboboxFilterData(["B", "BO", "K", "M", "NBO"], index, currentValue);
                break;
            case "Series":
                this.setComboboxFilterData(["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"], index, currentValue);
                break;
        }
    }

    private setComboboxFilterData(data, index, currentValue) {
        this.filterColumnsData[index].type = "combobox";
        this.filterColumnsData[index].operators = this.getAllowFilterOperator(index);
        this.filterColumnsData[index].values = data;

        currentValue = currentValue !== "" ? currentValue : this.filterColumnsData[index].values[0];
        this.filterColumnsData[index].currentValue = currentValue;

        this.filters[index].type = "combobox";
        this.filters[index].valueList = data;
        this.filters[index].value = currentValue;
    }

    private setNonComboboxFilterData(currentValue, index, type) {
        this.filterColumnsData[index].type = type;
        this.filterColumnsData[index].operators = this.getAllowFilterOperator(index);
        this.filterColumnsData[index].currentValue = currentValue;

        this.filters[index].type = type;
        //this.filters[index].valueList = data;
        this.filters[index].value = currentValue;
    }

    private getAllowFilterOperator(index: number) {
        let filterColumnsData: Array<any> = [];
        let filterName = this.filterColumnsData[index].id;
        switch (filterName) {
            case "NotamId":
            case "ReferredNotamId":
            case "ItemA":
            case "ItemE":
            case "Code23":
            case "Code45":
                filterColumnsData = this.addTextFieldFilters(filterColumnsData);
                break;
            case "StartActivity":
            case "EndValidity":
            case "Published":
                filterColumnsData = this.addDateFieldFilters(filterColumnsData);
                break;
            case "Series":
            case "Type":
                filterColumnsData = this.addBooleanFieldFilters(filterColumnsData);
                break;
            case "Traffic":
            case "Purpose":
            case "Scope":
                filterColumnsData = this.addBooleanExFieldFilters(filterColumnsData);
                break;
            case "Radius":
            case "LowerLimit":
            case "UpperLimit":
                filterColumnsData = this.addNumberFieldFilters(filterColumnsData);
                break;
            case "Operator":
            case "Originator":
            case "ModifiedByUsr":
                filterColumnsData = this.addTextFieldFilters(filterColumnsData);
                break;
        }

        return filterColumnsData;
    }

    private addAllFilters(filterColumnsData) {
        for (let i = 0; i < this.filterOperatorsData.length; i++) {
            filterColumnsData.push(this.filterOperatorsData[i]);
        }

        return filterColumnsData;
    }

    private addTextFieldFilters(filterColumnsData) {
        filterColumnsData.push(this.filterOperatorsData[0]);
        filterColumnsData.push(this.filterOperatorsData[1]);
        filterColumnsData.push(this.filterOperatorsData[6]);
        filterColumnsData.push(this.filterOperatorsData[7]);
        filterColumnsData.push(this.filterOperatorsData[8]);

        return filterColumnsData;
    }

    private addBooleanFieldFilters(filterColumnsData) {
        filterColumnsData.push(this.filterOperatorsData[0]);
        filterColumnsData.push(this.filterOperatorsData[1]);

        return filterColumnsData;
    }

    private addBooleanExFieldFilters(filterColumnsData) {
        filterColumnsData.push(this.filterOperatorsData[0]);
        filterColumnsData.push(this.filterOperatorsData[1]);
        filterColumnsData.push(this.filterOperatorsData[8]);

        return filterColumnsData;
    }


    private addDateFieldFilters(filterColumnsData) {
        filterColumnsData.push(this.filterOperatorsData[0]);
        filterColumnsData.push(this.filterOperatorsData[1]);
        filterColumnsData.push(this.filterOperatorsData[2]);
        filterColumnsData.push(this.filterOperatorsData[3]);
        filterColumnsData.push(this.filterOperatorsData[4]);
        filterColumnsData.push(this.filterOperatorsData[5]);

        return filterColumnsData;
    }

    private addNumberFieldFilters(filterColumnsData) {
        filterColumnsData.push(this.filterOperatorsData[0]);
        filterColumnsData.push(this.filterOperatorsData[1]);
        filterColumnsData.push(this.filterOperatorsData[2]);
        filterColumnsData.push(this.filterOperatorsData[3]);
        filterColumnsData.push(this.filterOperatorsData[4]);
        filterColumnsData.push(this.filterOperatorsData[5]);

        return filterColumnsData;
    }

    private showProgressBar() {
        if (this.parent) {
            this.parent.loadingData = true;
        }
    }

    private hideProgressBar() {
        if (this.parent) {
            this.parent.loadingData = false;
        }
    }


    private toogleFilterPanel(): void {
        $(".panel-toggle").toggleClass("closed").parents(".panel:first").find(".panel-content").slideToggle();
    }
}

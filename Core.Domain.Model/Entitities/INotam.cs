﻿using NavCanada.Core.Domain.Model.Enums;
using System;

namespace NavCanada.Core.Domain.Model.Entitities
{
    public interface INotam
    {
        string Series { get; }
        int Number { get; }
        int Year { get; }
        NotamType? ReferenceType { get; }
        string ReferredSeries { get; }
        int ReferredNumber { get; }
        int ReferredYear { get; }
        string Fir { get; }
        string Code23 { get; }
        string Code45 { get; }
        string Traffic { get; }
        string Purpose { get; }
        string Scope { get; }
        int LowerLimit { get; }
        int UpperLimit { get; }
        string Coordinates { get; }
        int Radius { get; }
        string ItemA { get; }
        DateTime? StartActivity { get; }
        DateTime? EndValidity { get; }
        bool Estimated { get; }
        string ItemD { get; }
        string ItemE { get; }
        string ItemEFrench { get; }
        string ItemF { get; }
        string ItemG { get; }
        string ItemX { get; }
        string Operator { get; }
        string Originator { get; }
        string OriginatorEmail { get; }
        string OriginatorPhone { get; }
        bool Immediate { get; }
        bool Permanent { get; }
    }
}

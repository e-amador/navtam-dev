﻿using NavCanada.Core.Domain.Model.Entitities;
using NDS.Relay;
using System.Collections.Generic;
using NavCanada.Core.TaskEngine.Contracts;
using NavCanada.Core.Common.Contracts;

namespace NavCanada.Core.TaskEngine.NdsTasks
{
    public enum NdsTaskType
    {
        AftnConfirmation,
        AftnError,
        AftnMessage,
        AftnRqn,
        AftnRql,
        Unknown
    }

    public class NdsTaskHandler
    {
        static NdsTaskHandler()
        {
            _taskHandlers = new Dictionary<NdsTaskType, INdsTask>()
            {
                { NdsTaskType.AftnConfirmation, new AftnConfirmationTask() },
                { NdsTaskType.AftnError, new AftnErrorTask() },
                { NdsTaskType.AftnRqn, new AftnRqnTask() },
                { NdsTaskType.AftnRql, new AftnRqlTask() },
                { NdsTaskType.AftnMessage, new AftnMessageTask() }
            };
        }

        public static void Execute(NdsIncomingRequest request, IClusterStorage clusterStorage, ILogger logger)
        {
            var taskType = GetTaskTypeFromRequest(request);
            if (_taskHandlers.ContainsKey(taskType))
            {
                _taskHandlers[taskType].Execute(request, clusterStorage, logger);
            }
        }

        private static NdsTaskType GetTaskTypeFromRequest(NdsIncomingRequest request)
        {
            if (RelayValues.AFTN_CONFIRMATION.Equals(request.MessageType))
                return NdsTaskType.AftnConfirmation;

            if (RelayValues.AFTN_ERROR.Equals(request.MessageType))
                return NdsTaskType.AftnError;

            if (RelayValues.AFTN_MESSAGE.Equals(request.MessageType))
            {
                AftnRequestType requestType;
                if (TryParseAftnRequestMessage(request, out requestType))
                {
                    if (requestType == AftnRequestType.Rqn)
                        return NdsTaskType.AftnRqn;

                    if (requestType == AftnRequestType.Rql)
                        return NdsTaskType.AftnRql;
                }

                return NdsTaskType.AftnMessage;
            }

            // handle unknowns
            return NdsTaskType.Unknown;
        }

        private static bool TryParseAftnRequestMessage(NdsIncomingRequest request, out AftnRequestType requestType)
        {
            // handle metadata of message (MSG_TYPE)
            requestType = AftnRequestType.Unknown;

            var message = AftnMessageParser.ParseRequestMessage(request.Body);
            if (message != null)
            {
                requestType = message.Type;
                return true;
            }

            return false;
        }

        private static Dictionary<NdsTaskType, INdsTask> _taskHandlers;
    }
}
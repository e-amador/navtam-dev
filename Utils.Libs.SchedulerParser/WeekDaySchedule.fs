﻿module Utils.Libs.SchedulerParser.WeekDay

open FParsec
open FParsec.Pipes
open Common

type Parser ()=

    static member private SingleWeekDay : Parser<DayPeriod, unit> = 
        %% +.Filter.WeekDay -- ' ' -? ws -%> fun x -> { startDay = x; endDay = x}

    static member private SingleRangeWeekDay : Parser<DayPeriod, unit> = 
        %% +.Filter.WeekDay -- '-' -? ws -- +.Filter.WeekDay -- ws -%> fun x y -> { startDay = x; endDay = y }

    static member private MultipleWeekDays : Parser<DayPeriod[], unit> = 
        Parser.SingleWeekDay * qty.[1..] |>> (fun x -> x.ToArray();)

    static member private MultipleRangeWeekDays : Parser<DayPeriod[], unit> = 
        Parser.SingleRangeWeekDay * qty.[1..] |>> (fun x -> x.ToArray();)  

    static member    private WeekDays : Parser<DayPeriod[], unit> = 
        let comb = %[Parser.MultipleWeekDays; Parser.MultipleRangeWeekDays ] 
        comb * qty.[1..] |>> 
            (fun dayPeriods -> [| for dps in dayPeriods do for dp in dps -> dp |])

    static member private WeekDayParser : Parser<ParserResult, unit> = 
        %% ws -- +.Parser.WeekDays 
        -- ws -- +.Filter.MultiplePeriodInMinutes -? ws -%> 
            fun x y -> 
                { 
                    case= WeekDay; 
                    subcase= None; 
                    months= [||]; 
                    days= x; 
                    included= true; 
                    minutes= y;  
                }

    static member Pipeline : Parser<ParserResult, unit> = 
        Parser.WeekDayParser 

    static member Parse( str : string ) : ParserResult<ParserResult, unit> = 
        run Parser.Pipeline (str + " ")






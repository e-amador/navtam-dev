﻿using NavCanada.Core.Common.Common;
using NavCanada.Core.Data.NotamWiz.Contracts.Repositories;
using NavCanada.Core.Data.NotamWiz.EF.Extensions;
using NavCanada.Core.Domain.Model.Dtos;
using NavCanada.Core.Domain.Model.Entitities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace NavCanada.Core.Data.NotamWiz.EF.Repositories
{
    public class OrganizationRepo : Repository<Organization>, IOrganizationRepo
    {
        public OrganizationRepo(DbContext dbContext) : base(dbContext)
        {
        }

        public Task<List<OrgPartialDto>> GetAllPartialAsync()
        {
            return DbSet.Where(e => !e.Deleted).Select(e => new OrgPartialDto { Id = e.Id, Name = e.Name }).ToListAsync();
        }

        public Task<Organization> GetByIdAsync(int id)
        {
            return DbSet.FirstOrDefaultAsync(o => o.Id == id);
        }

        public Task<Organization> GetByIdAsync(int id, bool shouldIncludeUsers)
        {
            if (!shouldIncludeUsers)
            {
                return DbSet.FirstOrDefaultAsync(o => o.Id == id);
            }
            else
            {
                return DbSet.Include("Users").FirstOrDefaultAsync(e => e.Id == id);
            }
        }

        public async Task<bool> IsNameAvailableAsync(int id, string name)
        {
            return await DbSet.AnyAsync(e => e.Id != id && e.Name == name) == false;
        }

        public Task<Organization> GetByNameAsync(string name)
        {
            return DbSet.FirstOrDefaultAsync(e => e.Name == name);
        }

        public async Task<int> GetAllCountAsync()
        {
            return await DbSet.CountAsync(e => !e.Deleted);
        }

        public async Task<int> GetAllCountAsync(QueryOptions queryOptions)
        {
            var filter = queryOptions.FilterValue;
            return await DbSet.CountAsync(e => !e.Deleted && 
                    (e.Name.Contains(filter) || e.Address.Contains(filter) || e.EmailAddress.Contains(filter) ||
                    e.TypeOfOperation.Contains(filter) || e.HeadOffice.Contains(filter))
                    );
        }

        public async Task<List<OrgDto>> GetAllAsync(QueryOptions queryOptions)
        {
            var filter = queryOptions.FilterValue;
            var orgs = await DbSet
                .Where(e => !e.Deleted &&
                    (e.Name.Contains(filter) || e.Address.Contains(filter) || e.EmailAddress.Contains(filter) ||
                    e.TypeOfOperation.Contains(filter) || e.HeadOffice.Contains(filter)))
                .ApplySort(queryOptions.Sort)
                .Skip(queryOptions.PageSize * (queryOptions.Page - 1))
                .Take(queryOptions.PageSize)
                .ToListAsync();

            var dtos = new List<OrgDto>();

            foreach (var org in orgs)
            {
                dtos.Add(new OrgDto
                {
                    Id = org.Id,
                    Name = org.Name,
                    Address = org.Address,
                    EmailAddress = org.EmailAddress,
                    EmailDistribution = org.EmailDistribution,
                    Telephone = org.Telephone,
                    TypeOfOperation = org.TypeOfOperation
                });
            }
            return dtos;
        }

        public Task<List<Organization>> GetOrgsByIdsAsync(int[] idsArray)
        {
            return DbSet.Where(o => idsArray.Contains(o.Id)).ToListAsync();
        }

        public Task<Organization> GetByUserIdAsync(string userId, bool shouldIncludeUsers)
        {
            var ctx = DbContext as AppDbContext;

            if (ctx == null)
                throw new NullReferenceException("Unable to cast DbContext to AppDbContext");

            var query = DbSet
                .Join(ctx.Users,
                    o => o.Id,
                    u => u.OrganizationId,
                    (o, u) => new
                    {
                        user = u,
                        org = o
                    })
                .Where(r => r.user.Id == userId);

            return shouldIncludeUsers
                ? query.Select(r => r.org).Include(r => r.Users).SingleOrDefaultAsync()
                : query.Select(r => r.org).SingleOrDefaultAsync();
        }

        public Task<List<Organization>> GetAllAsyncWithDoa()
        {
            return DbSet.Include("AssignedDoa").ToListAsync();
        }

        public async Task<List<OrgDto>> GetOrganizationsByNsdId(int nsdId)
        {
            var ctx = DbContext as AppDbContext;

            if (ctx == null) throw new NullReferenceException("Unable to cast DbContext to AppDbContext");

            var orgs = await ctx.OrganizationNsds.Where(x => x.NsdCategoryId == nsdId && x.NsdCategory.Operational).ToListAsync();

            List<OrgDto> orgDto = new List<OrgDto>();
            foreach (var org in orgs)
            {
                orgDto.Add(new OrgDto
                {
                    Id = org.Organization.Id,
                    Name = org.Organization.Name,
                    Address = org.Organization.Address,
                    EmailAddress = org.Organization.EmailAddress,
                    EmailDistribution = org.Organization.EmailDistribution,
                    HeadOffice = org.Organization.HeadOffice,
                    Telephone = org.Organization.Telephone,
                    TypeOfOperation = org.Organization.TypeOfOperation,
                    Type = org.Organization.Type,
                });
            }

            return orgDto;
        }

        public async Task<OrgDto> GetOrgInfoAsync(int id)
        {
            var org = await DbSet.FirstOrDefaultAsync(e => e.Id == id);
            if (org != null)
            {
                var doaIds = await AppDbContext.OrganizationDoas.Where(od => od.OrganizationId == id).Select(od => od.DoaId).ToListAsync();
                var nsdIds = await AppDbContext.OrganizationNsds.Where(on => on.OrganizationId == id).Select(on => on.NsdCategoryId).ToListAsync();
                return new OrgDto
                {
                    Id = org.Id,
                    Name = org.Name,
                    Address = org.Address,
                    EmailAddress = org.EmailAddress,
                    EmailDistribution = org.EmailDistribution,
                    HeadOffice = org.HeadOffice,
                    Telephone = org.Telephone,
                    TypeOfOperation = org.TypeOfOperation,
                    Type = org.Type,
                    DoaIds = doaIds,
                    NsdIds = nsdIds
                };
            }
            return null;
        }
    }
}
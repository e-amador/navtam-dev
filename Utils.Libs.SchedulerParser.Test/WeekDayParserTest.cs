﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScheduleParserTest;

namespace Utils.Libs.SchedulerParser.ScheduleParserTest
{
    [TestClass]
    public class WeekDayParserTest 
    {
        
        [TestMethod] 
        public void SchedulerParser_WeekDay_ShouldReturnSuccess_WhenSingleDay()
        {
            //TODO: This specific case requires space at the end. Needs to improve the matching 
            var result = Parser.Agent.Run("SUN ");
            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.WeekDay);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.None);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 0);
            Assert.IsTrue(parserResult.months.Length == 0);
            Assert.IsTrue(parserResult.days.Length == 1);
            Assert.IsTrue(parserResult.days[0].startDay == 0);
            Assert.IsTrue(parserResult.days[0].endDay == 0);
        }

        [TestMethod]
        public void SchedulerParser_TimeOnly_ShouldReturnSuccess_WhenOnlyWeekDayWithNoneTrailingSpacesArePassed()
        {
            var result = Parser.Agent.Run("SUN");
            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.WeekDay);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.None);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 0);
            Assert.IsTrue(parserResult.months.Length == 0);
            Assert.IsTrue(parserResult.days.Length == 1);
            Assert.IsTrue(parserResult.days[0].startDay == 0);
            Assert.IsTrue(parserResult.days[0].endDay == 0);
        }


        [TestMethod]
        public void SchedulerParser_WeekDay_ShouldReturnSuccess_WhenMultipleDays()
        {
            //TODO: This specific case requires space at the end. Needs to improve the matching 
            var result = Parser.Agent.Run("MON TUE WED");
            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.WeekDay);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.None);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 0);
            Assert.IsTrue(parserResult.months.Length == 0);
            Assert.IsTrue(parserResult.days.Length == 3);
            Assert.IsTrue(parserResult.days[0].startDay == 1);
            Assert.IsTrue(parserResult.days[0].endDay == 1);
            Assert.IsTrue(parserResult.days[1].startDay == 2);
            Assert.IsTrue(parserResult.days[1].endDay == 2);
            Assert.IsTrue(parserResult.days[2].startDay == 3);
            Assert.IsTrue(parserResult.days[2].endDay == 3);
        }

        [TestMethod]
        public void SchedulerParser_WeekDay_ShouldReturnSuccess_WhenMultipleDaysAndSingleMinutesRange()
        {
            var result = Parser.Agent.Run("THU FRI 0101-0202");
            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.WeekDay);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.None);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.months.Length == 0);
            Assert.IsTrue(parserResult.minutes.Length == 1);
            Assert.IsTrue(parserResult.minutes[0].start == 61);
            Assert.IsTrue(parserResult.minutes[0].finish == 122);
            Assert.IsTrue(parserResult.days.Length == 2);
            Assert.IsTrue(parserResult.days[0].startDay == 4);
            Assert.IsTrue(parserResult.days[0].endDay == 4);
            Assert.IsTrue(parserResult.days[1].startDay == 5);
            Assert.IsTrue(parserResult.days[1].endDay == 5);
        }

        [TestMethod]
        public void SchedulerParser_WeekDay_ShouldReturnSuccess_WhenMultipleGropuDaysAndSingleMinutesRange()
        {
            var result = Parser.Agent.Run("THU 0101-0202 FRI 0101-0202");
            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.WeekDay);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.None);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.months.Length == 0);
            Assert.IsTrue(parserResult.minutes.Length == 1);
            Assert.IsTrue(parserResult.minutes[0].start == 61);
            Assert.IsTrue(parserResult.minutes[0].finish == 122);
            Assert.IsTrue(parserResult.days.Length == 2);
            Assert.IsTrue(parserResult.days[0].startDay == 4);
            Assert.IsTrue(parserResult.days[0].endDay == 4);
            Assert.IsTrue(parserResult.days[1].startDay == 5);
            Assert.IsTrue(parserResult.days[1].endDay == 5);
        }


        [TestMethod]
        public void SchedulerParser_WeekDay_ShouldReturnSuccess_WhenMultipleDaysAndMultipleMinutesRange()
        {
            var result = Parser.Agent.Run("THU FRI 0101-0202 SR MINUS10-SS PLUS10");
            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.WeekDay);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.None);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.months.Length == 0);
            Assert.IsTrue(parserResult.minutes.Length == 2);
            Assert.IsTrue(parserResult.minutes[0].start == 61);
            Assert.IsTrue(parserResult.minutes[0].finish == 122);
            Assert.IsTrue(parserResult.minutes[1].start == Common._SUNRISE_IN_MINUTES_ + 10);
            Assert.IsTrue(parserResult.minutes[1].finish == Common._SUNSET_IN_MINUTES_ + 10);
            Assert.IsTrue(parserResult.days.Length == 2);
            Assert.IsTrue(parserResult.days[0].startDay == 4);
            Assert.IsTrue(parserResult.days[0].endDay == 4);
            Assert.IsTrue(parserResult.days[1].startDay == 5);
            Assert.IsTrue(parserResult.days[1].endDay == 5);
        }


        [TestMethod]
        public void SchedulerParser_WeekDay_ShouldReturnSuccess_WhenSingleRangeDay()
        {
            var result = Parser.Agent.Run("SUN-SAT");
            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.WeekDay);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.None);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 0);
            Assert.IsTrue(parserResult.months.Length == 0);
            Assert.IsTrue(parserResult.days.Length == 1);
            Assert.IsTrue(parserResult.days[0].startDay == 0);
            Assert.IsTrue(parserResult.days[0].endDay == 6);
        }

        [TestMethod]
        public void SchedulerParser_WeekDay_ShouldReturnSuccess_WhenMultipleRangeDays()
        {
            var result = Parser.Agent.Run("SUN-TUE THU-SAT");
            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.WeekDay);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.None);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 0);
            Assert.IsTrue(parserResult.months.Length == 0);
            Assert.IsTrue(parserResult.days.Length == 2);
            Assert.IsTrue(parserResult.days[0].startDay == 0);
            Assert.IsTrue(parserResult.days[0].endDay == 2);
            Assert.IsTrue(parserResult.days[1].startDay == 4);
            Assert.IsTrue(parserResult.days[1].endDay == 6);
        }

        [TestMethod]
        public void SchedulerParser_WeekDay_ShouldReturnSuccess_WhenMultipleRangeDaysAndSingleMinutesRange()
        {
            var result = Parser.Agent.Run("SUN-TUE THU-SAT 0101-0202");
            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.WeekDay);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.None);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 1);
            Assert.IsTrue(parserResult.minutes[0].start == 61);
            Assert.IsTrue(parserResult.minutes[0].finish == 122);
            Assert.IsTrue(parserResult.months.Length == 0);
            Assert.IsTrue(parserResult.days.Length == 2);
            Assert.IsTrue(parserResult.days[0].startDay == 0);
            Assert.IsTrue(parserResult.days[0].endDay == 2);
            Assert.IsTrue(parserResult.days[1].startDay == 4);
            Assert.IsTrue(parserResult.days[1].endDay == 6);
        }

        [TestMethod]
        public void SchedulerParser_WeekDay_ShouldReturnSuccess_WhenMultipleRangeDaysAndMultipleMinutesRange()
        {
            var result = Parser.Agent.Run("SUN-TUE THU-SAT 0101-0202 SR MINUS10-SS PLUS10");
            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.WeekDay);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.None);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 2);
            Assert.IsTrue(parserResult.minutes[0].start == 61);
            Assert.IsTrue(parserResult.minutes[0].finish == 122);
            Assert.IsTrue(parserResult.minutes[1].start == Common._SUNRISE_IN_MINUTES_ + 10);
            Assert.IsTrue(parserResult.minutes[1].finish == Common._SUNSET_IN_MINUTES_ + 10);
            Assert.IsTrue(parserResult.months.Length == 0);
            Assert.IsTrue(parserResult.days.Length == 2);
            Assert.IsTrue(parserResult.days[0].startDay == 0);
            Assert.IsTrue(parserResult.days[0].endDay == 2);
            Assert.IsTrue(parserResult.days[1].startDay == 4);
            Assert.IsTrue(parserResult.days[1].endDay == 6);
        }

        [TestMethod]
        public void SchedulerParser_WeekDay_ShouldReturnSuccess_WhenSingleDayAndSingleRangeDay()
        {
            var result = Parser.Agent.Run("SUN WED-FRI");
            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.WeekDay);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.None);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 0);
            Assert.IsTrue(parserResult.months.Length == 0);
            Assert.IsTrue(parserResult.days.Length == 2);
            Assert.IsTrue(parserResult.days[0].startDay == 0);
            Assert.IsTrue(parserResult.days[0].endDay == 0);
            Assert.IsTrue(parserResult.days[1].startDay == 3);
            Assert.IsTrue(parserResult.days[1].endDay == 5);
        }

        [TestMethod]
        public void SchedulerParser_WeekDay_ShouldReturnSuccess_WhenMultipleDaysAndMultipleRangeDays()
        {
            var result = Parser.Agent.Run("SUN MON SUN-TUE THU-SAT");
            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.WeekDay);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.None);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.minutes.Length == 0);
            Assert.IsTrue(parserResult.months.Length == 0);
            Assert.IsTrue(parserResult.days.Length == 4);
            Assert.IsTrue(parserResult.days[0].startDay == 0);
            Assert.IsTrue(parserResult.days[0].endDay == 0);
            Assert.IsTrue(parserResult.days[1].startDay == 1);
            Assert.IsTrue(parserResult.days[1].endDay == 1);
            Assert.IsTrue(parserResult.days[2].startDay == 0);
            Assert.IsTrue(parserResult.days[2].endDay == 2);
            Assert.IsTrue(parserResult.days[3].startDay == 4);
            Assert.IsTrue(parserResult.days[3].endDay == 6);
        }

        [TestMethod]
        public void SchedulerParser_WeekDay_ShouldReturnSuccess_WhenMultipleDaysAndMultipleRangeDaysAndMultipleMinutes()
        {
            var result = Parser.Agent.Run("SUN MON SUN-TUE THU-SAT 0101-0202 SR MINUS10-SS PLUS10");
            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.WeekDay);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.None);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.months.Length == 0);
            Assert.IsTrue(parserResult.minutes.Length == 2);
            Assert.IsTrue(parserResult.minutes[0].start == 61);
            Assert.IsTrue(parserResult.minutes[0].finish == 122);
            Assert.IsTrue(parserResult.minutes[1].start == Common._SUNRISE_IN_MINUTES_ + 10);
            Assert.IsTrue(parserResult.minutes[1].finish == Common._SUNSET_IN_MINUTES_ + 10);
            Assert.IsTrue(parserResult.days.Length == 4);
            Assert.IsTrue(parserResult.days[0].startDay == 0);
            Assert.IsTrue(parserResult.days[0].endDay == 0);
            Assert.IsTrue(parserResult.days[1].startDay == 1);
            Assert.IsTrue(parserResult.days[1].endDay == 1);
            Assert.IsTrue(parserResult.days[2].startDay == 0);
            Assert.IsTrue(parserResult.days[2].endDay == 2);
            Assert.IsTrue(parserResult.days[3].startDay == 4);
            Assert.IsTrue(parserResult.days[3].endDay == 6);
        }

        [TestMethod]
        public void SchedulerParser_WeekDay_ShouldReturnSuccess_WhenMultipleDaysAndMultipleRangeDaysWithoudMinutes()
        {
            var result = Parser.Agent.Run("SUN MON SUN-TUE THU-SAT");
            Assert.IsTrue(result.IsSuccess);

            var parserResult = ParserUtils.CastSuccess(result);
            Assert.IsTrue(parserResult != null);
            Assert.IsTrue(parserResult.@case == Common.ParserCase.WeekDay);
            Assert.IsTrue(parserResult.subcase == Common.ParserSubcase.None);
            Assert.IsTrue(parserResult.included);
            Assert.IsTrue(parserResult.months.Length == 0);
            Assert.IsTrue(parserResult.minutes.Length == 0);
            Assert.IsTrue(parserResult.days.Length == 4);
            Assert.IsTrue(parserResult.days[0].startDay == 0);
            Assert.IsTrue(parserResult.days[0].endDay == 0);
            Assert.IsTrue(parserResult.days[1].startDay == 1);
            Assert.IsTrue(parserResult.days[1].endDay == 1);
            Assert.IsTrue(parserResult.days[2].startDay == 0);
            Assert.IsTrue(parserResult.days[2].endDay == 2);
            Assert.IsTrue(parserResult.days[3].startDay == 4);
            Assert.IsTrue(parserResult.days[3].endDay == 6);
        }

        [TestMethod]
        public void SchedulerParser_WeekDay_ShouldReturnFailure_WhenWeekDayInvalid()
        {
            var result = Parser.Agent.Run("SUT ");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        [TestMethod]
        public void SchedulerParser_WeekDay_ShouldReturnFailure_WhenMissingSideRangeDay()
        {
            var result = Parser.Agent.Run("SAT-");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        [TestMethod]
        public void SchedulerParser_WeekDay_ShouldReturnFailure_WhenWeekDaysAndMissingSideRangeDay()
        {
            var result = Parser.Agent.Run("SUN MON SUN-TUE THU-SAT 0200-");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

        [TestMethod]
        public void SchedulerParser_WeekDay_ShouldReturnFailure_WhenWeekDaysAndInvalidSunsetMinutes()
        {
            var result = Parser.Agent.Run("SUN MON SUN-TUE THU-SAT SS-ST");
            Assert.IsTrue(result.IsFailure);

            var parserResult = ParserUtils.CastFailure(result);
            Assert.IsTrue(parserResult.Length > 0);
        }

    }
}

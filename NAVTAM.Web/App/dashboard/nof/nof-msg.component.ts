﻿import { Component, OnInit, Inject, Input, Output, EventEmitter, HostListener, ViewChild } from '@angular/core'
import { TOASTR_TOKEN, Toastr } from './../../common/toastr.service';
import { ActivatedRoute, Router } from '@angular/router'

import { Subject } from 'rxjs/Subject';

import { Select2OptionData } from 'ng2-select2';

import { LocaleService, TranslationService } from 'angular-l10n';

import { DataService } from '../shared/data.service'
import { NofQueues } from '../shared/data.model'
import { MemoryStorageService } from '../shared/mem-storage.service'
import { WindowRef } from '../../common/windowRef.service'

import {
    IMessageQueue,
    IMessageQueuePartial,
    IUserMessageTemplate,
    INdsClientFilter,
    INdsClientPartial,
    ClientType,
    MessageStatus,
    IQueryOptions,
    IPaging,
    ISorting,
    ITableHeaders
} from '../shared/data.model'

declare var $: any;

class MessageType {
    static Notification = 0 
    static Error = 1;
}

class MessagePanel {
    static Read = 0
    static Compose = 1;
}

@Component({
    selector: 'nof-message',
    templateUrl: '/app/dashboard/nof/nof-msg.component.html'
})
export class NofMsgComponent implements OnInit {
    @Output() onUpdateCounter: EventEmitter<string> = new EventEmitter<string>();
    @Output() notify: EventEmitter<string> = new EventEmitter<string>();

    messages: IMessageQueuePartial[];
    queryOptions: IQueryOptions;
    selectedMessage: IMessageQueuePartial;
    paging: IPaging = {
        currentPage: 1,
        pageSize: 0,
        totalCount: 0,
        totalPages: 0,
    };
    filterPaging: IPaging = {
        currentPage: 1,
        pageSize: 0,
        totalCount: 0,
        totalPages: 0,
    };
    filterTotalPages: number = 0;
    totalPages: number = 0;

    itemsPerPageData: Array<any>;
    itemsPerPageStartValue: string;
    loadingData: boolean = false;
    tableHeaders: ITableHeaders;
    currentMessageStatusView: MessageStatus = MessageStatus.None;
    savedMessageStatusView: MessageStatus = MessageStatus.None;
    inbound: boolean = true;
    unreaded: number = 0;
    parked: number = 0;
    messagePanel: MessagePanel = MessagePanel.Read;
    subscriptions: INdsClientFilter[];
    selectedSubscription: INdsClientFilter = null;
    searchFilter: INdsClientFilter = {
        clientTypeStr: "",
        client: "",
        address: ""
    };
    addresses: string[] = [];
    recipients: string = "";
    messageBody: string = "";
    addressIndexSelected: number = -1;

    public prioriTyCodeOptions: Select2Options;
    public selectedPriorityCodeId: string = "-1";
    public priorityCodes: Array<Select2OptionData>;

    public templateOptions: Select2Options;
    public selectedTemplateId: string = "-1";
    public templates: Array<Select2OptionData>;
    public userTemplates: IUserMessageTemplate[] = [];
    public maxNumberOfTemplates = 4;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private dataService: DataService,
        private memStorageService: MemoryStorageService,
        private router: Router,
        private winRef: WindowRef,
        private route: ActivatedRoute,
        private translation: TranslationService) {

        this.tableHeaders = {
            sorting:
            [
                { columnName: 'PriorityCode', direction: 0 },
                { columnName: 'Originator', direction: 1 },
                { columnName: 'Created', direction: 0 },
            ],
            itemsPerPage:
            [
                { id: '15', text: '15' },
                { id: '30', text: '30' },
                { id: '75', text: '75' },
                { id: '100', text: '100' },
            ]
        };
        this.paging.pageSize = +this.tableHeaders.itemsPerPage[0].id;
        this.queryOptions = {
            page: 1,
            pageSize: this.paging.pageSize,
            sort: "_" + this.tableHeaders.sorting[1].columnName //Default ClientType
        }
    }

    @HostListener('window:keyup', ['$event'])
    onKeyup($event: any) {
        switch ($event.code) {
            case "ArrowUp"://NAVIGATE TABLE ROW UP
                if (this.selectedMessage.index > 0) {
                    this.onRowSelected(this.selectedMessage.index - 1);
                }
                break;
            case "ArrowDown": //NAVIGATE TABLE ROW DOWN
                if (this.selectedMessage.index < this.messages.length - 1) {
                    this.onRowSelected(this.selectedMessage.index + 1);
                }
                break;
        }
    }

    ngOnInit() {
        this.loadUserMessageTemplates();

        this.prioriTyCodeOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('')
            },
            width: "100%"
        }
        this.setPriorityCodes();

        this.templateOptions = {
            placeholder: {
                id: '-1',
                text: this.translation.translate('MessageQueue.SelectTemplate')
            },
            width: "100%"
        }
        this.loadTemplates();

        //Set latest query options
        const queryOptions = this.memStorageService.get(this.memStorageService.MESSAGES_QUEUE_QUERY_OPTIONS_KEY);
        if (queryOptions) {
            this.queryOptions = queryOptions;
        }
        this.getMessages(this.queryOptions.page);
        this.getParkedMessageCount();
    }

    sort(index) {
        const dir = this.tableHeaders.sorting[index].direction;

        for (let i = 0; i < this.tableHeaders.sorting.length; i++) {
            this.tableHeaders.sorting[i].direction = 0;
        }

        if (dir > 0) {
            this.tableHeaders.sorting[index].direction = -1;
            this.queryOptions.sort = '_' + this.tableHeaders.sorting[index].columnName;
        }
        else if (dir <= 0) {
            this.tableHeaders.sorting[index].direction = 1;
            this.queryOptions.sort = this.tableHeaders.sorting[index].columnName;
        }

        this.getMessages(this.queryOptions.page);
    }

    changeMessageView(status: MessageStatus, inbound: boolean = true, confirmLeave: boolean = true) {

        if (this.messagePanel === MessagePanel.Compose) {
            if (confirmLeave) {
                this.savedMessageStatusView = status;
                if (!this.disableSendButton()) {
                    let elm = $('#modal-discard-message');
                    const self = this;
                    elm.modal({});
                    return;
                }
            } 
            this.toggleComposeMessage(MessagePanel.Read, "");
        }

        this.currentMessageStatusView = status;
        this.inbound = inbound;
        this.getMessages(1);
    }

    discardMessage() {
        this.changeMessageView(this.savedMessageStatusView, true, false);
    }

    sortingClass(index) {
        if (this.tableHeaders.sorting[index].direction > 0)
            return 'sorting sorting_asc';

        if (this.tableHeaders.sorting[index].direction < 0)
            return 'sorting sorting_desc';

        return 'sorting';
    }

    rowColor(message) {
        switch (message.messageType) {
            case MessageType.Notification: return 'bg-submitted';
            case MessageType.Error: return 'bg-cancel';
            default: return '';
        }
    }

    toggleComposeMessage(panel: MessagePanel, recipients: string) {
        
        this.recipients = recipients || "";
        this.messageBody = "";
        this.selectedPriorityCodeId = this.priorityCodes[0].id;
        this.messagePanel = panel;
    }

    replyTo() {
        this.toggleComposeMessage(MessagePanel.Compose, this.selectedMessage.clientAddress);
    }

    updateMessageStatus(status : MessageStatus) {
        if (this.selectedMessage) {
            this.selectedMessage.prevStatus = this.selectedMessage.status;
            switch (this.currentMessageStatusView) {
                case MessageStatus.Important:
                    this.selectedMessage.status = status === MessageStatus.Important ? MessageStatus.None : this.selectedMessage.status = <number>status;
                    break;
                case MessageStatus.Parked:
                    this.selectedMessage.status = status === MessageStatus.Parked ? MessageStatus.None : this.selectedMessage.status = <number>status;
                    break;
                case MessageStatus.Deleted:
                    this.selectedMessage.status = status === MessageStatus.Deleted ? MessageStatus.None : this.selectedMessage.status = <number>status;
                    break;
                default:
                    this.selectedMessage.status = <number>status;
            }

            this.dataService.updateMessage(this.selectedMessage)
                .subscribe(result => {
                    if (result.anyError) {
                        this.selectedMessage.status = this.selectedMessage.prevStatus;

                        let error = `The message was changed by ${result.lastOwner} to ${result.status}`
                        this.toastr.error(error, "", { 'positionClass': 'toast-bottom-right' });
                        return;
                    }

                    if (this.messages.length === 1) {
                        this.paging.currentPage = this.paging.currentPage > 1 ? this.paging.currentPage - 1 : 1
                    }

                    this.getMessages(this.paging.currentPage);

                    let msg = "";
                    switch (this.selectedMessage.status) {
                        case MessageStatus.Parked: msg = this.translation.translate('MessageQueue.SuccessMessageArchived'); break;
                        case MessageStatus.Important: msg = this.translation.translate('MessageQueue.SuccessMessageImportant'); break;
                        case MessageStatus.Deleted: msg = this.translation.translate('MessageQueue.SuccessMessageDeleted'); break;
                        case MessageStatus.None:
                            if (status === MessageStatus.Parked) {
                                msg = this.translation.translate('MessageQueue.SuccessMessageUnarchived');
                            } else if (status === MessageStatus.Important) {
                                msg = this.translation.translate('MessageQueue.SuccessMessageUnimportant');
                            } else if (status === MessageStatus.Deleted) {
                                msg = this.translation.translate('MessageQueue.SuccessMessageUndeleted');
                            }
                            break;
                    }
                    //msg = this.translation.translate('MessageQueue.SuccessMessageArchived');
                    this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                }, error => {
                    let msg = "";
                    switch (status) {
                        case MessageStatus.Parked: msg = this.translation.translate('MessageQueue.FailureMessageArchived');; break;
                        case MessageStatus.Important: msg = this.translation.translate('MessageQueue.FailureMessageImportant'); break;
                        case MessageStatus.Deleted: msg = this.translation.translate('MessageQueue.FailureMessageDeleted'); break;
                        case MessageStatus.None: msg = this.translation.translate('MessageQueue.FailureMessageStatusChange'); break;
                    }
                    this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        }
    }

    confirmDelete() {
        if (this.currentMessageStatusView === MessageStatus.Deleted) {
            this.updateMessageStatus(MessageStatus.Deleted);
        } else {
            $('#modal-delete').modal({});

        }
    }

    openSearchDialog() {
        this.notify.emit('disable-tabs');

        this.subscriptions = [];
        this.dataService.getNdsClientAddresses()
            .subscribe(subscriptions => {

                this.selectedSubscription = null;
                for (var iter = 0; iter < subscriptions.length; iter++) {
                    let subscription = subscriptions[iter];
                    subscription.index = iter;
                    subscription.clientTypeStr = this.toClientTypeStr(subscription.clientType);
                    subscription.isSelected = false;

                    this.subscriptions.push(subscription);
                }

                if (subscriptions.length > 0) {
                    this.selectedSubscription = this.subscriptions[0];
                    this.selectedSubscription.isSelected = true;
                };

            }, error => {
                //TODO
            });

        let elm = $('#modal-search');
        const self = this;
        elm.modal({});
        elm.on('hidden.bs.modal', function (e) {
            self.closeSearchDialog();
        })
    }

    closeSearchDialog() {
        this.addresses = [];
        this.addressIndexSelected = -1;
        this.notify.emit('enable-tabs');
    }

    deleteMessage() {
        if (this.selectedMessage) {
            this.dataService.deleteIncomingMessage(this.selectedMessage.id)
                .subscribe(result => {
                    if (this.messages.length === 1 ) {
                        this.paging.currentPage = this.paging.currentPage > 1 ? this.paging.currentPage - 1 : 1 
                    }

                    this.getMessages(this.paging.currentPage);
                    let msg = this.translation.translate('MessageQueue.SuccessMessageDeleted');
                    this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                }, error => {
                    let msg = this.translation.translate('MessageQueue.FailureMessageDeleted');
                    this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
                });
        }
    }

    itemsPerPageChanged(e: any): void {
        this.queryOptions.pageSize = e.value;
        this.getMessages(1);
    }

    toggleReadStatus() {
        this.selectedMessage.read = !this.selectedMessage.read;
        this.dataService.updateMessage(this.selectedMessage)
            .subscribe(result => {
                if (this.currentMessageStatusView === MessageStatus.None) {
                    this.unreaded = 0;
                    for (let iter = 0; iter < this.messages.length; iter++) {
                        if (!this.messages[iter].read) {
                            this.unreaded++;
                        }
                    }
                }
            }, error => {
                this.selectedMessage.read = !this.selectedMessage.read;
            });
    }


    onRowSelected(index: number) {
        if (this.selectedMessage) {
            this.selectedMessage.isSelected = false;
        }

        this.selectedMessage = this.messages[index];
        this.selectedMessage.isSelected = true;

        this.memStorageService.save(this.memStorageService.MESSAGE_QUEUE_ID_KEY, this.selectedMessage.id);

        if (!this.selectedMessage.read) {
            this.selectedMessage.read = true;
            this.dataService.updateMessage(this.selectedMessage)
                .subscribe(result => {
                    if (this.currentMessageStatusView === MessageStatus.None) {
                        this.unreaded = 0;
                        for (let iter = 0; iter < this.messages.length; iter++) {
                            if (!this.messages[iter].read) {
                                this.unreaded++;
                            }
                        }
                    }
                }, error => {
                    this.selectedMessage.read = false;
                });
        }
    }

    onPageChange(page: number) {
        this.getMessages(page);
    }

    selectSubscription(client: INdsClientFilter) {
        this.selectedSubscription.isSelected = false;
        this.selectedSubscription = client;
        this.selectedSubscription.isSelected = true;
    }

    addAddress() {
        this.addresses.push(this.selectedSubscription.address);
    }

    selectAddress(index: number) {
        this.addressIndexSelected = this.addressIndexSelected !== index ? index : -1;
    }

    deleteAddress(index: number) {
        if (this.addressIndexSelected >= 0) {
            this.addresses.splice(this.addressIndexSelected,1);
        }
    }

    appendAddressToRecipientList() {
        let len = this.recipients.length;
        let addresses = len == 0 ? "" : (this.recipients[len - 1] === ";" ? "" : ";");
        for (let i = 0; i < this.addresses.length; i++) {
            addresses += this.addresses[i] + ";"
        }

        this.recipients += addresses;
    }

    disableSendButton(): boolean {
        if (!this.recipients) {
            return true;
        }

        if ( !(this.recipients.length > 0 && this.messageBody.length > 0)) {
            return true;
        }

        let disabled = false;
        let addresses = this.recipients.split(";")

        let reg = /^[a-z]+$/i;
        for (let i = 0; i < addresses.length; i++) {
            let address = addresses[i];

            if (address.length === 0) {
                continue;
            }

            if (address.length !== 8) {
                disabled = true;
                break;
            }

            if (!reg.test(address)) {
                disabled = true;
                break;
            }
        }

        return disabled;
    }

    sendMessage() {
        let queueMessage = this.prepareQueueMessagePayload();
        this.dataService.sendQueueMessage(queueMessage)
            .subscribe(data => {
                let msg = this.translation.translate('MessageQueue.SuccessMessageSent');
                this.toastr.success(msg, "", { 'positionClass': 'toast-bottom-right' });
                this.toggleComposeMessage(MessagePanel.Read, "");
            }, error => {
                let msg = this.translation.translate('MessageQueue.FailureMessageSent');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    onPriorityCodeChanged(data: { value: string }) {
        this.selectedPriorityCodeId = data.value;
    }

    onTemplateChanged(data: { value: string }) {
        this.selectedTemplateId = data.value;
        this.dataService.getMessageTemplate(this.selectedTemplateId)
            .subscribe(template => {
                this.messageBody = template.body
            }, error => {
                let msg = this.translation.translate('MessageQueue.FailureTemplateLoad');
                this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    getTemplateBySlot(slotNumber: number, event: any = null) {
        if( this.userTemplates[slotNumber - 1] !== null) {
            return `${this.userTemplates[slotNumber - 1].templateName}`;
        }
        return `Preference ${slotNumber}`;
    } 

    setTemplate(slotNumber: number) {
    //    let userTemplate: IUserMessageTemplate = {
    //        slotNumber: slotNumber,
    //        templateId: this.selectedTemplateId,
    //        username: this.winRef.appConfig.usrName
    //    };  

    //    this.dataService.saveUserTemplate(userTemplate)
    //        .subscribe(template => {
    //            this.messageBody = template.body
    //            this.loadUserMessageTemplates();
    //        }, error => {
    //            let msg = this.translation.translate('MessageQueue.FailureTemplateLoad');
    //            this.toastr.error(msg + error.message, "", { 'positionClass': 'toast-bottom-right' });
    //        });
    }

    composeMessageUsingTemplate(slotNumber: number, event: any) {
        this.onTemplateChanged({ value: this.userTemplates[slotNumber - 1].templateId });

        this.toggleComposeMessage(MessagePanel.Compose, this.selectedMessage.clientAddress);

        event.stopPropagation();
        return false;
    }

    private loadUserMessageTemplates() {
        //var userName = this.winRef.appConfig.usrName;

        //this.userTemplates = [];
        //for (let i = 0; i < this.maxNumberOfTemplates; i++) {
        //    this.userTemplates.push(null);
        //};
        //this.dataService.getUserTemplates(userName)
        //    .subscribe(templates => {
        //        for (let i = 0; i < this.maxNumberOfTemplates; i++) {
        //            let template = templates[i];
        //            if (templates.length - 1 >= i) {
        //                this.userTemplates[template.slotNumber - 1] = template;
        //            }
        //        }
        //    }, error => {
        //        //TODO
        //    });
    }

    private getMessages(page) {
        try {
            this.loadingData = true;
            this.queryOptions.page = page;
            this.memStorageService.save(this.memStorageService.MESSAGES_QUEUE_QUERY_OPTIONS_KEY, this.queryOptions);

            if (this.inbound) {
                this.dataService.getIncomingMessages(this.queryOptions, this.currentMessageStatusView)
                    .subscribe((messages: any) => {
                        this.processMessages(messages);
                    });
            } else {
                this.dataService.getOutgoingMessages(this.queryOptions, this.currentMessageStatusView)
                    .subscribe((messages: any) => {
                        this.processMessages(messages);
                    });
            }
        }
        catch (e) {
            this.loadingData = false;
        }
    }

    private processMessages(messages: any) {
        this.paging = messages.paging;
        this.totalPages = messages.paging.totalPages;

        if (this.inbound && this.currentMessageStatusView === MessageStatus.None) {
            this.unreaded = 0;
        }

        if (this.inbound && this.currentMessageStatusView === MessageStatus.Parked) {
            this.parked = 0;
        }


        for (var iter = 0; iter < messages.data.length; iter++) {
            let message = messages.data[iter];
            message.index = iter;
            message.clientTypeStr = this.toClientTypeStr(messages.data[iter].clientType);
            if (this.inbound && this.currentMessageStatusView === MessageStatus.None && !message.read) {
                this.unreaded++;
            }
            if (this.inbound && this.currentMessageStatusView === MessageStatus.Parked) {
                this.parked++;
            }
        }

        const lastMessageSelected = this.memStorageService.get(this.memStorageService.NSD_CLIENT_ID_KEY);
        if (messages.data.length > 0) {
            this.messages = messages.data;
            let index = 0;
            if (lastMessageSelected) {
                index = this.messages.findIndex(x => x.id === lastMessageSelected);
                if (index < 0) index = 0; // when not found first should be selected;
            }
            this.messages[index].isSelected = true;
            this.selectedMessage = this.messages[index];
            this.loadingData = false;
            this.onRowSelected(index);
        } else {
            this.messages = [];
            this.selectedMessage = null;
            this.loadingData = false;
        }
    }

    private setPriorityCodes(): void {
        this.priorityCodes = [
            { id: "GG", text: "GG" },
            { id: "SS", text: "SS" },
            { id: "DD", text: "DD" },
            { id: "FF", text: "FF" },
            { id: "KK", text: "KK" }];

        this.selectedPriorityCodeId = this.priorityCodes[0].id;
    }

    private loadTemplates(): void {
        this.dataService.getMessageTemplates()
            .subscribe((template: any) => {
                this.templates = template.map(
                    function (r: any) {
                        return <Select2OptionData>{
                            id: r.id,
                            text: r.name
                        };
                    });

                //this.templates.splice(0, 0, this.templates.splice(0, 1)[0]);
                //this.selectedTemplateId = "-1";
            });
    }

    getParkedMessageCount() {
        try {
            this.dataService.getTotalParkedMessages()
                .subscribe((count: any) => {
                    this.parked = count;
                });
        }
        catch (e) {
        }
    }

    private prepareQueueMessagePayload(): IMessageQueue {
        let now = new Date()
        let messageQueue: IMessageQueue = {
            messageType: MessageType.Notification,
            clientType: ClientType.AFTN,
            priorityCode: this.selectedPriorityCodeId,
            inbound: false,
            recipients: this.recipients,
            body: this.messageBody,
            clientName: this.winRef.appConfig.usrName,
            clientAddress: "",
            created: now,
            locked: false,
            read: true,
            status: MessageStatus.None
        }

        return messageQueue;
    }

    toClientTypeStr(clientType) {
        switch (clientType) {
            case ClientType.AFTN: return 'AFTN';
            case ClientType.SWIM: return 'SWIM';
            case ClientType.REST: return 'REST';
            default: return '';
        }
    }
}


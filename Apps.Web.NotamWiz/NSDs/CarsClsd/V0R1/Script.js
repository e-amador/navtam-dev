﻿define(["jquery", "Resources", "utilities", "mapping", "jsonpath", "kendoExt", "kendoCustomDataBinders", "javaExt"], function($, res, Util)
{
    var RES = res.getResources("NSD-Cars");
    var viewModel = null;
    var self = null;
    var conditionValidator = null;
    var nsdEditor = null;
    var labels = {};
    return {
        obstPushPin: null,
        validationErrorMessage: null,
        init: function(editor, model)
        {
            self = this;
            nsdEditor = editor;
            viewModel = model;
            viewModel.createNOTAMViewModel($.extend($("#nsdModel").data("model"), {
                Bilingual: viewModel.Options.Bilingual,
                CondStatusValue: 0,
                CondStatus: [0, 1],
                SchText: ""
            }));
            self.initUI();
            self.setupUpNSDEditor();
            conditionValidator = $("#nsdConditionContainer").data("kendoValidator");
            editor.conditionValidator = conditionValidator;
            viewModel.nsdTokens.bind("change", self.validate);
            self.validate();
        },
        initUI: function()
        {
            labels = $("#ScheduleData").data("reasons").Data;
            $("#scheduleTr").hide();
            $("#btnCarsScheduleAPI").prop('disabled', true);

            // hide and unhide controls
            $('.RadioButtonStatusSelection').click(function ()
            {

                if ($(this).attr("value") == "0")
                {
                    $("#btnCarsScheduleAPI").prop('disabled', true);  
                    nsdEditor.proposalVM.nsdTokens.set("TimeScheduleData","");
                    nsdEditor.proposalVM.nsdTokens.set("SchText", "");     
                }

                else if ($(this).attr("value") == "1")
                {
                    $("#btnCarsScheduleAPI").prop('disabled', false);                     
                }    
            });

            var CarsScheduleAPIWindow = $("#CarsScheduleAPI-Popup").kendoWindow({
                height: 380,
                width: 700,
                resizable: false,
                visible: false,
                modal: true,
            }).data("kendoWindow");

            CarsScheduleAPIWindow.bind("refresh", function ()
            {
                $("#CarsScheduleAPI-Popup").data("kendoWindow").center();
                $("#CarsScheduleAPI-Popup").data("kendoWindow").open();
            });

            $("#btnCarsScheduleAPI").on('click', function ()
            {                    
                $("#CarsScheduleAPI-Popup").data("kendoWindow")
                               .content("")
                               .refresh({
                                   url: "../CarsClsd/GetTimeScheduleAPI",
                                   type: "POST",
                                   complete: function ()
                                   {
                                       var viewCodeFile = nsdEditor.proposalVM.NSDURL + "/GetSchScript";

                                       debugger;
                                       require([viewCodeFile], function (CarsSchedule)
                                       {
                                           var _BeginDatetime = kendo.parseDate($("#beginDatetime").val().insertAt(14, ":"), ["dd MMM yyyy HH:mm"]);
                                           var _ExpiryDatetime = kendo.parseDate($("#expiryDatetime").val().insertAt(14, ":"), ["dd MMM yyyy HH:mm"]);

                                           var _TimeScheduleData = JSON.stringify((typeof (nsdEditor.proposalVM.nsdTokens.TimeScheduleData) === "undefined" ? "" : nsdEditor.proposalVM.nsdTokens.TimeScheduleData));
                                           CarsSchedule.init(_TimeScheduleData, _BeginDatetime, _ExpiryDatetime, nsdEditor.proposalVM);
                                           CarsSchedule.initUI(labels);
                                           CarsSchedule.PostInitUI();
                                           document.getElementById("TimeSchedulecontainer").style.visibility = "visible";
                                       })

                                   }
                               });
            });
            self.obstPushPin = nsdEditor.bingMap.addPushPin(
                viewModel.subject.SubjectGeoRefLat,
                viewModel.subject.SubjectGeoRefLong,
                {
                    draggable: false,
                    height: 50,
                    width: 50,
                    anchor: new Microsoft.Maps.Point(0, 50),
                    icon: "/Images/BluePushpin.png"
                });

            $("#nsdConditionContainer").kendoValidator({
                messages: {
                    freeTextRequired: labels.Schdlr_SchRequired
                },
                rules: {
                    freeTextRequired: function (input) {
                        if (input.is("[name=Generatedtext]")) {
                            if (nsdEditor.proposalVM.nsdTokens.CondStatusValue !== "1")
                                return true;
                            return input.val() !== "";
                        }
                        return true;
                    }
                }
            });
        },
        setValidationError: function(condition, error)
        {
            var result = condition();
            if (result)
            {
                self.validationErrorMessage = "";
            }
            else
            {
                self.validationErrorMessage = error;
            }
            return result;
        },
        validate: function(e)
        {
            viewModel.validModel = false;
            Util.setConditionValid(false);
            if (!conditionValidator.validate())
            {
                return;
            }
            viewModel.generateNOTAM();
        },
        setupUpNSDEditor: function()
        {
            Util.hidePermement();
        }
    };
});
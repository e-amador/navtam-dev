﻿using System;
using System.Collections.Generic;

namespace NAVTAM.ViewModels
{
    public class ValidationResult
    {
        public static ValidationResult Fail(List<string> errors)
        {
            return new ValidationResult()
            {
                HasErrors = true,
                ErrorMessages = errors
            };
        }

        public static ValidationResult Fail(string error)
        {
            return new ValidationResult()
            {
                HasErrors = true,
                ErrorMessages = new List<string> { error }
            };
        }

        public static ValidationResult Succeed() => new ValidationResult();

        public bool HasErrors { get; private set; }
        public List<string> ErrorMessages { get; set; }
    }
}
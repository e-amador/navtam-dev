﻿namespace NavCanada.Services.Api.Diagnostics
{
    using System.Web.Configuration;
    using System.Web.Http;

    using log4net;

    using NavCanada.Core.Common.Common;

    public class WebApiApplication : System.Web.HttpApplication
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(WebApiApplication));


        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            GlobalConfig();
        }

        private void GlobalConfig()
        {
            // Log4Net initialization
            if (WebConfigurationManager.ConnectionStrings["LogConnection"] != null )
                Log4NetManager.InitializeLog4Net(WebConfigurationManager.ConnectionStrings["LogConnection"].ConnectionString);
        }


    }
}

﻿import { OpaqueToken } from '@angular/core'

export let TOASTR_TOKEN = new OpaqueToken('toastr');

export interface Toastr {
    options : any,
    success(message: string, title?: string, defaultOptions?: any): void;
    info(message: string, title?: string, defaultOptions?: any): void;
    warning(message: string, title?: string, defaultOptions?: any): void;
    error(message: string, title?: string, defaultOptions?: any): void;
}

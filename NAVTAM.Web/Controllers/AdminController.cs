﻿using Microsoft.AspNet.Identity;
using NavCanada.Core.Common.Common;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace NAVTAM.Controllers
{
    [Authorize(Roles = "Administrator, Organization Administrator, CCS, NOF_ADMINISTRATOR")]
    public class AdministrationController : BaseController
    {
        private readonly ILogger _logger;

        public AdministrationController(IUow uow, ILogger logger) : base(uow, logger)
        {
            _logger = logger;
        }

        // GET: Admin
        public async Task<ActionResult> Index()
        {
            return await LogAction(async () =>
            {
                ViewData["mapServerHost"] = ConfigurationManager.AppSettings["MapServerHost"];
                var userId = User.Identity.GetUserId();
                var user = await UserManager.FindByIdAsync(userId);
                if (user != null)
                {
                    var roles = await UserManager.GetRolesAsync(userId);

                    var adminAreaAccess = new[] { CommonDefinitions.AdminRole, CommonDefinitions.CcsRole, CommonDefinitions.OrgAdminRole, CommonDefinitions.NofAdminRole };
                    if (adminAreaAccess.Any(i => roles.Contains(i)))
                    {
                        if (TrySetViewData(2, null))
                            return View() as ActionResult;
                    }
                }

                return RedirectToAction("Login", "Account") as ActionResult;

            }, "Administration/Index");
        }


        private bool TrySetViewData(int catId, string sidebarName)
        {
            // cookie needs to match logged user (extra level of security)
            var cookie = HttpContext.Request.Cookies["usr_id"];
            if (cookie == null || cookie.Value != User.Identity.GetUserId())
                return false;

            var userId = cookie.Value;

            cookie = HttpContext.Request.Cookies["org_id"];
            if (cookie == null)
                return false;

            var orgId = cookie.Value;

            ViewData["usr_id"] = userId;
            ViewData["org_id"] = orgId;

            return true;
        }
    }
}
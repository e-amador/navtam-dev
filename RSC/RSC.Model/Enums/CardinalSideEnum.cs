﻿namespace RSC.Model.Enums
{
    /// <summary>
    /// Defines which side of the runway is affected by a condition. 
    /// </summary>
    public enum CardinalSideEnum : int
    {
        E = 250,
        SE = 251,
        S = 252,
        SW = 253,
        W = 254,
        NW = 255,
        N = 256,
        NE = 257
    }
}
."$PSScriptRoot\utils.ps1"

$remoteWebServer = "10.128.33.206,10.128.33.207"  #IPs of the Remote Web App
$username = "se\administrator"  #Username of user with installation priveledges
$password = "Navcan1!"  #Username of user with installation priveledges

$remoteWebAppDir = "Z:\NAVTAMTST" #" Shared folder location for the ISS
$serviceHostList = "10.128.33.155,10.128.33.156"  #Task Engine IPs

$packageDirectory = "C:\Packages\NAVTAM_TEST_XX"  #Folder where package was donwloaded
$webSiteName = "Default Web Site" #Name of the WebSite on IIS

function InstallWebApplication($credentials) {
	try{
			Write-Host " ";
			Write-Host "-----------------------------------------------------";
			Write-Host "-----------------------------------------------------";
			Write-Host "Start WebApplication installation to $remoteWebServer";
			Write-Host "-----------------------------------------------------";

			$webAppDirectory = "$packageDirectory\WebApp"

			#Unzip WebApp.Zip to folder
			#UnZipFileToFolder "$packageDirectory\WebApp.zip" $webAppDirectory

			$remoteServers = $remoteWebServer.split(",");
			foreach($webAppSrver in $remoteServers){ 
				#Start remote server session
				$s = New-PSSession -ErrorAction Stop -ComputerName $webAppSrver -Credential $credentials
				
				#Stop Internet Information Server Before Start Deployment on 
				Write-Host "Stop IIS on $webAppSrver"
				StopIIS $s

				#Start Deployment to remote server
				DeployWebApp $remoteWebServer $webSiteName "$packageDirectory\WebApp" $username $password

				#Remove remote server session
				Remove-PSSession $s
			}

			#Run Migrations
			RunMigrations $webAppDirectory

			foreach($webAppSrver in $remoteServers){ 
				#Start remote server session
				$s = New-PSSession -ErrorAction Stop -ComputerName $webAppSrver -Credential $credentials
				
				#Start Internet Information Server After Deployment
				Write-Host "Start IIS on $webAppSrver"
				StartIIS $s

				#Remove remote server session
				Remove-PSSession $s
			}

			Write-Host "WebApplication fully installed on $server";
	}
	catch{
		Write-Host "There was an error installing WebApplication to $server";
		Write-Host $_.Exception.Message;
		throw $LASTEXITCODE
	}
}

function InstallTaskEnginService($credentials) {
	try {
		$taskEngineDirectory = "$packageDirectory\TaskEngine"

		#Unzip WebApp.Zip to folder
		#UnZipFileToFolder "$packageDirectory\TaskEngine.zip" $taskEngineDirectory

		#Repeat the installation on each remote server
		$remoteServers = $serviceHostList.split(",");

		foreach($remoteServer in $remoteServers){ 
			Write-Host " ";
			Write-Host "-----------------------------------------------------";
			Write-Host "-----------------------------------------------------";
			Write-Host "Start installation of TaskEngine service on $remoteServer";
			Write-Host "-----------------------------------------------------";

			#Start remote server session
			$session = New-PSSession -ErrorAction Stop -ComputerName $remoteServer -Credential $credentials

			$remoteDir = "C:\NAVTAM_BIN"
			$serviceName = "SERVICES.WIN.TASKENGINE"

			#Stop TaskEngine
			StopWindowsService $session $serviceName

			#Copy TaskEngine binaries to remote server
			CopyFilesToRemoteServer $session "$taskEngineDirectory\*" $remoteDir 1

			#Install TaskEngine
			$serviceExePath = "$remoteDir\Services.Win.TaskEngine.exe"
			InstallWindowsService64 $session $serviceExePath 

			#Start TaskEngine
			StartWindowsService $session $serviceName 

			#Remove remote server session
			Remove-PSSession $session

			Write-Host "TaskEngine service fully installed on $remoteServer";
		}
	}
	catch {
		Write-Host "There was an error deploying TaskEngine to $remoteWebServer";
		Write-Host $_.Exception.Message;
		throw $LASTEXITCODE
	}
}

try {
	#create remote server credentials
	$credentials = CreateCredentials $username $password

	#Install WebApplication on remote server
	InstallWebApplication $credentials

	#Install TaskEngine to all remote servers
	InstallTaskEnginService $credentials
}
catch {
	throw $LASTEXITCODE
}



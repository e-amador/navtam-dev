﻿import { Select2OptionData } from 'ng2-select2';

export interface INsdCategory {
    id: number,
    groupCategoryId: number,
    name: string,
    text: string,
    isSelected: boolean,
    children: INsdCategory[]
}

export interface INsdDropdown {
    id: number,
    parentCategory: string,
    name: string
}

export interface IAuthData {
    access_token: string,
    expires_in: string,
    token_type: string
}

export interface IProposal {
    id: number,
    categoryId: number,
    categoryName: string,
    startActivity?: Date,
    endValidity?: Date,
    estimated: boolean,
    received: Date,
    status: number,
    notamId: string,
    icaoSubjectDesc: string,
    icaoConditionDesc: string,
    itemA: string,
    isSelected: boolean,
    proposalType: ProposalType,
    seriesChecklist: boolean,
    siteId?: string,
    index: number,
    groupId?: string,
    grouped?: boolean,
    group?: IProposal[],
    groupOpen?: boolean,
    displayName: string,
    isObsolete: boolean,
    soonToExpire: boolean,
    itemE: string,
    operator: string,
    originator: string,
    modifiedByUsr: string,
}

export interface ICloneRequest {
    id: number,
    history: boolean,
}

export interface INotam {
    id: string,
    rootId: string,
    notamId: string,
    referredNotamId: string,
    proposalId: number,
    originator: string,
    originatorEmail: string,
    originatorPhone: string,
    itemA: string,
    startActivity?: Date,
    endValidity?: Date,
    published: Date,
    notamType: ProposalType,
    series: string,
    scope: string,
    code23: string,
    code45: string,
    traffic: string,
    purpose: string,
    isReplaced: boolean,
    isEstimated: boolean,
    isPermanent: boolean,
    itemF: string,
    itemG: string,
    siteId: string,
    coordinates: string,
    radius: number,
    lowerLimit: number,
    upperLimit: number,
    modifiedByOrg: string,
    modifiedByUsr: string,
    operator: string,
    effectAreaGeoJson: string,
    urgent: boolean,
    seriesChecklist: boolean,
    isObsolete: boolean,

    itemE?: string,
    itemEFrench?: string,
    icaoFormat?: string,
    soonToExpire: boolean,

    isSelected: boolean,
    index: number,
}

export interface Icao {
    notamNumber: string,
    itemB: string,
    itemC: string,
    qLine: string,
    itemA: string,
    itemD: string,
    itemE: string,
    itemEFrench: string,
    itemF: string,
    itemG: string,
    itemX: string,
}

export interface ItemD {
    id?: number
    calendarType: number,
    proposalId: number,
    calendarId: number,
    event: string
}

export interface IProposalDetails {
    id: number,
    date: string,
    status: string,
    isSelected: boolean
}

export class IPoint {
    x: string;
    y: string;
}

// >> merge or change with ISdoSubject
export interface ISubject {
    id: string,
    name: string,
    designator: string,
    sdoEntityName: string,
    subjectGeo: string,
    subjectGeoRefPoint: string
    isBilingualRegion: boolean,
}

export interface IcaoSubject {
    id: number,
    name?: string,
    text: string,
    entityCode?: string,
    code?: string,
    scope?: string,
    requiresItemA?: boolean,
    requiresScope?: boolean,
    requiresSeries?: boolean
}

export interface IcaoCondition {
    id: number,
    code?: string,
    text: string,
    i?: boolean,
    v?: boolean,
    n?: boolean,
    b?: boolean,
    o?: boolean,
    m?: boolean,
    lower?: number,
    upper?: number,
    radius?: number,
    requiresItemFG?: boolean,
    requiresPurpose?: boolean
    description?: string
}

export interface INdsClientPartial {
    id: number,
    clientType: number,
    clientTypeStr?: string,
    address: string,
    itemA: string,
    french: boolean,
    client: string,
    isSelected?: boolean,
    index?: number,
    subscriptionSelected?: boolean,
    updated: string,
    synced: string
}

export interface IMessageTemplate {
    id?: number,
    name: string,
    username?: string,
    addresses: string,
    body: string,
    created?: Date,
    index?: number,
    isSelected?: boolean
}

export interface IMessageQueuePartial {
    id: string,
    messageType: number,
    clientType: number,
    priorityCode: string,
    inbound: boolean,
    clientName: string,
    clientAddress: string,
    recipients: string,
    body: string,
    created: Date,
    lastUpdated?: Date
    lastOwner?: string,
    delivered?: Date,
    read: boolean,
    status: number;
    isSelected?: boolean,
    index?: number,
    locked: boolean;
    prevStatus?: number;
}

export interface IMessageQueue {
    id?: string,
    messageType: number,
    clientType: number,
    priorityCode: string,
    inbound: boolean,
    clientName: string,
    clientAddress: string,
    recipients: string,
    body: string,
    created: Date,
    lastUpdated?: Date
    delivered?: Date,
    read: boolean,
    status: number;
    prevStatus?: number;
    anyError?: boolean;
    replyToId?: string;
    locked: boolean;
    lastOwner?: string;
}

export interface INdsClient {
    id?: number,
    clientType: number,
    address: string,
    itemsA: string[],
    series: string[],
    french: boolean,
    client: string,
    allowQueries: boolean,
    active: boolean,
    isRegionSubscription: boolean,
    region: IRegionViewModel,
    lowerLimit: number,
    upperLimit: number,
}

export class RegionSource {
    static None: number = 0;
    static Geography: number = 1;
    static FIR: number = 2;
    static Points: number = 3;
    static PointAndRadius: number = 4;
}

export interface IRegionViewModel {
    source: RegionSource,
    geography: string,
    fir?: string,
    points?: string,
    location?: string,
    radius?: number
    geoFeatures?: string,
}

export interface IRegionMapRender {
    region: IRegionViewModel,
    changes?: IRegionPointModel[]
}

export interface IRegionPointModel {
    source: string,
    location: string,
    inDoa: boolean,
    radius?: number,
}

export interface IUserMessageTemplate {
    id?: number,
    username: string,
    templateId: string,
    templateName?: string,
    slotNumber: number
}

export interface INdsClientFilter {
    clientTypeStr?: string,
    clientType?: number,
    address: string,
    client: string,
    isSelected?: boolean,
    index?: number,
}

export interface IQueryOptions {
    page: number,
    pageSize: number,
    sort: string,
    start?: Date,
    end?: Date,
}

export interface IPaging {
    currentPage: number,
    pageSize: number,
    totalCount: number,
    totalPages: number,
    nextPageLink?: string,
    previuosPageLink?: string,
}

export interface ISdoSubject {
    id: string,
    parentId: string,
    name: string,
    suffix: string,
    designator: string,
    sdoEntityName: string,
    disabled: boolean,
    selected: boolean,
    subjectGeo: string,
    subjectGeoRefPoint: string,
    text: string,
    isBilingualRegion: boolean,
    servedCity: string,
}

export interface IFIRSubject {
    id: string,
    designator: string,
    name: string,
    count: number,
}

export interface ISelectOptions {
    id: string,
    text: string
}

export interface ISorting {
    columnName: string;
    direction: number; // -1|0|+1
}

export interface ITableHeaders {
    sorting: ISorting[],
    itemsPerPage: Select2OptionData[]
}

export interface IMemoryStorage {
    key: string,
    value: any
}

export interface IFilterCriteria {
    field: string,
    operator: FilterOperator,
    value: string,
    valueList?: [string],
    type?: string
};

export class NofQueues {
    static Pending = "PENDING_QUEUE";
    static Park = "PARK_QUEUE"
    static Review = "REVIEW_QUEUE"
    static Message = "MESSAGE_QUEUE"
}

export class FilterOperator {
    static EQ = "EQ";
    static NEQ = "NEQ";
    static LT = "LT";
    static GT = "GT";
    static LTE = "LTE";
    static GTE = "GTE";
    static SW = "SW";
    static EW = "EW";
    static CT = "CT";
}

export class ProposalType {
    static New = 0;
    static Replace = 1;
    static Cancel = 2;
}

export class NsdCategory {
    static All = 1;
    static CatchAll = 2;
    static Obstacles = 3;
    static Obst = 4;
    static ObstLGT = 5;
    static Runways = 6;
    static RwyClsd = 7;
    static TwyClsd = 11;
    static CarsClsd = 13;
    static MultiObst = 14;
    static MultiObstLGT = 15;
}

export class ClientType {
    static AFTN = 0;
    static SWIM = 1;
    static REST = 2;
}

export class MessageStatus {
    static None = 0;
    static Important = 1;
    static Draft = 2;
    static Parked = 3;
    static Deleted = 4;
    static Reply = 5;
    static AlreadyTaken = 200;
}

export class ProposalStatus {
    static Undefined = 0;

    // User Proposal status
    static Draft = 1;
    static Submitted = 2;
    static Withdrawn = 3;

    // NOF Proposal status
    static Picked = 10;
    static Parked = 11;
    static Disseminated = 12;
    static Rejected = 13;
    static SoontoExpire = 14;
    static Expired = 15;
    static ParkedDraft = 16;
    static Terminated = 17;
    static DisseminatedModified = 18;
    static ParkedPicked = 19;

    //Everybody
    static Cancelled = 20;
    static Replaced = 21;
    static Deleted = 22;
    static Discarded = 23;

    static Taken = 255;
}

export interface IMObstViewModel {
    obstacleType: string,
    otherObstacleNameE: string,
    otherObstacleNameF: string,
    areaType: MObstAreaType,
    location: string,
    radius: number,
    radiusUnit: boolean,
    fromPoint: string,
    toPoint: string,
    obstacleDescriptionE: string,
    obstacleDescriptionF: string,
    elevation: number,
    height: number,
    lighted: number,
    painted: number,
    ballmarks: number
}

export interface IOriginatorInfo {
    id?: string,
    organizationId?: number,
    fullname?: string,
    email?: string,
    phone?: string
}

export class MObstAreaType {
    static Circle: number = 0;
    static Line: number = 1;
}

export interface IPointsViewModel {
    points: string[],
    calculatedPoint: string,
    calculatedRadius: number,
    pointRadius: number,
    error: string,
    internationalAerodromesDto: InternationalAerodromeDto[],
}

export interface InternationalAerodromeDto {
    codeId: string,
    distance: string,
    referencePoint: string,
    IsPointAndRadius: boolean
}

export interface IInDoaValidationResult {
    location: string,
    inDoa: boolean,
    inBilingualRegion: boolean
}

//export class RadiusUnit {
//    static NM: number = 0;
//    static FT: number = 1;
//}
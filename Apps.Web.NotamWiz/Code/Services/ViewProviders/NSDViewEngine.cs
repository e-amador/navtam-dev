﻿namespace NavCanada.Applications.NotamWiz.Web.Code.Services.ViewProviders
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web.Mvc;

    public class NSDViewEngine : RazorViewEngine
    {

  

        private string[] _newViewLocations = new string[] {
            "~/Nsds/{1}/{0}.cshtml",
            "~/Views/{1}/{0}.cshtml",
            "~/Views/{1}/{0}.vbhtml",
            "~/Views/Shared/{0}.cshtml",
            "~/Views/Shared/{0}.vbhtml"
        };

        private string[] _newMasterLocations = new string[] {
            "~/Views/{1}/{0}.cshtml",
            "~/Views/{1}/{0}.vbhtml",
            "~/Views/Shared/{0}.cshtml",
            "~/Views/Shared/{0}.vbhtml"
        };

        private string[] _newPartialViewLocations = new string[] {
            "~/Nsds/{1}/{0}.cshtml",
            "~/Views/{1}/{0}.vbhtml",
            "~/Views/Shared/{0}.cshtml",
            "~/Views/Shared/{0}.vbhtml"
        };

        public NSDViewEngine()
            : base()
        {
            

            ViewLocationFormats = AppendLocationFormats(this._newViewLocations, ViewLocationFormats);

            MasterLocationFormats = AppendLocationFormats(this._newMasterLocations, MasterLocationFormats);

            PartialViewLocationFormats = AppendLocationFormats(this._newPartialViewLocations, PartialViewLocationFormats);
        }

        private string[] AppendLocationFormats(string[] newLocations, string[] defaultLocations)
        {
            List<string> viewLocations = new List<string>();
            viewLocations.AddRange(newLocations);
            viewLocations.AddRange(defaultLocations);
            return viewLocations.ToArray();
        }

        protected override IView CreateView(ControllerContext controllerContext, string viewPath, string masterPath)
        {
            CorrectNSDViewPAth(controllerContext, ref viewPath);

            return base.CreateView(controllerContext, viewPath, masterPath);
        }

        protected override IView CreatePartialView(ControllerContext controllerContext, string partialPath)
        {
            CorrectNSDViewPAth(controllerContext, ref partialPath);

            return base.CreatePartialView(controllerContext, partialPath);
        }

        protected override bool FileExists(ControllerContext controllerContext, string virtualPath)
        {
            CorrectNSDViewPAth(controllerContext, ref virtualPath);

            bool result = base.FileExists(controllerContext, virtualPath);

            return result;
        }

        private void CorrectNSDViewPAth(ControllerContext controllerContext, ref string virtualPath)
        {
            string pat = @"^V[0-9]+R[0-9]+$";
            Regex r = new Regex(pat);
            string versionInfo = controllerContext.Controller.GetType().FullName.Split('.').SingleOrDefault(a => r.Match(a).Success);
            if (versionInfo != null)
            {
                int place = virtualPath.LastIndexOf("/");
                virtualPath = virtualPath.Remove(place , 1).Insert(place, "/"+versionInfo+"/");
            }

        }

    }
}
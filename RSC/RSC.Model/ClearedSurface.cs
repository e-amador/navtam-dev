﻿namespace RSC.Model
{
    /// <summary>
    /// Data Elements related to the Cleared Area of the Runway.
    /// </summary>
    public class ClearedSurface
    {
        /// <summary>
        /// There is some Runway contamination.
        /// </summary>
        public ContaminantIns[] Contaminants { get; set; }
        /// <summary>
        /// Runway condition code on cleared portion of the runway (0 - 6)
        /// </summary>
        public string RwyCC { get; set; }
    }
}
﻿export interface RSCProposal {

    /**Attritute: The version of the RSC schema to which the message conforms. */
    Version: string;

    /**Attritute: The originator (source) of the message. This should consist of a unique company name, application name and version id. */
    Origin: string;

    /**Attritute: The date and time when the message was created. */
    Created: Date | string;

    /**Aerodrome to which this RSC relates */
    Aerodrome: string;

    /**
     * Runway observations. All Runways must exist in the NAV CANADA
     * Aeronautical Data Management System(ADMS).
     */
    Runways: Runway[];

    /**Taxiway Observations freetext. */
    Taxiways: FreeformText;

    /**Apron Observations - freetext. */
    Aprons: FreeformText;

    /**General remarks - freetext. */
    GeneralRemarks: FreeformText;

    /**Provide dat/time for next planned observation. Can be any time at or after the current time. */
    NextPlannedObs: Date | string | null;

    /**Conditions are changing rapidly. A contact must be provided. */
    ConditionsChangingRapidly: Telephone;

    /**Cancel current RSC. */
    CancelCurrentRSC: boolean;
}

export interface RSCResponse {
    Version: number;
    Status: ResponseStatus;
    Mode: string;
    Messages: RSCResponseMessage[];
    RSC: RSCText;
}

export interface RSCResponseMessage {
    Number: number;
    Description: string;
    RunwayId: string;
    Third: number | null;
    Status: ResponseStatus;

}

export enum ResponseStatus {
    Ok = 0,
    Warning = 1,
    Error = 2
}

export interface RSCText {
    EnglishRSC: string;
    FrenchRSC: string;
}


/**Data elements related to a single runway. */
export interface Runway {
    new(runwayId: string): Runway;
    RunwayId: string;
    ObsTimeUTC: Date | string | null;
    ClearedPortion: RunwayClearedPortion;
    ClearedFull: boolean;
    CentredClearedWidth: number;
    ClearedPortionOffsets: ClearedPortionOffset[];
    RemainingWidth: RemainingWidth;
    Snowbanks: SnowbanksRunway;
    NoWinterMaintenance: boolean;
}

/**Remaining width of the runway */
export interface RemainingWidth {
    /**Remaining width of the runway */
    ContaminantFI: ContaminantFtIns;
}

/**Contaminant identification, coverage and depth (if appropriate) in feet or inches */
export interface ContaminantFtIns {
    /**
     * Type of contamination to be found on this portion of the runway - that
     * must be accompanied by a depth modifier.This element provides the ability to specify
     * differing layers of contamination.
     */
    ContamType: ContaminantWithDepthEnum | null;

    /**Depth of contamination. */
    ContamDepthFI: ContamDepthFtIn;

    /**
     * Type of contamination that does not have a Depth modifier to be found on this
     * portion of the runway.This element provides the ability to specify differing layers of
     * contamination.
     */
    ContamTypeNoDepth: ContaminantWithNoDepthEnum | null;

    HasDepth: boolean;
}

/**Contaminant height/depth as Trace or inches or feet */
export interface ContamDepthFtIn {
    /**Pattern for just a TRACE of the contaminant. */
    ContamTrace: boolean;

    /**Restrictions for Depth of Contamination in inches (with value 1.5 for Contaminants in Cleared Portion) */
    ContamInches: number;

    /**Restrictions for Depth of Contamination in feet */
    ContamFeet: number;
}

export interface FreeformText {
    /**English text should always be present if the parent data element is specified. */
    EnglishText: string;

    /**French text is optional except for certain aerodromes. */
    FrenchText: string;
}

export interface Telephone {
    /**Three digits area code */
    AreaCode: string;

    /**Seven digit number. */
    Number: string;

    /**Extension. */
    Extension: string;
}


/**Complex structure describing the surface condition of the cleared portion.  This structure allows reporting in thirds or as an average across the entire surface. */
export interface RunwayClearedPortion extends ClearedPortion {
    /**Treatment applied to this runway. */
    Treatments: TreatmentEnum[];

    /**
     * An additional text comment may be provided. This field should only be used
     * for information that cannot be accommodated using existing defined fields.
     */
    ContaminantComment: FreeformText;

    /**Complex structure describing additional Runway Surface Conditions. */
    OtherConditions: OtherConditions;

    /**Complex Structure describing the Runway Friction. */
    AverageRwyFriction: RwyFriction;
}

/**Height of snowbank in feet and/or inches. */
export interface BankHeight {
    /**Height in feet. */
    BankHeightFt: number;

    /**Height in inches. */
    BankHeightInch: number;
}

/**Distance of snowbank from edge of runway. */
export interface BankEdge {
    /**
     * Rwy edge to Snowbank distance in feet.
     * @remarks  Valid value must be in the range (1 - 200).
     */
    BankEdgeFt: number;

    /**The snowbank is located on the runway edge. */
    OnRwyEdge: boolean;

    /**
     * Rwy edge to Snowbank distance in inches.
     * @remarks  Valid value must be in the range (1 - 100).
     */
    BankEdgeIns: number;
}

export interface ClearedPortion {
    /**Average of the Runway Surface. */
    Average?: ClearedSurface;

    /**List of Third of Runway (expected 3 thirds) */
    RunwayThirds?: ClearedSurfaceThird[];

    /**Is Average or Thirds */
    IsAverage: boolean;
}

/**Offset of the cleared portion limits from the runway centreline. */
export interface ClearedPortionOffset {
    /**The offset in feet from the centreline to the side indicated (in OffsetSide) of the cleared portion. */
    OffsetFT: number;

    /**
     * The side of the centreline for the current edge of the cleared area. Either E-W
     * or N-S the latter enumerations depending upon exact runway orientation: Low end # from 05 to 13
     * use N/S; anything else use E/W.
     */
    OffsetSide: OffsetSideEnum;
}

/**Data Elements related to the Cleared Area of the Runway. */
export interface ClearedSurface {
    /**There is some Runway contamination. */
    Contaminants: ContaminantIns[];

    /**Runway condition code on cleared portion of the runway (0 - 6) */
    RwyCC: string;
}

/**Data Elements related to the Cleared Area of the Runway. */
export interface ClearedSurfaceThird {
    /**
     * Simple element indicating which portion of the runway that the next higher level
     * structure refers to: 1 refers to third closest to lowest runway designator. 2 refers to middle portion. 
     * 3 refers to runway third closest to highest runway designator.
     * If using average, use 1.
     */
    Portion: number;

    /**Contaminant descriptions for runway. */
    Contaminants: ContaminantIns[];

    /**Complex Structure describing the Runway Friction. */
    RwyFrictionThird: RwyFriction;

    /**Structure describing the Runway Condition Code. */
    RwyCC: string;
}

/**Contaminant depth as Trace or inches. */
export interface ContamDepth {
    /**Depth of Contamination in inches. */
    ContamInches: number;

    /**There is just a TRACE of the contaminant. */
    ContamTrace: boolean;
}

/**Contaminant identification, coverage and depth (if appropriate) in feet or inches. */
export interface ContaminantIns {
    /**
     * Percentage of the current runway portion covered by the indicated contamination.
     * The total of all defined percentages must not exceed 100. If less than 100 it will be assumed
     * that the remaining percentage is bare and dry.
     * These are pre-defined values in the schema.
     */
    ContamCoverage: number;

    /**
     * Type of contamination to be found on this portion of the runway - that
     * must be accompanied by a depth modifier.This element provides the ability to specify
     * differing layers of contamination.
     */
    ContamType: ContaminantWithDepthEnum | null;

    /**Depth of contamination. */
    ContamDepthI: ContamDepth;

    /**
     * Type of contamination that does not have a Depth modifier to be found on this
     * portion of the runway.This element provides the ability to specify differing layers of
     * contamination.
     */
    ContamTypeNoD: ContaminantWithNoDepthEnum | null;
    /**The contaminant has depth or not. */

    HasDepdth: boolean;
}

export interface OCHeightType {
    /**Constructor */
    new(value: number, inInches: boolean): OCHeightType;

    /**Height messured in inches */
    InInches: boolean;

    /**Height messured in feet */
    InFeet: boolean;

    /**Depth/height of Contamination in inches/feet. */
    Value: number;
}

/**Other conditions affecting the cleared portion of the runway. */
export interface OtherConditions {
    /**Structure to define area type 'Other Conditions' ON the runway and which do NOT have an associated height. */
    OtherConditArea: OtherConditArea;
    /**
     * Structure is used to define other, generally 'edge' related conditions such as
     * windrows and snow drifts in terms of the location(as a distance from a specified runway edge or
     * edges, and across intersections with other runways) and height.
     */
    OtherConditEdge: OtherConditEdge;
    /**
     * An additional text comment may be provided. This field should only be used for
     * information that cannot be accommodated using existing defined fields.
     */
    OtherConditionsComment: FreeformText;
}

/**
 * Structure to define area type 'Other Conditions' ON the runway and which do NOT
 * have an associated height.
 */
export interface OtherConditArea {
    /**Type of additional surface condition. 'L' = Left HMI pane. */
    ConditTypeL: OtherConditionsAreaEnum;

    /**The distance, in feet, between the location and the threshold specified by ThresholdNum. */
    DistFromThreshold: number;

    /**Runway number corresponding to the threshold from which the location is measured. */
    ThresholdNum: number;
}

/**
 * Structure is used to define other, generally 'edge' related conditions such as
 * windrows and snow drifts in terms of the location(as a distance from a specified runway edge or
 * edges, and across intersections with other runways) and height.
 */
export interface OtherConditEdge {
    /**Type of additional surface condition. 'R' = Right HMI pane. */
    ConditTypeR: OtherConditionsEdgeEnum;

    /**Other Condition Height. */
    OCHeight: OCHeightType;

    /**The location of the condition. */
    OCLocation: OCLocationEnum;

    /**Distance: integer value from 0 to half the runway width. */
    OCDistance: number;

    /**Distance Unit of Measure: either ft or ins. */
    OCDistanceUOM: UnitOfMeasurement;

    /**Define which sides of the runway are affected by this condition. */
    OCSide: CardinalSideEnum[];

    /**
     * Runway ident in the form 'RWY-xx/yy'. This ident must exist in the
     * ADMS for the specified aerodrome.Identifies a runway intersection which is affected
     * by the conditions being reported for the subject runway.
     */
    AcrossRwy: string[];
}

/**Runway friction on cleared portion of runway. */
export interface RwyFriction {
    /**Time that the RF observation / measurement was made. */
    RFObsTime: Date | string;

    /**RF Coefficient in range 1 to 99 which maps to values of 0.01 to 0.99. */
    FrictionCoefficients: number[];

    /**Runway Temperature at the observation time. Celcius. */
    Temperature: string;

    /**Device used to measure the RF. */
    Device: MeasurementDeviceEnum | null;

    /**Boolean "true" value indicating that the CRFI is unreliable if present. */
    Unreliable: boolean;
}

/**Description of significant snowbanks by runway. */
export interface SnowbanksRunway {
    /**Height of snowbank in feet and/or inches. */
    BankHeight: BankHeight;

    /**Distance from edge of runway to snowbank. i.e. distance OUTSIDE runway edge. */
    BankEdge: BankEdge;

    /**
     * Location of snowbank with respect to the runway. Either E-W or N-S enumerations
     * depending upon exact runway orientation: Low end # from 05 to 13 use N/S; anything else use E/W.
     */
    BankSides: CardinalSideEnum[];
}

/**Defines which side of the runway is affected by a condition.  */
export enum CardinalSideEnum {
    E = 250,
    SE = 251,
    S = 252,
    SW = 253,
    W = 254,
    NW = 255,
    N = 256,
    NE = 257
}

export enum ContaminantWithDepthEnum {
    StandingWater = 60,
    DrySnow = 61,
    WetSnow = 62,
    Slush = 63,
    Unused64 = 64,
    DrySnowOnTopOfIce = 65,
    WetSnowOnTopOfIce = 66,
    SlushOnTopOfIce = 67,
    DrySnowOnTopOfCompactedSnow = 68,
    Unused69 = 69,
    Unused70 = 70,
    WaterOnTopOfCompactedSnow = 71,
    WetSnowOnTopOfCompactedSnow = 72
}

export enum ContaminantWithNoDepthEnum {
    Dry = 20,
    Unused21 = 21,
    Wet = 22,
    Frost = 23,
    Ice = 24,
    CompactedSnow = 25,
    Unused26 = 26,
    WetIce = 27,
    Unused28 = 28,
    CompactedSnowGravelMix = 29,
    Unused30 = 30,
    Unused31 = 31,
    SlipperyWhenWet = 32
}

export enum MeasurementDeviceEnum {
    TapleyMeter = 130,
    JamesDecelerometer = 131,
    Bowmonk = 132,
    DecelerometerTES = 133
}

/**The location of the condition */
export enum OCLocationEnum {
    AlongInsideRedl = 127, // LE LONG INTERIEUR REDL
    AlongInsideRwyEdge = 128, // LE LONG INTERIEUR DU BORD DE PISTE
    AlongClearedWidth = 129, // LE LONG DE LA LARGEUR DEGAÉE
    FMCL = 134, // FM CL
}

/**
* The side of the centreline for the current edge of the cleared area. Either E-W
* or N-S the latter enumerations depending upon exact runway orientation: Low end # from 05 to 13
* use N/S; anything else use E/W.
*/
export enum OffsetSideEnum {
    East = 11,
    West = 12,
    North = 13,
    South = 14
}

export enum OtherConditionsAreaEnum {
    IcePatches = 122,
    CompactedSnowPatches = 123,
    StandingWaterPatches = 124
}

export enum OtherConditionsEdgeEnum {
    SnowDrifts = 120,
    Windrows = 121,
    SnowBanks = 125
}
export enum TreatmentEnum {
    LoseSand = 100,
    Unused101 = 101,
    Graded = 104,
    Packed = 106,
    Unused107 = 107,
    ChemicallyTreated = 108,
    Scarified = 109
}


/**Distance Unit of Measure: either ft or ins. */
export enum UnitOfMeasurement {
    Inches = 9, // INCHES (INS) / POUCES (PO)
    Feet = 10, // FEET (FT) / PIEDS (PI)
}
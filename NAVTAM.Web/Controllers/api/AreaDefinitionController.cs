﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using NavCanada.Core.Common.Contracts;
using NavCanada.Core.Data.NotamWiz.Contracts;
using NavCanada.Core.Proxies.AeroRdsProxy;
using NAVTAM.Helpers;
using NAVTAM.NsdEngine.AIRSPACE.V10;

namespace NAVTAM.Controllers.api
{
    [Authorize]
    [RoutePrefix("api")]
    public class AreaDefinitionController : ApiBaseController
    {
        public AreaDefinitionController(IUow uow, IAeroRdsProxy aeroRdsProxy, IGeoLocationCache geoLocationCache, ILogger logger) : base(uow, aeroRdsProxy, geoLocationCache, logger)
        {
        }

        /// > POLYGON
        /// 

        [Route("areadefinition/calculate")]
        [HttpPost]
        public IHttpActionResult Calculate([FromBody] AreaDefinitionViewModel vm)
        {
            if(vm.Points.Length < 2)
            {
                
                // what business rules here are needed
                //get list of points
                //validate list of points
                // construct multiline or polygon ??
                // return calculation
            }

            throw new NotImplementedException();
        }
        //get list of points
        //validate list of points
        // construct multiline or polygon ??
        // return calculation
    }
}
﻿using System;

namespace NAVTAM.NsdEngine.CARSCLSD.V10
{
    public class CarsClsdViewModel : TokenViewModel
    {
        public CarsClsdViewModel()
        {
            DaysOfWeek = new DaysOfOperation();
        }
        public int CarsStatusId { get; set; }
        public bool Everyday { get; set; }
        public string StartHour { get; set; }
        public string EndHour { get; set; }
        public DaysOfOperation DaysOfWeek { get; set; }
        public bool DaylightSavingTime { get; set; }
        public bool AddReason { get; set; }
        public string Reason { get; set; }
        public string ReasonFr { get; set; }
        public string SubjectLocation { get; set; }
    }

    public class DaysOfOperation
    {
        public DaysOfOperation(bool Mon = false, bool Tue = false, bool Wed = false, bool Thu = false, bool Fri = false, bool Sat = false, bool Sun = false)
        {
            Monday = Mon;
            Tuesday = Tue;
            Wednesday = Wed;
            Thursday = Thu;
            Friday = Fri;
            Saturday = Sat;
            Sunday = Sun; 
        }
        public bool Monday { get; set; }
        public bool Tuesday { get; set; }
        public bool Wednesday { get; set; }
        public bool Thursday { get; set; }
        public bool Friday { get; set; }
        public bool Saturday { get; set; }
        public bool Sunday { get; set; }
    }

}
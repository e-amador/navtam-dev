﻿using NavCanada.Core.Domain.Model.Enums;
using System;
using System.Data.Entity.Spatial;

namespace NavCanada.Core.Domain.Model.Dtos
{
    public class NotamDto
    {
        public Guid Id { get; set; }
        public Guid RootId { get; set; }
        public string NotamId { get; set; }
        public string ReferredNotamId { get; set; }
        public string SiteId { get; set; }
        public int ProposalId { get; set; }
        public string Originator { get; set; }
        public string OriginatorEmail { get; set; }
        public string OriginatorPhone { get; set; }
        public string ItemA { get; set; }
        public string Scope { get; set; }
        public string Series { get; set; }
        public string Traffic { get; set; }
        public string Purpose { get; set; }
        public string Code23 { get; set; }
        public string Code45 { get; set; }
        public string StartActivity { get; set; }
        public string EndValidity { get; set; }
        public DateTime Published { get; set; }
        public NotamType NotamType { get; set; }
        public bool IsReplaced { get; set; }
        public bool IsPermanent { get; set; }
        public bool IsEstimated { get; set; }
        public string ItemF { get; set;  }
        public string ItemG { get; set; }
        public string Coordinates { get; set; }
        public int Radius { get; set; }
        public int LowerLimit { get; set; }
        public int UpperLimit { get; set; }
        public bool Urgent { get; set; }
        public string Operator { get; set; }
        public string ModifiedByOrg { get; set; }
        public string ModifiedByUsr { get; set; }
        public DbGeography EffectArea { get; set;  }
        public string EffectAreaGeoJson { get; set; }
        public bool SeriesChecklist { get; set; }
        public bool IsObsolete { get; set; }
        public bool IsExpired { get; set; }
    }

}

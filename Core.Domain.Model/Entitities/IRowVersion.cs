﻿namespace NavCanada.Core.Domain.Model.Entitities
{
    public interface IRowVersion
    {
        byte[] RowVersion { get; }
    }
}
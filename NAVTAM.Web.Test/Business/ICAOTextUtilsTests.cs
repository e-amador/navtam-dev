﻿using Business.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NavCanada.Core.Domain.Model.Entitities;
using NavCanada.Core.Domain.Model.Enums;
using System;

namespace NAVTAM.Web.Test.Business
{
    [TestClass]
    public class ICAOTextUtilsTests
    {
        [TestMethod]
        public void ICAOTextUtils_CanGenerateICAOText()
        {
            // fail on cordinates
            Assert.IsFalse(ICAOTextUtils.CanGenerateText(new Notam()));
            // pass cordinates, not immediate, fails on StartActivity
            Assert.IsFalse(ICAOTextUtils.CanGenerateText(new Notam() { Coordinates = "514525N 1200925W" }));
            // pass cordinates, passes immediate, fails on EndActivity
            Assert.IsFalse(ICAOTextUtils.CanGenerateText(new Notam() { Coordinates = "514525N 1200925W", StartActivity = DateTime.UtcNow }));
            // pass cordinates, passes immediate, fails on EndActivity
            Assert.IsFalse(ICAOTextUtils.CanGenerateText(new Proposal() { Coordinates = "514525N 1200925W" }));
            // valid cases
            Assert.IsTrue(ICAOTextUtils.CanGenerateText(new Notam() { Coordinates = "514525N 1200925W", StartActivity = DateTime.UtcNow, EndValidity = DateTime.UtcNow.AddDays(30) }));
            Assert.IsTrue(ICAOTextUtils.CanGenerateText(new Notam() { Coordinates = "514525N 1200925W", StartActivity = DateTime.UtcNow, Permanent = true }));
            Assert.IsTrue(ICAOTextUtils.CanGenerateText(new Notam() { Coordinates = "514525N 1200925W", StartActivity = DateTime.UtcNow, Type = NotamType.C }));
        }

        [TestMethod]
        public void ICAOTextUtils_FormatNotamId()
        {
            var number = ICAOTextUtils.FormatNotamId("X", 23, 2017);
            Assert.IsTrue(number == "X0023/17");
        }

        [TestMethod]
        public void ICAOGenerator_TryParseNotamId()
        {
            string s;
            int n, y;
            Assert.IsTrue(ICAOTextUtils.TryParseNotamId("X0023/17", out s, out n, out y));
            Assert.AreEqual("X", s);
            Assert.AreEqual(23, n);
            Assert.AreEqual(2017, y);
        }

        [TestMethod]
        public void ICAOGenerator_TryParseNotamId_invalid_cases()
        {
            string s;
            int n, y;

            Assert.IsFalse(ICAOTextUtils.TryParseNotamId("", out s, out n, out y), "Must fail on empty notam id.");
            Assert.IsFalse(ICAOTextUtils.TryParseNotamId("0023/17", out s, out n, out y), "Must fail on shorter than 8 chars.");

            Assert.IsFalse(ICAOTextUtils.TryParseNotamId(" 0023/17", out s, out n, out y), "Must fail on invalid series < 'A'. ");
            Assert.IsFalse(ICAOTextUtils.TryParseNotamId("a0023/17", out s, out n, out y), "Must fail on invalid series > 'Z'.");

            Assert.IsFalse(ICAOTextUtils.TryParseNotamId("ABCD3/17", out s, out n, out y), "Must fail on invalid number.");
            Assert.IsFalse(ICAOTextUtils.TryParseNotamId("A-123/17", out s, out n, out y), "Must fail on invalid number.");

            Assert.IsFalse(ICAOTextUtils.TryParseNotamId("A1234/X7", out s, out n, out y), "Must fail on invalid year.");
            Assert.IsFalse(ICAOTextUtils.TryParseNotamId("A1234/-7", out s, out n, out y), "Must fail on invalid year.");
        }

        [TestMethod]
        public void ICAOTextUtils_FormatNotamNumber_Empty_Proposal()
        {
            var proposalRefNumber = ICAOTextUtils.FormatNotamNumber(new Proposal());
            Assert.IsTrue(proposalRefNumber == string.Empty);
        }

        [TestMethod]
        public void ICAOTextUtils_FormatNotamNumber_New()
        {
            var newNotam = new Notam()
            {
                Type = NotamType.N,
                Series = "X",
                Number = 23,
                Year = 2017
            };
            var numberWithRef = ICAOTextUtils.FormatNotamNumber(newNotam);
            Assert.IsTrue(numberWithRef == "X0023/17 NOTAMN");
        }

        [TestMethod]
        public void ICAOTextUtils_FormatNotamNumber_Replace()
        {
            var replaceNotam = new Notam()
            {
                Type = NotamType.R,
                Series = "X",
                Number = 23,
                Year = 2017,
                ReferredSeries = "A",
                ReferredNumber = 13,
                ReferredYear = 2017,
            };
            var numberWithRef = ICAOTextUtils.FormatNotamNumber(replaceNotam);
            Assert.IsTrue(numberWithRef == "X0023/17 NOTAMR A0013/17");
        }

        [TestMethod]
        public void ICAOTextUtils_FormatMultipartNotamNumber()
        {
            var newNotam = CreateNotam(NotamType.N, "X", 1, 2017, "A", 2, 2017);
            Assert.AreEqual(ICAOTextUtils.FormatMultipartNotamNumber(newNotam, 3, 3), "X0001/17D03 NOTAMN");

            var replaceNotam = CreateNotam(NotamType.R, "X", 1, 2017, "A", 2, 2017);
            Assert.AreEqual(ICAOTextUtils.FormatMultipartNotamNumber(replaceNotam, 2, 11), "X0001/17C11 NOTAMR A0002/17");

            var cancelNotam = CreateNotam(NotamType.C, "X", 1, 2017, "A", 2, 2017);
            Assert.AreEqual(ICAOTextUtils.FormatMultipartNotamNumber(cancelNotam, 2, 11), "X0001/17C11 NOTAMC A0002/17");
        }

        static Notam CreateNotam(NotamType type, string series, int number, int year, string refSeries, int refNumber, int refYear)
        {
            return new Notam()
            {
                Type = type,
                Series = series,
                Number = number,
                Year = year,
                ReferredSeries = refSeries,
                ReferredNumber = refNumber,
                ReferredYear = refYear
            };
        }

        [TestMethod]
        public void ICAOTextUtils_FormatNotamNumber_Cancel()
        {
            var cancelNotam = new Notam()
            {
                Type = NotamType.C,
                Series = "X",
                Number = 23,
                Year = 2017,
                ReferredSeries = "A",
                ReferredNumber = 13,
                ReferredYear = 2017,
            };
            var numberWithRef = ICAOTextUtils.FormatNotamNumber(cancelNotam);
            Assert.IsTrue(numberWithRef == "X0023/17 NOTAMC A0013/17");
        }

        [TestMethod]
        public void ICAOTextUtils_FormatStartActivity()
        {
            var startActivityText = ICAOTextUtils.FormatStartActivity(true, null);
            Assert.IsTrue("IMMEDIATE".Equals(startActivityText));

            var date = new DateTime(2017, 08, 17, 10, 38, 58);
            startActivityText = ICAOTextUtils.FormatStartActivity(false, date);
            Assert.IsTrue("1708171038".Equals(startActivityText));
        }

        [TestMethod]
        public void ICAOTextUtils_FormatEndValidity()
        {
            var endValidityText = ICAOTextUtils.FormatEndValidity(true, false, null);
            Assert.IsTrue("PERM".Equals(endValidityText));

            var date = new DateTime(2017, 08, 17, 10, 38, 58);
            endValidityText = ICAOTextUtils.FormatEndValidity(false, false, date);
            Assert.IsTrue("1708171038".Equals(endValidityText));

            endValidityText = ICAOTextUtils.FormatEndValidity(false, true, date);
            Assert.IsTrue("1708171038EST".Equals(endValidityText));
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ICAOTextUtils_FormatEndValidity_InvalidDate()
        {
            ICAOTextUtils.FormatEndValidity(false, false, null);
        }

        [TestMethod]
        public void ICAOTextUtils_FormatEndValidity_Cancel()
        {
            var endValidityText = ICAOTextUtils.FormatEndValidity(new Notam() { Type = NotamType.C, EndValidity = DateTime.UtcNow });
            Assert.IsNull(endValidityText);
        }

        [TestMethod]
        public void ICAOTextUtils_FormatEndValidity_New()
        {
            var endValidityText = ICAOTextUtils.FormatEndValidity(new Notam() { Type = NotamType.N, EndValidity = DateTime.UtcNow });
            Assert.IsNotNull(endValidityText);
        }

        [TestMethod]
        public void ICAOTextUtils_FormatEndValidity_Replace()
        {
            var endValidityText = ICAOTextUtils.FormatEndValidity(new Notam() { Type = NotamType.R, EndValidity = DateTime.UtcNow });
            Assert.IsNotNull(endValidityText);
        }

        [TestMethod]
        public void ICAOTextUtils_FormatQLine()
        {
            var notam = new Notam()
            {
                Fir = "CZVR",
                Code23 = "OB",
                Code45 = "CE",
                Traffic = "V",
                Purpose = "M",
                Scope = "E",
                LowerLimit = 0,
                UpperLimit = 35,
                Radius = 2
            };

            // no coordinate rounding
            notam.Coordinates = "514525N 1200925W";
            var qLine = ICAOTextUtils.FormatQLine(notam);
            Assert.IsTrue("CZVR/QOBCE/V/M/E/000/035/5145N12009W002".Equals(qLine));

            // with coordinate rounding
            notam.Coordinates = "514545N 1200945W";
            qLine = ICAOTextUtils.FormatQLine(notam);
            Assert.IsTrue("CZVR/QOBCE/V/M/E/000/035/5146N12010W002".Equals(qLine));
        }

        [TestMethod]
        public void ICAOTextUtils_RoundDMSCoordinates()
        {
            Assert.IsTrue("5145N 12009W" == ICAOTextUtils.RoundDMSCoordinates("514525N 1200925W", " "));
            Assert.IsTrue("5146N 12010W" == ICAOTextUtils.RoundDMSCoordinates("514545N 1200955W", " "));
            Assert.IsTrue("5145N12009W" == ICAOTextUtils.RoundDMSCoordinates("514525N 1200925W", string.Empty));
            Assert.IsTrue("5146N12010W" == ICAOTextUtils.RoundDMSCoordinates("514545N 1200955W", string.Empty));
        }
    }
}

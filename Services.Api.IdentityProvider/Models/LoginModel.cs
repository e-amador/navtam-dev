﻿namespace NavCanada.Services.Api.IdentityProvider.Models
{
    using System.ComponentModel.DataAnnotations;

    public class LoginModel
    {
        [Required(ErrorMessage = "Field 'User Name' can't be empty")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Field 'Password' can't be empty")]
        public string Password { get; set; }
    }
}
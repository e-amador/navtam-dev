﻿using RSC.Model.Enums;

namespace RSC.Model
{
    /// <summary>
    /// Offset of the cleared portion limits from the runway centreline.
    /// </summary>
    public class ClearedPortionOffset
    {
        /// <summary>
        /// The offset in feet from the centreline to the side indicated (in OffsetSide) of the cleared portion.
        /// </summary>
        public int OffsetFT { get; set; }
        /// <summary>
        /// The side of the centreline for the current edge of the cleared area. Either E-W
        /// or N-S the latter enumerations depending upon exact runway orientation: Low end # from 05 to 13
        /// use N/S; anything else use E/W.
        /// </summary>
        public OffsetSideEnum OffsetSide { get; set; }
    }
}
﻿namespace NavCanada.Applications.NotamWiz.Web.NSDs.TwyClsd.V0R1
{
    using System.Collections.Generic;

    using NSDs;

    public class ViewModel : NotamProposalViewModelBase
    {
        public bool AllTaxiwaysSelected
        {
            get;
            set;
        }

        public bool HasClosedTaxiways
        {
            get;
            set;
        }

        public List<ClosedTaxiway> ClosedTaxiways
        {
            get;
            set;
        }

        public int ClosureReason
        {
            get;
            set;
        }

        public string ClosureReasonText
        {
            get;
            set;
        }

        public string ClosureReasonTextFrench
        {
            get;
            set;
        }

        public int ClosureType
        {
            get;
            set;
        }

        public string ClosureTypeName
        {
            get;
            set;
        }

        public bool IsOtherClosureReason
        {
            get;
            set;
        }

        public bool IsPartialClosure
        {
            get;
            set;
        }

        public string Point1
        {
            get;
            set;
        }

        public string Point1French
        {
            get;
            set;
        }

        public string Point2
        {
            get;
            set;
        }

        public string Point2French
        {
            get;
            set;
        }

        public string TaxiwayDesignator
        {
            get;
            set;
        }

        public string TaxiwayId
        {
            get;
            set;
        }
    }

    public class ClosedTaxiway
    {
        public int ClosureType
        {
            get;
            set;
        }

        public string ClosureTypeName
        {
            get;
            set;
        }

        public bool IsPartialClosure
        {
            get;
            set;
        }

        public string Point1
        {
            get;
            set;
        }

        public string Point1French
        {
            get;
            set;
        }

        public string Point2
        {
            get;
            set;
        }

        public string Point2French
        {
            get;
            set;
        }

        public string TaxiwayDesignator
        {
            get;
            set;
        }

        public string TaxiwayId
        {
            get;
            set;
        }
    }
}
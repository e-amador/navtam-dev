﻿using NavCanada.Core.Domain.Model.Entitities;
using System.Data.Entity.ModelConfiguration;

namespace NavCanada.Core.Data.EF.Configurations
{
    public class NdsNotamMessageConfig : EntityTypeConfiguration<NdsNotamMessage>
    {
        public NdsNotamMessageConfig()
        {
            HasKey(e => new { e.MessageId, e.NotamId } );

            HasRequired(e => e.Notam)
                .WithMany()
                .HasForeignKey(e => e.NotamId)
            .WillCascadeOnDelete(false);

            HasRequired(e => e.Message)
                .WithMany()
                .HasForeignKey(e => e.MessageId)
            .WillCascadeOnDelete(false);
        }
    }
}

﻿using NavCanada.Core.Domain.Model.Enums;
using NavCanada.Core.TaskEngine.Contracts;
using NavCanada.Core.TaskEngine.Mto;
using System;

namespace NavCanada.Core.TaskEngine.Tasks.NotificationTasks
{
    class EmailNotificationTask : IMessageTask
    {
        public void Execute(IMessageTaskContext context)
        {
            var notifier = context.ExternalServices.Notifier;
            var logger = context.ExternalServices.Logger;
            var loggerType = GetType();

            var recipients = "";
            var subject = "";
            var content = "";

            try
            {
                // if this were to fail we don't want to put the system down
                context.Mto.Add(MessageDefs.OverridesCriticalFailureKey, true, DataType.Boolean);

                recipients = context.Mto.GetProperty(MessageDefs.EmailRecipientKey, string.Empty);
                subject = context.Mto.GetProperty(MessageDefs.EmailSubjectKey, string.Empty);
                content = context.Mto.GetProperty(MessageDefs.EmailContentKey, string.Empty);

                logger.LogDebug(loggerType, $"Begin sending email To: '{recipients}' Subject: '{subject}' ");

                var response = notifier.NotifyByEmail(recipients, subject, content);
                if (response.Code != ApiStatusCode.Success)
                {
                    var reason = response.Code == ApiStatusCode.ConnectionError ? "Connection failed" : "Unknown error";
                    throw new Exception($"{reason} sending email. Error message: '{response.ErrorMessage}'");
                }

                logger.LogDebug(loggerType, $"End sending email To: '{recipients}' Subject: '{subject}' ");
            }
            catch (Exception ex)
            {
                logger.LogError(ex, loggerType, $"Fail to send email To: '{recipients}' Subject: '{subject}' Content: {content}");
                throw;
            }
        }
    }
}

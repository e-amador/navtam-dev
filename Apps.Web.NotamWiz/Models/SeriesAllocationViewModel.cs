﻿namespace NavCanada.Applications.NotamWiz.Web.Models
{
    using NavCanada.Core.Domain.Model.Enums;

    public class SeriesAllocationViewModel
    {

        public int Id { get; set; }
      
        public int RegionId { get; set; }

      
        public string Series { get; set; }

      
        public DisseminationCategory DisseminationCategory { get; set; }

      
        public string QCode { get; set; }
    }
}
﻿namespace NavCanada.Core.TaskEngine.PerformanceCounters
{
    using NavCanada.Core.Common.Contracts;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    /// <summary>
    /// Manages creation and use of performance counters and categories.
    /// </summary>
    public static class PerfCounterManager
    {
        private const string BaseNameId = "Base";
        private const string MachineNameId = "."; //set for local host
        private static bool enabled; // by defaul performnace counters will be disabed

        private static readonly Dictionary<string, CategoryData> categoriesData = new Dictionary<string, CategoryData>();

        //private static readonly ILogger _logger;

        // If we encounter problems registering counter name we need to re-create a category to fix it.
        private static bool recreateCategory = false;

        private class CategoryData
        {
            public string Name { get; set; }

            public PerformanceCounterCategory Category { get; set; }

            public bool IsCategoryCreated { get { return Category != null; } }

            public readonly List<CounterCreationData> CounterData = new List<CounterCreationData>();
        }

        /// <summary>
        /// This methods should be called in order to activate the performance counters.
        /// Otherwise perfromance counters will be deactivated (dummy counters will be used).
        /// </summary>
        public static void ActivateCounters()
        {
            enabled = true;
        }

        /// <summary>
        /// Registers simple performance counter.  Will not re-register if already present.
        /// </summary>
        /// <param name="categoryName"></param>
        /// <param name="counterName"></param>
        /// <param name="counterType"></param>
        /// <param name="reset">If category existed, and set to true, it will be reset it to zero.</param>
        public static void RegisterSimplePerfCounter(string categoryName, string counterName, PerformanceCounterType counterType, string counterHelp = "", bool reset = false)
        {
            if (!enabled)
                return;

            CategoryData catData;
            if (!categoriesData.TryGetValue(categoryName, out catData))
            {
                catData = new CategoryData() { Name = categoryName };
                categoriesData.Add(categoryName, catData);
            }

            if (!catData.IsCategoryCreated)
                catData.CounterData.Add(new CounterCreationData { CounterName = counterName, CounterType = counterType, CounterHelp = counterHelp });

            //if(reset)
            GetSimplePerfCounter(categoryName, counterName, reset);
        }

        /// <summary>
        /// Registeres avergate performance counter.
        /// </summary>
        /// <param name="categoryName"></param>
        /// <param name="counterName"></param>
        /// <param name="categoryHelp"></param>
        /// <param name="reset">If true, and counter existed, will also reset it.</param>
        public static void RegisterAveragePerfCounter(string categoryName, string counterName, string categoryHelp = "", bool reset = false)
        {
            if (!enabled)
                return;

            RegisterSimplePerfCounter(categoryName, counterName, PerformanceCounterType.AverageTimer32, categoryHelp, reset);
            RegisterSimplePerfCounter(categoryName, counterName + BaseNameId, PerformanceCounterType.AverageBase, categoryHelp, reset);
        }

        /// <summary>
        /// Initializes a performance counter category.  Will create or re-create
        /// if a category doesn't exist, or it is different then the one requested.
        /// </summary>
        /// <param name="categoryName">Category needs to exist.</param>
        /// <param name="categoryType"></param>
        /// <param name="categoryHelp"></param>
        public static void InitializeCategory(string categoryName, PerformanceCounterCategoryType categoryType, string categoryHelp = "")
        {
            if (!enabled)
                return;

            CategoryData catData;
            if (!categoriesData.TryGetValue(categoryName, out catData))
            {
                //logger.Error("No counters registered for category: " + categoryName);
                return;
            }

            // We only need to re-create the category, if counters changed.
            // This only applies when an update is deployed.
            if (PerformanceCounterCategory.Exists(categoryName)
                && (recreateCategory || new PerformanceCounterCategory(categoryName).GetCounters().Length != catData.CounterData.Count))
            {
                PerformanceCounterCategory.Delete(categoryName);
            }

            if (!PerformanceCounterCategory.Exists(categoryName))
            {
                catData.Category = PerformanceCounterCategory.Create(categoryName, categoryHelp, categoryType, new CounterCreationDataCollection(catData.CounterData.ToArray()));

                //note: CloseSharedResources flushes all these internal caches inside the internal PerformanceCounterLib
                //so the calls to  PerformanceCounterCategory.Exists always return the same answer
                //check: http://blog.dezfowler.com/search?updated-min=2007-01-01T00:00:00Z&updated-max=2008-01-01T00:00:00Z&max-results=28
                PerformanceCounter.CloseSharedResources();

                //logger.InfoFormat("Created {0} category.", categoryName);
                recreateCategory = false;
            }
        }

        /// <summary>
        /// Returns a simple perf counter, but avoids re-creating the instance if exists.
        /// Singel threading.
        /// </summary>
        /// <param name="currentCounter">If null will create a new one.</param>
        /// <param name="categoryName">Category needs to exist.</param>
        /// <param name="counterName">Counter needs to exist.</param>
        /// <param name="reset">If true, will reset the counter to zero. Defaults to false.</param>
        /// <returns>SimpleCounter</returns>
        public static ISimpleCounter GetSimplePerfCounter(ISimpleCounter currentCounter, string categoryName,
            string counterName, bool reset = false)
        {
            return currentCounter ?? (currentCounter = GetSimplePerfCounter(categoryName, counterName, reset));
        }

        /// <summary>
        /// Returns a simple performance counter.
        /// Single threading.
        /// </summary>
        /// <param name="categoryName">Category needs to exist and be initialized.</param>
        /// <param name="counterName">Counter needs to exist.</param>
        /// <param name="reset">If true, will reset the counter to zero. Defaults to false.</param>
        /// <returns></returns>
        public static ISimpleCounter GetSimplePerfCounter(string categoryName, string counterName, bool reset = false)
        {
            if (!enabled || recreateCategory)
                return new DummySimpleCounter();

            CategoryData catData;
            if (!categoriesData.TryGetValue(categoryName, out catData))
            {
                //logger.Error("Category not registered or not initialized.");
                recreateCategory = true;
                return new DummySimpleCounter();
            }

            // If the categories have not been initialized or no elevated privilages
            // this may error out.
            PerformanceCounterCategory cat;
            try
            {
                cat = PerformanceCounterCategory.GetCategories().First(c => c.CategoryName == categoryName);
            }
            catch (Exception)
            {
                //logger.ErrorFormat("Error getting category {0}", e, categoryName);
                recreateCategory = true;
                return new DummySimpleCounter();
            }

            // PerformanceCounterCategory.GetCategories may not return a category.
            if (cat == null)
            {
                //logger.ErrorFormat("Category {0} not found.", categoryName);
                recreateCategory = true;
                return new DummySimpleCounter();
            }
            PerformanceCounter counter;

            try
            {
                counter = cat.GetCounters().First(c => c.CounterName == counterName);
            }
            catch (Exception)
            {
                //logger.ErrorFormat("Counter {0} not registered or not initialized.", e, counterName);
                recreateCategory = true;
                return new DummySimpleCounter();
            }

            if (counter == null)
            {
                //logger.ErrorFormat("Counter {0} is null returning a dummy counter.", counterName);
                recreateCategory = true;
                return new DummySimpleCounter();
            }

            counter.ReadOnly = false;
            counter.MachineName = MachineNameId;

            var simpleCounter = new SimpleCounter(counter);

            if (reset)
                simpleCounter.Reset();

            return simpleCounter;
        }

        /// <summary>
        /// Returns an average performance counter, but avoid re-creating the instance if exists.
        /// </summary>
        /// <param name="currentCounter">If null a new one will be created.</param>
        /// <param name="categoryName">Category needs to exist and be initialized.</param>
        /// <param name="counterName">Counter needs to exist.</param>
        /// <param name="reset">If true, will reset the counter to zero. Defaults to false.</param>
        /// <returns></returns>
        public static IAverageCounter GetAveragePerfCounter(IAverageCounter currentCounter, string categoryName,
            string counterName, bool reset = false)
        {
            return currentCounter ?? (currentCounter = GetAveragePerfCounter(categoryName, counterName, reset));
        }

        /// <summary>
        /// Returns an average perfromance counter.
        /// Single threading.
        /// </summary>
        /// <param name="categoryName">Category needs to exist and be initialized.</param>
        /// <param name="counterName">Counter needs to exist.</param>
        /// <param name="reset">If true, will reset the counter to zero. Defaults to false.</param>
        /// <returns></returns>
        public static IAverageCounter GetAveragePerfCounter(string categoryName, string counterName, bool reset = false)
        {
            if (!enabled || recreateCategory)
                return new DummyAverageCounter();

            CategoryData catData;
            if (!categoriesData.TryGetValue(categoryName, out catData))
                throw new InvalidOperationException("Category not registered or not initialized.");

            var counterNameBase = counterName + BaseNameId;

            var cat = PerformanceCounterCategory.GetCategories().First(c => c.CategoryName == categoryName);
            if (cat == null)
            {
                //logger.Error("Category not registered or not initialized.");
                recreateCategory = true;
                return new DummyAverageCounter();
            }

            var counter = cat.GetCounters().First(c => c.CounterName == counterName);
            if (counter == null)
            {
                //logger.Error("Counter not registered or not initialized.");
                recreateCategory = true;
                return new DummyAverageCounter();
            }

            var counterBase = cat.GetCounters().First(c => c.CounterName == counterNameBase);
            if (counterBase == null)
            {
                //logger.Error("counterBase not registered or not initialized.");
                recreateCategory = true;
                return new DummyAverageCounter();
            }

            counter.ReadOnly = false;
            counter.MachineName = MachineNameId;
            counterBase.ReadOnly = false;
            counterBase.MachineName = MachineNameId;

            var averageCounter = new AverageCounter(counter, counterBase);

            if (reset)
                averageCounter.Reset();

            return averageCounter;
        }

        /// <summary>
        /// Returns a new simple performanc counter.
        /// Multithreading
        /// </summary>
        /// <param name="categoryName">Category needs to exist and be initialized.</param>
        /// <param name="counterName">Counter needs to exist.</param>
        /// <param name="instanceName"></param>
        /// <returns></returns>
        public static ISimpleCounter CreateNewSimplePerfCounter(string categoryName, string counterName, string instanceName)
        {
            if (!enabled)
                return new DummySimpleCounter();

            CategoryData catData;
            if (!categoriesData.TryGetValue(categoryName, out catData))
            {
                //logger.ErrorFormat("CreateNewSimplePerfCounter(): Category {0} not registered or not initialized. Multithreading.", categoryName);
                recreateCategory = true;
                return new DummySimpleCounter();
            }

            var counter = new PerformanceCounter
            {
                CategoryName = categoryName,
                CounterName = counterName,
                InstanceName = instanceName,
                ReadOnly = false,
                MachineName = MachineNameId
            };
            return new SimpleCounter(counter);
        }

        /// <summary>
        /// Returns an instance of Average performance counter
        /// Multithreading.
        /// </summary>
        /// <param name="categoryName">Category needs to exist and be initialized.</param>
        /// <param name="counterName">Counter needs to exist.</param>
        /// <param name="instanceName"></param>
        /// <returns></returns>
        public static IAverageCounter CreateNewAveragePerfCounter(string categoryName, string counterName, string instanceName)
        {
            if (!enabled)
                return new DummyAverageCounter();

            CategoryData catData;
            if (!categoriesData.TryGetValue(categoryName, out catData))
            {
                //logger.Error("CreateNewAveragePerfCounter(): Category not registered or not initialized. Multithreading.");
                recreateCategory = true;
                return new DummyAverageCounter();
            }

            var counterNameBase = counterName + BaseNameId;
            var counter = new PerformanceCounter
            {
                CategoryName = categoryName,
                CounterName = counterName,
                InstanceName = instanceName,
                ReadOnly = false,
                MachineName = MachineNameId
            };
            var counterBase = new PerformanceCounter
            {
                CategoryName = categoryName,
                CounterName = counterNameBase,
                InstanceName = instanceName,
                ReadOnly = false,
                MachineName = MachineNameId
            };

            return new AverageCounter(counter, counterBase);
        }
    }
}
﻿import { Injectable } from '@angular/core';
import { Resolve,  ActivatedRouteSnapshot } from '@angular/router';

import { DataService } from './../shared/data.service'

@Injectable()
export class SubscriptionResolver implements Resolve<any> {

    constructor( private dataService : DataService ) {
    }

    resolve( route : ActivatedRouteSnapshot ) {
        const id = +route.params['id'];
        return this.dataService.getNdsClient(id).map(model => model);
    }
}
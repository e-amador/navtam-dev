﻿using RSC.Model.Enums;
using System;

namespace RSC.Model
{
    /// <summary>
    /// Runway friction on cleared portion of runway.
    /// </summary>
    public class RwyFriction
    {
        /// <summary>
        /// Time that the RF observation / measurement was made.
        /// </summary>
        public DateTime RFObsTime { get; set; }
        /// <summary>
        /// RF Coefficient in range 1 to 99 which maps to values of 0.01 to 0.99.
        /// </summary>
        public int FrictionCoefficients { get; set; }
        /// <summary>
        /// Runway Temperature at the observation time. Celcius.
        /// </summary>
        public string Temperature { get; set; }
        /// <summary>
        /// Device used to measure the RF.
        /// </summary>
        public MeasurementDeviceEnum? Device { get; set; }
        /// <summary>
        /// Boolean "true" value indicating that the CRFI is unreliable if present.
        /// </summary>
        public bool Unreliable { get; set; }
    }
}
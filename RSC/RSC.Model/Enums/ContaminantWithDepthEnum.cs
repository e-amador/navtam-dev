﻿namespace RSC.Model.Enums
{
    /// <summary>
    /// Contaminant type restrictions for contaminants with depth.
    /// </summary>
    public enum ContaminantWithDepthEnum : int
    {
        StandingWater = 60, // EAU STAGNANTE
        DrySnow = 61, // NEIGE SECHE
        WetSnow = 62, // NEIGE MOUILLEE
        Slush = 63, // NEIGE FONDANTE
        Unused64 = 64,
        DrySnowOnTopOfIce = 65,
        WetSnowOnTopOfIce = 66,
        SlushOnTopOfIce = 67,
        DrySnowOnTopOfCompactedSnow = 68,
        Unused69 = 69,
        Unused70 = 70,
        WaterOnTopOfCompactedSnow = 71,
        WetSnowOnTopOfCompactedSnow = 72
    }
}
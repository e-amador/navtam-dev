"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClearingOperationsComponent = void 0;
var core_1 = require("@angular/core");
var angular_l10n_1 = require("angular-l10n");
var forms_1 = require("@angular/forms");
var rsc_service_1 = require("../rsc.service");
var toastr_service_1 = require("../../../../common/toastr.service");
var ClearingOperationsComponent = /** @class */ (function () {
    function ClearingOperationsComponent(fb, translation, rscService, toastr) {
        this.fb = fb;
        this.translation = translation;
        this.rscService = rscService;
        this.toastr = toastr;
        this.mainForm = null;
        this.isReadOnly = false;
        this.model = null;
    }
    ClearingOperationsComponent.prototype.ngOnInit = function () {
        this.isFormLoadingData = false;
        this.configureReactiveForm();
    };
    ClearingOperationsComponent.prototype.ngOnChanges = function () {
    };
    ClearingOperationsComponent.prototype.ngOnDestroy = function () {
    };
    ClearingOperationsComponent.prototype.clearingUnderwayClick = function () {
        if (this.isClearingUnderway) {
            this.isClearingUnderway = false;
        }
        else {
            this.isClearingUnderway = true;
        }
    };
    ClearingOperationsComponent.prototype.setClearingCompletedBy = function () {
    };
    ClearingOperationsComponent.prototype.setClearingStartBy = function () {
    };
    ClearingOperationsComponent.prototype.configureReactiveForm = function () {
        var frmArrControls = this.mainForm.controls['frmCleaningOperationsArr'];
        this.frmGroup = this.fb.group({
            startAt: [{ value: this.model.startAt, disabled: this.isReadOnly }, []],
            completedBy: [{ value: this.model.completedBy, disabled: this.isReadOnly }, []],
            remarks: [{ value: this.model.remarks.english, disabled: this.isReadOnly }, []],
            clearingUnderway: [{ value: this.model.clearingUnderway, disabled: this.isReadOnly }, []],
        });
        frmArrControls.push(this.frmGroup);
    };
    ClearingOperationsComponent.prototype.$ctrl = function (name) {
        return this.frmGroup.get(name);
    };
    ClearingOperationsComponent.prototype.validateKeyPressHours = function (evt, ctrlName) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault)
                    evt.preventDefault();
                return;
            }
            var num = +key;
            var pos = evt.currentTarget.selectionStart;
            if (pos === 0) { //val.length
                //First number
                if (key !== '0' && key !== '1' && key !== '2') {
                    evt.returnValue = false;
                    if (evt.preventDefault)
                        evt.preventDefault();
                    return;
                }
            }
            else if (pos === 1) { //val.length === 1
                //Second number
                var fc = this.$ctrl(ctrlName);
                var val = fc.value;
                if (val[0] === '2' && num > 3) {
                    evt.returnValue = false;
                    if (evt.preventDefault)
                        evt.preventDefault();
                    return;
                }
            }
            else if (pos === 2) { //val.length === 2
                //Third number
                if (num > 5) {
                    evt.returnValue = false;
                    if (evt.preventDefault)
                        evt.preventDefault();
                    return;
                }
            }
        }
    };
    //Export functions
    ClearingOperationsComponent.prototype.isFormValid = function () {
        return true;
    };
    ClearingOperationsComponent.prototype.exportComponentInternalData = function () {
        var data = {
            parent: this.model.parent,
            clearingUnderway: this.model.clearingUnderway,
            startAt: this.model.startAt,
            completedBy: this.model.completedBy,
            remarks: this.model.remarks,
        };
        return data;
    };
    ClearingOperationsComponent.prototype.clearValues = function () {
        alert('Clear component values: Cleaning Operations');
    };
    __decorate([
        core_1.Input('form'),
        __metadata("design:type", forms_1.FormGroup)
    ], ClearingOperationsComponent.prototype, "mainForm", void 0);
    __decorate([
        core_1.Input('isReadOnly'),
        __metadata("design:type", Boolean)
    ], ClearingOperationsComponent.prototype, "isReadOnly", void 0);
    __decorate([
        core_1.Input('model'),
        __metadata("design:type", Object)
    ], ClearingOperationsComponent.prototype, "model", void 0);
    ClearingOperationsComponent = __decorate([
        core_1.Component({
            selector: 'rsc-clearing-operations',
            templateUrl: './app/dashboard/nsds/rsc/components/rsc-clearingoperations.component.html'
        }),
        __param(3, core_1.Inject(toastr_service_1.TOASTR_TOKEN)),
        __metadata("design:paramtypes", [forms_1.FormBuilder,
            angular_l10n_1.TranslationService,
            rsc_service_1.RscService, Object])
    ], ClearingOperationsComponent);
    return ClearingOperationsComponent;
}());
exports.ClearingOperationsComponent = ClearingOperationsComponent;
function isANumber(str) {
    return !/\D/.test(str);
}
//# sourceMappingURL=rsc-clearingoperations.component.js.map
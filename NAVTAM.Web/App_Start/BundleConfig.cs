﻿using System.Web.Optimization;

namespace NAVTAM
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/css/plugins").Include(
                      "~/content/css/bootstrap.min.css",
                      "~/content/css/plugins/jquery-ui.min.css",
                      "~/node_modules/toastr/build/toastr.min.css",
                      "~/scripts/plugins/font-awesome-animation.min.css",
                      "~/scripts/plugins/bootstrap-datepicker3.min.css",
                       "~/scripts/plugins/fullcalendar/dist/fullcalendar.min.css"
                      ).Include("~/Content/css/font-awesome.min.css", new CssRewriteUrlTransform()
                      ).Include("~/Content/css/simple-line-icons.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/bundles/css/layout").Include(
                      "~/content/css/Lato-300-400-700-900.css",
                      "~/content/css/Open+Sans-400-300-600-700.css",
                      "~/content/css/app/style.min.css",
                      "~/content/css/app/theme.min.css",
                      "~/content/css/app/ui.min.css",
                      "~/content/css/app/layout.css",
                      "~/content/css/app/custom.css"));

            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                        "~/scripts/plugins/modernizr-2.6.2-respond-1.1.0.min.js",
                        "~/node_modules/core-js/client/shim.min.js",
                        "~/node_modules/zone.js/dist/zone.min.js",
                        "~/node_modules/reflect-metadata/reflect.js",
                        "~/node_modules/systemjs/dist/system.src.js",
                        "~/scripts/plugins/jquery-1.11.1.min.js",
                        "~/scripts/plugins/jquery.mask.min.js",
                        "~/scripts/plugins/moment.min.js",
                        "~/scripts/plugins/fullcalendar/dist/fullcalendar.min.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/scripts/addons").Include(
                        "~/scripts/plugins/jquery-migrate-1.2.1.min.js",
                        "~/scripts/plugins/jquery-ui-1.11.2.min.js",
                        "~/node_modules/toastr/build/toastr.min.js",
                        "~/scripts/plugins/bootstrap.min.js",
                        "~/scripts/app/template.js",
                        "~/node_modules/select2/dist/js/select2.min.js"));


            BundleTable.EnableOptimizations = true;
        }
    }
}

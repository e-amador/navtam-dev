﻿define(["jquery", "Resources", "kendoExt"], function ($, res, model) {

    var self = null;
    var vm = null;
    var kendoValidator = null;
    var labels = {};

    return {
        init: function (lbls) {
            self = this;
            labels = lbls;

            rolesList = $("#rolesList").data("model");
            orgsList = $("#orgsList").data("model");
            ADMIN_ACTION = "START_UP";

            self.initUI();
        },
        initUI: function () {

            var ShowDetails_UserTemplate = kendo.template($("#ShowDetails_UserTemplate").html());
            var ShowResetPassword_Template = kendo.template($("#ResetPassword_Template").html());
            var Update_UserTemplate = kendo.template($("#Update_UserTemplate").html());
            var UserDetailwindow = $("#User-Popup").kendoWindow({
                actions: ["Close"],
                pinned: true,
                width: 520,
                length: 900,
                visible: false,
            }).data("kendoWindow");
            var grid = $("#userGrid").kendoGrid({
                change: onChange,
                sortable: true,
                scrollable: true,
                resizable: { columns: true },
                selectable: true,
                filterable: true,
                groupable: true,
                reorderable: true,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [
                    { field: "UserName", title: labels.userGrid_fieldUserName, width: "~100" },
                    {
                        field: "UserRoles",
                        title: labels.userGrid_fieldRoles,
                        template: function(data)
                        {
                            var res = [];
                            if (data && data.UserRoles)
                            {
                                $.each(data.UserRoles, function(idx, elem)
                                {
                                    res.push($.grep(rolesList, function(e)
                                    {
                                        return e.value == elem;
                                    })[0].text);
                                });
                            }
                            return res.join(", ");
                        },
                        editor: function(container, options)
                        {
                            $("<select multiple='multiple' data-bind='value : UserRoles'/>")
                                .appendTo(container)
                                .kendoMultiSelect({
                                    dataTextField: "text",
                                    dataValueField: "value",
                                    dataSource: rolesList,
                                    valuePrimitive: true,
                                });
                        },
                        width: "~100"
                    },
                    {
                        field: "UserOrgId",
                        title: labels.userGrid_fieldOrganization,
                        template: function(data)
                        {
                            if (data && data.UserOrgId)
                            {
                                return $.grep(orgsList, function(e)
                                {
                                    return e.value === data.UserOrgId;
                                })[0].text;
                            }
                            return "";
                        },
                        editor: function(container, options)
                        {
                            $("<input data-bind='value : UserOrgId'/>")
                                .appendTo(container)
                                .kendoComboBox({
                                    dataTextField: "text",
                                    dataValueField: "value",
                                    dataSource: orgsList,
                                    valuePrimitive: true,
                                });
                        },
                        width: "~100"
                    },
                    { field: "FirstName", title: labels.userGrid_fieldFirstName, width: "~100" },
                    { field: "LastName", title: labels.userGrid_fieldLastName, width: "~100" }
                ],
                toolbar: [
                    { name: "create", text: labels.userGrid_toolbarCreate },
                    { name: "showDetails", text: labels.userGrid_toolbarShow },
                    { name: "update", text: labels.userGrid_toolbarUpdate },
                    { name: "destroy", text: labels.userGrid_toolbarDelete },
                    { name: "resetPassword", text: labels.userGrid_toolbarResetPassword }
                ],
                editable: {
                    mode: "popup",
                    template: Update_UserTemplate,
                    window: { title: labels.userGrid_optionAddUser },
                    destroy: false
                },
                edit: function(e)
                {
                    if (e.model.isNew())
                    {
                        e.container.find("tr[name=PasswordTr]").show();
                        e.container.find("tr[name=ConfirmPasswordTr]").show();
                    }
                    else
                    {
                        e.container.find("tr[name=PasswordTr]").hide();
                        e.container.find("tr[name=ConfirmPasswordTr]").hide();
                    }

                    RolesSelector();
                    OrgSelector();
                    setValidators("#register-form");
                    wireControllers();

                    e.model.isCreateMode = this.isCreateMode;

                    $(".k-edit-form-container").width(520);
                    //e.container.find(":checkbox").removeAttr("disabled");
                },
                dataBound: function(e) {
                    $("#userGrid").find(".k-grid-delete").hide();
                    $("#userGrid").find(".k-grid-update").hide();
                    $("#userGrid").find(".k-grid-showDetails").hide();
                    $("#userGrid").find(".k-grid-resetPassword").hide();
                },
                dataSource: {
                    sync: function()
                    {
                        //debugger;
                        //this.dataSource.sync();
                    },
                    requestEnd: function(e)
                    {
                        if (e.type === "create" || e.type === "update")
                        {
                            $("#userGrid").data("kendoGrid").dataSource.read();
                        }
                    },
                    transport: {
                        read: { url: "/UserManagement/ReadUsers", dataType: "json", type: "POST" },
                        update: { url: "/UserManagement/UpdateUser", dataType: "json", type: "POST" },
                        destroy: { url: "/UserManagement/DeleteUser", dataType: "json", type: "POST" },
                        create: { url: "/UserManagement/CreateUser", dataType: "json", type: "POST" }
                        /*
                        create: function (options) {
                            $.ajax( {
                                url: "/UserManagement/CreateUser",
                                dataType: "json",
                                type: "POST",
                                // send the updated data items as the "models" service parameter encoded in JSON
                                data: {
                                    models: kendo.stringify(options.data.models)
                                },
                                success: function(result) {
                                    debugger;
                                    options.success(result);
                                },
                                error: function(result) {
                                    debugger;
                                    options.error(result);
                                }
                            });                            
                        }
*/
                    },
                    error: function(response)
                    {
                        if (response.xhr) {
                            $('#error-panel li').remove();
                            for (var iter = 0; iter < response.xhr.responseJSON.length; iter++) {
                                $('#error-panel ul').append('<li>' + response.xhr.responseJSON[iter] + '</li>');
                            }
                            $("#error-panel").show();
                        }
                    },
                    pageSize: 10,
                    schema: {
                        errors: "error",
                        data: "Data",
                        model: {
                            id: "UserId"
                        },
                    }
                }
            }).data("kendoGrid");

            $("#userGrid .k-grid-toolbar .k-grid-update").on("click", function (e)
            {
                var grid = $("#userGrid").data("kendoGrid");
                var dataItem = grid.dataItem(grid.select());
                dataItem.isCreateMode = false;

                this.isCreateMode = false;

                grid.editRow(grid.select());
            });

            $("#userGrid .k-grid-toolbar .k-grid-showDetails").on("click", function (e)
            {
                showDetails(e);
            });

            $("#userGrid .k-grid-toolbar .k-grid-resetPassword").on("click", function (e) {
                showResetPassword(e);
            });

            function showRoles(data)
            {
                var res = [];
                 if (data && data.UserRoles)
                {
                    $.each(data.UserRoles, function (idx, elem)
                    {
                        res.push($.grep(rolesList, function (e) { return e.value == elem; })[0].text);
                    });
                }
                return res.join(", ");
            };
            function RolesSelector()
            {
                $("#Update_UserRolesMultiSelect")
                            .kendoMultiSelect({
                                dataTextField: "text",
                                dataValueField: "value",
                                dataSource: rolesList,
                                valuePrimitive: true
                            });
            }
            function OrgSelector()
            {
                $("#Update_UserOrgContainer")
                            .kendoComboBox({
                                dataTextField: "text",
                                dataValueField: "value",
                                dataSource: orgsList,
                                valuePrimitive: true,
                            });
            }
            function setValidators(id) {
                 kendoValidator = $(id).kendoValidator({
                    messages: {
                        matches: function (input) {
                            return labels.userGrid_valPasswordMatch;
                        },
                        email: labels.userGrid_valEmailValid,
                        passwordRule: labels.userGrid_valPasswordRule,
                        passwordDifferentRule: labels.userGrid_valPasswordDifferentRule,
                        validmask: function(input) {
                            if (input.is("[name=update-telephone]")) {
                                return labels.userGrid_valPhoneIncomplete;
                            }
                            if (input.is("[name=update-fax]")) {
                                return labels.userGrid_valFaxIncomplete;
                            }
                        },
                        required: function (input) {
                            if (input.is("[name=update_username]")) {
                                return labels.userGrid_valUsernameRequired;
                            }
                            if (input.is("[name=update-email]")) {
                                return labels.userGrid_valEmailRequired;
                            }
                            if (input.is("[name=update-password]")) {
                                return labels.userGrid_valPasswordRequired;
                            }
                            if (input.is("[name=update-confirmPassword]")) {
                                return labels.userGrid_valConfirmRequired;
                            }
                            if (input.is("[name=update-lastname]")) {
                                return labels.userGrid_valLastNameRequired;
                            }
                            if (input.is("[name=update-firstname]")) {
                                return labels.userGrid_valFirstNameRequired;
                            }
                            if (input.is("[name=update-position]")) {
                                return labels.userGrid_valPositionRequired;
                            }
                            if (input.is("[name=update-address]")) {
                                return labels.userGrid_valAddressRequired;
                            }
                            return "";
                        },
                        available: function (input) {
                            var cache = availability.cache[input.attr('id')];
                            if (cache.checking) {
                                return labels.userGrid_valAvailableMsg;
                            }
                            else {
                                return labels.userGrid_valAvailableError;
                            }
                        }
                    },
                    rules: {
                        available: function(input) {
                            var validate = input.data('available');
                            if (typeof validate !== 'undefined' && validate !== false) {
                                var id = input.attr('id');

                                // new up a new cache object for this field if one doesn't already eist
                                var cache = availability.cache[id] = availability.cache[id] || {};
                                cache.checking = true;
                                // pull the url and message off of the proper data attributes
                                var settings = {
                                    url: '/UserManagement/IsUserRegistered',
                                    message: 'sdfasasd'
                                };
                                if (cache.value === input.val() && cache.valid) {
                                    return true;
                                }
                                if (cache.value === input.val() && !cache.valid) {
                                    cache.checking = false;
                                    return false;
                                }
                                availability.check(input, settings);
                                return false;
                            }
                            return true;
                        },
                        passwordRule: function (input) {
                            if (input.is("[name=update-password]")) {
                                var validated = true;

                                var value = input.val();  // contains at least 8 chars
                                if (value.length < 8)
                                    validated = false;

                                if (!/\d/.test(value))  // contains at least one digit
                                    validated = false;

                                if (!/[a-z]/.test(value)) // contains at least one lower case
                                    validated = false;

                                if (!/[A-Z]/.test(value)) // contains at least one upper case
                                    validated = false;

                                if (!/[\]\[?\/<>~#`!@$%^&*()+={}|_:";',{ ]/.test(value)) // contains at least one special character
                                    validated = false;

                                return validated;
                            }

                            return true;
                        },
                        passwordDifferentRule: function (input) {
                            if (input.is("[name=update-password]")) {
                                $.ajax({
                                    url: "/Admin/UserManagement/IsSamePassword",
                                    data: {
                                        username: $("#username").val(),
                                        password: input.val()
                                    },
                                    async: false,
                                    success:
                                        function (result) {
                                            return result;
                                        }
                                });
                            }
                            return true;
                        },
                        matches: function (input) {
                            if (input.is("[name=update-confirmPassword]")) {
                                var confirmPassword = input.val();
                                var match = $("#update-password").val();
                                return confirmPassword === match;
                            }
                            return true;
                        },
                        validmask: function (input) {
                            if (input.is("[name=update-telephone]")) {
                                if (input.val() !== '') {
                                    var data = input.data("kendoMaskedTextBox");
                                    return data.value().indexOf(data.options.promptChar) === -1;
                                }
                            }
                            if (input.is("[name=update-fax]")) {
                                if (input.val() !== '') {
                                    var data = input.data("kendoMaskedTextBox");
                                    return data.value().indexOf(data.options.promptChar) === -1;
                                }
                            }
                            return true;
                        }
                    }
                 }).data("kendoValidator");;
            }
            var availability = {
                cache: {},
                check: function (element, settings) {
                    var id = element.attr('id');
                    var cache = this.cache[id] = this.cache[id] || {};
                    $.ajax({
                        url: settings.url,
                        dataType: 'json',
                        data: { userName: element.val() },

                        success: function (data) {
                            cache.valid = data === false;
                            kendoValidator.validateInput(element);
/*
                            if (data === true) 
                            {
                                cache.valid = data;
                                kendoValidator.validateInput(element);
                            }
                            else {
                                cache.valid = true;
                                kendoValidator.validateInput(element);
                            }
*/
                        },
                        failure: function () {
                            cache.valid = true;
                            kendoValidator.validateInput(element);
                        },
                        complete: function () {
                            cache.value = element.val();
                        }
                    });
                }
            };
            function wireControllers()
            {
                $("#update-telephone").kendoMaskedTextBox({
                    mask: "(000) 000-0000"
                });                
                $("#update-fax").kendoMaskedTextBox({
                    mask: "(000) 000-0000"
                });                
            }
            function showOrganizationName(data)
            {
                if (data && data.UserOrgId)
                {
                    return $.grep(orgsList, function (e) { return e.value === data.UserOrgId; })[0].text;
                }
                return "";
            };
            function showDetails(e)
            {
                UserDetailwindow.content(ShowDetails_UserTemplate);
                var grid = $("#userGrid").data("kendoGrid");
                var dataItem = grid.dataItem(grid.select());
                kendo.bind($('#ShowDetails_DetailContent'), dataItem);

                $("#User-Popup").data("kendoWindow").center();
                $("#User-Popup").data("kendoWindow").open();
                //$(".k-textbox").attr('readonly', true);
                var roles = showRoles(dataItem);
                var OrganizationName = showOrganizationName(dataItem);
                $('[name="ShowDetails_UserRolesDisplay"]').val(roles);
                $('[name="ShowDetails_UserOrgDisplay"]').val(OrganizationName);
            };

            function showResetPassword(e) {
                UserDetailwindow.content(ShowResetPassword_Template);
                var grid = $("#userGrid").data("kendoGrid");
                var dataItem = grid.dataItem(grid.select());

                setValidators("#resetPassword-form");

                dataItem.closeResetPasswordWindow = function() {
                    UserDetailwindow.close();
                }

                dataItem.resetPasswordWindow = function () {
                    var data = {}; 
                    data.value = { userId: dataItem.UserId, password: dataItem.Password, confirmPassword: dataItem.ConfirmPassword };

                    $.ajax({
                        type: "POST",
                        url: '/UserManagement/ResetPassword',
                        dataType: "json",
                        data: { model: data.value },
                        success: function() {
                            UserDetailwindow.close();
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            //todo: later on
                        }                        
                    });
                }

                kendo.bind($('#ResetPassword_Content'), dataItem);

                $("#User-Popup").data("kendoWindow").center();
                $("#User-Popup").data("kendoWindow").open();
                //$(".k-textbox").attr('readonly', true);
                var roles = showRoles(dataItem);
                var OrganizationName = showOrganizationName(dataItem);
                $('[name="ShowDetails_UserRolesDisplay"]').val(roles);
                $('[name="ShowDetails_UserOrgDisplay"]').val(OrganizationName);

            };

            function onChange(arg)
            {
                var grid = $("#userGrid").data("kendoGrid");
                var dataItem = grid.dataItem(grid.select());
                $("#userGrid").find(".k-grid-showDetails").show();
                $("#userGrid").find(".k-grid-delete").hide();
                $("#userGrid").find(".k-grid-update").hide();
                if (dataItem.UserName != "Admin")
                {
                    $("#userGrid").find(".k-grid-resetPassword").show();
                    $("#userGrid").find(".k-grid-delete").show();
                    $("#userGrid").find(".k-grid-update").show();
                }
            };
            $("#userGrid .k-grid-toolbar .k-grid-delete").on("click", function (e)
            {
                e.preventDefault();
                var grid = $("#userGrid").data("kendoGrid");
                var dataItem = grid.dataItem(grid.select());

                kendo.ui.ExtOkCancelDialog.show({
                    title: labels.userGrid_deleteUserMessage,
                    message: labels.userGrid_deleteUserMessage + "?",
                    width: '400px',
                    height: '100px',
                    icon: "k-Ext-warning"
                }).done(function (response)
                {
                    if (response.button === 'OK')
                    {
                        if (dataItem.UserName != "Admin")
                        {
                            grid.dataSource.remove(dataItem);
                            grid.saveChanges();
                        }
                    }
                });
            });
        },

        passwordEditor: function (container, options) {
            $('<input data-text-field="' + options.field + '" ' +
                    'class="k-input k-textbox" ' +
                    'type="password" ' +
                    'data-value-field="' + options.field + '" ' +
                    'data-bind="value:' + options.field + '"/>')
                    .appendTo(container)
        },

    };
});

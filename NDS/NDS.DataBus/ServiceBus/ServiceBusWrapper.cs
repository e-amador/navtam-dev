﻿using System;
using System.Collections.Generic;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using NDS.DataBus.Contracts;

namespace NDS.DataBus.ServiceBus
{
    public class ServiceBusWrapper : IDataBus
    {
        public ServiceBusWrapper(string connectionString)
        {
            _connectionString = connectionString;
            _namespaceManager = NamespaceManager.CreateFromConnectionString(connectionString);
        }

        public INdsMessage CreateMessage(string content, IEnumerable<Property> properties)
        {
            return new BrokeredMessageWrapper(content, properties);
        }

        public IPostQueue CreateQueue(string name)
        {
            if (!_namespaceManager.QueueExists(name))
                _namespaceManager.CreateQueue(name);

            return new QueueClientWrapper(QueueClient.CreateFromConnectionString(_connectionString, name));
        }

        public bool DeleteQueue(string name)
        {
            if (!_namespaceManager.QueueExists(name))
                _namespaceManager.DeleteQueue(name);

            return true;
        }

        public IPostQueue TryPostToQueue(string name)
        {
            return _namespaceManager.QueueExists(name) ? new QueueClientWrapper(QueueClient.CreateFromConnectionString(_connectionString, name)) : null;
        }

        public IPostQueue CreateTopic(string name)
        {
            if (!_namespaceManager.TopicExists(name))
                _namespaceManager.CreateTopic(name);

            return new TopicClientWrapper(TopicClient.CreateFromConnectionString(_connectionString, name));
        }

        public IPostQueue TryPostToTopic(string name)
        {
            return _namespaceManager.TopicExists(name) ? new TopicClientWrapper(TopicClient.CreateFromConnectionString(_connectionString, name)) : null;
        }

        public ISubscription ReadFromQueue(string name, Action<INdsMessage> callback)
        {
            if (!_namespaceManager.QueueExists(name))
                return null;

            var client = QueueClient.CreateFromConnectionString(_connectionString, name);
            client.OnMessage(message => callback.Invoke(new BrokeredMessageWrapper(message)));
            return new QueueClientWrapper(client);
        }

        public ISubscription ReadFromTopic(string name, string subscriptionName, string filter, Action<INdsMessage> callback)
        {
            if (!_namespaceManager.TopicExists(name))
                return null;

            var description = CreateSubscriptionDescription(name, CreateSubscriptionName(subscriptionName), filter);
            var client = SubscriptionClient.CreateFromConnectionString(_connectionString, description.TopicPath, description.Name);
            client.OnMessage(message => callback.Invoke(new BrokeredMessageWrapper(message)));
            return new SubscriptionClientWrapper(_namespaceManager, client);
        }

        private SubscriptionDescription CreateSubscriptionDescription(string topicPath, string name, string filter)
        {
            if (_namespaceManager.SubscriptionExists(topicPath, name))
            {
                return _namespaceManager.GetSubscription(topicPath, name);
            }
            else
            {
                return string.IsNullOrEmpty(filter)
                    ? _namespaceManager.CreateSubscription(topicPath, name)
                    : _namespaceManager.CreateSubscription(topicPath, name, new SqlFilter(filter));
            }
        }

        private string CreateSubscriptionName(string suggested)
        {
            return string.IsNullOrEmpty(suggested) ? Guid.NewGuid().ToString() : suggested;
        }

        readonly string _connectionString;
        readonly NamespaceManager _namespaceManager;
        
    }
}

import { Component, OnInit, OnDestroy, Inject, Injector, ChangeDetectorRef } from '@angular/core'
import { FormControl, FormGroup, FormBuilder, FormArray, AbstractControl, Validators } from '@angular/forms'

import { LocationService } from '../../shared/location.service';
import { ISdoSubject, ISelectOptions } from '../../shared/data.model';
import { DataService } from '../../shared/data.service';
import { RwyClsdService, IClosureType, IRwy } from './rwyclsd.service';

import { TOASTR_TOKEN, Toastr } from './../../../common/toastr.service';
import { TranslationService } from 'angular-l10n';

@Component({
    templateUrl: '/app/dashboard/nsds/rwyclsd/rwyclsd.component.html'
})

export class RwyClsdComponent implements OnInit, OnDestroy {
    action: string = "";
    token: any = null;
    nsdForm: FormGroup;
    nsdFrmGroup: FormGroup;
    isReadOnly: boolean = false;

    isBilingualRegion: boolean = false;

    closureReasons: ISelectOptions[] = [];
    rwySelections: any[] = [];

    subjects: any[];
    addClosureReason: boolean = false;
    otherReason: boolean = false;
    availableAsTaxiway: boolean = false;
    effectedAerodromeName: string = "";

    acftWeightRestriction: number = 99999999;
    acftWingRestriction: number = 99999999;

    isReplacementNotam: boolean = false;

    subjectSelected: ISdoSubject = null;
    runwayEffected: ISdoSubject = null;

    leaflet: any;

    disableIcaoSubjectSelect: boolean = true;
    disableIcaoConditionSelect: boolean = true;

    isFormLoadingData: boolean;

    constructor(
        @Inject(TOASTR_TOKEN) private toastr: Toastr,
        private injector: Injector,
        private fb: FormBuilder,
        private locationService: LocationService,
        private closureService: RwyClsdService,
        private changeDetectionRef: ChangeDetectorRef,
        private dataService: DataService,
        public translation: TranslationService) {

        this.token = this.injector.get('token');
        this.isReadOnly = this.injector.get('isReadOnly');
        this.nsdForm = this.injector.get('form');
        this.action = this.injector.get('type');

        this.isReplacementNotam = this.token.type === "replace" ? true : false;
    }
    ngOnInit() {
        this.isFormLoadingData = !(!this.token.subjectId);
        
        const app = this.closureService.app;
        this.leaflet = app.leafletmap;

        this.loadClosureTypes();

        let frmArr = <FormArray>this.nsdForm.controls['frmArr'];
        if (this.isFormLoadingData) {
            if (frmArr.length === 0) {
                this.configureReactiveForm();
                //This "dirty trick" is important to make the parent form not
                //to run the generateIcao until everything is loaded.
                this.$ctrl('refreshDummy').setValue(0);
                this.nsdForm.controls['frmArr'].setErrors({ 'required': true });
            } else {
                /*
               this is required because of the grouping funtionallity. When the NsdFormGroup 
               was configured and any of items in the group has changed (new set of tokens values)
               the NsdForm Group needs to be updated with the new tokens values */
               this.updateReactiveForm(frmArr);
            }

            this.setAerodromeFromToken();
        } else {
            this.configureReactiveForm();
        }

        this.showDoaMapRegion(app.doaId);
    }

    ngOnDestroy() {
    }

    private checkIfReviewDisabled() {
        let review = !(!this.token.subjectId);
        return review;
    }
    
    private setAerodromeFromToken() {
        this.setDummyError(0);
        this.dataService.getSdoSubject(this.token.effectedAerodrome)
            .subscribe((sdoSubjectResult: ISdoSubject) => {
                this.subjectSelected = sdoSubjectResult;
                this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);

                this.getEffectedRunway(sdoSubjectResult.id);

                this.subjects = [];
                this.subjects.push(sdoSubjectResult);
                this.loadMapLocations(this.closureService.app.doaId, true);
                this.updateMap(sdoSubjectResult, true);

                this.setDummyError(1);
            });
    }

    private loadMapLocations(doaId: number, loadingFromProposal: boolean) {
        const self = this;
        this.dataService.getDoaLocation(doaId)
            .subscribe((geoJsonStr: any) => {
                const interval = window.setInterval(function () {
                    if (self.leaflet && self.leaflet.isReady()) {
                        window.clearInterval(interval);
                        self.leaflet.addDoa(geoJsonStr, 'DOA');
                        if (loadingFromProposal) {
                            self.updateMap(self.subjectSelected, false);
                        }
                    };
                }, 100);
            }, (error: any) => {
                this.toastr.error(this.translation.translate('Nsd.DOAArea') + error.message, "", { 'positionClass': 'toast-bottom-right' });
            });
    }

    $ctrl(name: string): AbstractControl {
        return this.nsdFrmGroup.get(name);
    }

    IsBilingualRegion() {
        return this.isBilingualRegion;
    }

    ItemASelectOptions() {
        return {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.ItemASelect')
            }
        }
    }

    closureTypeSelectOptions() {
        return {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.SelectClosure')
            }
        }
    }

    runwaySelectOptions() {
        return {
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.RunwaySelect')
            }
        }
    }

    icaoSubjectSelectOptions = {
        width: "100%",
        placeholder: {
            id: '-1',
            text: this.translation.translate('Nsd.ItemASelect')
        }
    }

    getEffectedAerodrome(): any {
        const self = this;
        const app = this.closureService.app;
        
        return {
            language: {
                inputTooShort: function (args) {
                    return self.translation.translate('Schedule.InputTooShort');
                },
            },
            minimumInputLength: 2,
            width: "100%",
            placeholder: {
                id: '-1',
                text: this.translation.translate('Nsd.SdoSubjectPlaceholder')
            },
            ajax: {
                type: 'GET',
                url: app.apiUrl + "sdosubjects/rwyclosurefilter",
                dataType: 'json',
                delay: 500,
                headers: {
                    "RequestVerificationToken": app.antiForgeryTokenValue
                },
                data: function (params, page) {
                    return {
                        Query: params.term,
                        Size: 200,
                        doaId: app.doaId
                    }
                },
                processResults: function (data) {
                    return {
                        results: data
                    }
                },
                cache: true
            },
            templateResult: function (data: any) {
                if (data.loading) {
                    return data.text;
                }
                return self.dataService.getSdoExtendedName(data);
            },
            templateSelection: function (selection: ISdoSubject) {
                if (selection && selection.id === '-1') {
                    return selection.text;
                }

                self.updateSubjectSelected(selection);
                self.effectedAerodromeName = self.dataService.getSdoExtendedName(selection);
                return self.effectedAerodromeName
            },
        }
    }

    private updateSubjectSelected(sdoSubject: ISdoSubject) {
        const subjectId = this.$ctrl('subjectId').value;
        if (subjectId === sdoSubject.id) {
            return;
        }

        if (sdoSubject.id !== subjectId) {
            const subjectCtrl = this.$ctrl('subjectId');
            subjectCtrl.setValue(sdoSubject.id);
            subjectCtrl.markAsDirty();
        }

        this.subjectSelected = sdoSubject;
        this.dataService.getSdoSubject(sdoSubject.id)
            .subscribe((sdoSubjectResult: ISdoSubject) => {
                this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);

                if (!this.isFormLoadingData) {

                    this.setEffectedAerodromeName(this.effectedAerodromeName);

                    this.$ctrl('effectedRunway').setValue("");
                    this.$ctrl('effectedRunwayName').setValue("");
                    this.token.effectedRunwayName = "";
                    this.token.effectedRunway = "";
                }

                this.updateMap(sdoSubjectResult, true);
            });
    }

    setEffectedAerodrome($event) {
        const effectedAerodromeCtrl = this.$ctrl('effectedAerodrome');
        const subjectId = this.$ctrl('subjectId');
        const newValue = $event.value;

        if (newValue !== '-1' && effectedAerodromeCtrl.value.value != newValue) {
            effectedAerodromeCtrl.setValue(newValue);
            subjectId.patchValue(newValue);
            effectedAerodromeCtrl.markAsDirty();
            this.getEffectedRunway(newValue);
        }
    }

    checkForDisableItemAIfReplacement() {
        if (this.isReadOnly) {
            return this.isReadOnly //same condiitons
        }

        return this.isReplacementNotam;
    }

    private checkIfBilingualRegion(subjectId: string) {
        this.dataService.getSdoSubject(subjectId)
            .subscribe((sdoSubjectResult: ISdoSubject) => {
                this.setBilingualRegion(sdoSubjectResult.isBilingualRegion);
            });
    }

    private setBilingualRegion(value: boolean) {
        this.token.isBilingual = value;
        this.isBilingualRegion = value;
        if (this.isBilingualRegion) {
            if (!this.isReadOnly) {
            }
        } else {
            if (!this.isReadOnly) {
            }
        }
    }

    setEffectedAerodromeName(name: string) {
        const effectedAerodromeNameCtrl = this.$ctrl('effectedAerodromeName');
        effectedAerodromeNameCtrl.patchValue(name);
    }

    invalidEffectedAerodrome(): boolean {
        const ctrl = this.$ctrl('effectedAerodrome');
        return !this.isReadOnly && this.$ctrl('effectedAerodrome').invalid;
    }
    
    validateRunway(): boolean {
        let rwyName = this.$ctrl('effectedRunwayName');
        if (rwyName.value === "") {
            let error = { RunwayNeeded: { valid: false } }
            this.nsdFrmGroup.setErrors(error);

            return true;
        }
        return false;
    }

    getEffectedRunway(subjectAerodrome: string) {
        if (subjectAerodrome) {
            this.closureService
                .getEffectedRunway(subjectAerodrome)
                .subscribe((rwySelections: IRwy[]) => {
                    let runways = rwySelections.map((item: IRwy) => {
                        return {
                            id: item.id,
                            text: item.designator
                        }
                    });
                    this.rwySelections = null;
                    this.makeFirstElement(runways, (rwy) => rwy.text === this.token.effectedRunwayName, !this.isFormLoadingData);
                    this.rwySelections = runways;
                    this.isFormLoadingData = false;
                });
        }
    }

    setEffectedRunway($event) {
        if ($event.value && $event.value !== '-1') {
            const effectedRwyCtrl = this.$ctrl('effectedRunway');
            const newValue = $event.value;

            if (newValue !== "") {
                const effectedRwyNameCtrl = this.$ctrl('effectedRunwayName')
                effectedRwyNameCtrl.setValue(this.rwySelections.find(x => x.id === newValue).text);
                effectedRwyCtrl.setValue(newValue);
                effectedRwyCtrl.markAsDirty();
                this.closureService.convertGeo($event.value)
                    .subscribe((sdoSubjectResult: ISdoSubject) => {
                        this.updateMap(sdoSubjectResult, true);
                    });
            }
        }

        if (!this.isFormLoadingData) {
            this.setDummyError(1);
        }
    }

    loadClosureTypes() {
        this.closureService
            .getClosureReasons()
            .subscribe((closureReasons: IClosureType[]) => {
                let closures = closureReasons.map((item: IClosureType) => {
                    return {
                        id: item.id,
                        text: item.name
                    }
                });

                this.makeFirstElement(closures, (clsr) => clsr.id === this.token.closureReasonId, true);
                this.closureReasons = closures;
            });
    }

    setClosureType($event) {
        const closureTypeCtrl = this.$ctrl('closureReasonId');
        const newValue = $event.value;
        if (newValue !== "-1") {
            if (newValue !== closureTypeCtrl.value) {
                closureTypeCtrl.setValue(newValue);
                closureTypeCtrl.markAsDirty();
            }
            if (!this.isFormLoadingData) {
                this.$ctrl('refreshDummy').setValue(0);
                this.validateOtherReason();
            }
        }
    }

    getOtherReason() {
        const closureQuestionValue = this.$ctrl('closureQuestion');
        const closureTypeCtrl = this.$ctrl('closureReasonId');
        if (closureQuestionValue.value && closureTypeCtrl.value === "Re9") {
            return true;
        }
        return false;
    }    

    private setDummyError(val: number) {
        if (val === 1 && this.$ctrl('refreshDummy').value !== 1) {
            this.$ctrl('refreshDummy').setValue(1);
            this.nsdForm.controls['frmArr'].setErrors(null);
        }
        else if (val === 0 && this.$ctrl('refreshDummy').value !== 0) {
            this.$ctrl('refreshDummy').setValue(0);
            this.nsdForm.controls['frmArr'].setErrors({ 'required': true });
        }
    }

    changeReasonFree() {
        if (this.isFormLoadingData) return;
        this.setDummyError(0);
        const closureFreeEn = this.$ctrl('closureReasonOtherFreeText');
        const closureFreeFr = this.$ctrl('closureReasonOtherFreeTextFr');
        if (this.isBilingualRegion) {
            if (closureFreeEn.value.length > 0 && closureFreeFr.value.length > 0) {
                this.setDummyError(1);
                return;
            }
        } else if (closureFreeEn.value.length > 0) {
            this.setDummyError(1);
            return;
        }
    }

    validateAerodromeRequired() {
        let ad = this.$ctrl('effectedAerodrome').value;
        if (ad.value === "") {
            let error = { aerodromeRequired: { valid: false } }
            this.nsdFrmGroup.setErrors(error);

            return false;
        }

        return true;
    }

    validateOtherReason() {
        if (this.getOtherReason()) {
            const closureFreeEn = <string>this.$ctrl('closureReasonOtherFreeText').value;
            const closureFreeFr = <string>this.$ctrl('closureReasonOtherFreeTextFr').value;
            let error = { closureReasonNeeded: { valid: false } }

            if (this.isBilingualRegion) {
                if (closureFreeEn.length > 0 && closureFreeFr.length > 0) {
                    this.setDummyError(1);
                    return false;
                }
            } else if (closureFreeEn.length > 0) {
                this.setDummyError(1);
                return false;
            }

            this.setDummyError(0);
            return true;
        } else {
            const closureTypeCtrl = this.$ctrl('closureReasonId');
            if (closureTypeCtrl.value !== "Re9") {
                this.cleanFreeTextClosureReason();
            }
        }
        if (this.$ctrl('refreshDummy').value === 0) {
            this.$ctrl('refreshDummy').setValue(1);
            this.nsdFrmGroup.setErrors(null);
        }
        return false;
    }
    private cleanFreeTextClosureReason() {
        this.updateFormControlValue("closureReasonOtherFreeText", "");
        this.updateFormControlValue("closureReasonOtherFreeTextFr", "");
    }

    validateClosures() {
        if (!this.getOtherReason()) {
            const closureId = <number>this.$ctrl('closureReasonId').value;
            const closureQuestion = this.$ctrl('closureQuestion').value;
            let error = { closureReasonNeeded: { valid: false } }

            if (closureQuestion) {
                if (closureId === -1 || closureId.toString() === "-1") {
                    this.nsdFrmGroup.setErrors(error);
                    return true;
                }
            }
        }
        return false;
    }

    validateTwyRestrictionsFreeText() {
        if (this.CheckIfTaxiwayRestrictions()) {
            if (this.isBilingualRegion) {
                if (this.$ctrl('freeTextTaxiwayRestriction').value === "" ||
                    this.$ctrl('freeTextTaxiwayRestrictionFr').value === "") {
                    let error = { TaxiwayRestrictionRequired: { valid: false } }
                    this.nsdFrmGroup.setErrors(error);
                    return true;
                }
            }

            if (this.$ctrl('freeTextTaxiwayRestriction').value === "") {
                let error = { TaxiwayRestrictionRequired: { valid: false } }
                this.nsdFrmGroup.setErrors(error);
                return true;
            }
        }

        return false;
    }

    validateAircraftRestrictionsValues() {
        if (this.CheckIfAircraftRestrictions) {
            const wingCtrlValue = this.$ctrl('maxWingSpanFt').value;
            const weightCtrlValue = this.$ctrl('maxWeightLbs').value;

            if (weightCtrlValue > 0 || wingCtrlValue > 0) {
                return false;
            } else {
                let error = { AircraftRestrictionRequired: { valid: false } }
                this.nsdFrmGroup.setErrors(error);
                return true;
            }
        }
        return false;
    }

    validateIsNaN(): boolean {
        let wing = <number> this.$ctrl('maxWingSpanFt').value;
        let weight = <number> this.$ctrl('maxWeightLbs').value;

        if (!isFinite(weight) || !isFinite(wing)) {
            let error = { validateIsNaN: { valid: false } }
            this.nsdFrmGroup.setErrors(error);
            return true;
        }
        return false;
    }

    validateBigInt(): boolean {
        let wing = <string> this.$ctrl('maxWingSpanFt').value;
        let weight = <string> this.$ctrl('maxWeightLbs').value;

        if (this.InvalidNumber(wing, weight)) {
            let error = { validateBigInt: { valid: false } }
            this.nsdFrmGroup.setErrors(error);
            return true;
        }
        return false;
    }

    private InvalidNumber(wing: string, weight: string): boolean {
        if ((wing !== null && wing.length > 20) || (weight !== null && weight.length > 20)) {
            return true
        }
        return false;
    }

    private CheckIfTaxiwayRestrictions() {
        const controlValue = this.$ctrl('txyRestrictions').value;
        return controlValue;
    }

    private CheckIfAircraftRestrictions() {
        const value = this.$ctrl('acftRestrictions').value;
        if (this.getAvailAsTxyway() && value)
            return true;

        return false;
    }

    public validateKeyPressNumber(evt: any) {
        if (evt instanceof KeyboardEvent) {
            if (evt.altKey || evt.ctrlKey) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
            var key = String.fromCharCode(evt.charCode);
            if (!isANumber(key)) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
                return;
            }
        }
    }

    private configureReactiveForm() {
        const frmArrControls = <FormArray>this.nsdForm.controls['frmArr'];

        this.nsdFrmGroup = this.fb.group({
            subjectLocation: this.token.subjectLocation,
            subjectId: this.token.effectedAerodrome,

            closureReasonId: [{ value: this.token.closureReasonId || -1, disabled: this.isReadOnly }],
            closureQuestion: [{ value: this.token.closureQuestion, disabled: this.isReadOnly }, [closureTypeValidator(this.nsdFrmGroup)]],

            effectedAerodrome: [{ value: this.token.effectedAerodrome || "" }, [effectedAerodromeValidator]],
            effectedAerodromeName: [{ value: this.token.effectedAerodromeName || "" }],

            effectedRunway: [{ value: this.token.effectedRunway, disabled: this.isReadOnly }],
            effectedRunwayName: [{ value: this.token.effectedRunwayName || "" }],

            availableAsTaxiway: [{ value: this.token.availableAsTaxiway, disabled: this.isReadOnly }],
            txyRestrictions: [{ value: this.token.txyRestrictions, disabled: this.isReadOnly }, [txyRestrictions]],
            freeTextTaxiwayRestriction: [{ value: this.token.freeTextTaxiwayRestriction || "", disabled: this.isReadOnly }],
            freeTextTaxiwayRestrictionFr: [{ value: this.token.freeTextTaxiwayRestrictionFr || "", disabled: this.isReadOnly }],

            otherReason: [{ value: this.token.otherReason }],
            closureReasonOtherFreeText: [{ value: this.token.closureReasonOtherFreeText || "", disabled: this.isReadOnly }],
            closureReasonOtherFreeTextFr: [{ value: this.token.closureReasonOtherFreeTextFr || "", disabled: this.isReadOnly }],

            acftRestrictions: [{ value: this.token.acftRestrictions, disabled: this.isReadOnly }],
            maxWingSpanFt: [{ value: this.token.maxWingSpanFt, disabled: this.isReadOnly }, [aircraftRestrictionValidator(this.nsdFrmGroup)]],
            maxWeightLbs: [{ value: this.token.maxWeightLbs, disabled: this.isReadOnly }, [aircraftRestrictionValidator(this.nsdFrmGroup)]],

            refreshDummy: [{ value: 0 }, [refreshDummyValidator]],
        });

        this.subscribeRadioButtons();

        frmArrControls.push(this.nsdFrmGroup);
    }

    private updateReactiveForm(formArr: FormArray) {
        this.nsdFrmGroup = <FormGroup>formArr.controls[0];

        this.updateFormControlValue("subjectId", this.token.subjectId);
        this.updateFormControlValue("subjectLocation", this.token.subjectLocation);
        this.updateFormControlValue("closureReasonId", this.token.closureReasonId);
        this.updateFormControlValue("closureQuestion", this.token.closureQuestion);
        this.updateFormControlValue("effectedAerodrome", this.token.effectedAerodrome);
        this.updateFormControlValue("effectedAerodromeName", this.token.effectedAerodromeName);
        this.updateFormControlValue("effectedRunway", this.token.effectedRunway);
        this.updateFormControlValue("effectedRunwayName", this.token.effectedRunwayName);
        this.updateFormControlValue("availableAsTaxiway", this.token.availableAsTaxiway);
        this.updateFormControlValue("txyRestrictions", this.token.txyRestrictions);
        this.updateFormControlValue("freeTextTaxiwayRestriction", this.token.freeTextTaxiwayRestriction);
        this.updateFormControlValue("freeTextTaxiwayRestrictionFr", this.token.freeTextTaxiwayRestrictionFr);
        this.updateFormControlValue("otherReason", this.token.otherReason);
        this.updateFormControlValue("closureReasonOtherFreeText", this.token.closureReasonOtherFreeText);
        this.updateFormControlValue("closureReasonOtherFreeTextFr", this.token.closureReasonOtherFreeTextFr);
        this.updateFormControlValue("acftRestrictions", this.token.acftRestrictions);
        this.updateFormControlValue("maxWingSpanFt", this.token.maxWingSpanFt);
        this.updateFormControlValue("maxWeightLbs", this.token.maxWeightLbs);

        this.subscribeRadioButtons();
    }

    private updateFormControlValue(controlName: string, value) {
        let control = this.nsdFrmGroup.get(controlName);
        if (control && control.value !== value) {
            control.setValue(value);
        }
    }

    private subscribeRadioButtons() {
        this.$ctrl('availableAsTaxiway').valueChanges.subscribe((value) => this.radioButtonChanged('availableAsTaxiway'));
        this.$ctrl('txyRestrictions').valueChanges.subscribe((value) => this.radioButtonChanged('txyRestrictions'));
        this.$ctrl('closureQuestion').valueChanges.subscribe((value) => this.radioButtonChanged('closureQuestion'));
        this.$ctrl('acftRestrictions').valueChanges.subscribe((value) => this.radioButtonChanged('acftRestrictions'));
    }

    private radioButtonChanged(name: string) {
        if (name === "closureQuestion") {
            this.resetClosureQuestionItemE(name);
        }
        if (name === "availableAsTaxiway") {
            this.resetTxyAvail();
        }
        if (name === "acftRestrictions") {
            this.resetAcftRestrictionsItemE(name);
        }       
    }

    private resetClosureQuestionItemE(name: string) {
        if (!this.$ctrl(name).value) {
            this.token.closureReasonId = -1;
            this.$ctrl('closureReasonId').patchValue(-1);
            if (!this.$ctrl('closureQuestion').value) {
                this.token.closureReason = null;
            }
            this.$ctrl('refreshDummy').setValue(1);
        } else {
            this.$ctrl('refreshDummy').setValue(0);
        }
    }

    private resetAcftRestrictionsItemE(name: string) {
        if (!this.$ctrl(name).value) {
            this.token.acftRestrictions = false;
            this.token.maxWingSpanFt = null;
            this.token.maxWeightLbs = null;
            this.updateFormControlValue("maxWingSpanFt", null);
            this.updateFormControlValue("maxWeightLbs", null);
        } else {
            this.token.acftRestrictions = true;
        }
    }

    private resetTxyAvail() {
        if (this.availableAsTaxiway) {
            this.token.availableAsTaxiway = true;
        } else {
            this.sanitizeTwyAvail();
        }
    }

    private sanitizeTwyAvail() {        
        this.$ctrl('freeTextTaxiwayRestriction').setValue("");
        this.$ctrl('freeTextTaxiwayRestrictionFr').setValue("");
        this.$ctrl('maxWingSpanFt').setValue("");
        this.$ctrl('maxWeightLbs').setValue("");
        //if not available as twy impossible to have restrictions -> sanitize control
        this.$ctrl('txyRestrictions').setValue(false);
        this.$ctrl('acftRestrictions').setValue(false);
    }
    
    private showDoaMapRegion(doaId: number) {
        this.dataService
            .getDoaLocation(doaId)
            .subscribe((geoJsonStr: any) => this.renderMap(geoJsonStr));
    }

    private renderMap(geoJsonStr: string) {
        const self = this;
        const interval = window.setInterval(function () {
            if (self.leaflet && self.leaflet.isReady()) {
                window.clearInterval(interval);
                self.leaflet.addDoa(geoJsonStr, 'DOA');
            }
        }, 100);

        
    }

    private updateMap(sdoSubject: ISdoSubject, acceptSubjectLocation: boolean) {
        if (sdoSubject.subjectGeoRefPoint) {
            let geojson = JSON.parse(sdoSubject.subjectGeoRefPoint);
            if (geojson.features.length === 1) {
                this.leaflet.clearFeaturesOnLayers(['DOA']);
                const subLocation = this.leaflet.geojsonToTextCordinates(geojson);
                const coordinates = geojson.features[0].geometry.coordinates;
                const markerGeojson = acceptSubjectLocation ? geojson : this.getGeoJsonFromLocation(this.token.subjectLocation);
                if (acceptSubjectLocation) {
                    this.nsdFrmGroup.patchValue({
                        subjectLocation: subLocation,
                        latitude: coordinates[1],
                        longitude: coordinates[0],
                    }, { onlySelf: true, emitEvent: false });
                }

                this.leaflet.addMarker(geojson, sdoSubject.designator);                  
            }
        }
    }

    private getGeoJsonFromLocation(value: string) {
        const location = this.locationService.parse(value);
        return location ? this.leaflet.latitudeLongitudeToGeoJson(location.latitude, location.longitude) : null;
    }

    private makeFirstElement(arr: any[], makeFirst: Function, insertUndefinedAtZero: boolean = true) {
        let len = arr.length;
        for (let i = 0; i < len; i++) {
            if (makeFirst(arr[i])) {
                this.swapArrayPositions(arr, 0, i);
                return;
            }
        }
        if (insertUndefinedAtZero) {
            arr.unshift({
                id: -1,
                text: '',
            });
        }
    }

    private swapArrayPositions(arr: any[], p1: number, p2: number) {
        const temp = arr[p1];
        arr[p1] = arr[p2];
        arr[p2] = temp;
    }

    private checkBothAircraftRestrictionsValidator(): boolean {
        const weight = this.$ctrl('acftWeightRestrictions').value;
        const wing = this.$ctrl('acftWingspanRestrictions').value;

        if (weight || wing) {
            return false;
        }

        return true;
    }

    private checkBothTxyRestrictionsValidator(): boolean {
        const eng = this.$ctrl('freeTextTaxiwayRestriction').value;
        const fr = this.$ctrl('freeTextTaxiwayRestrictionFr').value;

        if (this.IsBilingualRegion()) {
            if (eng.length == 0 || fr.length === 0) {
                return false
            }
        }

        if (eng.length === 0) {
            return false
        }

        return true;
    }

    getAvailAsTxyway() {
        return this.$ctrl('availableAsTaxiway').value;
    }

    private getTwyRestrictions() {
        return this.$ctrl('twyRestrictions').value;
    }

    setMaxWeight(resetCtrl: boolean) {
        const maxWeightCntrl = this.$ctrl('maxWeightLbs');
        this.token.maxWeightLbs = maxWeightCntrl.value;

        maxWeightCntrl.markAsDirty();
    }

    setMaxWingSpanFt() {
        const maxWingSpanFt = this.$ctrl('maxWingSpanFt');
        this.token.maxWingSpanFt = maxWingSpanFt.value;
        maxWingSpanFt.markAsDirty();
    }

    decWeight() {
        if (!this.isReadOnly) {
            const maxWeightCntrl = this.$ctrl("maxWeightLbs");
            if (+maxWeightCntrl.value > 0) {
                maxWeightCntrl.setValue(+maxWeightCntrl.value - 1);
                maxWeightCntrl.markAsDirty();
            }
        }
    }

    incWeight() {
        if (!this.isReadOnly) {
            const maxWeightCntrl = this.$ctrl("maxWeightLbs");
            if (+maxWeightCntrl.value < this.acftWeightRestriction) {
                maxWeightCntrl.setValue(+maxWeightCntrl.value + 1);
                maxWeightCntrl.markAsDirty();
            }
        }
    }

    decWingspan() {
        if (!this.isReadOnly) {
            const maxWingSpanFt = this.$ctrl("maxWingSpanFt");
            if (+maxWingSpanFt.value > 0) {
                maxWingSpanFt.setValue(+maxWingSpanFt.value - 1);
                maxWingSpanFt.markAsDirty();
            }
        }
    }

    incWingspan() {
        if (!this.isReadOnly) {
            const maxWingSpanFt = this.$ctrl("maxWingSpanFt");
            if (+maxWingSpanFt.value < this.acftWingRestriction) {
                maxWingSpanFt.setValue(+maxWingSpanFt.value + 1);
                maxWingSpanFt.markAsDirty();
            }
        }
    }

    public validateReasonCtrls(ctrlName: string) {
        if (this.$ctrl(ctrlName).value === '-1') {
            return true;
        }
     
        return false;
    }
}

function closureTypeValidator(ctrl: any): Function {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value === -1) {
            if (ctrl) {
                if (ctrl.closureQuestion) {
                    return { 'required': false }
                }
                return null;
            }
        }
        return null;
    }
}

function aircraftRestrictionValidator(ctrl: any): Function {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (ctrl) {
            if (ctrl.checkBothAircraftRestrictionsValidator()) {
                return null;
            } else {
                return { 'required': true }
            }
        }
        return null;
    }
}

function effectedAerodromeValidator(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value === -1) {
        return { 'required': true }
    }
    return null;
}

function txyRestrictions(ctrl: any): Function {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (typeof (c.value) === "string") {
            if (!ctrl.checkBothTxyRestrictionsValidator()) {
                return null
            } else {
                return { 'required': true }
            }
        }
        return null;
    }
}

function refreshDummyValidator(c: AbstractControl): { [key: string]: boolean } | null {
    if (c.value === 0) {
        return { 'required': true }
    }
    return null;
}

function isANumber(str: string): boolean {
    return !/\D/.test(str);
}

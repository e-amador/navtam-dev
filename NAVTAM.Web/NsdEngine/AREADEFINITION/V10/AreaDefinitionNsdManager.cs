﻿using System;
using System.Threading.Tasks;
using NavCanada.Core.Domain.Model.Enums;
using NAVTAM.ViewModels;

namespace NAVTAM.NsdEngine.AIRSPACE.V10
{
    public class AreaDefinitionNsdManager : NsdManager
    {

        public AreaDefinitionNsdManager(int categoryId, string categoryName, string version) : base(categoryId, categoryName, version)
        {
        }

        protected override TokenViewModel CreateTokenViewModel() => new AreaDefinitionViewModel();

        protected override TokenViewModel DeserializeToken(string tokens) => Deserialize<AreaDefinitionViewModel>(tokens);

        protected override string GenerateItemA(TokenViewModel token, ProposalContext context) => throw new NotImplementedException();

        protected override BilingualText GenerateItemE(TokenViewModel token, ProposalContext context) => throw new NotImplementedException();

        protected override string GetCode23(TokenViewModel token, ProposalContext context) => throw new NotImplementedException();

        protected override string GetCode45(TokenViewModel token, ProposalContext context) => throw new NotImplementedException();

        protected override string GetDisplayName() => Resources.AreaDefinition;

        protected override string GetFir(TokenViewModel token, ProposalContext context) => throw new NotImplementedException();

        protected override NearSdoSubject GetNearestSdoSubject(TokenViewModel token, ProposalContext context) => throw new NotImplementedException();

        protected override string GetNotamLocation(TokenViewModel token) => throw new NotImplementedException();

        protected override string GetNsdHelpLink() => Resources.RwyClsdHelpLink;

        protected override string GetPurpose(TokenViewModel token, ProposalContext context) => throw new NotImplementedException();

        protected override string GetScope(TokenViewModel token, ProposalContext context) => ScopeType.W.ToString();

        protected override Task<string> GetSeries(TokenViewModel token, ProposalContext context) => throw new NotImplementedException();

        protected override string GetTraffic(TokenViewModel token, ProposalContext context) => TrafficType.IV.ToString();

        protected override Task UpdateNsdContext(TokenViewModel token, ProposalContext context) => throw new NotImplementedException();

        protected override ValidationResult ValidateModel(ProposalViewModel model, ProposalContext context) => throw new NotImplementedException();
    }
}
﻿namespace NavCanada.Core.TaskEngine.Test
{
    using System;
    using System.Collections.Generic;
    using System.Threading;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;

    using Contracts;
    using Implementations;
    using Mto;

    [TestClass]
    public class MtoDispatcherTest : BaseTaskEngineTest
    {
        private const int ThreadCount = 5;
        private const int MtoCount = 20;

        private readonly object _lockObject = new object();

        private Mock<IClusterStorageFactory> _clusterDataSourcesFactoryMock;
        private Mock<IMtoProcessorFactory> _mtoProcessorFactoryMock;
        private Mock<IMtoProcessor> _mtoProcessorMock;
        private Mock<IMessageTaskFactory> _taskFactoryMock;

        [TestInitialize]
        public override void Initialize()
        {
            base.Initialize();
            var retrievedMtos = 0;
            TaskEngineQueueMock.Setup(q => q.Retrieve<MessageTransferObject>()).Returns(() =>
            {
                lock (_lockObject)
                {
                    if (retrievedMtos < MtoCount)
                    {
                        retrievedMtos = retrievedMtos + 1;
                        Thread.Sleep(100); // delay dispatching to ensure all threads have a chance to get at least one message
                        var result = new MessageTransferObject();
                        result.Add(MessageDefs.UniqueIdentifierKey, Guid.NewGuid().ToString(), DataType.String, false);
                        return result;
                    }
                    return null;
                }
            }); // return new empty MTOs when requested, up to a max of MtoCount
            var clusterStorage = ClusterStorage;
            if (clusterStorage != null)
            {
                _clusterDataSourcesFactoryMock = new Mock<IClusterStorageFactory>();
                _clusterDataSourcesFactoryMock.Setup(f => f.NewStorageConnection(out clusterStorage)).Returns(true);
            }
            _taskFactoryMock = new Mock<IMessageTaskFactory>();
        }

        [TestMethod]
        public void When_MultipleMtos_InQueue_ProcessAll()
        {
            var processedMessages = 0;
            _mtoProcessorMock = new Mock<IMtoProcessor>();
            _mtoProcessorMock.Setup(p => p.Run(It.IsAny<MessageTransferObject>())).Callback(() =>
            {
                processedMessages++;
            }).Returns(true);
            _mtoProcessorFactoryMock = new Mock<IMtoProcessorFactory>();
            _mtoProcessorFactoryMock.Setup(f => f.CreateMtoProcessor(
                It.IsAny<IClusterStorage>(), 
                It.IsAny<IMessageTaskFactory>(),
                It.IsAny<MtoProcessorSettings>())).Returns(_mtoProcessorMock.Object);
            var dispatcher = new TaskEngineMtoDispatcher(_clusterDataSourcesFactoryMock.Object, _taskFactoryMock.Object, _mtoProcessorFactoryMock.Object, new TaskEngineDispatcherSettings
            {
                MaxPoisonMtoRetryCount = 1
            });
            dispatcher.StartProcessing(5, 1000);
            Thread.Sleep(10000); // sleep for 10 second let dispatcher finish his job in other threads
            dispatcher.StopProcessing(1000);
            Assert.AreEqual(MtoCount, processedMessages);
        }

        [TestMethod]
        public void When_MultipleMtos_InQueue_AllThreadsRun()
        {
            var observedThreads = new HashSet<int>();
            _mtoProcessorMock = new Mock<IMtoProcessor>();
            _mtoProcessorMock.Setup(p => p.Run(It.IsAny<MessageTransferObject>())).Callback(() =>
            {
                observedThreads.Add(Thread.CurrentThread.ManagedThreadId);
            }).Returns(true);
            _mtoProcessorFactoryMock = new Mock<IMtoProcessorFactory>();
            _mtoProcessorFactoryMock.Setup(f => f.CreateMtoProcessor(
                It.IsAny<IClusterStorage>(), 
                It.IsAny<IMessageTaskFactory>(),
                It.IsAny<MtoProcessorSettings>())).Returns(_mtoProcessorMock.Object);
            var dispatcher = new TaskEngineMtoDispatcher(_clusterDataSourcesFactoryMock.Object, _taskFactoryMock.Object, _mtoProcessorFactoryMock.Object, new TaskEngineDispatcherSettings
            {
                MaxPoisonMtoRetryCount = 1
            });
            dispatcher.StartProcessing(5, 1000);
            Thread.Sleep(5000); // sleep for 5 second let dispatcher finish his job in other threads
            dispatcher.StopProcessing(1000);
            Assert.AreEqual(ThreadCount, observedThreads.Count);
        }
    }
}
﻿import { ISdoSubject } from '../../shared/data.model'

export interface ITwyClsdReason {
    id: number,
    text: string,
    textFr: string,
}

export interface ITwyClsdReasonLanguage {
    id: number,
    text: string,
}

export interface ITaxiwaySubject {
    id: string,
    index: number,
    mid: string,
    //name: string,
    designator: string,
    sdoEntityName: string,
    selected: boolean,
    full: boolean,
    start: string,
    startFr: string,
    end: string,
    endFr: string,
}



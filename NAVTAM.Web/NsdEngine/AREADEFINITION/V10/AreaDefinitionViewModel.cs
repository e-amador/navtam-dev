﻿namespace NAVTAM.NsdEngine.AIRSPACE.V10
{
    public class AreaDefinitionViewModel : TokenViewModel
    {
        public string[] Points { get; set; }
        public bool PolygonEntry { get; set; }
        public string Buffer { get; set; }
        public bool PointAndRadiusEntry { get; set; }
        public string Radius { get; set; }
        public bool LineBufferEntry { get; set; }
    }
}
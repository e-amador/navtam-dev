﻿namespace NavCanada.Services.Api.IdentityProvider.Config
{
    using System.Collections.Generic;
    using System.Security.Claims;

    using IdentityServer3.Core;
    using IdentityServer3.Core.Services.InMemory;

    public class Users
    {

        public static List<InMemoryUser> Get()
        {

            return new List<InMemoryUser>()
            {
                new InMemoryUser
                {
                    Username = "user1",
                    Password = "password",
                    Subject = "5AA8AA30-6BD3-4FCC-A81B-C3CD5B8C116A",

                    Claims = new[]
                    {
                        new Claim (Constants.ClaimTypes.GivenName, "User"),
                        new Claim(Constants.ClaimTypes.FamilyName, "ONE"),
                        new Claim(Constants.ClaimTypes.Address, "Address One"),
                        new Claim(Constants.ClaimTypes.Role, "Admin"),
                        new Claim(Constants.ClaimTypes.Role, "Free User")
                    }
                },
                new InMemoryUser
                {
                    Username = "user2",
                    Password = "password",
                    Subject = "27065FB5-282E-4C16-A79F-DAB1CD0BF1E6",

                    Claims = new[]
                    {
                        new Claim (Constants.ClaimTypes.GivenName, "User"),
                        new Claim(Constants.ClaimTypes.FamilyName, "TWO"),
                        new Claim(Constants.ClaimTypes.Address, "Address Two"),
                        new Claim(Constants.ClaimTypes.Role, "Free User")
                    }
                }
            };
        }
    }
}
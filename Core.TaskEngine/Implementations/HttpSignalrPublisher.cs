﻿using System;
using NavCanada.Core.TaskEngine.Contracts;
using System.Net;
using System.Collections.Specialized;
using System.Text;

namespace NavCanada.Core.TaskEngine.Implementations
{
    public class HttpSignalrPublisher : IHttpSignalrPublisher
    {
        public HttpSignalrPublisher(string endpoint)
        {
            _client = new WebClient();
            _client.UseDefaultCredentials = false;
            _client.Headers.Add("Accept:application/json");
            _uri = new Uri(endpoint + "/api/signalrnotification/statuschange");
        }

        public string PostRequest(NameValueCollection requestParams)
        {
            requestParams.Add("username", Username);
            requestParams.Add("password", Password);

            byte[] response = null;
            try
            {
                response = _client.UploadValues(_uri, "POST", requestParams);
            }
            catch (Exception e)
            {
                var x = e;
            }

            return Encoding.ASCII.GetString(response);
        }

        private string BuildSignalrUri(string endpoint, string path)
        {
            return !endpoint.EndsWith("/") ? $"{endpoint}/{path}" : $"{endpoint}{path}";
        }

        private WebClient _client;
        private Uri _uri;
        private const string Username = "signalr_user";
        private const string Password = "_Password123!";
    }
}

﻿using RSC.Model;

namespace RSC.Business.Helpers
{
    class RunwayDirection
    {
        public RunwayDirection(Runway runway, string designator, string sortKey, bool reversed)
        {
            Runway = runway;
            Designator = designator;
            SortKey = sortKey;
            Reversed = reversed;
        }
        public Runway Runway { get; }
        public string Designator { get; }
        public string SortKey { get; }
        public bool Reversed { get; }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NAVTAM.Controllers.api;
using NAVTAM.ViewModels;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace NAVTAM.Web.Test.Geography
{
    [TestClass]
    public class GeoRefToolTests
    {
        private GeoReferenceToolController cntrl;
        private PrivateObject obj;
        private PointViewModel pvm;
        private string[] pointsDD;
        private string[] pointsDDNW;
        private string[] pointsDMS;
        private string[] points;
        private string[] duplicatePoints;
        private string[] onePoint;
        private string[] testPoint;
        private string[] threePoints;


        [TestInitialize]
        public void GeoRefTool_SetUpDataForTests()
        {
            cntrl = new GeoReferenceToolController(null, null, null, null);
            obj = new PrivateObject(cntrl);
            pvm = new PointViewModel();

            pointsDD = new string[] { "48.7018377 -79.1729736", "47.0963053 -79.5135498", "47.3685943 -75.2124023", "50.2612538 -72.5976563", "49.7954499 -76.9921875", "51.9848801 -76.2451172" };
            pointsDDNW = new string[] { "48.7018377N 79.1729736W", "47.0963053N 79.5135498W", "47.3685943N 75.2124023W", "50.2612538N 72.5976563W", "49.7954499N 76.9921875W", "51.9848801N 76.2451172W" };
            pointsDMS = new string[] { "404533N 735924W", "444536N 745924W", "403536N 732924W", "414536N 715924W", "394536N 705924W", "402536N 703924W" };
            points = new string[] { "44.034657 -80.228911", "44.045658 -80.191060", "44.018257 -80.164293", "43.999219 -80.170169", "44.005416 -80.219087", "44.025504 -80.248783", "44.047078 -80.244486" };
            duplicatePoints = new string[] { "44.034657 -80.228911", "44.034657 -80.228911" };
            onePoint = new string[] { "44.034657 -80.228911" };
            threePoints = new string[] { "45.0 -75.0", "45.193774 -74.881822", "45.011850 -74.816608" };
            testPoint = new string[] { "48.7018377 -79.1729736", "47.0963053 -79.5135498", "47.3685943 -75.2124023" };
        }

        #region ViewModel
        private PointViewModel SetPvm(string type)
        {
            switch (type)
            {
                case "DD":
                    return new PointViewModel
                    {
                        Points = pointsDD
                    };

                case "DDNW":
                    return new PointViewModel
                    {
                        Points = pointsDDNW
                    };
                case "threePoints":
                    return new PointViewModel
                    {
                        Points = threePoints
                    };

                case "DMS":
                    return new PointViewModel
                    {
                        Points = pointsDMS
                    };
                case "DD2":
                    return new PointViewModel
                    {
                        Points = points
                    };
                case "duplicatePoints":
                    return new PointViewModel
                    {
                        Points = duplicatePoints
                    };
                case "onePoint":
                    return new PointViewModel
                    {
                        Points = onePoint
                    };
                case "testPoint":
                    return new PointViewModel
                    {
                        Points = testPoint
                    };
                default:
                    return new PointViewModel
                    {
                        Points = pointsDD
                    };
            }
        }
        #endregion

        //[TestMethod]
        //public async Task GeoRefTool_CalculatesCirleDDAsync()
        //{
        //    var response = await cntrl.CalculateCircle(SetPvm("DD"));
        //    var contentResult = response as OkNegotiatedContentResult<PointViewModel>;
        //    Assert.IsTrue(contentResult.Content.Error == null); //no errors
        //    Assert.IsNotNull(contentResult.Content.CalculatedPoint); //correct parsing
        //}

        //[TestMethod]
        //public async Task GeoRefTool_ValidateCircleBuffersRadiusAsync()
        //{
        //    var response = await cntrl.CalculateCircle(SetPvm("DD"));
        //    var contentResult = response as OkNegotiatedContentResult<PointViewModel>;
        //    Assert.IsTrue(contentResult.Content.Error == null); //no errors
        //    Assert.IsNotNull(contentResult.Content.CalculatedPoint); //correct parsing
        //}

        //[TestMethod]
        //public async Task GeoRefTool_CalculatesCirleThreePointsAsync()
        //{
        //    var response = await cntrl.CalculateCircle(SetPvm("threePoints"));
        //    var contentResult = response as OkNegotiatedContentResult<PointViewModel>;
        //    Assert.IsTrue(contentResult.Content.Error == null); //no errors
        //    Assert.IsNotNull(contentResult.Content.CalculatedPoint); //correct parsing
        //}

        //[TestMethod]
        //public async Task GeoRefTool_CalculatesCirleTestPointAsync()
        //{
        //    var response = await cntrl.CalculateCircle(SetPvm("testPoint"));
        //    var contentResult = response as OkNegotiatedContentResult<PointViewModel>;
        //    Assert.IsTrue(contentResult.Content.Error == null); //no errors
        //    Assert.IsNotNull(contentResult.Content.CalculatedPoint); //correct parsing
        //}

        //[TestMethod]
        //public async Task GeoRefTool_CalculatesCirleDD2Async()
        //{
        //    var response = await cntrl.CalculateCircle(SetPvm("DD2"));
        //    var contentResult = response as OkNegotiatedContentResult<PointViewModel>;
        //    Assert.IsTrue(contentResult.Content.Error == null); //no errors
        //    Assert.IsNotNull(contentResult.Content.CalculatedPoint); //correct parsing
        //}

        //[TestMethod]
        //public async Task GeoRefTool_CalculateCirclePointsDMSAsync()
        //{
        //    var response = await cntrl.CalculateCircle(SetPvm("DMS"));
        //    var contentResult = response as OkNegotiatedContentResult<PointViewModel>;
        //    Assert.IsTrue(contentResult.Content.Error == null); //no errors
        //    Assert.IsNotNull(contentResult.Content.CalculatedPoint); //correct parsing
        //}

        //[TestMethod]
        //public async Task GeoRefTool_CalculateCirclePointsDuplicatePointsAsync()
        //{
        //    var response = await cntrl.CalculateCircle(SetPvm("duplicatePoints"));
        //    var contentResult = response as OkNegotiatedContentResult<PointViewModel>;
        //    Assert.IsTrue(contentResult.Content.Error == null); //no errors
        //    Assert.IsTrue(contentResult.Content.CalculatedRadius == 1); //duplicate points should have a radius of one
        //}

        //[TestMethod]
        //public async Task GeoRefTool_CalculateCirclePointOnePointAsync()
        //{
        //    var response = await cntrl.CalculateCircle(SetPvm("onePoint"));
        //    var contentResult = response as OkNegotiatedContentResult<PointViewModel>;
        //    Assert.IsTrue(contentResult.Content.Error.Length > 0); // should be error
        //    Assert.IsTrue(contentResult.Content.CalculatedPoint == null); // did not calculate point
        //    Assert.IsTrue(contentResult.Content.CalculatedRadius == null); // did not calculate radious
        //}

        //[TestMethod]
        //public async Task GeoRefTool_CalculatedRadiusIsCorrectAsync()
        //{
        //    var response = await cntrl.CalculateCircle(SetPvm("DD"));
        //    var contentResult = response as OkNegotiatedContentResult<PointViewModel>;

        //    Assert.IsTrue(contentResult.Content.Error == null);
        //    Assert.IsNotNull(contentResult.Content.CalculatedRadius);

        //    response = await cntrl.CalculateCircle(SetPvm("DMS"));
        //    contentResult = response as OkNegotiatedContentResult<PointViewModel>;

        //    Assert.IsTrue(contentResult.Content.Error == null);
        //    Assert.IsNotNull(contentResult.Content.CalculatedRadius);
        //}

        //[TestMethod]
        //public async Task GeoRefTool_ReturnsProperCalculatedPointAccuracyAsync()
        //{
        //    var responseDD = await cntrl.CalculateCircle(SetPvm("DD"));
        //    var contentResult = responseDD as OkNegotiatedContentResult<PointViewModel>;
        //    var str = contentResult.Content.CalculatedPoint.Split(' ');

        //    Assert.IsTrue(str[0].Length == 5);
        //    Assert.IsTrue(str[1].Length == 6);
        //    Assert.IsNotNull(contentResult.Content.CalculatedPoint);

        //    var responseDMS = await cntrl.CalculateCircle(SetPvm("DMS"));
        //    contentResult = responseDMS as OkNegotiatedContentResult<PointViewModel>;
        //    str = contentResult.Content.CalculatedPoint.Split(' ');

        //    Assert.IsTrue(str[0].Length == 5);
        //    Assert.IsTrue(str[1].Length == 6);
        //    Assert.IsNotNull(contentResult.Content.CalculatedPoint);

        //    var responseDDNW = await cntrl.CalculateCircle(SetPvm("DDNW"));
        //    contentResult = responseDDNW as OkNegotiatedContentResult<PointViewModel>;
        //    str = contentResult.Content.CalculatedPoint.Split(' ');

        //    Assert.IsTrue(str[0].Length == 5);
        //    Assert.IsTrue(str[1].Length == 6);
        //    Assert.IsNotNull(contentResult.Content.CalculatedPoint);
        //}
    }
}
﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RSC.Model;
using RSC.Model.Enums;
using RSC.Business.Helpers;

namespace RSC.WebService.Tests
{
    [TestClass]
    public class RWYCCCalculationTests
    {

        #region Factories
        private ContaminantIns ContaminantInsFactory(int coverage, ContaminantWithDepthEnum contamType, ContamDepth contamDepth)
        {
            return new ContaminantIns { ContamCoverage = coverage, ContamDepthI = contamDepth, ContamType = contamType };
        }

        private ContamDepth ContamDepthFactory(decimal inches, bool trace = false)
        {
            return new ContamDepth { ContamInches = inches, ContamTrace = trace };
        }

        private ContaminantIns ContaminantInsFactoryNoD(int coverage, ContaminantWithNoDepthEnum conNoD)
        {
            return new ContaminantIns { ContamCoverage = coverage, ContamTypeNoD = conNoD };
        }

        private ClearedSurfaceThird[] ClearedSurfaceThirdArr(ContaminantIns[] portion1, ContaminantIns[] portion2, ContaminantIns[] portion3, 
            string Rwycc1, string Rwycc2, string Rwycc3, int temperature1 = 0, int temperature2 = 0, int temperature3 = 0)
        {
            return new ClearedSurfaceThird[]
            {
                new ClearedSurfaceThird { Portion = 1, Contaminants = portion1, RwyFrictionThird = RwyFrictionFactory(temperature1), RwyCC = Rwycc1},
                new ClearedSurfaceThird { Portion = 2, Contaminants = portion2, RwyFrictionThird = RwyFrictionFactory(temperature2), RwyCC = Rwycc2},
                new ClearedSurfaceThird { Portion = 3, Contaminants = portion3, RwyFrictionThird = RwyFrictionFactory(temperature3), RwyCC = Rwycc3}
            };
        }

        private ClearedPortion ClearedPortionFactory(ClearedSurfaceThird[] clearedSurfaceThirds, ClearedSurface average = null)
        {
            if (average != null)
            {
                return new ClearedPortion { Average = average };
            }

            return new ClearedPortion { Average = average, RunwayThirds = clearedSurfaceThirds };
        }

        private ClearedSurface ClearedSurfaceFactory(ContaminantIns[] contamintsIns)
        {
            return new ClearedSurface { Contaminants = contamintsIns };
        }

        private RwyFriction RwyFrictionFactory(int temperature)
        {
            return new RwyFriction { Temperature = temperature.ToString() };
        }
        #endregion

        #region Tests
        [TestMethod]
        public void DRY_Test()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];
            cnts1[0] = ContaminantInsFactoryNoD(100, ContaminantWithNoDepthEnum.Dry);
            cnts2[0] = ContaminantInsFactoryNoD(100, ContaminantWithNoDepthEnum.Dry);
            cnts3[0] = ContaminantInsFactoryNoD(100, ContaminantWithNoDepthEnum.Dry);

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "6", "6", "6"));
            var RunwayClearedPortion = new RunwayClearedPortion();

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void DRYSNOW_Less3mm_Less25PercentCoverage()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[2];
            ContaminantIns[] cnts2 = new ContaminantIns[2];
            ContaminantIns[] cnts3 = new ContaminantIns[2];
            cnts1[0] = ContaminantInsFactory(10, ContaminantWithDepthEnum.DrySnow, ContamDepthFactory(13m));
            cnts1[1] = ContaminantInsFactory(10, ContaminantWithDepthEnum.DrySnow, ContamDepthFactory(13m));

            cnts2[0] = ContaminantInsFactory(10, ContaminantWithDepthEnum.DrySnow, ContamDepthFactory(13m));
            cnts2[1] = ContaminantInsFactory(10, ContaminantWithDepthEnum.DrySnow, ContamDepthFactory(13m));

            cnts3[0] = ContaminantInsFactory(10, ContaminantWithDepthEnum.DrySnow, ContamDepthFactory(13m));
            cnts3[1] = ContaminantInsFactory(10, ContaminantWithDepthEnum.DrySnow, ContamDepthFactory(13m));

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "6", "6", "6"));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result);
        }


        [TestMethod]
        public void FROST_Less25Coverage()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];

            cnts1[0] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.Frost);
            cnts2[0] = ContaminantInsFactoryNoD(20, ContaminantWithNoDepthEnum.Frost);
            cnts3[0] = ContaminantInsFactoryNoD(10, ContaminantWithNoDepthEnum.Frost);

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "6", "6", "6"));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void FROST_More25Coverage()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];

            cnts1[0] = ContaminantInsFactoryNoD(30, ContaminantWithNoDepthEnum.Frost);
            cnts2[0] = ContaminantInsFactoryNoD(30, ContaminantWithNoDepthEnum.Frost);
            cnts3[0] = ContaminantInsFactoryNoD(40, ContaminantWithNoDepthEnum.Frost);

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "5", "5", "5"));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void WET_More25Coverage_Under3mm()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];

            cnts1[0] = ContaminantInsFactoryNoD(30, ContaminantWithNoDepthEnum.Wet);
            cnts2[0] = ContaminantInsFactoryNoD(30, ContaminantWithNoDepthEnum.Wet);
            cnts3[0] = ContaminantInsFactoryNoD(30, ContaminantWithNoDepthEnum.Wet);

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "5", "5", "5"));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result  );
        }

        [TestMethod]
        public void SLUSH_More25Coverage_Under3mm()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];

            cnts1[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.Slush, ContamDepthFactory(0m));
            cnts2[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.Slush, ContamDepthFactory(0.13m));
            cnts3[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.Slush, ContamDepthFactory(0.13m));

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "5", "5", "5"));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result);
        }


        [TestMethod]
        public void DRYSNOW_More25Coverage()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];

            cnts1[0] = ContaminantInsFactory(25, ContaminantWithDepthEnum.DrySnow, ContamDepthFactory(13m));
            cnts2[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.DrySnow, ContamDepthFactory(0.13m));
            cnts3[0] = ContaminantInsFactory(10, ContaminantWithDepthEnum.DrySnow, ContamDepthFactory(13m));

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "6", "5", "6"));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result  );
        }

        [TestMethod]
        public void WETSNOW_More25Coverage_More3mm()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];

            cnts1[0] = ContaminantInsFactory(25, ContaminantWithDepthEnum.WetSnow, ContamDepthFactory(13m));
            cnts2[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.WetSnow, ContamDepthFactory(13m));
            cnts3[0] = ContaminantInsFactory(10, ContaminantWithDepthEnum.WetSnow, ContamDepthFactory(13m));

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "6", "3", "6"));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }

        [TestMethod]
        public void COMPACTEDSNOW_More25Coverage_More3mm()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];

            cnts1[0] = ContaminantInsFactoryNoD(30, ContaminantWithNoDepthEnum.CompactedSnow);
            cnts2[0] = ContaminantInsFactoryNoD(30, ContaminantWithNoDepthEnum.CompactedSnow);
            cnts3[0] = ContaminantInsFactoryNoD(30, ContaminantWithNoDepthEnum.CompactedSnow);

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "4", "4", "4" , -15, -15, -15));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }

        [TestMethod]
        public void COMPACTEDSNOW_More25Coverage_More3mm_Warm()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];

            cnts1[0] = ContaminantInsFactoryNoD(30, ContaminantWithNoDepthEnum.CompactedSnow);
            cnts2[0] = ContaminantInsFactoryNoD(30, ContaminantWithNoDepthEnum.CompactedSnow);
            cnts3[0] = ContaminantInsFactoryNoD(30, ContaminantWithNoDepthEnum.CompactedSnow);

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "3", "3", "3", -14, -14, -14));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }


        [TestMethod]
        public void SLIPWET_More25Coverage()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];

            cnts1[0] = ContaminantInsFactoryNoD(30, ContaminantWithNoDepthEnum.SlipperyWhenWet);
            cnts2[0] = ContaminantInsFactoryNoD(30, ContaminantWithNoDepthEnum.SlipperyWhenWet);
            cnts3[0] = ContaminantInsFactoryNoD(30, ContaminantWithNoDepthEnum.SlipperyWhenWet);

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "3", "3", "3"));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void DRYSNOWCOMPCSNOW_More25Coverage_3mm()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];

            cnts1[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.DrySnowOnTopOfCompactedSnow, ContamDepthFactory(0.13m));
            cnts2[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.DrySnowOnTopOfCompactedSnow, ContamDepthFactory(0.13m));
            cnts3[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.DrySnowOnTopOfCompactedSnow, ContamDepthFactory(0.13m));

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "3", "3", "3"));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }

        [TestMethod]
        public void DRYSNOWCOMPCSNOW_More25Coverage_Greater3mm()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];

            cnts1[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.DrySnowOnTopOfCompactedSnow, ContamDepthFactory(13m));
            cnts2[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.DrySnowOnTopOfCompactedSnow, ContamDepthFactory(13m));
            cnts3[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.DrySnowOnTopOfCompactedSnow, ContamDepthFactory(13m));

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "3", "3", "3"));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }

        [TestMethod]
        public void WETSNOWCOMPCSNOW_More25Coverage_Greater3mm()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];

            cnts1[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.WetSnowOnTopOfCompactedSnow, ContamDepthFactory(13m));
            cnts2[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.WetSnowOnTopOfCompactedSnow, ContamDepthFactory(13m));
            cnts3[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.WetSnowOnTopOfCompactedSnow, ContamDepthFactory(13m));

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "3", "3", "3"));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void WETSNOWCOMPCSNOW_Greater3mm()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];

            cnts1[0] = ContaminantInsFactory(25, ContaminantWithDepthEnum.WetSnowOnTopOfCompactedSnow, ContamDepthFactory(13m));
            cnts2[0] = ContaminantInsFactory(25, ContaminantWithDepthEnum.WetSnowOnTopOfCompactedSnow, ContamDepthFactory(13m));
            cnts3[0] = ContaminantInsFactory(10, ContaminantWithDepthEnum.WetSnowOnTopOfCompactedSnow, ContamDepthFactory(13m));

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "6", "6", "6"));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }

        [TestMethod]
        public void STANDINGWATER_More25Coverage_Greater3mm()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];

            cnts1[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.StandingWater, ContamDepthFactory(13m));
            cnts2[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.StandingWater, ContamDepthFactory(13m));
            cnts3[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.StandingWater, ContamDepthFactory(13m));

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "2", "2", "2"));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void SLUSH_More25Coverage_Greater3mm()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];

            cnts1[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.Slush, ContamDepthFactory(13m));
            cnts2[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.Slush, ContamDepthFactory(13m));
            cnts3[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.Slush, ContamDepthFactory(13m));

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "2", "2", "2"));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ICE_More25Coverage()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];

            cnts1[0] = ContaminantInsFactoryNoD(30, ContaminantWithNoDepthEnum.Ice);
            cnts2[0] = ContaminantInsFactoryNoD(30, ContaminantWithNoDepthEnum.Ice);
            cnts3[0] = ContaminantInsFactoryNoD(30, ContaminantWithNoDepthEnum.Ice);

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "1", "1", "1"));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result  );
        }

        [TestMethod]
        public void ICE_Less25Coverage()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];

            cnts1[0] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.Ice);
            cnts2[0] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.Ice);
            cnts3[0] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.Ice);

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "6", "6", "6"));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }

        [TestMethod]
        public void WETICE_Less25Coverage()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];

            cnts1[0] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.WetIce);
            cnts2[0] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.WetIce);
            cnts3[0] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.WetIce);

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "6", "6", "6"));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }

        [TestMethod]
        public void WETICE_More25Coverage()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];

            cnts1[0] = ContaminantInsFactoryNoD(30, ContaminantWithNoDepthEnum.WetIce);
            cnts2[0] = ContaminantInsFactoryNoD(10, ContaminantWithNoDepthEnum.WetIce);
            cnts3[0] = ContaminantInsFactoryNoD(60, ContaminantWithNoDepthEnum.WetIce);

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "0", "6", "0"));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }

        [TestMethod]
        public void SLUSHONICE_Less25Coverage_MixedDepth()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];

            cnts1[0] = ContaminantInsFactory(25, ContaminantWithDepthEnum.SlushOnTopOfIce, ContamDepthFactory(0.13m));
            cnts2[0] = ContaminantInsFactory(25, ContaminantWithDepthEnum.SlushOnTopOfIce, ContamDepthFactory(13m));
            cnts3[0] = ContaminantInsFactory(25, ContaminantWithDepthEnum.SlushOnTopOfIce, ContamDepthFactory(13m));

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "6", "6", "6"));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }

        [TestMethod]
        public void SLUSHONICE_More25Coverage_MixedDepth()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];

            cnts1[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.SlushOnTopOfIce, ContamDepthFactory(0.13m));
            cnts2[0] = ContaminantInsFactory(10, ContaminantWithDepthEnum.SlushOnTopOfIce, ContamDepthFactory(13m));
            cnts3[0] = ContaminantInsFactory(60, ContaminantWithDepthEnum.SlushOnTopOfIce, ContamDepthFactory(13m));

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "0", "6", "0"));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }

        [TestMethod]
        public void WATERONCOMPACTEDSNOW_Less25Coverage_MixedDepth()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];

            cnts1[0] = ContaminantInsFactory(25, ContaminantWithDepthEnum.WaterOnTopOfCompactedSnow, ContamDepthFactory(0.13m));
            cnts2[0] = ContaminantInsFactory(25, ContaminantWithDepthEnum.WaterOnTopOfCompactedSnow, ContamDepthFactory(13m));
            cnts3[0] = ContaminantInsFactory(25, ContaminantWithDepthEnum.WaterOnTopOfCompactedSnow, ContamDepthFactory(13m));

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "6", "6", "6"));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }

        [TestMethod]
        public void WATERONCOMPACTEDSNOW_More25Coverage_MixedDepth()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];

            cnts1[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.WaterOnTopOfCompactedSnow, ContamDepthFactory(0.13m));
            cnts2[0] = ContaminantInsFactory(10, ContaminantWithDepthEnum.WaterOnTopOfCompactedSnow, ContamDepthFactory(13m));
            cnts3[0] = ContaminantInsFactory(50, ContaminantWithDepthEnum.WaterOnTopOfCompactedSnow, ContamDepthFactory(13m));

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "0", "6", "0"));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }

        [TestMethod]
        public void DRYSNOWONICE_More25Coverage_MixedDepth()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];

            cnts1[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.DrySnowOnTopOfIce, ContamDepthFactory(0.13m));
            cnts2[0] = ContaminantInsFactory(10, ContaminantWithDepthEnum.DrySnowOnTopOfIce, ContamDepthFactory(13m));
            cnts3[0] = ContaminantInsFactory(50, ContaminantWithDepthEnum.DrySnowOnTopOfIce, ContamDepthFactory(13m));

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "0", "6", "0"));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }

        [TestMethod]
        public void WETSNOWONICE_More25Coverage_MixedDepth()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[1];
            ContaminantIns[] cnts2 = new ContaminantIns[1];
            ContaminantIns[] cnts3 = new ContaminantIns[1];

            cnts1[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.WetSnowOnTopOfIce, ContamDepthFactory(0.13m));
            cnts2[0] = ContaminantInsFactory(10, ContaminantWithDepthEnum.WetSnowOnTopOfIce, ContamDepthFactory(13m));
            cnts3[0] = ContaminantInsFactory(50, ContaminantWithDepthEnum.WetSnowOnTopOfIce, ContamDepthFactory(13m));

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "0", "6", "0"));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }

        [TestMethod]
        public void MIXEDCONTAMINENTS_1()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[2];
            ContaminantIns[] cnts2 = new ContaminantIns[2];
            ContaminantIns[] cnts3 = new ContaminantIns[2];

            //TIE COVERAGE & ONE DEPTH OF 13"
            cnts1[0] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.Wet);
            cnts1[1] = ContaminantInsFactory(25, ContaminantWithDepthEnum.DrySnow, ContamDepthFactory(13m));

            //BOTH UNDER 25% COVERAGE // DEPTH OF 13"
            cnts2[0] = ContaminantInsFactory(10, ContaminantWithDepthEnum.StandingWater, ContamDepthFactory(13m));
            cnts2[1] = ContaminantInsFactory(10, ContaminantWithDepthEnum.DrySnowOnTopOfIce, ContamDepthFactory(13m));

            //ONE > 25 one <= 25
            cnts3[0] = ContaminantInsFactory(25, ContaminantWithDepthEnum.WetSnowOnTopOfIce, ContamDepthFactory(13m));
            cnts3[1] = ContaminantInsFactory(50, ContaminantWithDepthEnum.WetSnowOnTopOfIce, ContamDepthFactory(13m));

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "3", "6", "0"));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }

        [TestMethod]
        public void MIXEDCONTAMINENTS_2()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[2];
            ContaminantIns[] cnts2 = new ContaminantIns[2];
            ContaminantIns[] cnts3 = new ContaminantIns[2];

            //TIE COVERAGE & ONE DEPTH OF 13" & COLDER THEN -15c
            cnts1[0] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.Wet);
            cnts1[1] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.CompactedSnow);

            //BOTH UNDER 25% COVERAGE // DEPTH OF 13"
            cnts2[0] = ContaminantInsFactory(10, ContaminantWithDepthEnum.StandingWater, ContamDepthFactory(13m));
            cnts2[1] = ContaminantInsFactory(10, ContaminantWithDepthEnum.DrySnowOnTopOfIce, ContamDepthFactory(13m));

            //ONE > 25 one <= 25
            cnts3[0] = ContaminantInsFactory(50, ContaminantWithDepthEnum.WetSnowOnTopOfIce, ContamDepthFactory(13m));
            cnts3[1] = ContaminantInsFactory(25, ContaminantWithDepthEnum.DrySnow, ContamDepthFactory(13m));

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "4", "6", "0", - 17, -17, -17));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }

        [TestMethod]
        public void MIXEDCONTAMINENTS_3()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[2];
            ContaminantIns[] cnts2 = new ContaminantIns[2];
            ContaminantIns[] cnts3 = new ContaminantIns[2];

            //TIE COVERAGE & ONE DEPTH OF 13" & WARMER THEN -15c
            cnts1[0] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.Wet);
            cnts1[1] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.CompactedSnow);

            //BOTH UNDER 25% COVERAGE // DEPTH OF 13"
            cnts2[0] = ContaminantInsFactory(10, ContaminantWithDepthEnum.StandingWater, ContamDepthFactory(13m));
            cnts2[1] = ContaminantInsFactory(10, ContaminantWithDepthEnum.DrySnowOnTopOfIce, ContamDepthFactory(13m));

            //ONE > 25 one <= 25
            cnts3[0] = ContaminantInsFactory(50, ContaminantWithDepthEnum.WetSnowOnTopOfIce, ContamDepthFactory(13m));
            cnts3[1] = ContaminantInsFactory(50, ContaminantWithDepthEnum.DrySnow, ContamDepthFactory(13m));

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "3", "6", "0", -14, -14, -14));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }

        [TestMethod]
        public void MIXEDCONTAMINENTS_4()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[2];
            ContaminantIns[] cnts2 = new ContaminantIns[2];
            ContaminantIns[] cnts3 = new ContaminantIns[2];

            //TIE COVERAGE & ONE DEPTH OF 13" & -15c
            cnts1[0] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.WetIce);
            cnts1[1] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.CompactedSnow);

            //TIE COVERAGE // DEPTH OF 13"
            cnts2[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.StandingWater, ContamDepthFactory(13m));
            cnts2[1] = ContaminantInsFactory(30, ContaminantWithDepthEnum.WetSnowOnTopOfIce, ContamDepthFactory(13m));

            //TIE COVERAGE // DEPTH OF 13"
            cnts3[0] = ContaminantInsFactory(50, ContaminantWithDepthEnum.StandingWater, ContamDepthFactory(13m));
            cnts3[1] = ContaminantInsFactory(50, ContaminantWithDepthEnum.WetSnowOnTopOfIce, ContamDepthFactory(13m));

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "0", "0", "0", -15, -15, -15));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }

        [TestMethod]
        public void MIXEDCONTAMINENTS_5()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[2];
            ContaminantIns[] cnts2 = new ContaminantIns[2];
            ContaminantIns[] cnts3 = new ContaminantIns[2];

            //TIE COVERAGE & ONE DEPTH OF 13" & -15c
            cnts1[0] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.WetIce);
            cnts1[1] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.CompactedSnow);

            //TIE COVERAGE // DEPTH OF 13"
            cnts2[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.StandingWater, ContamDepthFactory(13m));
            cnts2[1] = ContaminantInsFactory(30, ContaminantWithDepthEnum.WetSnowOnTopOfIce, ContamDepthFactory(13m));

            //TIE COVERAGE // TIE DEPTH
            cnts3[0] = ContaminantInsFactory(50, ContaminantWithDepthEnum.WetSnow, ContamDepthFactory(13m));
            cnts3[1] = ContaminantInsFactory(50, ContaminantWithDepthEnum.WetSnowOnTopOfIce, ContamDepthFactory(13m));

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "0", "0", "0", -15, -15, -15));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }

        [TestMethod]
        public void MIXEDCONTAMINENTS_6()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[2];
            ContaminantIns[] cnts2 = new ContaminantIns[2];
            ContaminantIns[] cnts3 = new ContaminantIns[2];

            //TIE COVERAGE & ONE DEPTH OF 13" & -15c
            cnts1[0] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.WetIce);
            cnts1[1] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.CompactedSnow);

            //TIE COVERAGE // DEPTH OF 13"
            cnts2[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.StandingWater, ContamDepthFactory(13m));
            cnts2[1] = ContaminantInsFactory(30, ContaminantWithDepthEnum.WetSnowOnTopOfIce, ContamDepthFactory(5m));

            //TIE COVERAGE // TIE DEPTH
            cnts3[0] = ContaminantInsFactory(50, ContaminantWithDepthEnum.WetSnowOnTopOfIce, ContamDepthFactory(13m));
            cnts3[1] = ContaminantInsFactory(50, ContaminantWithDepthEnum.DrySnow, ContamDepthFactory(26m));

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "0", "0", "0", -15, -15, -15));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }

        [TestMethod]
        public void MIXEDCONTAMINENTS_7()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[2];
            ContaminantIns[] cnts2 = new ContaminantIns[2];
            ContaminantIns[] cnts3 = new ContaminantIns[2];

            //TIE COVERAGE & ONE DEPTH OF 13" & -15c
            cnts1[0] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.WetIce);
            cnts1[1] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.CompactedSnow);

            //TIE COVERAGE // DEPTH OF 13"
            cnts2[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.StandingWater, ContamDepthFactory(13m));
            cnts2[1] = ContaminantInsFactory(30, ContaminantWithDepthEnum.Slush, ContamDepthFactory(5m));

            //TIE COVERAGE // MIXED DEPTH
            cnts3[0] = ContaminantInsFactory(50, ContaminantWithDepthEnum.WetSnowOnTopOfIce, ContamDepthFactory(13m));
            cnts3[1] = ContaminantInsFactory(50, ContaminantWithDepthEnum.DrySnow, ContamDepthFactory(26m));

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "0", "2", "0", -15, -15, -15));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }

        [TestMethod]
        public void MIXEDCONTAMINENTS_8()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[2];
            ContaminantIns[] cnts2 = new ContaminantIns[2];
            ContaminantIns[] cnts3 = new ContaminantIns[2];

            //TIE COVERAGE & ONE DEPTH OF 13" & -15c
            cnts1[0] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.WetIce);
            cnts1[1] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.CompactedSnow);

            //TIE COVERAGE // MIXED DEPTH
            cnts2[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.StandingWater, ContamDepthFactory(13m));
            cnts2[1] = ContaminantInsFactory(30, ContaminantWithDepthEnum.WetSnowOnTopOfIce, ContamDepthFactory(5m));

            //TIE COVERAGE // MIXED DEPTH
            cnts3[0] = ContaminantInsFactory(40, ContaminantWithDepthEnum.WetSnowOnTopOfIce, ContamDepthFactory(13m));
            cnts3[1] = ContaminantInsFactory(50, ContaminantWithDepthEnum.DrySnow, ContamDepthFactory(26m));

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "0", "0", "0", -15, -15, -15));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }

        [TestMethod]
        public void MIXEDCONTAMINENTS_9()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[2];
            ContaminantIns[] cnts2 = new ContaminantIns[2];
            ContaminantIns[] cnts3 = new ContaminantIns[2];

            //TIE COVERAGE & ONE DEPTH OF 13" & -15c
            cnts1[0] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.CompactedSnow);
            cnts1[1] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.Dry);

            //TIE COVERAGE // MIXED DEPTH
            cnts2[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.StandingWater, ContamDepthFactory(13m));
            cnts2[1] = ContaminantInsFactoryNoD(50, ContaminantWithNoDepthEnum.Ice);

            //TIE COVERAGE // MIXED DEPTH
            cnts3[0] = ContaminantInsFactory(40, ContaminantWithDepthEnum.WetSnowOnTopOfIce, ContamDepthFactory(13m));
            cnts3[1] = ContaminantInsFactoryNoD(50, ContaminantWithNoDepthEnum.SlipperyWhenWet);

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "4", "1", "0", -15, -15, -15));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }

        [TestMethod]
        public void MIXEDCONTAMINENTS_10()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[2];
            ContaminantIns[] cnts2 = new ContaminantIns[2];
            ContaminantIns[] cnts3 = new ContaminantIns[2];

            //TIE COVERAGE & ONE DEPTH OF 13" & -15c
            cnts1[0] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.CompactedSnow);
            cnts1[1] = ContaminantInsFactoryNoD(25, ContaminantWithNoDepthEnum.Dry);

            //TIE COVERAGE // MIXED DEPTH
            cnts2[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.StandingWater, ContamDepthFactory(13m));
            cnts2[1] = ContaminantInsFactoryNoD(50, ContaminantWithNoDepthEnum.Ice);

            //TIE COVERAGE // MIXED DEPTH
            cnts3[0] = ContaminantInsFactory(40, ContaminantWithDepthEnum.WetSnowOnTopOfIce, ContamDepthFactory(13m));
            cnts3[1] = ContaminantInsFactoryNoD(50, ContaminantWithNoDepthEnum.SlipperyWhenWet);

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "4", "1", "0", -15, -15, -15));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }

        [TestMethod]
        public void MIXEDCONTAMINENTS_11()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[2];
            ContaminantIns[] cnts2 = new ContaminantIns[2];
            ContaminantIns[] cnts3 = new ContaminantIns[2];

            cnts1[0] = ContaminantInsFactoryNoD(20, ContaminantWithNoDepthEnum.CompactedSnow);
            cnts1[1] = ContaminantInsFactoryNoD(30, ContaminantWithNoDepthEnum.Dry);

            cnts2[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.StandingWater, ContamDepthFactory(13m));
            cnts2[1] = ContaminantInsFactoryNoD(50, ContaminantWithNoDepthEnum.Ice);

            cnts3[0] = ContaminantInsFactory(20, ContaminantWithDepthEnum.WetSnowOnTopOfIce, ContamDepthFactory(13m));
            cnts3[1] = ContaminantInsFactoryNoD(20, ContaminantWithNoDepthEnum.SlipperyWhenWet);

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "6", "1", "0", -17, -15, -15));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result );
        }

        [TestMethod]
        public void MIXEDCONTAMINENTS_12()
        {
            ContaminantIns[] cnts1 = new ContaminantIns[2];
            ContaminantIns[] cnts2 = new ContaminantIns[2];
            ContaminantIns[] cnts3 = new ContaminantIns[2];

            cnts1[0] = ContaminantInsFactory(25, ContaminantWithDepthEnum.WetSnow, ContamDepthFactory(13m));
            cnts1[1] = ContaminantInsFactory(10, ContaminantWithDepthEnum.DrySnowOnTopOfIce, ContamDepthFactory(13m));
                        
            cnts2[0] = ContaminantInsFactory(30, ContaminantWithDepthEnum.StandingWater, ContamDepthFactory(13m));
            cnts2[1] = ContaminantInsFactoryNoD(50, ContaminantWithNoDepthEnum.Ice);

            cnts3[0] = ContaminantInsFactory(20, ContaminantWithDepthEnum.WetSnowOnTopOfIce, ContamDepthFactory(13m));
            cnts3[1] = ContaminantInsFactoryNoD(20, ContaminantWithNoDepthEnum.SlipperyWhenWet);

            var clearedPortion = ClearedPortionFactory(ClearedSurfaceThirdArr(cnts1, cnts2, cnts3, "3", "1", "0", -17, -15, -15));

            var result = RCAMCalculation.GetCalculationFromClearedPortion(clearedPortion);
            Assert.IsTrue(result);
        }
        #endregion
    }
}
﻿define(["jquery", "Resources", "kendoExt"], function ($, res)
{
    var labels = {};
    var dropDownValue, RegistrationItem, rolesList, RegProcess;

    return {
        model: null,
        init: function (dataItem, lbls)
        {
            labels = lbls;

            $(document).ajaxStart(function ()
            {
                kendo.ui.progress($(".StertRegAjax-loading"), true);
            });

            $(document).ajaxStop(function ()
            {
                kendo.ui.progress($(".StertRegAjax-loading"), false);
            });

            RegProcess = this;
            dropDownValue = null;
            RegistrationItem = $.parseJSON(dataItem);
            RegProcess.model = kendo.observable(RegistrationItem);
            this.initUI();
        },

        initUI: function ()
        {
            var OrganizationTemplate = kendo.template($("#Update_OrganizationTemplate").html());
            var Update_UserTemplate = kendo.template($("#Update_UserTemplate").html());
            var CreateOrAssignDOATemplate = kendo.template($("#CreateOrAssignDOATemplate").html());

            $("#assignOrganization").kendoDropDownList({
                optionLabel: labels.newReg_selectOrganization,
                filter: "contains",
                dataTextField: "Name",
                dataValueField: "Id",
                valuePrimitive: true,
                change: onOrganizationSelect,
                dataSource: {
                    type: "odata-v4",
                    pageSize: 20,
                    serverFiltering: true,
                    transport: {
                        read: {
                            url: "/odata/Organizations?$filter=Name ne 'NAV CANADA'&$select=Id,Name",
                            dataType: "json"
                        }
                    },
                    schema: {
                        data: function (data)
                        {
                            return data["value"];
                        },
                        total: function (data)
                        {
                            return data["odata.count"];
                        },
                        model: {
                            Id: "Id"
                        }
                    }
                }
            });

            function onOrganizationSelect()
            {
                var orglist = $("#assignOrganization").data("kendoDropDownList");
                var dataItem = orglist.dataItem(orglist.select());
                if (dataItem.Id !== "")
                {
                    RegProcess.model.SelectedOrganizationName = dataItem.Name;
                    StartUserRolesStep();
                }

            };

            $("#btnCreateOrg").click(function (e)
            {
                CreateOrganization();
            });

            $("#btnPreviousOrg").click(function (e)
            {
                var assignOrSelectOrganization_win = $("#assignOrSelectOrganization-Popup").data("kendoWindow");
                assignOrSelectOrganization_win.close();
            });

            function CreateOrAssignDOA()
            {
                var CreateOrAssignDOAWindow = $("#CreateOrAssignDOA-Popup").kendoWindow({
                    actions: ["Cancel"],
                    width: 520,
                    resizable: true,
                    visible: false,
                    title: labels.newReg_doaInfo,
                    modal: true
                }).data("kendoWindow");

                CreateOrAssignDOAWindow.bind("refresh", function ()
                {
                    $("#CreateOrAssignDOA-Popup").data("kendoWindow").center();
                    $("#CreateOrAssignDOA-Popup").data("kendoWindow").open();
                });
                CreateOrAssignDOAWindow.content(CreateOrAssignDOATemplate);
                RegProcess.model.DOAId = 0;
                RegProcess.model.AirdromeMid = "";
                $("#CreateOrAssignDOA-Popup").data("kendoWindow")
                    .refresh();

                $("#AssignAirdromes").kendoDropDownList({
                    optionLabel: "Select AD...",
                    filter: "contains",
                    dataTextField: "txtName",
                    dataValueField: "mid",
                    template: '<span class="k-state-default">#: codeId#</span> <span class="k-state-default">#: txtName#</span>',
                    change: onAirdromesSelect,
                    dataSource: {
                        type: "odata-v4",
                        serverFiltering: true,
                        pageSize: 20,
                        transport: {
                            read: {
                                url: "/SDO/Ahp?$select=mid,codeId,txtName",
                                dataType: "json"
                            },
                            parameterMap: function (data) {
                                var d = kendo.data.transports.odata.parameterMap(data);
                                delete d.$format;
                                delete d.$inlinecount;
                                d.$count = true;

                                if (d.$filter) {
                                    if (d.$filter.substring(0, 12) == 'substringof(') {
                                        var parms = d.$filter.substring(12, d.$filter.length - 1).split(',');
                                        d.$filter = "contains(txtName," + parms[0] + ") or contains(codeId," + parms[0] + ")";
                                    }
                                }

                                return d;
                            }

                        },
                        schema: {
                            data: function (data)
                            {
                                return data["value"];
                            },
                            total: function (data)
                            {
                                return data["odata.count"];
                            },
                            model: {
                                Id: "mid"
                            }
                        }
                    }
                });

                $("#AssignDOA").kendoDropDownList({

                    optionLabel: labels.newReg_selectDOA,
                    filter: "contains",
                    dataTextField: "Name",
                    dataValueField: "Id",
                    valuePrimitive: true,
                    change: onDOASelect,
                    dataSource: {
                        type: "odata-v4",
                        pageSize: 20,
                        serverFiltering: true,
                        transport: {
                            read: {
                                url: "/odata/Doas?$select=Id,Name",
                                dataType: "json"
                            }
                        },
                        schema: {
                            data: function (data)
                            {
                                return data["value"];
                            },
                            total: function (data)
                            {
                                return data["odata.count"];
                            },
                            model: {
                                Id: "Id"
                            }
                        }
                    }
                });
                $("#btnPreviousDOA").click(function (e)
                {
                    var CreateOrAssignDOA_win = $("#CreateOrAssignDOA-Popup").data("kendoWindow");
                    CreateOrAssignDOA_win.close();
                    $("#assignOrganization").data("kendoDropDownList").value(-1);
                });

                function onDOASelect()
                {
                    var DOAlist = $("#AssignDOA").data("kendoDropDownList");
                    var dataItem = DOAlist.dataItem(DOAlist.select());
                    if (dataItem.Id != "")
                    {
                        RegProcess.model.DOAId = dataItem.Id;
                        StartUserRolesStep();
                    }

                };

                function onAirdromesSelect()
                {
                    var DOAlist = $("#AssignAirdromes").data("kendoDropDownList");
                    var dataItem = DOAlist.dataItem(DOAlist.select());
                    RegProcess.model.AirdromeMid = dataItem.mid;
                    StartUserRolesStep();
                };

                $("#CreateOrAssignDOA-Popup").data("kendoWindow").wrapper.find(".k-i-cancel").click(function (e)
                {
                    cancelReg();
                });

            };

            function CreateOrganization() {
                var CreateOrgWindow = $("#CreateOrganization-Popup").kendoWindow({
                    actions: ["Cancel"],
                    width: 520,
                    resizable: true,
                    visible: false,
                    title: labels.newReg_organizationInfo,
                    modal: true
                }).data("kendoWindow");
                CreateOrgWindow.bind("refresh", function ()
                {
                    $("#CreateOrganization-Popup").data("kendoWindow").center();
                    $("#CreateOrganization-Popup").data("kendoWindow").open();
                });

                CreateOrgWindow.content(OrganizationTemplate);
                $("#CreateOrganization-Popup").data("kendoWindow")
                    .refresh();
                kendo.bind($('#detailContent'), RegProcess.model.orgVM);

                $("#saveOrgFromReg").show();

                var kendoValidatorOrganization = $("#CreateOrganization-Popup").kendoValidator({
                    messages: {
                        validmask: function (input) {
                            if (input.is("[name=OrganizationTelephone]")) {
                                return labels.newReg_val_phoneIncomplete;
                            }
                            return "";
                        },
                        required: function (input) {
                            if (input.is("[name=Name]")) {
                                return labels.newReg_val_organizationRequired;
                            }
                            if (input.is("[name=Address]")) {
                                return labels.newReg_val_addressRequired;
                            }
                            if (input.is("[name=EmailAddress]")) {
                                return labels.newReg_val_emailRequired;
                            }
                            if (input.is("[name=OrganizationTelephone]")) {
                                return labels.newReg_val_phoneRequired;
                            }
                            if (input.is("[name=TypeOfOperation]")) {
                                return labels.newReg_val_operationRequired;
                            }
                            return "";
                        }
                    },
                    rules: {
                        validmask: function (input) {
                            if (input.is("[name=OrganizationTelephone]")) {
                                if (input.val() !== '') {
                                    var data = input.data("kendoMaskedTextBox");
                                    return data.value().indexOf(data.options.promptChar) === -1;
                                }
                            }
                            return true;
                        }
                    }
                }).data("kendoValidator");

                $("#btnSaveOrg").click(function (e)
                {
                    // validate first if every required field is filled allow the next step
                    if (kendoValidatorOrganization.validate()) {
                        RegProcess.model.SelectedOrganizationName = RegProcess.model.orgVM.Name;
                        CreateOrAssignDOA();
                    }

                });
                $("#btnPreviousOrgFromReg").click(function (e)
                {
                    var CreateOrganization_win = $("#CreateOrganization-Popup").data("kendoWindow");
                    CreateOrganization_win.close();
                });

                $("#CreateOrganization-Popup").data("kendoWindow").wrapper.find(".k-i-cancel").click(function (e)
                {
                    cancelReg();
                });

                $("#OrganizationTelephone").kendoMaskedTextBox({
                    mask: "(000) 000-0000"
                });

            };

            function StartUserRolesStep()
            {
                var UpdateUserWindow = $("#UpdateUser-Popup").kendoWindow({
                    actions: ["Cancel"],
                    width: 520,
                    resizable: false,
                    visible: false,
                    title: labels.newReg_updateUser_title,
                    modal: true,
                }).data("kendoWindow");

                UpdateUserWindow.bind("refresh", function ()
                {
                    $("#UpdateUser-Popup").data("kendoWindow").center();
                    $("#UpdateUser-Popup").data("kendoWindow").open();

                });
                UpdateUserWindow.content(Update_UserTemplate);
                $("#UpdateUser-Popup").data("kendoWindow")
                    .refresh();
                kendo.bind($('#Update_DetailContent'), RegProcess.model.userVM);
                if (!rolesList)
                {
                    $.ajax({
                        cache: false,
                        async: false,
                        type: "Get",
                        dataType: 'json',
                        url: "/NewRegistration/GetRoles",
                        success: function (data)
                        {
                            rolesList = data;
                            //$('[name="Update_UserRolesDisplay"]').text(RegProcess.model.SelectedOrganizationName);
                        }
                    })

                }

                RolesSelector();
                OrgSelector();

                var kendoValidatorUserInformation = $("#UpdateUser-Popup").kendoValidator({
                    messages: {
                        validmask: function (input) {
                            if (input.is("[id=update-telephone]")) {
                                return labels.newReg_val_phoneIncomplete;
                            }
                            if (input.is("[id=update-fax]")) {
                                return labels.newReg_val_faxIncomplete;
                            }
                            return "";
                        },
                        email: labels.newReg_val_emailIncomplete,
                        required: function (input) {
                            if (input.is("[id=update_username]")) {
                                return labels.newReg_val_usernameRequired;
                            }
                            if (input.is("[id=Update_UserRolesMultiSelect]")) {
                                return labels.newReg_val_roleRequired;
                            }
                            if (input.is("[id=update-firstname]")) {
                                return labels.newReg_val_firstnameRequired;
                            }
                            if (input.is("[id=update-lastname]")) {
                                return labels.newReg_val_lastnameRequired;
                            }
                            if (input.is("[id=update-email]")) {
                                return labels.newReg_val_emailRequired;
                            }
                            if (input.is("[id=update-position]")) {
                                return labels.newReg_val_positionRequired;
                            }
                            if (input.is("[id=update-address]")) {
                                return labels.newReg_val_addressRequired;
                            }
                            if (input.is("[id=update-telephone]")) {
                                return labels.newReg_val_phoneRequired;
                            }
                            return "";
                        }
                    },
                    rules: {
                        validmask: function (input) {
                            if (input.is("[id=update-telephone]")) {
                                if (input.val() !== '') {
                                    var data = input.data("kendoMaskedTextBox");
                                    return data.value().indexOf(data.options.promptChar) === -1;
                                }
                            }
                            if (input.is("[id=update-fax]")) {
                                if (input.val() !== '') {
                                    var data = input.data("kendoMaskedTextBox");
                                    return data.value().indexOf(data.options.promptChar) === -1;
                                }
                            }
                            return true;
                        }
                    }
                }).data("kendoValidator");

                $("#update-telephone").kendoMaskedTextBox({
                    mask: "(000) 000-0000"
                });
                $("#update-fax").kendoMaskedTextBox({
                    mask: "(000) 000-0000"
                });

                //setValidatorUserInformation();
                //wireControllersUserInformation();

                $("#saveUserFromReg").show();
                $("#IsApprovedtr").hide();
                $("#IsDisabledtr").hide();
                $("#btnPreviousUserFromReg").click(function (e)
                {
                    var UpdateUser_win = $("#UpdateUser-Popup").data("kendoWindow");
                    UpdateUser_win.close();
                    if ($("#CreateOrganization-Popup").data("kendoWindow"))
                    {
                        $("#AssignDOA").data("kendoDropDownList").value(-1);
                        $("#AssignAirdromes").data("kendoDropDownList").value(-1);
                        RegProcess.model.DOAId = 0;
                        RegProcess.model.AirdromeMid = "";
                    }
                    $("#assignOrganization").data("kendoDropDownList").value(-1);
                });

                $("#btnCompleteRegistration").click(function (e)
                {
                    if (kendoValidatorUserInformation.validate())
                    {
                        $.ajax({
                            url: '/NewRegistration/CompleteRegistration',
                            type: 'POST',
                            contentType: 'application/json',
                            dataType: 'json',
                            async: false,
                            cache: false,
                            data: JSON.stringify({ UserRegistrationManagement: RegProcess.model }),

                            success: function (data) {
                                if (data.Success === true) { //need to refresh grid
                                    $("#userGrid").data('kendoGrid').dataSource.read();
                                    $("#userGrid").data('kendoGrid').refresh();
                                    kendo.ui.ExtAlertDialog.show({
                                        message: data.Message,
                                        icon: "k-ext-information"
                                    });
                                }
                                else {
                                    var errorMessage = labels.newReg_approveError_message + " " + labels.newReg_rejectError_message_cont + (data.Message !== undefined ? " - " + data.Message : "");
                                    kendo.ui.ExtAlertDialog.show({ //need to refresh grid
                                        title: labels.newReg_rejectError_unidentify,
                                        message: errorMessage,
                                        icon: "k-ext-information"
                                    });
                                }
                            }

                        });
                        closeallkendoWindows();
                    }
                });

                $("#UpdateUser-Popup").data("kendoWindow").wrapper.find(".k-i-cancel").click(function (e)
                {
                    cancelReg();
                });

            };

            function showRoles(data)
            {

                RoleMutliSelect = $('#Update_UserRolesMultiSelect').data('kendoMultiSelect');
                RoleMutliSelect.dataSource.filter({});

                var res = [];
                if (data)
                {
                    $.each(data, function (idx, elem)
                    {
                        if (data[idx].text === "Organization Administrator" || data[idx].text === "User")
                        {
                            res.push(data[idx].value);
                        }
                    });
                    RoleMutliSelect.value(res);
                }
            };

            function RolesSelector()
            {
                $('[name="Update_UserName"]').attr('readonly', true);
                $('[name="PasswordTr"]').hide();
                $('[name="ConfirmPasswordTr"]').hide();
                if (!$('#Update_UserRolesMultiSelect').data('kendoMultiSelect'))
                {
                    $("#Update_UserRolesMultiSelect")
                                .kendoMultiSelect({
                                    dataTextField: "text",
                                    dataValueField: "value",
                                    dataSource: rolesList,
                                    valuePrimitive: true
                                });
                }
                showRoles(rolesList);

            }

            function OrgSelector()
            {
                $('[name="Update_UserOrgDisplay"]').val(RegProcess.model.SelectedOrganizationName);
                $('[name="Update_UserOrgDisplay"]').show();
                $('[name="Update_UserOrgDisplay"]').attr('readonly', true);
                $("#Update_UserOrgContainer").hide();
            }

            function closeallkendoWindows()
            {
                if ($("#userDetail-Popup").data("kendoWindow"))
                {
                    $("#userDetail-Popup").data("kendoWindow").close();
                }
                if ($("#assignOrSelectOrganization-Popup").data("kendoWindow"))
                {
                    $("#assignOrSelectOrganization-Popup").data("kendoWindow").close();
                }
                if ($("#CreateOrganization-Popup").data("kendoWindow"))
                {
                    $("#CreateOrganization-Popup").data("kendoWindow").close();
                    $("#CreateOrganization-Popup").data("kendoWindow").destroy();
                }

                if ($("#UpdateUser-Popup").data("kendoWindow"))
                {
                    $("#UpdateUser-Popup").data("kendoWindow").close();
                    $("#UpdateUser-Popup").data("kendoWindow").destroy();
                }

                if ($("#CreateOrAssignDOA-Popup").data("kendoWindow"))
                {
                    $("#CreateOrAssignDOA-Popup").data("kendoWindow").close();
                    $("#CreateOrAssignDOA-Popup").data("kendoWindow").destroy();
                }

            }

            function cancelReg()
            {
                kendo.ui.ExtOkCancelDialog.show({
                    message: labels.newReg_cancelRegistration,
                    width: '400px',
                    height: '150px',
                    icon: "k-ext-warning"
                }).done(function (response)
                {
                    if (response.button === 'OK')
                    {
                        closeallkendoWindows();
                    }

                });

            };
        },
    };

});